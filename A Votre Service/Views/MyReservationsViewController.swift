//
//  MyReservationsViewController.swift
//  A Votre Service
//
//  Created by Oscar Sevilla on 05/08/21.
//  Copyright © 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit
import SwiftyUserDefaults
import AsyncDisplayKit
import SVProgressHUD
import Fakery
import PromiseKit
import Combine
import DifferenceKit

class MyReservationsViewController: UIViewController {
    
    @IBOutlet var reservationButton: UIButton!
    @IBOutlet var dateReservation: UILabel!
    @IBOutlet var activeReservations: UILabel!
    @IBOutlet var nextIcon: UIImageView!
    @IBOutlet var conciergeName: UILabel!
    
    var concierges = [User]()
    
    enum ReservationFilter {
        case all
        case upcoming
        case past
        case byDate(Date)

        var displayString: String {
            switch self {
            case .all:
                return "All"
            case .upcoming:
                return "Upcoming"
            case .past:
                return "Past"
            case .byDate:
                return "By Date"
            }
        }

        var selectedString: String {
            switch self {
            case .all, .upcoming, .past:
                return displayString
            case .byDate(let date):
                return date.avsDateShortMonthYearString()
            }
        }

        var startDate: Date? {
            switch self {
            case .all:
                return Date(timeIntervalSince1970: 0)
            case .upcoming:
                return Date()
            case .byDate(let date):
                return date.convertedToBuildingTimeZone(.startOfDay)
            case .past:
                return nil
            }
        }

        var endDate: Date? {
            switch self {
            case .all:
                return 10.years.later
            case .upcoming:
                return nil
            case .past:
                return Date()
            case .byDate(let date):
                return date.convertedToBuildingTimeZone(.endOfDay)
            }
        }
    }
    
    enum Sections: Int, CaseIterable, Differentiable {
        case filter
        case reservations
    }
    
    lazy var emptyNode = EmptyNode(emptyNodeType: .reservation)
    lazy var refreshControl = UIRefreshControl()
    private var subscriptions = Set<AnyCancellable>()
    var dataSource = ObservablePaginatedDataSource<AmenityReservation>()
    private var sections = [ArraySection<Sections, AnyDifferentiable>]()
    var filter = ReservationFilter.upcoming {
        didSet { refreshItems() }
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
        dataSource.dynamicCacheable = { [weak self] in
            guard let self = self else { return false }
            switch self.filter {
            case .upcoming:
                return true
            default:
                return false
            }
        }
        
        dataSource.cacheKey = "my_reservations"
        dataSource.paginate = { per, page in
            ResidentService().getAmenityReservationList(page: page, per: per, startDate: self.filter.startDate, endDate: self.filter.endDate)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addComponents()
        setupBindings()
        refreshItems()
        activeReservations.text = "ACTIVE RESERVATIONS - \(dataSource.items.count)"
        if dataSource.items.count > 0 {
            reservationButton.setTitle(dataSource.items[0].amenityName, for: .normal)
            dateReservation.text = dataSource.items[0].startTime?.avsDateFormatted
        }else{
            reservationButton.isHidden = true
            dateReservation.isHidden = true
            nextIcon.isHidden = true
        }
        
        if let concierges = [User].readCached(key: "feed_concierges") {
            self.concierges = concierges
        }
        if self.concierges.count>0 {
            conciergeName.text = self.concierges[0].firstName
        }else{
            reloadConcierges()
            if self.concierges.count>0{
                conciergeName.text = self.concierges[0].firstName
            }else{
                conciergeName.isHidden = true
            }
        }
    }
    
    func addComponents(){
        var y = 83
        if(UIScreen.main.bounds.height<800) {
            y -= 35
        }
        let tabBar = BottonTabBarView(frame: CGRect(x: 0, y: UIScreen.main.bounds.height-CGFloat(y), width: UIScreen.main.bounds.width, height: 83))
        self.view.addSubview(tabBar)
    }
    
    func reloadConcierges() {
        ResidentService().getConciergeOnDutyList().done { [weak self] users in
            guard let self = self else { return }
            users.cache(key: "feed_concierges")
            self.concierges = users
        }.catch { error in
            Log.thisError(error)
        }
    }
        
    func reloadData() {
        let showsFilter: Bool
        switch filter {
        case .all:
            showsFilter = false
        default:
            showsFilter = true
        }

        let filterSectionItems: [AnyDifferentiable] = showsFilter ? [AnyDifferentiable(filter.displayString)] : []
        let reservationItems = dataSource.items.map { AnyDifferentiable($0) }

        let target: [ArraySection<Sections, AnyDifferentiable>] = [
            ArraySection(model: .filter, elements: filterSectionItems),
            ArraySection(model: .reservations, elements: reservationItems)
        ]

        let stagedChangeSet = StagedChangeset(source: sections, target: target)
        for changeSet in stagedChangeSet {
            sections = changeSet.data
        }
    }
    
    func setupBindings() {
        dataSource.$items
                .receive(on: DispatchQueue.main)
                .sink { [weak self] items in
                    self?.reloadData()
                }.store(in: &subscriptions)

        dataSource.$hasNoItemsAfterLoading
                .receive(on: DispatchQueue.main)
                .sink { [weak self] hasNoItemsAfterLoading in
                    self?.showEmptyScreen(hasNoItemsAfterLoading)
                }.store(in: &subscriptions)

        dataSource.$showLoadingIndicator
                .receive(on: DispatchQueue.main)
                .sink { [weak self] loading in
                    guard let self = self else { return }

                    if loading {
                        if !self.refreshControl.isRefreshing {
                            SVProgressHUD.show()
                        }
                    } else {
                        SVProgressHUD.dismiss()
                        self.refreshControl.endRefreshing()
                    }
                }.store(in: &subscriptions)
    }
    
    @objc func refreshItems() {
        dataSource.loadMoreContentIfNeeded()
    }
    
    func showEmptyScreen(_ show: Bool) {
        emptyNode.isHidden = !show
    }
    
    @objc func tapCreateButton() {
        guard let tabBarController = tabBarController as? ResidentTabBarViewController else { return }
        tabBarController.switchTo(controllerType: .amenities)
    }
    
    @IBAction func chatButton(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController(page: 1))
    }
    
    @IBAction func backButton(_ sender: Any){
        AppState.switchTo(viewController: ResidentTabBarViewController(page: nil))
    }

    @IBAction func reservationPreviewAction(_ sender: Any) {
        if dataSource.items.count > 0 {
            let previewVC = AmenityReservationPreviewViewController(reservation: dataSource.items[0])

            previewVC.reloadReservations = { [weak self] in
                guard let self = self else { return }
                self.refreshItems()
            }

            previewVC.canceledReservation = { [weak self] in
                guard let self = self else { return }
                self.navigationController?.popToViewController(self, animated: true)
                SVProgressHUD.show()
                self.refreshItems()
            }
            AppState.switchTo(viewController: previewVC)
        }
    }
}
