//
//  MyBuildingViewController.swift
//  A Votre Service
//
//  Created by Oscar Sevilla on 05/08/21.
//  Copyright © 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit

class MyBuildingViewController: UIViewController {
    
    @IBOutlet var buildingPicture: UIImageView!
    @IBOutlet var buildingName: UILabel!
    @IBOutlet var buildingAddress: UILabel!
    @IBOutlet var conciergeName: UILabel!
    var concierges = [User]()
    var images: [URL]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addComponents()
        reloadBuilding()
        if let concierges = [User].readCached(key: "feed_concierges") {
            self.concierges = concierges
        }
        if self.concierges.count>0 {
            conciergeName.text = self.concierges[0].firstName
        }else{
            reloadConcierges()
            if self.concierges.count>0{
                conciergeName.text = self.concierges[0].firstName
            }else{
                conciergeName.isHidden = true
            }
        }
    }
    
    func addComponents(){
        var y = 83
        if(UIScreen.main.bounds.height<800) {
            y -= 35
        }
        let tabBar = BottonTabBarView(frame: CGRect(x: 0, y: UIScreen.main.bounds.height-CGFloat(y), width: UIScreen.main.bounds.width, height: 83))
        self.view.addSubview(tabBar)
    }
    
    func reloadConcierges() {
        ResidentService().getConciergeOnDutyList().done { [weak self] users in
            guard let self = self else { return }
            users.cache(key: "feed_concierges")
            self.concierges = users
        }.catch { error in
            Log.thisError(error)
        }
    }
    
    func reloadBuilding() {
        BuildingDetails.getBuildingImages().done { images in
            self.images = images
            print(self.images![0])
            if let image = self.images {
                if image.count > 1 {
                    self.downloadImage(from: image[1])
                } else if images.count > 0 {
                    self.downloadImage(from: image[0])
                }
            }
        }.catch{ error in
            Log.thisError(error)
        }
        
        BuildingDetails.getBuildingAddress().done { [weak self] address in
            self!.buildingAddress.text = address
            print("La dirección es: \(address)")
        }.catch { error in
            Log.thisError(error)
        }
        
        BuildingDetails.getBuildingName(force: true).done { [weak self] name in
            self!.buildingName.text = name
            print("El nombre del edificio es: \(name)")
        }.catch { error in
            Log.thisError(error)
        }
    }
    
    func downloadImage(from url: URL) {
        print("Download Started")
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            DispatchQueue.main.async() { [weak self] in
                self?.buildingPicture.image = UIImage(data: data)
            }
        }
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    @IBAction func chatButton(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController(page: 1))
    }
    
    @IBAction func backButton(_ sender: Any){
        AppState.switchTo(viewController: ResidentTabBarViewController(page: nil))
    }
    
    @IBAction func feedButton(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController(page: nil))
    }
    
    @IBAction func chatButton2(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController(page: 1))
    }
    
    @IBAction func reserveButton(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController(page: 2))
    }
    
    @IBAction func offerngButton(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController(page: 3))
    }
    
    @IBAction func thingstoknowButton(_ sender: Any) {
        AppState.switchTo(viewController: ConciergeThingsToKnowListViewController())
    }
    
    @IBAction func buildingFAQsButton(_ sender: Any) {
        AppState.switchTo(viewController: AmenityListViewController(requiresReservation: false))
    }
    
    @IBAction func upcomingEventsButton(_ sender: Any) {
        
    }
    
}
