//
//  OfferingViewController.swift
//  A Votre Service
//
//  Created by Oscar Sevilla Garduño on 03/08/21.
//  Copyright © 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD
import Combine
import DifferenceKit

class OfferingViewController: UIViewController {
    
    var items = [ALaCarteCategory]()
    var cartManager: CartProductManager
    private var isMainCategory = true
    private var subscriptions = Set<AnyCancellable>()
    
    init(cartManager: CartProductManager, items: [ALaCarteCategory]? = nil) {
        self.cartManager = cartManager
        self.items = items ?? []
        isMainCategory = items == nil
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addComponents()
        
        cartManager.$totalProductsCount.receive(on: DispatchQueue.main)
                                       .sink { [weak self] count in
                                           self?.updateBadgeLabel()
                                       }.store(in: &subscriptions)
        
        if isMainCategory {
            loadCachedItems()
            if items.count == 0 {
                SVProgressHUD.show()
            }
            refreshItems()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func addComponents(){
        let lateralMenu = LateralMenuView(frame: CGRect(x: 80, y: 0, width: 347, height: UIScreen.main.bounds.height))
        let myView = HeaderTabBarView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 190), tabBarVC: self, lm: lateralMenu)
        lateralMenu.isHidden = true
        lateralMenu.header = myView
        lateralMenu.rootView = self
        self.view.addSubview(myView)
        self.view.addSubview(lateralMenu)
    }
    
    
    func updateBadgeLabel() {
        print(cartManager.totalProductsCountString)
    }
    
    func loadCachedItems() {
        if let items = CodableCache.get([ALaCarteCategory].self, key: "a_la_carte_main_category") {
            reloadData(items)
        }
    }
    
    func reloadData(_ target: [ALaCarteCategory]) {
        let source = items

        let stagedChangeSet = StagedChangeset(source: source, target: target)
        for changeSet in stagedChangeSet {
            items = changeSet.data
        }
    }
    
    @objc func refreshItems() {
        ResidentService().getALaCarteCategories().done { [weak self] categories in
            guard let self = self else { return }
            SVProgressHUD.dismiss()
            self.reloadData(categories)
            CodableCache.set(categories, key: "a_la_carte_main_category")
        }.ensure { [weak self] in
            guard self != nil else { return }
            print("Se actualizó correctamente")
        }.catch { error in
            SVProgressHUD.showError(withStatus: error.localizedDescription)
            Log.this(error)
        }
    }
    
    @IBAction func carButton(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController.buildNavControllerType(title: "Services", vc: OfferingServiceViewController(cartManager: cartManager, category: items[0])))
    }
    
    @IBAction func dogButton(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController.buildNavControllerType(title: "Services", vc: OfferingServiceViewController(cartManager: cartManager, category: items[1])))
    }
    
    @IBAction func gourmetButton(_ sender: Any) {
        let category = items[2]
        AppState.switchTo(viewController: ResidentTabBarViewController.buildNavControllerType(title: "Services", vc: GourmetHomeViewController(cartManager: cartManager, items: category.children)))
    }
    
    @IBAction func helpButton(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController.buildNavControllerType(title: "Services", vc: OfferingServiceViewController(cartManager: cartManager, category: items[3])))
    }
    
    @IBAction func maidButton(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController.buildNavControllerType(title: "Services", vc: OfferingServiceViewController(cartManager: cartManager, category: items[4])))
    }
    
    @IBAction func chefButton(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController.buildNavControllerType(title: "Services", vc: OfferingServiceViewController(cartManager: cartManager, category: items[5])))
    }
}
