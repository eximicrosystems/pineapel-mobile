//
//  WelcomePageViewController.swift
//  A Votre Service
//
//  Created by Oscar Sevilla Garduño on 04/08/21.
//  Copyright © 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit
import SwiftyUserDefaults

class  WelcomePageViewController: UIViewController {

    @IBOutlet var welcomeLbl: UILabel!
    var user: User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.welcomeLbl.text = "Welcome \(Defaults.user?.firstName ?? ""),"
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: {
            AppState.switchTo(viewController: ResidentTabBarViewController(page: nil))
        })
    }
}
