//
//  MyProfileViewController.swift
//  Pineapel
//
//  Created by Oscar Sevilla Garduño on 24/09/21.
//  Copyright © 2021 A Votre Service LLC. All rights reserved.
//

import UIKit
import SwiftyUserDefaults
import SVProgressHUD
import PromiseKit

class MyProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet var profileButton: UIButton!
    @IBOutlet var picture: UIImageView!
    @IBOutlet var saveButton: UIButton!
    @IBOutlet var firstName: UITextField!
    @IBOutlet var lastName: UITextField!
    @IBOutlet var mobilePhone: UITextField!
    @IBOutlet var email: UITextField!
    @IBOutlet var password: UITextField!
    
    private var isCamera = true
    private var imgData: Data?
    var userID: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addComponents()
        loadData()
    }
    
    func addComponents(){
        var y = 83
        if(UIScreen.main.bounds.height<800) {
            y -= 35
        }
        let tabBar = BottonTabBarView(frame: CGRect(x: 0, y: UIScreen.main.bounds.height-CGFloat(y), width: UIScreen.main.bounds.width, height: 83))
        let lateralMenu = LateralMenuView(frame: CGRect(x: 80, y: 0, width: 347, height: UIScreen.main.bounds.height))
        let myView = HeaderTabBarView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 190), tabBarVC: self, lm: lateralMenu)
        lateralMenu.isHidden = true
        lateralMenu.header = myView
        lateralMenu.rootView = self
        self.view.addSubview(myView)
        self.view.addSubview(tabBar)
        self.view.addSubview(lateralMenu)
    }
    
    func loadData(){
        if let userid = Defaults.user?.uid { userID = userid }
        if let fn = Defaults.user?.firstName { firstName.text = fn }
        if let ln = Defaults.user?.lastName { lastName.text = ln }
        if let mp = Defaults.user?.phoneNumber { mobilePhone.text = mp }
        if let mail = Defaults.user?.email { email.text = mail }
        //if let password = Defaults.user? { email.text = mail }
        if let userImage = Defaults.user?.userBioImageURL {
            downloadImage(from: userImage)
            picture.layer.cornerRadius = 40
        }
    }
    
    func downloadImage(from url: URL) {
        print("Download Started")
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            DispatchQueue.main.async() { [weak self] in
                self?.picture.image = UIImage(data: data)
            }
        }
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    @IBAction func imageButton(_ sender: Any) {
        selectImage()
    }
    
    func selectImage(){
        let message = NSLocalizedString("Select an option", comment: "")
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default) { [unowned self] _ in
            isCamera = true
            let picker = UIImagePickerController()
            picker.sourceType = .camera
            picker.delegate = self
            present(picker, animated: true, completion: nil)
        })
        alert.addAction(UIAlertAction(title: "Photo Library", style: .default) { [unowned self] _ in
            isCamera = false
            let picker = UIImagePickerController()
            picker.sourceType = .photoLibrary
            picker.delegate = self
            picker.allowsEditing = true
            present(picker, animated: true, completion: nil)
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        view.endEditing(true)
        picker.dismiss(animated: true, completion: nil)
        if isCamera{
            guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
            self.picture.image = image
        }else {
            guard let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else { return }
            self.picture.image = image
        }
        self.picture.layer.cornerRadius = 40
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
        
    @IBAction func saveButton(_ sender: Any) {
        SVProgressHUD.show()
        let apiService = ResidentService()
        var userProfile = UserProfile(userID: userID, firstName: firstName.text, lastName: lastName.text, phoneNumber: mobilePhone.text)
        userProfile.email = email.text

        ResidentService().updateUserProfile(userID: userID!, profile: userProfile).done { profile in
            SVProgressHUD.dismiss()
        }.catch { error in
            Log.thisError(error)
            SVProgressHUD.showError(withStatus: error.localizedDescription)
        }

        SVProgressHUD.show()
        guard let imageData = self.picture.image!.jpegData(compressionQuality: 1.0) else{ return }
        imgData = imageData

        ResidentService().uploadImage(imageData: imgData!).then { (fileUploadResponse: FileUploadResponse) -> Promise<UserProfile> in
            let profile = UserProfile(userID: self.userID, userPicture: UserPicture(targetID: fileUploadResponse.fileID, width: nil, height: nil, url: nil))
            return apiService.updateUserProfile(userID: self.userID!, profile: profile)
        }.done { [weak self] userProfile in
            guard self != nil else { return }
            SVProgressHUD.dismiss()
        }.catch { error in
            SVProgressHUD.showError(withStatus: error.localizedDescription)
            Log.thisError(error)
        }
    }
}
