//
//  ProductsNotFoundViewController.swift
//  Pineapel
//
//  Created by Oscar Sevilla Garduño on 29/09/21.
//  Copyright © 2021 A Votre Service LLC. All rights reserved.
//

import UIKit

class ProductsNotFoundViewController: UIViewController {

    @IBOutlet var icon: UIImageView!
    @IBOutlet var ttitle: UILabel!
    @IBOutlet var subtitle: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addComponents()
        icon.image = Asset.Images.icGourmetCart.image
        ttitle.attributedText = L10n.Empty.ProductList.title.attributed(.H2, .primaryLabel, alignment: .center)
        subtitle.attributedText = L10n.Empty.ProductList.subtitle.attributed(.body, .primaryLabel, alignment: .center)
    }
    
    func addComponents(){
        var y = 83
        if(UIScreen.main.bounds.height<800) {
            y -= 35
        }
        let tabBar = BottonTabBarView(frame: CGRect(x: 0, y: UIScreen.main.bounds.height-CGFloat(y), width: UIScreen.main.bounds.width, height: 83))
        let lateralMenu = LateralMenuView(frame: CGRect(x: 80, y: 0, width: 347, height: UIScreen.main.bounds.height))
        let myView = HeaderTabBarView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 190), tabBarVC: self, lm: lateralMenu)
        lateralMenu.isHidden = true
        lateralMenu.header = myView
        lateralMenu.rootView = self
        self.view.addSubview(myView)
        self.view.addSubview(tabBar)
        self.view.addSubview(lateralMenu)
    }
    
    @IBAction func chatButton(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController(page: 1))
    }
}
