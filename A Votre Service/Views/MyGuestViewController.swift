//
//  MyGuestViewController.swift
//  A Votre Service
//
//  Created by Oscar Sevilla on 05/08/21.
//  Copyright © 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit

class MyGuestViewController: UIViewController {
    
    @IBOutlet var conciergeName: UILabel!
    var concierges = [User]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addComponents()
        if let concierges = [User].readCached(key: "feed_concierges") {
            self.concierges = concierges
        }
        if self.concierges.count>0 {
            conciergeName.text = self.concierges[0].firstName
        }else{
            reloadConcierges()
            if self.concierges.count>0{
                conciergeName.text = self.concierges[0].firstName
            }else{
                conciergeName.isHidden = true
            }
        }
    }
    
    func addComponents(){
        var y = 83
        if(UIScreen.main.bounds.height<800) {
            y -= 35
        }
        let tabBar = BottonTabBarView(frame: CGRect(x: 0, y: UIScreen.main.bounds.height-CGFloat(y), width: UIScreen.main.bounds.width, height: 83))
        self.view.addSubview(tabBar)
    }
    
    func reloadConcierges() {
        ResidentService().getConciergeOnDutyList().done { [weak self] users in
            guard let self = self else { return }
            users.cache(key: "feed_concierges")
            self.concierges = users
        }.catch { error in
            Log.thisError(error)
        }
    }
    
    @IBAction func chatButton(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController(page: 1))
    }
    
    @IBAction func backButton(_ sender: Any){
        AppState.switchTo(viewController: ResidentTabBarViewController(page: nil))
    }
}
