//
// Created by Andrei Stoicescu on 25/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation

struct Page {
    static var defaultPerPage = 25

    let per: Int
    let page: Int

    var all: Bool = false

    static var `default`: Page {
        Page(per: Page.defaultPerPage, page: 0)
    }

    static var all: Page {
        Page(per: 0, page: 0, all: true)
    }

    var params: [String: Any] {
        ["items_per_page": all ? "All" : per, "page": page]
    }
}