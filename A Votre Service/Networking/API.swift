//
//  API.swift
//  A Votre Service
//
//  Created by Andrei Stoicescu on 02/06/2020.
//  Copyright © 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyUserDefaults
import PromiseKit
import Combine

struct ContentType {
    static let applicationJson = "application/json"
    static let formUrlEncoded = "application/x-www-form-urlencoded"
    static let multipartFormData = "multipart/form-data"
    static let octetStreamFormData = "application/octet-stream"
}

class AVSInterceptor: RequestInterceptor {
    var retryQueue: [(RetryResult) -> ()] = []

    private var subscriptions = Set<AnyCancellable>()

    init() {
        RefreshTokenManager.shared.$refreshTokenResult
                .receive(on: DispatchQueue.main)
                .sink { result in
                    guard let result = result else { return }
                    self.refreshTokenFinished(result: result)
                }.store(in: &subscriptions)
    }

    func adapt(_ urlRequest: URLRequest, for session: Session, completion: @escaping (Swift.Result<URLRequest, Error>) -> ()) {
        var urlRequest = urlRequest

        if urlRequest.value(forHTTPHeaderField: "Content-Type") == nil {
            urlRequest.headers.add(.contentType(ContentType.applicationJson))
        }

        if let accessToken = AppState.refreshTokenCredentials?.accessToken {
            urlRequest.headers.add(.authorization(bearerToken: accessToken))
            completion(.success(urlRequest))
        } else {
            completion(.failure(APIError(message: "Missing Access Token")))
        }
    }

    func retry(_ request: Request, for session: Session, dueTo error: Error, completion: @escaping (RetryResult) -> ()) {
        Log.this("Retry due to error \(error.localizedDescription)")
        guard let statusCode = request.response?.statusCode,
              AppState.isLoggedIn else {
            completion(.doNotRetry)
            return
        }

        switch statusCode {
        case 401:
            retryQueue.append(completion)
            RefreshTokenManager.shared.refresh()
        default:
            completion(.doNotRetry)
        }
    }

    func refreshTokenFinished(result: Swift.Result<RefreshTokenResponse, APIError>) {
        switch result {
        case .success:
            retryQueue.forEach { $0(.retry) }
        case .failure:
            retryQueue.forEach { $0(.doNotRetry) }
        }

        retryQueue.removeAll()
    }
}

class API {
    static var shared = API()
    public var session: Alamofire.Session

    public var timezoneISO8601Decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        return decoder
    }()

    public var secondsDecoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .secondsSince1970
        return decoder
    }()

    init() {
        let config = URLSessionConfiguration.ephemeral
        let storage = HTTPCookieStorage.shared
        storage.cookieAcceptPolicy = .never
        config.httpCookieStorage = storage
        config.headers = .default
        session = Alamofire.Session(configuration: config, interceptor: AVSInterceptor())
    }
}
