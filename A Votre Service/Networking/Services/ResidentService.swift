//
// Created by Andrei Stoicescu on 03/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyUserDefaults
import PromiseKit
import Fakery

class ResidentService: AVSService<ResidentRouter> {
    static let per: Int = 25

    override init() {
        super.init()
        useRefreshToken = true
        session = API.shared.session
    }

    func getUser() -> Promise<User> {
        request(.GetUser, type: [User].self).firstValue.get { Defaults.user = $0 }
    }

    func uploadImage(imageData: Data) -> Promise<FileUploadResponse> {
        refreshTokenIfNeeded().then {
            self.session.upload(imageData, with: ResidentRouter.UploadUserImage).validate().responseDecodable()
        }
    }

    func updateUserProfile(userID: String, profile: UserProfile) -> Promise<UserProfile> {
        request(.UpdateUserProfile(userID: userID, params: profile))
    }

    func getFeedNotificationList(page: Int = 0, per: Int = Page.defaultPerPage) -> Promise<[FeedNotificationItem]> {
        request(.GetFeedNotificationList(Page(per: per, page: page)), decoder: API.shared.timezoneISO8601Decoder)
    }

    func getFeedNotification(notificationID: String) -> Promise<FeedNotificationItem> {
        request(.GetFeedNotification(notificationID: notificationID), decoder: API.shared.timezoneISO8601Decoder)
    }

    func getConciergeList(page: Int = 0, per: Int = Page.defaultPerPage) -> Promise<[User]> {
        request(.GetConciergeList(Page(per: per, page: page)))
    }

    func getBuildingDetails() -> Promise<BuildingDetails> {
        requestSingle(.GetBuildingDetails).get {
            Defaults.buildingID = $0.buildingID
            Defaults.buildingName = $0.name
            Defaults.buildingTimeZone = $0.timezone
        }
    }

    func getAmenityList(requiresReservation: Bool = false, page: Int = 0, per: Int = Page.defaultPerPage) -> Promise<[Amenity]> {
        request(.GetAmenityList(requiresReservation: requiresReservation, page: Page(per: per, page: page)))
    }

    func getAllAmenities(requiresReservation: Bool = true) -> Promise<[Amenity]> {
        request(.GetAmenityList(requiresReservation: requiresReservation, page: .all))
    }

    func getAmenity(amenityID: String) -> Promise<Amenity> {
        requestSingle(.GetAmenity(amenityID: amenityID))
    }

    func getBuildingInfo(buildingID: String) -> Promise<[Article]> {
        request(.GetBuildingInfoArticles(buildingID: buildingID))
    }

    func getBuildingFAQ(buildingID: String) -> Promise<[Article]> {
        request(.GetBuildingFAQ(buildingID: buildingID))
    }

    func getBuildingThingsToKnow(buildingID: String) -> Promise<[Article]> {
        request(.GetBuildingThingsToKnow(buildingID: buildingID))
    }

    func getResidentGuests(per: Int, page: Int) -> Promise<[ResidentGuest]> {
        request(.GetResidentGuests(.init(per: per, page: page)))
    }

    func createResidentGuest(_ residentGuest: ResidentGuestRequest) -> Promise<ResidentGuest> {
        request(.CreateResidentGuest(params: residentGuest))
    }

    func deleteResidentGuest(guestID: String) -> Promise<Void> {
        emptyRequest(.DeleteResidentGuest(guestID: guestID))
    }

    func updateResidentGuest(guestID: String, residentGuest: ResidentGuestRequest) -> Promise<Void> {
        emptyRequest(.UpdateResidentGuest(guestID: guestID, params: residentGuest))
    }

    func getFeedbackList() -> Promise<[Feedback]> {
        request(.GetFeedbackList)
    }

    func createFeedback(_ feedback: Feedback) -> Promise<Feedback> {
        request(.CreateFeedback(params: feedback))
    }

    func deleteFeedback(feedbackID: String) -> Promise<Void> {
        emptyRequest(.DeleteFeedback(feedbackID: feedbackID))
    }

    func getTerms() -> Promise<TextArticle> {
        requestSingle(.GetTerms)
    }

    func getPrivacy() -> Promise<TextArticle> {
        requestSingle(.GetPrivacy)
    }

    func getSettings() -> Promise<Settings> {
        request(.GetSettings)
    }

    func updateSettings(_ settings: Settings) -> Promise<Void> {
        emptyRequest(.UpdateSettings(params: settings))
    }

    func getAmenityAvailability(amenityID: String, date: Date) -> Promise<AvailableHours> {
        request(.GetAmenityAvailability(amenityID: amenityID, date: date))
    }

    func getAmenityDaysAvailability(amenityID: String, startDate: Date, endDate: Date) -> Promise<AvailableDays> {
        request(.GetAmenityDaysAvailability(amenityID: amenityID, startDate: startDate, endDate: endDate))
    }

    func createAmenityReservation(params: AmenityReservationRequest) -> Promise<AmenityReservationResponse> {
        request(.CreateAmenityReservation(params: params))
    }

    func createAndGetAmenityReservation(params: AmenityReservationRequest) -> Promise<AmenityReservation> {
        createAmenityReservation(params: params).then { reservationResponse in
            self.getAmenityReservation(reservationID: reservationResponse.reservationID)
        }
    }

    func createUserRequest(params: UserRequestParams) -> Promise<Void> {
        emptyRequest(.CreateUserRequest(params: params))
    }

    func deleteUserRequest(requestID: String) -> Promise<Void> {
        emptyRequest(.DeleteUserRequest(requestID: requestID))
    }

    func getUserRequestList(statuses: [UserRequest.Status]? = nil, page: Int = 0, per: Int = Page.defaultPerPage) -> Promise<[UserRequest]> {
        request(.GetUserRequestList(statuses: statuses, page: Page(per: per, page: page)))
    }

    func getUserRequest(requestID: String) -> Promise<UserRequest> {
        requestSingle(.GetUserRequest(requestID: requestID))
    }

    func deleteNotification(notificationID: String) -> Promise<Void> {
        emptyRequest(.DeleteNotification(notificationID: notificationID))
    }

    func getConciergeOnDutyList() -> Promise<[User]> {
        #if DEBUG
        return getConciergeList().map { users in
            Array(users.prefix(3))
        }
        #else
        return request(.GetConciergeOnDutyList)
        #endif
    }

    func getTwilioAccessToken() -> Promise<TwilioAccessToken> {
        request(.GetTwilioAccessToken)
    }

    func createTwilioChannel(userID: String? = nil) -> Promise<Void> {
        emptyRequest(.CreateTwilioChannel(userID: userID))
    }

    func joinTwilioChannel() -> Promise<Void> {
        emptyRequest(.JoinTwilioChannel)
    }

    func deleteTwilioChannel() -> Promise<Void> {
        emptyRequest(.DeleteTwilioChannel)
    }

    func getAmenityReservationList(page: Int = 0, per: Int = Page.defaultPerPage, startDate: Date? = nil, endDate: Date? = nil) -> Promise<[AmenityReservation]> {
        request(.GetAmenityReservationList(Page(per: per, page: page), startDate: startDate, endDate: endDate))
    }

    func getAmenityReservation(reservationID: String) -> Promise<AmenityReservation> {
        requestSingle(.GetAmenityReservation(reservationID: reservationID))
    }

    func updateAmenityReservation(reservationID: String, params: AmenityReservationRequest) -> Promise<Void> {
        emptyRequest(.UpdateAmenityReservation(reservationID: reservationID, params: params))
    }

    func deleteAmenityReservation(reservationID: String) -> Promise<Void> {
        emptyRequest(.DeleteAmenityReservation(reservationID: reservationID))
    }

    func getEphemeralKey(apiVersion: String = Configuration.default.aVotreService.stripe.apiVersion) -> Promise<[AnyHashable: Any]> {
        refreshTokenIfNeeded().then {
            API.shared.session.request(ResidentRouter.GetStripeEphemeralKey(stripeApiVersion: apiVersion))
                              .validate()
                              .responseJSON().then { json, response in
                                  Promise<[AnyHashable: Any]> { seal in
                                      guard let json = json as? [AnyHashable: Any] else {
                                          seal.reject(APIError(message: "Cannot decode stripe ephemeral key"))
                                          return
                                      }
                                      seal.fulfill(json)
                                  }
                              }
        }
    }

    func getStripeCustomerDetails() -> Promise<ResidentCustomerDetails> {
        request(.GetStripeCustomerDetails).get { Defaults.stripeCustomerID = $0.customerID }
    }

    func getALaCarteItems(categoryID: String, page: Int = 0, per: Int = Page.defaultPerPage) -> Promise<[CartProductManager.Product]> {
        request(.GetALaCarteItems(categoryID: categoryID, page: Page(per: per, page: page)))
    }

    func getALaCarteCategories() -> Promise<[ALaCarteCategory]> {
        request(.GetALaCarteCategories)
    }

    func createOrder(params: OrderRequestData) -> Promise<OrderClientSecretResponse> {
        request(.CreateOrder(params: params))
    }

    func calculateOrder(params: OrderRequestData) -> Promise<[OrderResponseItem]> {
        request(.CalculateOrder(params: params))
    }

    func createSetupIntent() -> Promise<StripeSetupIntent> {
        request(.CreateSetupIntent)
    }

    func getOrderList(page: Int = 0, per: Int = Page.defaultPerPage) -> Promise<[OrderData]> {
        request(.GetUserOrders(Page(per: per, page: page)))
    }

    func getOrder(orderID: String) -> Promise<OrderData> {
        request(.GetUserOrder(orderID: orderID))
    }

    func getBellNotifications(page: Int = 0, per: Int = Page.defaultPerPage) -> Promise<[BellNotification]> {
        request(.GetBellNotifications(Page(per: per, page: page)))
    }

    func readBellNotification(notificationID: String) -> Promise<Void> {
        emptyRequest(.ReadBellNotification(notificationID: notificationID))
    }

    func readBellNotificationObject(params: BellNotificationReadRequest) -> Promise<Void> {
        emptyRequest(.ReadBellNotificationObject(params))
    }

    func readAllNotifications() -> Promise<Void> {
        emptyRequest(.ReadAllNotifications)
    }

    func getUnreadBellNotificationsCount() -> Promise<UnreadBellNotifications> {
        request(.GetUnreadBellNotificationCount)
    }

    func getLeasingOfficeDetails() -> Promise<LeaseOfficeData> {
        requestSingle(.GetLeasingOffice)
    }

    func getLeasingOfficeTeam(officeID: String) -> Promise<[LeaseOfficeData]> {
        request(.GetLeasingOfficeTeam(officeID: officeID))
    }

    func getSubscriptionList() -> Promise<[Subscription]> {
        request(.GetSubscriptionList)
    }

    func getUserSubscription() -> Promise<UserSubscription?> {
        request(.GetSubscription, decoder: API.shared.secondsDecoder)
    }

    func updateSubscription(params: SubscriptionManageRequest) -> Promise<Void> {
        emptyRequest(.UpdateSubscription(params))
    }

    func nextInvoice(subscriptionID: String) -> Promise<SubscriptionNextInvoice> {
        request(.NextInvoice(subscriptionID: subscriptionID), decoder: API.shared.secondsDecoder)
    }

    func getDocuments(per: Int, page: Int, categoryID: String) -> Promise<[LibraryItem]> {
        request(.GetLibraryItemList(Page(per: per, page: page), type: .document, categoryID: categoryID))
    }

    func getVideos(per: Int, page: Int, categoryID: String) -> Promise<[LibraryItem]> {
        request(.GetLibraryItemList(Page(per: per, page: page), type: .video, categoryID: categoryID))
    }

    func getLibraryItems(per: Int, page: Int, type: LibraryItem.ItemType, categoryID: String) -> Promise<[LibraryItem]> {
        request(.GetLibraryItemList(Page(per: per, page: page), type: type, categoryID: categoryID))
    }

    func getLibraryCategories() -> Promise<[LibraryCategory]> {
        request(.GetLibraryCategories)
    }

    func getRefundInfo(reservationID: String) -> Promise<OrderRefund> {
        request(.GetRefundInfo(reservationID: reservationID))
    }
}
