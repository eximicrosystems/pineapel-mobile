//
// Created by Andrei Stoicescu on 23/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit

class AVSService<Router: Alamofire.URLRequestConvertible> {

    func refreshTokenIfNeeded() -> Promise<Void> {
        guard useRefreshToken else { return .value(()) }

        return Promise<Void> { seal in
            if RefreshTokenManager.shared.shouldRefresh {
                RefreshTokenManager.shared.refresh { result in
                    switch result {
                    case .success:
                        seal.fulfill(())
                    case .failure(let error):
                        seal.reject(error)
                    }
                }
            } else {
                seal.fulfill(())
            }
        }
    }

    var session: Session = AF

    var useRefreshToken = false

    func request<T: Decodable>(_ router: Router, type: T.Type = T.self, decoder: JSONDecoder = JSONDecoder()) -> Promise<T> {
        refreshTokenIfNeeded().then {
            self.session.request(router).validate().responseDecodable(type, decoder: decoder)
        }
    }

    func requestSingle<T: Decodable>(_ router: Router, type: [T].Type = [T].self, decoder: JSONDecoder = JSONDecoder()) -> Promise<T> {
        request(router, type: type, decoder: decoder).firstValue
    }

    func requestSingle<T: Decodable>(_ router: Router, type: [T].Type = [T].self, decoder: JSONDecoder = JSONDecoder(), catchFallback: T) -> Promise<T> {
        Promise<T> { seal in
            request(router, type: type, decoder: decoder).firstValue.done { value in
                seal.fulfill(value)
            }.catch { error in
                Log.thisError(error)
                seal.fulfill(catchFallback)
            }
        }
    }

    // Returns an array by removing any undecodable elements
    func requestSafeArray<T: Decodable>(_ router: Router, type: [T].Type = [T].self, decoder: JSONDecoder = JSONDecoder()) -> Promise<[T]> {
        Promise<[T]> { seal in
            request(router, type: SafeArrayDecodable<T>.self, decoder: decoder).done { value in
                seal.fulfill(value.array)
            }.catch { error in
                Log.thisError(error)
                seal.reject(error)
            }
        }
    }

    func emptyRequest(_ router: Router) -> Promise<Void> {
        refreshTokenIfNeeded().then {
            self.requestWithEmptyResponse(router)
        }
    }

    private func requestWithEmptyResponse(_ router: Router) -> Promise<Void> {
        let responseSerializer = DataResponseSerializer(emptyResponseCodes: [200, 201, 204, 205])
        return Promise<Void> { seal in
            session.request(router)
                   .validate()
                   .response(responseSerializer: responseSerializer) { response in
                       switch response.result {
                       case .success(_):
                           seal.fulfill(())
                       case .failure(_):
                           seal.reject(APIError.from(response))
                       }
                   }
        }
    }
}
