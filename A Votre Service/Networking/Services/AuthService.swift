//
//  AuthService.swift
//  A Votre Service
//
//  Created by Andrei Stoicescu on 02/06/2020.
//  Copyright © 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyUserDefaults
import PromiseKit

class AuthService: AVSService<AuthRouter> {

    override init() {
        super.init()
        useRefreshToken = false
        session = AF
    }

    func login(user: String, password: String) -> Promise<Void> {
        request(.Login(parameters: AuthRequestParams.default(user: user, password: password))).get { (response: RefreshTokenResponse) in
            AppState.refreshTokenCredentials = response.credentials
            AppState.conciergeShift = response.currentShift
            AppState.features = .init(enabledFlags: response.buildingFeatures)
            Log.this("FEATURES")
            Log.this(AppState.features)
            Defaults.userType = response.role
            Defaults.maintenanceRequestUrl = response.maintenanceRequestUrl
            KeychainHelper().user = KeychainHelper.User(email: user, password: password)
        }.then { _ in
            AppState.reloadBuildingTimeZone()
        }
    }

    func refreshToken() -> Promise<RefreshTokenResponse> {
        request(.RefreshToken(parameters: AuthRequestParams.refresh(token: AppState.refreshTokenCredentials?.refreshToken))).get { response in
            AppState.refreshTokenCredentials = response.credentials
            AppState.conciergeShift = response.currentShift
            AppState.features = .init(enabledFlags: response.buildingFeatures)
            Defaults.maintenanceRequestUrl = response.maintenanceRequestUrl
        }
    }

    func requestNewPassword(email: String) -> Promise<Void> {
        emptyRequest(.RequestNewPassword(parameters: AuthPasswordParams(mail: email, code: nil, password: nil)))
    }

    func setNewPassword(email: String, code: String, password: String) -> Promise<Void> {
        emptyRequest(.SetNewPassword(parameters: AuthPasswordParams(mail: email, code: code, password: password)))
    }

    func getMinAppVersion() -> Promise<MinAppVersion> {
        request(.GetMinAppVersion)
    }
}
