//
// Created by Andrei Stoicescu on 25/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit

class ConciergeService: AVSService<ConciergeRouter> {

    override init() {
        super.init()
        useRefreshToken = true
        session = API.shared.session
    }

    func getReservationsCount() -> Promise<CountResponse> {
        requestSingle(.GetReservationsCount, catchFallback: .zero)
    }

    func getRequestsCount() -> Promise<CountResponse> {
        requestSingle(.GetRequestsCount, catchFallback: .zero)
    }

    func getPendingRequestsCount() -> Promise<CountResponse> {
        requestSingle(.GetPendingRequestsCount, catchFallback: .zero)
    }

    func getRunOfShow() -> Promise<[RunOfShowItem]> {
        request(.GetRunOfShow)
    }

    func getAllUsers() -> Promise<[User]> {
        request(.GetUsers(.all))
    }

    func getUsers(page: Int, per: Int) -> Promise<[User]> {
        request(.GetUsers(Page(per: per, page: page)))
    }

    func getUser(userID: String) -> Promise<User> {
        requestSingle(.GetUser(userID))
    }

    func getConciergeList() -> Promise<[User]> {
        request(.GetConcierges(.all))
    }

    func getConciergeStats() -> Promise<ConciergeStats> {
        request(.GetConciergeStats)
    }

    func updateRunOfShow(_ params: RunOfShowRequest) -> Promise<Void> {
        emptyRequest(.UpdateRunOfShow(params))
    }

    func getNotes(page: Int = 0, per: Int = Page.defaultPerPage, id: String? = nil) -> Promise<[ConciergeNote]> {
        request(.GetNotes(Page(per: per, page: page), id: id))
    }

    func createNote(params: ConciergeNoteRequest) -> Promise<Void> {
        emptyRequest(.CreateNote(params))
    }

    func updateNote(params: ConciergeNoteRequest, noteID: String) -> Promise<Void> {
        emptyRequest(.UpdateNote(params, noteID: noteID))
    }

    func deleteNote(noteID: String) -> Promise<Void> {
        emptyRequest(.DeleteNote(noteID: noteID))
    }

    func getAmenityReservationList(page: Int = 0, per: Int = Page.defaultPerPage, startDate: Date? = nil, endDate: Date? = nil, amenityID: String? = nil) -> Promise<[AmenityReservation]> {
        request(.GetAmenityReservations(Page(per: per, page: page), startDate: startDate, endDate: endDate, amenityID: amenityID))
    }

    func updateAmenityReservationStatus(status: AmenityReservation.Status, reservationID: String) -> Promise<Void> {
        emptyRequest(.UpdateAmenityReservationStatus(status, reservationID: reservationID))
    }

    func uploadImage(imageData: Data, progress: @escaping (Progress) -> Void = { _ in }) -> Promise<FileUploadResponse> {
        refreshTokenIfNeeded().then {
            API.shared.session.upload(imageData, with: ConciergeRouter.UploadImage).uploadProgress(closure: progress).validate().responseDecodable()
        }
    }

    func uploadVideo(videoData: Data, progress: @escaping (Progress) -> Void = { _ in }) -> Promise<FileUploadResponse> {
        refreshTokenIfNeeded().then {
            API.shared.session.upload(videoData, with: ConciergeRouter.UploadVideo).uploadProgress(closure: progress).validate().responseDecodable()
        }
    }

    func updateMedia(params: ConciergeMediaRequest) -> Promise<ConciergeMediaResponse> {
        request(.UpdateMedia(params))
    }

    func getTags() -> Promise<[ConciergeTag]> {
        request(.GetTags)
    }

    func createNotification(params: ConciergeNotificationRequest) -> Promise<Void> {
        emptyRequest(.CreateNotification(params))
    }

    func getUserRequests(per: Int, page: Int, status: UserRequest.Status) -> Promise<[UserRequest]> {
        request(.GetUserRequests(Page(per: per, page: page), status: status))
    }

    func updateUserRequest(requestID: String, status: UserRequest.Status) -> Promise<Void> {
        emptyRequest(.UpdateUserRequest(requestID: requestID, status: status))
    }

    func getOrderList(per: Int, page: Int, orderType: OrderData.OrderType) -> Promise<[OrderData]> {
        request(.GetOrderList(Page(per: per, page: page), orderType: orderType))
    }

    func completeOrder(orderID: String) -> Promise<Void> {
        emptyRequest(.CompleteOrder(orderID: orderID))
    }

    func getBuildingList(per: Int, page: Int) -> Promise<[BuildingDetails]> {
        request(.GetBuildingList(Page(per: per, page: page)))
    }

    func getResidentList(userID: String, per: Int, page: Int) -> Promise<[ResidentGuest]> {
        request(.GetResidentList(userID: userID, Page(per: per, page: page)))
    }

    func logResidentGuest(guestID: String, guest: ResidentGuest) -> Promise<Void> {
        emptyRequest(.LogResident(guestID: guestID, guest))
    }

    func getVisitors(per: Int, page: Int) -> Promise<[Visitor]> {
        request(.GetGlobalResidentList(Page(per: per, page: page)))
    }

    func logVisitorRequest(_ params: VisitorRequest) -> Promise<Void> {
        emptyRequest(.LogVisit(params))
    }

    func uploadPackageImage(imageData: Data, progress: @escaping (Progress) -> Void = { _ in }) -> Promise<FileUploadResponse> {
        refreshTokenIfNeeded().then {
            API.shared.session.upload(imageData, with: ConciergeRouter.UploadPackageImage).uploadProgress(closure: progress).validate().responseDecodable()
        }
    }

    func uploadALaCarteImage(imageData: Data, progress: @escaping (Progress) -> Void = { _ in }) -> Promise<FileUploadResponse> {
        refreshTokenIfNeeded().then {
            API.shared.session.upload(imageData, with: ConciergeRouter.UploadALaCarteImage).uploadProgress(closure: progress).validate().responseDecodable()
        }
    }

    func createPackageRequest(_ params: PackageRequest) -> Promise<PackageCreateResponse> {
        request(.CreatePackageRequest(params))
    }

    func notifyPackage(packageID: String) -> Promise<Void> {
        emptyRequest(.NotifyPackage(packageID: packageID))
    }

    func getPackageList(per: Int, page: Int, status: Package.Status) -> Promise<[Package]> {
        request(.GetPackageList(Page(per: per, page: page), status: status))
    }

    func deliverPackage(packageID: String, params: PackageDeliveryRequest) -> Promise<Void> {
        emptyRequest(.DeliverPackage(packageID: packageID, params))
    }

    func getAmenityList(per: Int, page: Int) -> Promise<[Amenity]> {
        request(.GetAmenityList(Page(per: per, page: page)))
    }

    func createAmenityReservation(_ params: ConciergeAmenityReservation) -> Promise<Void> {
        emptyRequest(.CreateAmenityReservation(params))
    }

    func getCurrentShift() -> Promise<ConciergeShift> {
        request(.GetCurrentShift).get { shift in
            AppState.conciergeShift = shift
        }
    }

    func getIncidentReports(per: Int, page: Int) -> Promise<[IncidentReport]> {
        request(.GetIncidentReports(Page(per: per, page: page)))
    }

    func getConciergeNotifications(per: Int, page: Int, buildingID: String) -> Promise<[ConciergeNotification]> {
        request(.GetNotifications(Page(per: per, page: page), buildingID: buildingID))
    }

    func createIncidentReport(_ report: IncidentReportRequest) -> Promise<Void> {
        emptyRequest(.CreateIncidentReport(report))
    }

    func createActivityLog(_ log: ActivityLogRequest) -> Promise<Void> {
        emptyRequest(.CreateActivityLog(log))
    }

    func updateActivityLog(_ log: ActivityLogRequest, activityLogID: String) -> Promise<Void> {
        emptyRequest(.UpdateActivityLog(log, activityLogID: activityLogID))
    }

    func updateIncidentReport(_ report: IncidentReportRequest, incidentReportID: String) -> Promise<Void> {
        emptyRequest(.UpdateIncidentReport(report, incidentReportID: incidentReportID))
    }

    func getActivityLogs(per: Int, page: Int) -> Promise<[ActivityLog]> {
        request(.GetActivityLogs(Page(per: per, page: page)))
    }

    func deleteActivityLog(activityLogID: String) -> Promise<Void> {
        emptyRequest(.DeleteActivityLog(activityLogID))
    }

    func getSubscriptionList(per: Int, page: Int) -> Promise<[ConciergeSubscription]> {
        request(.GetSubscriptionList(Page(per: per, page: page)))
    }

    func getConciergeFeedback(per: Int, page: Int) -> Promise<[ConciergeFeedback]> {
        request(.GetFeedback(Page(per: per, page: page)))
    }

    func getConciergeALaCarteItems(per: Int, page: Int, categoryId: String, buildingId: String) -> Promise<[ConciergeALaCarteProduct]> {
        request(.GetALaCarteItems(categoryID: categoryId, buildingID: buildingId, page: Page(per: per, page: page)))
    }

    func getConciergeALaCarteCategories(buildingId: String? = nil) -> Promise<[ConciergeALaCarteCategory]> {
        request(.GetALaCarteCategories(buildingID: buildingId))
    }

    func updateALaCarteProduct(_ params: ConciergeProductRequest, productId: String) -> Promise<Void> {
        emptyRequest(.UpdateALaCarteProduct(params, productId: productId))
    }

    func createALaCarteProduct(_ params: ConciergeProductRequest) -> Promise<Void> {
        emptyRequest(.CreateALaCarteProduct(params))
    }

    func getReports(per: Int, page: Int, reportType: ConciergeReport.ReportType, otherParams: ReportFilterParams) -> Promise<ConciergeReport> {
        request(.GetReports(Page(per: per, page: page), reportType: reportType, params: otherParams))
    }

    func getPerformanceReports(_ params: ReportFilterParams) -> Promise<ConciergePerformanceReport> {
        request(.GetReports(nil, reportType: .performance, params: params))
    }

    func getContacts(per: Int, page: Int) -> Promise<[User]> {
        request(.GetContacts(.init(per: per, page: page)))
    }

    func getServices(userId: String) -> Promise<[ConciergeServiceModel]> {
        request(.GetServices(userId: userId))
    }

    func getServiceProviders(per: Int, page: Int, buildingId: String, serviceId: String) -> Promise<[ConciergeServiceModel]> {
        request(.GetServiceProviders(.init(per: per, page: page), buildingId: buildingId, serviceId: serviceId))
    }

    func assignRequestProvider(params: RequestProviderAssignParams) -> Promise<Void> {
        emptyRequest(.AssignServiceProvider(params))
    }

    func searchNotes(per: Int, page: Int, term: String) -> Promise<[ConciergeNote]> {
        request(.SearchNotes(.init(per: per, page: page), term: term))
    }

    func searchActivityLogs(per: Int, page: Int, term: String) -> Promise<[ActivityLog]> {
        request(.SearchActivityLogs(.init(per: per, page: page), term: term))
    }

    func getConciergeFAQ(per: Int, page: Int, term: String? = nil) -> Promise<[ConciergeFAQ]> {
        request(.ConciergeFAQ(.init(per: per, page: page), term: term))
    }

    func createConciergeFAQ(params: ConciergeFAQRequest) -> Promise<Void> {
        emptyRequest(.CreateFAQ(params))
    }

    func updateConciergeFAQ(params: ConciergeFAQRequest, faqID: String) -> Promise<Void> {
        emptyRequest(.UpdateFAQ(params, id: faqID))
    }

    func deleteConciergeFAQ(id: String) -> Promise<Void> {
        emptyRequest(.DeleteFAQ(id))
    }

}
