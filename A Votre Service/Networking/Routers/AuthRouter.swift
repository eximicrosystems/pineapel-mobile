//
//  AuthRouter.swift
//  A Votre Service
//
//  Created by Andrei Stoicescu on 02/06/2020.
//  Copyright © 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import Alamofire

enum AuthRouter: URLRequestConvertible {
    case Login(parameters: AuthRequestParams)
    case RefreshToken(parameters: AuthRequestParams)
    case RequestNewPassword(parameters: AuthPasswordParams)
    case SetNewPassword(parameters: AuthPasswordParams)
    case GetMinAppVersion

    var baseURL: URL {
        Configuration.default.aVotreService.apis.main.baseURL
    }

    var method: Alamofire.HTTPMethod {
        switch self {
        case .GetMinAppVersion:
            return .get
        case .Login, .RefreshToken, .RequestNewPassword, .SetNewPassword:
            return .post
        }
    }

    var path: String {
        switch self {
        case .Login, .RefreshToken:
            return "/oauth/token"
        case .RequestNewPassword:
            return "/user/password"
        case .SetNewPassword:
            return "/user/password/reset"
        case .GetMinAppVersion:
            return "/appversion"
        }
    }

    func asURLRequest() throws -> URLRequest {
        var urlRequest = URLRequest(url: baseURL.appendingPathComponent(path))
        urlRequest.method = method

        switch self {
        case .Login(let parameters), .RefreshToken(let parameters):
            urlRequest = try URLEncodedFormParameterEncoder.default.encode(parameters, into: urlRequest)
        case .SetNewPassword(let parameters), .RequestNewPassword(let parameters):
            urlRequest = try URLEncoding.init(destination: .queryString).encode(urlRequest, with: ["_format": "json"])
            urlRequest = try JSONParameterEncoder.default.encode(parameters, into: urlRequest)
        case .GetMinAppVersion:
            urlRequest = try URLEncoding.init(destination: .queryString).encode(urlRequest, with: ["_format": "json"])
        }

        return urlRequest
    }
}
