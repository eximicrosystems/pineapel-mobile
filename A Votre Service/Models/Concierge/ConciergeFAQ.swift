//
// Created by Andrei Stoicescu on 10.03.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import DifferenceKit

extension ConciergeFAQ: Codable {
    enum CodingKeys: String, CodingKey {
        case id, name
        case body = "field_body"
        case createdAt = "created"
        case buildingId = "field_building"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        id = try map.decode(.id)
        name = try? map.decodeIfPresent(.name)
        body = try? map.decodeIfPresent(.body)
        createdAt = map.decodeDateIfPresent(.createdAt)
        buildingId = try map.decode(.buildingId)
    }
}

struct ConciergeFAQ: Identifiable, Hashable, Differentiable {
    var id: String
    var buildingId: String
    var body: String?
    var name: String?
    var createdAt: Date?

    func hash(into hasher: inout Hasher) { hasher.combine(id) }
    var differenceIdentifier: Int { hashValue }
}

struct ConciergeFAQRequest: Encodable {
    let id: String?
    let body: String
    let name: String
    let buildingID: String

    enum CodingKeys: String, CodingKey {
        case id
        case body = "field_body"
        case name = "name"
        case buildingID = "field_building"
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encodeValueCodableItemArrayIfPresent(id, forKey: .id)
        try? container.encodeItemArray(["value": body, "format": "basic_html"], forKey: .body)
        try? container.encodeValueCodableItemArrayIfPresent(name, forKey: .name)
        try? container.encodeSingleItemArrayIfPresent(buildingID, paramName: "target_id", forKey: .buildingID)
    }
}