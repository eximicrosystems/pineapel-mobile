//
// Created by Andrei Stoicescu on 28/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import DifferenceKit

struct ConciergeNote: Hashable, Differentiable, Identifiable {
    var id: String { noteID }
    let noteID: String
    let note: String?
    let name: String?
    let created: String
    let userID: String
    let firstName: String?
    let lastName: String?

    func hash(into hasher: inout Hasher) { hasher.combine(noteID) }
    var differenceIdentifier: Int { hashValue }
}

extension ConciergeNote: Codable {
    enum CodingKeys: String, CodingKey {
        case noteID = "id"
        case note = "field_note"
        case name
        case created
        case userID = "user_id"
        case firstName = "field_first_name"
        case lastName = "field_last_name"
    }
}

struct ConciergeNoteRequest: Encodable {
    let id: String?
    let note: String
    let name: String
    let residentId: String?

    enum CodingKeys: String, CodingKey {
        case id
        case note = "field_note"
        case name
        case residentId = "field_resident"
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try container.encodeValueCodableItemArrayIfPresent(id, forKey: .id)
        try container.encodeValueCodableItemArray(note, forKey: .note)
        try container.encodeValueCodableItemArray(name, forKey: .name)
        try container.encodeSingleItemArrayIfPresent(residentId, paramName: "target_id", forKey: .residentId)
    }
}