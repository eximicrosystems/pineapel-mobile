//
// Created by Andrei Stoicescu on 27/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation

struct ConciergeStats: Codable {
    let buildingID: String
    let buildingName: String
    let residents: Int
}

extension ConciergeStats {
    enum CodingKeys: String, CodingKey {
        case buildingID = "building_id"
        case buildingName = "building_name"
        case residents = "residents"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        buildingID = try map.decode(.buildingID)
        buildingName = try map.decode(.buildingName)
        residents = try map.decodeLossless(.residents)
    }
}