//
// Created by Andrei Stoicescu on 05.03.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//

import Foundation

struct ConciergeServiceModel: Codable, Identifiable, Hashable {
    var id: String
    var buildingId: String
    var categoryId: String
    var categoryName: String
    var firstName: String
    var lastName: String
    var phoneNumber: String
    var mail: String
    var serviceId: String
    var serviceName: String
    var userId: String

    enum CodingKeys: String, CodingKey {
        case id
        case buildingId = "building_id"
        case categoryId = "field_category"
        case categoryName = "field_category_name"
        case firstName = "field_first_name"
        case lastName = "field_last_name"
        case phoneNumber = "field_phone_number"
        case mail
        case serviceId = "service_id"
        case serviceName = "service_name"
        case userId = "uid"
    }

    var fullName: String {
        [firstName, lastName].joined(separator: " ")
    }
}