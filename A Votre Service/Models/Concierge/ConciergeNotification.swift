//
// Created by Andrei Stoicescu on 11/11/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation

struct ConciergeNotification: Codable, Identifiable, Hashable {
    var id: String { notification.notificationID }
    let scheduleNotification: Date?
    let notification: FeedNotificationItem
    let buildingID: String
    let recipientIDs: [String]

    enum CodingKeys: String, CodingKey {
        case scheduleNotification = "field_schedule_notification"
        case notification
        case buildingID = "building"
        case recipientIDs = "field_recipient"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        scheduleNotification = map.decodeDateIfPresent(.scheduleNotification)
        notification = try FeedNotificationItem(from: decoder)
        buildingID = try map.decode(.buildingID)

        let recipientIDsArray = try? map.decodeIfPresent(.recipientIDs, as: [String].self)
        let recipientIDString = try? map.decodeIfPresent(.recipientIDs, as: String.self)
        if let recipientIDsArray = recipientIDsArray {
            recipientIDs = recipientIDsArray
        } else if let recipientIDString = recipientIDString {
            recipientIDs = [recipientIDString]
        } else {
            recipientIDs = []
        }
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try? container.encodeIfPresent(scheduleNotification, forKey: .scheduleNotification)
        try container.encode(buildingID, forKey: .buildingID)
        try container.encode(recipientIDs, forKey: .recipientIDs)

        try notification.encode(to: encoder)
    }
}