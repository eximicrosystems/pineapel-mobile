//
// Created by Andrei Stoicescu on 07/10/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation

struct ConciergeShift: Decodable {
    let buildingID: String
    let conciergeAdmin: Bool

    enum CodingKeys: String, CodingKey {
        case buildingID = "building"
        case conciergeAdmin = "concierge_admin"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        buildingID = try map.decode(.buildingID)
        conciergeAdmin = (try? map.decodeLosslessIfPresent(.conciergeAdmin)) ?? false
    }
}