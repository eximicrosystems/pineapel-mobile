//
// Created by Andrei Stoicescu on 02/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation

struct ConciergeMediaRequest: Encodable {
    let name: String
    let bundle: String
    let status: Int = 1
    var imageTargetID: String? = nil
    var videoTargetID: String? = nil

    static func video(targetID: String) -> ConciergeMediaRequest {
        ConciergeMediaRequest(name: "media.mp4", bundle: "video", videoTargetID: targetID)
    }

    static func image(targetID: String) -> ConciergeMediaRequest {
        ConciergeMediaRequest(name: "media.jpg", bundle: "image", imageTargetID: targetID)
    }

    enum CodingKeys: String, CodingKey {
        case name
        case bundle
        case status
        case imageTargetID = "field_media_image"
        case videoTargetID = "field_media_video_file"
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeValueCodableItemArray(name, forKey: .name)
        try container.encode(bundle, forKey: .bundle)
        try container.encodeValueCodableItemArray(status, forKey: .status)
        try container.encodeSingleItemArrayIfPresent(imageTargetID, paramName: "target_id", forKey: .imageTargetID)
        try container.encodeSingleItemArrayIfPresent(videoTargetID, paramName: "target_id", forKey: .videoTargetID)
    }
}

struct ConciergeMediaResponse: Decodable {
    let mediaID: String

    enum CodingKeys: String, CodingKey {
        case mediaID = "mid"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        let value: ValueCodable<Int> = try map.decodeSingleItem(.mediaID)
        self.mediaID = String(value.value)
    }
}