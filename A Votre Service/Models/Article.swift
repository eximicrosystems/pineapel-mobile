//
// Created by Andrei Stoicescu on 10/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import DifferenceKit

protocol ArticleProtocol {
    var articleThumbnailURL: URL? { get }
    var articleBody: String? { get }
    var articleTitle: String? { get }
}

struct TextArticle: Codable, Hashable, Differentiable {
    var title: String?
    let body: String?
    var differenceIdentifier: Int { hashValue }
}

struct Article: Codable, ArticleProtocol, Hashable, Identifiable, Differentiable {
    enum DisplayMode: String, Codable {
        // Display mode 1: Full width article, full width image
        // Display mode 2: Full width article, Half width image
        // Display mode 3: Half width article
        case mode1 = "1"
        case mode2 = "2"
        case mode3 = "3"
    }

    var id: String { articleNodeID }

    var title: String?
    let body: String?
    let imageURL: URL?
    let teaser: String?
    let articleNodeID: String
    var displayMode: DisplayMode

    enum CodingKeys: String, CodingKey {
        case title
        case body
        case imageURL = "field_image"
        case teaser = "field_teaser"
        case articleNodeID = "nid"
        case displayMode = "field_display_mode"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        self.title = try? map.decodeIfPresent(.title)
        self.body = try? map.decodeIfPresent(.body)
        self.imageURL = try? map.decodeIfPresent(.imageURL)
        self.teaser = try? map.decodeIfPresent(.teaser)
        let articleNodeID: String? = try? map.decodeIfPresent(.articleNodeID)
        self.articleNodeID = articleNodeID ?? UUID().uuidString
        self.displayMode = (try? map.decodeIfPresent(.displayMode)) ?? .mode1
    }

    // MARK: - ArticleProtocol
    var articleThumbnailURL: URL? {
        imageURL
    }

    var articleBody: String? {
        body
    }

    var articleTitle: String? {
        title
    }

    var articleNodeURL: URL {
        Article.articleNodeURL(id: articleNodeID)
    }

    static func articleNodeURL(id: String) -> URL {
        var url = Configuration.default.aVotreService.apis.main.baseURL
        url.appendPathComponent("node")
        url.appendPathComponent(id)
        return url
    }

    func hash(into hasher: inout Hasher) { hasher.combine(articleNodeID) }
    var differenceIdentifier: Int { hashValue }
}