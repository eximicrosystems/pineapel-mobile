//
// Created by Andrei Stoicescu on 10/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import DifferenceKit

struct AmenityHoursInterval: Codable, Hashable {
    let start: String?
    let end: String?
}

struct AmenityOpenHours: Codable, Hashable {
    let label: String?
    let text: String?
    let slots: [AmenityHoursInterval]?
}

struct Amenity: ArticleProtocol, Identifiable {
    var id: String { amenityID }

    enum Price: String, Codable, Hashable {
        case flat
        case slot
        case day
    }

    let amenityID: String
    let name: String?
    let body: String?
    let images: [URL]?
    let slotDuration: Int?
    let price: Float?
    let requiresReservation: Bool
    let teaser: String?
    let openHours: [AmenityOpenHours]?
    let priceType: Price?
    let maxSlots: Int
    let maxPeople: Int
    let taxPercent: Float
    let requiresTerms: Bool
    let termsBody: String?
    let additionalFeeName: String?
    let additionalFeeAmount: Float?
    let cancellationPolicy: String?
    var upsellCategories = [ALaCarteCategory]()

    // MARK: - ArticleProtocol
    var articleThumbnailURL: URL? {
        images?.first
    }

    var articleBody: String? {
        body
    }

    var articleTitle: String? {
        name
    }

    var isFree: Bool {
        if let price = price {
            return price == Float(0)
        }
        return true
    }

    var openHoursStrings: [String] {
        // {label: "Monday: ", slots: [{start: "0800", end: "2300"}], text: "08:00 - 23:00"}
        guard let hours = openHours?.filter({ $0.text != nil }) else { return [] }
        let dict = Dictionary(grouping: hours) { $0.text! }
        return dict.map { textString, hours in
            let array = hours.count != 1 ? [hours.first?.label, hours.last?.label] : [hours.first?.label]
            let str = array.compactMap({ $0 }).map({ $0.trimmingCharacters(in: CharacterSet(arrayLiteral: ":", " ")) }).joined(separator: " - ")
            return "\(str): \(textString)"
        }
    }

    static var `default`: Amenity {
        Amenity(amenityID: UUID().uuidString,
                name: "Sample Amenity",
                body: "Some body",
                images: nil,
                slotDuration: 1,
                price: 10,
                requiresReservation: true,
                teaser: "Some teaser",
                openHours: nil,
                priceType: .slot,
                maxSlots: 5,
                maxPeople: 5,
                taxPercent: 5,
                requiresTerms: true,
                termsBody: "Terms body",
                additionalFeeName: "Cleaning fee",
                additionalFeeAmount: 10,
                cancellationPolicy: "Cancellation text")
    }
}

extension Amenity: Codable {
    enum CodingKeys: String, CodingKey {
        case amenityID = "id"
        case name
        case body = "field_body"
        case images = "field_images"
        case slotDuration = "field_slot_duration"
        case teaser = "field_teaser"
        case openHours = "field_open_hours"
        case price = "field_price"
        case requiresReservation = "field_requires_reservation"
        case priceType = "field_price_type"
        case maxSlots = "field_max_slots_per_user"
        case maxPeople = "field_slots"
        case taxPercent = "field_tax_percent"
        case requiresTerms = "field_require_terms_acceptance"
        case termsBody = "field_terms_of_use"
        case additionalFeeName = "field_additional_fee_name"
        case additionalFeeAmount = "field_additional_fee_amount"
        case cancellationPolicy = "field_cancellation_policy"
        case upsellCategories = "field_upsell_categories"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        amenityID = try map.decode(.amenityID)
        name = try? map.decode(.name)
        body = try? map.decode(.body)
        images = try? map.decodeIfPresent([Safe<URL>].self, forKey: .images)?.compactMap { $0.value }
        slotDuration = try? map.decodeLosslessIfPresent(.slotDuration)
        price = try? map.decodeLossless(.price)
        requiresReservation = (try? map.decodeLossless(.requiresReservation)) ?? false
        teaser = try? map.decode(.teaser)
        openHours = try? map.decodeIfPresent([Safe<AmenityOpenHours>].self, forKey: .openHours)?.compactMap { $0.value }
        priceType = try? map.decodeIfPresent(.priceType)
        maxSlots = (try? map.decodeLosslessIfPresent(.maxSlots)) ?? 0
        maxPeople = (try? map.decodeLosslessIfPresent(.maxPeople)) ?? 0
        taxPercent = (try? map.decodeLosslessIfPresent(.taxPercent)) ?? 0
        requiresTerms = (try? map.decodeLosslessIfPresent(.requiresTerms)) ?? false
        termsBody = try? map.decodeIfPresent(.termsBody)
        additionalFeeAmount = try? map.decodeLosslessIfPresent(.additionalFeeAmount)
        additionalFeeName = try? map.decodeIfPresent(.additionalFeeName)
        cancellationPolicy = try? map.decodeIfPresent(.cancellationPolicy)
        upsellCategories = (try? map.decodeIfPresent(.upsellCategories)) ?? []
    }
}

extension Amenity: Hashable, Differentiable {
    func hash(into hasher: inout Hasher) { hasher.combine(amenityID) }
    var differenceIdentifier: Int { hashValue }
}