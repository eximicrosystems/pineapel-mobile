//
// Created by Andrei Stoicescu on 22/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit

struct UserRequestParams: Encodable {
    let dueDate: Date?
    let body: String?
    let name: String?
    let type: UserRequest.RequestType?
    let notificationID: String?

    enum CodingKeys: String, CodingKey {
        case dueDate = "field_due_date"
        case body = "field_body"
        case name
        case type
        case notificationID = "field_notification"
    }

    static func genericRequest(notificationID: String) -> UserRequestParams {
        .init(dueDate: Date(), body: "Please deliver received package(s) to my apartment.", name: "Package delivery request", type: .generic, notificationID: notificationID)
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        if let dueDate = dueDate {
            try container.encodeValueCodableItemArray(String(Int(dueDate.timeIntervalSince1970)), forKey: .dueDate)
        }

        try container.encodeValueCodableItemArrayIfPresent(body, forKey: .body)
        try container.encodeValueCodableItemArrayIfPresent(name, forKey: .name)
        try container.encodeIfPresent(type, forKey: .type)
        try container.encodeSingleItemArrayIfPresent(notificationID, paramName: "target_id", forKey: .notificationID)
    }
}

struct UserRequest: Identifiable, Hashable {
    enum RequestType: String, Codable {
        case generic
    }

    enum Status: String, Codable {
        // These are the actual values to send to the server
        case done = "Done"
        case accepted = "Accepted"
        case pending = "Pending"

        var displayString: String {
            switch self {
            case .done:
                return L10n.UserRequest.Status.done
            case .accepted:
                return L10n.UserRequest.Status.accepted
            case .pending:
                return L10n.UserRequest.Status.pending
            }
        }
    }

    var id: String { userRequestID }

    var dueDate: Date?
    var body: String?
    var name: String?
    var type: RequestType?
    var status: Status?
    var notificationID: String?
    var userRequestID: String
    var createdAt: String?
    var firstName: String?
    var lastName: String?
    var userID: String?
    var userPictureURL: URL?
    var apartmentNo: String?
    var categoryID: String?
    var orderID: String?
    var serviceID: String?
    var serviceProviderName: String?
    var serviceFulfillmentTime: Date?
    var serviceProviderImageURL: URL?

    var fullName: String {
        [firstName, lastName].compactMap { $0 }.joined(separator: " ")
    }

    var serviceProviderNameSplit: [String] {
        serviceProviderName?.components(separatedBy: " ") ?? []
    }
}

extension UserRequest: Codable {
    enum CodingKeys: String, CodingKey {
        case dueDate = "field_due_date"
        case body = "field_body"
        case name
        case type
        case createdAt = "created"
        case status = "field_status"
        case notificationID = "field_notification"
        case userRequestID = "id"
        case firstName = "field_first_name"
        case lastName = "field_last_name"
        case userID = "uid"
        case userPictureURL = "user_picture"
        case apartmentNo = "field_apartment_nr"
        case categoryID = "field_item_category"
        case orderID = "field_order_id"
        case serviceID = "field_service_id"
        case serviceProviderName = "field_service_provider_name"
        case serviceFulfillmentTime = "field_fulfilment_time"
        case serviceProviderImageURL = "field_service_provider_picture"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        userRequestID = try map.decode(.userRequestID)

        dueDate = map.decodeDateIfPresent(.dueDate)
        body = try? map.decodeIfPresent(.body)
        name = try? map.decodeIfPresent(.name)
        type = try? map.decodeIfPresent(.type)
        status = try? map.decodeIfPresent(.status)
        notificationID = try? map.decodeIfPresent(.notificationID)
        createdAt = try? map.decodeIfPresent(.createdAt)
        firstName = try? map.decodeIfPresent(.firstName)
        lastName = try? map.decodeIfPresent(.lastName)
        userID = try? map.decodeIfPresent(.userID)
        userPictureURL = try? map.decodeIfPresent(.userPictureURL)
        apartmentNo = try? map.decodeIfPresent(.apartmentNo)
        categoryID = try? map.decodeIfPresent(.categoryID)
        orderID = try? map.decodeIfPresent(.orderID)
        serviceID = try? map.decodeIfPresent(.serviceID)
        serviceProviderName = try? map.decodeIfPresent(.serviceProviderName)
        serviceFulfillmentTime = map.decodeDateIfPresent(.serviceFulfillmentTime)
        serviceProviderImageURL = try? map.decodeIfPresent(.serviceProviderImageURL)
    }

}

extension UserRequest.Status {
    var color: UIColor {
        switch self {
        case .done:
            return AVSColor.colorGreen.color
        case .accepted:
            return AVSColor.colorOrange.color
        case .pending:
            return AVSColor.colorYellow.color
        }
    }
}