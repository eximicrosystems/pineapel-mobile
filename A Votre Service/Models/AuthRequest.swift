//
//  AuthRequest.swift
//  A Votre Service
//
//  Created by Andrei Stoicescu on 02/06/2020.
//  Copyright © 2020 A Votre Service LLC. All rights reserved.
//

import Foundation

struct AuthRequestParams: Encodable {
    let username: String?
    let password: String?
    let clientId: String?
    let clientSecret: String?
    let scope: String?
    let grantType: String?

    enum CodingKeys: String, CodingKey {
        case username
        case clientId = "token_type"
        case clientSecret = "access_token"
        case password
        case scope
        case grantType = "grant_type"
    }
}
