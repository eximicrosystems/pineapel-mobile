//
// Created by Andrei Stoicescu on 16/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation

struct Feedback: Codable {

    enum FeedbackType: String, Codable, CaseIterable {
        case building = "building"
        case concierge = "concierge"
        case other = "other"
    }

    let feedbackID: String?
    let feedbackType: FeedbackType?
    let rating: String?
    let body: String?
    let apartment: String?
    let firstName: String?
    let lastName: String?
    let userPicture: URL?
    let userID: String?
    let dueDate: Date?
    let conciergeID: String?

    enum CodingKeys: String, CodingKey {
        case feedbackID = "id"
        case feedbackType = "type"
        case rating = "field_rating"
        case body = "field_body"
        case apartment = "field_apartment_nr"
        case firstName = "field_first_name"
        case lastName = "field_last_name"
        case userPicture = "field_user_picture"
        case userID = "uid"
        case dueDate = "field_due_date"
        case conciergeID = "field_concierge"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        self.feedbackID = try? map.decodeIfPresent(.feedbackID)
        self.feedbackType = try? map.decodeIfPresent(.feedbackType)
        self.rating = try? map.decodeIfPresent(.rating)
        self.body = try? map.decodeIfPresent(.body)
        self.apartment = try? map.decodeIfPresent(.apartment)
        self.firstName = try? map.decodeIfPresent(.firstName)
        self.lastName = try? map.decodeIfPresent(.lastName)
        self.userPicture = try? map.decodeIfPresent(.userPicture)
        self.userID = try? map.decodeIfPresent(.userID)
        self.dueDate = try? map.decodeIfPresent(.dueDate)
        self.conciergeID = try? map.decodeIfPresent(.conciergeID)
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        if let rating = rating {
            var arrayContainer = container.nestedUnkeyedContainer(forKey: .rating)
            try arrayContainer.encode(["rating": rating])
        }

        if let body = body {
            var arrayContainer = container.nestedUnkeyedContainer(forKey: .body)
            try arrayContainer.encode(ValueCodable(value: body))
        }

        if let feedbackType = feedbackType {
            try container.encode(feedbackType, forKey: .feedbackType)
        }

        if let conciergeID = conciergeID {
            var arrayContainer = container.nestedUnkeyedContainer(forKey: .conciergeID)
            try arrayContainer.encode(["target_id": conciergeID])
        }
    }

    init(feedbackType: FeedbackType?, body: String?, rating: String?, conciergeID: String?) {
        self.feedbackType = feedbackType
        self.body = body
        self.rating = rating
        self.conciergeID = conciergeID

        self.feedbackID = nil
        self.apartment = nil
        self.firstName = nil
        self.lastName = nil
        self.userPicture = nil
        self.userID = nil
        self.dueDate = nil
    }
}