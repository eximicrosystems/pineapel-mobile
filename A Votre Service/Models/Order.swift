//
// Created by Andrei Stoicescu on 13/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit
import DifferenceKit

struct OrderRequestItem: Encodable {
    enum ItemType: String, Codable {
        case amenity = "avs_amenity"
        case aLaCarte = "avs_alacarte"
        case discountCode = "discount_code"
    }

    let type: ItemType
    let id: String?
    let quantity: String?
    let dueTimestamp: Date?
    let reservationID: String?
    let startTime: Date?
    let endTime: Date?
    let startDate: Date?
    let endDate: Date?

    enum CodingKeys: String, CodingKey {
        case type = "item_type"
        case id = "item_id"
        case quantity = "quantity"
        case dueTimestamp = "due_timestamp"
        case reservationID = "reservation_id"
        case startTime = "field_start_time"
        case endTime = "field_end_time"
        case startDate = "field_start_date"
        case endDate = "field_end_date"
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encodeIfPresent(reservationID, forKey: .reservationID)

        if let due = dueTimestamp {
            try? container.encode(Int(due.timeIntervalSince1970), forKey: .dueTimestamp)
        }

        try? container.encodeIfPresent(id, forKey: .id)
        try? container.encodeIfPresent(quantity, forKey: .quantity)
        try? container.encode(type, forKey: .type)

        if let startTime = startTime {
            try container.encode(reservationHourKey(date: startTime), forKey: .startTime)
        }

        if let endTime = endTime {
            try container.encode(reservationHourKey(date: endTime), forKey: .endTime)
        }

        if let startDate = startDate {
            try container.encode(reservationDateKey(date: startDate), forKey: .startDate)
        }

        if let endDate = endDate {
            try container.encode(reservationDateKey(date: endDate), forKey: .endDate)
        }
    }

    func reservationHourKey(date: Date) -> String {
        // Amenities use dates in current timezone
        // AlaCarte uses dates in the building's timezone
        let calendar: Calendar

        switch type {
        case .amenity, .discountCode:
            calendar = .current
        case .aLaCarte:
            calendar = .buildingTimeZoneCalendar
        }

        let hour = calendar.component(.hour, from: date)
        let minute = calendar.component(.minute, from: date)
        return String(hour * 60 + minute)
    }

    func reservationDateKey(date: Date) -> String {
        let timeZone: TimeZone
        switch type {
        case .amenity, .discountCode:
            timeZone = .current
        case .aLaCarte:
            timeZone = AppState.buildingTimeZone
        }

        return DateFormatter(format: "yyyy-MM-dd", timeZone: timeZone).string(from: date)
    }

    static func amenity(id: String, reservationID: String?) -> Self {
        OrderRequestItem(type: .amenity,
                         id: id,
                         quantity: nil,
                         dueTimestamp: nil,
                         reservationID: reservationID,
                         startTime: nil,
                         endTime: nil,
                         startDate: nil,
                         endDate: nil)
    }
}

extension CartProduct {
    var orderRequestItem: OrderRequestItem {
        if product.schedulable {
            return OrderRequestItem(type: .aLaCarte, id: product.productID, quantity: "1", dueTimestamp: scheduledAt, reservationID: nil, startTime: nil, endTime: nil, startDate: nil, endDate: nil)
        } else {
            return OrderRequestItem(type: .aLaCarte, id: product.productID, quantity: String(quantity), dueTimestamp: nil, reservationID: nil, startTime: nil, endTime: nil, startDate: nil, endDate: nil)
        }
    }
}

struct OrderRequestData: Encodable {
    var paymentMethodID: String?
    var items: [OrderRequestItem]
    var discountCodes: [String]?

    enum CodingKeys: String, CodingKey {
        case paymentMethodID = "payment_method"
        case items = "line_items"
        case discountCodes = "discount_codes"
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try? container.encodeIfPresent(paymentMethodID, forKey: .paymentMethodID)
        try container.encode(items, forKey: .items)
        if let codes = discountCodes, codes.count > 0 {
            try container.encode(codes, forKey: .discountCodes)
        }
    }
}

struct OrderClientSecretResponse: Decodable {
    let clientSecret: String

    enum CodingKeys: String, CodingKey {
        case clientSecret = "client_secret"
    }
}

struct OrderResponseItem: Codable, Hashable, Identifiable {
    var id: String { itemID }
    let itemType: OrderRequestItem.ItemType?
    let itemID: String
    let name: String
    let quantity: Int
    let totalPriceWithoutTax: Float
    let totalPriceWithTax: Float
    let taxAmount: Float
    let unitPrice: Float
    let imageUrl: URL?
    let discountCode: String?

    var nameWithQuantity: String {
        if itemType == .discountCode { return name }
        return "\(quantity) × \(name)"
    }

    enum CodingKeys: String, CodingKey {
        case itemType = "item_type"
        case itemID = "item_id"
        case name
        case quantity
        case totalPriceWithoutTax = "total_price_without_tax"
        case totalPriceWithTax = "total_price_with_tax"
        case taxAmount = "tax_amount"
        case unitPrice = "unit_price"
        case imageUrl = "image"
        case discountCode = "discount_code"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)

        itemType = try? map.decodeIfPresent(.itemType)
        itemID = try map.decode(.itemID)
        name = try map.decode(.name)
        quantity = (try? map.decodeLossless(.quantity)) ?? 0
        totalPriceWithoutTax = try map.decodeLossless(.totalPriceWithoutTax)
        totalPriceWithTax = try map.decodeLossless(.totalPriceWithTax)
        taxAmount = try map.decodeLossless(.taxAmount)
        unitPrice = try map.decodeLossless(.unitPrice)
        imageUrl = try? map.decodeIfPresent(.imageUrl)
        discountCode = try? map.decodeIfPresent(.discountCode)
    }
}

struct ResidentCustomerDetails: Decodable {
    let customerID: String

    enum CodingKeys: String, CodingKey {
        case customerID = "customer_id"
    }
}

struct StripeSetupIntent: Decodable {
    let clientSecret: String

    enum CodingKeys: String, CodingKey {
        case clientSecret = "client_secret"
    }
}

struct ALaCarteCategory: Codable {

    enum IconTag: String, Codable {
        case carwash
        case dogwalk
        case gourmetcart
        case helpinghand
        case maidservice
        case privatechef
    }
    
    enum ImageTag: String, Codable {
        case carwash
        case dogwalk
        case gourmetcart
        case helpinghand
        case maidservice
        case privatechef
    }

    let categoryID: String
    let name: String?
    let imageURL: URL?
    let children: [ALaCarteCategory]
    let iconTag: IconTag?
    let imageTag: ImageTag?

    enum CodingKeys: String, CodingKey {
        case categoryID = "tid"
        case name = "name"
        case imageURL = "field_image"
        case children
        case iconTag = "field_icon_tag"
        case imageTag = "field_image_tag"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        categoryID = try map.decode(.categoryID)
        name = try? map.decodeIfPresent(.name)
        imageURL = try? map.decodeIfPresent(.imageURL)
        children = (try? map.decodeIfPresent(.children)) ?? []
        iconTag = try? map.decodeIfPresent(.iconTag)
        imageTag = try? map.decodeIfPresent(.imageTag)
    }
}

extension ALaCarteCategory {
    var icon: UIImage? {
        guard let tag = iconTag else { return nil }
        switch tag {
        case .carwash:
            return Asset.Images.icCarSmall.image
        case .dogwalk:
            return Asset.Images.icDogSmall.image
        case .gourmetcart:
            return Asset.Images.icCartSmall2.image
        case .helpinghand:
            return Asset.Images.icWrenchSmall.image
        case .maidservice:
            return Asset.Images.icCleaningSmall.image
        case .privatechef:
            return Asset.Images.icChefSmall.image
        }
    }
}

extension ALaCarteCategory{
    var image: UIImage?{
        guard let tag = imageTag else { return nil }
        switch tag {
        case .carwash:
            return UIImage(named: "CAR-WASH-ICON-01")
        case .dogwalk:
            return UIImage(named: "DOG-WALK-ICON-01")
        case .gourmetcart:
            return UIImage(named: "GOURMET-CART-ICON-01")
        case .helpinghand:
            return UIImage(named: "HELPING-HAND-ICON-01")
        case .maidservice:
            return UIImage(named: "MAID-SERVICE-ICON-01")
        case .privatechef:
            return UIImage(named: "PRIVATE-CHEF-ICON-01")
        }
    }
}

struct OrderData: Codable, Identifiable, Hashable, Differentiable {
    enum Status: String, Codable {
        case newOrder = "0"
        case paidOrder = "1"
        case paymentError = "2"
    }

    enum OrderType: String, Codable {
        case pending = "0"
        case done = "1"

        var displayString: String {
            switch self {
            case .pending:
                return "New"
            case .done:
                return "Past"
            }
        }
    }

    var id: String { orderID }

    let orderID: String
    let userID: String?
    let status: Status
    let amountWithoutTax: Float
    let amountWithTax: Float
    let taxAmount: Float
    let createdAt: Date?
    let orderItems: [OrderResponseItem]
    let authorName: String?
    var orderType: OrderType
    var userPictureURL: URL?

    enum CodingKeys: String, CodingKey {
        case orderID = "id"
        case userID = "uid"
        case status
        case amountWithoutTax = "amount_without_tax"
        case amountWithTax = "amount_with_tax"
        case taxAmount = "tax_amount"
        case createdAt = "created"
        case orderItems = "line_items"
        case authorName = "author_name"
        case userPictureURL = "user_picture"
        case orderType = "completion_status"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        orderID = try map.decode(.orderID)
        userID = try? map.decode(.userID)
        status = try map.decode(.status)
        amountWithoutTax = try map.decodeLossless(.amountWithoutTax)
        amountWithTax = try map.decodeLossless(.amountWithTax)
        taxAmount = try map.decodeLossless(.taxAmount)
        createdAt = map.decodeDateIfPresent(.createdAt)
        orderItems = try map.decode(.orderItems)
        authorName = try? map.decode(.authorName)
        userPictureURL = try? map.decodeIfPresent(.userPictureURL)
        orderType = try map.decode(.orderType)
    }

    func hash(into hasher: inout Hasher) { hasher.combine(orderID) }

    var differenceIdentifier: Int { hashValue }
}

struct OrderRefund: Decodable {
    struct Item: Decodable {
        let name: String
        let amount: Float

        enum CodingKeys: String, CodingKey {
            case name
            case amount
        }

        init(from decoder: Decoder) throws {
            let map = try decoder.container(keyedBy: CodingKeys.self)
            name = try map.decode(.name)
            amount = try map.decodeLossless(.amount)
        }
    }

    let amount: Float
    let items: [Item]

    enum CodingKeys: String, CodingKey {
        case amount
        case items = "line_items"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        amount = try map.decodeLossless(.amount)
        items = try map.decode(.items)
    }
}

extension ALaCarteCategory: Hashable, Differentiable {
    static func ==(lhs: ALaCarteCategory, rhs: ALaCarteCategory) -> Bool {
        lhs.categoryID == rhs.categoryID
    }

    func hash(into hasher: inout Hasher) { hasher.combine(categoryID) }

    var differenceIdentifier: Int { hashValue }
}
