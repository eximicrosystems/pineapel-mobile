//
// Created by Andrei Stoicescu on 27/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation

struct Subscription: Hashable, Identifiable {
    var id: String { subscriptionID }
    let subscriptionID: String
    let name: String
    let description: String?
    let price: Float
    let taxPercent: Float
    let image: URL?
    let recommended: Bool
}

extension Subscription: Codable {
    enum CodingKeys: String, CodingKey {
        case subscriptionID = "id"
        case name
        case description = "field_description"
        case price = "field_price"
        case taxPercent = "field_tax_percent"
        case image = "field_image"
        case recommended = "field_recommended"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)

        subscriptionID = try map.decode(.subscriptionID)
        name = try map.decode(.name)
        description = try? map.decodeIfPresent(.description)
        price = try map.decodeLossless(.price)
        taxPercent = try map.decodeLossless(.taxPercent)
        image = try? map.decodeIfPresent(.image)
        recommended = (try? map.decodeLosslessIfPresent(.recommended)) ?? false
    }
}

struct UserSubscription: Hashable, Identifiable {
    enum Status: String, Codable {
        case active
        case pastDue = "past_due"
        case incomplete
        case incompleteExpired = "incomplete_expired"
        case trialing
        case canceled
        case unpaid
    }

    var id: String { subscriptionID }

    let subscriptionID: String
    let name: String
    let description: String?
    let priceWithTax: Float
    let startTime: Date
    let endTime: Date
    let status: Status?
    let canceled: Bool
}

extension UserSubscription: Codable {
    enum CodingKeys: String, CodingKey {
        case subscriptionID = "id"
        case name
        case description = "field_description"
        case priceWithTax = "field_price_with_tax"
        case startTime = "current_period_start"
        case endTime = "current_period_end"
        case status
        case canceled = "cancel_at_period_end"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)

        subscriptionID = try map.decode(.subscriptionID)
        name = try map.decode(.name)
        description = try? map.decodeIfPresent(.description)
        priceWithTax = try map.decodeLossless(.priceWithTax)
        status = try? map.decodeIfPresent(.status)
        startTime = try map.decodeDate(.startTime)
        endTime = try map.decodeDate(.endTime)
        canceled = try map.decode(.canceled)
    }
}

struct SubscriptionManageRequest: Encodable {
    enum Operation: String, Encodable {
        case edit
        case cancel
    }

    let operation: Operation
    let subscriptionID: String
    let paymentID: String?

    enum CodingKeys: String, CodingKey {
        case operation = "op"
        case subscriptionID = "product"
        case paymentID = "payment_id"
    }
}

struct SubscriptionInvoiceLine: Decodable {
    let amount: Float
    let description: String
    let period: Period

    struct Period: Decodable, Hashable {
        let start: Date
        let end: Date
    }
}

struct SubscriptionNextInvoice: Decodable {
    let amountRemaining: Float
    let startingBalance: Float
    let total: Float
    let subtotal: Float
    let amountDue: Float
    let lines: [SubscriptionInvoiceLine]

    enum CodingKeys: String, CodingKey {
        case amountRemaining = "amount_remaining"
        case startingBalance = "starting_balance"
        case total = "total"
        case subtotal = "subtotal"
        case amountDue = "amount_due"
        case lines
    }

    var groupedLines: [SubscriptionInvoiceLine.Period: [SubscriptionInvoiceLine]] {
        Dictionary(grouping: lines) { (el: SubscriptionInvoiceLine) in
            el.period
        }
    }
}

extension Subscription: ArticleProtocol {
    var articleThumbnailURL: URL? {
        image
    }

    var articleBody: String? {
        description
    }

    var articleTitle: String? {
        name
    }

}
