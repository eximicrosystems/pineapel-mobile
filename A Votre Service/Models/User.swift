//
// Created by Andrei Stoicescu on 03/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import SwiftyUserDefaults
import PromiseKit
import DifferenceKit

enum UserType: String, Decodable, DefaultsSerializable {
    case resident
    case concierge
}

struct User: Codable, DefaultsSerializable, Hashable {
    enum Gender: String, Codable, CaseIterable, Hashable {
        case male = "Male"
        case female = "Female"
        case na = "N/A"
        case undisclosed = "Undisclosed"
    }

    let uid: String
    let firstName: String?
    let lastName: String?
    var email: String?
    let phoneNumber: String?
    var userPictureURL: URL?
    let about: String?
    let buildingID: String?
    let apartmentNo: String?
    let floor: Int?
    let tagIDs: [String]
    let gender: Gender?
    let appVersion: String?
    let job: String?
}

extension User: Identifiable {
    var id: String { uid }
}

extension User: Differentiable {
    func hash(into hasher: inout Hasher) { hasher.combine(uid) }
    var differenceIdentifier: Int { hashValue }
}

extension User {
    enum CodingKeys: String, CodingKey {
        case uid = "uid"
        case firstName = "field_first_name"
        case lastName = "field_last_name"
        case email = "mail"
        case phoneNumber = "field_phone_number"
        case userPictureURL = "user_picture"
        case about = "field_about"
        case buildingID = "field_building"
        case apartmentNo = "field_apartment_nr"
        case floor = "field_floor"
        case tagIDs = "field_user_tags_ids"
        case gender = "field_gender"
        case appVersion = "field_app_version"
        case job = "field_job"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)

        uid = try map.decode(.uid)
        firstName = try? map.decodeIfPresent(.firstName)
        lastName = try? map.decodeIfPresent(.lastName)
        email = try? map.decodeIfPresent(.email)
        phoneNumber = try? map.decodeIfPresent(.phoneNumber)
        userPictureURL = try? map.decodeIfPresent(.userPictureURL)
        about = try? map.decodeIfPresent(.about)
        buildingID = try? map.decodeIfPresent(.buildingID)
        apartmentNo = try? map.decodeIfPresent(.apartmentNo)
        floor = try? map.decodeLosslessIfPresent(.floor)
        gender = try? map.decodeIfPresent(.gender)
        appVersion = try? map.decodeIfPresent(.appVersion)
        job = try? map.decodeIfPresent(.job)

        let tagIDsString: String? = try? map.decodeIfPresent(.tagIDs)
        if let tagIDs = tagIDsString {
            self.tagIDs = tagIDs.components(separatedBy: ",")
        } else {
            tagIDs = []
        }
    }

    static var `default`: User {
        User(uid: UUID().uuidString,
             firstName: "Andrei",
             lastName: "Stoicescu",
             email: "andrei@stoicescu.com",
             phoneNumber: "0727838211",
             userPictureURL: URL(string: "https://placeimg.com/200/200/arch?id=\(UUID().uuidString)"),
             about: nil,
             buildingID: nil,
             apartmentNo: nil,
             floor: nil,
             tagIDs: [],
             gender: nil,
             appVersion: nil,
             job: nil)
    }

    static func basicUser(uid: String, firstName: String?, lastName: String?, userPictureURL: URL?) -> User {
        User(uid: uid,
             firstName: firstName,
             lastName: lastName,
             email: nil,
             phoneNumber: nil,
             userPictureURL: userPictureURL,
             about: nil,
             buildingID: nil,
             apartmentNo: nil,
             floor: nil,
             tagIDs: [],
             gender: nil,
             appVersion: nil,
             job: nil)
    }

    static func getCurrentUser() -> Promise<User> {
        if let user = Defaults.user {
            return .value(user)
        } else {
            return ResidentService().getUser()
        }
    }

    var fullName: String {
        [firstName, lastName].compactMap { $0 }.joined(separator: " ")
    }

    var initials: String {
        [firstName, lastName].compactMap { $0?.prefix(1).uppercased() }.prefix(2).joined()
    }
}

extension User {
    static func fetch() -> Promise<User> {
        if let user = Defaults.user {
            return .value(user)
        } else {
            return ResidentService().getUser()
        }
    }
}