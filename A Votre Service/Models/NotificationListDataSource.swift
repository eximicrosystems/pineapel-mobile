//
// Created by Andrei Stoicescu on 24/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import PromiseKit
import Disk

class NotificationListDataSource {
    var items: [FeedNotificationItem] = []
    private (set) var currentPage: Int = 0
    private (set) var hasNextPage: Bool = false

    var showEmptyScreen: ((Bool) -> Void)?

    let apiService = ResidentService()

    func reloadCached() {
        currentPage = 0
        hasNextPage = false
        if let items = CodableCache.get([FeedNotificationItem].self, key: "feed_notifications") {
            self.items = items
        }
    }

    private func cacheCurrentItems() {
        CodableCache.set(items, key: "feed_notifications")
    }

    func getFirstPage() -> Promise<Void> {
        currentPage = 0
        hasNextPage = false
        return getNextPage()
    }

    func getNextPage() -> Promise<Void> {
        apiService.getFeedNotificationList(page: currentPage).then { [weak self] items in
            Promise<Void> { [weak self] seal in
                guard let self = self else {
                    seal.fulfill(())
                    return
                }

                let replace = self.currentPage == 0

                self.currentPage += 1

                self.hasNextPage = items.count > 0
                replace ? self.replace(items: items) : self.append(items: items)
                seal.fulfill(())
            }
        }
    }

    private func append(items: [FeedNotificationItem]) {
        DispatchQueue.main.async {
            self.items.append(contentsOf: items)
            self.showEmptyScreen?(self.items.count == 0)
        }
    }

    func replace(item: FeedNotificationItem) -> Int? {
        let firstIndex = items.firstIndex {
            $0.notificationID == item.notificationID
        }

        if let firstIndex = firstIndex {
            items[firstIndex] = item
            return firstIndex
        }

        return nil
    }

    private func replace(items: [FeedNotificationItem]) {
        DispatchQueue.main.async {
            self.items = items
            self.cacheCurrentItems()
            self.showEmptyScreen?(self.items.count == 0)
        }
    }

    func delete(notificationID: String) -> Int? {
        let firstIndex = items.firstIndex {
            $0.notificationID == notificationID
        }

        if let firstIndex = firstIndex {
            items.remove(at: firstIndex)
            return firstIndex
        }

        showEmptyScreen?(items.count == 0)

        return nil
    }
}
