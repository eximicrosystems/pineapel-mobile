//
// Created by Andrei Stoicescu on 20/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import PromiseKit

protocol PaginatedDataSourceDelegate: class {
    associatedtype ElementType
    func getItems(page: Int, per: Int) -> Promise<[ElementType]>
}

class AnyPaginatedDataSourceDelegate<ElementType>: PaginatedDataSourceDelegate {
    private let _getItems: (Int, Int) -> Promise<[ElementType]>

    init<P: PaginatedDataSourceDelegate>(_ type: P) where P.ElementType == ElementType {
        _getItems = type.getItems
    }

    func getItems(page: Int, per: Int) -> Promise<[ElementType]> {
        _getItems(page, per)
    }
}

class PaginatedDataSource<Element> {
    enum PaginatedDataSourceError: Error {
        case missingDelegate
    }

    var items: [Element] = []

    private (set) var currentPage: Int = 0
    private (set) var hasNextPage: Bool = false
    var per = Page.defaultPerPage

    let apiService = ResidentService()

    weak var delegate: AnyPaginatedDataSourceDelegate<Element>?

    func getItems(page: Int, per: Int) -> Promise<[Element]> {
        fatalError("Must override in subclasses")
    }

    func getFirstPage() -> Promise<[Element]> {
        self.currentPage = 0
        self.hasNextPage = false
        return self.getNextPage()
    }

    func getNextPage() -> Promise<[Element]> {
        let promise = delegate?.getItems(page: currentPage, per: per) ?? getItems(page: currentPage, per: per)
        return promise.then { [weak self] (items: [Element]) in
            Promise<[Element]> { [weak self] seal in
                guard let self = self else {
                    seal.fulfill([])
                    return
                }

                let replace = self.currentPage == 0

                self.currentPage += 1

                self.hasNextPage = items.count == self.per
                Log.this("current page increased \(self.currentPage)")
                Log.this("has next page \(self.hasNextPage)")
                replace ? self.items = items : self.items.append(contentsOf: items)
                seal.fulfill(items)
            }
        }
    }
}

extension PaginatedDataSource where Element: Identifiable {
    func removeItem(_ item: Element) -> Bool {
        guard let index = self.items.firstIndex(where: { $0.id == item.id }) else { return false }
        self.items.remove(at: index)
        return true
    }

    func indexOf(_ item: Element) -> Int? {
        self.items.firstIndex { $0.id == item.id }
    }

    func insertItem(_ item: Element, at index: Int) {
        self.items.insert(item, at: index)
    }
}