//
//  AuthResponse.swift
//  A Votre Service
//
//  Created by Andrei Stoicescu on 02/06/2020.
//  Copyright © 2020 A Votre Service LLC. All rights reserved.
//

import Foundation

struct RefreshTokenCredentials: Codable, Hashable {
    let accessToken: String
    let refreshToken: String
    let expiresAt: Date
}

struct RefreshTokenResponse: Decodable {
    let accessToken: String
    let refreshToken: String
    let expiresIn: Int
    let role: UserType
    let currentShift: ConciergeShift?
    let buildingFeatures: [FeatureFlags.Flag]
    let maintenanceRequestUrl: URL?

    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case refreshToken = "refresh_token"
        case expiresIn = "expires_in"
        case role
        case currentShift = "current_shift"
        case buildingFeatures = "field_building_features"
        case maintenanceRequestUrl = "field_maintenance_requests_url"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        accessToken = try map.decode(.accessToken)
        refreshToken = try map.decode(.refreshToken)
        expiresIn = try map.decode(.expiresIn)
        currentShift = try? map.decodeIfPresent(.currentShift)
        role = try map.decode(.role)
        let featuresStrings: [String] = (try? map.decodeIfPresent(.buildingFeatures)) ?? []
        buildingFeatures = featuresStrings.compactMap { FeatureFlags.Flag(rawValue: $0) }
        maintenanceRequestUrl = try? map.decodeIfPresent(.maintenanceRequestUrl)
    }
}

extension RefreshTokenResponse {
    var userAccessAllowed: Result<Void, APIError> {
        switch role {
        case .resident:
            return .success(())
        case .concierge:
            if currentShift != nil {
                return .success(())
            } else {
                return .failure(APIError(message: "Current Shift not available"))
            }
        }
    }

    var credentials: RefreshTokenCredentials {
        .init(accessToken: accessToken, refreshToken: refreshToken, expiresAt: expiresIn.seconds.later)
    }
}
