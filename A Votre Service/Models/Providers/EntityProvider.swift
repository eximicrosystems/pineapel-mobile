//
// Created by Andrei Stoicescu on 01/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import SwiftyUserDefaults
import PromiseKit

class EntityProvider<T>: ObservableObject {
    @Published var entity: T?
    @Published private (set) var loading = false
    private var reloadedFromServer = false

    var getLocalEntity: (() -> T?)?
    var getServerEntity: (() -> Promise<T>)?

    init(entity: T? = nil) {
        update(entity: entity)
    }

    func loadEntity() {
        guard entity == nil else { return }
        guard !loading else { return }

        loading = true

        if let entity = getLocalEntity?() {
            self.entity = entity
            if !reloadedFromServer {
                reloadServerEntity()
            } else {
                loading = false
            }
        } else {
            reloadServerEntity()
        }
    }

    private func reloadServerEntity() {
        guard AppState.isLoggedIn else {
            loading = false
            return
        }

        loading = true

        guard let serverPromise = getServerEntity else { return }

        serverPromise().done { entity in
            self.reloadedFromServer = true
            self.update(entity: entity)
        }.catch { error in
            Log.thisError(error)
        }.finally {
            self.loading = false
        }
    }

    func update(entity: T?) {
        self.entity = entity
    }
}
