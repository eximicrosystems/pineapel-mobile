//
// Created by Andrei Stoicescu on 09/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import PromiseKit
import Stripe
import SwiftyUserDefaults

class AVSStripeClient: NSObject, STPCustomerEphemeralKeyProvider {
    func createCustomerKey(withAPIVersion apiVersion: String, completion: @escaping STPJSONResponseCompletionBlock) {
        ResidentService().getEphemeralKey().done { json in
            Log.this(json)
            completion(json, nil)
        }.catch { error in
            Log.thisError(error)
            completion(nil, error)
        }
    }
}

class StripeProvider {
    struct CustomerData {
        let email: String
        let customerID: String
    }

    func getPaymentMethods() -> Promise<[STPPaymentMethod]?> {
        Promise<[STPPaymentMethod]?> { seal in
            STPCustomerContext(keyProvider: AVSStripeClient()).listPaymentMethodsForCustomer { methods, error in
                if let error = error {
                    seal.reject(error)
                } else {
                    seal.fulfill(methods)
                }
            }
        }
    }

    func delete(paymentMethod: STPPaymentMethod) -> Promise<Void> {
        Promise<Void> { seal in
            STPCustomerContext(keyProvider: AVSStripeClient()).detachPaymentMethod(fromCustomer: paymentMethod) { error in
                if let error = error {
                    seal.reject(error)
                } else {
                    seal.fulfill(())
                }
            }
        }
    }

    func getCustomerID() -> Promise<String> {
        if let customer = Defaults.stripeCustomerID {
            return .value(customer)
        } else {
            return ResidentService().getStripeCustomerDetails().map { details in
                details.customerID
            }
        }
    }

    func getUser() -> Promise<User> {
        if let user = Defaults.user {
            return .value(user)
        } else {
            return ResidentService().getUser()
        }
    }

    func getCustomerData() -> Promise<CustomerData> {
        when(fulfilled: getCustomerID(), getUser()).then { customerID, user in
            Promise<CustomerData> { seal in
                guard let email = user.email else {
                    seal.reject(APIError(message: "Missing email"))
                    return
                }
                seal.fulfill(CustomerData(email: email, customerID: customerID))
            }

        }
    }
}
