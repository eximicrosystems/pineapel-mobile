//
// Created by Andrei Stoicescu on 25.01.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import Combine
import PromiseKit
import DifferenceKit

struct CategoryUserRequest: Codable, Hashable, Differentiable, Identifiable {
    var id: String { request.id }
    var request: UserRequest
    var category: ALaCarteCategory?
}

class UserRequestsProvider: ObservableObject {
    private var requests = [UserRequest]()
    private var categories = [ALaCarteCategory]()
    @Published var categoryRequests = [CategoryUserRequest]()

    var dataSource: ObservablePaginatedDataSource<UserRequest>

    private (set) var cacheable: Bool

    private var subscriptions = Set<AnyCancellable>()

    @Published var showLoadingIndicator = false

    private var cacheKey: String {
        if let statuses = statuses {
            return "\(statuses.map(\.rawValue).joined(separator: "_"))_category_user_requests"
        } else {
            return "all_category_user_requests"
        }
    }

    private var statusChanged = false

    var statuses: [UserRequest.Status]? {
        didSet {
            statusChanged = true

            let value = statuses
            dataSource.paginate = { per, page in
                ResidentService().getUserRequestList(statuses: value, page: page, per: per)
            }
            reload()
        }
    }

    init(statuses: [UserRequest.Status]? = nil, cacheable: Bool = false) {
        self.statuses = statuses
        self.cacheable = cacheable

        dataSource = .init { per, page in
            ResidentService().getUserRequestList(statuses: statuses, page: page, per: per)
        }

        setupBindings()
    }

    func setupBindings() {
        dataSource.$items
                .receive(on: DispatchQueue.main)
                .sink { [weak self] items in
                    guard let self = self else { return }
                    self.remapCategories(cache: true)
                }.store(in: &subscriptions)

        dataSource.$showLoadingIndicator
                .receive(on: RunLoop.main)
                .sink { [weak self] loading in
                    guard let self = self else { return }
                    if self.cacheable {
                        self.showLoadingIndicator = (self.dataSource.items.count == 0 || self.statusChanged) && loading
                    } else {
                        self.showLoadingIndicator = loading
                    }
                }.store(in: &subscriptions)
    }

    func loadCache() {
        Log.this("loading from cache \(cacheKey)")
        if let items = CodableCache.get([CategoryUserRequest].self, key: cacheKey) {
            categories = items.compactMap(\.category)
            dataSource.items = items.map(\.request)
        }
    }

    func reload() {
        ResidentService().getALaCarteCategories().get { [weak self] categories in
            self?.categories = categories
        }.ensure { [weak self] in
            self?.dataSource.loadMoreContentIfNeeded()
        }.cauterize()
    }

    func loadRequest(requestId: String) -> Promise<CategoryUserRequest> {
        ResidentService().getALaCarteCategories().get { [weak self] categories in
            self?.categories = categories
        }.then { categories in
            ResidentService().getUserRequest(requestID: requestId)
        }.map { request in
            CategoryUserRequest(request: request, category: self.findCategory(request.categoryID))
        }
    }

    private func remapCategories(cache: Bool = false) {
        Log.this("REMAP CATEGORIES")
        categoryRequests = dataSource.items.map { request in
            CategoryUserRequest(request: request, category: findCategory(request.categoryID))
        }

        if cache && cacheable {
            Log.this("Storing cache to \(cacheKey)")
            CodableCache.set(categoryRequests, key: cacheKey)
        }
    }

    private func findCategory(_ categoryID: String?) -> ALaCarteCategory? {
        categories.first { category in
            category.categoryID == categoryID
        }
    }
}