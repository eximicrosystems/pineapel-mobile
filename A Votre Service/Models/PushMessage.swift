//
// Created by Andrei Stoicescu on 23/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit

typealias PushMessageCompletion = (UIBackgroundFetchResult) -> ()

enum PushCategoryIdentifier: String, CaseIterable, Decodable {
    case simpleFeedNotification = "simple_feed_notification"
    case deliveryFeedNotification = "delivery_feed_notification"

    var category: UNNotificationCategory {
        UNNotificationCategory(identifier: self, actions: actions, intentIdentifiers: [])
    }

    var actions: [PushActionIdentifier] {
        switch self {
        case .simpleFeedNotification:
            return [.openFeed]
        case .deliveryFeedNotification:
            return [.openFeed, .deliver]
        }
    }

    static var categories: Set<UNNotificationCategory> {
        Set(allCases.map(\.category))
    }
}

enum PushActionIdentifier: String {
    case openFeed
    case deliver

    var title: String {
        switch self {
        case .openFeed:
            return "View Post"
        case .deliver:
            return L10n.UserRequest.Labels.createRequest
        }
    }

    var action: UNNotificationAction {
        UNNotificationAction(identifier: self, options: .foreground)
    }
}

extension UNNotificationAction {
    convenience init(identifier: PushActionIdentifier, options: UNNotificationActionOptions = []) {
        self.init(identifier: identifier.rawValue, title: identifier.title, options: options)
    }
}

extension UNNotificationCategory {
    convenience init(identifier: PushCategoryIdentifier, actions: [PushActionIdentifier], intentIdentifiers: [String], options: UNNotificationCategoryOptions = []) {
        self.init(identifier: identifier.rawValue, actions: actions.map(\.action), intentIdentifiers: intentIdentifiers, options: options)
    }
}

struct PushMessage: Decodable {
    struct APS: Decodable {
        let category: PushCategoryIdentifier?
    }

    enum CategoryType: String, Decodable {
        case request
        case channel
        case notification
    }

    struct Category: Decodable {
        let type: CategoryType
        let id: String

        static func fromString(_ string: String?) -> Category? {
            guard let string = string else { return nil }
            let components = string.components(separatedBy: ":")
            guard components.count == 2 else { return nil }
            guard let categoryType = CategoryType(rawValue: components[0]) else { return nil }
            return Category(type: categoryType, id: components[1])
        }
    }

    let unread: Int?
    let category: PushCategoryIdentifier?
    let clickAction: Category?
    let imageURL: URL?
    let aps: APS?
    var actionIdentifier: PushActionIdentifier?

    enum CodingKeys: String, CodingKey {
        case unread
        case category
        case clickAction = "click_action"
        case imageURL = "image_url"
        case aps
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        unread = try? map.decodeLosslessIfPresent(.unread)
        category = try? map.decodeIfPresent(.category)
        clickAction = .fromString(try? map.decodeIfPresent(.clickAction))
        imageURL = try? map.decodeIfPresent(.imageURL)
        aps = try? map.decodeIfPresent(.aps)
    }
}