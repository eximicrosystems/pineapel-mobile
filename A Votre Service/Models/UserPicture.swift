//
// Created by Andrei Stoicescu on 06/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation

struct UserPicture: Codable {
    let targetID: Int?
    let width: Int?
    let height: Int?
    let url: URL?

    enum CodingKeys: String, CodingKey {
        case targetID = "target_id"
        case width
        case height
        case url
    }

    static func withTarget(_ targetID: Int?) -> UserPicture? {
        if let targetID = targetID {
            return UserPicture(targetID: targetID, width: nil, height: nil, url: nil)
        }
        return nil
    }
}