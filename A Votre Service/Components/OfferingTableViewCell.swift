//
//  OfferingTableViewCell.swift
//  A Votre Service
//
//  Created by Oscar Sevilla on 18/08/21.
//  Copyright © 2021 A Votre Service LLC. All rights reserved.
//

import UIKit

protocol MyTableViewCellDelegate: AnyObject {
    func didTabButton(title: String, index: Int)
}

class OfferingTableViewCell: UITableViewCell {
    
    weak var delegate: MyTableViewCellDelegate?
    static let identifier = "OfferingTableViewCell"
    
    @IBOutlet var button: UIButton!
    
    private var title: String = ""
    private var index: Int = 0
    
    static func nib() -> UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        button.setTitleColor(Asset.Colors.Semantic.colorMilitary.color, for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @IBAction func didTapButton(_ sender: Any){
        delegate?.didTabButton(title: self.title, index: index)
    }
    
    func configure(title: String, index: Int){
        self.title = title
        self.index = index
        button.setTitle(title, for: .normal)
    }
}
