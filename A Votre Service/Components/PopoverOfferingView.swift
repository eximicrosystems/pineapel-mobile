//
//  PopoverOfferingView.swift
//  A Votre Service
//
//  Created by Oscar Sevilla on 06/08/21.
//  Copyright © 2021 A Votre Service LLC. All rights reserved.
//

import UIKit
import Popover

class PopoverOfferingView: UIView {
    @IBOutlet var button1: UIButton!
    @IBOutlet var button2: UIButton!
    @IBOutlet var button3: UIButton!
    @IBOutlet var button4: UIButton!
    @IBOutlet var button5: UIButton!
    @IBOutlet var button6: UIButton!
    @IBOutlet var button7: UIButton!
    @IBOutlet var button8: UIButton!
    @IBOutlet var button9: UIButton!
    @IBOutlet var button10: UIButton!
    @IBOutlet var button11: UIButton!
    @IBOutlet var button12: UIButton!
    @IBOutlet var button13: UIButton!
    @IBOutlet var button14: UIButton!
    @IBOutlet var button15: UIButton!
    @IBOutlet var button16: UIButton!
    @IBOutlet var button17: UIButton!
    @IBOutlet var button18: UIButton!
    @IBOutlet var button19: UIButton!
    @IBOutlet var button20: UIButton!
    @IBOutlet var button21: UIButton!
    @IBOutlet var button22: UIButton!
    @IBOutlet var button23: UIButton!
    @IBOutlet var button24: UIButton!
    @IBOutlet var button25: UIButton!
    
    var delegateVC: OfferingServiceViewController!
    var pop: Popover!
    
    var botones: [String]!
    
    init(frame: CGRect, tBotones: [String]) {
        botones = tBotones
        super.init(frame: frame)
        commonInit()
        buttonsInit()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
 
    func commonInit() {
        let viewFromXib = Bundle.main.loadNibNamed("PopoverOfferingView", owner: self, options: nil)![0] as! UIView
        viewFromXib.frame = self.bounds
        addSubview(viewFromXib)
    }
    
    func buttonsInit(){
        button1.setTitle(botones[safe: 0] ?? "", for: .normal)
        button2.setTitle(botones[safe: 1] ?? "", for: .normal)
        button3.setTitle(botones[safe: 2] ?? "", for: .normal)
        button4.setTitle(botones[safe: 3] ?? "", for: .normal)
        button5.setTitle(botones[safe: 4] ?? "", for: .normal)
        button6.setTitle(botones[safe: 5] ?? "", for: .normal)
        button7.setTitle(botones[safe: 6] ?? "", for: .normal)
        button8.setTitle(botones[safe: 7] ?? "", for: .normal)
        button9.setTitle(botones[safe: 8] ?? "", for: .normal)
        button10.setTitle(botones[safe: 9] ?? "", for: .normal)
        button11.setTitle(botones[safe: 10] ?? "", for: .normal)
        button12.setTitle(botones[safe: 11] ?? "", for: .normal)
        button13.setTitle(botones[safe: 12] ?? "", for: .normal)
        button14.setTitle(botones[safe: 13] ?? "", for: .normal)
        button15.setTitle(botones[safe: 14] ?? "", for: .normal)
        button16.setTitle(botones[safe: 15] ?? "", for: .normal)
        button17.setTitle(botones[safe: 16] ?? "", for: .normal)
        button18.setTitle(botones[safe: 17] ?? "", for: .normal)
        button19.setTitle(botones[safe: 18] ?? "", for: .normal)
        button20.setTitle(botones[safe: 19] ?? "", for: .normal)
        button21.setTitle(botones[safe: 20] ?? "", for: .normal)
        button22.setTitle(botones[safe: 21] ?? "", for: .normal)
        button23.setTitle(botones[safe: 22] ?? "", for: .normal)
        button24.setTitle(botones[safe: 23] ?? "", for: .normal)
        button25.setTitle(botones[safe: 24] ?? "", for: .normal)
    }
    
    @IBAction func button1Action(_ sender: Any) {
        delegateVC.dataSelector(option: 0)
        pop.dismiss()
    }
    @IBAction func button2Action(_ sender: Any) {
        delegateVC.dataSelector(option: 1)
        pop.dismiss()
    }
    @IBAction func button3Action(_ sender: Any) {
        delegateVC.dataSelector(option: 2)
        pop.dismiss()
    }
    @IBAction func button4Action(_ sender: Any) {
        delegateVC.dataSelector(option: 3)
        pop.dismiss()
    }
    @IBAction func button5Action(_ sender: Any) {
        delegateVC.dataSelector(option: 4)
        pop.dismiss()
    }
    @IBAction func button6Action(_ sender: Any) {
        delegateVC.dataSelector(option: 5)
        pop.dismiss()
    }
    @IBAction func button7Action(_ sender: Any) {
        delegateVC.dataSelector(option: 6)
        pop.dismiss()
    }
    @IBAction func button8Action(_ sender: Any) {
        delegateVC.dataSelector(option: 7)
        pop.dismiss()
    }
    @IBAction func button9Action(_ sender: Any) {
        delegateVC.dataSelector(option: 8)
        pop.dismiss()
    }
    @IBAction func button10Action(_ sender: Any) {
        delegateVC.dataSelector(option: 9)
        pop.dismiss()
    }
    @IBAction func button11Action(_ sender: Any) {
        delegateVC.dataSelector(option: 10)
        pop.dismiss()
    }
    @IBAction func button12Action(_ sender: Any) {
        delegateVC.dataSelector(option: 11)
        pop.dismiss()
    }
    @IBAction func button13Action(_ sender: Any) {
        delegateVC.dataSelector(option: 12)
        pop.dismiss()
    }
    @IBAction func button14Action(_ sender: Any) {
        delegateVC.dataSelector(option: 13)
        pop.dismiss()
    }
    @IBAction func button15Action(_ sender: Any) {
        delegateVC.dataSelector(option: 14)
        pop.dismiss()
    }
    @IBAction func button16Action(_ sender: Any) {
        delegateVC.dataSelector(option: 15)
        pop.dismiss()
    }
    @IBAction func button17Action(_ sender: Any) {
        delegateVC.dataSelector(option: 16)
        pop.dismiss()
    }
    @IBAction func button18Action(_ sender: Any) {
        delegateVC.dataSelector(option: 17)
        pop.dismiss()
    }
    @IBAction func button19Action(_ sender: Any) {
        delegateVC.dataSelector(option: 18)
        pop.dismiss()
    }
    @IBAction func button20Action(_ sender: Any) {
        delegateVC.dataSelector(option: 19)
        pop.dismiss()
    }
    @IBAction func button21Action(_ sender: Any) {
        delegateVC.dataSelector(option: 20)
        pop.dismiss()
    }
    @IBAction func button22Action(_ sender: Any) {
        delegateVC.dataSelector(option: 21)
        pop.dismiss()
    }
    @IBAction func button23Action(_ sender: Any) {
        delegateVC.dataSelector(option: 22)
        pop.dismiss()
    }
    @IBAction func button24Action(_ sender: Any) {
        delegateVC.dataSelector(option: 23)
        pop.dismiss()
    }
    @IBAction func button25Action(_ sender: Any) {
        delegateVC.dataSelector(option: 24)
        pop.dismiss()
    }
}
