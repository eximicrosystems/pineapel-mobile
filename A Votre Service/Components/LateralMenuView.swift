//
//  LateralMenu.swift
//  A Votre Service
//
//  Created by Oscar Sevilla Garduño on 04/08/21.
//  Copyright © 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit
import SwiftyUserDefaults

class LateralMenuView: UIView {
    
    var navigation: UINavigationController!
    var rootView: UIViewController!
    @IBOutlet var helpButton: UIButton!
    @IBOutlet var title: UILabel!
    
    enum PageSource: String {
        case terms
        case privacy
    }
    
    var header: HeaderTabBarView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        self.title.text = "Hi \(Defaults.user?.firstName ?? "")"
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func commonInit() {
        let viewFromXib = Bundle.main.loadNibNamed("LateralMenu", owner: self, options: nil)![0] as! UIView
        viewFromXib.frame = self.bounds
        addSubview(viewFromXib)
    }
    
    func logout() {
        let alertController = UIAlertController(title: nil, message: L10n.MoreScreen.Actions.Logout.confirm, preferredStyle: .alert)

        alertController.addAction(UIAlertAction(title: L10n.MoreScreen.Actions.Logout.ok, style: .default, handler: { action in
            AppState.logout(userAction: true)
        }))

        alertController.addAction(UIAlertAction(title: L10n.MoreScreen.Actions.Logout.cancel, style: .cancel, handler: nil))

        rootView.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func hideMenu(_ sender: Any) {
        self.isHidden = true
        header.tabBar.tabBarController?.tabBar.isHidden = false
    }
    
    @IBAction func homeButton(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController(page: nil))
    }
    
    @IBAction func profileButton(_ sender: Any) {
        AppState.switchTo(viewController: MyProfileViewController())
    }
    
    @IBAction func buildingButton(_ sender: Any) {
        AppState.switchTo(viewController: MyBuildingViewController())
    }
    
    @IBAction func reservationsButton(_ sender: Any) {
        AppState.switchTo(viewController: MyReservationsViewController())
    }
    
    @IBAction func maintenanceButton(_ sender: Any) {
        AppState.switchTo(viewController: MyMaintenanceViewController())
    }
    
    @IBAction func guestButton(_ sender: Any) {
        AppState.switchTo(viewController: MyGuestViewController())
    }
    @IBAction func paymentButton(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController.buildNavControllerType(title: "Payment", vc: PaymentMethodManagerViewController()))
    }
    
    @IBAction func suscriptionsButton(_ sender: Any) {
        AppState.switchTo(viewController: SubscriptionListCarouselViewController())
    }
    
    @IBAction func orderHistoryButton(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController.buildNavControllerType(title: "Orders", vc: OrderListViewController()))
    }
    @IBAction func feedbackButton(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController.buildNavControllerType(title: "Feed", vc: FeedbackViewController()))
    }
    @IBAction func helpButton(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController.buildNavControllerType(title: "FAQ", vc: ConciergeBuildingFAQViewController()))
        
    }
    @IBAction func termsConditionsButton(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController.buildNavControllerType(title: "Terms", vc: TextPageViewController(pageSource: .terms)))
    }
    
    @IBAction func privacyButton(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController.buildNavControllerType(title: "Privacy", vc: TextPageViewController(pageSource: .privacy)))
    }
    
    @IBAction func logoutButton(_ sender: Any) {
        logout()
    }
}
