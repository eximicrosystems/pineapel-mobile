//
//  HeaderTabBarView.swift
//  A Votre Service
//
//  Created by Oscar Sevilla Garduño on 04/08/21.
//  Copyright © 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit

class HeaderTabBarView: UIView {
    
    @IBOutlet var buttonMenu: UIButton!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var chatButton: UIButton!
    @IBOutlet var conciergeName: UIButton!
    @IBOutlet var conciergelbl: UILabel!
    @IBOutlet var conciergeCheck: UIImageView!
    
    var lateralMenu: LateralMenuView!
    var tabBar: UIViewController!
    var concierges = [User]()
    
    init(frame: CGRect, tabBarVC: UIViewController, lm: LateralMenuView) {
        lateralMenu = lm
        tabBar = tabBarVC
        super.init(frame: frame)
        commonInit()
        
        if let concierges = [User].readCached(key: "feed_concierges") {
            self.concierges = concierges
        }
        if self.concierges.count>0 {
            conciergeName.setAttributedTitle(NSAttributedString(string: self.concierges[0].firstName?.uppercased(),
                                                                attributes: [.underlineStyle: NSUnderlineStyle.single.rawValue,
                                                                             NSAttributedString.Key.foregroundColor: UIColor.white]),
                                             for: .normal)
        }else{
            reloadConcierges()
            if self.concierges.count>0 {
                conciergeName.setAttributedTitle(NSAttributedString(string: self.concierges[0].firstName?.uppercased(),
                                                                attributes: [.underlineStyle: NSUnderlineStyle.single.rawValue,
                                                                             NSAttributedString.Key.foregroundColor: UIColor.white]),
                                             for: .normal)
            }else{
                conciergeName.isHidden = true
                conciergelbl.isHidden = true
                conciergeCheck.isHidden = true
            }
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func commonInit() {
        let viewFromXib = Bundle.main.loadNibNamed("HeaderTabBar", owner: self, options: nil)![0] as! UIView
        viewFromXib.frame = self.bounds
        addSubview(viewFromXib)
    }
    
    func reloadConcierges() {
        ResidentService().getConciergeOnDutyList().done { [weak self] users in
            guard let self = self else { return }
            users.cache(key: "feed_concierges")
            self.concierges = users
        }.catch { error in
            Log.thisError(error)
        }
    }
    
    @IBAction func buttonMenuAction(_ sender: Any) {
        if lateralMenu.isHidden {
            lateralMenu.isHidden = false
            tabBar.tabBarController?.tabBar.isHidden = true
        }
    }
    @IBAction func backButtonAction(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController(page: nil))
    }
    
    @IBAction func chatButtonAction(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController(page: 1))
    }
    
}
