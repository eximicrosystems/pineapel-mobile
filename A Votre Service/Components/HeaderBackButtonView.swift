//
//  HeaderBackButtonView.swift
//  A Votre Service
//
//  Created by Oscar Sevilla on 09/08/21.
//  Copyright © 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit

class HeaderBackButtonView: UIView {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var backButtonItem: UIButton!
    private var titleL: String!
    
    init(frame: CGRect, title: String) {
        titleL = title
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func commonInit() {
        let viewFromXib = Bundle.main.loadNibNamed("HeaderBackButtonView", owner: self, options: nil)![0] as! UIView
        viewFromXib.frame = self.bounds
        addSubview(viewFromXib)
        titleLabel.text = titleL
    }
    
    @IBAction func backButton(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController(page: nil))
    }
}
