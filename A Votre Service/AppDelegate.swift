//
//  AppDelegate.swift
//  A Votre Service
//
//  Created by Andrei Stoicescu on 01/06/2020.
//  Copyright © 2020 A Votre Service LLC. All rights reserved.
//

import UIKit
import Firebase
import FirebaseMessaging
import Stripe

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    // AppDelegate singleton. For now used in SceneDelegate
    static var standard: AppDelegate {
        UIApplication.shared.delegate as! AppDelegate
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        UNUserNotificationCenter.current().delegate = AVSPushNotifications.shared

        #if DEBUG
        NetworkActivityLogger.shared.startLogging()
        #endif

        FirebaseApp.configure()
        Messaging.messaging().delegate = AVSPushNotifications.shared
        StripeAPI.defaultPublishableKey = Configuration.default.aVotreService.stripe.publishableKey
        return true
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.map { String(format: "%02x", $0) }.joined()
        Log.this("RAW PUSH TOKEN: \(deviceTokenString)")
        AVSPushNotifications.shared.didRegisterWithDeviceToken(deviceToken: deviceToken)
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping PushMessageCompletion) {
        Log.this("received remote notification: \(userInfo)")

        guard let pushMessage = try? PushMessage(jsonDictionary: userInfo),
              let tabBar = window?.rootViewController as? PushNotificationHandler else {
            completionHandler(.noData)
            return
        }

        tabBar.processNotification(pushMessage, completion: completionHandler)
    }
}

// Notifications that don't require user action
protocol PushNotificationHandler {
    func processNotification(_ message: PushMessage, completion: @escaping PushMessageCompletion)
}

// Notifications that happen due to user action on the notification
protocol PushNotificationActionable {
    func processActionable(_ message: PushMessage)
}
