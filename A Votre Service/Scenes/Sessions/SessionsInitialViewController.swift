//
// Created by Andrei Stoicescu on 24/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import SwiftyUserDefaults
import SVProgressHUD
import Validator

class SessionsInitialViewController: BaseSessionsViewController {

    var biometrics = Biometrics()

    lazy var loginButton = UIButton.primaryButton(title: L10n.SessionsScreen.Buttons.Login.title)
    lazy var createAccountButton: UIButton = {
        let button = UIButton(type: .custom)

        let dontHave = "Don't have an account?".attributed(.body, .primaryLabel)
        let createOne = " Create one.".attributed(.body, .accentBackground)

        button.setAttributedTitle(
                dontHave?.appendingAttributedString(createOne),
                for: .normal)
        return button
    }()

    lazy var emailFieldContainer: AVSTextFieldContainer = {
        let emailFieldContainer = AVSTextFieldContainer(placeholder: L10n.SessionsScreen.Fields.EmailAddress.placeholder)

        emailFieldContainer.textField.keyboardType = .emailAddress

        #if DEBUG
        emailFieldContainer.text = "Dougy90@gmx.com"
        #endif

        return emailFieldContainer
    }()

    lazy var passwordFieldContainer: AVSTextFieldContainer = {
        let container = AVSTextFieldContainer(placeholder: L10n.SessionsScreen.Fields.Password.placeholder)
        container.textField.isSecureTextEntry = true
        return container
    }()

    lazy var forgotPasswordButton: UIButton = {
        let button = UIButton(type: .custom)
        button.contentHorizontalAlignment = .left
        button.setAttributedTitle(
                L10n.SessionsScreen.Buttons.ForgotPassword.title.attributed(.body, .accentBackground, alignment: .left),
                for: .normal)
        return button
    }()

    lazy var biometricsIDSwitch: UISwitch = {
        let sw = UISwitch()
        sw.setOn(biometrics.isEnabled, animated: false)
        sw.addTarget(self, action: #selector(switchChanged), for: .valueChanged)
        return sw
    }()

    @objc func switchChanged(control: UISwitch) {
        if control.isOn {
            biometrics.authenticate(saveResult: true).done { [weak self] in
                Log.this("auth done")
                self?.updateLoginButtonTitle()
            }.catch { error in
                control.setOn(false, animated: true)
                self.updateLoginButtonTitle()
                Log.thisError(error)
            }
        } else {
            Biometrics().isEnabled = false
            self.updateLoginButtonTitle()
        }
    }

    lazy var biometricsIDContainer: UIView = {
        let view = UIView()
        let label = UILabel()

        var text = ""
        if let biometryTypeString = biometrics.biometryTypeString {
            text = "Enable \(biometryTypeString)"
        }

        label.attributedText = text.attributed(.body, .primaryLabel)

        view.addSubview(biometricsIDSwitch)
        view.addSubview(label)

        label.snp.makeConstraints { make in
            make.top.left.bottom.equalToSuperview()
        }

        biometricsIDSwitch.snp.makeConstraints { make in
            make.left.equalTo(label.snp.right).offset(10)
            make.top.right.bottom.equalToSuperview()
        }

        return view
    }()

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if biometrics.shouldAuthenticate, let user = KeychainHelper().user {
            biometrics.authenticate().done { [weak self] in
                self?.login(email: user.email, password: user.password)
            }.cauterize()
        }
    }

    override func viewDidLoad() {
        titleLabelString = L10n.SessionsScreen.title
        subtitleLabelString = "" //L10n.SessionsScreen.subtitle
        //buttons = [loginButton, createAccountButton]
        buttons = [loginButton]

        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .horizontal
        stack.distribution = .equalSpacing
        stack.alignment = .fill

        stack.addArrangedSubviews(subviews: biometrics.canUseBiometricsHardware ? [forgotPasswordButton, biometricsIDContainer] : [forgotPasswordButton])

        fields = [emailFieldContainer, passwordFieldContainer, stack]

        super.viewDidLoad()
        replaceSystemBackButton()

        self.updateLoginButtonTitle()

        // Setup Events
        loginButton.addTarget(self, action: #selector(loginTapped), for: .touchUpInside)
        createAccountButton.addTarget(self, action: #selector(createAccount), for: .touchUpInside)
        forgotPasswordButton.addTarget(self, action: #selector(forgotPassword), for: .touchUpInside)
    }

    func updateLoginButtonTitle() {
        if biometrics.shouldAuthenticate, KeychainHelper().user != nil, let biometryType = biometrics.biometryTypeString {
            loginButton.setTitle(L10n.SessionsScreen.Buttons.loginWith(biometryType), for: .normal)
        } else {
            loginButton.setTitle(L10n.SessionsScreen.Buttons.Login.title, for: .normal)
        }
    }

    func validate() -> Bool {
        guard let email = emailFieldContainer.text, !String.isBlank(emailFieldContainer.text) else {
            SVProgressHUD.showError(withStatus: L10n.Errors.cantBeBlank(L10n.SessionsScreen.Fields.EmailAddress.placeholder))
            return false
        }

        guard !String.isBlank(passwordFieldContainer.text) else {
            SVProgressHUD.showError(withStatus: L10n.Errors.cantBeBlank(L10n.SessionsScreen.Fields.Password.placeholder))
            return false
        }

        let emailFormatRule = ValidationRulePattern(pattern: EmailValidationPattern.standard, error: AVSValidationError("Email must be valid"))

        let result = email.validate(rule: emailFormatRule)
        if !result.isValid {
            SVProgressHUD.showError(withStatus: emailFormatRule.error.message)
            return false
        }

        return true
    }

    @objc func forgotPassword(_ sender: UIButton) {
        let vc = CreateAccountViewController()
        vc.hasTerms = false
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @objc func createAccount(_ sender: UIButton) {
        self.navigationController?.pushViewController(CreateAccountViewController(), animated: true)
    }

    @objc func loginTapped(_ sender: UIButton) {
        // If we can use biometrics, and the switch is ticked, use face ID

        if biometrics.shouldAuthenticate, let user = KeychainHelper().user {
            biometrics.authenticate().done { [weak self] in
                self?.login(email: user.email, password: user.password)
            }.cauterize()
        } else {
            guard validate(),
                  let email = emailFieldContainer.text,
                  let password = passwordFieldContainer.text else { return }

            login(email: email, password: password)
        }
    }

    func login(email: String, password: String) {
        SVProgressHUD.show()
        AuthService().login(user: email, password: password).done {
            SVProgressHUD.dismiss()
            AppState.switchToMainViewController()
        }.catch { error in
            SVProgressHUD.showError(withStatus: error.localizedDescription)
        }
    }
}
