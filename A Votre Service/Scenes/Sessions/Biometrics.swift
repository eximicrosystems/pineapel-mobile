//
// Created by Andrei Stoicescu on 24/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import LocalAuthentication
import PromiseKit
import SwiftyUserDefaults

class Biometrics {
    private let context = LAContext()

    var biometryType: LABiometryType {
        let _ = canUseBiometricsHardware
        return context.biometryType
    }

    var biometryTypeString: String? {
        switch biometryType {
        case .touchID:
            return "Touch ID"
        case .faceID:
            return "Face ID"
        default:
            return nil
        }
    }

    var canUseBiometricsHardware: Bool {
        var error: NSError?
        let result = context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error)
        if let error = error {
            Log.thisError(error)
        }
        return result
    }

    var isEnabled: Bool {
        get {
            guard let enabled = Defaults.useBiometrics else { return false }
            return enabled
        }
        set {
            Defaults.useBiometrics = newValue
        }
    }

    var shouldAuthenticate: Bool {
        canUseBiometricsHardware && isEnabled
    }

    func authenticate(saveResult: Bool = false) -> Promise<Void> {
        Promise<Void> { seal in
            let reason = "Identify yourself when doing payments for Amenities or items from the A La Carte Menu"
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) { success, authenticationError in
                DispatchQueue.main.async {
                    if saveResult {
                        Defaults.useBiometrics = success
                    }

                    if success {
                        Log.this("success")
                        seal.fulfill(())
                    } else {
                        Log.thisError(authenticationError)
                        seal.reject(APIError.from(authenticationError))
                    }
                }
            }
        }
    }
}