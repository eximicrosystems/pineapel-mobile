//
// Created by Andrei Stoicescu on 18/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD
import LocalAuthentication
import AsyncDisplayKit
import DifferenceKit

class SettingsViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

    struct SettingItem: Codable, Hashable, Differentiable {
        var item: SettingsItems
        var value: Bool

        static func build(_ settings: Settings) -> [SettingItem] {
            SettingsItems.allCases.map { .init(item: $0, value: $0.isOn(settings)) }
        }

        var differenceIdentifier: Int { hashValue }
    }

    enum SettingsItems: String, Codable, CaseIterable {
        case chatMessages
        case deliveries
        case fitness
        case information
        case mailDelivery
        case packages
        case event

        var label: String {
            switch self {
            case .chatMessages:
                return L10n.SettingsScreen.Fields.ChatMessages.title
            case .deliveries:
                return L10n.SettingsScreen.Fields.Deliveries.title
            case .fitness:
                return L10n.SettingsScreen.Fields.Fitness.title
            case .information:
                return L10n.SettingsScreen.Fields.Information.title
            case .mailDelivery:
                return L10n.SettingsScreen.Fields.MailDelivery.title
            case .packages:
                return L10n.SettingsScreen.Fields.Packages.title
            case .event:
                return "Events"
            }
        }

        var subtitle: String {
            switch self {

            case .chatMessages:
                return "Opt to receive notifications for concierge chat messages"
            case .deliveries:
                return "Opt to receive notifications for orders and other product deliveries"
            case .fitness:
                return "Opt to receive notifications for fitness events"
            case .information:
                return "Opt to receive notifications for general information"
            case .mailDelivery:
                return "Opt to receive notifications for updates, news and services emails"
            case .packages:
                return "Opt to receive notifications for received packages"
            case .event:
                return "Opt to receive notifications for general events"
            }
        }

        func isOn(_ settings: Settings?) -> Bool {
            guard let settings = settings else { return false }
            switch self {
            case .chatMessages:
                return settings.conciergeChatMessages == true
            case .deliveries:
                return settings.deliveries == true
            case .fitness:
                return settings.fitness == true
            case .information:
                return settings.information == true
            case .mailDelivery:
                return settings.mailDelivery == true
            case .packages:
                return settings.packages == true
            case .event:
                return settings.event == true
            }
        }

        func rebuildSettings(_ settings: Settings?, value: Bool) -> Settings? {
            guard var settings = settings else { return nil }
            switch self {
            case .chatMessages:
                settings.conciergeChatMessages = value
            case .deliveries:
                settings.deliveries = value
            case .fitness:
                settings.fitness = value
            case .information:
                settings.information = value
            case .mailDelivery:
                settings.mailDelivery = value
            case .packages:
                settings.packages = value
            case .event:
                settings.event = value
            }

            return settings
        }

        func buildSettingObject(_ value: Bool) -> Settings {
            switch self {
            case .chatMessages:
                return Settings(conciergeChatMessages: value)
            case .deliveries:
                return Settings(deliveries: value)
            case .fitness:
                return Settings(fitness: value)
            case .information:
                return Settings(information: value)
            case .mailDelivery:
                return Settings(mailDelivery: value)
            case .packages:
                return Settings(packages: value)
            case .event:
                return Settings(event: value)
            }
        }
    }

    class Cell: ASCellNode {
        var titleNode: ASTextNode2
        var subtitleNode: ASTextNode2
        var switchNode = ASDisplayNode()
        var lineNode = ASDisplayNode.divider()

        var didChangeSetting: ((SettingItem, UISwitch) -> Void)?
        var setting: SettingItem

        init(settingItem: SettingItem) {
            setting = settingItem
            titleNode = .init(with: setting.item.label.attributed(.bodyTitleHeavy, .primaryLabel))
            subtitleNode = .init(with: setting.item.subtitle.attributed(.body, .primaryLabel))

            super.init()

            switchNode = ASDisplayNode { () -> UIView in
                let uiSwitch = UISwitch(frame: .zero)
                uiSwitch.setOn(self.setting.value, animated: false)
                uiSwitch.addTarget(self, action: #selector(self.settingSwitchChanged), for: .valueChanged)
                return uiSwitch
            }

            automaticallyManagesSubnodes = true
            backgroundColor = AVSColor.secondaryBackground.color
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            [
                [
                    [titleNode, subtitleNode].vStacked().spaced(10).filling(),
                    switchNode.size(51, 31)
                ].hStacked().justified(.spaceBetween).spaced(10)
                 .margin(15),
                lineNode
            ].vStacked()
        }

        @objc func settingSwitchChanged(control: UISwitch) {
            didChangeSetting?(setting, control)
        }
    }

    var collectionNode: ASCollectionNode {
        node as! ASCollectionNode
    }

    var settings: Settings?

    var items = [SettingItem]()

    override init() {
        let collectionNode = ASCollectionNode.avsCollection()
        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()
        view.backgroundColor = AVSColor.primaryBackground.color
        navigationItem.title = L10n.SettingsScreen.Navigation.title

        if !loadCached() {
            SVProgressHUD.show()
        }

        ResidentService().getSettings().done { settings in
            self.settings = settings
            self.reloadData(newSettings: settings)
            SVProgressHUD.dismiss()
            CodableCache.set(settings, key: "resident_notification_settings")
        }.catch { error in
            Log.thisError(error)
            SVProgressHUD.showError(withStatus: error.localizedDescription)
        }
    }

    func loadCached() -> Bool {
        if let settings = CodableCache.get(Settings.self, key: "resident_notification_settings") {
            self.settings = settings
            reloadData(newSettings: settings)
            return true
        }
        return false
    }

    func reloadData(newSettings: Settings) {
        let source = items
        let target = SettingItem.build(newSettings)

        let stagedChangeSet = StagedChangeset(source: source, target: target)
        for changeSet in stagedChangeSet {
            items = changeSet.data
            collectionNode.performDiffBatchUpdates(changeSet: changeSet)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        items.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let setting = items[indexPath.item]

        return {
            let cell = Cell(settingItem: setting)
            cell.didChangeSetting = { [weak self] item, control in
                guard let self = self else { return }
                self.updateSettings(item: item, control: control)
            }
            return cell
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }

    func updateSettings(item: SettingItem, control: UISwitch) {
        let params = item.item.buildSettingObject(control.isOn)

        ResidentService().updateSettings(params).done { [weak self] in
            guard let self = self else { return }
            if let settings = item.item.rebuildSettings(self.settings, value: control.isOn) {
                self.settings = settings
                CodableCache.set(settings, key: "resident_notification_settings")
            }
        }.catch { error in
            control.setOn(!control.isOn, animated: true)
            Log.thisError(error)
            SVProgressHUD.showError(withStatus: error.localizedDescription)
        }
    }

}
