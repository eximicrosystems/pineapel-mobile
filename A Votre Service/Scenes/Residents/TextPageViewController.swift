//
// Created by Andrei Stoicescu on 18/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import PromiseKit

enum PageSource: String {
    case terms
    case privacy
}

class TextPageViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {
    var collectionNode: ASCollectionNode {
        node as! ASCollectionNode
    }

    var article: TextArticle?
    var pageSource: PageSource

    lazy var navigationNode = UserNotificationsNodes.Navigation()
    
    init(pageSource: PageSource) {
        self.pageSource = pageSource
        
        let collectionNode = ASCollectionNode.avsCollection()
        super.init(node: collectionNode)

        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "chevron.left"), style: .plain, target: self, action: #selector(backView))
        
        if let article = CodableCache.get(TextArticle.self, key: "text_page_\(pageSource.rawValue)") {
            self.article = article
            updateArticleInterface()
        } else {
            SVProgressHUD.show()
        }
        loadPageSource()
    }
    
    @objc func backView(){
        AppState.switchTo(viewController: ResidentTabBarViewController(page: nil))
    }

    func updateArticleInterface() {
        navigationItem.title = article?.title?.html?.string
        collectionNode.reloadData()
    }

    func loadPageSource() {
        let apiService = ResidentService()

        let loadPromise: Promise<TextArticle>

        switch pageSource {
        case .terms:
            loadPromise = apiService.getTerms()
        case .privacy:
            loadPromise = apiService.getPrivacy()
        }

        loadPromise.done { [weak self] article in
            guard let self = self else { return }
            SVProgressHUD.dismiss()
            if self.article != article {
                self.article = article
                CodableCache.set(article, key: "text_page_\(self.pageSource.rawValue)")
                self.updateArticleInterface()
            }

        }.catch { error in
            SVProgressHUD.showError(withStatus: error.localizedDescription)
            Log.thisError(error)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        article != nil ? 1 : 0
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let article = self.article
        let body = article?.body?.html
        return {
            let textNode = ASTextNode2()
            textNode.attributedText = body

            let cellNode = ASCellNode()
            cellNode.automaticallyManagesSubnodes = true
            cellNode.layoutSpecBlock = { node, range in
                textNode.margin(20)
            }

            return cellNode
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if traitCollection.userInterfaceStyle != previousTraitCollection?.userInterfaceStyle {
            collectionNode.reloadData()
        }
    }
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return nil
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
