//
// Created by Andrei Stoicescu on 21/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit
import SwiftyUserDefaults
import AsyncDisplayKit
import SVProgressHUD
import PromiseKit
import Combine

protocol BellNotificationsViewControllerProtocol: class {
    func bellNotificationsChangedNotificationsCount()
    func bellNotificationsFeedNotificationCellTapped(icon: UserNotificationsNodes.FeedCell.Icon, item: FeedNotificationItem)
}

protocol DaysDataSourceItem {
    var date: Date? { get }
}

extension BellNotification: DaysDataSourceItem {
    var date: Date? { createdAt }
}

class DaysDataSource<Item: DaysDataSourceItem> {
    var sections = [[Item]]()
    var items = [Item]() {
        didSet {
            sections = items.split { item1, item2 in
                guard let date1 = item1.date, let date2 = item2.date else { return false }
                return !date1.isSameDay(date: date2)
            }
        }
    }
}

fileprivate extension ObservablePaginatedDataSource where ListItem == BellNotification {
    enum NotificationError: Error {
        case readNotificationError
        case notificationAlreadyRead
    }

    @discardableResult
    func readNotification(_ notification: BellNotification) -> Promise<Void> {
        guard !notification.read else { return Promise(error: NotificationError.notificationAlreadyRead) }
        let existingNotificationIndex = items.firstIndex { $0.notificationID == notification.notificationID }
        guard let index = existingNotificationIndex else { return Promise(error: NotificationError.readNotificationError) }
        items[index].readNotification()
        return ResidentService().readBellNotification(notificationID: notification.notificationID)
    }
}

class BellNotificationsViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource, ASCollectionDelegateFlowLayout {

    lazy var emptyNode = EmptyNode(emptyNodeType: .bellNotifications)
    lazy var refreshControl = UIRefreshControl()

    var dataSource: ObservablePaginatedDataSource<BellNotification>
    var daysDataSource: DaysDataSource<BellNotification>

    weak var delegate: BellNotificationsViewControllerProtocol?
    private var subscriptions = Set<AnyCancellable>()
    var collectionNode: ASCollectionNode

    override init() {
        collectionNode = .avsCollection()
        dataSource = .init { per, page in
            ResidentService().getBellNotifications(page: page, per: per)
        }
        dataSource.cacheable = true
        dataSource.cacheKey = "bell_notifications"

        daysDataSource = .init()

        super.init(node: collectionNode)

        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupBindings()
        replaceSystemBackButton()

        navigationItem.title = "Notifications"
        navigationItem.rightBarButtonItems = [
            UIBarButtonItem(image: Asset.Images.Concierge.icCog.image.withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(openNotificationSettings)),
            UIBarButtonItem(image: Asset.Images.Concierge.icCheck.image.withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(markAllRead))
        ]
        showEmptyScreen(false)
        collectionNode.view.backgroundView = emptyNode.view
        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)
        collectionNode.view.addSubview(refreshControl)

        emptyNode.actionButtonTapped = { [weak self] in
            guard let tabBarController = self?.tabBarController as? ResidentTabBarViewController else { return }
            tabBarController.switchTo(controllerType: .chat)
        }

        refreshItems()
    }

    func setupBindings() {
        dataSource.$items
                .receive(on: DispatchQueue.main)
                .sink { [weak self] items in
                    guard let self = self else { return }
                    self.daysDataSource.items = items
                    self.collectionNode.reloadData()
                }.store(in: &subscriptions)

        dataSource.$hasNoItemsAfterLoading
                .receive(on: DispatchQueue.main)
                .sink { [weak self] hasNoItemsAfterLoading in
                    self?.showEmptyScreen(hasNoItemsAfterLoading)
                }.store(in: &subscriptions)

        dataSource.$showLoadingIndicator
                .receive(on: DispatchQueue.main)
                .sink { [weak self] loading in
                    guard let self = self else { return }
                    if loading {
                        if !self.refreshControl.isRefreshing {
                            SVProgressHUD.show()
                        }
                    } else {
                        SVProgressHUD.dismiss()
                        self.refreshControl.endRefreshing()
                    }
                }.store(in: &subscriptions)
    }

    @objc func refreshItems() {
        dataSource.loadMoreContentIfNeeded()
    }

    @objc func markAllRead() {
        let message = "Mark all notifications as read?"
        let deleteAlert = UIAlertController(title: nil, message: message, preferredStyle: .alert)

        deleteAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            SVProgressHUD.show()
            ResidentService().readAllNotifications().done { [weak self] in
                self?.refreshItems()
            }.catch { error in
                SVProgressHUD.showError(withStatus: error.localizedDescription)
            }
        }))

        deleteAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        present(deleteAlert, animated: true, completion: nil)
    }

    @objc func openNotificationSettings() {
        navigationController?.pushViewController(SettingsViewController(), animated: true)
    }

    func showEmptyScreen(_ show: Bool) {
        emptyNode.isHidden = !show
    }

    func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
        daysDataSource.sections.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let notification = daysDataSource.sections[indexPath.section][indexPath.item]
        return {
            ConciergePagerListNodes.RequestCell(bellNotification: notification)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, supplementaryElementKindsInSection section: Int) -> [String] {
        [UICollectionView.elementKindSectionHeader]
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> ASCellNode {
        guard kind == UICollectionView.elementKindSectionHeader else { return ASCellNode() }
        let item = daysDataSource.sections[indexPath.section][indexPath.item]

        return ConciergePagerListNodes.Header(title: item.date?.avsDateAgo, subject: nil, layoutType: .spaceBetween)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, sizeRangeForHeaderInSection section: Int) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        daysDataSource.sections[section].count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        let notification = daysDataSource.sections[indexPath.section][indexPath.item]

        if !notification.read {
            dataSource.readNotification(notification).done { [weak self] in
                self?.delegate?.bellNotificationsChangedNotificationsCount()
            }.catch { error in
                Log.thisError(error)
            }
        }

        self.collectionNode.performBatch(animated: false, updates: { [weak self] in
            guard let self = self else { return }
            self.collectionNode.reloadItems(at: [indexPath])
        }, completion: nil)

        guard let notificationType = notification.itemType else { return }
        guard !notification.itemID.isEmpty else { return }

        let vc: UIViewController

        switch notificationType {
        case .notification:
            let feedNotificationsVC = FeedNotificationViewController(notificationID: notification.itemID)
            feedNotificationsVC.delegate = self
            vc = feedNotificationsVC
        case .request:
            vc = UserRequestSingleViewController(requestID: notification.itemID)
        case .order:
            let orderVC = OrderSummaryViewController(orderID: notification.itemID)
            orderVC.isNavigational = true
            vc = orderVC
        }

        navigationController?.pushViewController(vc, animated: true)
    }

    func collectionView(_ collectionView: ASCollectionView, willDisplayNodeForItemAt indexPath: IndexPath) {
        dataSource.loadMoreContentIfNeeded(currentItem: daysDataSource.sections[indexPath.section][indexPath.item])
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }
}

extension BellNotificationsViewController: FeedNotificationViewControllerProtocol {
    func notificationViewControllerFeedCellTapped(icon: UserNotificationsNodes.FeedCell.Icon, item: FeedNotificationItem) {
        switch icon {
        case .chat:
            navigationController?.dismiss(animated: true)
        case .delivery, .readMore:
            ()
        }
        delegate?.bellNotificationsFeedNotificationCellTapped(icon: icon, item: item)
    }
}