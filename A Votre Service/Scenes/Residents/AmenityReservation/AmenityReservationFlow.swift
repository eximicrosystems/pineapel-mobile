// //
// // Created by Andrei on 12.04.2021.
// // Copyright (c) 2021 A Votre Service LLC. All rights reserved.
// //
//
// import Foundation
// import UIKit
//
// class AmenityReservationFlow {
//     var amenity: Amenity
//     var reservation: AmenityReservation?
//     var rootViewController: UIViewController
//
//     init(amenity: Amenity, reservation: AmenityReservation?, rootViewController: UIViewController) {
//         self.amenity = amenity
//         self.reservation = reservation
//         self.rootViewController = rootViewController
//     }
//
//     func startFlow() {
//         let amenityVC: UIViewController
//         let shouldReload: Bool
//
//         if amenity.priceType == .day {
//             amenityVC = AmenityDateSlotsViewController(amenity: amenity)
//             shouldReload = false
//         } else {
//             amenityVC = AmenityDateSelectionViewController(amenity: amenity, reservation: reservation)
//             shouldReload = true
//         }
//
//         let navController = AVSNavigationController(rootViewController: amenityVC)
//         navController.dismissNavigation = { [weak self] in
//             guard let self = self else { return }
//             self.navigationController?.dismiss(animated: true)
//             if shouldReload {
//                 // self.reloadReservations?()
//                 // self.reloadReservation()
//             }
//         }
//
//         rootViewController.navigationController?.present(navController, animated: true)
//     }
//
// }