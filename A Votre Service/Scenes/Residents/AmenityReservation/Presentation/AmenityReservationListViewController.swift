//
// Created by Andrei Stoicescu on 26/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit
import SwiftyUserDefaults
import AsyncDisplayKit
import SVProgressHUD
import Fakery
import PromiseKit
import Combine
import DifferenceKit

class AmenityReservationListViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {
    enum ReservationFilter {
        case all
        case upcoming
        case past
        case byDate(Date)

        var displayString: String {
            switch self {
            case .all:
                return "All"
            case .upcoming:
                return "Upcoming"
            case .past:
                return "Past"
            case .byDate:
                return "By Date"
            }
        }

        var selectedString: String {
            switch self {
            case .all, .upcoming, .past:
                return displayString
            case .byDate(let date):
                return date.avsDateShortMonthYearString()
            }
        }

        var startDate: Date? {
            switch self {
            case .all:
                return Date(timeIntervalSince1970: 0)
            case .upcoming:
                return Date()
            case .byDate(let date):
                return date.convertedToBuildingTimeZone(.startOfDay)
            case .past:
                return nil
            }
        }

        var endDate: Date? {
            switch self {
            case .all:
                return 10.years.later
            case .upcoming:
                return nil
            case .past:
                return Date()
            case .byDate(let date):
                return date.convertedToBuildingTimeZone(.endOfDay)
            }
        }
    }

    enum Sections: Int, CaseIterable, Differentiable {
        case filter
        case reservations
    }

    lazy var emptyNode = EmptyNode(emptyNodeType: .reservation)
    lazy var refreshControl = UIRefreshControl()
    private var subscriptions = Set<AnyCancellable>()
    var collectionNode: ASCollectionNode

    private let textField = UITextField(frame: .zero)

    var dataSource = ObservablePaginatedDataSource<AmenityReservation>()
    private var sections = [ArraySection<Sections, AnyDifferentiable>]()
    var filter = ReservationFilter.upcoming {
        didSet { refreshItems() }
    }

    override init() {
        collectionNode = .avsCollection()
        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self

        dataSource.dynamicCacheable = { [weak self] in
            guard let self = self else { return false }
            switch self.filter {
            case .upcoming:
                return true
            default:
                return false
            }
        }

        dataSource.cacheKey = "my_reservations"
        dataSource.paginate = { per, page in
            ResidentService().getAmenityReservationList(page: page, per: per, startDate: self.filter.startDate, endDate: self.filter.endDate)
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupBindings()
        replaceSystemBackButton()

        view.addSubview(textField)
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }

        textField.inputView = datePicker
        textField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(hitDone))

        navigationItem.title = L10n.MoreScreen.Items.amenityReservations

        showEmptyScreen(false)
        collectionNode.view.backgroundView = emptyNode.view

        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)
        collectionNode.view.addSubview(refreshControl)

        emptyNode.actionButtonTapped = { [weak self] in
            self?.tapCreateButton()
        }

        emptyNode.actionButtonTapped = { [weak self] in
            guard let tabBarController = self?.tabBarController as? ResidentTabBarViewController else { return }
            tabBarController.switchTo(controllerType: .amenities)
        }

        navigationItem.rightBarButtonItems = [
            UIBarButtonItem(image: Asset.Images.Concierge.icFilter.image.withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(tapFilterButton)),
            UIBarButtonItem(image: Asset.Images.Concierge.icPlus.image.withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(tapCreateButton))
        ]

        refreshItems()
    }

    func reloadData() {
        let showsFilter: Bool
        switch filter {
        case .all:
            showsFilter = false
        default:
            showsFilter = true
        }

        let filterSectionItems: [AnyDifferentiable] = showsFilter ? [AnyDifferentiable(filter.displayString)] : []
        let reservationItems = dataSource.items.map { AnyDifferentiable($0) }

        let target: [ArraySection<Sections, AnyDifferentiable>] = [
            ArraySection(model: .filter, elements: filterSectionItems),
            ArraySection(model: .reservations, elements: reservationItems)
        ]

        let stagedChangeSet = StagedChangeset(source: sections, target: target)
        for changeSet in stagedChangeSet {
            sections = changeSet.data
            collectionNode.performDiffBatchUpdates(changeSet: changeSet)
        }
    }

    func setupBindings() {
        dataSource.$items
                .receive(on: DispatchQueue.main)
                .sink { [weak self] items in
                    self?.reloadData()
                }.store(in: &subscriptions)

        dataSource.$hasNoItemsAfterLoading
                .receive(on: DispatchQueue.main)
                .sink { [weak self] hasNoItemsAfterLoading in
                    self?.showEmptyScreen(hasNoItemsAfterLoading)
                }.store(in: &subscriptions)

        dataSource.$showLoadingIndicator
                .receive(on: DispatchQueue.main)
                .sink { [weak self] loading in
                    guard let self = self else { return }

                    if loading {
                        if !self.refreshControl.isRefreshing {
                            SVProgressHUD.show()
                        }
                    } else {
                        SVProgressHUD.dismiss()
                        self.refreshControl.endRefreshing()
                    }
                }.store(in: &subscriptions)
    }

    @objc func refreshItems() {
        dataSource.loadMoreContentIfNeeded()
    }

    @objc func hitDone() {
        guard let picker = textField.inputView as? UIDatePicker else { return }
        filter = .byDate(picker.date)
    }

    func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
        sections.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        sections[section].elements.count
    }

    @objc func tapFilterButton() {
        let alertController = UIAlertController(title: "Filter Reservations", message: nil, preferredStyle: .actionSheet)

        [ReservationFilter.all, .upcoming, .past].map { filter in
            UIAlertAction(title: filter.displayString, style: .default) { [weak self] _ in
                guard let self = self else { return }
                self.filter = filter
            }
        }.forEach { action in
            alertController.addAction(action)
        }

        alertController.addAction(UIAlertAction(title: ReservationFilter.byDate(Date()).displayString, style: .default) { [weak self] _ in
            guard let self = self else { return }
            self.textField.becomeFirstResponder()
        })

        alertController.addAction(UIAlertAction(title: L10n.General.cancel, style: .cancel, handler: nil))
        alertController.fixIpadIfNeeded(view: view)
        present(alertController, animated: true)
    }

    @objc func tapCreateButton() {
        guard let tabBarController = tabBarController as? ResidentTabBarViewController else { return }
        tabBarController.switchTo(controllerType: .amenities)
    }

    func showEmptyScreen(_ show: Bool) {
        emptyNode.isHidden = !show
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        guard let section = Sections(rawValue: indexPath.section) else { return { ASCellNode() } }

        switch section {
        case .filter:
            return {
                let pill = ConciergePagerListNodes.PillTagCell(title: self.filter.selectedString)
                pill.onTapClose = { [weak self] in
                    self?.filter = .all
                }

                let cell = ASCellNode()
                cell.automaticallyManagesSubnodes = true
                cell.layoutSpecBlock = { node, range in
                    [pill, ASDisplayNode.spacer()].hStacked()
                }
                return cell
            }
        case .reservations:
            let item = dataSource.items[indexPath.item]
            return {
                ConciergePagerListNodes.RequestCell.residentReservationListItem(reservation: item)
            }
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        textField.resignFirstResponder()

        guard let section = Sections(rawValue: indexPath.section) else { return }

        switch section {
        case .filter:
            ()
        case .reservations:
            let item = dataSource.items[indexPath.item]
            let previewVC = AmenityReservationPreviewViewController(reservation: item)

            previewVC.reloadReservations = { [weak self] in
                guard let self = self else { return }
                self.refreshItems()
            }

            previewVC.canceledReservation = { [weak self] in
                guard let self = self else { return }
                self.navigationController?.popToViewController(self, animated: true)
                SVProgressHUD.show()
                self.refreshItems()
            }

            navigationController?.pushViewController(previewVC, animated: true)
        }
    }

    func collectionView(_ collectionView: ASCollectionView, willDisplayNodeForItemAt indexPath: IndexPath) {
        if indexPath.section == Sections.reservations.rawValue {
            dataSource.loadMoreContentIfNeeded(currentItem: dataSource.items[indexPath.item])
        }
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        textField.resignFirstResponder()
    }
}