//
// Created by Andrei Stoicescu on 26/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import PromiseKit
import SwiftyUserDefaults

class AmenityReservationPreviewViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

    var canceledReservation: (() -> Void)?
    var reloadReservations: (() -> Void)?
    var navigator: AmenityReservationNavigator?

    var collectionNode: ASCollectionNode

    var reservation: AmenityReservation
    var amenity: Amenity?

    lazy var navigationNode = UserNotificationsNodes.Navigation()
    
    init(amenity: Amenity? = nil, reservation: AmenityReservation) {
        self.reservation = reservation
        self.amenity = amenity
        collectionNode = .avsCollection()
        let mainNode = ASDisplayNode()
        mainNode.automaticallyManagesSubnodes = true
//        super.init(node: collectionNode)
        super.init(node: mainNode)
        mainNode.layoutSpecBlock = { [weak self] node, range in
            guard let self = self else { return ASDisplayNode().wrapped() }
            return [
                self.navigationNode,
                self.collectionNode.growing()
            ].vStacked()
        }
        
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let tabBar = BottonTabBarView(frame: CGRect(x: 0, y: UIScreen.main.bounds.height-83, width: UIScreen.main.bounds.width, height: 83))
        self.view.addSubview(tabBar)
        let lateralMenu = LateralMenuView(frame: CGRect(x: 80, y: 0, width: 347, height: UIScreen.main.bounds.height))
        let myView = HeaderTabBarView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 190), tabBarVC: self, lm: lateralMenu)
        myView.backButton.addTarget(self, action: #selector(tappedButtonActivator), for: .touchUpInside)
        myView.backButton.isHidden = false
        lateralMenu.isHidden = true
        lateralMenu.header = myView
        lateralMenu.rootView = self
        self.view.addSubview(myView)
        self.view.addSubview(lateralMenu)

        showMenuIfNeeded()

        if amenity == nil {
            SVProgressHUD.show()
            ResidentService().getAmenity(amenityID: reservation.amenityID).done { [weak self] amenity in
                SVProgressHUD.dismiss()
                guard let self = self else { return }
                self.amenity = amenity
                self.collectionNode.reloadData()
                self.showMenuIfNeeded()
            }.catch { error in
                SVProgressHUD.showError(withStatus: error.localizedDescription)
                Log.thisError(error)
            }
        }

        replaceSystemBackButton()
        navigationItem.title = L10n.AmenityListScreen.Navigation.title
    }

    func showMenuIfNeeded() {
        guard let amenity = amenity else { return }

        let buildingTimeZone = AppState.buildingTimeZone

        if reservation.editable(buildingTimeZone: buildingTimeZone, amenity: amenity) ||
           reservation.cancellable(buildingTimeZone: buildingTimeZone) {
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: Asset.Images.icDotsHorizontal.image.withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(tapMenu))
        } else {
            navigationItem.rightBarButtonItem = nil
        }
    }
    
    @objc func tappedButtonActivator() {
        AppState.switchTo(viewController: MyReservationsViewController())
    }

    @objc func tapMenu() {
        guard let amenity = amenity else { return }

        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let buildingTimeZone = AppState.buildingTimeZone

        if reservation.editable(buildingTimeZone: buildingTimeZone, amenity: amenity) {
            alertController.addAction(UIAlertAction(title: "Edit Reservation", style: .default) { [weak self] _ in
                self?.editReservation()
            })
        }

        if reservation.cancellable(buildingTimeZone: buildingTimeZone) {
            alertController.addAction(UIAlertAction(title: "Cancel Reservation", style: .destructive) { [weak self] _ in
                self?.cancelReservation()
            })
        }

        alertController.addAction(UIAlertAction(title: L10n.General.cancel, style: .cancel, handler: nil))
        alertController.fixIpadIfNeeded(view: view)
        present(alertController, animated: true)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        amenity != nil ? 1 : 0
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        guard let amenity = amenity else { return { ASCellNode() } }
        return {
            AmenityReservationNodes.PreviewCell(amenity: amenity, reservation: self.reservation)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }

    func editReservation() {
        guard let amenity = amenity else { return }

        navigator = AmenityReservationNavigator(amenity: amenity, reservation: reservation)
        navigator?.present(in: navigationController ?? self) { [weak self] in
            guard let self = self else { return }

            if amenity.priceType != .day {
                self.reloadReservations?()
                self.reloadReservation()
            }

            self.navigator = nil
        }

    }

    func reloadReservation() {
        guard let reservationID = reservation.reservationID else { return }
        ResidentService().getAmenityReservation(reservationID: reservationID).done { reservation in
            self.reservation = reservation
            self.collectionNode.reloadData()
        }.cauterize()
    }

    func cancelReservation() {
        guard let reservationID = reservation.reservationID else { return }
        guard let amenity = amenity else { return }

        let vc = AmenityReservationCancelViewController(amenity: amenity, reservation: reservation)
        vc.canceledReservation = { [weak self] in
            guard let self = self else { return }
            self.dismiss(animated: true, completion: nil)

            SVProgressHUD.show()
            ResidentService().deleteAmenityReservation(reservationID: reservationID).done { [weak self] in
                self?.canceledReservation?()
            }.catch { error in
                Log.thisError(error)
                SVProgressHUD.showError(withStatus: error.localizedDescription)
            }
        }

        let nav = AVSNavigationController(rootViewController: vc)
        nav.dismissNavigation = { [weak self] in
            self?.dismiss(animated: true, completion: nil)
        }

        present(nav, animated: true, completion: nil)
    }
}
