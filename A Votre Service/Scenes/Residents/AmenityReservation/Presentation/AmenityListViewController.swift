//
// Created by Andrei Stoicescu on 18/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import PromiseKit
import SwiftyUserDefaults
import Combine
import DifferenceKit

class AmenityListViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {
    lazy var emptyNode = EmptyNode(emptyNodeType: .amenityList)

    var collectionNode: ASCollectionNode

    var dataSource = ObservablePaginatedDataSource<Amenity>()
    private var subscriptions = Set<AnyCancellable>()
    var items = [Amenity]()
    var showBackButton: Bool!

    lazy var refreshControl = UIRefreshControl()
    
    lazy var navigationNode = UserNotificationsNodes.Navigation()

    init(requiresReservation: Bool) {
        collectionNode = .avsCollection()

        showBackButton = requiresReservation
        
        let mainNode = ASDisplayNode()
        mainNode.automaticallyManagesSubnodes = true

        super.init(node: mainNode)
        
        mainNode.layoutSpecBlock = { [weak self] node, range in
            guard let self = self else { return ASDisplayNode().wrapped() }
            return [
                self.navigationNode,
                self.collectionNode.growing()
            ].vStacked()
        }
        
        collectionNode.delegate = self
        collectionNode.dataSource = self

        dataSource.cacheable = true
        dataSource.cacheKey = "amenity_reservations_reservable_\(requiresReservation)"
        dataSource.paginate = { per, page in
            ResidentService().getAmenityList(requiresReservation: requiresReservation, page: page, per: per)
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let lateralMenu = LateralMenuView(frame: CGRect(x: 80, y: 0, width: 347, height: UIScreen.main.bounds.height))
        let myView = HeaderTabBarView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 190), tabBarVC: self, lm: lateralMenu)
        lateralMenu.isHidden = true
        lateralMenu.header = myView
        lateralMenu.rootView = self
        
        if !showBackButton {
            let tabBar = BottonTabBarView(frame: CGRect(x: 0, y: UIScreen.main.bounds.height-83, width: UIScreen.main.bounds.width, height: 83))
            self.view.addSubview(tabBar)
            myView.backButton.addTarget(self, action: #selector(tappedButtonActivator), for: .touchUpInside)
            myView.backButton.isHidden = showBackButton
        }
        
        self.view.addSubview(myView)
        self.view.addSubview(lateralMenu)
        
        setupBindings()
        showEmptyScreen(false)
        collectionNode.view.backgroundView = emptyNode.view
        
        collectionNode.frame.origin.y = 180
        
        emptyNode.actionButtonTapped = { [weak self] in
            guard let tabBarController = self?.tabBarController as? ResidentTabBarViewController else { return }
            tabBarController.switchTo(controllerType: .chat)
        }
        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)
        collectionNode.view.addSubview(refreshControl)

        refreshItems()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    @objc func tappedButtonActivator() {
        AppState.switchTo(viewController: MyBuildingViewController())
    }
    
    @objc func refreshItems() {
        dataSource.loadMoreContentIfNeeded()
    }

    func setupBindings() {
        dataSource.$items
                .receive(on: DispatchQueue.main)
                .sink { [weak self] items in
                    self?.reloadData()
                }.store(in: &subscriptions)

        dataSource.$hasNoItemsAfterLoading
                .receive(on: DispatchQueue.main)
                .sink { [weak self] hasNoItemsAfterLoading in
                    self?.showEmptyScreen(hasNoItemsAfterLoading)
                }.store(in: &subscriptions)

        dataSource.$showLoadingIndicator
                .receive(on: DispatchQueue.main)
                .sink { [weak self] loading in
                    guard let self = self else { return }

                    if loading {
                        if !self.refreshControl.isRefreshing {
                            SVProgressHUD.show()
                        }
                    } else {
                        SVProgressHUD.dismiss()
                        self.refreshControl.endRefreshing()
                    }
                }.store(in: &subscriptions)
    }

    func showEmptyScreen(_ show: Bool) {
        emptyNode.isHidden = !show
    }

    func reloadData() {
        let source = items
        let target = dataSource.items

        let stagedChangeSet = StagedChangeset(source: source, target: target)
        for changeSet in stagedChangeSet {
            items = changeSet.data
            collectionNode.performDiffBatchUpdates(changeSet: changeSet)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        items.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let amenity = dataSource.items[indexPath.item]
        return {
            ConciergeThingsToKnowListViewController.HorizontalModeCell(title: amenity.name, body: amenity.teaser, url: amenity.articleThumbnailURL)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        let amenity = dataSource.items[indexPath.item]
        navigationController?.pushViewController(AmenityViewController(amenity: amenity), animated: true)
    }

    func collectionView(_ collectionView: ASCollectionView, willDisplayNodeForItemAt indexPath: IndexPath) {
        dataSource.loadMoreContentIfNeeded(currentItem: dataSource.items[indexPath.item])
    }
}
