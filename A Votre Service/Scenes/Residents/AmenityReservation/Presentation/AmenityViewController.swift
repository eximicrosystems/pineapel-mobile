//
// Created by Andrei Stoicescu on 18/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import PromiseKit
import SwiftyUserDefaults

class AmenityViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

    var collectionNode: ASCollectionNode {
        node as! ASCollectionNode
    }

    var amenity: Amenity
    var navigator: AmenityReservationNavigator?

    init(amenity: Amenity) {
        self.amenity = amenity
        let collectionNode = ASCollectionNode.avsCollection()
        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()
        navigationItem.title = "Amenity Details"
    }

    func editReservation() {
        navigator = AmenityReservationNavigator(amenity: amenity, reservation: nil)
        navigator?.present(in: navigationController ?? self) { [weak self] in
            self?.navigator = nil
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        1
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        {
            let cell = AmenityReservationNodes.AmenityCell(amenity: self.amenity)
            cell.checkAvailability = { [weak self] in
                self?.editReservation()
            }
            return cell
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }
}
