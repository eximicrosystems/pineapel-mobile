//
// Created by Andrei Stoicescu on 26/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit

struct AmenityReservationNodes {}

extension AmenityReservationNodes {
    class EditSummaryLineNode: ASControlNode {
        var leftLabelNode: ASTextNode2
        var rightLabelNode: ASTextNode2
        var rightIconNode: ASImageNode?

        init(leftLabel: String, rightLabel: String?, rightIcon: UIImage? = nil) {
            leftLabelNode = .init(with: leftLabel.attributed(.bodyTitleHeavy, .primaryLabel))
            rightLabelNode = .init(with: rightLabel?.attributed(.bodyTitleMedium, .primaryLabel))

            if rightIcon != nil {
                rightIconNode = .init(image: rightIcon?.withRenderingMode(.alwaysTemplate))
                rightIconNode?.tintColor = AVSColor.primaryIcon.color
            }

            super.init()
            borderWidth = 1
            borderColor = AVSColor.inputBorder.color.cgColor
            cornerRadius = 4
            automaticallyManagesSubnodes = true
        }

        override func asyncTraitCollectionDidChange(withPreviousTraitCollection previousTraitCollection: ASPrimitiveTraitCollection) {
            super.asyncTraitCollectionDidChange(withPreviousTraitCollection: previousTraitCollection)
            if asyncTraitCollection().userInterfaceStyle != previousTraitCollection.userInterfaceStyle {
                borderColor = AVSColor.inputBorder.color.cgColor
            }
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            [
                leftLabelNode.filling(),
                [rightLabelNode, rightIconNode].hStacked().spaced(10).shrinking()
            ].hStacked().spaced(10)
             .margin(10)
        }
    }

    class PriceImagePager: ASDisplayNode {
        var pager: ImagePager
        var priceBar: AVSPriceBar

        init(images: [URL], ratio: CGFloat = 0.7, priceBarItem: AVSPriceBar.Item) {
            pager = .init(images: images, ratio: ratio)
            priceBar = .init(item: priceBarItem)
            super.init()
            automaticallyManagesSubnodes = true
        }

        init(amenity: Amenity, ratio: CGFloat = 0.7) {
            pager = .init(images: amenity.images ?? [], ratio: ratio)
            priceBar = .init(amenity: amenity)
            super.init()
            automaticallyManagesSubnodes = true
        }

        convenience init(product: ProductProtocol, ratio: CGFloat = 0.7) {
            self.init(
                    images: [product.productImageURL].compactMap { $0 },
                    priceBarItem: .init(title: product.name, price: product.price, per: nil)
            )
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            priceBar.margin(top: CGFloat.infinity, left: 20, bottom: 20 + pager.bottomOffset, right: 20)
                    .overlay(pager)
        }
    }

    class ImagePager: ASDisplayNode, ASPagerDataSource, ASPagerDelegate {
        var pageControlNode: ASDisplayNode?
        var pageControl: UIPageControl?
        var thumbnailCollection = ASPagerNode()
        var images: [URL] = []
        var ratio: CGFloat
        var bottomOffset: CGFloat {
            images.count > 1 ? 10 : 0
        }

        init(images: [URL]?, ratio: CGFloat = 1) {
            self.images = images ?? []
            self.ratio = ratio

            super.init()

            if self.images.count > 1 {
                pageControlNode = ASDisplayNode { [weak self] () -> UIView in
                    let pageControl = UIPageControl()
                    pageControl.pageIndicatorTintColor = AVSColor.pageIndicatorTintColor.color
                    pageControl.currentPageIndicatorTintColor = AVSColor.currentPageIndicatorTintColor.color
                    pageControl.numberOfPages = self?.images.count ?? 0
                    self?.pageControl = pageControl
                    return pageControl
                }
            }

            thumbnailCollection.cornerRadius = 4
            thumbnailCollection.clipsToBounds = true

            thumbnailCollection.setDataSource(self)
            thumbnailCollection.setDelegate(self)

            automaticallyManagesSubnodes = true
        }

        lazy var gradient: CAGradientLayer = {
            let gradient = CAGradientLayer()
            gradient.type = .axial
            gradient.colors = [
                UIColor.black.withAlphaComponent(0).cgColor,
                UIColor.black.withAlphaComponent(0.2).cgColor,
                UIColor.black.withAlphaComponent(0.7).cgColor,
            ]
            gradient.locations = [0, 0.8, 1]
            return gradient
        }()

        override func layout() {
            super.layout()
            gradient.frame = thumbnailCollection.bounds
            gradient.cornerRadius = 4
            layer.addSublayer(gradient)
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            let thumb: ASLayoutElement = ratio == 0 ? thumbnailCollection : thumbnailCollection.ratio(ratio)

            return [
                thumb.growing(),
                pageControlNode?.height(.points(bottomOffset))
            ].vStacked().spaced(10)
        }

        func numberOfPages(in pagerNode: ASPagerNode) -> Int {
            images.count
        }

        func pagerNode(_ pagerNode: ASPagerNode, nodeBlockAt index: Int) -> ASCellNodeBlock {
            let imageURL = images[index]
            return {
                let imageNode = ASNetworkImageNode()
                imageNode.url = imageURL
                imageNode.contentMode = .scaleAspectFill

                let cell = ASCellNode()
                cell.automaticallyManagesSubnodes = true
                cell.layoutSpecBlock = { node, range in
                    imageNode.wrapped()
                }
                return cell
            }
        }

        func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
            pageControl?.currentPage = thumbnailCollection.currentPageIndex
        }
    }

    class AmenityCell: ASCellNode {
        var backgroundNode = ASDisplayNode()
        var title = ASTextNode2()
        var body = ASTextNode2()
        var checkAvailabilityButton: ASButtonNode
        var thumbnailPager: ImagePager

        var checkAvailability: (() -> Void)?

        var amenity: Amenity

        init(amenity: Amenity) {
            self.amenity = amenity
            thumbnailPager = ImagePager(images: amenity.images, ratio: 0.7)
            checkAvailabilityButton = .primaryButton(title: "Check Availability")
            body.attributedText = amenity.articleBody?.html?.string.attributed(.body, .primaryLabel)
            title.attributedText = amenity.articleTitle?.attributed(.bodyTitleHeavy, .primaryLabel)
            backgroundNode.backgroundColor = AVSColor.secondaryBackground.color

            super.init()

            checkAvailabilityButton.addTarget(self, action: #selector(tappedCheckAvailability), forControlEvents: .touchUpInside)
            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            var stackItems: [ASLayoutElement] = [
                thumbnailPager.margin(bottom: 20),
                title,
                body.margin(bottom: 20)
            ]

            if amenity.requiresReservation {
                checkAvailabilityButton.style.height = ASDimensionMakeWithPoints(50)
                stackItems.append(checkAvailabilityButton)
            }

            return ASStackLayoutSpec(direction: .vertical,
                                     spacing: 10,
                                     justifyContent: .start,
                                     alignItems: .stretch,
                                     children: stackItems)
                    .margin(20)
                    .background(backgroundNode)
        }

        @objc func tappedCheckAvailability() {
            checkAvailability?()
        }
    }

    class PreviewCell: ASCellNode {
        var backgroundNode = ASDisplayNode()
        var title = ASTextNode2()
        var body = ASTextNode2()
        var thumbnailPager: ImagePager
        var reservationSummary: ConciergePagerListNodes.SummaryNode

        init(amenity: Amenity, reservation: AmenityReservation) {
            thumbnailPager = ImagePager(images: amenity.images, ratio: 0.7)

            let dateString = reservation.dateString(amenity: amenity)
            let timeString = reservation.timeString(amenity: amenity)

            reservationSummary = ConciergePagerListNodes.SummaryNode(config: .matrix(
                    [
                        [
                            .init(title: "Date Reserved", subtitle: dateString),
                            .init(title: "Time", subtitle: timeString)
                        ],
                        [.init(title: "Duration", subtitle: reservation.durationString)],
                        [.init(title: "Note", subtitle: reservation.note)],
                        [
                            .init(title: "Guest Name", subtitle: reservation.guestName),
                            .init(title: "Guest Phone", subtitle: reservation.guestPhone)
                        ],
                        [.init(title: "Open Hours", subtitle: amenity.openHoursStrings.joined(separator: "\n"))]
                    ], skipBlankSubtitles: true))

            title.attributedText = [amenity.articleTitle, "Reservation"].compactMap { $0 }.joined(separator: " ").attributed(.bodyTitleHeavy, .primaryLabel)
            body.attributedText = amenity.articleBody?.html?.string.attributed(.bodyRoman, .primaryLabel)
            backgroundNode.backgroundColor = AVSColor.secondaryBackground.color

            super.init()
            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            [
                thumbnailPager,
                title,
                reservationSummary,
                body
            ].vStacked(spacing: 20)
             .margin(20)
             .background(backgroundNode)
        }
    }
}
