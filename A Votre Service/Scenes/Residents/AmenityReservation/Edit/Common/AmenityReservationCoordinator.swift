//
// Created by Andrei on 15.04.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI

protocol Navigator {
    associatedtype Destination
    func navigate(to destination: Destination)
}

class PaymentReviewNavigator: Navigator, DiscountCodesSelectable {
    enum Destination {
        case discountCodes([String])
    }

    weak var navigationController: UINavigationController?
    weak var paymentReviewViewController: AmenityPaymentReviewViewController?

    init(navigationController: UINavigationController?, paymentReviewViewController: AmenityPaymentReviewViewController) {
        self.navigationController = navigationController
        self.paymentReviewViewController = paymentReviewViewController
    }

    func navigate(to destination: Destination) {
        navigationController?.pushViewController(makeViewController(for: destination), animated: true)
    }

    private func makeViewController(for destination: Destination) -> UIViewController {
        switch destination {
        case .discountCodes(let codes):
            return UIHostingController(rootView: DiscountCodesFormView(codes: codes.count > 0 ? codes : [""], delegate: self))
        }
    }

    func dismissCodesDidFinish(_ codes: [String]) {
        guard let vc = paymentReviewViewController else { return }
        navigationController?.popToViewController(vc, animated: true)
        vc.updateCodes(codes)
    }
}

class CartNavigator: Navigator {
    enum Destination {
        case cart
        case category(ALaCarteCategory)
        case products(ALaCarteCategory)
    }

    weak var navigationController: UINavigationController?
    var cart: CartProductManager

    init(navigationController: UINavigationController?, cart: CartProductManager) {
        self.navigationController = navigationController
        self.cart = cart
    }

    func navigate(to destination: Destination) {
        navigationController?.pushViewController(makeViewController(for: destination), animated: true)
    }

    private func makeViewController(for destination: Destination) -> UIViewController {
        switch destination {
        case .cart:
            return CartViewController(cartManager: cart)
        case .products(let category):
            return ProductListViewController(cartManager: cart, category: category, emptyNodeAction: false)
        case .category(let category):
            return ALaCarteCategoriesViewController(cartManager: cart, items: category.children)
        }
    }
}

class AmenityReservationNavigator: Navigator {

    class Form {
        var startDate: Date? = nil
        var endDate: Date? = nil
        var comment: String? = nil
        var guestName: String? = nil
        var guestPhone: String? = nil
    }

    enum Destination {
        case dateSelection
        case timeSlots(day: Date)
        case additionalInfo(startDate: Date, endDate: Date)
        case upsell(cart: CartProductManager)
        case terms
        case amenitySummary
        case thankYou(params: AmenityReservation)
        case paymentSelection
        case paymentReview(data: AmenityCheckoutData)
    }

    private weak var navigationController: AVSNavigationController?

    var amenity: Amenity
    var reservation: AmenityReservation?
    var form = Form()
    var checkoutData: AmenityCheckoutData?

    var showUpsell: Bool { !amenity.isFree && amenity.upsellCategories.count > 0 }
    var showTerms: Bool { amenity.requiresTerms }

    lazy var cartUpsellManager: CartProductManager = {
        let config = CartProductManager.Config()
        config.openCartModal = false
        config.cartEmptyCloseButton = false
        config.cartPrimaryLabel = "Continue"
        config.cartSecondaryLabel = "Skip"
        return .init(config: config)
    }()

    init(amenity: Amenity, reservation: AmenityReservation?) {
        self.amenity = amenity
        self.reservation = reservation

        cartUpsellManager.config.cartExternalSecondaryAction = { [weak self] in
            self?.upsellDidFinish(skip: true)
        }

        cartUpsellManager.config.cartExternalPrimaryAction = { [weak self] in
            self?.upsellDidFinish(skip: false)
        }
    }

    // MARK: - Navigator
    func navigate(to destination: Destination) {
        navigationController?.pushViewController(makeViewController(for: destination), animated: true)
    }

    func present(in viewController: UIViewController, dismiss: (() -> ())?) {
        let navController = AVSNavigationController(rootViewController: initialViewController())
        navigationController = navController
        navController.isModalInPresentation = true

        navController.dismissNavigation = { [weak viewController] in
            viewController?.dismiss(animated: true) {
                dismiss?()
            }
        }

        viewController.present(navController, animated: true)
    }

    func initialViewController() -> UIViewController {
        if amenity.priceType == .day {
            return AmenityDateSlotsViewController(navigator: self)
        } else {
            return AmenityDateSelectionViewController(navigator: self)
        }
    }

    // MARK: - Private
    private func makeViewController(for destination: Destination) -> UIViewController {
        switch destination {
        case .dateSelection:
            return initialViewController()
        case .timeSlots(let date):
            return AmenityTimeSlotsViewController(day: date, navigator: self)
        case .additionalInfo(let startDate, let endDate):
            return AmenityAdditionalInfoViewController(startDate: startDate, endDate: endDate, navigator: self)
        case .upsell(let cart):
            return UpsellCategoriesViewController(cartManager: cart, items: amenity.upsellCategories, navigator: self)
        case .terms:
            return AmenityTermsViewController(terms: amenity.termsBody, navigator: self)
        case .amenitySummary:
            return AmenitySummaryViewController(navigator: self)
        case .thankYou(let params):
            let thankYouVC = AmenityThankYouViewController(amenity: amenity, reservation: params)
            thankYouVC.onTapDone = { [weak self] in
                self?.navigationController?.dismissNavigation?()
            }
            return thankYouVC
        case .paymentSelection:
            return AmenityPaymentViewController(navigator: self)
        case .paymentReview(let checkoutData):
            return AmenityPaymentReviewViewController(navigator: self, checkoutData: checkoutData)
        }
    }

    func additionalInfoDidFinish(guestName: String?, guestPhone: String?, comment: String?) {
        form.guestName = guestName
        form.guestPhone = guestName
        form.comment = comment

        if showUpsell {
            navigate(to: .upsell(cart: cartUpsellManager))
        } else if showTerms {
            navigate(to: .terms)
        } else {
            navigate(to: .amenitySummary)
        }
    }

    func upsellDidFinish(skip: Bool) {
        cartUpsellManager.config.skipProducts = skip

        if showTerms {
            navigate(to: .terms)
        } else {
            navigate(to: .amenitySummary)
        }
    }

    func termsFinished() {
        navigate(to: .amenitySummary)
    }

    func timeSlotsDidFinish(startDate: Date, endDate: Date) {
        form.startDate = startDate
        form.endDate = endDate

        navigate(to: .additionalInfo(startDate: startDate, endDate: endDate))
    }

    func summaryDidFinish(params: AmenityReservation) {
        if amenity.isFree {
            navigate(to: .thankYou(params: params))
        } else {
            checkoutData = AmenityCheckoutData(reservation: params, amenity: amenity, cart: cartUpsellManager)
            navigate(to: .paymentSelection)
        }
    }

    func paymentSelectionDidFinish(_ method: PaymentMethodSelection) {
        guard let checkoutData = checkoutData else { return }
        checkoutData.paymentMethod = method
        navigate(to: .paymentReview(data: checkoutData))
    }

    func paymentReviewDidFinish() {
        guard let checkoutData = checkoutData else { return }
        navigate(to: .thankYou(params: checkoutData.reservation))
    }

    func dateSelectionDidFinish(day: Date) {
        navigate(to: .timeSlots(day: day))
    }

    func dateRangeSelectionDidFinish(startDate: Date, endDate: Date) {
        form.startDate = startDate
        form.endDate = endDate

        navigate(to: .additionalInfo(startDate: startDate, endDate: endDate))
    }
}