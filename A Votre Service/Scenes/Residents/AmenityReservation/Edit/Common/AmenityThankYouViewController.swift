//
// Created by Andrei Stoicescu on 07/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit

class AmenityThankYouViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

    class ThankYouCell: ASCellNode {

        var icon: ASImageNode
        var titleLabel: ASTextNode2
        var bodyLabel: ASTextNode2

        init(title: String, body: String) {
            icon = .init(image: Asset.Images.appLogoNoText.image.withRenderingMode(.alwaysTemplate), tintColor: AVSColor.primaryLabel.color)
            titleLabel = .init(with: title.attributed(.H2Black, .primaryLabel, alignment: .center))
            bodyLabel = .init(with: body.attributed(.body, .primaryLabel, alignment: .center))
            super.init()
            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            [icon.size(127 / 2, 158 / 2), titleLabel, bodyLabel].vStacked().aligned(.center).spaced(30).margin(horizontal: 30).margin(vertical: 40)
        }

    }

    class ThankYouDoneCell: ASCellNode {
        var button: ASButtonNode

        override init() {
            button = .primaryButton(title: "Done")
            button.isUserInteractionEnabled = false
            super.init()
            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            button.height(.points(50)).margin(horizontal: 15)
        }
    }

    var onTapDone: (() -> Void)?

    var amenity: Amenity
    var reservation: AmenityReservation
    var collectionNode: ASCollectionNode

    init(amenity: Amenity, reservation: AmenityReservation) {
        self.amenity = amenity
        self.reservation = reservation
        collectionNode = .avsCollection()
        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionNode.contentInset = UIEdgeInsets(top: view.safeAreaInsets.top + 15, left: 0, bottom: view.safeAreaInsets.bottom + 15, right: 0)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        2
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        {
            if indexPath.item == 0 {

                var body = "Your booking has been confirmed"

                if let amenityName = self.amenity.name {
                    body += " for \(amenityName)"
                }

                if let dateString = self.reservation.dateString(amenity: self.amenity) {
                    body += " on \(dateString)"
                }

                if let timeString = self.reservation.timeString(amenity: self.amenity) {
                    body += "at \(timeString)"
                }

                body += "."

                return ThankYouCell(title: "Thank you for your reservation!", body: body)
            } else {
                return ThankYouDoneCell()
            }
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        if indexPath.item == 1 {
            onTapDone?()
        }
    }
}
