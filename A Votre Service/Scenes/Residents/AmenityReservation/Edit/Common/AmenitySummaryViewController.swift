//
// Created by Andrei Stoicescu on 06/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import PromiseKit

class AmenitySummaryViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {
    enum CloseAction {
        case cancel
        case reservationConfirmed
        case reservationUpdated
    }

    enum Item {
        case summary(Summary)
        case label(String)
    }

    struct Summary {
        let label: String
        let value: String?
    }

    var amenity: Amenity
    var startDate: Date?
    var endDate: Date?
    var selectionBar: AmenitySelectionBar
    var collectionNode: ASCollectionNode
    var priceFetcher: AmenityPriceFetcher
    var user: User?
    var price: AmenityPriceFetcher.Price?

    var onClose: ((CloseAction) -> ())?

    var items = [Item]()

    var comment: String?
    var guestName: String?
    var guestPhone: String?
    var reservation: AmenityReservation?
    var navigator: AmenityReservationNavigator
    var cartUpsellManager: CartProductManager

    init(navigator: AmenityReservationNavigator) {
        amenity = navigator.amenity
        reservation = navigator.reservation
        startDate = navigator.form.startDate
        endDate = navigator.form.endDate
        comment = navigator.form.comment
        guestName = navigator.form.guestName
        guestPhone = navigator.form.guestPhone
        cartUpsellManager = navigator.cartUpsellManager
        self.navigator = navigator
        priceFetcher = AmenityPriceFetcher(amenity: amenity)

        let collectionNode = ASCollectionNode.avsCollection()
        collectionNode.backgroundColor = AVSColor.secondaryBackground.color

        self.collectionNode = collectionNode

        let node = ASDisplayNode()
        node.automaticallyManagesSubnodes = true

        let pager = AmenityReservationNodes.PriceImagePager(amenity: amenity, ratio: 0.5)

        let selectionBarTitle: String
        if reservation != nil {
            selectionBarTitle = "Update Reservation"
        } else {
            selectionBarTitle = amenity.isFree ? "Reserve" : "Reserve & Pay"
        }

        let selectionBar = AmenitySelectionBar(buttonTitle: selectionBarTitle)
        self.selectionBar = selectionBar

        super.init(node: node)

        collectionNode.delegate = self
        collectionNode.dataSource = self

        selectionBar.tappedButton = { [weak self] in
            guard let self = self else { return }

            let params: AmenityReservation

            if self.amenity.priceType == .day {
                params = .buildDailyReservation(amenityID: self.amenity.amenityID,
                                                     note: self.comment,
                                                     startDate: self.startDate,
                                                     endDate: self.endDate,
                                                     guestName: self.guestName,
                                                     guestPhone: self.guestPhone)
            } else {
                params = .buildSlotReservation(amenityID: self.amenity.amenityID,
                                                    note: self.comment,
                                                    startTime: self.startDate,
                                                    endTime: self.endDate)
            }

            if self.reservation != nil {
                // Edit mode
                self.updateReservation(with: params)
            } else {
                // Create
                if self.amenity.isFree {
                    self.createFreeReservation(params)
                } else {
                    self.authenticateForPaidReservationIfNeeded(params)
                }
            }
        }

        node.layoutSpecBlock = { node, range in
            [
                pager.margin(15).margin(top: node.safeAreaInsets.top),
                self.collectionNode.growing(),
                selectionBar
            ].vStacked()
        }
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func buildItemsReport() -> [Item] {
        var items = [Item]()

        if let user = user {
            items.append(contentsOf: [
                Item.label("Reservation Info"),
                .summary(Summary(label: "Apartment", value: user.apartmentNo)),
                .summary(Summary(label: "Name", value: user.fullName)),
                .summary(Summary(label: "Phone", value: user.phoneNumber))
            ])
        }

        if amenity.priceType == .day {
            if startDate == endDate {
                items.append(.summary(Summary(label: "Date", value: startDate?.avsDateString())))
            } else {
                items.append(contentsOf: [
                    .summary(Summary(label: "From", value: startDate?.avsDateString())),
                    .summary(Summary(label: "To", value: endDate?.avsDateString()))
                ])
            }

            items.append(.summary(Summary(label: "Guest Name", value: guestName)))
            items.append(.summary(Summary(label: "Guest Phone", value: guestPhone)))

        } else {
            items.append(contentsOf: [
                .summary(Summary(label: "Date", value: startDate?.avsDateString())),
                .summary(Summary(label: "Time", value: "\(startDate?.avsTimeString() ?? "") to \(endDate?.avsTimeString() ?? "")"))
            ])
        }

        items.append(.summary(Summary(label: "Note", value: comment)))

        if let priceItems = price?.items {
            items.append(.label("Reservation Items"))
            let summaryItems = priceItems.map { el -> Item in
                Item.summary(Summary(label: el.itemType == .aLaCarte ? el.nameWithQuantity : el.name,
                                     value: el.totalPriceWithoutTax.currencyDollarClean))
            }

            items.append(contentsOf: summaryItems)
        }

        return items
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()
        view.backgroundColor = AVSColor.secondaryBackground.color
        navigationController?.isModalInPresentation = true
        navigationItem.title = "Reservation"

        SVProgressHUD.show()

        ResidentService().getUser().done { [weak self] user in
            SVProgressHUD.dismiss()
            guard let self = self else { return }
            self.user = user
            self.items = self.buildItemsReport()
            self.collectionNode.reloadData()
        }.catch { error in
            Log.thisError(error)
            SVProgressHUD.showError(withStatus: error.localizedDescription)
        }

        if amenity.isFree {
            selectionBar.state = .selected("Free")
        } else {
            guard let startDate = startDate, let endDate = endDate else { return }
            let products = cartUpsellManager.config.skipProducts ? [] : cartUpsellManager.products
            priceFetcher.getPrice(query: .init(startTime: startDate, endTime: endDate, discountCodes: nil, products: products)).done { [weak self] price in
                guard let self = self else { return }
                self.price = price
                if self.priceFetcher.currentQuery == price.query {
                    self.selectionBar.state = .selected("Total Fee: \(price.priceString)")
                    self.price = price
                    self.items = self.buildItemsReport()
                    self.collectionNode.reloadData()
                }
            }.catch { error in
                Log.thisError(error)
            }
        }

    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        items.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let item = items[indexPath.item]
        return {
            switch item {
            case .summary(let summary):
                let lineNode = AmenityReservationNodes.EditSummaryLineNode(leftLabel: summary.label, rightLabel: summary.value)
                let cell = ASCellNode()
                cell.automaticallyManagesSubnodes = true
                cell.layoutSpecBlock = { node, range in
                    lineNode.margin(.init(top: 0, left: 15, bottom: 5, right: 15))
                }
                return cell
            case .label(let label):
                return FormNodes.SectionLabel(label: label)
            }
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }

    func updateReservation(with params: AmenityReservation) {
        guard let reservationID = reservation?.reservationID else { return }

        var reservation = params
        reservation.reservationID = reservationID
        let params = AmenityReservationRequest.withReservation(reservation)
        SVProgressHUD.show()
        ResidentService().updateAmenityReservation(reservationID: reservationID, params: params).done { [weak self] response in
            SVProgressHUD.dismiss()
            self?.navigator.summaryDidFinish(params: reservation)
        }.catch { error in
            Log.thisError(error)
            SVProgressHUD.showError(withStatus: error.localizedDescription)
        }
    }

    func createFreeReservation(_ params: AmenityReservation) {
        SVProgressHUD.show()
        let reservationParams = AmenityReservationRequest.withReservation(params)

        ResidentService().createAmenityReservation(params: reservationParams).done { [weak self] response in
            SVProgressHUD.dismiss()
            self?.navigator.summaryDidFinish(params: params)
        }.catch { error in
            Log.thisError(error)
            SVProgressHUD.showError(withStatus: error.localizedDescription)
        }
    }

    func authenticateForPaidReservationIfNeeded(_ params: AmenityReservation) {
        let biometrics = Biometrics()
        if biometrics.shouldAuthenticate {
            biometrics.authenticate().done { [weak self] in
                self?.navigator.summaryDidFinish(params: params)
            }.catch { error in
                SVProgressHUD.dismiss()
            }
        } else {
            navigator.summaryDidFinish(params: params)
        }
    }
}
