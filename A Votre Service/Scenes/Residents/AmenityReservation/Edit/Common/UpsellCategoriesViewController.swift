//
// Created by Andrei on 12.04.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import Combine
import DifferenceKit

class UpsellCategoriesViewController: ASDKViewController<ASDisplayNode> {

    enum Sections: Int, CaseIterable, Differentiable {
        case header
        case items
        case continueButton
        case skipButton
    }

    enum DismissMode {
        case skip
        case products
    }

    var collectionNode: ASCollectionNode

    var badgeLabel = UIBadgeLabel(image: Asset.Images.icCart.image)
    var cartManager: CartProductManager

    lazy var emptyNode = EmptyNode(emptyNodeType: .productList, showAction: false)

    private var subscriptions = Set<AnyCancellable>()
    private var sections = [ArraySection<Sections, AnyDifferentiable>]()

    var items = [ALaCarteCategory]()
    var amenityReservationNavigator: AmenityReservationNavigator
    var cartNavigator: CartNavigator?

    init(cartManager: CartProductManager, items: [ALaCarteCategory], navigator: AmenityReservationNavigator) {
        amenityReservationNavigator = navigator
        self.items = items
        self.cartManager = cartManager
        collectionNode = .avsCollection()
        super.init(node: collectionNode)

        collectionNode.showsVerticalScrollIndicator = false
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func updateBadgeLabel() {
        badgeLabel.label.isHidden = cartManager.totalProductsCount == 0
        badgeLabel.label.text = cartManager.totalProductsCountString
    }

    func reloadData() {
        let headerSectionItems = [AnyDifferentiable("header")]
        let items = self.items.map { AnyDifferentiable($0) }
        let continueButtonItems: [AnyDifferentiable] = cartManager.products.count > 0 ? [AnyDifferentiable("continue")] : []
        let skipButtonItems = [AnyDifferentiable("skip")]

        let target: [ArraySection<Sections, AnyDifferentiable>] = [
            ArraySection(model: .header, elements: headerSectionItems),
            ArraySection(model: .items, elements: items),
            ArraySection(model: .continueButton, elements: continueButtonItems),
            ArraySection(model: .skipButton, elements: skipButtonItems)
        ]

        let stagedChangeSet = StagedChangeset(source: sections, target: target)
        for changeSet in stagedChangeSet {
            sections = changeSet.data
            collectionNode.performDiffBatchUpdates(changeSet: changeSet)
        }
    }

    @objc func viewCartAction() {
        cartNavigator?.navigate(to: .cart)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()
        navigationItem.title = L10n.ALaCarte.Navigation.title

        cartNavigator = .init(navigationController: navigationController, cart: cartManager)

        cartManager.$totalProductsCount.receive(on: DispatchQueue.main)
                                       .sink { [weak self] count in
                                           self?.updateBadgeLabel()
                                           self?.reloadData()
                                       }.store(in: &subscriptions)

        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: badgeLabel)
        badgeLabel.tappedButton = { [weak self] in
            self?.viewCartAction()
        }

        updateBadgeLabel()

        collectionNode.view.backgroundView = emptyNode.view
        emptyNode.actionButtonTapped = { [weak self] in
            guard let tabBarController = self?.tabBarController as? ResidentTabBarViewController else { return }
            tabBarController.switchTo(controllerType: .chat)
        }

        showEmptyScreen(false)
        reloadData()
    }

    func showEmptyScreen(_ show: Bool) {
        emptyNode.isHidden = !show
    }
}

extension UpsellCategoriesViewController: ASCollectionDelegate, ASCollectionDataSource {
    typealias MenuItemCell = ALaCarteCategoriesViewController.MenuItemCell

    public func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
        sections.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        sections[section].elements.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let section = sections[indexPath.section]

        switch section.model {
        case .items:
            guard let items = section.elements.map(\.base) as? [ALaCarteCategory] else { return { ASCellNode() } }
            let itemType = items[indexPath.item]
            return {
                MenuItemCell(itemType: itemType)
            }
        case .header:
            return {
                ASCellNode.withNode(ASTextNode2(with: "Make your reservation better with these items.".attributed(.H4, .primaryLabel)), margin: UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20))
            }
        case .continueButton:
            return {
                ASCellNode.withNode(ASButtonNode.primaryButton(title: "Continue"), margin: UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20))
            }
        case .skipButton:
            return {
                ASCellNode.withNode(ASButtonNode.secondaryButton(title: "Skip"), margin: UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20))
            }
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        let section = sections[indexPath.section]

        switch section.model {
        case .items:
            guard let items = section.elements.map(\.base) as? [ALaCarteCategory] else { return }

            let category = items[indexPath.item]
            if category.children.count > 0 {
                cartNavigator?.navigate(to: .category(category))
            } else {
                cartNavigator?.navigate(to: .products(category))
            }
        case .skipButton:
            amenityReservationNavigator.upsellDidFinish(skip: true)
        case .continueButton:
            amenityReservationNavigator.upsellDidFinish(skip: false)
        case .header:
            ()
        }

    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        let section = sections[indexPath.section]
        switch section.model {
        case .items:
            return .halfWidth(collectionNode: collectionNode, right: indexPath.row % 2 == 0)
        default:
            return .fullWidth(collectionNode: collectionNode)
        }
    }
}

