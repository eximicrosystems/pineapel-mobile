//
// Created by Andrei Stoicescu on 15/10/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit

class AmenityTermsViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

    var selectionBar: AmenitySelectionBar
    var collectionNode: ASCollectionNode

    var terms: String?

    init(terms: String?, navigator: AmenityReservationNavigator) {
        self.terms = terms
        let collectionNode = ASCollectionNode.avsCollection()
        collectionNode.contentInset = .collectionBottomInset
        collectionNode.backgroundColor = AVSColor.secondaryBackground.color
        self.collectionNode = collectionNode

        let node = ASDisplayNode()
        node.automaticallyManagesSubnodes = true

        let selectionBar = AmenitySelectionBar(buttonTitle: "Agree")
        selectionBar.state = .selected("Terms and Conditions")
        self.selectionBar = selectionBar

        super.init(node: node)

        collectionNode.delegate = self
        collectionNode.dataSource = self

        selectionBar.tappedButton = {
            navigator.termsFinished()
        }

        node.layoutSpecBlock = { node, range in
            [collectionNode.growing(), selectionBar].vStacked()
        }
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()
        view.backgroundColor = AVSColor.primaryBackground.color
        navigationController?.isModalInPresentation = true
        navigationItem.title = "Terms and Conditions"
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        1
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let terms = self.terms?.html

        return {
            let textNode = ASTextNode2(with: terms)

            let cellNode = ASCellNode()
            cellNode.automaticallyManagesSubnodes = true
            cellNode.layoutSpecBlock = { node, range in
                textNode.margin(20)
            }
            return cellNode
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }
}
