//
// Created by Andrei Stoicescu on 17/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import Stripe
import SVProgressHUD
import PromiseKit
import PassKit
import SwiftUI
import DifferenceKit

class AmenityPaymentReviewManager {

    enum ReviewProduct: Hashable, Differentiable {
        case titleSubtitle(title: String?, subtitle: String?, rightLabel: String? = nil)
        case paymentMethod(method: PaymentMethodSelection)
        case discountCodes(codes: [String])
        case cartProduct(product: CartProduct)

        func hash(into hasher: inout Hasher) {
            switch self {
            case .titleSubtitle(let title, let subtitle, let rightLabel):
                hasher.combine([title, subtitle, rightLabel].compactMap { $0 }.joined(separator: ""))
            case .paymentMethod(let method):
                hasher.combine(method.typeString ?? "")
            case .discountCodes(let codes):
                hasher.combine(codes.joined(separator: ""))
            case .cartProduct(let product):
                hasher.combine(product.id)
            }
        }
    }

    var items = [ReviewProduct]()

    var checkoutData: AmenityCheckoutData

    var user: User? { didSet { rebuildItems() } }
    var orderItems = [OrderResponseItem]() { didSet { rebuildItems() } }
    var discountCodes = [String]() { didSet { rebuildItems() } }
    var cartProducts = [CartProduct]() { didSet { rebuildItems() } }

    var invalidDiscountCodes: [String] {
        discountCodes.filter { code in
            orderItems.first { $0.discountCode?.lowercased() == code.lowercased() } == nil
        }
    }

    private var createdAmenityReservation: AmenityReservation?

    static func reservationDateString(reservation: AmenityReservation, amenity: Amenity) -> String? {
        if amenity.priceType == .day {
            guard let startDate = reservation.startDate?.avsDateString(),
                  let endDate = reservation.endDate?.avsDateString() else { return nil }

            return "\(startDate) - \(endDate)"
        } else {
            guard let date = reservation.startTime?.avsDateString(),
                  let startTime = reservation.startTime?.avsTimeString(),
                  let endTime = reservation.endTime?.avsTimeString() else { return nil }

            return "\(date) - \(startTime) to \(endTime)"
        }
    }

    init(checkoutData: AmenityCheckoutData) {
        self.checkoutData = checkoutData
        rebuildItems()
    }

    func rebuildItems() {
        var items = [ReviewProduct]()

        let reservationDate = Self.reservationDateString(reservation: checkoutData.reservation, amenity: checkoutData.amenity)
        let reservationPrice = orderItems.first(where: { $0.name == checkoutData.amenity.name })?.totalPriceWithoutTax.currencyDollarClean
        items.append(.titleSubtitle(title: checkoutData.amenity.name, subtitle: reservationDate, rightLabel: reservationPrice))

        let reviewProductOrderItems = orderItems.filter { $0.name != checkoutData.amenity.name }.map {
            ReviewProduct.titleSubtitle(title: $0.itemType == .aLaCarte ? $0.nameWithQuantity : $0.name,
                                        subtitle: nil,
                                        rightLabel: $0.totalPriceWithoutTax.currencyDollarClean)
        }

        items.append(contentsOf: reviewProductOrderItems)

        items.append(.discountCodes(codes: discountCodes))

        if let user = user {
            let reservationName = [user.firstName, user.lastName].compactMap({ $0 }).joined(separator: " ")
            items.append(.titleSubtitle(title: "Name", subtitle: reservationName))
        }

        if let note = checkoutData.reservation.note, note.count > 0 {
            items.append(.titleSubtitle(title: "Comment", subtitle: note))
        }

        if let guestName = checkoutData.reservation.guestName {
            items.append(.titleSubtitle(title: "Guest Name", subtitle: guestName))
        }

        if let guestPhone = checkoutData.reservation.guestPhone {
            items.append(.titleSubtitle(title: "Guest Phone", subtitle: guestPhone))
        }

        if let method = checkoutData.paymentMethod {
            items.append(.paymentMethod(method: method))
        }

        self.items = items
    }

    private func reserveIfNeeded() -> Promise<AmenityReservation> {
        if let existing = createdAmenityReservation {
            return .value(existing)
        } else {
            SVProgressHUD.show()
            let params = AmenityReservationRequest.withReservation(checkoutData.reservation)
            return ResidentService().createAndGetAmenityReservation(params: params).get {
                self.createdAmenityReservation = $0
                SVProgressHUD.dismiss()
            }
        }
    }

    func createOrder(stripePaymentMethodID: String) -> Promise<OrderClientSecretResponse> {
        reserveIfNeeded().then { reservation -> Promise<OrderClientSecretResponse> in
            var items = self.cartProducts.map(\.orderRequestItem)
            items.append(.amenity(id: self.checkoutData.amenity.amenityID, reservationID: reservation.reservationID))

            return ResidentService().createOrder(params: OrderRequestData(paymentMethodID: stripePaymentMethodID,
                                                                          items: items,
                                                                          discountCodes: self.discountCodes))
        }
    }
}

class AmenityPaymentReviewViewController: ASDKViewController<ASDisplayNode> {

    private typealias OrderSummary = PaymentSummary.OrderSummary

    private enum Sections: Int, CaseIterable, Differentiable {
        case items
        case summary
        case save
    }

    private var collectionNode: ASCollectionNode
    private var checkoutData: AmenityCheckoutData
    private var reviewManager: AmenityPaymentReviewManager
    private var summary: OrderSummary?

    private var _forceSaveButtonDisabled = false
    private var priceFetcher: AmenityPriceFetcher

    private var saveButtonDisabled: Bool {
        _forceSaveButtonDisabled || summary == nil
    }

    private var upsellCart: CartProductManager

    private var sections = [ArraySection<Sections, AnyDifferentiable>]()
    private var navigator: AmenityReservationNavigator
    private var paymentReviewNavigator: PaymentReviewNavigator?

    init(navigator: AmenityReservationNavigator, checkoutData: AmenityCheckoutData) {
        upsellCart = navigator.cartUpsellManager
        self.navigator = navigator
        self.checkoutData = checkoutData
        collectionNode = .avsCollection()
        priceFetcher = AmenityPriceFetcher(amenity: navigator.amenity)

        reviewManager = AmenityPaymentReviewManager(checkoutData: checkoutData)
        if !upsellCart.config.skipProducts {
            reviewManager.cartProducts = upsellCart.products
        }

        super.init(node: collectionNode)

        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        paymentReviewNavigator = .init(navigationController: navigationController, paymentReviewViewController: self)

        replaceSystemBackButton()
        navigationItem.title = L10n.AmenityListScreen.Navigation.title

        calculateOrder()

        ResidentService().getUser().done { user in
            self.reviewManager.user = user
            self.reloadData()
        }.cauterize()
    }

    func calculateOrder() {
        if let startDate = checkoutData.reservation.startDate ?? checkoutData.reservation.startTime,
           let endDate = checkoutData.reservation.endDate ?? checkoutData.reservation.endTime {
            priceFetcher.getPrice(query: .init(startTime: startDate, endTime: endDate, discountCodes: reviewManager.discountCodes, products: reviewManager.cartProducts)).done { price in
                self.summary = OrderSummary.fromResponses(price.items)
                self.reviewManager.orderItems = price.items
                self.reloadData()
                let invalidCodes = self.reviewManager.invalidDiscountCodes
                if invalidCodes.count > 0 {
                    SVProgressHUD.showError(withStatus: "The following discount \(invalidCodes.count == 1 ? "code doesn't" : "codes don't") apply to any item: \(invalidCodes.joined(separator: ", "))")
                }
            }.catch { error in
                SVProgressHUD.showError(withStatus: error.localizedDescription)
                Log.thisError(error)
            }
        }
    }

    func reloadData() {
        let items = reviewManager.items.map { AnyDifferentiable($0) }
        let summaryItems: [AnyDifferentiable] = summary != nil ? [AnyDifferentiable(summary!)] : []
        let saveItems: [AnyDifferentiable] = summary != nil ? [AnyDifferentiable("save")] : []

        let target: [ArraySection<Sections, AnyDifferentiable>] = [
            ArraySection(model: .items, elements: items),
            ArraySection(model: .summary, elements: summaryItems),
            ArraySection(model: .save, elements: saveItems),
        ]

        let stagedChangeSet = StagedChangeset(source: sections, target: target)
        for changeSet in stagedChangeSet {
            sections = changeSet.data
            collectionNode.performDiffBatchUpdates(changeSet: changeSet)
        }
    }

    func updateCodes(_ codes: [String]) {
        reviewManager.discountCodes = codes
        reloadData()
        calculateOrder()
    }
}

extension AmenityPaymentReviewViewController: ASCollectionDelegate, ASCollectionDataSource {
    func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
        sections.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        sections[section].elements.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let section = sections[indexPath.section]

        switch section.model {
        case .items:
            guard let product = section.elements[indexPath.item].base as? AmenityPaymentReviewManager.ReviewProduct else { return { ASCellNode() } }
            return {
                switch product {

                case .cartProduct(let product):
                    return GenericNodes.TitleSubtitleCell(title: product.nameWithQuantity, subtitle: nil, rightLabel: product.displayPrice)
                case .discountCodes(let codes):
                    return GenericNodes.TitleSubtitleCell(title: "Discount Codes",
                                                          subtitle: codes.count > 0 ? codes.joined(separator: ", ") : "Tap to Add Discount Codes")
                case .titleSubtitle(let title, let subtitle, let rightLabel):
                    return GenericNodes.TitleSubtitleCell(title: title, subtitle: subtitle, rightLabel: rightLabel)
                case .paymentMethod(let method):
                    return GenericNodes.TitleSubtitleCell(title: "Payment", subtitle: method.typeString)
                }
            }
        case .save:
            return { GenericNodes.PrimaryButtonCellNode(title: self.checkoutData.paymentMethod != nil ? "Pay Now" : "Continue") }
        case .summary:
            return { PaymentSummary.Cell(summary: self.summary!) }
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        let section = sections[indexPath.section]

        switch section.model {
        case .save:
            if !saveButtonDisabled {
                finalizeOrder()
            }
        case .items:
            guard let item = section.elements[indexPath.item].base as? AmenityPaymentReviewManager.ReviewProduct else { return }
            switch item {
            case .discountCodes(let codes):
                paymentReviewNavigator?.navigate(to: .discountCodes(codes))
            case .cartProduct:
                ()
            default:
                ()
            }
        default:
            ()
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }

    func finalizeOrder() {
        guard let paymentMethod = checkoutData.paymentMethod else {
            SVProgressHUD.showError(withStatus: "Payment method error")
            return
        }

        _forceSaveButtonDisabled = true

        switch paymentMethod {

        case .applePay:
            payWithApplePay()
        case .paymentMethod(let method):
            SVProgressHUD.show()
            reviewManager.createOrder(stripePaymentMethodID: method.stripeId).done { response in
                self.payWithCard(response: response)
            }.catch { error in
                self._forceSaveButtonDisabled = false
                SVProgressHUD.showError(withStatus: error.localizedDescription)
                Log.thisError(error)
            }
        }
    }

    func payWithApplePay() {
        guard let summary = summary else { return }

        let merchantIdentifier = Configuration.default.aVotreService.app.applePayMerchantID

        let paymentRequest = StripeAPI.paymentRequest(withMerchantIdentifier: merchantIdentifier, country: "US", currency: "USD")

        // Configure the line items on the payment request
        paymentRequest.paymentSummaryItems = [
            // The final line should represent your company;
            // it'll be prepended with the word "Pay" (i.e. "Pay iHats, Inc $50")
            PKPaymentSummaryItem(label: "A Votre Service LLC", amount: NSDecimalNumber(string: String(summary.total))),
        ]

        // Initialize an STPApplePayContext instance
        if let applePayContext = STPApplePayContext(paymentRequest: paymentRequest, delegate: self) {
            // Present Apple Pay payment sheet
            applePayContext.presentApplePay {

            }
        } else {
            SVProgressHUD.showError(withStatus: "There is a problem with the Apple Pay configuration")
            _forceSaveButtonDisabled = false
            // There is a problem with your Apple Pay configuration
        }
    }

    func payWithCard(response: OrderClientSecretResponse) {
        let paymentIntentParams = STPPaymentIntentParams(clientSecret: response.clientSecret)

        // Submit the payment
        let paymentHandler = STPPaymentHandler.shared()
        paymentHandler.confirmPayment(paymentIntentParams, with: self) { (status, paymentIntent, error) in
            self._forceSaveButtonDisabled = false

            switch (status) {
            case .failed:
                SVProgressHUD.showError(withStatus: error?.localizedDescription)
                Log.thisError(error)
                break
            case .canceled:
                Log.this("Payment canceled")
                Log.thisError(error)
                break
            case .succeeded:
                Log.this("Payment succeeded")
                self.handlePaymentSucceeded()
                break
            @unknown default:
                SVProgressHUD.showError(withStatus: "Unknown Error")
                break
            }
        }
    }

    func handlePaymentSucceeded() {
        _forceSaveButtonDisabled = true
        SVProgressHUD.dismiss()
        navigator.paymentReviewDidFinish()
    }
}

extension AmenityPaymentReviewViewController: STPAuthenticationContext {
    func authenticationPresentingViewController() -> UIViewController {
        self
    }
}

extension AmenityPaymentReviewViewController: STPApplePayContextDelegate {
    func applePayContext(_ context: STPApplePayContext, didCreatePaymentMethod paymentMethod: STPPaymentMethod, paymentInformation: PKPayment, completion: @escaping STPIntentClientSecretCompletionBlock) {
        reviewManager.createOrder(stripePaymentMethodID: paymentMethod.stripeId).done { response in
            completion(response.clientSecret, nil)
        }.catch { error in
            completion(nil, error)
            Log.thisError(error)
        }
    }

    func applePayContext(_ context: STPApplePayContext, didCompleteWith status: STPPaymentStatus, error: Error?) {
        _forceSaveButtonDisabled = false
        switch status {
        case .success:
            Log.this("Success")
            handlePaymentSucceeded()
        case .error:
            SVProgressHUD.showError(withStatus: error?.localizedDescription)
            Log.thisError(error)
        case .userCancellation:
            Log.this("user canceled")
        @unknown default:
            ()
        }
    }

}
