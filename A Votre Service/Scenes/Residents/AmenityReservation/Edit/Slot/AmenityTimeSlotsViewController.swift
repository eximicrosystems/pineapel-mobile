//
// Created by Andrei Stoicescu on 04/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import PromiseKit

class AmenityTimeSlotsViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

    enum Position {
        case start
        case mid
    }

    enum SelectionError: Error {
        case noAdjacentSelected
        case maximumSlotsReached
        case slotOccupied
    }

    struct Selection {
        var selected: Bool
        var prevSelected: Bool
        var nextSelected: Bool
    }

    class LineSelection: ASDisplayNode {
        var topLine = ASImageNode()
        var bottomLine = ASDisplayNode()
        var leftLine = ASDisplayNode()
        var rightLine = ASDisplayNode()

        init(data: TimeSlotData) {
            let selectionColor = AVSColor.accentBackground.color
            let dividerColor = AVSColor.dividerSecondary.color

            if data.isLast {
                if data.selection.selected {
                    bottomLine.backgroundColor = selectionColor
                } else {
                    bottomLine.backgroundColor = dividerColor
                }
            }

            switch data.intervalPosition {
            case .start:
                topLine.backgroundColor = dividerColor
            case .mid:
                topLine.backgroundColor = UIColor(patternImage: Asset.Images.dashedLine.image)
            }

            super.init()

            if data.selection.selected {
                leftLine.backgroundColor = selectionColor
                rightLine.backgroundColor = selectionColor

                if !data.selection.prevSelected {
                    topLine.backgroundColor = selectionColor
                }
            } else {
                if data.selection.prevSelected {
                    topLine.backgroundColor = selectionColor
                }
            }

            automaticallyManagesSubnodes = true
            isUserInteractionEnabled = false
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            topLine.style.height = ASDimensionMakeWithPoints(1)
            bottomLine.style.height = ASDimensionMakeWithPoints(1)
            leftLine.style.width = ASDimensionMakeWithPoints(1)
            rightLine.style.width = ASDimensionMakeWithPoints(1)

            let topBottomLines = ASStackLayoutSpec(direction: .vertical,
                                                   spacing: 0,
                                                   justifyContent: .spaceBetween,
                                                   alignItems: .stretch,
                                                   children: [topLine, bottomLine])
            let leftRightLines = ASStackLayoutSpec(direction: .horizontal,
                                                   spacing: 0,
                                                   justifyContent: .spaceBetween,
                                                   alignItems: .stretch,
                                                   children: [leftLine, rightLine])
            return leftRightLines.background(topBottomLines)
        }
    }

    class TimeSlotManager {
        var slots = [TimeSlotData]()
        var maxSlots = 0

        var selectedSlots: [TimeSlotData] {
            slots.filter { $0.selection.selected }
        }

        // TODO Remove this after we enable any-selection slots
        func adjacentSlotSelected(_ index: Int) -> Bool {
            if let prevSlot = slots[safe: index - 1] {
                if prevSlot.selection.selected {
                    return true
                }
            }

            if let nextSlot = slots[safe: index + 1] {
                if nextSlot.selection.selected {
                    return true
                }
            }

            return false
        }

        // TODO Remove this after we enable any-selection slots
        func allAdjacentSlotsSelected(_ index: Int) -> Bool {
            [slots[safe: index - 1],
             slots[safe: index + 1]].compactMap { $0 }.filter { $0.selection.selected }.count == 2
        }

        // TODO Remove this after we enable any-selection slots
        func canDeselectSlotAtIndex(_ index: Int) -> Bool {
            // Disable selecting more than one seat for now
            // A slot can be deselected if it doesn't have 2 surrounding slots selected (1 is fine)
            guard let slot = slots[safe: index], slot.selectedSeats == 1 else { return false }
            return !allAdjacentSlotsSelected(index)
        }

        func canSelectSlotAtIndex(_ index: Int) -> Bool {
            switch canSelectSlotAtIndexResult(index) {
            case .success():
                return true
            case .failure(_):
                return false
            }
        }

        func canSelectSlotAtIndexResult(_ index: Int) -> Swift.Result<Void, SelectionError> {
            if selectedSlots.count >= maxSlots {
                return .failure(.maximumSlotsReached)
            }

            if !isSlotAvailable(index) {
                return .failure(.slotOccupied)
            }

            if selectionDisabledDueToAdjacent(index) {
                return .failure(.noAdjacentSelected)
            }
            return .success(())
        }

        private func isSlotAvailable(_ index: Int) -> Bool {
            self.slots[safe: index]?.hasAvailableSeats ?? false
        }

        // TODO Remove this after we enable any-selection slots
        private func selectionDisabledDueToAdjacent(_ index: Int) -> Bool {
            guard let slot = slots[safe: index],
                  slot.selectedSeats == 0, // Disable selecting more than one seat for now
                  slot.hasAvailableSeats else { return false }

            if selectedSlots.count > 0 {
                // If there is a selection, there must be an adjacent slot selected
                return !adjacentSlotSelected(index)
            }

            return false
        }

        func availableAdjacentIndexesForIndex(_ index: Int) -> [Int]? {
            guard slots[safe: index] != nil else { return nil }

            var indexes = [index]
            if slots[safe: index - 1] != nil { indexes.insert(index - 1, at: 0) }
            if slots[safe: index + 1] != nil { indexes.append(index + 1) }

            return indexes
        }

        private func updateSlotSelected(selected: Bool, index: Int) {
            guard let slot = slots[safe: index] else { return }
            slot.selection.selected = selected
            if let prevSlot = slots[safe: index - 1] {
                prevSlot.selection.nextSelected = selected
            }
            if let nextSlot = slots[safe: index + 1] {
                nextSlot.selection.prevSelected = selected
            }
        }

        func selectSlotAtIndex(_ index: Int) {
            guard let slot = slots[safe: index] else { return }
            guard canSelectSlotAtIndex(index) else { return }

            slot.selectedSeats = min(slot.maxSeats, slot.selectedSeats + 1)
            updateSlotSelected(selected: true, index: index)
        }

        func deselectSlotAtIndex(_ index: Int) {
            guard let slot = slots[safe: index] else { return }
            guard canDeselectSlotAtIndex(index) else { return }

            // slot.selectedSeats = max(0, slot.selectedSeats - 1)
            // updateSlotSelected(selected: slot.selectedSeats > 0, index: index)

            slot.selectedSeats = 0
            updateSlotSelected(selected: false, index: index)
        }

        func buildFromTimeSlots(_ slots: [AvailableHours.TimeSlot]) {
            self.slots.removeAll()
            let count = slots.count
            self.slots = slots.enumerated().map { index, slot in
                TimeSlotData(startDate: slot.startDate,
                             endDate: slot.endDate,
                             maxSeats: slot.maxSeats,
                             selectedSeats: 0,
                             intervalPosition: .start,
                             isLast: index + 1 == count,
                             selection: Selection(selected: false,
                                                  prevSelected: false,
                                                  nextSelected: false)
                )
            }
        }
    }

    class TimeSlotCell: ASCellNode {
        var statusLabel = ASTextNode2()
        var actionButton = ASButtonNode()
        var statusBackground = ASDisplayNode()

        var startDateLabel: ASTextNode2?
        var endDateLabel: ASTextNode2?

        var lineSelection: LineSelection

        var data: TimeSlotData

        var tappedActionButton: (() -> Void)?

        init(data: TimeSlotData) {

            self.data = data

            lineSelection = LineSelection(data: data)

            if data.isLast {
                endDateLabel = ASTextNode2(with: data.endDate.avsTimeString().attributed(.note, .secondaryLabel))
            }

            let currentBuildingHour = data.startDate.convertedToSameTimeZone(AppState.buildingTimeZone)

            super.init()

            statusBackground.backgroundColor = .clear

            if data.maxSeats > 0 {
                actionButton.isHidden = false
                if data.selectedSeats > 0 {
                    // statusLabel.attributedText = NSAttributedString.string(text: "\(data.selectedSeats) seats reserved (\(data.remainingSeats) remaining)", font: .Note, color: Asset.Colors.green.color)

                    statusLabel.attributedText = "Reserved".attributed(.body, .colorGreen)
                    actionButton.setAttributedTitle("×".attributed(.bodyTitleMedium, .accentBackground), for: .normal)
                    statusBackground.backgroundColor = AVSColor.primaryBackground.color
                } else {
                    // statusLabel.attributedText = NSAttributedString.string(text: "\(data.maxSeats) seats", font: .Note, color: Asset.Colors.green.color)
                    statusLabel.attributedText = "Available".attributed(.body, .colorGreen)
                    actionButton.setAttributedTitle("+".attributed(.bodyTitleMedium, .colorGreen), for: .normal)
                }
            } else {
                actionButton.isHidden = true
                statusLabel.attributedText = "Full".attributed(.body, Date() > currentBuildingHour ? .clear : .accentBackground)
            }

            actionButton.hitTestSlop = UIEdgeInsets(top: -10, left: -10, bottom: -10, right: -10)

            // statusBackground.backgroundColor = AVSColor.secondaryBackground.color

            switch data.intervalPosition {
            case .start:
                var color = AVSColor.secondaryLabel
                if data.maxSeats == 0 && Date() > currentBuildingHour {
                    color = AVSColor.tertiaryLabel
                }

                startDateLabel = ASTextNode2(with: data.startDate.avsTimeString().attributed(.note, color))
            case .mid:
                ()
            }

            self.automaticallyManagesSubnodes = true
            actionButton.addTarget(self, action: #selector(tapActionButton), forControlEvents: .touchUpInside)
        }

        @objc func tapActionButton() {
            tappedActionButton?()
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            [
                [startDateLabel, endDateLabel].vStacked().justified(.spaceBetween)
                                              .width(.points(60)),

                lineSelection.overlay(
                        [statusLabel, actionButton].hStacked().spaced(10).justified(.spaceBetween)
                                                   .margin(10)
                                                   .background(statusBackground)
                ).growing()
            ].hStacked().spaced(10).aligned(.stretch)
             .margin(horizontal: 15)
        }
    }

    class TimeSlotData {
        let startDate: Date
        let endDate: Date
        var maxSeats: Int
        var selectedSeats: Int
        let intervalPosition: Position
        let isLast: Bool
        var selection: Selection

        init(startDate: Date, endDate: Date, maxSeats: Int, selectedSeats: Int, intervalPosition: Position, isLast: Bool, selection: Selection) {
            self.startDate = startDate
            self.endDate = endDate
            self.maxSeats = maxSeats
            self.selectedSeats = selectedSeats
            self.intervalPosition = intervalPosition
            self.isLast = isLast
            self.selection = selection
        }

        var hasAvailableSeats: Bool {
            maxSeats > 0 && remainingSeats > 0
        }

        var remainingSeats: Int {
            maxSeats - selectedSeats
        }
    }

    var collectionNode: ASCollectionNode

    var day: Date
    var amenity: Amenity
    var reservation: AmenityReservation?
    var selectionBar: AmenitySelectionBar

    var slotManager: TimeSlotManager
    var navigator: AmenityReservationNavigator

    init(day: Date, navigator: AmenityReservationNavigator) {
        self.day = day
        self.navigator = navigator
        amenity = navigator.amenity
        reservation = navigator.reservation
        slotManager = TimeSlotManager()
        slotManager.maxSlots = amenity.maxSlots

        let node = ASDisplayNode()
        node.automaticallyManagesSubnodes = true
        node.automaticallyRelayoutOnSafeAreaChanges = true

        let dateBar = AmenityReservationNodes.EditSummaryLineNode(leftLabel: "Date", rightLabel: day.avsDateShortMonthYearString(), rightIcon: nil)

        let pager = AmenityReservationNodes.PriceImagePager(amenity: amenity, ratio: 0.5)

        let selectTimeLabel = ASTextNode2(with: "Select Time".attributed(.bodyTitleHeavy, .primaryLabel))

        let selectionBar = AmenitySelectionBar(buttonTitle: "Continue")
        selectionBar.state = .empty("No time selected yet")
        self.selectionBar = selectionBar

        collectionNode = .avsCollection()
        collectionNode.backgroundColor = AVSColor.secondaryBackground.color

        super.init(node: node)
        collectionNode.delegate = self
        collectionNode.dataSource = self

        selectionBar.tappedButton = { [weak self] in
            guard let self = self else { return }
            let selectedSlots = self.slotManager.selectedSlots

            if selectedSlots.count > 0 {
                guard let startDate = selectedSlots.first?.startDate,
                      let endDate = selectedSlots.last?.endDate else { return }
                self.navigator.timeSlotsDidFinish(startDate: startDate, endDate: endDate)
            }
        }

        node.layoutSpecBlock = { node, range in
            [
                pager.margin(15).margin(top: node.safeAreaInsets.top),
                dateBar.margin(horizontal: 15),
                selectTimeLabel.margin(horizontal: 15),
                self.collectionNode.growing(),
                selectionBar.spacingBefore(-15)
            ].vStacked().spaced(15)
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()
        view.backgroundColor = AVSColor.secondaryBackground.color
        reloadAvailability(date: day)
        navigationItem.title = "Reservation"
    }

    func reloadAvailability(date: Date) {
        let apiService = ResidentService()
        apiService.getAmenityAvailability(amenityID: amenity.amenityID, date: date).done { [weak self] hours in
            guard let self = self else { return }
            SVProgressHUD.dismiss()
            self.slotManager.maxSlots = hours.availableSlots
            if hours.availableSlots == 0 {
                SVProgressHUD.showInfo(withStatus: "You have already reached the maximum slots you can select for this day")
            }
            let timeSlots = hours.availableTimeSlots(date: date)
            self.slotManager.buildFromTimeSlots(timeSlots)
            self.collectionNode.reloadData { [weak self] in
                self?.scrollToFirstAvailableSlot()
            }
        }.catch { [weak self] error in
            Log.thisError(error)
            SVProgressHUD.dismiss()
            guard let self = self else { return }
            self.collectionNode.reloadData()
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        slotManager.slots.count
    }

    func scrollToFirstAvailableSlot() {
        guard let idx = slotManager.slots.firstIndex(where: { $0.maxSeats > 0 }) else { return }
        collectionNode.scrollToItem(at: IndexPath(item: idx, section: 0), at: .top, animated: false)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let slot = slotManager.slots[indexPath.item]
        return { [weak self] in
            let cell = TimeSlotCell(data: slot)
            cell.tappedActionButton = { [weak self] in
                guard let self = self else { return }
                if slot.selection.selected {
                    self.deselectSlotAtIndex(indexPath.item)
                } else {
                    self.selectSlotAtIndex(indexPath.item)
                }
            }

            return cell
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        selectSlotAtIndex(indexPath.item)
    }

    func selectSlotAtIndex(_ index: Int) {
        switch slotManager.canSelectSlotAtIndexResult(index) {
        case .success():
            slotManager.selectSlotAtIndex(index)
            reloadSlotAtIndex(index)
            reloadSelectionBar()
        case .failure(let error):
            let message: String
            switch error {
            case .noAdjacentSelected:
                message = "You can only select consecutive slots"
            case .maximumSlotsReached:
                message = "Maximum slots reached"
            case .slotOccupied:
                // silent fail
                return
            }

            SVProgressHUD.showError(withStatus: message)
            SVProgressHUD.dismiss(withDelay: 1)
        }
    }

    func deselectSlotAtIndex(_ index: Int) {
        if slotManager.canDeselectSlotAtIndex(index) {
            slotManager.deselectSlotAtIndex(index)
            reloadSlotAtIndex(index)
            reloadSelectionBar()
        } else {
            SVProgressHUD.showError(withStatus: "You can only select consecutive slots")
            SVProgressHUD.dismiss(withDelay: 1)
        }
    }

    func reloadSelectionBar() {
        let selectedSlots = slotManager.selectedSlots

        if selectedSlots.count > 0 {
            guard let startDate = selectedSlots.first?.startDate,
                  let endDate = selectedSlots.last?.endDate else { return }

            selectionBar.state = .selected("\(day.avsDateString())\n\(startDate.avsTimeString()) to \(endDate.avsTimeString())")
        } else {
            selectionBar.state = .empty("No time slots selected yet")
        }
    }

    func reloadSlotAtIndex(_ index: Int) {
        guard let indexes = slotManager.availableAdjacentIndexesForIndex(index) else { return }

        let indexPaths = indexes.map { IndexPath(item: $0, section: 0) }

        collectionNode.performBatch(animated: false, updates: { [weak self] in
            guard let self = self else { return }
            self.collectionNode.reloadItems(at: indexPaths)
        })
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        ASSizeRange.fullWidth(collectionNode: collectionNode)
    }
}
