//
// Created by Andrei Stoicescu on 05/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit
import AsyncDisplayKit
import SVProgressHUD

class AmenitySelectionBar: ASDisplayNode {
    enum State {
        case empty(String, buttonTitle: String? = nil)
        case selected(String, buttonTitle: String? = nil)

        var isSelected: Bool {
            switch self {
            case .empty:
                return false
            case .selected:
                return true
            }
        }
    }

    var button: ASButtonNode
    var label = ASTextNode2()

    var tappedButton: (() -> Void)?

    var state: State = .empty("") {
        didSet {
            switch state {
            case .empty(let string, let buttonTitle):
                label.attributedText = string.attributed(.body, .accentForeground, alignment: .center)
                button.alpha = 0.3
                if let title = buttonTitle {
                    button.setPrimaryButtonTitle(title)
                }
            case .selected(let string, let buttonTitle):
                label.attributedText = string.attributed(.body, .accentForeground, alignment: .center)
                button.alpha = 1
                if let title = buttonTitle {
                    button.setPrimaryButtonTitle(title)
                }
            }
            setNeedsLayout()
        }
    }

    init(buttonTitle: String) {
        button = .secondaryButton(title: buttonTitle)
        super.init()
        backgroundColor = AVSColor.tertiaryBackground.color
        shadowColor = UIColor.black.cgColor
        shadowOpacity = 0.2
        shadowRadius = 3
        shadowOffset = CGSize(width: 0, height: -2)
        automaticallyManagesSubnodes = true
        automaticallyRelayoutOnSafeAreaChanges = true

        button.addTarget(self, action: #selector(tapButton), forControlEvents: .touchUpInside)
    }

    @objc func tapButton() {
        if state.isSelected {
            tappedButton?()
        }
    }

    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        label.style.flexBasis = ASDimensionMakeWithPoints(50)
        button.style.flexBasis = ASDimensionMakeWithPoints(40)
        label.style.flexGrow = 1
        label.style.flexShrink = 1
        button.style.flexGrow = 1

        let stack = ASStackLayoutSpec(direction: .horizontal, spacing: 20, justifyContent: .start, alignItems: .center, children: [label, button])
        return stack.margin(top: 20, left: 15, bottom: self.safeAreaInsets.bottom + 10, right: 15)
    }
}

private extension Amenity {
    var pricePerString: String? {
        guard let priceType = priceType else { return nil }
        guard let duration = slotDuration else { return nil }

        switch priceType {
        case .day:
            return "/night"
        case .flat:
            return nil
        case .slot:
            switch duration {
            case 60:
                return "/hr"
            default:
                return "/\(duration)m"
            }
        }
    }
}

class AVSPriceBar: ASDisplayNode {
    struct Item {
        let title: String?
        let price: Float?
        let per: String?
    }

    var nameLabel = ASTextNode2()
    var priceLabel: ASTextNode2?

    init(item: Item) {
        super.init()
        nameLabel.attributedText = item.title?.attributed(.H2, .accentForeground)
        if let attr = computePriceString(price: item.price, per: item.per) {
            priceLabel = .init(with: attr)
        }

        automaticallyManagesSubnodes = true
        isUserInteractionEnabled = false
    }

    convenience init(amenity: Amenity) {
        self.init(item: .init(title: amenity.name, price: amenity.price, per: amenity.pricePerString))
    }

    func computePriceString(price: Float?, per: String?) -> NSAttributedString? {
        guard let price = price, price > 0 else { return nil }
//        let priceAttr = "$".attributed(.body, .accentForeground)?
//                           .appendingAttributedString("\(price.currencyClean)".attributed(.H2, .accentForeground))
        let priceAttr = "$".attributed(.body, .colorMilitaryGreen)?
                           .appendingAttributedString("\(price.currencyClean)".attributed(.H2, .colorMilitaryGreen))

        if let per = per {
//            return priceAttr?.appendingAttributedString(per.attributed(.body, .accentForeground))
            return priceAttr?.appendingAttributedString(per.attributed(.body, .colorMilitaryGreen))
        }

        return priceAttr
    }

    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        [
            nameLabel.filling(),
            priceLabel
        ].hStacked().spaced(20).justified(.spaceBetween).aligned(.end)
    }
}

class AmenityDateSelectionViewController: ASDKViewController<ASDisplayNode> {
    private weak var navigator: AmenityReservationNavigator?

    var amenity: Amenity
    var reservation: AmenityReservation?

    var selectionBar: AmenitySelectionBar
    var selectedDate: Date?

    init(navigator: AmenityReservationNavigator) {
        amenity = navigator.amenity
        reservation = navigator.reservation
        self.navigator = navigator

        let node = ASDisplayNode()
        node.automaticallyManagesSubnodes = true
        node.automaticallyRelayoutOnSafeAreaChanges = true

        let pager = AmenityReservationNodes.PriceImagePager(amenity: amenity)

        let selectionBar = AmenitySelectionBar(buttonTitle: "Select Date")
        selectionBar.state = .empty("No date selected yet")
        self.selectionBar = selectionBar

        super.init(node: node)

        let dateRangePicker = ASCellNode(viewControllerBlock: {
            let dateRangePicker = CalendarDateRangePickerViewController(collectionViewLayout: UICollectionViewFlowLayout())
            dateRangePicker.delegate = self
            dateRangePicker.selectionMode = .single
            dateRangePicker.minimumDate = Date()
            dateRangePicker.selectedStartDate = self.reservation?.date ?? Date()
            dateRangePicker.maximumDate = 1.months.later
            dateRangePicker.firstDayOfWeek = .monday
            dateRangePicker.titleText = L10n.AmenityReservation.Fields.ReserveDate.datepickerTitle
            return dateRangePicker
        })

        selectionBar.tappedButton = { [weak self] in
            guard let date = self?.selectedDate else { return }
            self?.navigator?.dateSelectionDidFinish(day: date)
        }

        node.layoutSpecBlock = { node, range in
            [
                pager.margin(15).margin(top: node.safeAreaInsets.top),
                dateRangePicker.growing(),
                selectionBar
            ].vStacked()
        }
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()
        didSelectStartDate(startDate: reservation?.date ?? Date())
        view.backgroundColor = AVSColor.secondaryBackground.color
        navigationItem.title = "Amenity Details"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: L10n.General.close, style: .plain, target: self, action: #selector(didTapClose))
    }

    @objc func didTapClose() {
        guard let nav = navigationController as? AVSNavigationController else { return }
        nav.dismissNavigation?()
    }
}

extension AmenityDateSelectionViewController: CalendarDateRangePickerViewControllerDelegate {
    func didCancelPickingDateRange() {
        navigationController?.dismiss(animated: true)
    }

    func didPickSingleDate(date: Date) {}

    func didPickDateRange(startDate: Date!, endDate: Date!) {}

    func didSelectStartDate(startDate: Date!) {
        selectionBar.state = .selected(startDate.avsDateString())
        selectedDate = startDate
    }

    func didSelectEndDate(endDate: Date!) {}
}
