//
// Created by Andrei Stoicescu on 19.11.2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit
import AsyncDisplayKit
import SVProgressHUD

class AmenityDateSlotsViewController: ASDKViewController<ASDisplayNode> {

    var amenity: Amenity
    var selectionBar: AmenitySelectionBar

    var startDate: Date?
    var endDate: Date?

    var dateRangePicker: CalendarDateRangePickerViewController?

    init(navigator: AmenityReservationNavigator) {
        amenity = navigator.amenity
        let node = ASDisplayNode()
        node.automaticallyManagesSubnodes = true
        node.automaticallyRelayoutOnSafeAreaChanges = true

        let pager = AmenityReservationNodes.PriceImagePager(amenity: amenity)

        let date = Date()

        let selectionBar = AmenitySelectionBar(buttonTitle: "Continue")
        selectionBar.state = .empty("No date selected yet")
        self.selectionBar = selectionBar

        super.init(node: node)

        let dateRangePicker = ASCellNode(viewControllerBlock: {
            let dateRangePicker = CalendarDateRangePickerViewController(collectionViewLayout: UICollectionViewFlowLayout())
            self.dateRangePicker = dateRangePicker
            dateRangePicker.delegate = self
            dateRangePicker.selectionMode = .range
            dateRangePicker.minimumDate = date
            dateRangePicker.maximumDate = AmenityReservation.dailyReservationAvailabilityCheckEndDate
            dateRangePicker.firstDayOfWeek = .monday
            dateRangePicker.titleText = L10n.AmenityReservation.Fields.ReserveDate.datepickerTitle
            return dateRangePicker
        })

        selectionBar.tappedButton = { [weak self] in
            guard let self = self else { return }

            let minimumDays = max(self.amenity.slotDuration ?? 1, 1) // default to minimum 1 night, reject server response with less than 1

            guard let startDate = self.startDate,
                  let endDate = self.endDate,
                  endDate.daysLater(than: startDate) >= minimumDays else {
                SVProgressHUD.showError(withStatus: "You must select a minimum of \(minimumDays) nights")
                return
            }

            SVProgressHUD.dismiss()
            navigator.dateRangeSelectionDidFinish(startDate: startDate, endDate: endDate)
        }

        node.layoutSpecBlock = { node, range in
            [
                pager.margin(15).margin(top: node.safeAreaInsets.top),
                dateRangePicker.growing(),
                selectionBar
            ].vStacked()
        }
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()
        view.backgroundColor = AVSColor.secondaryBackground.color
        navigationController?.isModalInPresentation = true
        navigationItem.title = "Reservation"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: L10n.General.close, style: .plain, target: self, action: #selector(didTapClose))

        ResidentService().getAmenityDaysAvailability(amenityID: amenity.amenityID, startDate: Date(), endDate: AmenityReservation.dailyReservationAvailabilityCheckEndDate).done { days in
            Log.this(days)
            self.dateRangePicker?.disabledDates = days.notAvailableDates
            self.dateRangePicker?.collectionView.reloadData()
        }.catch { error in
            Log.thisError(error)
        }
    }

    @objc func didTapClose() {
        guard let nav = navigationController as? AVSNavigationController else { return }
        nav.dismissNavigation?()
    }
}

extension AmenityDateSlotsViewController: CalendarDateRangePickerViewControllerDelegate {
    func didCancelPickingDateRange() {
        navigationController?.dismiss(animated: true)
    }

    func didPickSingleDate(date: Date) {
    }

    func didPickDateRange(startDate: Date!, endDate: Date!) {

    }

    func didSelectStartDate(startDate: Date!) {
        self.startDate = startDate
        endDate = nil
        selectionBar.state = .selected(startDate.avsDateString())
    }

    func didSelectEndDate(endDate: Date!) {
        self.endDate = endDate
        let info = [startDate, self.endDate].compactMap { $0?.avsDateString() }.joined(separator: " - ")
        selectionBar.state = .selected(info)
    }
}
