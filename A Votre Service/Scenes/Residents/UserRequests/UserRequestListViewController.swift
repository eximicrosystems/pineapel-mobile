//
// Created by Andrei Stoicescu on 26/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit
import SwiftyUserDefaults
import AsyncDisplayKit
import SVProgressHUD
import PromiseKit
import Combine
import DifferenceKit

class UserRequestListViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource, ASCollectionDelegateFlowLayout {

    lazy var emptyNode = EmptyNode(emptyNodeType: .request)
    lazy var refreshControl = UIRefreshControl()

    var requestsProvider = UserRequestsProvider(cacheable: true)

    var items = [CategoryUserRequest]()

    private var subscriptions = Set<AnyCancellable>()
    var collectionNode: ASCollectionNode

    override init() {
        collectionNode = .avsCollection()
        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func reloadData() {
        let source = items
        let target = requestsProvider.categoryRequests

        let stagedChangeSet = StagedChangeset(source: source, target: target)
        for changeSet in stagedChangeSet {
            items = changeSet.data
            collectionNode.performDiffBatchUpdates(changeSet: changeSet)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupBindings()

        replaceSystemBackButton()

        navigationItem.title = "Requests"
        navigationItem.rightBarButtonItems = [
            UIBarButtonItem(image: Asset.Images.Concierge.icFilter.image.withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(tapFilterButton)),
            UIBarButtonItem(image: Asset.Images.Concierge.icPlus.image.withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(tapCreateButton))
        ]

        showEmptyScreen(false)
        collectionNode.view.backgroundView = emptyNode.view
        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)
        collectionNode.view.addSubview(refreshControl)

        emptyNode.actionButtonTapped = { [weak self] in
            self?.tapCreateButton()
        }

        requestsProvider.loadCache()
        refreshItems()
    }

    func setupBindings() {
        requestsProvider.$categoryRequests
                .receive(on: DispatchQueue.main)
                .sink { [weak self] items in
                    self?.reloadData()
                }.store(in: &subscriptions)

        requestsProvider.dataSource.$hasNoItemsAfterLoading
                .receive(on: DispatchQueue.main)
                .sink { [weak self] hasNoItemsAfterLoading in
                    self?.showEmptyScreen(hasNoItemsAfterLoading)
                }.store(in: &subscriptions)

        requestsProvider.$showLoadingIndicator
                .receive(on: DispatchQueue.main)
                .sink { [weak self] loading in
                    guard let self = self else { return }
                    if loading {
                        if !self.refreshControl.isRefreshing {
                            SVProgressHUD.show()
                        }
                    } else {
                        SVProgressHUD.dismiss()
                        self.refreshControl.endRefreshing()
                    }
                }.store(in: &subscriptions)
    }

    @objc func refreshItems() {
        requestsProvider.reload()
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let categoryRequest = items[indexPath.item]
        return {
            ConciergePagerListNodes.RequestCell(categoryUserRequest: categoryRequest)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        items.count
    }

    func collectionView(_ collectionView: ASCollectionView, willDisplayNodeForItemAt indexPath: IndexPath) {
        requestsProvider.dataSource.loadMoreContentIfNeeded(currentItem: items[indexPath.item].request)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        let item = items[indexPath.item]
        let vc = UserRequestSingleViewController(requestID: item.request.userRequestID)
        vc.categoryRequest = item
        navigationController?.pushViewController(vc, animated: true)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, supplementaryElementKindsInSection section: Int) -> [String] {
        [UICollectionView.elementKindSectionHeader]
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> ASCellNode {
        guard kind == UICollectionView.elementKindSectionHeader,
              let status = requestsProvider.statuses?.first else { return ASCellNode() }

        return ConciergePagerListNodes.Header(title: status.displayString, subject: nil, layoutType: .spaceBetween)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, sizeRangeForHeaderInSection section: Int) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }

    @objc func tapCreateButton() {
        let requestEditViewController = UserRequestEditViewController()
        requestEditViewController.delegate = self
        navigationController?.pushViewController(requestEditViewController, animated: true)
    }

    @objc func tapFilterButton() {
        let alertController = UIAlertController(title: "Filter Requests", message: nil, preferredStyle: .actionSheet)

        alertController.addAction(UIAlertAction(title: "All", style: .default) { [weak self] _ in
            self?.requestsProvider.statuses = nil
        })

        [UserRequest.Status.pending, .accepted, .done].map { status in
            UIAlertAction(title: status.displayString, style: .default) { [weak self] _ in
                self?.requestsProvider.statuses = [status]
            }
        }.forEach { action in
            alertController.addAction(action)
        }

        alertController.addAction(UIAlertAction(title: L10n.General.cancel, style: .cancel, handler: nil))
        alertController.fixIpadIfNeeded(view: view)
        present(alertController, animated: true)
    }

    func showEmptyScreen(_ show: Bool) {
        emptyNode.isHidden = !show
    }
}

extension UserRequestListViewController: RequestEditViewControllerProtocol {
    func requestEditViewControllerDidFinish(_ controller: UserRequestEditViewController) {
        navigationController?.popToViewController(self, animated: true)
        SVProgressHUD.show()
        refreshItems()
    }
}