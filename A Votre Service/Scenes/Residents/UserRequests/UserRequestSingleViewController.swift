//
// Created by Andrei Stoicescu on 22/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import SVProgressHUD
import AsyncDisplayKit

class UserRequestSingleViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource, ASCollectionDelegateFlowLayout {

    enum Sections: Int, CaseIterable {
        case request
        case order
    }

    var requestID: String

    var provider = UserRequestsProvider()

    var collectionNode: ASCollectionNode
    lazy var refreshControl = UIRefreshControl()
    var categoryRequest: CategoryUserRequest?

    init(requestID: String) {
        collectionNode = .avsCollection()
        self.requestID = requestID
        super.init(node: collectionNode)

        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()
        navigationItem.title = "Request"
        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)
        collectionNode.view.addSubview(refreshControl)
        if categoryRequest == nil {
            SVProgressHUD.show()
            refreshItems()
        }
    }

    @objc func refreshItems() {
        provider.loadRequest(requestId: requestID).done { [weak self] categoryRequest in
            SVProgressHUD.dismiss()
            guard let self = self else { return }
            self.categoryRequest = categoryRequest
            self.collectionNode.reloadData()
        }.catch { error in
            SVProgressHUD.showError(withStatus: error.localizedDescription)
            Log.thisError(error)
        }.finally { [weak self] in
            self?.refreshControl.endRefreshing()
        }
    }

    @objc func didTapClose() {
        guard let nav = navigationController as? AVSNavigationController else { return }
        nav.dismissNavigation?()
    }

    func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
        Sections.allCases.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        guard let section = Sections(rawValue: indexPath.section) else { return { ASCellNode() } }
        switch section {
        case .request:
            guard let categoryRequest = categoryRequest else { return { ASCellNode() } }
            return {
                ASCellNode.withNode(ConciergePagerListNodes.SummaryCellNode(categoryUserRequest: categoryRequest),
                                    margin: UIEdgeInsets(top: 0, left: 0, bottom: 15, right: 0))

            }
        case .order:
            return {
                ConciergeGenericNodes.MenuCell(icon: nil, title: "View Order Receipt", subtitle: nil, disclosure: true)
            }
        }

    }

    func collectionNode(_ collectionNode: ASCollectionNode, supplementaryElementKindsInSection section: Int) -> [String] {
        [UICollectionView.elementKindSectionHeader]
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> ASCellNodeBlock {
        guard kind == UICollectionView.elementKindSectionHeader,
              Sections(rawValue: indexPath.section) == .request,
              let item = categoryRequest else { return { ASCellNode() } }

        return {
            ConciergePagerListNodes.Header(title: item.request.status?.displayString, subject: nil)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, sizeRangeForHeaderInSection section: Int) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        guard let section = Sections(rawValue: section) else { return 0 }
        switch section {
        case .request:
            return categoryRequest == nil ? 0 : 1
        case .order:
            return categoryRequest?.request.orderID == nil ? 0 : 1
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        guard let section = Sections(rawValue: indexPath.section) else { return }

        switch section {
        case .order:
            guard let orderID = categoryRequest?.request.orderID else { return }
            let orderVC = OrderSummaryViewController(orderID: orderID)
            orderVC.isNavigational = true
            navigationController?.pushViewController(orderVC, animated: true)
        default:
            ()
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }
}
