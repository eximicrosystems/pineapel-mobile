//
// Created by Andrei Stoicescu on 16/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import Validator
import SVProgressHUD
import AsyncDisplayKit
import PromiseKit
import SwiftyUserDefaults
import ImageViewer
import SafariServices
import Combine
import DifferenceKit
import Disk

protocol FeedbackViewControllerProtocol: UIViewController {
    func feedbackViewController(_ controller: FeedbackViewController, didFinishWith feedback: Feedback)
}

class FeedbackViewController: UITableViewController{
    weak var delegate: FeedbackViewControllerProtocol?

    enum Sections: Int, CaseIterable {
        case formItems
        case saveButton
    }

    class FeedbackForm: AVSForm {
        var feedbackType: String?
        var conciergeID: String?
        var rating: String?
        var comment: String?

        var feedbackTypeItem = FormItem(placeholder: L10n.CreateFeedbackScreen.Fields.FeedbackType.title)
        var conciergeIDItem = FormItem(placeholder: L10n.CreateFeedbackScreen.Fields.Concierge.title)
        var ratingItem = FormItem(placeholder: L10n.CreateFeedbackScreen.Fields.Rating.title)
        var commentItem = FormItem(placeholder: L10n.CreateFeedbackScreen.Fields.Comment.placeholder)

        func resetFormItems() {
            if feedbackType == Feedback.FeedbackType.concierge.rawValue {
                formItems = conciergeFormItems()
            } else {
                formItems = normalFormItems()
            }
        }

        func conciergeFormItems() -> [FormItem] {
            [feedbackTypeItem, conciergeIDItem, ratingItem, commentItem]
        }

        func normalFormItems() -> [FormItem] {
            [feedbackTypeItem, ratingItem, commentItem]
        }

        override init() {
            super.init()

            feedbackTypeItem.value = feedbackType
            feedbackTypeItem.uiProperties.cellType = .singleSelect
            feedbackTypeItem.isMandatory = true
            feedbackTypeItem.selectOptions = [
                .init(value: Feedback.FeedbackType.building.rawValue, label: L10n.CreateFeedbackScreen.Fields.FeedbackType.Options.building),
                .init(value: Feedback.FeedbackType.concierge.rawValue, label: L10n.CreateFeedbackScreen.Fields.FeedbackType.Options.concierge),
                .init(value: Feedback.FeedbackType.other.rawValue, label: L10n.CreateFeedbackScreen.Fields.FeedbackType.Options.other)
            ]
            feedbackTypeItem.valueCompletion = { [weak self] value in
                guard let self = self else { return }
                self.feedbackType = value
                self.feedbackTypeItem.value = value
                self.resetFormItems()
            }

            conciergeIDItem.value = conciergeID
            conciergeIDItem.isMandatory = true
            conciergeIDItem.uiProperties.cellType = .singleSelect
            conciergeIDItem.valueCompletion = { [weak self] value in
                self?.conciergeID = value
                self?.conciergeIDItem.value = value
            }

            ratingItem.value = rating
            ratingItem.isMandatory = true
            ratingItem.uiProperties.cellType = .rating
            ratingItem.valueCompletion = { [weak self] value in
                self?.rating = value
                self?.ratingItem.value = value
            }

            commentItem.value = self.comment
            commentItem.title = L10n.CreateFeedbackScreen.Fields.Comment.title
            commentItem.isMandatory = true
            commentItem.uiProperties.cellType = .textArea
            commentItem.valueCompletion = { [weak self] value in
                self?.comment = value
                self?.commentItem.value = value
            }

            resetFormItems()
        }

        var serverRating: String? {
            if let rating = rating {
                if let ratingNumber = Int(rating) {
                    return String(ratingNumber * 20)
                }
            }
            return nil
        }

        func update(with conciergeList: [User]) {
            conciergeIDItem.selectOptions = conciergeList.map { user in
                FormItem.SelectOption(value: user.uid, label: user.fullName)
            }
        }
    }

    fileprivate var form = FeedbackForm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        replaceSystemBackButton()
        let tabBar = BottonTabBarView(frame: CGRect(x: 0, y: UIScreen.main.bounds.height-203, width: UIScreen.main.bounds.width, height: 83))
        self.view.addSubview(tabBar)
        
        view.backgroundColor = AVSColor.secondaryBackground.color
        navigationItem.title = L10n.CreateFeedbackScreen.Navigation.title
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "chevron.left"), style: .plain, target: self, action: #selector(backView))
        
        tableView.contentInset = .collectionDefaultInset

        AVSForm.FormItemCellType.registerCells(for: tableView)

        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none

        if let users = CodableCache.get([User].self, key: "feedback_concierges") {
            form.update(with: users)
            tableView.reloadData()
        } else {
            SVProgressHUD.show()
        }

        let apiService = ResidentService()
        apiService.getConciergeList().done { users in
            CodableCache.set(users, key: "feedback_concierges")
            SVProgressHUD.dismiss()
            self.form.update(with: users)
            self.tableView.reloadData()
        }.catch { error in
            SVProgressHUD.showError(withStatus: error.localizedDescription)
            Log.thisError(error)
        }
    }
    
    @objc func backView(){
        AppState.switchTo(viewController: ResidentTabBarViewController(page: nil))
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        Sections.allCases.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let section = Sections(rawValue: section) else { return 0 }
        switch section {
        case .formItems:
            return form.formItems.count
        case .saveButton:
            return 1
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let section = Sections(rawValue: indexPath.section) else { return UITableViewCell() }

        switch section {
        case .formItems:
            let item = form.formItems[indexPath.item]
            let cell = item.uiProperties.cellType.dequeueCell(for: tableView, at: indexPath)
            if let formUpdatableCell = cell as? AVSFormUpdatable {
                formUpdatableCell.update(with: item)
            }

            if let selectCell = cell as? AVSForm.FormCells.SingleSelectCell<SingleSelectView> {
                selectCell.formItemValueUpdate = { [weak self] formItem in
                    if formItem == self?.form.feedbackTypeItem {
                        self?.tableView.reloadData()
                    }
                }
            }

            return cell
        case .saveButton:
            let cell = tableView.dequeueReusableCell(withClass: AVSForm.FormCells.SaveCell.self, forIndexPath: indexPath)
            cell.title = L10n.CreateFeedbackScreen.Buttons.Save.title
            return cell
        }
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == Sections.saveButton.rawValue {
            updateForm()
        }
    }

    func updateForm() {
        let formValidationResult = form.isValid()

        switch formValidationResult {

        case .success(_):
            ()
        case .failure(let error):
            Log.thisError(error)
            let message = error.errors.map {
                $0.message
            }.joined(separator: "\n")

            SVProgressHUD.showError(withStatus: message)
            return;
        }

        guard let feedbackString = form.feedbackType,
              let feedbackType = Feedback.FeedbackType(rawValue: feedbackString) else {
            return
        }

        let apiService = ResidentService()
        let feedback = Feedback(
                feedbackType: Feedback.FeedbackType(rawValue: form.feedbackType ?? ""),
                body: form.comment,
                rating: form.serverRating,
                conciergeID: feedbackType == .concierge ? form.conciergeID : nil)

        SVProgressHUD.show()
        apiService.createFeedback(feedback).done { [weak self] feedbackResponse in
            if let self = self {
                SVProgressHUD.showInfo(withStatus: L10n.CreateFeedbackScreen.feedbackSent)
                self.delegate?.feedbackViewController(self, didFinishWith: feedbackResponse)
            }
        }.catch { error in
            SVProgressHUD.showError(withStatus: error.localizedDescription)
            Log.thisError(error)
        }
    }
}
