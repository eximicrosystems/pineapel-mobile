//
// Created by Andrei Stoicescu on 14/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import SwiftyUserDefaults

class CartScheduleManager {

    private var cartProducts: [CartProduct] {
        didSet { refreshScheduledProducts() }
    }

    private(set) var scheduledProducts = [CartProduct]()

    init(cartProducts: [CartProduct]) {
        self.cartProducts = cartProducts
        refreshScheduledProducts()
    }

    var isValid: Bool {
        scheduledProducts.allSatisfy { $0.scheduledAt != nil }
    }

    private func refreshScheduledProducts() {
        scheduledProducts = cartProducts.filter { $0.product.schedulable }
    }
}

