//
// Created by Andrei Stoicescu on 20/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit
import SwiftyUserDefaults
import AsyncDisplayKit
import SVProgressHUD
import PromiseKit

class OrderSummaryViewController: ASDKViewController<ASDisplayNode> {
    private enum Sections: Int, CaseIterable {
        case items
        case summary
    }

    var orderData: OrderData?
    var orderID: String
    var collectionNode: ASCollectionNode

    // Set it to true to not add the close button
    var isNavigational = false

    init(orderID: String) {
        self.orderID = orderID
        let collectionNode = ASCollectionNode.avsCollection()
        self.collectionNode = collectionNode
        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    convenience init(orderData: OrderData) {
        self.init(orderID: orderData.orderID)
        self.orderData = orderData
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()

        if !isNavigational {
            let btn = UIButton(type: .custom)
            btn.setImage(Asset.Images.Concierge.icClose.image.withRenderingMode(.alwaysTemplate), for: .normal)
            btn.addTarget(self, action: #selector(didTapClose), for: .touchUpInside)
            navigationItem.leftBarButtonItem = UIBarButtonItem(customView: btn)
        }

        navigationItem.title = "Order Receipt"

        if orderData == nil {
            SVProgressHUD.show()
            ResidentService().getOrder(orderID: orderID).done { [weak self] data in
                SVProgressHUD.dismiss()
                guard let self = self else { return }
                self.orderData = data
                self.collectionNode.reloadData()
            }.catch { error in
                SVProgressHUD.showError(withStatus: error.localizedDescription)
                Log.thisError(error)
            }
        }
    }

    @objc func didTapClose() {
        guard let nav = navigationController as? AVSNavigationController else { return }
        nav.dismissNavigation?()
    }
}

extension OrderSummaryViewController: ASCollectionDelegate, ASCollectionDataSource {
    func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
        Sections.allCases.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        ASSizeRange.fullWidth(collectionNode: collectionNode)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case Sections.items.rawValue:
            return orderData?.orderItems.count ?? 0
        case Sections.summary.rawValue:
            return 1
        default:
            return 0
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        guard let orderData = orderData, let section = Sections(rawValue: indexPath.section) else { return { ASCellNode() } }

        switch section {
        case .items:
            let orderItem = orderData.orderItems[indexPath.item]
            return {
                ConciergePagerListNodes.RequestCell(orderResponseItem: orderItem)
            }
        case .summary:
            let items = orderData.orderItems
            return { PaymentSummary.Cell(summary: PaymentSummary.OrderSummary.fromResponses(items)) }
        }
    }
}
