//
// Created by Andrei Stoicescu on 10/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import DifferenceKit

protocol ProductProtocol {
    var name: String? { get set }
    var description: String? { get set }
    var productImageURL: URL? { get set }
    var price: Float { get set }
    var productID: String { get set }
    var displayPrice: String { get }
    var schedulable: Bool { get set }
    var taxPercent: Float { get set }
    var inventoryCount: Int? { get set }
}

struct CartProduct: Identifiable {
    let id = UUID().uuidString
    let product: ProductProtocol
    var quantity: Int = 0
    var scheduledAt: Date? = nil // Set in the building's timezone

    var nameWithQuantity: String {
        "\(quantity) × \(product.name ?? "")"
    }

    var displayPrice: String {
        String(format: "$%.2f", Float(quantity) * product.price)
    }

    func canIncreaseQuantity(by increaseBy: Int, currentQuantity: Int? = nil) -> Bool {
        guard let maxQuantity = product.inventoryCount else { return true }
        return (currentQuantity ?? quantity) + increaseBy <= maxQuantity
    }

    func canSetQuantityToCart(_ quantity: Int) -> Bool {
        guard let maxQuantity = product.inventoryCount else { return true }
        return maxQuantity - quantity >= 0
    }
}

class CartProductManager: ObservableObject {

    class Config {
        // if true, CartViewController will be open as modal rather than pushed
        var openCartModal = true

        var cartPrimaryLabel = "Pay Now"
        var cartSecondaryLabel = "Continue Shopping"

        // if block is set, it will be called instead of proceeding with the standard payment flow from CartViewController
        var cartExternalPrimaryAction: (() -> Void)?

        // if block is set, it will be called instead of proceeding with the standard continue shopping from CartViewController
        var cartExternalSecondaryAction: (() -> Void)?

        // if set to true, will not count the products in an amenity reservation
        var skipProducts = false

        // if set to false, the empty screen won't have a close button
        var cartEmptyCloseButton = true

        static var `default`: Config { .init() }
    }

    static var shared = CartProductManager(config: .default)

    init(config: Config) {
        self.config = config
    }

    var config: Config

    struct Product: ProductProtocol, Identifiable {
        var id: String { productID }
        var name: String?
        var description: String?
        var productImageURL: URL?
        var price: Float
        var productID: String
        var inventoryCount: Int?

        var displayPrice: String {
            String(format: "$%.2f", price)
        }

        var schedulable: Bool
        var taxPercent: Float

        var entityType: OrderRequestItem.ItemType
    }

    @Published private (set) var products: [CartProduct] = [] {
        didSet {
            totalProductsCount = products.reduce(0) { $0 + $1.quantity }
        }
    }

    @Published var totalProductsCount: Int = 0

    var hasSchedulableItems: Bool {
        products.first {
            $0.product.schedulable
        } != nil
    }

    var totalProductsCountString: String {
        String(totalProductsCount)
    }

    var totalProductsPrice: Float {
        products.reduce(0) {
            $0 + $1.product.price
        }
    }

    private func canIncreaseProductQuantity(product: ProductProtocol, quantity: Int = 1) -> Bool {
        if product.schedulable { return true }

        guard let maxQuantity = product.inventoryCount else { return true }
        if maxQuantity == 0 { return false }

        if let cartProductIndex = findIndex(product: product) {
            return products[cartProductIndex].quantity + quantity <= maxQuantity
        } else {
            return quantity <= maxQuantity
        }
    }

    func add(product: ProductProtocol, quantity: Int = 1, scheduledAt: Date? = nil) {
        guard canIncreaseProductQuantity(product: product, quantity: quantity) else { return }

        if product.schedulable {
            // Always create new cart products for scheduled items
            products.append(CartProduct(product: product, quantity: quantity, scheduledAt: scheduledAt))
        } else if let cartProductIndex = findIndex(product: product) {
            products[cartProductIndex].quantity += quantity
        } else {
            products.append(CartProduct(product: product, quantity: quantity, scheduledAt: scheduledAt))
        }
    }

    func reduceQuantity(_ product: ProductProtocol) {
        guard let cartProductIndex = findIndex(product: product) else { return }
        reduceQuantity(products[cartProductIndex])
    }

    func reduceQuantity(_ cartProduct: CartProduct) {
        guard let cartProductIndex = findIndex(cartProduct: cartProduct) else { return }
        let cartProduct = products[cartProductIndex]

        if cartProduct.product.schedulable {
            products.remove(at: cartProductIndex)
            return
        }

        let quantity = cartProduct.quantity - 1

        if quantity <= 0 {
            products.remove(at: cartProductIndex)
        } else {
            products[cartProductIndex].quantity = quantity
        }
    }

    func increaseQuantity(_ product: ProductProtocol) {
        guard canIncreaseProductQuantity(product: product),
              let cartProductIndex = findIndex(product: product) else { return }

        products[cartProductIndex].quantity += 1
    }

    func increaseQuantity(_ cartProduct: CartProduct) {
        guard canIncreaseProductQuantity(product: cartProduct.product),
              let cartProductIndex = findIndex(product: cartProduct.product) else { return }

        if cartProduct.product.schedulable {
            add(product: cartProduct.product, quantity: 1)
        } else {
            products[cartProductIndex].quantity += 1
        }

    }

    func removeProduct(_ product: ProductProtocol) {
        guard let cartProductIndex = findIndex(product: product) else { return }
        products.remove(at: cartProductIndex)
    }

    func removeCartProduct(_ cartProduct: CartProduct) {
        guard let cartProductIndex = findIndex(cartProduct: cartProduct) else { return }
        products.remove(at: cartProductIndex)
    }

    private func findIndex(product: ProductProtocol) -> Int? {
        products.firstIndex { $0.product.productID == product.productID }
    }

    private func findIndex(cartProduct: CartProduct) -> Int? {
        products.firstIndex { $0.id == cartProduct.id }
    }

    func findCartProduct(product: ProductProtocol) -> CartProduct? {
        products.first { $0.product.productID == product.productID }
    }

    func findCartProduct(id: String) -> CartProduct? {
        products.first { $0.id == id }
    }

    func updateCartProduct(_ cartProduct: CartProduct) {
        guard let idx = products.firstIndex(where: { $0.id == cartProduct.id }) else { return }
        products[idx] = cartProduct
    }

    func cleanup() {
        products = []
    }
}

extension CartProductManager.Product: Codable {
    enum ProductError: Error {
        case priceDecodingError
    }

    enum CodingKeys: String, CodingKey {
        case name = "name"
        case productID = "id"
        case price = "field_price"
        case description = "field_description"
        case productImageURL = "field_image"
        case entityType = "entity_type"
        case schedulable = "field_scheduled_fulfilment"
        case taxPercent = "field_tax_percent"
        case inventoryCount = "field_inventory_count"
    }

    public init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        name = try? map.decodeIfPresent(.name)
        productID = try map.decode(.productID)

        schedulable = (try? map.decodeLosslessIfPresent(.schedulable)) ?? false
        taxPercent = (try? map.decodeLosslessIfPresent(.taxPercent)) ?? 0
        price = (try? map.decodeLosslessIfPresent(.price)) ?? 0

        description = try? map.decodeIfPresent(.description)
        productImageURL = try? map.decodeIfPresent(.productImageURL)
        entityType = try map.decode(.entityType)
        inventoryCount = try? map.decodeLosslessIfPresent(.inventoryCount)
    }
}

extension CartProductManager.Product: Hashable, Differentiable {
    var differenceIdentifier: Int { hashValue }
}

extension CartProduct: Hashable, Differentiable {
    func hash(into hasher: inout Hasher) { hasher.combine(product.productID) }

    static func ==(lhs: CartProduct, rhs: CartProduct) -> Bool {
        lhs.product.productID == rhs.product.productID && lhs.quantity == rhs.quantity
    }
}