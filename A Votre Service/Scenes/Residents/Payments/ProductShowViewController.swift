//
// Created by Andrei Stoicescu on 16/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import Combine
import IQKeyboardManagerSwift
import SwiftyUserDefaults

protocol ProductShowViewControllerProtocol: class {
    func productShowViewControllerDidAddToCart()
}

class ProductShowViewController: ASDKViewController<ASDisplayNode> {
    weak var delegate: ProductShowViewControllerProtocol?

    var collectionNode: ASCollectionNode {
        node as! ASCollectionNode
    }

    var cartManager: CartProductManager

    var badgeLabel = UIBadgeLabel(image: Asset.Images.icCart.image)

    var product: ProductProtocol
    // CartProduct won't exist if the product is not in the cart already
    var cartProduct: CartProduct?

    let textField = UITextField(frame: .zero)

    private var subscriptions = Set<AnyCancellable>()

    init(cartManager: CartProductManager, product: ProductProtocol, cartProduct: CartProduct?) {
        self.product = product
        self.cartManager = cartManager
        self.cartProduct = cartProduct
        let collectionNode = ASCollectionNode.avsCollection()
        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc func hitDone() {
        guard let picker = textField.inputView as? UIDatePicker else { return }
        addCartProduct(CartProduct(product: product, quantity: 1, scheduledAt: picker.date))
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        textField.resignFirstResponder()
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        textField.resignFirstResponder()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(textField)
        textField.inputView = UIDatePicker.aLaCartePicker()
        textField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(hitDone))
        // textField.addKeyboardToolbarWithTarget(target: self,
        //                                        titleText: nil,
        //                                        rightBarButtonConfiguration: IQBarButtonItemConfiguration(title: "Done", action: #selector(hitDone)))

        replaceSystemBackButton()
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: badgeLabel)
        badgeLabel.tappedButton = { [weak self] in
            self?.viewCartAction()
        }
        updateBadgeLabel()

        cartManager.$totalProductsCount.receive(on: DispatchQueue.main)
                                       .sink { [weak self] count in
                                           self?.updateBadgeLabel()
                                       }.store(in: &subscriptions)

        navigationItem.title = L10n.ALaCarte.Navigation.title
    }

    @objc func viewCartAction() {
        if cartManager.config.openCartModal {
            let nav = AVSNavigationController(rootViewController: CartViewController(cartManager: cartManager))
            nav.dismissNavigation = {
                self.navigationController?.dismiss(animated: true)
            }
            navigationController?.present(nav, animated: true)
        } else {
            navigationController?.pushViewController(CartViewController(cartManager: cartManager), animated: true)
        }
    }

    func updateBadgeLabel() {
        badgeLabel.label.isHidden = cartManager.totalProductsCount == 0
        badgeLabel.label.text = cartManager.totalProductsCountString
    }

    func addCartProduct(_ cartProduct: CartProduct) {
        cartManager.add(product: cartProduct.product, quantity: cartProduct.quantity, scheduledAt: cartProduct.scheduledAt)
        let message: String
        if let name = cartProduct.product.name {
            message = "\(name) added to cart"
        } else {
            message = "Added to cart"
        }
        SVProgressHUD.showInfo(withStatus: message)
        SVProgressHUD.dismiss(withDelay: 1)
        delegate?.productShowViewControllerDidAddToCart()
    }
}

extension ProductShowViewController: ASCollectionDelegate, ASCollectionDataSource {
    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        1
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let product = self.product
        let description = product.description?.html?.string
        let currentQuantity = cartProduct?.quantity ?? 0
        return {
            let cell = CartProductNodes.ProductDetailCell(product: product, htmlDescription: description, currentQuantity: currentQuantity)
            cell.delegate = self
            return cell
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        ASSizeRange.fullWidth(collectionNode: collectionNode)
    }
}

extension ProductShowViewController: CartProductCellProtocol {
    func selectedButton(action: CartProductNodes.ButtonType, cartProduct: CartProduct) {
        if product.schedulable {
            textField.becomeFirstResponder()
        } else {
            addCartProduct(cartProduct)
        }
    }
}

extension UIDatePicker {
    static func aLaCartePicker() -> UIDatePicker {
        let datePicker = buildingTimezonePicker()
        datePicker.minimumDate = 1.hours.later
        return datePicker
    }

    static func buildingTimezonePicker() -> UIDatePicker {
        let datePicker = UIDatePicker()
        datePicker.timeZone = AppState.buildingTimeZone
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        return datePicker
    }
}