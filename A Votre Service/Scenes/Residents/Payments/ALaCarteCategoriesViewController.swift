//
// Created by Andrei Stoicescu on 10/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import Combine
import DifferenceKit

class ALaCarteCategoriesViewController: ASDKViewController<ASDisplayNode> {

    class MenuItemCell: ASCellNode {
        var backgroundNode = ASDisplayNode()
        var iconNode = ASNetworkImageNode()
        var newIcon = UIImage()
        var labelNode = ASTextNode2()

        init(itemType: ALaCarteCategory) {
            backgroundNode.cornerRadius = 4

            iconNode.image = UIImage(named: itemType.name!)
            print("La imagen es: \(itemType.name!)")
            iconNode.cornerRadius = 4
            iconNode.contentMode = .scaleAspectFit
            

            labelNode.attributedText = itemType.name?.attributed(.bodyTitleHeavy, .primaryLabel)
            labelNode.maximumNumberOfLines = 1

            super.init()
            automaticallyManagesSubnodes = true
        }

        var margin: UIEdgeInsets {
            if let item = indexPath?.item {
                return item % 2 == 0 ? UIEdgeInsets(top: 0, left: 15, bottom: 15, right: 7) : UIEdgeInsets(top: 0, left: 7, bottom: 15, right: 15)
            }
            return UIEdgeInsets(top: 0, left: 10, bottom: 10, right: 10)
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            [
//                iconNode.ratio(3 / 4).margin(horizontal: 15).margin(top: 15),
                iconNode.ratio(1 / 2).margin(horizontal: 20).margin(top: 30),
                labelNode
            ].vStacked().spaced(30).aligned(.center)
             .margin(bottom: 10)
             .background(backgroundNode)
             .margin(margin)
        }
    }
    
    var collectionNode: ASCollectionNode

    var items = [ALaCarteCategory]()
    lazy var refreshControl = UIRefreshControl()

    var badgeLabel = UIBadgeLabel(image: Asset.Images.Concierge.icMore.image.withRenderingMode(.alwaysTemplate))
    var cartManager: CartProductManager

    lazy var emptyNode = EmptyNode(emptyNodeType: .productList)

    private var isMainCategory = true

    private var subscriptions = Set<AnyCancellable>()

    var closeAction: (() -> Void)?
    
    lazy var navigationNode = UserNotificationsNodes.Navigation()

    convenience init(cartManager: CartProductManager) {
        self.init(cartManager: cartManager, items: nil)
    }

    init(cartManager: CartProductManager, items: [ALaCarteCategory]? = nil) {
        self.cartManager = cartManager
        collectionNode = .avsCollection()
        self.items = items ?? []
        isMainCategory = items == nil
        
        let mainNode = ASDisplayNode()
        mainNode.automaticallyManagesSubnodes = true

        super.init(node: mainNode)
        
        mainNode.layoutSpecBlock = { [weak self] node, range in
            guard let self = self else { return ASDisplayNode().wrapped() }
            return [
                self.navigationNode,
                self.collectionNode.growing()
            ].vStacked()
        }

        collectionNode.showsVerticalScrollIndicator = false
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func updateBadgeLabel() {
        badgeLabel.label.isHidden = cartManager.totalProductsCount == 0
        badgeLabel.label.text = cartManager.totalProductsCountString
    }

    func reloadData(_ target: [ALaCarteCategory]) {
        let source = items

        let stagedChangeSet = StagedChangeset(source: source, target: target)
        for changeSet in stagedChangeSet {
            items = changeSet.data
            collectionNode.performDiffBatchUpdates(changeSet: changeSet)
        }
    }

    @objc func viewCartAction() {
        if cartManager.config.openCartModal {
            let nav = AVSNavigationController(rootViewController: CartViewController(cartManager: cartManager))
            nav.dismissNavigation = {
                self.navigationController?.dismiss(animated: true)
            }
            navigationController?.present(nav, animated: true)
        } else {
            navigationController?.pushViewController(CartViewController(cartManager: cartManager), animated: true)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()

        cartManager.$totalProductsCount.receive(on: DispatchQueue.main)
                                       .sink { [weak self] count in
                                           self?.updateBadgeLabel()
                                       }.store(in: &subscriptions)

//        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: badgeLabel)
//        badgeLabel.tappedButton = { [weak self] in
//            self?.viewCartAction()
//        }
        
        let lateralMenu = LateralMenuView(frame: CGRect(x: 80, y: 0, width: 347, height: UIScreen.main.bounds.height))
        let myView = HeaderTabBarView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 190), tabBarVC: self, lm: lateralMenu)
        lateralMenu.isHidden = true
        lateralMenu.header = myView
        lateralMenu.rootView = self
        self.view.addSubview(myView)
        self.view.addSubview(lateralMenu)

        updateBadgeLabel()

        collectionNode.view.backgroundView = emptyNode.view
        emptyNode.actionButtonTapped = { [weak self] in
            guard let tabBarController = self?.tabBarController as? ResidentTabBarViewController else { return }
            tabBarController.switchTo(controllerType: .chat)
        }

        showEmptyScreen(false)

        if isMainCategory {
            refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)
            collectionNode.view.addSubview(refreshControl)
            loadCachedItems()
            if items.count == 0 {
                SVProgressHUD.show()
            }
            refreshItems()
        }

        if closeAction != nil {
            navigationItem.leftBarButtonItem = UIBarButtonItem(title: L10n.General.close, style: .plain, target: self, action: #selector(didTapClose))
        }
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        navigationController?.setNavigationBarHidden(true, animated: false)
//    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        navigationController?.setNavigationBarHidden(false, animated: true)
//    }

    @objc func didTapClose() {
        closeAction?()
    }

    func loadCachedItems() {
        if let items = CodableCache.get([ALaCarteCategory].self, key: "a_la_carte_main_category") {
            reloadData(items)
        }
    }

    @objc func refreshItems() {
        ResidentService().getALaCarteCategories().done { [weak self] categories in
            guard let self = self else { return }
            SVProgressHUD.dismiss()
            self.reloadData(categories)
            CodableCache.set(categories, key: "a_la_carte_main_category")
        }.ensure { [weak self] in
            guard let self = self else { return }
            self.showEmptyScreen(self.items.count == 0)
            self.refreshControl.endRefreshing()
        }.catch { error in
            SVProgressHUD.showError(withStatus: error.localizedDescription)
            Log.this(error)
        }
    }

    func showEmptyScreen(_ show: Bool) {
        emptyNode.isHidden = !show
    }
}

extension ALaCarteCategoriesViewController: ASCollectionDelegate, ASCollectionDataSource {
    
    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        items.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let itemType = items[indexPath.item]
        return {
            MenuItemCell(itemType: itemType)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        let category = items[indexPath.item]
        if category.children.count > 0 {
            let vc = ALaCarteCategoriesViewController(cartManager: cartManager, items: category.children)
            navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = ProductListViewController(cartManager: cartManager, category: category)
            vc.didTapClose = { [weak self] in
                guard let self = self else { return }
                self.navigationController?.popToViewController(self, animated: true)
            }
            navigationController?.pushViewController(vc, animated: true)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .halfWidth(collectionNode: collectionNode, right: indexPath.row % 2 == 0)
    }
}

