//
// Created by Andrei Stoicescu on 10/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import PromiseKit
import Combine
import DifferenceKit

class ProductListViewController: ASDKViewController<ASDisplayNode> {

    struct ProductModel: Hashable, Differentiable {
        static func ==(lhs: ProductModel, rhs: ProductModel) -> Bool {
            lhs.product.productID == rhs.product.productID && lhs.currentQuantity == rhs.currentQuantity
        }

        let product: ProductProtocol
        let cartProduct: CartProduct?
        let currentQuantity: Int

        func hash(into hasher: inout Hasher) { hasher.combine(product.productID) }

        var differenceIdentifier: Int { hashValue }
    }

    var emptyNode: EmptyNode

    var didTapClose: (() -> Void)?

    var collectionNode: ASCollectionNode
    lazy var refreshControl = UIRefreshControl()

    var badgeLabel = UIBadgeLabel(image: Asset.Images.icCart.image)

    var dataSource: ObservablePaginatedDataSource<CartProductManager.Product>
    var products = [ProductModel]()

    var cartManager: CartProductManager
    var category: ALaCarteCategory

    private var subscriptions = Set<AnyCancellable>()
    private let textField = UITextField(frame: .zero)
    private var selectedQuickScheduleProduct: ProductProtocol?

    init(cartManager: CartProductManager, category: ALaCarteCategory, emptyNodeAction: Bool = true) {
        self.category = category
        self.cartManager = cartManager
        emptyNode = EmptyNode(emptyNodeType: .productList, showAction: emptyNodeAction)

        dataSource = .init { per, page in
            ResidentService().getALaCarteItems(categoryID: category.categoryID, page: page, per: per)
        }

        dataSource.cacheable = true
        dataSource.cacheKey = "products_category_\(category.categoryID)"

        collectionNode = .avsCollection()
        super.init(node: collectionNode)

        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func collectionView(_ collectionView: ASCollectionView, willDisplayNodeForItemAt indexPath: IndexPath) {
        dataSource.loadMoreContentIfNeeded(currentItem: dataSource.items[indexPath.item])
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        textField.resignFirstResponder()
    }

    @objc func hitDone() {
        guard let picker = textField.inputView as? UIDatePicker,
              let selectedProduct = selectedQuickScheduleProduct else { return }
        cartManager.add(product: selectedProduct, quantity: 1, scheduledAt: picker.date)
        selectedQuickScheduleProduct = nil
        let message: String
        if let name = selectedProduct.name {
            message = "\(name) added to cart"
        } else {
            message = "Added to cart"
        }
        SVProgressHUD.showInfo(withStatus: message)
        SVProgressHUD.dismiss(withDelay: 1)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)
        collectionNode.view.addSubview(refreshControl)

        view.addSubview(textField)
        let datePicker = UIDatePicker.aLaCartePicker()

        textField.inputView = datePicker
        textField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(hitDone))

        collectionNode.view.backgroundView = emptyNode.view
        emptyNode.actionButtonTapped = { [weak self] in
            self?.didTapClose?()
            let tabBarController = self?.tabBarController as? ResidentTabBarViewController
            tabBarController?.switchTo(controllerType: .chat)
        }

        replaceSystemBackButton()
        navigationItem.title = L10n.ALaCarte.Navigation.title

        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: badgeLabel)
        badgeLabel.tappedButton = { [weak self] in
            self?.viewCartAction()
        }

        updateBadgeLabel()
        showEmptyScreen(false)

        setupBindings()
        refreshItems()
    }

    func setupBindings() {
        cartManager.$products.receive(on: DispatchQueue.main).sink { [weak self] products in
            self?.reloadData()
        }.store(in: &subscriptions)

        cartManager.$totalProductsCount.receive(on: DispatchQueue.main)
                                       .sink { [weak self] count in
                                           self?.updateBadgeLabel()
                                       }.store(in: &subscriptions)

        dataSource.$items
                .receive(on: DispatchQueue.main)
                .sink { [weak self] items in
                    self?.reloadData()
                }.store(in: &subscriptions)

        dataSource.$showLoadingIndicator
                .receive(on: DispatchQueue.main)
                .sink { [weak self] loading in
                    guard let self = self else { return }

                    if loading {
                        if !self.refreshControl.isRefreshing {
                            SVProgressHUD.show()
                        }
                    } else {
                        SVProgressHUD.dismiss()
                        self.refreshControl.endRefreshing()
                    }
                }.store(in: &subscriptions)

        dataSource.$hasNoItemsAfterLoading.receive(on: DispatchQueue.main)
                                          .sink { [weak self] noItems in
                                              guard let self = self else { return }
                                              self.showEmptyScreen(noItems)
                                          }.store(in: &subscriptions)
    }

    func reloadData() {
        let newProducts = dataSource.items.map { product -> ProductModel in
            let cartProduct = cartManager.findCartProduct(product: product)
            return ProductModel(product: product, cartProduct: cartProduct, currentQuantity: cartProduct?.quantity ?? 0)
        }

        let stagedChangeSet = StagedChangeset(source: products, target: newProducts)
        for changeSet in stagedChangeSet {
            products = changeSet.data
            collectionNode.performDiffBatchUpdates(changeSet: changeSet)
        }
    }

    func showEmptyScreen(_ show: Bool) {
        emptyNode.isHidden = !show
    }

    func updateBadgeLabel() {
        badgeLabel.label.isHidden = cartManager.totalProductsCount == 0
        badgeLabel.label.text = cartManager.totalProductsCountString
    }

    @objc func viewCartAction() {
        if cartManager.config.openCartModal {
            let nav = AVSNavigationController(rootViewController: CartViewController(cartManager: cartManager))
            nav.dismissNavigation = {
                self.navigationController?.dismiss(animated: true)
            }
            navigationController?.present(nav, animated: true)
        } else {
            navigationController?.pushViewController(CartViewController(cartManager: cartManager), animated: true)
        }
    }

    @objc func refreshItems() {
        dataSource.loadMoreContentIfNeeded()
    }
}

extension ProductListViewController: ASCollectionDelegate, ASCollectionDataSource {

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        products.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let productModel = products[indexPath.item]
        let description = productModel.product.description?.html?.string
        return {
            let button = ConciergeGenericNodes.PurchaseButton(icon: Asset.Images.icCartPlus.image, label: productModel.product.price.currencyDollarClean)

            button.onTap = { [weak self] in
                guard let self = self else { return }
                let product = productModel.product

                if product.schedulable {
                    self.selectedQuickScheduleProduct = product
                    self.textField.placeholder = product.name
                    self.textField.becomeFirstResponder()
                } else {
                    self.cartManager.add(product: product)
                }
            }

            let cartProduct = CartProduct(product: productModel.product)
            if cartProduct.canSetQuantityToCart(productModel.currentQuantity + 1) {
                button.alpha = 1
                button.isUserInteractionEnabled = true
            } else {
                button.alpha = 0.3
                button.isUserInteractionEnabled = false
            }

            let cell = ConciergeThingsToKnowListViewController.HorizontalModeCell(title: productModel.product.name, body: description, url: productModel.product.productImageURL)
            cell.bottomNode = button
            cell.thumbAspectRatio = 11 / 13

            return cell
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        textField.resignFirstResponder()

        let productModel = products[indexPath.item]
        let product = productModel.product
        let vc = ProductShowViewController(cartManager: cartManager, product: product, cartProduct: cartManager.findCartProduct(product: product))
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        ASSizeRange.fullWidth(collectionNode: collectionNode)
    }
}

extension ProductListViewController: ProductShowViewControllerProtocol {
    func productShowViewControllerDidAddToCart() {
        navigationController?.popToViewController(self, animated: true)
    }
}
