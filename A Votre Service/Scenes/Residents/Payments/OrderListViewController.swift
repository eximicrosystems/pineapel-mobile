//
// Created by Andrei Stoicescu on 20/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit
import SwiftyUserDefaults
import AsyncDisplayKit
import SVProgressHUD
import PromiseKit
import Combine
import DifferenceKit

class OrderListViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {
    lazy var emptyNode = EmptyNode(emptyNodeType: .orderHistory, showAction: false)

    lazy var refreshControl = UIRefreshControl()
    var collectionNode: ASCollectionNode

    var dataSource: ObservablePaginatedDataSource<OrderData>
    var orders = [OrderData]()
    private var subscriptions = Set<AnyCancellable>()
    
    override init() {
        collectionNode = .avsCollection()
        dataSource = .init()

        super.init(node: collectionNode)
        
        dataSource.paginate = { per, page in
            ResidentService().getOrderList(page: page, per: per)
        }

        dataSource.cacheable = true
        dataSource.cacheKey = "resident_order_list"

        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tabBar = BottonTabBarView(frame: CGRect(x: 0, y: UIScreen.main.bounds.height-183, width: UIScreen.main.bounds.width, height: 83))
        self.view.addSubview(tabBar)
        navigationItem.title = L10n.MoreScreen.Items.orderHistory
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "chevron.left"), style: .plain, target: self, action: #selector(backView))

        setupBindings()
        replaceSystemBackButton()
        showEmptyScreen(false)
        collectionNode.view.backgroundView = emptyNode.view
        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)
        collectionNode.view.addSubview(refreshControl)
        refreshItems()
    }
    
    @objc func backView(){
        AppState.switchTo(viewController: ResidentTabBarViewController(page: nil))
    }

    func reloadData() {
        let source = orders
        let target = dataSource.items

        let stagedChangeSet = StagedChangeset(source: source, target: target)
        for changeSet in stagedChangeSet {
            orders = changeSet.data
            collectionNode.performDiffBatchUpdates(changeSet: changeSet)
        }
    }

    func showEmptyScreen(_ show: Bool) {
        emptyNode.isHidden = !show
    }

    func setupBindings() {
        dataSource.$items
                .receive(on: DispatchQueue.main)
                .sink { [weak self] items in
                    self?.reloadData()
                }.store(in: &subscriptions)

        dataSource.$hasNoItemsAfterLoading
                .receive(on: DispatchQueue.main)
                .sink { [weak self] hasNoItemsAfterLoading in
                    self?.showEmptyScreen(hasNoItemsAfterLoading)
                }.store(in: &subscriptions)

        dataSource.$showLoadingIndicator
                .receive(on: DispatchQueue.main)
                .sink { [weak self] loading in
                    guard let self = self else { return }

                    if loading {
                        if !self.refreshControl.isRefreshing {
                            SVProgressHUD.show()
                        }
                    } else {
                        SVProgressHUD.dismiss()
                        self.refreshControl.endRefreshing()
                    }
                }.store(in: &subscriptions)
    }

    @objc func refreshItems() {
        dataSource.loadMoreContentIfNeeded()
    }

    func collectionView(_ collectionView: ASCollectionView, willDisplayNodeForItemAt indexPath: IndexPath) {
        dataSource.loadMoreContentIfNeeded(currentItem: dataSource.items[indexPath.item])
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        orders.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let order = orders[indexPath.item]
        return {
            ConciergePagerListNodes.RequestCell(order: order)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        let orderData = orders[indexPath.item]
        let nav = AVSNavigationController(rootViewController: OrderSummaryViewController(orderData: orderData))
        nav.dismissNavigation = { [weak self] in
            self?.navigationController?.dismiss(animated: true)
        }
        navigationController?.present(nav, animated: true)
    }
}
