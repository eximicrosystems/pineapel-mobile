//
// Created by Andrei Stoicescu on 15/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import Stripe
import SVProgressHUD
import PassKit
import SwiftUI
import DifferenceKit

class CartReviewManager {

    enum ReviewProduct: Hashable, Differentiable {
        case scheduleProduct(product: CartProduct)
        case cartProduct(product: CartProduct)
        case paymentMethod(method: PaymentMethodSelection)
        case discountCodes(codes: [String])
        case discountCodeItem(OrderResponseItem)

        func hash(into hasher: inout Hasher) {
            switch self {
            case .scheduleProduct(let product):
                hasher.combine(product.id)
            case .cartProduct(let product):
                hasher.combine(product.id)
            case .paymentMethod(let method):
                hasher.combine(method.typeString ?? "")
            case .discountCodes(let codes):
                hasher.combine(codes.joined(separator: ""))
            case .discountCodeItem(let item):
                hasher.combine(item.id)
            }
        }
    }

    var checkoutData: CartCheckoutFlow.CheckoutData
    var items = [ReviewProduct]()
    var discountCodes = [String]() { didSet { buildItems() } }
    var orderItemResponses = [OrderResponseItem]() { didSet { buildItems() } }

    var invalidDiscountCodes: [String] {
        discountCodes.filter { code in
            orderItemResponses.first { $0.discountCode?.lowercased() == code.lowercased() } == nil
        }
    }

    init(checkoutData: CartCheckoutFlow.CheckoutData) {
        self.checkoutData = checkoutData
        buildItems()
    }

    func buildItems() {
        items = checkoutData.cartManager.products.map { cartProduct in
            if cartProduct.product.schedulable {
                return .scheduleProduct(product: cartProduct)
            } else {
                return .cartProduct(product: cartProduct)
            }
        }

        items.append(.discountCodes(codes: discountCodes))

        orderItemResponses.filter { $0.itemType == .discountCode }.forEach { item in
            items.append(.discountCodeItem(item))
        }

        if let method = checkoutData.paymentMethod {
            items.append(.paymentMethod(method: method))
        }
    }

    func buildOrder(paymentMethodID: String? = nil) -> OrderRequestData {
        var orderItems = [OrderRequestItem]()

        for item in items {
            switch item {
            case .scheduleProduct(let product), .cartProduct(let product):
                orderItems.append(product.orderRequestItem)
            case .paymentMethod, .discountCodes, .discountCodeItem:
                ()
            }
        }

        return OrderRequestData(paymentMethodID: paymentMethodID, items: orderItems, discountCodes: discountCodes)
    }
}

class CartCheckoutReviewViewController: ASDKViewController<ASDisplayNode>, DiscountCodesSelectable {
    private typealias OrderSummary = PaymentSummary.OrderSummary

    private enum Sections: Int, CaseIterable, Differentiable {
        case items
        case summary
        case save
    }

    private var collectionNode: ASCollectionNode
    private var checkoutData: CartCheckoutFlow.CheckoutData
    private var reviewManager: CartReviewManager
    private var summary: OrderSummary?

    private var _forceSaveButtonDisabled = false

    private var saveButtonDisabled: Bool {
        _forceSaveButtonDisabled || summary == nil
    }

    private var sections = [ArraySection<Sections, AnyDifferentiable>]()

    init(checkoutData: CartCheckoutFlow.CheckoutData) {
        collectionNode = .avsCollection()
        self.checkoutData = checkoutData

        reviewManager = CartReviewManager(checkoutData: checkoutData)

        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        replaceSystemBackButton()
        navigationItem.title = L10n.ALaCarte.Navigation.title
        calculateOrder()
    }

    func reloadData() {
        let items = reviewManager.items.map { AnyDifferentiable($0) }
        let summaryItems: [AnyDifferentiable] = summary != nil ? [AnyDifferentiable(summary!)] : []
        let saveItems: [AnyDifferentiable] = summary != nil ? [AnyDifferentiable("save")] : []

        let target: [ArraySection<Sections, AnyDifferentiable>] = [
            ArraySection(model: .items, elements: items),
            ArraySection(model: .summary, elements: summaryItems),
            ArraySection(model: .save, elements: saveItems),
        ]

        let stagedChangeSet = StagedChangeset(source: sections, target: target)
        for changeSet in stagedChangeSet {
            sections = changeSet.data
            collectionNode.performDiffBatchUpdates(changeSet: changeSet)
        }
    }

    func calculateOrder() {
        SVProgressHUD.show()
        let order = reviewManager.buildOrder()
        PaymentSummary.calculateOrder(order).done { [weak self] summary, responses in
            guard let self = self else { return }
            SVProgressHUD.dismiss()
            self.summary = summary
            self.reviewManager.orderItemResponses = responses
            self.reloadData()
            let invalidCodes = self.reviewManager.invalidDiscountCodes
            if invalidCodes.count > 0 {
                SVProgressHUD.showError(withStatus: "The following discount \(invalidCodes.count == 1 ? "code doesn't" : "codes don't") apply to any item: \(invalidCodes.joined(separator: ", "))")
            }
        }.catch { error in
            SVProgressHUD.showError(withStatus: error.localizedDescription)
            Log.thisError(error)
        }
    }

    func dismissCodesDidFinish(_ codes: [String]) {
        reviewManager.discountCodes = codes
        reloadData()
        navigationController?.popToViewController(self, animated: true)
        calculateOrder()
    }
}

extension CartCheckoutReviewViewController: ASCollectionDelegate, ASCollectionDataSource {
    public func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
        sections.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        sections[section].elements.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let section = sections[indexPath.section]

        switch section.model {
        case .items:
            guard let product = section.elements[indexPath.item].base as? CartReviewManager.ReviewProduct else { return { ASCellNode() } }
            return {
                switch product {
                case .discountCodeItem(let item):
                    return GenericNodes.TitleSubtitleCell(title: item.name,
                                                          subtitle: item.totalPriceWithoutTax.currencyDollarClean)
                case .scheduleProduct(let product):
                    return ConciergePagerListNodes.RequestCell(title: product.product.name, subject: product.scheduledAt?.avsTimestampString(timeZone: AppState.buildingTimeZone), imageUrl: product.product.productImageURL)
                case .cartProduct(let product):
                    return ConciergePagerListNodes.RequestCell(title: product.product.name, subject: "Quantity: \(product.quantity)", imageUrl: product.product.productImageURL)
                case .discountCodes(let codes):
                    return GenericNodes.TitleSubtitleCell(title: "Discount Codes",
                                                          subtitle: codes.count > 0 ? codes.joined(separator: ", ") : "Tap to Add Discount Codes")
                case .paymentMethod(let method):
                    return GenericNodes.TitleSubtitleCell(title: "Payment", subtitle: method.typeString)
                }
            }
        case .save:
            return { GenericNodes.PrimaryButtonCellNode(title: "Place Order") }
        case .summary:
            guard let summary = summary else { return { ASCellNode() } }
            return { PaymentSummary.Cell(summary: summary) }
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        let section = sections[indexPath.section]

        switch section.model {
        case .save:
            if !saveButtonDisabled {
                finalizeOrder()
            }
        case .items:
            guard let item = section.elements[indexPath.item].base as? CartReviewManager.ReviewProduct else { return }

            switch item {
            case .discountCodes(let codes):
                navigationController?.pushViewController(UIHostingController(rootView: DiscountCodesFormView(codes: codes.count > 0 ? codes : [""], delegate: self)), animated: true)
            default:
                ()
            }
        default:
            ()
        }
    }



    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }

    func finalizeOrder() {
        var order = reviewManager.buildOrder()
        guard let paymentMethod = checkoutData.paymentMethod else {
            SVProgressHUD.showError(withStatus: "Payment method error")
            return
        }

        _forceSaveButtonDisabled = true

        switch paymentMethod {

        case .applePay:
            payWithApplePay()
        case .paymentMethod(let method):
            order.paymentMethodID = method.stripeId
            SVProgressHUD.show()
            ResidentService().createOrder(params: order).done { response in
                self.payWithCard(response: response)
                SVProgressHUD.dismiss()
            }.catch { error in
                self._forceSaveButtonDisabled = false
                SVProgressHUD.showError(withStatus: error.localizedDescription)
                Log.thisError(error)
            }
        }
    }

    func payWithApplePay() {
        guard let summary = summary else { return }

        let merchantIdentifier = Configuration.default.aVotreService.app.applePayMerchantID

        let paymentRequest = StripeAPI.paymentRequest(withMerchantIdentifier: merchantIdentifier, country: "US", currency: "USD")

        // Configure the line items on the payment request
        paymentRequest.paymentSummaryItems = [
            // The final line should represent your company;
            // it'll be prepended with the word "Pay" (i.e. "Pay iHats, Inc $50")
            PKPaymentSummaryItem(label: "A Votre Service LLC", amount: NSDecimalNumber(string: String(summary.total))),
        ]

        // Initialize an STPApplePayContext instance
        if let applePayContext = STPApplePayContext(paymentRequest: paymentRequest, delegate: self) {
            // Present Apple Pay payment sheet
            applePayContext.presentApplePay() {

            }
        } else {
            SVProgressHUD.showError(withStatus: "There is a problem with the Apple Pay configuration")
            self._forceSaveButtonDisabled = false
            // There is a problem with your Apple Pay configuration
        }
    }

    func payWithCard(response: OrderClientSecretResponse) {
        let paymentIntentParams = STPPaymentIntentParams(clientSecret: response.clientSecret)

        // Submit the payment
        let paymentHandler = STPPaymentHandler.shared()
        paymentHandler.confirmPayment(paymentIntentParams, with: self) { (status, paymentIntent, error) in
            self._forceSaveButtonDisabled = false

            switch (status) {
            case .failed:
                SVProgressHUD.showError(withStatus: error?.localizedDescription)
                Log.thisError(error)
                break
            case .canceled:
                Log.this("Payment canceled")
                Log.thisError(error)
                break
            case .succeeded:
                Log.this("Payment succeeded")
                self.handlePaymentSucceeded()
                break
            @unknown default:
                SVProgressHUD.showError(withStatus: "Unknown Error")
                break
            }
        }
    }

    func handlePaymentSucceeded() {
        _forceSaveButtonDisabled = true
        SVProgressHUD.dismiss()
        navigationController?.pushViewController(CartPaymentDoneViewController(checkoutData: checkoutData), animated: true)
    }
}

extension CartCheckoutReviewViewController: STPAuthenticationContext {
    func authenticationPresentingViewController() -> UIViewController {
        self
    }
}

extension CartCheckoutReviewViewController: STPApplePayContextDelegate {
    func applePayContext(_ context: STPApplePayContext, didCreatePaymentMethod paymentMethod: STPPaymentMethod, paymentInformation: PKPayment, completion: @escaping STPIntentClientSecretCompletionBlock) {
        var order = reviewManager.buildOrder()
        order.paymentMethodID = paymentMethod.stripeId
        ResidentService().createOrder(params: order).done { response in
            completion(response.clientSecret, nil)
        }.catch { error in
            completion(nil, error)
            Log.thisError(error)
        }
    }

    func applePayContext(_ context: STPApplePayContext, didCompleteWith status: STPPaymentStatus, error: Error?) {
        _forceSaveButtonDisabled = false
        switch status {
        case .success:
            Log.this("Success")
            handlePaymentSucceeded()
        case .error:
            SVProgressHUD.showError(withStatus: error?.localizedDescription)
            Log.thisError(error)
        case .userCancellation:
            Log.this("user canceled")
        @unknown default:
            ()
        }
    }

}
