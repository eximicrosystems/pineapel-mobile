//
// Created by Andrei Stoicescu on 16/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD

class CartPaymentDoneViewController: ASDKViewController<ASDisplayNode> {

    enum Items: Int, CaseIterable {
        case thankYou
        case done
    }

    private var collectionNode: ASCollectionNode
    private var products: [CartProduct]

    init(checkoutData: CartCheckoutFlow.CheckoutData) {
        products = checkoutData.cartManager.products
        checkoutData.cartManager.cleanup()
        let collectionNode = ASCollectionNode.avsCollection()
        self.collectionNode = collectionNode

        let bgImageNode = ASImageNode(image: Asset.Images.bgSplash.image)

        let mainNode = ASDisplayNode()
        mainNode.automaticallyManagesSubnodes = true
        mainNode.layoutSpecBlock = { node, range in
            collectionNode.background(bgImageNode)
        }

        super.init(node: mainNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.setHidesBackButton(true, animated: false)
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: L10n.General.close, style: .plain, target: self, action: #selector(didTapClose))
        navigationItem.title = L10n.ALaCarte.Navigation.title
    }

    @objc func didTapClose() {
        guard let nav = navigationController as? AVSNavigationController else { return }
        nav.dismissNavigation?()
    }
}

extension CartPaymentDoneViewController: ASCollectionDelegate, ASCollectionDataSource {

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        Items.allCases.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        guard let item = Items(rawValue: indexPath.item) else { return { ASCellNode() } }

        switch item {
        case .thankYou:
            let itemsString = ListFormatter.localizedString(byJoining: products.compactMap { cartProduct in
                guard let productName = cartProduct.product.name else { return nil }
                return "\(String(cartProduct.quantity))x \(productName)"
            })

            return {
                AmenityThankYouViewController.ThankYouCell(title: "Thank you for your order!",
                                                            body: "Your order has been confirmed.\nA La Carte items includes: \(itemsString)")
            }
        case .done:
            return {
                AmenityThankYouViewController.ThankYouDoneCell()
            }
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        if indexPath.item == Items.done.rawValue {
            didTapClose()
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        ASSizeRange.fullWidth(collectionNode: collectionNode)
    }
}
