//
// Created by Andrei Stoicescu on 03/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit
import Stripe
import PromiseKit
import SwiftyUserDefaults
import SVProgressHUD

class AddCardViewController: UIViewController {

    var onCancelAction: (() -> Void)?
    var onAddedSuccessAction: (() -> Void)?

    lazy var cardTextField: STPPaymentCardTextField = {
        let cardTextField = STPPaymentCardTextField()
        cardTextField.font = AVSFont.bodyRoman.font
        cardTextField.textColor = AVSColor.primaryLabel.color
        cardTextField.textErrorColor = AVSColor.accentBackground.color
        cardTextField.placeholderColor = AVSColor.primaryIcon.color
        cardTextField.borderColor = AVSColor.inputBorder.color

        return cardTextField
    }()

    lazy var addCardLabel: UILabel = {
        let label = UILabel()
        label.font = AVSFont.bodyTitleHeavy.font
        label.textColor = AVSColor.primaryLabel.color
        label.text = "Enter your card details"
        return label
    }()

    lazy var saveCardButton = UIButton.primaryButton(title: "Save Credit Card")

    var settingUpCard = false

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if traitCollection.userInterfaceStyle != previousTraitCollection?.userInterfaceStyle {
            cardTextField.borderColor = AVSColor.inputBorder.color
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Add New Card"

        cardTextField.delegate = self
        updateButtonState(valid: false)

        navigationItem.leftBarButtonItem = UIBarButtonItem(title: L10n.General.cancel, style: .plain, target: self, action: #selector(didTapCancel))

        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 30

        stackView.addArrangedSubviews(subviews: [addCardLabel, cardTextField, saveCardButton])

        stackView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(stackView)
        stackView.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.topMargin).offset(20)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
        }

        saveCardButton.snp.makeConstraints { maker in
            maker.height.equalTo(50)
        }

        view.backgroundColor = AVSColor.secondaryBackground.color

        saveCardButton.addTarget(self, action: #selector(setupCardAction), for: .touchUpInside)
    }

    @objc private func didTapCancel() {
        onCancelAction?()
    }

    @objc private func setupCardAction() {
        guard !settingUpCard, cardTextField.isValid else { return }
        settingUpCard = true

        when(fulfilled: StripeProvider().getCustomerData(), ResidentService().createSetupIntent()).done { customerData, setupIntent in
            self.setupCard(intent: setupIntent, customerData: customerData)
        }.catch { error in
            SVProgressHUD.showError(withStatus: error.localizedDescription)
            Log.thisError(error)
            self.settingUpCard = false
        }
    }

    func setupCard(intent: StripeSetupIntent, customerData: StripeProvider.CustomerData) {

        // Collect card details
        let cardParams = cardTextField.cardParams

        // Collect the customer's email to know which customer the PaymentMethod belongs to.
        let billingDetails = STPPaymentMethodBillingDetails()
        billingDetails.email = customerData.email

        // Create SetupIntent confirm parameters with the above
        let paymentMethodParams = STPPaymentMethodParams(card: cardParams, billingDetails: billingDetails, metadata: nil)
        let setupIntentParams = STPSetupIntentConfirmParams(clientSecret: intent.clientSecret)
        setupIntentParams.paymentMethodParams = paymentMethodParams

        // Complete the setup
        SVProgressHUD.show()
        let paymentHandler = STPPaymentHandler.shared()
        paymentHandler.confirmSetupIntent(setupIntentParams, with: self) { [weak self] status, setupIntent, error in
            self?.settingUpCard = false

            switch (status) {
            case .failed:
                SVProgressHUD.showError(withStatus: error?.localizedDescription)
                Log.thisError(error)
                break
            case .canceled:
                Log.thisError(error)
                break
            case .succeeded:
                self?.onAddedSuccessAction?()
                break
            @unknown default:
                fatalError()
                break
            }
        }
    }

    func updateButtonState(valid: Bool) {
        if valid {
            saveCardButton.alpha = 1
        } else {
            saveCardButton.alpha = 0.3
        }
    }
}

extension AddCardViewController: STPAuthenticationContext {
    func authenticationPresentingViewController() -> UIViewController {
        self
    }
}

extension AddCardViewController: STPPaymentCardTextFieldDelegate {
    public func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
        updateButtonState(valid: textField.isValid)
    }
}
