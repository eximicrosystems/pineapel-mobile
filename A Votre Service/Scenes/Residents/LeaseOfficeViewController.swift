//
// Created by Andrei Stoicescu on 23/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import SwiftyUserDefaults
import PromiseKit
import AsyncDisplayKit
import SVProgressHUD

class LeaseOfficeViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

    enum LeaseOfficeSections: Int, CaseIterable {
        case building
        case usersHeader
        case users
    }

    lazy var emptyNode = EmptyNode(emptyNodeType: .leaseOffice)

    var buildingData: LeaseOfficeData?
    var users: [LeaseOfficeData] = []

    var collectionNode: ASCollectionNode {
        node as! ASCollectionNode
    }

    lazy var refreshControl = UIRefreshControl()

    override init() {
        let collectionNode = ASCollectionNode.avsCollection()
        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func showEmptyScreen(_ show: Bool) {
        emptyNode.isHidden = !show
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()
        collectionNode.view.backgroundView = emptyNode.view
        showEmptyScreen(false)
        navigationItem.title = "Leasing Office"

        emptyNode.actionButtonTapped = { [weak self] in
            guard let tabBarController = self?.tabBarController as? ResidentTabBarViewController else { return }
            tabBarController.switchTo(controllerType: .chat)
        }

        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)

        collectionNode.view.addSubview(refreshControl)

        if !loadFromCache() {
            SVProgressHUD.show()
        }

        refreshItems()
    }

    func loadFromCache() -> Bool {
        buildingData = CodableCache.get(LeaseOfficeData.self, key: "lease_office_building")
        if let users = CodableCache.get([LeaseOfficeData].self, key: "lease_office_users") {
            self.users = users
        }
        if buildingData != nil && users.count > 0 {
            collectionNode.reloadData()
            return true
        }

        return false
    }

    @objc func refreshItems() {
        var didChangeUsers = false
        var didChangeLeasingData = false

        let apiService = ResidentService()
        apiService.getLeasingOfficeDetails().get { data in
            if data != self.buildingData {
                didChangeLeasingData = true
                self.buildingData = data
                CodableCache.set(data, key: "lease_office_building")
            }
        }.then { data in
            apiService.getLeasingOfficeTeam(officeID: data.buildingID)
        }.done { users in
            if users != self.users {
                didChangeUsers = true
                CodableCache.set(users, key: "lease_office_users")
                self.users = users
            }
            self.showEmptyScreen(false)

            if didChangeUsers || didChangeLeasingData {
                self.collectionNode.reloadData()
            }
        }.ensure {
            SVProgressHUD.dismiss()
            self.refreshControl.endRefreshing()
        }.catch { error in
            self.buildingData = nil
            self.users = []
            self.showEmptyScreen(true)
            Log.thisError(error)
        }
    }

    func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
        LeaseOfficeSections.allCases.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case LeaseOfficeSections.building.rawValue:
            return buildingData != nil ? 1 : 0
        case LeaseOfficeSections.usersHeader.rawValue:
            return users.count == 0 ? 0 : 1
        case LeaseOfficeSections.users.rawValue:
            return users.count
        default:
            return 0
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        guard let section = LeaseOfficeSections(rawValue: indexPath.section) else { return { ASCellNode() } }

        switch section {
        case .building:
            let body = buildingData?.body?.html?.string
            return { [weak self] in
                ConciergeThingsToKnowListViewController.ArticleCell(title: self?.buildingData?.title,
                                                                    body: body,
                                                                    url: self?.buildingData?.imageURL)
            }
        case .usersHeader:
            return {
                let cell = ASCellNode()
                cell.automaticallyManagesSubnodes = true
                let text = ASTextNode2()
                text.attributedText = "Management Team".attributed(.bodyTitleHeavy, .primaryLabel)
                cell.layoutSpecBlock = { node, range in
                    text.margin(15)
                }
                return cell
            }
        case .users:
            let user = users[indexPath.item]
            return {
                ConciergeGenericNodes.MenuCell(iconURL: user.userBioImageURL,
                                               title: user.userBioName ?? "",
                                               subtitle: user.userBioPosition,
                                               disclosure: true,
                                               menuStyle: .resident)
            }
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        ASSizeRange.fullWidth(collectionNode: collectionNode)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.section {
        case LeaseOfficeSections.building.rawValue:
            ()
        case LeaseOfficeSections.users.rawValue:
            let user = users[indexPath.item]
            navigationController?.pushViewController(ConciergeViewController(bio: user), animated: true)
        default:
            ()
        }
    }
}
