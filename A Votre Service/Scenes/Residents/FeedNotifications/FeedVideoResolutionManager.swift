//
// Created by Andrei Stoicescu on 05.02.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import PromiseKit
import Disk

class FeedVideoResolutionManager: Codable {
    enum ResolutionError: Error {
        case cantGetResolution
    }

    enum CodingKeys: String, CodingKey {
        case cache
    }

    struct Resolution: Identifiable, Codable {
        var id: String
        var size: CGSize
    }

    private static var cacheName = "resolution_manager.json"

    static var shared = FeedVideoResolutionManager()
    private var fetching = [String: Promise<Resolution>]()
    private var cache = [String: Resolution]() {
        didSet {
            CodableCache.set(cache, key: Self.cacheName)
        }
    }

    init() {
        if let cache = CodableCache.get([String: Resolution].self, key: Self.cacheName) {
            self.cache = cache
        }
    }

    func resolution(_ id: String) -> Resolution? {
        cache[id]
    }

    func cleanup() {
        cache = [:]
        fetching = [:]
        try? Disk.remove(Self.cacheName, from: .caches)
    }

    func fetchResolution(asset: AVAsset?, id: String) -> Promise<Resolution> {
        if let resolution = cache[id] {
            return .value(resolution)
        } else {
            if let fetching = fetching[id] {
                return fetching
            } else {
                let promise = Promise<Resolution> { seal in
                    DispatchQueue.global(qos: .background).async { [weak self] in
                        guard let asset = asset,
                              let image = self?.generateThumbnail(for: asset) else {
                            seal.reject(ResolutionError.cantGetResolution)
                            self?.cache.removeValue(forKey: id)
                            return
                        }

                        Log.this("RECEIVED IMAGE SIZE for media ID \(id): \(image.size)")
                        DispatchQueue.main.async { [weak self] in
                            guard let self = self else { return }
                            let resolution = Resolution(id: id, size: image.size)
                            self.cache[id] = resolution
                            seal.fulfill(resolution)
                            self.fetching.removeValue(forKey: id)
                        }
                    }
                }

                fetching[id] = promise
                return promise
            }
        }
    }

    private func generateThumbnail(for asset: AVAsset) -> UIImage? {
        let assetImgGenerate: AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        let time = CMTimeMake(value: 1, timescale: 2)
        let img = try? assetImgGenerate.copyCGImage(at: time, actualTime: nil)
        if img != nil {
            let frameImg = UIImage(cgImage: img!)
            return frameImg
        }
        return nil
    }
}