//
// Created by Andrei Stoicescu on 09/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import PromiseKit
import SwiftyUserDefaults
import ImageViewer
import SafariServices
import Combine
import DifferenceKit
import Disk

class FeedNotificationsListViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

    lazy var collectionNode: ASCollectionNode = {
        let collection = ASCollectionNode.avsCollection()
        collection.contentInset = .collectionBottomInset
        return collection
    }()

    lazy var navigationNode = UserNotificationsNodes.Navigation()
    lazy var emptyNode = EmptyNode(emptyNodeType: .userNotification)

    var notificationListDataSource = ObservablePaginatedDataSource<FeedNotificationItem>()
    var requestsProvider = UserRequestsProvider(statuses: [.accepted, .pending], cacheable: true)
    var concierges = [User]()
    private var subscriptions = Set<AnyCancellable>()

    var pushMessageCompletion: PushMessageCompletion? = nil

    var unreadCountRefreshTimer = TimerTrigger(time: 5)

    var sections = [ArraySection<Sections, AnyDifferentiable>]()

    enum Sections: Int, CaseIterable, Differentiable {
//        case ConciergeBar
        case Notifications
    }

    lazy var refreshControl = UIRefreshControl()

    var selectedMediaFiles: [FeedNotificationMediaFile] = []

    override init() {
        let mainNode = ASDisplayNode()
        mainNode.automaticallyManagesSubnodes = true

        super.init(node: mainNode)

        mainNode.layoutSpecBlock = { [weak self] node, range in
            guard let self = self else { return ASDisplayNode().wrapped() }
            return [
                self.navigationNode,
                self.collectionNode.growing()
            ].vStacked()
        }

        collectionNode.delegate = self
        collectionNode.dataSource = self
        collectionNode.leadingScreensForBatching = 0

        notificationListDataSource.cacheable = true
        notificationListDataSource.cacheKey = "feed_notifications"
        notificationListDataSource.paginate = { per, page in
            ResidentService().getFeedNotificationList(page: page, per: per)
        }

        setupBindings()

        showEmptyScreen(false)

        emptyNode.actionButtonTapped = { [weak self] in
            guard let tabBarController = self?.tabBarController as? ResidentTabBarViewController else { return }
            tabBarController.switchTo(controllerType: .chat)
        }
    }

    func setupBindings() {
        notificationListDataSource.$items
                .receive(on: DispatchQueue.main)
                .dropFirst()
                .sink { [weak self] items in
                    guard let self = self else { return }
                    self.reloadData()
                    self.handlePushCompletion()
                }.store(in: &subscriptions)

        notificationListDataSource.$hasNoItemsAfterLoading
                .receive(on: DispatchQueue.main)
                .sink { [weak self] hasNoItemsAfterLoading in
                    self?.showEmptyScreen(hasNoItemsAfterLoading)
                }.store(in: &subscriptions)

        notificationListDataSource.$showLoadingIndicator
                .receive(on: DispatchQueue.main)
                .sink { [weak self] loading in
                    guard let self = self else { return }

                    if loading {
                        if !self.refreshControl.isRefreshing {
                            SVProgressHUD.show()
                        }
                    } else {
                        SVProgressHUD.dismiss()
                        self.refreshControl.endRefreshing()
                    }
                }.store(in: &subscriptions)

        notificationListDataSource.$lastError
                .receive(on: DispatchQueue.main)
                .sink { [weak self] error in
                    guard let self = self, let error = error else { return }
                    self.notificationListDataSource.lastError = nil
                    self.handlePushCompletion(error: error)
                }.store(in: &subscriptions)
    }

    private func handlePushCompletion(error: Error? = nil) {
        guard let completion = pushMessageCompletion else { return }
        var result: UIBackgroundFetchResult = .noData

        if error != nil {
            result = .failed
        } else if notificationListDataSource.items.count > 0 {
            result = .newData
        } else {
            result = .noData
        }

        pushMessageCompletion = nil

        completion(result)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func showEmptyScreen(_ show: Bool) {
        emptyNode.isHidden = !show
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.publisher(for: UIApplication.willEnterForegroundNotification)
                                  .receive(on: DispatchQueue.main)
                                  .sink { [weak self] _ in
                                      guard let self = self else { return }
                                      self.refreshItems()
                                  }.store(in: &subscriptions)

        unreadCountRefreshTimer.trigger = { [weak self] in
            guard let self = self else { return }
            self.reloadBadgeCount()
            self.unreadCountRefreshTimer.stop()
        }

        view.backgroundColor = AVSColor.colorMilitaryGreen.color

        navigationNode.zPosition = 999
        replaceSystemBackButton()

        collectionNode.view.backgroundView = emptyNode.view
        
//        if(UIScreen.main.bounds.width>375){
//            let tabBar = BottonTabBarView(frame: CGRect(x: 0, y: UIScreen.main.bounds.height-83, width: UIScreen.main.bounds.width, height: 83))
//            self.view.addSubview(tabBar)
//        }
        
        let lateralMenu = LateralMenuView(frame: CGRect(x: 80, y: 0, width: 347, height: UIScreen.main.bounds.height))
        let myView = HeaderTabBarView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 190), tabBarVC: self, lm: lateralMenu)
        lateralMenu.isHidden = true
        lateralMenu.header = myView
        lateralMenu.rootView = self
        lateralMenu.helpButton.addTarget(self, action: #selector(tappedButtonActivator), for: .touchUpInside)
        self.view.addSubview(myView)
        self.view.addSubview(lateralMenu)
        
        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)

        collectionNode.view.addSubview(refreshControl)

        requestsProvider.$categoryRequests
                .receive(on: DispatchQueue.main)
                .sink { [weak self] items in
                    guard let self = self else { return }
                    self.reloadData()
                }.store(in: &subscriptions)

//        navigationNode.didTapNotificationsButton = { [weak self] in
//            guard let self = self else { return }
//            self.willPushViewController = true
////            let vc = BellNotificationsViewController()
////            vc.delegate = self
//            let vc = ResidentMoreViewController()
//            self.navigationController?.pushViewController(vc, animated: true)
//        }

        // Loading Cache

        requestsProvider.loadCache()

        if let concierges = [User].readCached(key: "feed_concierges") {
            self.concierges = concierges
        }

//        navigationNode.buildingName = AppState.buildingName

        notificationListDataSource.loadMoreContentIfNeeded()
        reloadData()

        if notificationListDataSource.items.count == 0 {
            SVProgressHUD.show()
        } else {
            refreshControl.beginRefreshing()
        }

        refreshItems()
    }
    
    @objc func tappedButtonActivator(){
        navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.pushViewController(ConciergeBuildingFAQViewController(), animated: true)
    }

    func reloadConcierges() {
        ResidentService().getConciergeOnDutyList().done { [weak self] users in
            guard let self = self else { return }
            users.cache(key: "feed_concierges")
            self.concierges = users
            self.reloadData()
        }.catch { error in
            Log.thisError(error)
        }
    }

    func reloadBuilding() {
//        BuildingDetails.getBuildingName(force: true).done { //[weak self] name in
            //self?.navigationNode.buildingName = name
//        }.catch { error in
//            Log.thisError(error)
//        }
    }

    func reloadUserRequests() {
        requestsProvider.reload()
    }

    @discardableResult
    func reloadBadgeCount() -> Promise<UnreadBellNotifications> {
        ResidentService().getUnreadBellNotificationsCount().map { [weak self] notifications in
            self?.updateBadgeCount(notifications.count)
            return notifications
        }
    }

    func updateBadgeCount(_ count: Int) {
//        navigationNode.notificationsBadge.count = count
    }

    @objc func refreshItems() {
        reloadBuilding()
        reloadBadgeCount()
        reloadUserRequests()
        reloadConcierges()
        notificationListDataSource.loadMoreContentIfNeeded()
    }

    func reloadNotifications(completion: @escaping PushMessageCompletion) {
        pushMessageCompletion = completion
        notificationListDataSource.loadMoreContentIfNeeded()
    }

    func processActionable(_ message: PushMessage) {

        if let actionIdentifier = message.actionIdentifier {
            switch actionIdentifier {
            case .deliver:
                if let id = message.clickAction?.id {
                    if let item = notificationListDataSource.findItemById(id) {
                        createUserRequestIfNeeded(item: item)
                    } else {
                        Log.this("Could not find item. Requesting it manually")
                        SVProgressHUD.show()
                        ResidentService().getFeedNotification(notificationID: id).done { [weak self] item in
                            SVProgressHUD.dismiss()
                            guard let self = self else { return }
                            self.createUserRequestIfNeeded(item: item)
                        }.catch { error in
                            Log.thisError(error)
                            SVProgressHUD.showError(withStatus: "Could not load the requested item")
                        }
                    }
                }

            default:
                ()
            }
        }

        if let action = message.clickAction {
            attemptToScrollToItemId(action.id)
        }
    }

    func attemptToScrollToItemId(_ id: String) {
        guard let itemIndex = notificationListDataSource.findIndexOfItemId(id),
              let section = sections.firstIndex(where: { $0.model == .Notifications }) else {
            Log.this("Not scrolling notifications")
            return
        }

        let indexPath = IndexPath(item: itemIndex, section: section)
        Log.this("Scrolling to indexPath \(indexPath)")
        collectionNode.scrollToItem(at: indexPath, at: .centeredVertically, animated: true)
    }

    func reloadData() {
        let categoryBarVisible = requestsProvider.categoryRequests.count > 0 || concierges.count > 0
        let categoryBarHash = (requestsProvider.categoryRequests.map(\.request.userRequestID) + concierges.map(\.uid)).joined(separator: "")
        Log.this("CATEGORY BAR HASH: \(categoryBarHash)")
        let feedItems = notificationListDataSource.items.map { AnyDifferentiable($0) }
//        let categoryBarSectionItems = categoryBarVisible ? [AnyDifferentiable(categoryBarHash)] : []

        let target: [ArraySection<Sections, AnyDifferentiable>] = [
//            ArraySection(model: .ConciergeBar, elements: categoryBarSectionItems),
            ArraySection(model: .Notifications, elements: feedItems)
        ]

        let stagedChangeSet = StagedChangeset(source: sections, target: target)
        for changeSet in stagedChangeSet {
            sections = changeSet.data
            collectionNode.performDiffBatchUpdates(changeSet: changeSet)
        }
    }

    func replaceItem(item: FeedNotificationItem, reload: Bool = false) {
        DispatchQueue.main.async {
            self.notificationListDataSource.replaceItem(item)
        }
    }

    func delete(notificationID: String) {
        DispatchQueue.main.async {
            self.notificationListDataSource.removeItemId(notificationID)
        }
    }

    func collectionView(_ collectionView: ASCollectionView, willDisplayNodeForItemAt indexPath: IndexPath) {
        guard sections[indexPath.section].model == .Notifications else { return }
        notificationListDataSource.loadMoreContentIfNeeded(currentItem: notificationListDataSource.items[indexPath.item])
    }

    func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
        sections.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        sections[section].elements.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        guard let section = Sections(rawValue: indexPath.section) else { return { ASCellNode() } }

        switch section {
//        case .ConciergeBar:
//            let requests = requestsProvider.categoryRequests
//            let concierges = self.concierges
//            return {
//                let cell = UserNotificationsNodes.ConciergeBar(concierges: concierges, requests: requests)
//                cell.didTapConcierge = { [weak self] user in
//                    guard let self = self else { return }
//                    self.willPushViewController = true
//                    self.navigationController?.pushViewController(ConciergeViewController(bio: user), animated: true)
//                }
//                cell.didTapRequest = { [weak self] request in
//                    guard let self = self else { return }
//                    self.willPushViewController = true
//                    self.navigationController?.pushViewController(UserRequestListViewController(), animated: true)
//                }
//                return cell
//            }
        case .Notifications:
            let item = notificationListDataSource.items[indexPath.item]
            return { [weak self] in
                let cell = UserNotificationsNodes.FeedCell(item: item)
                cell.delegate = self
                return cell
            }
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let scrollOffset = scrollView.contentOffset.y;
//        navigationNode.makeTransparent(scrollOffset <= 0)
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        handleCellMediaPlayback()
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        handleCellMediaPlayback()
    }

    func collectionNode(_ collectionNode: ASCollectionNode, willDisplayItemWith node: ASCellNode) {
        handleCellMediaPlayback()
    }

    func handleCellMediaPlayback() {
        let videoNodeCandidates = collectionNode.visibleNodes.filter { node in
            guard let node = node as? UserNotificationsNodes.FeedCell,
                  node.mediaNode.currentItemIsVideo,
                  let indexPath = collectionNode.indexPath(for: node),
                  let attrs = collectionNode.view.layoutAttributesForItem(at: indexPath) else { return false }

            let cellRect = collectionNode.convert(attrs.frame, to: self.node)
            return collectionNode.frame.contains(CGPoint(x: cellRect.midX, y: cellRect.midY))
        }

        let playableNode = videoNodeCandidates.first as? UserNotificationsNodes.FeedCell

        for node in collectionNode.visibleNodes {
            guard let indexPath = collectionNode.indexPath(for: node), indexPath.section == Sections.Notifications.rawValue else { continue }

            if node == playableNode {
                playableNode?.mediaNode.playVideo()
            } else {
                (node as? UserNotificationsNodes.FeedCell)?.mediaNode.stopVideo()
            }
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didEndDisplayingItemWith node: ASCellNode) {
        guard let node = node as? UserNotificationsNodes.FeedCell else { return }
        node.mediaNode.stopVideo()
    }

    func handleReadItem(_ item: FeedNotificationItem) {
        guard !item.read else { return }

        Log.this("reading item ID: \(item.notificationID)")

        ResidentService().readBellNotificationObject(params: BellNotificationReadRequest(type: .notification, itemID: item.notificationID)).catch { error in
            Log.thisError(error)
        }

        unreadCountRefreshTimer.stop()
        unreadCountRefreshTimer.start()

        var item = item
        item.read = true
        replaceItem(item: item, reload: true)
    }

}

extension FeedNotificationsListViewController: UserNotificationsFeedCellProtocol {
    func feedCellVisible(item: FeedNotificationItem) {
        handleCellMediaPlayback()
        handleReadItem(item)
    }

    func feedCellMenuItemTapped(item: FeedNotificationItem) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let deleteAction = UIAlertAction(title: L10n.General.delete, style: .destructive) { [unowned self] _ in
            confirmDelete(notificationID: item.notificationID)
        }

        if [FeedNotificationItem.NotificationType.delivery, .packages].contains(item.notificationType) {
            if String.isBlank(item.requestID) {
                let requestAction = UIAlertAction(title: L10n.UserRequest.Labels.createRequest, style: .default) { [weak self] _ in
                    self?.createUserRequestIfNeeded(item: item)
                }

                alertController.addAction(requestAction)
            }
        }

        alertController.addAction(deleteAction)
        alertController.addAction(UIAlertAction(title: L10n.General.cancel, style: .cancel, handler: nil))

        alertController.fixIpadIfNeeded(view: view)

        present(alertController, animated: true)
    }

    func createUserRequestIfNeeded(item: FeedNotificationItem) {
        guard String.isBlank(item.requestID) else {
            Log.this("Notification already has a request")
            return
        }

        let request = UserRequestParams.genericRequest(notificationID: item.notificationID)

        SVProgressHUD.show()

        ResidentService().createUserRequest(params: request).get {
            self.reloadUserRequests()
        }.done { [weak self] in
            SVProgressHUD.showInfo(withStatus: L10n.UserRequest.Labels.requestSuccess)
            var item = item
            item.requestID = UUID().uuidString
            self?.replaceItem(item: item)
            self?.reloadData()
        }.catch { error in
            Log.thisError(error)
            SVProgressHUD.showError(withStatus: error.localizedDescription)
        }
    }

    func confirmDelete(notificationID: String) {

        let message = L10n.UserNotifications.Labels.deleteNotificationConfirm
        let deleteAlert = UIAlertController(title: nil, message: message, preferredStyle: .alert)

        deleteAlert.addAction(UIAlertAction(title: L10n.General.delete, style: .default, handler: { action in
            SVProgressHUD.show()
            let apiService = ResidentService()
            apiService.deleteNotification(notificationID: notificationID).done { [weak self] in
                SVProgressHUD.dismiss()
                self?.delete(notificationID: notificationID)
            }.catch { error in
                SVProgressHUD.showError(withStatus: error.localizedDescription)
                Log.thisError(error)
            }
        }))

        deleteAlert.addAction(UIAlertAction(title: L10n.General.cancel, style: .cancel, handler: nil))

        present(deleteAlert, animated: true, completion: nil)
    }

    func feedCellTapped(icon: UserNotificationsNodes.FeedCell.Icon, item: FeedNotificationItem) {
        guard let tabBarController = tabBarController as? ResidentTabBarViewController else { return }

        switch icon {
        case .chat:
            tabBarController.switchTo(controllerType: .chat)
        case .delivery:
            createUserRequestIfNeeded(item: item)
        case .readMore:
            if let link = item.linkType() {
                switch link {
                case .url(let url):
                    self.tabBarController?.present(SFSafariViewController(url: url, configuration: SFSafariViewController.Configuration()), animated: true)
                case .article(let id, let title):
                    let url = Article.articleNodeURL(id: id)
                    let vc = ArticleWebViewController(url: url)
                    vc.title = title
                    vc.closeBlock = { [weak self] in
                        guard let self = self else { return }
                        self.tabBarController?.dismiss(animated: true)
                    }

                    self.tabBarController?.present(AVSNavigationController(rootViewController: vc), animated: true)
                }
            }
        }
    }

    func feedCellMediaTapped(cell: UserNotificationsNodes.FeedCell, item: FeedNotificationMediaFile, list: [FeedNotificationMediaFile]) {
        selectedMediaFiles = list
        let idx = list.firstIndex(where: { $0.targetID == item.targetID }) ?? 0
        let galleryVC = GalleryViewController(startIndex: idx, itemsDataSource: self, configuration: GalleryHelpers.defaultConfiguration)
        let headerView = GalleryCounterView(frame: CGRect(x: 0, y: 0, width: 200, height: 24), currentIndex: 0, count: selectedMediaFiles.count)
        galleryVC.headerView = headerView
        galleryVC.landedPageAtIndexCompletion = { [weak self] index in
            guard let self = self else { return }
            headerView.count = self.selectedMediaFiles.count
            headerView.currentIndex = index
        }

        galleryVC.swipedToDismissCompletion = { [weak cell] in
            cell?.mediaNode.playVideo()
        }

        galleryVC.closedCompletion = { [weak cell] in
            cell?.mediaNode.playVideo()
        }

        presentImageGallery(galleryVC)
    }
}

extension FeedNotificationsListViewController: GalleryItemsDataSource {
    func itemCount() -> Int {
        selectedMediaFiles.count
    }

    func provideGalleryItem(_ index: Int) -> GalleryItem {
        let mediaFile = selectedMediaFiles[index]

        switch mediaFile.type {
        case .image:
            return .image { completion in
                mediaFile.mediaURL.downloadAsImage().done { image in
                    completion(image)
                }.catch { error in
                    completion(.pixelImage(color: .clear))
                }
            }
        case .video:
            return .videoAsset(fetchPreviewImageBlock: { completion in
                completion(.pixelImage(color: .clear))
            }, videoAsset: cachedOrCachingAsset(mediaFile.mediaURL))
        }
    }
}

extension FeedNotificationsListViewController: BellNotificationsViewControllerProtocol {
    func bellNotificationsFeedNotificationCellTapped(icon: UserNotificationsNodes.FeedCell.Icon, item: FeedNotificationItem) {
        feedCellTapped(icon: icon, item: item)
    }

    func bellNotificationsChangedNotificationsCount() {
        reloadBadgeCount()
    }
}
