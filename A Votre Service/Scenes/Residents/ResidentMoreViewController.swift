//
// Created by Andrei Stoicescu on 05/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import UIKit
import SafariServices
import PromiseKit
import AsyncDisplayKit
import SVProgressHUD
import SwiftyUserDefaults
import SwiftUI

class ResidentMoreViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

    enum Sections: Int, CaseIterable {
        case profile
        case menu
        case developer
    }

    enum DeveloperDataSource: String, CaseIterable {
        case exportLogs = "Export Logs"
    }

    enum MoreDataSource: Int, CaseIterable {

        case buildingInfo
        case leasingOffice
        case reservations
        case maintenanceRequest
        case requests
        case thingsToKnow
        case concierges
        case allowedGuestList
        case payment
        case subscriptions
        case aLaCarte
        case orderHistory
        case feedback
        case documentLibrary
        case videoLibrary
        case notifications
        case help
        case terms
        case privacy

        var icon: UIImage {
            switch self {

            case .buildingInfo:
                return Asset.Images.Concierge.icBuilding.image
            case .leasingOffice:
                return Asset.Images.Concierge.icBuilding.image
            case .thingsToKnow:
                return Asset.Images.Concierge.icInfo.image
            case .concierges:
                return Asset.Images.Concierge.icResidents.image
            case .allowedGuestList:
                return Asset.Images.Concierge.icVisitors.image
            case .payment:
                return Asset.Images.Concierge.icPayment.image
            case .aLaCarte:
                return Asset.Images.Concierge.icAlacarte.image
            case .orderHistory:
                return Asset.Images.Concierge.icReceipt.image
            case .feedback:
                return Asset.Images.Concierge.icChatNav.image
            case .notifications:
                return Asset.Images.Concierge.icBell.image
            case .help:
                return Asset.Images.Concierge.icQuestionCircle.image
            case .terms:
                return Asset.Images.Concierge.icDocument.image
            case .privacy:
                return Asset.Images.Concierge.icShield.image
            case .reservations:
                return Asset.Images.Concierge.icReservations.image
            case .requests:
                return Asset.Images.Concierge.icPackage.image
            case .maintenanceRequest:
                return Asset.Images.Concierge.icInfo.image
            case .subscriptions:
                return Asset.Images.icBoxes.image
            case .documentLibrary:
                return Asset.Images.Concierge.icDocumentLibrary.image
            case .videoLibrary:
                return Asset.Images.Concierge.icVideoLibrary.image
            }
        }

        var title: String {
            switch self {
            case .buildingInfo:
                return L10n.MoreScreen.Items.buildingInfo
            case .leasingOffice:
                return "Leasing Office"
            case .thingsToKnow:
                return L10n.MoreScreen.Items.thingsToKnow
            case .concierges:
                return L10n.MoreScreen.Items.concierges
            case .allowedGuestList:
                return L10n.MoreScreen.Items.allowedGuestList
            case .payment:
                return L10n.MoreScreen.Items.payment
            case .aLaCarte:
                return L10n.MoreScreen.Items.aLaCarte
            case .orderHistory:
                return L10n.MoreScreen.Items.orderHistory
            case .feedback:
                return L10n.MoreScreen.Items.feedback
            case .notifications:
                return L10n.MoreScreen.Items.notifications
            case .help:
                return L10n.MoreScreen.Items.help
            case .terms:
                return L10n.MoreScreen.Items.terms
            case .privacy:
                return L10n.MoreScreen.Items.privacy
            case .reservations:
                return L10n.MoreScreen.Items.amenityReservations
            case .requests:
                return L10n.MoreScreen.Items.requests
            case .maintenanceRequest:
                return L10n.MoreScreen.Items.maintenanceRequest
            case .subscriptions:
                return "Subscriptions"
            case .documentLibrary:
                return "Document Library"
            case .videoLibrary:
                return "Video Library"
            }
        }

        var controller: UIViewController? {
            switch self {
            case .buildingInfo:
                return BuildingDetailsViewController()
            case .leasingOffice:
                return LeaseOfficeViewController()
            case .concierges:
                return ConciergeListViewController()
            case .thingsToKnow:
                return ConciergeThingsToKnowListViewController()
            case .allowedGuestList:
                return AllowedGuestListViewController()
            case .feedback:
                return FeedbackViewController()
            case .help:
                return ConciergeBuildingFAQViewController(title: "Help")
            case .terms:
                return TextPageViewController(pageSource: .terms)
            case .privacy:
                return TextPageViewController(pageSource: .privacy)
            case .notifications:
                return SettingsViewController()
            case .reservations:
                return AmenityReservationListViewController()
            case .requests:
                return UserRequestListViewController()
            case .payment:
                return PaymentMethodManagerViewController()
            case .aLaCarte:
                return ALaCarteCategoriesViewController(cartManager: .shared)
            case .orderHistory:
                return OrderListViewController()
            case .maintenanceRequest:
                return nil
            case .subscriptions:
                return SubscriptionListCarouselViewController()
            case .documentLibrary:
                return LibraryPagerViewController(type: .document)
            case .videoLibrary:
                return LibraryPagerViewController(type: .video)
            }
        }

        var canRequireBiometricAuth: Bool {
            self == .payment
        }

        static var allowedItems: [MoreDataSource] {
            let flags = AppState.features.enabledFlags
            return MoreDataSource.allCases.compactMap { source -> MoreDataSource? in
                switch source {
                case .buildingInfo:
                    return flags.contains(.buildingInfo) ? source : nil
                case .leasingOffice:
                    return flags.contains(.leasingOffice) ? source : nil
                case .reservations:
                    return flags.contains(.reservations) ? source : nil
                case .requests:
                    return flags.contains(.requests) ? source : nil
                case .thingsToKnow:
                    return flags.contains(.thingsToKnow) ? source : nil
                case .concierges:
                    return flags.contains(.concierges) ? source : nil
                case .allowedGuestList:
                    return flags.contains(.allowedGuestList) ? source : nil
                case .payment:
                    return flags.contains(.payment) ? source : nil
                case .aLaCarte:
                    return flags.contains(.aLaCarte) ? source : nil
                case .orderHistory:
                    return flags.contains(.orderHistory) ? source : nil
                case .feedback:
                    return flags.contains(.feedback) ? source : nil
                case .notifications:
                    return flags.contains(.notifications) ? source : nil
                case .help:
                    return flags.contains(.help) ? source : nil
                case .terms:
                    return flags.contains(.terms) ? source : nil
                case .privacy:
                    return flags.contains(.privacy) ? source : nil
                case .maintenanceRequest:
                    return flags.contains(.maintenanceRequest) && Defaults.maintenanceRequestUrl != nil ? source : nil
                case .subscriptions:
                    return flags.contains(.subscriptions) ? source : nil
                case .documentLibrary:
                    return flags.contains(.documents) ? source : nil
                case .videoLibrary:
                    return flags.contains(.videoLibrary) ? source : nil
                }
            }
        }
    }

    var dataSource = MoreDataSource.allowedItems

    var developerDataSource: [DeveloperDataSource] = {
        #if DEBUG
        return DeveloperDataSource.allCases
        #else
        return []
        #endif
    }()

    var collectionNode: ASCollectionNode
    var user: User?

    override init() {
        collectionNode = .avsCollection()
        collectionNode.backgroundColor = .clear

        let navigationNode = ConciergeGenericNodes.LargeNavigationNode(title: "Menu", rightButtonType: .logout)

        let mainNode = ASDisplayNode()
        mainNode.backgroundColor = AVSColor.primaryBackground.color
        mainNode.automaticallyManagesSubnodes = true

        super.init(node: mainNode)

        mainNode.layoutSpecBlock = { node, range in
            self.collectionNode.style.flexGrow = 1
            return [navigationNode, self.collectionNode].vStacked()
        }

        navigationNode.didTapRightButton = { [weak self] in
            self?.confirmLogout()
        }

        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()
        loadUser()
    }

//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        navigationController?.setNavigationBarHidden(true, animated: false)
//    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        navigationController?.setNavigationBarHidden(false, animated: true)
//    }

    func loadUser() {
        if let user = Defaults.user {
            self.user = user
            collectionNode.reloadData()
            return
        }

        SVProgressHUD.show()

        ResidentService().getUser().done { [weak self] user in
            SVProgressHUD.dismiss()
            guard let self = self else { return }
            self.user = user
            self.collectionNode.reloadData()
        }.catch { error in
            SVProgressHUD.showError(withStatus: error.localizedDescription)
            Log.thisError(error)
        }
    }

    func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
        Sections.allCases.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        guard let section = Sections(rawValue: section) else { return 0 }
        switch section {
        case .profile:
            return user != nil ? 1 : 0
        case .menu:
            return dataSource.count
        case .developer:
            return developerDataSource.count
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let blankCell = { ASCellNode() }
        guard let section = Sections(rawValue: indexPath.section) else { return blankCell }

        switch section {
        case .profile:
            guard let user = user else { return blankCell }
            return { ConciergeMoreNodes.ProfileCell(user: user) }
        case .menu:
            let menu = dataSource[indexPath.item]
            return {
                ConciergeGenericNodes.MenuCell(icon: menu.icon.withRenderingMode(.alwaysTemplate),
                                               title: menu.title,
                                               subtitle: nil,
                                               disclosure: true,
                                               menuStyle: .resident)
            }
        case .developer:
            let menu = developerDataSource[indexPath.item]
            return {
                ConciergeGenericNodes.MenuCell(icon: Asset.Images.icEnvelope.image.withRenderingMode(.alwaysTemplate),
                                               title: menu.rawValue,
                                               subtitle: nil,
                                               menuStyle: .resident)
            }
        }
    }

    func openViewController(_ viewController: UIViewController) {
        show(viewController, sender: self)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        guard let section = Sections(rawValue: indexPath.section) else { return }

        switch section {
        case .profile:
            guard let user = user else { return }
            // TODO: other editor?
            openViewController(ConciergeProfileViewController(user: user))
        case .menu:
            let type = dataSource[indexPath.item]
            biometricAuthenticationIfNeededForType(type).done {
                self.handleMoreDataSourceSelection(for: type)
            }.catch { error in
                self.collectionNode.deselectItem(at: indexPath, animated: false)
                Log.thisError(error)
            }
        case .developer:
            let type = developerDataSource[indexPath.item]
            switch type {
            case .exportLogs:
                openViewController(UIHostingController(rootView: AVSLogsView()))
            }
        }
    }

    func confirmLogout() {
        switch Configuration.default.aVotreService.app.environment {

        case .development:
            logoutDev()
        case .production:
            logoutProd()
        }
    }

    func logoutProd() {
        let alertController = UIAlertController(title: nil, message: L10n.MoreScreen.Actions.Logout.confirm, preferredStyle: .alert)

        alertController.addAction(UIAlertAction(title: L10n.MoreScreen.Actions.Logout.ok, style: .default, handler: { action in
            AppState.logout(userAction: true)
        }))

        alertController.addAction(UIAlertAction(title: L10n.MoreScreen.Actions.Logout.cancel, style: .cancel, handler: nil))

        present(alertController, animated: true, completion: nil)
    }

    func logoutDev() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let logoutAction = UIAlertAction(title: "Logout", style: .destructive) { action in
            AppState.logout(userAction: true)
        }
        alertController.addAction(logoutAction)

        let biometrics = Biometrics()
        if let biometricsType = biometrics.biometryTypeString {
            let logoutActionAllowBiometrics = UIAlertAction(title: "Logout (Allow login via \(biometricsType))", style: .default) { action in
                AppState.logout(userAction: false)
            }
            alertController.addAction(logoutActionAllowBiometrics)
        }

        alertController.addAction(UIAlertAction(title: L10n.General.cancel, style: .cancel, handler: nil))
        alertController.fixIpadIfNeeded(view: view)
        present(alertController, animated: true)
    }

    func handleMoreDataSourceSelection(for type: MoreDataSource, animated: Bool = true) {
        guard let nav = navigationController else { return }

        if nav.viewControllers.count > 1 {
            nav.popToRootViewController(animated: false)
        }

        let controller = type.controller

        if let feedbackViewController = controller as? FeedbackViewController {
            feedbackViewController.delegate = self
        }

        switch type {
        case .maintenanceRequest:
            if let url = Defaults.maintenanceRequestUrl {
                present(SFSafariViewController(url: url, configuration: SFSafariViewController.Configuration()), animated: animated)
            }
        default:
            guard let vc = controller else { return }
            openViewController(vc)
        }
    }

    func biometricAuthenticationIfNeededForType(_ type: MoreDataSource) -> Promise<Void> {
        let biometrics = Biometrics()
        if type.canRequireBiometricAuth && biometrics.canUseBiometricsHardware && biometrics.isEnabled {
            return biometrics.authenticate()
        } else {
            return .value(())
        }
    }
}

extension ResidentMoreViewController: FeedbackViewControllerProtocol {
    func feedbackViewController(_ controller: FeedbackViewController, didFinishWith feedback: Feedback) {
        navigationController?.popToRootViewController(animated: true)
    }
}
