//
// Created by Andrei Stoicescu on 10/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit
import SwiftyUserDefaults
import AsyncDisplayKit
import SVProgressHUD

class ConciergeViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

    var collectionNode: ASCollectionNode {
        node as! ASCollectionNode
    }

    var bio: UserBioCellProtocol

    init(bio: UserBioCellProtocol) {
        self.bio = bio
        let collectionNode = ASCollectionNode.avsCollection()
        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        1
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let bio = self.bio
        let about = bio.userBioBody?.html?.string
        return {
            let contentNode = ConciergeGenericNodes.BioProfileContent(title: bio.userBioName, subtitle: bio.userBioPosition, body: about)
            return ConciergeGenericNodes.BioProfileCell(url: bio.userBioImageURL, imageSize: 150, contentNode: contentNode)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        ASSizeRange.fullWidth(collectionNode: collectionNode)
    }
}
