//
// Created by Andrei Stoicescu on 12/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import Stripe
import SVProgressHUD
import AsyncDisplayKit

class SubscriptionPaymentSelectionViewController: PaymentMethodListViewController {

    var checkoutData: SubscriptionCheckoutData

    init(checkoutData: SubscriptionCheckoutData) {
        self.checkoutData = checkoutData
        super.init()
        disableApplePay = true
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: L10n.General.cancel, style: .plain, target: self, action: #selector(didTapClose))
    }

    @objc func didTapClose() {
        guard let nav = navigationController as? AVSNavigationController else { return }
        nav.dismissNavigation?()
    }

    override func paymentMethodSelected(method: PaymentMethodSelection) {
        checkoutData.paymentMethod = method
        navigationController?.pushViewController(SubscriptionReviewViewController(checkoutData: self.checkoutData), animated: true)
    }
}