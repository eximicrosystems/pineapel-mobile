//
// Created by Andrei Stoicescu on 27/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import PromiseKit
import SwiftyUserDefaults

class SubscriptionListViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

    lazy var emptyNode = EmptyNode(emptyNodeType: .subscriptions, showAction: true)

    var collectionNode: ASCollectionNode {
        node as! ASCollectionNode
    }

    var subscriptions: [Subscription] = []
    var userSubscription: UserSubscription?

    lazy var refreshControl = UIRefreshControl()

    override init() {
        let collectionNode = ASCollectionNode.avsCollection()
        collectionNode.showsVerticalScrollIndicator = false
        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        emptyNode.actionButtonTapped = { [weak self] in
            guard let tabBarController = self?.tabBarController as? ResidentTabBarViewController else { return }
            tabBarController.switchTo(controllerType: .chat)
        }

        replaceSystemBackButton()
        navigationItem.title = "Subscriptions"
        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)
        collectionNode.view.addSubview(refreshControl)
        showEmptyScreen(false)
        collectionNode.view.backgroundView = emptyNode.view
        SVProgressHUD.show()
        refreshItems()
    }

    func showEmptyScreen(_ show: Bool) {
        emptyNode.isHidden = !show
    }

    @objc func refreshItems() {
        let apiService = ResidentService()

        let getUserSub = apiService.getUserSubscription().recover { error -> Promise<UserSubscription?> in
            .value(nil)
        }

        when(fulfilled: apiService.getSubscriptionList(), getUserSub).done { [weak self] subscriptions, subscription in
            guard let self = self else { return }
            self.subscriptions = subscriptions
            self.userSubscription = subscription
            self.collectionNode.reloadData()
        }.ensure { [weak self] in
            guard let self = self else { return }
            self.refreshControl.endRefreshing()
            self.showEmptyScreen(self.subscriptions.count == 0)
            SVProgressHUD.dismiss()
        }.catch { error in
            Log.thisError(error)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        self.subscriptions.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let subscription = subscriptions[indexPath.item]
        let userSubscription = self.userSubscription
        let description = subscription.description?.html?.string
        return {

            let activeSubscription = subscription.subscriptionID == userSubscription?.subscriptionID
            let title = "$\(subscription.price.currencyClean)/mo"
            let button: ConciergeGenericNodes.PurchaseButton

            if activeSubscription {
                button = .init(icon: nil, label: title, leftLabel: "Subscribed")
            } else {
                button = .init(icon: Asset.Images.icCartPlus.image, label: title)
            }

            let cell = ConciergeThingsToKnowListViewController.HorizontalModeCell(title: subscription.name, body: description, url: subscription.image)
            cell.bottomNode = button
            cell.thumbAspectRatio = 11 / 13

            return cell
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        ASSizeRange.fullWidth(collectionNode: collectionNode)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        let vc = SubscriptionViewController(subscription: subscriptions[indexPath.item], currentSubscription: userSubscription)
        vc.subscriptionChanged = { [weak self] userSubscription in
            guard let self = self else { return }
            self.userSubscription = userSubscription
            self.collectionNode.reloadData()
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
