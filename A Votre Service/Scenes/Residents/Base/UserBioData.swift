//
// Created by Andrei Stoicescu on 24/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation

protocol UserBioCellProtocol {
    var userBioName: String? { get }
    var userBioPosition: String? { get }
    var userBioImageURL: URL? { get }
    var userBioBody: String? { get }
}

extension User: UserBioCellProtocol {
    var userBioName: String? {
        self.firstName
    }

    var userBioPosition: String? {
        L10n.Models.Concierge.ConciergeType.concierge
    }

    var userBioImageURL: URL? {
        self.userPictureURL
    }

    var userBioBody: String? {
        self.about
    }
}

extension LeaseOfficeData: UserBioCellProtocol {
    var userBioName: String? {
        self.title
    }

    var userBioPosition: String? {
        self.position
    }

    var userBioImageURL: URL? {
        self.imageURL
    }

    var userBioBody: String? {
        self.body
    }
}
