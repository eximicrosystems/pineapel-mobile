// //
// // Created by Andrei Stoicescu on 17/06/2020.
// // Copyright (c) 2020 A Votre Service LLC. All rights reserved.
// //
//
// import Foundation
// import AsyncDisplayKit
// import SVProgressHUD
// import PromiseKit
// import SwiftyUserDefaults
// import Combine
// import DifferenceKit
//
// class HelpViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {
//
//     class HelpDataSource: ObservableObject {
//         var attributedStrings = [String: NSAttributedString?]()
//
//         var filterTerm = "" {
//             didSet {
//                 filterArticles()
//             }
//         }
//
//         var articles = [Article]() {
//             didSet {
//                 attributedStrings = [:]
//
//                 for article in articles {
//                     attributedStrings[article.articleNodeID] = article.body?.html
//                 }
//
//                 filterArticles()
//             }
//         }
//
//         private func filterArticles() {
//             let articles: [Article]
//
//             if filterTerm.isEmpty {
//                 articles = self.articles
//             } else {
//                 articles = self.articles.filter { model in
//                     (model.body?.contains(filterTerm) ?? false) || (model.title?.contains(filterTerm) ?? false)
//                 }
//             }
//
//             let openedArticles = filteredArticles.filter { $0.open }
//
//             filteredArticles = articles.map { article in
//                 HelpViewModel(article: article,
//                               open: openedArticles.contains { $0.article.articleNodeID == article.articleNodeID },
//                               attributedString: attributedStrings[article.articleNodeID, default: nil])
//             }
//         }
//
//         @Published var filteredArticles = [HelpViewModel]()
//     }
//
//     struct HelpViewModel: Hashable, Differentiable {
//         var article: Article
//         var open: Bool = false
//         var attributedString: NSAttributedString?
//
//         func hash(into hasher: inout Hasher) { hasher.combine(article.articleNodeID) }
//
//         static func ==(lhs: HelpViewModel, rhs: HelpViewModel) -> Bool {
//             lhs.article.articleNodeID == rhs.article.articleNodeID && lhs.open == rhs.open
//         }
//     }
//
//     class HelpCell: ASCellNode {
//         var backgroundNode = ASDisplayNode()
//         var titleNode = ASTextNode2()
//         var bodyNode = ASTextNode2()
//         var toggleButton = ASButtonNode()
//
//         var topLineNode = ASDisplayNode()
//         var bottomLineNode = ASDisplayNode()
//
//         var isFirst: Bool {
//             get { self.indexPath?.item == 0 }
//         }
//
//         var open: Bool = false
//
//         init(vm: HelpViewModel) {
//             backgroundNode.backgroundColor = .white
//             topLineNode.backgroundColor = Asset.Colors.lightGray.color
//             bottomLineNode.backgroundColor = Asset.Colors.lightGray.color
//             bodyNode.attributedText = vm.attributedString
//             titleNode.attributedText = NSAttributedString.string(text: vm.article.title, font: .NormalText, color: Asset.Colors.darkGray.color)
//
//             open = vm.open
//
//             let image = open ? Asset.Images.icMinusCircle.image : Asset.Images.icPlusCircle.image
//             toggleButton.setImage(image, for: .normal)
//
//             super.init()
//
//             self.automaticallyManagesSubnodes = true
//         }
//
//         override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
//             titleNode.style.flexGrow = 1
//             titleNode.style.flexShrink = 1
//
//             let titleButtonStack = ASStackLayoutSpec(
//                     direction: .horizontal,
//                     spacing: 20,
//                     justifyContent: .start,
//                     alignItems: .center,
//                     children: [titleNode, toggleButton]
//             )
//
//             var bodyStackItems: [ASLayoutElement] = [titleButtonStack]
//             if open {
//                 bodyStackItems.append(bodyNode)
//             }
//
//             let bodyStack = ASStackLayoutSpec(
//                     direction: .vertical,
//                     spacing: 20,
//                     justifyContent: .start,
//                     alignItems: .stretch,
//                     children: bodyStackItems
//             )
//
//             topLineNode.style.height = ASDimensionMakeWithPoints(1)
//             bottomLineNode.style.height = ASDimensionMakeWithPoints(1)
//
//             var lineItems: [ASLayoutElement] = []
//
//             lineItems.append(isFirst ? topLineNode : ASDisplayNode())
//             lineItems.append(bodyStack.with(hMargin: 15))
//             lineItems.append(bottomLineNode)
//
//             let lineItemsStack = ASStackLayoutSpec(
//                     direction: .vertical,
//                     spacing: 15,
//                     justifyContent: .start,
//                     alignItems: .stretch,
//                     children: lineItems
//             )
//
//             return lineItemsStack.on(background: backgroundNode)
//         }
//     }
//
//     class HelpSearchCell: ASCellNode, ASEditableTextNodeDelegate {
//         let titleNode = ASTextNode2()
//         let searchInput = ASEditableTextNode()
//         let searchIcon = ASImageNode()
//         let inputBackground = ASDisplayNode()
//         let backgroundNode = ASDisplayNode()
//         let textChanged: (String) -> Void
//
//         init(textChanged: @escaping (String) -> Void) {
//             self.textChanged = textChanged
//             titleNode.attributedText = NSAttributedString.string(text: L10n.HelpScreen.Fields.Faq.title, font: .NormalTextHead, color: .white)
//             searchIcon.image = Asset.Images.icSearch.image
//
//             inputBackground.backgroundColor = .white
//             inputBackground.cornerRadius = 4
//
//             searchInput.attributedPlaceholderText = NSAttributedString.string(text: L10n.HelpScreen.Fields.Faq.placeholder, font: .NormalText, color: Asset.Colors.darkGray.color)
//             searchInput.onDidLoad { node in
//                 let attributes = NSAttributedString.attributes(font: Asset.Fonts.NormalText.uiFont, color: Asset.Colors.darkGray.color, alignment: .left)
//                 let node = node as! ASEditableTextNode
//                 node.textView.typingAttributes = attributes
//             }
//             searchInput.autocorrectionType = .no
//             searchInput.autocapitalizationType = .none
//
//             backgroundNode.backgroundColor = Asset.Colors.darkGray.color
//
//             super.init()
//             searchInput.delegate = self
//             self.backgroundColor = .clear
//             self.automaticallyManagesSubnodes = true
//         }
//
//         override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
//             searchInput.style.flexGrow = 1
//             let iconInputStack = ASStackLayoutSpec(direction: .horizontal,
//                                                    spacing: 5,
//                                                    justifyContent: .start,
//                                                    alignItems: .center,
//                                                    children: [searchIcon, searchInput])
//
//             let titleInputStack = ASStackLayoutSpec(direction: .vertical,
//                                                     spacing: 5,
//                                                     justifyContent: .start,
//                                                     alignItems: .stretch,
//                                                     children: [
//                                                         titleNode,
//                                                         iconInputStack.with(margin: 10).on(background: inputBackground)
//                                                     ])
//
//             return titleInputStack.with(margin: 10).on(background: backgroundNode).withMargin(bottom: 10)
//         }
//
//         func editableTextNodeDidUpdateText(_ editableTextNode: ASEditableTextNode) {
//             let text = editableTextNode.attributedText?.string ?? ""
//             textChanged(text)
//         }
//     }
//
//     var collectionNode: ASCollectionNode {
//         node as! ASCollectionNode
//     }
//
//     var dataSource = HelpDataSource()
//
//     lazy var refreshControl = UIRefreshControl()
//     var searchController: UISearchController?
//     private var subscriptions = Set<AnyCancellable>()
//
//     enum Sections: Int, CaseIterable, Differentiable {
//         case search
//         case items
//     }
//
//     var sections = [ArraySection<Sections, AnyDifferentiable>]()
//
//     override init() {
//         let collectionNode = ASCollectionNode.avsCollection()
//         super.init(node: collectionNode)
//         collectionNode.delegate = self
//         collectionNode.dataSource = self
//     }
//
//     required init?(coder: NSCoder) {
//         fatalError("init(coder:) has not been implemented")
//     }
//
//     override func viewDidLoad() {
//         super.viewDidLoad()
//         refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)
//         replaceSystemBackButton()
//         self.navigationItem.title = L10n.HelpScreen.Navigation.title
//         collectionNode.view.addSubview(refreshControl)
//         SVProgressHUD.show()
//         setupBindings()
//         refreshItems()
//     }
//
//     func setupBindings() {
//         dataSource.$filteredArticles
//                 .receive(on: DispatchQueue.main)
//                 .sink { [weak self] items in
//                     self?.reloadData()
//                 }.store(in: &subscriptions)
//     }
//
//     @objc func refreshItems() {
//         let apiService = ResidentService()
//         firstly {
//             BuildingDetails.getBuildingID()
//         }.then { buildingID in
//             apiService.getBuildingFAQ(buildingID: buildingID)
//         }.done { [weak self] articles in
//             guard let self = self else { return }
//             self.dataSource.articles = articles
//         }.ensure { [weak self] in
//             self?.refreshControl.endRefreshing()
//             SVProgressHUD.dismiss()
//         }.catch { error in
//             SVProgressHUD.showError(withStatus: error.localizedDescription)
//             Log.thisError(error)
//         }
//     }
//
//     func reloadData() {
//         let anyArticles = dataSource.filteredArticles.map { AnyDifferentiable($0) }
//         let searchItem = [AnyDifferentiable("search")]
//         let target: [ArraySection<Sections, AnyDifferentiable>] = [
//             ArraySection(model: .search, elements: searchItem),
//             ArraySection(model: .items, elements: anyArticles),
//         ]
//
//         let stagedChangeSet = StagedChangeset(source: sections, target: target)
//         for changeSet in stagedChangeSet {
//             self.sections = changeSet.data
//             self.collectionNode.performDiffBatchUpdates(changeSet: changeSet)
//         }
//     }
//
//     func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
//         sections.count
//     }
//
//     func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
//         sections[section].elements.count
//     }
//
//     func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
//         switch indexPath.section {
//         case Sections.search.rawValue:
//             return {
//                 HelpSearchCell { [weak self] term in
//                     self?.dataSource.filterTerm = term
//                 }
//             }
//         case Sections.items.rawValue:
//             let viewModel = self.dataSource.filteredArticles[indexPath.item]
//             return { HelpCell(vm: viewModel) }
//         default:
//             return { ASCellNode() }
//         }
//     }
//
//     func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
//         ASSizeRange.fullWidth(collectionNode: collectionNode)
//     }
//
//     func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
//         self.dataSource.filteredArticles[indexPath.item].open.toggle()
//     }
// }
