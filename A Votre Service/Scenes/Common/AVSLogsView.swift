//
// Created by Andrei on 24.03.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//

import SwiftUI
import Disk
import SVProgressHUD

struct ActivityViewController: UIViewControllerRepresentable {

    var activityItems: [Any]
    var applicationActivities: [UIActivity]? = nil

    func makeUIViewController(context: UIViewControllerRepresentableContext<ActivityViewController>) -> UIActivityViewController {
        let controller = UIActivityViewController(activityItems: activityItems, applicationActivities: applicationActivities)
        return controller
    }

    func updateUIViewController(_ uiViewController: UIActivityViewController, context: UIViewControllerRepresentableContext<ActivityViewController>) {}

}

struct AVSLogsView: View {

    struct AVSLogFile: Identifiable {
        var id: String { name }
        var name: String
        var url: URL
    }

    @State var files = [AVSLogFile]()
    @State var selectedFile: AVSLogFile?

    var body: some View {
        ScrollView {
            VStack {

                Button {
                    do {
                        try Disk.remove("Logs", from: .caches)
                        files = []
                    } catch let error {
                        SVProgressHUD.showError(withStatus: error.localizedDescription)
                    }
                } label: {
                    Text("Clear Logs").fontWeight(.bold).padding()
                }.padding()

                Button {
                    refreshLogs()
                } label: {
                    Text("Refresh Logs").fontWeight(.bold).padding()
                }.padding()

                ForEach(files) { file in
                    HStack {
                        Text(file.name)
                        Spacer()
                    }.padding()
                     .onTapGesture {
                         selectedFile = file
                     }
                }
            }
        }.sheet(item: $selectedFile) { file in
            ActivityViewController(activityItems: [file.url])
        }.onAppear {
            refreshLogs()
        }
    }

    func refreshLogs() {
        do {
            let fileManager = FileManager.default
            let logsURL = try Disk.url(for: "Logs", in: .caches)
            let fileURLs = try fileManager.contentsOfDirectory(at: logsURL, includingPropertiesForKeys: nil)
            files = fileURLs.map { AVSLogFile(name: $0.lastPathComponent, url: $0) }.sorted { $0.name > $1.name }
        } catch {
            print("Error while enumerating files: \(error.localizedDescription)")
        }
    }
}