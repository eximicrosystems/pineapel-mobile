//
// Created by Andrei Stoicescu on 26/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit
import SwiftyUserDefaults
import AsyncDisplayKit
import SVProgressHUD
import Fakery

class ASListViewController<ItemType>: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {
    var items: [ItemType] = []

    lazy var refreshControl = UIRefreshControl()
    var collectionNode: ASCollectionNode

    override init() {
        let collectionNode = ASCollectionNode.avsCollection()
        self.collectionNode = collectionNode
        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()
        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)
        collectionNode.view.addSubview(refreshControl)
        SVProgressHUD.show()
        refreshItems()
    }

    @objc func refreshItems() {
        // Override in subclasses
        fatalError("refreshItems has not been implemented")
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        items.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        // Override in subclasses
        fatalError("nodeBlockForItemAtIndexPath has not been implemented")
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        // Override in subclasses
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        ASSizeRange.fullWidth(collectionNode: collectionNode)
    }

}
