//
// Created by Andrei Stoicescu on 30/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit

class SpinnerLoadingNode: ASDisplayNode {
    enum Size {
        case small
        case medium
        case large
        var size: CGFloat {
            switch self {

            case .small:
                return 16
            case .medium:
                return 32
            case .large:
                return 64
            }
        }
    }

    private var activityIndicatorNode: ASDisplayNode?
    private var activityIndicatorView: UIActivityIndicatorView?
    private var size: Size
    private var scale: CGFloat

    var loading: Bool {
        didSet {
            guard let node = activityIndicatorNode else { return }
            if node.isNodeLoaded {
                loading ? startAnimating() : stopAnimating()
            }
        }
    }

    init(size: Size = .large, color: UIColor, scale: CGFloat = 1, loading: Bool = false) {
        self.size = size
        self.scale = scale
        self.loading = loading
        super.init()

        automaticallyManagesSubnodes = true

        activityIndicatorNode = ASDisplayNode(viewBlock: { () -> UIView in
            let view = UIActivityIndicatorView(style: .medium)
            view.backgroundColor = .clear
            view.color = color
            view.tintColor = color
            if self.scale != 1 {
                view.transform = view.transform.scaledBy(x: self.scale, y: self.scale)
            }
            self.activityIndicatorView = view
            return view
        }, didLoad: { [weak self] node in
            guard let self = self else { return }
            if self.loading {
                self.startAnimating()
            } else {
                self.stopAnimating()
            }
        })
    }

    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        activityIndicatorNode!.size(constrainedSize.max.width, size.size).centered()
    }

    private func startAnimating() {
        isHidden = false
        activityIndicatorView?.startAnimating()
    }

    private func stopAnimating() {
        isHidden = true
        activityIndicatorView?.stopAnimating()
    }

}