//
// Created by Andrei Stoicescu on 22/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit
import SwiftyUserDefaults
import AsyncDisplayKit
import SVProgressHUD
import Fakery
import PromiseKit

class ASPaginatedListViewController<ItemType>: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

    var dataSource = PaginatedDataSource<ItemType>()

    lazy var refreshControl = UIRefreshControl()
    var collectionNode: ASCollectionNode

    override init() {
        collectionNode = ASCollectionNode.avsCollection()
        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    func shouldBatchFetch(for collectionNode: ASCollectionNode) -> Bool {
        dataSource.hasNextPage
    }

    func collectionNode(_ collectionNode: ASCollectionNode, willBeginBatchFetchWith context: ASBatchContext) {
        dataSource.getNextPage().done { _ in
            self.collectionNode.reloadData {
                context.completeBatchFetching(true)
            }
        }.catch { error in
            Log.thisError(error)
            context.completeBatchFetching(false)
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()

        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)

        collectionNode.view.addSubview(refreshControl)

        SVProgressHUD.show()
        refreshItems()
    }

    func refreshItems(showErrors: Bool = true) {
        dataSource.getFirstPage().done { items in
            SVProgressHUD.dismiss()
            self.collectionNode.reloadData()
        }.ensure { [weak self] in
            guard let self = self else { return }
            SVProgressHUD.dismiss()
            self.refreshControl.endRefreshing()
            self.showEmptyScreen(self.dataSource.items.count == 0)
        }.catch { error in
            Log.thisError(error)
            if showErrors {
                SVProgressHUD.showError(withStatus: error.localizedDescription)
            }
        }
    }

    func showEmptyScreen(_ show: Bool) {
        // override in subclasses. used in refresh items
    }

    @objc func refreshItems() {
        refreshItems(showErrors: true)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        dataSource.items.count
    }

    func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
        1
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        // Override in subclasses
        fatalError("nodeBlockForItemAtIndexPath has not been implemented")
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        // Override in subclasses
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        ASSizeRange.fullWidth(collectionNode: collectionNode)
    }

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
    }
}
