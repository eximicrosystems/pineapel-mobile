//
//  SendNotificationsView.swift
//  A Votre Service
//
//  Created by Andrei Stoicescu on 01/09/2020.
//  Copyright © 2020 A Votre Service LLC. All rights reserved.
//

import SwiftUI
import Alamofire
import PromiseKit
import SVProgressHUD

struct SendNotificationsView: View {
    struct NotificationUploader: AssetMediaUploadBuilder {

        func createUploader() -> ConciergeForm.AssetMediaUploader.MediaUploaderClosure {
            { data, fileType, progress, completion in
                let uploadDataPromise: Promise<FileUploadResponse>

                switch fileType {
                case .image:
                    uploadDataPromise = ConciergeService().uploadImage(imageData: data, progress: progress)
                case .video:
                    uploadDataPromise = ConciergeService().uploadVideo(videoData: data, progress: progress)
                }

                uploadDataPromise.then { result -> Promise<ConciergeMediaResponse> in
                    switch fileType {
                    case .image:
                        return ConciergeService().updateMedia(params: .image(targetID: String(result.fileID)))
                    case .video:
                        return ConciergeService().updateMedia(params: .video(targetID: String(result.fileID)))
                    }
                }.done { result in
                    completion(.success(result.mediaID))
                }.catch { error in
                    completion(.failure(APIError.from(error)))
                }
            }
        }
    }

    @ObservedObject var form: NotificationForm
    @ObservedObject var dataProvider = DataProvider()
    @State private var enableSheet = false
    var dismissBlock: (() -> Void)?

    var isInEditMode: Bool {
        form.editMode == .edit
    }

    var isMultiRecipientNotification: Bool {
        guard let notificationType = self.form.notificationType.first?.underlyingValue else { return false }
        switch notificationType {
        case .info, .fitness, .event:
            return true
        default:
            return false
        }
    }

    func notificationTypeInput() -> some View {
        ConciergeForm.SelectInput<FeedNotificationItem.NotificationType>(
                label: "Type of Notification",
                options: NotificationForm.notificationTypes,
                value: $form.notificationType,
                allowBlank: false,
                readonly: isInEditMode
        )
    }

    func nameInput() -> some View {
        ConciergeForm.TextInput(label: "Subject", value: $form.subject)
    }

    func recipientIDsInput() -> some View {
        if isMultiRecipientNotification {
            return ConciergeForm.SelectInput<User>(
                    label: "Recipient",
                    placeholder: "Select Recipient",
                    options: form.recipientOptions,
                    value: $form.recipient,
                    multipleSelect: true,
                    readonly: isInEditMode,
                    filterStore: form.recipientFilterStore)
        } else {
            return ConciergeForm.SelectInput<User>(
                    label: "Recipient",
                    placeholder: "Select Recipient",
                    options: form.recipientOptions,
                    value: $form.recipient,
                    readonly: isInEditMode)
        }
    }

    func mediaFileIDInput() -> some View {
        ConciergeForm.MediaFile(
                label: "Media File",
                placeholder: "Tap to upload media file",
                value: $form.file,
                uploadBuilder: NotificationUploader(),
                initialMediaFiles: form.conciergeNotification?.notification.mediaFile ?? []
        )
    }

    func bodyInput() -> some View {
        ConciergeForm.TextArea(label: "Details", value: $form.details)
    }

    func articleIDInput() -> some View {
        ConciergeForm.SelectInput<Article>(
                label: "Internal Link",
                placeholder: "- None -",
                options: form.internalLinkOptions,
                value: $form.internalLink
        )
    }

    func externalLinkInput() -> some View {
        ConciergeForm.TextInput(label: "External Link", value: $form.externalLink)
    }

    func deliveryTimeInput() -> some View {
        ConciergeForm.DateField(label: "Delivery Time", value: $form.deliveryTime)
    }

    func deliveryItemInput() -> some View {
        ConciergeForm.TextInput(label: "Item for Delivery", value: $form.deliveryItem)
    }

    func fitnessStartDateInput() -> some View {
        ConciergeForm.DateField(label: "Start Date", value: $form.startDate)
    }

    func fitnessTypeInput() -> some View {
        ConciergeForm.TextInput(label: "Type", value: $form.fitnessType)
    }

    func fitnessInstructorInput() -> some View {
        ConciergeForm.TextInput(label: "Instructor", value: $form.fitnessInstructor)
    }

    func fitnessDurationInput() -> some View {
        ConciergeForm.TextInput(label: "Duration", value: $form.fitnessDuration)
    }

    func packagesNumberInput() -> some View {
        ConciergeForm.TextInput(label: "Number", value: $form.packagesNumber)
    }

    func packagesSignedByConciergeIDInput() -> some View {
        ConciergeForm.SelectInput<User>(
                label: "Signed By",
                options: form.signedByOptions,
                value: $form.signedBy
        )
    }

    func eventNameInput() -> some View {
        ConciergeForm.TextInput(label: "Event Name", value: $form.eventName)
    }

    func eventHostInput() -> some View {
        ConciergeForm.TextInput(label: "Event Host", value: $form.eventHost)
    }

    var body: some View {
        VStack {
            if dataProvider.isLoading {
                Text("Please Wait")
            } else {
                ScrollView(showsIndicators: false) {
                    VStack(spacing: 20) {

                        VStack(spacing: 20) {
                            if form.displayField(.notificationType) {
                                notificationTypeInput()
                            }

                            if form.displayField(.name) {
                                nameInput()
                            }

                            if form.displayField(.deliveryTime) {
                                deliveryTimeInput()
                            }

                            if form.displayField(.deliveryItem) {
                                deliveryItemInput()
                            }

                            if form.displayField(.body) {
                                bodyInput()
                            }
                        }

                        VStack(spacing: 10) {
                            if form.displayField(.eventName) {
                                eventNameInput()
                            }

                            if form.displayField(.eventName) {
                                eventHostInput()
                            }

                            if form.displayField(.fitnessDuration) {
                                fitnessDurationInput()
                            }

                            if form.displayField(.fitnessInstructor) {
                                fitnessInstructorInput()
                            }

                            if form.displayField(.externalLink) {
                                externalLinkInput()
                            }

                            if form.displayField(.articleID) {
                                articleIDInput()
                            }

                            if form.displayField(.mediaFileID) {
                                mediaFileIDInput()
                            }
                        }

                        VStack(spacing: 10) {
                            if form.displayField(.packagesNumber) {
                                packagesNumberInput()
                            }

                            if form.displayField(.recipientIDs) {
                                recipientIDsInput()
                            }

                            if form.displayField(.packagesSignedByConciergeID) {
                                packagesSignedByConciergeIDInput()
                            }

                            if form.displayField(.startDate) {
                                fitnessStartDateInput()
                            }

                            if form.displayField(.fitnessType) {
                                fitnessTypeInput()
                            }
                        }

                    }.padding(.top, 20)
                     .padding(.horizontal, 20)
                }
            }
        }.fullViewBackgroundColor()
         .navigationBarItems(
                 leading: Button(action: {
                     dismissBlock?()
                 }) {
                     Image(uiImage: Asset.Images.Concierge.icClose.image.withRenderingMode(.alwaysTemplate)).foregroundColor(.primaryIcon)
                 },
                 trailing: Button(action: {
                     guard let params = form.asParams else { return }
                     SVProgressHUD.show()

                     ConciergeService().createNotification(params: params).done {
                         SVProgressHUD.dismiss()
                         dismissBlock?()
                     }.catch { error in
                         Log.thisError(error)
                         SVProgressHUD.showError(withStatus: error.localizedDescription)
                     }

                 }) {
                     Text("Save").navigationSaveButton()
                 }.disabled(!form.isValid))
         .navigationBarTitle(Text("Send Notification"), displayMode: .inline)
         .onAppear {
             dataProvider.loadData().done {
                 form.internalLinkOptions = dataProvider.articles.map { article in
                     .init(id: article.articleNodeID, label: article.title ?? "", underlyingValue: article)
                 }

                 form.recipientOptions = dataProvider.users.map { user in
                     .init(id: user.uid, label: user.fullName, underlyingValue: user)
                 }

                 form.signedByOptions = dataProvider.concierges.map { user in
                     .init(id: user.uid, label: user.fullName, underlyingValue: user)
                 }

                 if form.editMode == .new {
                     form.buildingID = dataProvider.buildingID
                 } else {
                     form.setupConciergeNotification()
                 }

                 let floors = dataProvider.users.compactMap(\.floor).sorted { $0 < $1 }.map { PickerOption(id: String($0), label: String($0)) }
                 let tags = dataProvider.tags.map { PickerOption(id: $0.tagID, label: $0.name) }
                 form.recipientFilterStore.filters = [
                     .init(name: "Floor", options: floors) { (user: User, selectedOption: PickerOption?) in
                         guard let option = selectedOption else { return false }
                         guard let floor = user.floor else { return false }
                         return String(floor) == option.id
                     },
                     .init(name: "Tag", options: tags) { (user: User, selectedOption: PickerOption?) in
                         guard let option = selectedOption else { return false }
                         return user.tagIDs.contains(option.id)
                     }
                 ]

                 form.setupRecipientFilterStoreListening()

             }.catch { error in
                 Log.thisError(error)
             }
         }
    }
}

struct SendNotificationsView_Previews: PreviewProvider {
    static let form = SendNotificationsView.NotificationForm()

    static var previews: some View {
        Appearance.apply()

        return NavigationView {
            SendNotificationsView(form: form)
                    .navigationBarTitle(Text("Send Notification"), displayMode: .inline)
        }
    }
}
