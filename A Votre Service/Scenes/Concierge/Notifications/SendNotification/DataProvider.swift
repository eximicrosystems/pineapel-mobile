//
// Created by Andrei Stoicescu on 02/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import PromiseKit

extension SendNotificationsView {
    class DataProvider: ObservableObject {
        @Published var users: [User] = []
        @Published var concierges: [User] = []
        @Published var articles: [Article] = []
        @Published var tags: [ConciergeTag] = []
        @Published var buildingID: String?

        @Published var isLoading = false
        @Published var error: Error? = nil

        func loadData() -> Promise<Void> {
            guard !isLoading else { return .value(()) }

            isLoading = true
            let service = ConciergeService()

            return firstly {
                BuildingDetails.getBuildingID().get { self.buildingID = $0 }
            }.then { buildingID in
                when(fulfilled:
                     service.getTags(),
                     service.getConciergeList(),
                     service.getAllUsers(),
                     ResidentService().getBuildingThingsToKnow(buildingID: buildingID))
            }.map { tags, concierges, users, articles in
                self.tags = tags
                self.concierges = concierges
                self.users = users
                self.articles = articles
            }.ensure {
                self.isLoading = false
            }
        }
    }
}