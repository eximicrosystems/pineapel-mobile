//
// Created by Andrei Stoicescu on 11/11/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import PromiseKit
import Combine
import SVProgressHUD
import SwiftUI

class ConciergeNotificationsListViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

    lazy var emptyNode = EmptyNode(emptyNodeType: .generic, showAction: false)

    lazy var refreshControl = UIRefreshControl()
    var collectionNode: ASCollectionNode

    var dataSource: ObservablePaginatedDataSource<ConciergeNotification>
    private var subscriptions = Set<AnyCancellable>()

    override init() {
        collectionNode = .avsCollection()
        dataSource = .init { per, page in
            BuildingDetails.getBuildingID().then { buildingID in
                ConciergeService().getConciergeNotifications(per: per, page: page, buildingID: buildingID)
            }
        }

        dataSource.cacheable = true
        dataSource.cacheKey = "concierge_notifications"

        super.init(node: collectionNode)

        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupBindings()
        navigationItem.title = "Sent Notifications"
        replaceSystemBackButton()
        showEmptyScreen(false)
        collectionNode.view.backgroundView = emptyNode.view
        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)
        collectionNode.view.addSubview(refreshControl)
        refreshItems()
    }

    func setupBindings() {
        dataSource.$items
                .receive(on: DispatchQueue.main)
                .dropFirst()
                .sink { [weak self] items in
                    self?.collectionNode.reloadData()
                }.store(in: &subscriptions)

        dataSource.$hasNoItemsAfterLoading
                .receive(on: DispatchQueue.main)
                .sink { [weak self] hasNoItemsAfterLoading in
                    self?.showEmptyScreen(hasNoItemsAfterLoading)
                }.store(in: &subscriptions)

        dataSource.$showLoadingIndicator
                .receive(on: DispatchQueue.main)
                .sink { [weak self] loading in
                    guard let self = self else { return }

                    if loading {
                        if !self.refreshControl.isRefreshing {
                            SVProgressHUD.show()
                        }
                    } else {
                        SVProgressHUD.dismiss()
                        self.refreshControl.endRefreshing()
                    }
                }.store(in: &subscriptions)
    }

    @objc func refreshItems() {
        dataSource.loadMoreContentIfNeeded()
    }

    func showEmptyScreen(_ show: Bool) {
        emptyNode.isHidden = !show
    }

    func collectionView(_ collectionView: ASCollectionView, willDisplayNodeForItemAt indexPath: IndexPath) {
        dataSource.loadMoreContentIfNeeded(currentItem: dataSource.items[indexPath.item])
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        dataSource.items.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let notification = dataSource.items[indexPath.item]
        return {
            ConciergePagerListNodes.RequestCell(conciergeNotification: notification)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        let conciergeNotification = dataSource.items[indexPath.item]
        let form = SendNotificationsView.NotificationForm(conciergeNotification: conciergeNotification)

        let view = SendNotificationsView(form: form, dismissBlock: { [weak self] in
            self?.navigationController?.dismiss(animated: true)
        })

        let nav = AVSNavigationController(rootViewController: UIHostingController(rootView: view))
        nav.isModalInPresentation = true
        navigationController?.present(nav, animated: true)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        ASSizeRange.fullWidth(collectionNode: collectionNode)
    }
}
