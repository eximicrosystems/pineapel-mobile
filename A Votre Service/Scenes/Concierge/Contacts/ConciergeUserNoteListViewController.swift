//
// Created by Andrei Stoicescu on 05.03.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import PromiseKit
import Combine
import SVProgressHUD
import SwiftUI
import DifferenceKit

class ConciergeUserNoteListViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

    lazy var emptyNode = EmptyNode(emptyNodeType: .generic, showAction: false)

    lazy var refreshControl = UIRefreshControl()
    var collectionNode: ASCollectionNode

    var dataSource: ObservablePaginatedDataSource<ConciergeNote>
    var notes = [ConciergeNote]()
    private var subscriptions = Set<AnyCancellable>()
    var userId: String

    init(userId: String) {
        self.userId = userId
        collectionNode = .avsCollection()
        dataSource = .init { per, page in
            ConciergeService().getNotes(page: page, per: per, id: userId)
        }

        super.init(node: collectionNode)

        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupBindings()
        navigationItem.title = "Notes"
        replaceSystemBackButton()
        showEmptyScreen(false)
        collectionNode.view.backgroundView = emptyNode.view
        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)
        collectionNode.view.addSubview(refreshControl)
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: Asset.Images.Concierge.icCompose.image.withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(tapAdd))
        refreshItems()
    }

    @objc func tapAdd() {
        startEditing(note: nil)
    }

    func reloadData() {
        let source = notes
        let target = dataSource.items

        let stagedChangeSet = StagedChangeset(source: source, target: target)
        for changeSet in stagedChangeSet {
            notes = changeSet.data
            collectionNode.performDiffBatchUpdates(changeSet: changeSet)
        }
    }

    func setupBindings() {
        dataSource.$items
                .receive(on: DispatchQueue.main)
                .sink { [weak self] items in
                    self?.reloadData()
                }.store(in: &subscriptions)

        dataSource.$hasNoItemsAfterLoading
                .receive(on: DispatchQueue.main)
                .sink { [weak self] hasNoItemsAfterLoading in
                    self?.showEmptyScreen(hasNoItemsAfterLoading)
                }.store(in: &subscriptions)

        dataSource.$showLoadingIndicator
                .receive(on: DispatchQueue.main)
                .sink { [weak self] loading in
                    guard let self = self else { return }
                    if loading {
                        if !self.refreshControl.isRefreshing {
                            SVProgressHUD.show()
                        }
                    } else {
                        SVProgressHUD.dismiss()
                        self.refreshControl.endRefreshing()
                    }
                }.store(in: &subscriptions)
    }

    @objc func refreshItems() {
        dataSource.loadMoreContentIfNeeded()
    }

    func showEmptyScreen(_ show: Bool) {
        emptyNode.isHidden = !show
    }

    func collectionView(_ collectionView: ASCollectionView, willDisplayNodeForItemAt indexPath: IndexPath) {
        dataSource.loadMoreContentIfNeeded(currentItem: notes[indexPath.item])
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        notes.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let note = notes[indexPath.item]
        return {
            let cell = NoteNodes.MenuCell(title: note.name ?? "", subtitle: note.created)

            cell.didSwipe = { [weak self, weak cell] swiped in
                guard let cell = cell else { return }
                if swiped {
                    self?.swipeCloseAll(except: cell)
                }
            }

            cell.didTapAction = { [weak self] action in
                guard let self = self else { return }
                switch action {
                case .edit:
                    self.swipeCloseAll()
                    self.startEditing(note: note)
                case .delete:
                    self.swipeCloseAll()
                    guard let index = self.dataSource.removeItem(note) else { return }
                    self.reloadData()

                    ConciergeService().deleteNote(noteID: note.noteID).catch { error in
                        Log.thisError(error)
                        DispatchQueue.main.async { [weak self] in
                            guard let self = self else { return }
                            SVProgressHUD.showError(withStatus: "There was an error while deleting your note")
                            self.dataSource.insertItem(note, at: index)
                            self.reloadData()
                        }
                    }
                }
            }
            return cell
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        swipeCloseAll()
        let note = notes[indexPath.item]
        startEditing(note: note)
    }

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        swipeCloseAll()
    }

    func swipeCloseAll(except exceptNode: NoteNodes.MenuCell? = nil) {
        for node in collectionNode.visibleNodes {
            guard let node = node as? NoteNodes.MenuCell else { continue }
            if node == exceptNode { continue }
            if node.swiped { node.swiped = false }
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }

    func startEditing(note: ConciergeNote?) {
        let view = NotesFormView(note: note, residentId: userId, dismissBlock: { [weak self] requiresReload in
            guard let self = self else { return }
            self.navigationController?.dismiss(animated: true)
            if requiresReload {
                self.refreshItems()
            }
        })

        let nav = AVSNavigationController(rootViewController: UIHostingController(rootView: view))
        nav.isModalInPresentation = true
        navigationController?.present(nav, animated: true)
    }
}
