//
// Created by Andrei Stoicescu on 28/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit

struct NoteNodes {

    enum MenuButtonAction {
        case edit
        case delete
    }

    class MenuCellOverlay: ASDisplayNode {
        var labelsNode: ConciergeGenericNodes.TitleSubtitleNode

        var iconNode: ASImageNode?
        var cardNode: CardNode

        init(title: String, subtitle: String?) {
            labelsNode = ConciergeGenericNodes.TitleSubtitleNode(title: title, subtitle: subtitle)
            cardNode = CardNode(rounded: false)
            iconNode = ASImageNode(image: Asset.Images.Concierge.icNote.image)

            super.init()
            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            [
                iconNode?.margin(top: 5).alignSelf(.start),
                labelsNode.filling()
            ].hStacked().spaced(15).cardWrapped(cardNode)
        }
    }

    class MenuCellUnderlay: ASDisplayNode {

        var editButton = ASButtonNode()
        var deleteButton = ASButtonNode()

        var tappedButtonAction: ((MenuButtonAction) -> Void)?

        override init() {
            editButton.setImage(Asset.Images.Concierge.editButton.image, for: .normal)
            deleteButton.setImage(Asset.Images.Concierge.trashButton.image, for: .normal)

            super.init()
            automaticallyManagesSubnodes = true

            editButton.addTarget(self, action: #selector(tappedEditButton), forControlEvents: .touchUpInside)
            deleteButton.addTarget(self, action: #selector(tappedDeleteButton), forControlEvents: .touchUpInside)
        }

        @objc func tappedEditButton() {
            tappedButtonAction?(.edit)
        }

        @objc func tappedDeleteButton() {
            tappedButtonAction?(.delete)
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            [
                ASDisplayNode.spacer(),
                [editButton, deleteButton].hStacked().spaced(10).aligned(.center).justified(.center)
            ].hStacked().margin(10)
        }
    }

    class MenuCell: ASCellNode {

        var didSwipe: ((Bool) -> Void)?
        var didTapAction: ((MenuButtonAction) -> Void)?

        var swiped = false {
            didSet {
                didSwipe?(swiped)

                switch swiped {
                case true:
                    UIView.animate(withDuration: 0.3) {
                        self.overlay.view.transform = CGAffineTransform(translationX: -120, y: 0)
                        self.dimGradient.alpha = 1
                    }
                case false:
                    UIView.animate(withDuration: 0.3) {
                        self.overlay.view.transform = .identity
                        self.dimGradient.alpha = 0
                    }
                }
            }
        }

        var overlay: MenuCellOverlay
        var underlay: MenuCellUnderlay
        var line = ASDisplayNode()
        var dimGradient = ASImageNode()

        init(title: String, subtitle: String?) {
            overlay = MenuCellOverlay(title: title, subtitle: subtitle)
            underlay = MenuCellUnderlay()
            line.backgroundColor = AVSColor.divider.color
            dimGradient.alpha = 0
            dimGradient.backgroundColor = .clear
            // dimGradient.image = Asset.Images.Concierge.dimGradient.image

            super.init()

            underlay.tappedButtonAction = { [weak self] action in
                self?.didTapAction?(action)
            }

            backgroundColor = AVSColor.secondaryBackground.color
            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            let dimRelative = ASRelativeLayoutSpec(horizontalPosition: .start, verticalPosition: .center, sizingOption: .minimumWidth, child: dimGradient)
            return [dimRelative.overlay(overlay.background(underlay)), line.height(.points(1))].vStacked()
        }

        override func didLoad() {
            super.didLoad()

            let swipeOpen = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction))
            swipeOpen.direction = .left

            let swipeClosed = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction))
            swipeClosed.direction = .right

            view.addGestureRecognizer(swipeOpen)
            view.addGestureRecognizer(swipeClosed)
        }

        @objc func swipeAction(_ gesture: UISwipeGestureRecognizer) {
            switch gesture.direction {
            case .left:
                swiped = true
            case .right:
                swiped = false
            default:
                ()
            }
        }
    }
}