//
// Created by Andrei Stoicescu on 21/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import PromiseKit
import Combine

class ConciergeAmenityReservationListViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

    enum Sections: Int, CaseIterable {
        case header
        case items
    }

    lazy var emptyNode = EmptyNode(emptyNodeType: .generic, showAction: false)

    let status: AmenityReservationsPagerDataSource.Status

    lazy var refreshControl = UIRefreshControl()
    var collectionNode: ASCollectionNode

    var dataSource: ObservablePaginatedDataSource<AmenityReservation>
    private var subscriptions = Set<AnyCancellable>()

    var didSelectReservation: ((AmenityReservation) -> Void)?
    var didReloadDataSource: (() -> Void)?

    init(status: AmenityReservationsPagerDataSource.Status, dataSource: ObservablePaginatedDataSource<AmenityReservation>) {
        collectionNode = .avsCollection()
        self.dataSource = dataSource
        self.dataSource.cacheable = true
        self.dataSource.cacheKey = "concierge_more_amenity_reservations_\(status.rawValue)"
        self.status = status
        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupBindings()
        replaceSystemBackButton()
        showEmptyScreen(false)
        collectionNode.view.backgroundView = emptyNode.view
        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)
        collectionNode.view.addSubview(refreshControl)
        refreshItems()
    }

    func setupBindings() {
        dataSource.$items
                .receive(on: DispatchQueue.main)
                .sink { [weak self] items in
                    self?.collectionNode.reloadData()
                    // TODO: right now this is for the pager counter to trigger a reload since it needs the items
                    // If there will be a badge request for reload, this should be removed
                    self?.didReloadDataSource?()
                }.store(in: &subscriptions)

        dataSource.$showLoadingIndicator
                .receive(on: DispatchQueue.main)
                .sink { [weak self] loading in
                    guard let self = self else { return }
                    if loading && self.dataSource.currentPage == 0 {
                        self.didReloadDataSource?()
                    }

                    if loading {
                        if !self.refreshControl.isRefreshing {
                            SVProgressHUD.show()
                        }
                    } else {
                        SVProgressHUD.dismiss()
                        self.refreshControl.endRefreshing()
                    }
                }.store(in: &subscriptions)

        dataSource.$hasNoItemsAfterLoading.receive(on: DispatchQueue.main)
                                          .sink { [weak self] noItems in
                                              guard let self = self else { return }
                                              self.showEmptyScreen(noItems)
                                          }.store(in: &subscriptions)
    }

    @objc func refreshItems() {
        dataSource.loadMoreContentIfNeeded()
    }

    func showEmptyScreen(_ show: Bool) {
        emptyNode.isHidden = !show
    }

    func collectionView(_ collectionView: ASCollectionView, willDisplayNodeForItemAt indexPath: IndexPath) {
        if indexPath.section == Sections.items.rawValue {
            dataSource.loadMoreContentIfNeeded(currentItem: self.dataSource.items[indexPath.item])
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case Sections.header.rawValue:
            return dataSource.items.count > 0 ? 1 : 0
        case Sections.items.rawValue:
            return dataSource.items.count
        default:
            return 0
        }
    }

    func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
        Sections.allCases.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        switch indexPath.section {
        case Sections.header.rawValue:
            return { ConciergePagerListNodes.Header(title: "Tenant Name", subject: "Reserved Date") }
        case Sections.items.rawValue:
            let item = dataSource.items[indexPath.item]
            return {
                ConciergePagerListNodes.RequestCell(reservation: item)
            }
        default:
            return { ASCellNode() }
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        let request = dataSource.items[indexPath.item]
        didSelectReservation?(request)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }
}