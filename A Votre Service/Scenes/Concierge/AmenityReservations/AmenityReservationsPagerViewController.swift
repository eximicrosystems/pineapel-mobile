//
// Created by Andrei Stoicescu on 21/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation

import Foundation
import AsyncDisplayKit
import PromiseKit
import Combine
import SwiftUI

class AmenityReservationsPagerDataSource: ObservableObject {
    enum Status: String {
        case new
        case future

        var startDate: Date {
            switch self {
            case .new:
                return Date().start(of: .day)
            case .future:
                return 1.days.later.start(of: .day)
            }
        }

        var endDate: Date {
            switch self {
            case .new:
                return Date().end(of: .day)
            case .future:
                return 1.weeks.later.end(of: .day)
            }
        }

        var displayString: String {
            switch self {
            case .new:
                return "New"
            case .future:
                return "Future"
            }
        }
    }

    static var statuses = [Status.new, .future]
    typealias DataSource = ObservablePaginatedDataSource<AmenityReservation>

    @Published var sources: [Status: DataSource] = {
        AmenityReservationsPagerDataSource.statuses.reduce(into: [Status: DataSource]()) { dict, status in
            dict[status] = .init(paginate: { per, page in
                ConciergeService().getAmenityReservationList(page: page, per: per, startDate: status.startDate, endDate: status.endDate)
            })
        }
    }()

    @Published var newCount: Int = 0

    func reloadBadge() {
        guard let source = sources[.new] else { return }
        newCount = source.items.count
    }

    func reloadReservation(_ reservation: AmenityReservation, source: AmenityReservationsPagerDataSource.Status) {
        guard let fromSource = sources[source],
              let fromIndex = fromSource.items.firstIndex(where: { $0.id == reservation.id }) else { return }

        fromSource.items[fromIndex] = reservation
    }

}

class AmenityReservationsPagerViewController: ASDKViewController<ASDisplayNode>, ASPagerDataSource, ASPagerDelegate {
    var pager = ASPagerNode()

    let pageItems = AmenityReservationsPagerDataSource.statuses
    var segmentedBar: SegmentedDisplayNode

    var dataSource: AmenityReservationsPagerDataSource
    private var subscriptions = Set<AnyCancellable>()

    init(dataSource: AmenityReservationsPagerDataSource) {
        self.dataSource = dataSource
        segmentedBar = SegmentedDisplayNode(items: pageItems.map(\.displayString), selection: 0)

        let mainNode = ASDisplayNode()
        mainNode.automaticallyManagesSubnodes = true
        mainNode.automaticallyRelayoutOnSafeAreaChanges = true

        super.init(node: mainNode)

        mainNode.layoutSpecBlock = { node, range in
            [
                self.segmentedBar.margin(top: node.safeAreaInsets.top),
                self.pager.growing()
            ].vStacked()
        }

        segmentedBar.didSelect = { [weak self] index in
            self?.pager.scrollToPage(at: index, animated: true)
        }

        pager.setDataSource(self)
        pager.setDelegate(self)

        setupBindings()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func numberOfPages(in pagerNode: ASPagerNode) -> Int {
        pageItems.count
    }

    func setupBindings() {
        dataSource.$newCount
                .receive(on: DispatchQueue.main)
                .sink { [weak self] val in
                    self?.updateBadge(val)
                }
                .store(in: &subscriptions)
    }

    func updateBadge(_ val: Int) {
        segmentedBar.pendingBadgeCount = val
    }

    func pagerNode(_ pagerNode: ASPagerNode, nodeBlockAt index: Int) -> ASCellNodeBlock {
        let item = pageItems[index]
        let dataSource = self.dataSource.sources[item]!
        return {
            ASCellNode(viewControllerBlock: { [weak self] in
                guard let self = self else { return UIViewController() }
                let vc = ConciergeAmenityReservationListViewController(status: item, dataSource: dataSource)
                vc.didSelectReservation = { [weak self] reservation in
                    guard let self = self else { return }
                    let detailVC = CurrentReservationDetailViewController(reservation: reservation)
                    detailVC.didChangeReservation = { [weak self] reservation in
                        self?.dataSource.reloadReservation(reservation, source: item)
                    }
                    self.navigationController?.pushViewController(detailVC, animated: true)
                }
                vc.didReloadDataSource = { [weak self] in
                    guard let self = self else { return }
                    self.reloadPendingBadge()
                }
                return vc
            })
        }
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        segmentedBar.selection = pager.currentPageIndex
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Amenity Reservations"
        replaceSystemBackButton()
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(tappedAddReservation))
    }

    @objc func tappedAddReservation() {
        let view = ConciergeAmenityReservationView(dismissBlock: { [weak self] requiresRefresh in
            self?.navigationController?.dismiss(animated: true)
            if requiresRefresh {
                self?.dataSource.sources[.new]?.loadMoreContentIfNeeded()
            }
        })

        let nav = AVSNavigationController(rootViewController: UIHostingController(rootView: view))
        nav.isModalInPresentation = true
        navigationController?.present(nav, animated: true)
    }

    func reloadPendingBadge() {
        dataSource.reloadBadge()
    }
}
