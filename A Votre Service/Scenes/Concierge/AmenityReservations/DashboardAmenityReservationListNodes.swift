//
// Created by Andrei Stoicescu on 31/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit

struct DashboardAmenityReservationListNodes {
    class ReservationCell: ASCellNode {

        var titleNode: ConciergeGenericNodes.TitleSubtitleNode
        var iconNode: ASImageNode
        var line = ASDisplayNode.divider()
        var card = CardNode(rounded: false)

        static func reservationString(reservation: AmenityReservation) -> String {
            var reservationString = " wants to reserve the \"\(reservation.amenityName ?? "amenity")\""
            guard let reservationStartTime = reservation.startTime,
                  let reservationEndTime = reservation.endTime
                    else { return reservationString }

            reservationString +=
            " on \(reservationStartTime.avsDateString())" +
            " at \(reservationStartTime.avsTimeString())" +
            " - \(reservationEndTime.avsTimeString())"

            return reservationString
        }

        init(reservation: AmenityReservation) {
            iconNode = ASImageNode(image: Asset.Images.Concierge.icReservations.image.withRenderingMode(.alwaysTemplate))
            iconNode.tintColor = AVSColor.primaryIcon.color

            let titleAttributedString = NSAttributedString(string: reservation.fullName, attributes: ConciergeGenericNodes.TitleSubtitleNode.highlightedTitleAttributes)
                    .appendingAttributedString(NSAttributedString(string: Self.reservationString(reservation: reservation), attributes: ConciergeGenericNodes.TitleSubtitleNode.titleAttributes))

            titleNode = .init(attributedTitle: titleAttributedString,
                              attributedSubtitle: NSAttributedString(string: reservation.status?.string, attributes: ConciergeGenericNodes.TitleSubtitleNode.subtitleAttributes))

            super.init()
            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            [
                [
                    iconNode,
                    titleNode.filling()
                ].hStacked().spaced(20).aligned(.start)
                 .cardWrapped(card),
                line
            ].vStacked()
        }
    }
}