//
// Created by Andrei Stoicescu on 09/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import Combine

class ConciergeOrderDetailViewController: ASDKViewController<ASDisplayNode>, ASCollectionDataSource, ASCollectionDelegate {
    var collectionNode: ASCollectionNode

    var order: OrderData

    var editingPublisher: PassthroughSubject<(OrderData, to: OrderData.OrderType), Never>

    init(order: OrderData, editingPublisher: PassthroughSubject<(OrderData, to: OrderData.OrderType), Never>) {
        self.editingPublisher = editingPublisher
        self.order = order
        self.collectionNode = ASCollectionNode.avsCollection()
        super.init(node: collectionNode)
        self.collectionNode.delegate = self
        self.collectionNode.dataSource = self
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.replaceSystemBackButton()
        self.navigationItem.title = "Order Details"
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        1
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let order = self.order
        return {
            let cell = ConciergePagerListNodes.SummaryCellNode(order: order)
            cell.didTapAction = { [weak self] in
                guard let self = self else { return }
                guard let nextOrderType = order.orderType.nextOrderType else { return }

                switch nextOrderType {
                case .done:
                    SVProgressHUD.show()
                    ConciergeService().completeOrder(orderID: order.orderID).done { [weak self] in
                        SVProgressHUD.showInfo(withStatus: "The order was marked as completed")
                        self?.requestDidChange(orderType: nextOrderType)
                    }.catch { error in
                        Log.thisError(error)
                        SVProgressHUD.showError(withStatus: error.localizedDescription)
                    }
                default:
                    ()
                }
            }
            return cell
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        ASSizeRange.fullWidth(collectionNode: collectionNode)
    }

    func requestDidChange(orderType: OrderData.OrderType) {
        self.editingPublisher.send((self.order, to: orderType))
    }
}
