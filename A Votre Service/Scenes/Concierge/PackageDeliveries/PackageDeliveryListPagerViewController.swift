//
// Created by Andrei Stoicescu on 17/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import PromiseKit
import Combine
import SwiftUI

class PackageListPagerDataSource: ObservableObject {
    static var statuses = [Package.Status.pending, .delivered]
    typealias DataSource = ObservablePaginatedDataSource<Package>

    @Published var sources: [Package.Status: DataSource] = {
        PackageListPagerDataSource.statuses.reduce(into: [Package.Status: DataSource]()) { dict, status in
            dict[status] = .init(paginate: { per, page in
                ConciergeService().getPackageList(per: per, page: page, status: status)
            })
        }
    }()

    @Published var pendingCount: Int = 0

    var editingPackagePublisher = PassthroughSubject<(Package, to: Package.Status), Never>()

    func movePackageIfNeeded(_ package: Package, to: Package.Status) {
        guard let fromSource = sources[package.status],
              package.status != to,
              let toSource = sources[to] else { return }

        guard let fromIndex = fromSource.items.firstIndex(where: { $0.id == package.id }) else { return }

        var mutatedOrder = package
        mutatedOrder.status = to

        fromSource.items.remove(at: fromIndex)
        toSource.items.insert(mutatedOrder, at: 0)

        if package.status == .pending {
            pendingCount = max(pendingCount - 1, 0)
        }

        if fromSource.items.count == 0 {
            fromSource.hasNoItemsAfterLoading = true
        }
    }

    func reloadPendingBadge() {
        // There's no badge endpoint to load from (was present in the design)
    }

    func reloadNewPackageDeliveries() {
        sources[.pending]?.loadMoreContentIfNeeded()
    }
}

class PackageDeliveryListPagerViewController: ASDKViewController<ASDisplayNode>, ASPagerDataSource, ASPagerDelegate {
    var pager = ASPagerNode()

    let pageItems = PackageListPagerDataSource.statuses
    var segmentedBar: SegmentedDisplayNode

    var dataSource: PackageListPagerDataSource
    private var subscriptions = Set<AnyCancellable>()

    init(dataSource: PackageListPagerDataSource) {
        self.dataSource = dataSource
        segmentedBar = SegmentedDisplayNode(items: pageItems.map(\.displayString), selection: 0)

        let mainNode = ASDisplayNode()
        mainNode.automaticallyManagesSubnodes = true
        mainNode.automaticallyRelayoutOnSafeAreaChanges = true

        super.init(node: mainNode)

        mainNode.layoutSpecBlock = { node, range in
            [
                self.segmentedBar.margin(top: node.safeAreaInsets.top),
                self.pager.growing()
            ].vStacked()
        }

        segmentedBar.didSelect = { [weak self] index in
            self?.pager.scrollToPage(at: index, animated: true)
        }

        pager.setDataSource(self)
        pager.setDelegate(self)

        setupBindings()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func numberOfPages(in pagerNode: ASPagerNode) -> Int {
        pageItems.count
    }

    func setupBindings() {
        dataSource.editingPackagePublisher
                .receive(on: DispatchQueue.main)
                .sink { [weak self] request, toStatus in
                    guard let self = self else { return }
                    self.dataSource.movePackageIfNeeded(request, to: toStatus)
                    self.navigationController?.popToViewController(self, animated: true)
                }.store(in: &subscriptions)

        dataSource.$pendingCount
                .receive(on: DispatchQueue.main)
                .sink { [weak self] val in
                    self?.updateBadge(val)
                }
                .store(in: &subscriptions)
    }

    func updateBadge(_ val: Int) {
        segmentedBar.pendingBadgeCount = val
    }

    func pagerNode(_ pagerNode: ASPagerNode, nodeBlockAt index: Int) -> ASCellNodeBlock {
        let item = pageItems[index]
        let dataSource = self.dataSource.sources[item]!
        return {
            ASCellNode(viewControllerBlock: { [weak self] in
                guard let self = self else { return UIViewController() }
                let vc = PackageDeliveryListViewController(status: item, dataSource: dataSource)
                vc.didSelectPackage = { [weak self] package in
                    guard let self = self else { return }
                    self.navigationController?.pushViewController(PackageDeliveryDetailViewController(package: package, editingPublisher: self.dataSource.editingPackagePublisher), animated: true)
                }
                vc.didReloadDataSource = { [weak self] in
                    guard let self = self else { return }
                    self.reloadPendingBadge()
                }
                return vc
            })
        }
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        segmentedBar.selection = pager.currentPageIndex
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Package Deliveries"
        replaceSystemBackButton()
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(tappedAddPackage))
    }

    @objc func tappedAddPackage() {
        let view = PackageDeliveryFormView(form: .init(), dismissBlock: { [weak self] requiresRefresh in
            self?.navigationController?.dismiss(animated: true)
            if requiresRefresh {
                self?.dataSource.reloadNewPackageDeliveries()
            }
        })

        let nav = AVSNavigationController(rootViewController: UIHostingController(rootView: view))
        nav.isModalInPresentation = true
        navigationController?.present(nav, animated: true)
    }

    func reloadPendingBadge() {
        dataSource.reloadPendingBadge()
    }
}
