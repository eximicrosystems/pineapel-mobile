//
// Created by Andrei Stoicescu on 08/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SFSafeSymbols
import DifferenceKit

struct ConciergePagerListNodes {

    struct PillTag: Identifiable, Hashable, Differentiable {
        var id: String
        var label: String
        var allowClose: Bool = true

        func hash(into hasher: inout Hasher) { hasher.combine(id) }

        var differenceIdentifier: Int { hashValue }
    }

    class PillTagCollection: ASCellNode, ASCollectionDelegate, ASCollectionDataSource {

        var collectionNode: ASCollectionNode
        var items: [PillTag] {
            didSet {
                collectionNode.reloadData()
            }
        }
        var onTap: ((PillTag) -> Void)?

        init(items: [PillTag]) {
            self.items = items
            let flowLayout = UICollectionViewFlowLayout()
            flowLayout.minimumInteritemSpacing = 0
            flowLayout.minimumLineSpacing = 0
            flowLayout.scrollDirection = .horizontal
            collectionNode = ASCollectionNode(collectionViewLayout: flowLayout)
            collectionNode.showsHorizontalScrollIndicator = false
            collectionNode.backgroundColor = .clear

            super.init()
            collectionNode.delegate = self
            collectionNode.dataSource = self
            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            collectionNode.height(.points(50)).wrapped()
        }

        func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
            items.count
        }

        func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
            let item = items[indexPath.item]
            return {
                let cell = PillTagCell(title: item.label, allowClose: item.allowClose)
                cell.onTapClose = { [weak self] in
                    self?.onTap?(item)
                }
                return cell
            }
        }

        func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
            ASSizeRangeUnconstrained
        }
    }

    class PillTagCell: ASCellNode {

        var backgroundNode: ASControlNode
        var closeIndicator: ASImageNode
        var titleNode: ASTextNode2
        var onTapClose: (() -> Void)?
        var allowClose: Bool

        init(title: String, allowClose: Bool = true) {
            self.allowClose = allowClose
            backgroundNode = .init()
            backgroundNode.backgroundColor = AVSColor.selectSheetFill.color
            closeIndicator = .init(image: Asset.Images.Concierge.icCloseSm.image)
            closeIndicator.tintColor = AVSColor.primaryIcon.color
//            titleNode = .init(with: title.attributed(.body, .accentForeground))
            titleNode = .init(with: title.attributed(.body, .colorMilitaryGreen))

            super.init()
            backgroundNode.addTarget(self, action: #selector(tappedClose), forControlEvents: .touchUpInside)
            automaticallyManagesSubnodes = true
        }

        @objc private func tappedClose() {
            onTapClose?()
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            [
                titleNode,
                allowClose ? closeIndicator : nil
            ].hStacked(spacing: 20)
             .margin(horizontal: 10, vertical: 5)
             .background(backgroundNode)
             .margin(10)
        }

        override func layout() {
            super.layout()
            backgroundNode.cornerRadius = backgroundNode.frame.height / 2
        }
    }

    class Header: ASCellNode {
        enum LayoutType {
            case spaceBetween
            case avatarFlexBasis
        }

        let titleNode = ASTextNode2()
        let subjectNode = ASTextNode2()
        let avatarPlaceholder = ASDisplayNode()
        let line = ASDisplayNode.divider()
        let layoutType: LayoutType

        init(title: String?, subject: String?, layoutType: LayoutType = .avatarFlexBasis) {
            self.layoutType = layoutType
            titleNode.attributedText = title?.uppercased().attributed(.noteMedium, .secondaryLabel)
            subjectNode.attributedText = subject?.uppercased().attributed(.noteMedium, .secondaryLabel)
            super.init()
            automaticallyManagesSubnodes = true
            backgroundColor = AVSColor.secondaryBackground.color
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {

            switch layoutType {
            case .avatarFlexBasis:
                avatarPlaceholder.style.width = ASDimensionMakeWithPoints(RequestCell.avatarSize)
                let userNameWithMargin = titleNode.margin(left: -RequestCell.avatarSize)

                userNameWithMargin.style.flexBasis = ASDimensionMakeWithPoints(20)
                userNameWithMargin.style.flexGrow = 1
                subjectNode.style.flexBasis = ASDimensionMakeWithPoints(20)
                subjectNode.style.flexGrow = 1

                return [
                    [
                        avatarPlaceholder,
                        userNameWithMargin,
                        subjectNode
                    ].hStacked().margin(horizontal: 20).margin(vertical: 10),
                    line
                ].vStacked()

            case .spaceBetween:
                return [
                    [
                        titleNode.filling(),
                        subjectNode
                    ].hStacked().justified(.spaceBetween).margin(horizontal: 20).margin(vertical: 10),
                    line
                ].vStacked()
            }

        }
    }

    class AvatarNameNode: ASDisplayNode {
        private var avatarNode: GenericNodes.AvatarNode
        private var userNameNode = ASTextNode2()
        private var size: CGFloat

        init(size: CGFloat, url: URL?, names: [String?]) {
            self.size = size
            userNameNode.attributedText = names.compactMap { $0 }.joined(separator: " ").attributed(.body, .primaryLabel)
            avatarNode = .init(size: size, shadow: false, nameFallback: .initials(names))
            avatarNode.url = url
            super.init()
            automaticallyManagesSubnodes = true
        }

        init(size: CGFloat,
             image: UIImage?,
             imageBackground: UIColor = AVSColor.accentBackground.color,
             imageTint: UIColor? = nil,
             text: String?) {

            self.size = size
            avatarNode = .init(size: size, shadow: false, border: false, nameFallback: .icon(image, backgroundColor: imageBackground), tintColor: imageTint)
            userNameNode.attributedText = text?.attributed(.body, .primaryLabel)
            super.init()
            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            [
                avatarNode.square(size).margin(right: 15),
                userNameNode.growing(),
            ].hStacked()
        }
    }

    class RequestCell: ASCellNode {
        typealias AvatarNode = GenericNodes.AvatarNode
        static var avatarSize: CGFloat = 36

        var titleNode = ASTextNode2()
        let subjectNode = ASTextNode2()
        var avatarNode: AvatarNode
        var rightIcon: ASImageNode?
        var line = ASDisplayNode.divider()

        // set it to true for the title to push the subject to the far right of the cell
        var titlePriority = false {
            didSet { setSubjectAlignment() }
        }

        var hidesAvatar = false

        func setSubjectAlignment() {
            subjectNode.attributedText = subjectNode.attributedText?.string.attributed(.note, .secondaryLabel, alignment: titlePriority ? .right : .left)
        }

        init(title: String?, subject: String?, avatarNode: AvatarNode, rightIcon: UIImage? = nil) {
            titleNode.attributedText = title?.attributed(.body, .primaryLabel)
            subjectNode.attributedText = subject?.attributed(.note, .secondaryLabel)
            self.avatarNode = avatarNode

            super.init()

            backgroundColor = AVSColor.secondaryBackground.color
            automaticallyManagesSubnodes = true
        }

        static func defaultAvatar(nameFallback: AvatarNode.NameFallback? = nil, border: Bool = false, tintColor: UIColor? = nil, url: URL? = nil) -> AvatarNode {
            let avatar = AvatarNode(size: RequestCell.avatarSize, shadow: false, border: border, nameFallback: nameFallback, tintColor: tintColor)
            avatar.url = url
            return avatar
        }

        convenience init(title: String?, subject: String?, imageUrl: URL?, titlePriority: Bool = true) {
            self.init(title: title,
                      subject: subject,
                      avatarNode: Self.defaultAvatar(nameFallback: .icon(nil), url: imageUrl))
            self.titlePriority = titlePriority
            setSubjectAlignment()
        }

        convenience init(request: UserRequest) {
            self.init(title: request.fullName,
                      subject: request.name,
                      avatarNode: Self.defaultAvatar(nameFallback: .initials([request.firstName, request.lastName]), url: request.userPictureURL))
        }

        convenience init(order: OrderData) {
            switch AppState.userType {
            case .concierge:
                self.init(title: order.authorName,
                          subject: order.orderItems.map(\.itemWithQuantityDescription).joined(separator: ", "),
                          avatarNode: Self.defaultAvatar(nameFallback: .initials(order.authorName?.components(separatedBy: " ") ?? []), url: order.userPictureURL))
            case .resident:
                var time = ""
                if let orderTime = order.createdAt {
                    time = "\n\(orderTime.avsDateShortMonthYearString())"
                }

                self.init(title: order.orderItems.filter { $0.itemType != .discountCode }.map(\.itemWithQuantityDescription).joined(separator: ", "),
                          subject: "Order \(order.orderID)\(time)",
                          avatarNode: Self.defaultAvatar(nameFallback: .icon(Asset.Images.icCartSmall.image.withRenderingMode(.alwaysTemplate)),
//                                                         tintColor: AVSColor.accentForeground.color))
                                                         tintColor: AVSColor.colorMilitaryGreen.color))
                titlePriority = true
                setSubjectAlignment()
            }
        }

        convenience init(orderResponseItem: OrderResponseItem) {
            self.init(title: orderResponseItem.nameWithQuantity,
                      subject: orderResponseItem.totalPriceWithoutTax.currencyDollarClean,
                      avatarNode: Self.defaultAvatar(nameFallback: .icon(nil), url: orderResponseItem.imageUrl))
            titlePriority = true
            setSubjectAlignment()
        }

        convenience init(user: User) {
            self.init(title: user.fullName,
                      subject: user.apartmentNo,
                      avatarNode: Self.defaultAvatar(nameFallback: .initials([user.firstName, user.lastName]), url: user.userPictureURL))
        }

        convenience init(contact: User) {
            self.init(title: contact.fullName,
                      subject: [contact.email, contact.phoneNumber].compactMap { $0 }.joined(separator: "\n"),
                      avatarNode: Self.defaultAvatar(nameFallback: .initials([contact.firstName, contact.lastName]), url: contact.userPictureURL))
            titlePriority = true
            setSubjectAlignment()
        }

        convenience init(visitor: Visitor) {
            self.init(title: visitor.name, subject: visitor.apartment,
                      avatarNode: Self.defaultAvatar(nameFallback: .icon(Asset.Images.Concierge.icUserGroup.image)))
        }

        convenience init(visitor: ResidentGuest) {
            self.init(title: visitor.name,
                      subject: nil,
                      avatarNode: Self.defaultAvatar(nameFallback: .initials(visitor.name?.components(separatedBy: " ") ?? [])),
                      rightIcon: Asset.Images.Concierge.icHorizontalDots.image)
        }

        convenience init(package: Package) {
            self.init(title: package.name, subject: package.apartment, avatarNode: Self.defaultAvatar(url: package.image))
        }

        static func residentReservationListItem(reservation item: AmenityReservation) -> RequestCell {
            let time = item.startTime?.avsTimestampString() ?? item.startDate?.avsDateString()
            let status = item.status?.string
            let subtitle = [time, status].compactMap { $0 }.joined(separator: "\n")
            let cell = RequestCell(title: item.amenityName,
                                   subject: subtitle,
                                   avatarNode: Self.defaultAvatar(nameFallback: .icon(Asset.Images.icCalendarSmall.image.withRenderingMode(.alwaysTemplate)),
//                                                                  tintColor: AVSColor.accentForeground.color))
                                                                  tintColor: AVSColor.colorMilitaryGreen.color))

            cell.titlePriority = true
            return cell
        }

        convenience init(reservation: AmenityReservation) {
            let reservationDateText = [reservation.date?.avsDateFormatted ?? reservation.startDate?.avsDateFormatted, reservation.amenityName].compactMap { $0 }.joined(separator: "\n")
            self.init(title: reservation.fullName,
                      subject: reservationDateText,
                      avatarNode: Self.defaultAvatar(nameFallback: .initials([reservation.firstName, reservation.lastName]),
                                                     url: reservation.userPictureURL))
        }

        convenience init(incidentReport: IncidentReport) {
            let data = [incidentReport.createdAt?.avsTimeAgo, incidentReport.shift].compactMap { $0 }.joined(separator: "\n")
            self.init(title: incidentReport.subject, subject: data, avatarNode: Self.defaultAvatar(nameFallback: .icon(Asset.Images.Concierge.icUserGroup.image)))
        }

        convenience init(conciergeSubscription: ConciergeSubscription, user: User?) {
            self.init(title: user?.fullName, subject: conciergeSubscription.package.name, avatarNode: Self.defaultAvatar(nameFallback: .icon(Asset.Images.Concierge.icUserGroup.image), url: user?.userPictureURL))
        }

        convenience init(activityLog: ActivityLog) {
            let data = [activityLog.createdAt?.avsTimeAgo, activityLog.shift].compactMap { $0 }.joined(separator: "\n")
            self.init(title: activityLog.subject, subject: data, avatarNode: Self.defaultAvatar(nameFallback: .icon(Asset.Images.Concierge.icUserGroup.image)))
        }

        convenience init(conciergeFAQ: ConciergeFAQ) {
            self.init(title: conciergeFAQ.name, subject: conciergeFAQ.createdAt?.avsTimeAgo, avatarNode: Self.defaultAvatar(nameFallback: .icon(Asset.Images.Concierge.icUserGroup.image)))
        }

        convenience init(conciergeNotification: ConciergeNotification) {
            let notification = conciergeNotification.notification

            let data = [notification.notificationType.string,
                        notification.sent ? "Sent" : "Not Sent",
                        notification.createdAt?.avsTimeAgo].compactMap { $0 }.joined(separator: "\n")

            self.init(title: notification.name,
                      subject: data,
                      avatarNode: Self.defaultAvatar(nameFallback: .icon(Asset.Images.Concierge.icPaperPlane.image.withRenderingMode(.alwaysTemplate)),
//                                                     tintColor: AVSColor.accentForeground.color,
                                                     tintColor: AVSColor.colorMilitaryGreen.color,
                                                     url: notification.mediaFile.first { $0.type == .image }?.mediaURL))
        }

        convenience init(bellNotification: BellNotification) {

            let icon: UIImage?

            switch bellNotification.itemType {
            case .request:
                icon = Asset.Images.Concierge.icNotificationRequest.image
            case .order:
                icon = Asset.Images.icCartSmall2.image
            case .notification:
                icon = Asset.Images.Concierge.icNotificationRequest.image
            default:
                icon = nil
            }

            self.init(title: nil,
                      subject: bellNotification.createdAt?.avsTimeString(),
                      avatarNode: Self.defaultAvatar(nameFallback: .icon(icon?.withRenderingMode(.alwaysTemplate)),
//                                                     tintColor: AVSColor.accentForeground.color))
                                                     tintColor: AVSColor.colorMilitaryGreen.color))
            titleNode.attributedText = bellNotification.data?.attributed(bellNotification.read ? .body : .bodyHeavy, .primaryLabel)
            titlePriority = true
        }

        convenience init(categoryUserRequest: CategoryUserRequest) {
            let image = categoryUserRequest.category?.icon ?? Asset.Images.Concierge.icPackageSm.image
            let color = categoryUserRequest.request.status?.color ?? UserRequest.Status.pending.color

            self.init(title: [categoryUserRequest.request.name, categoryUserRequest.request.body].compactMap { $0 }.joined(separator: "\n"),
                      subject: categoryUserRequest.request.dueDate?.avsTimestampStringShort,
                      avatarNode: Self.defaultAvatar(nameFallback: .icon(image.withRenderingMode(.alwaysTemplate), backgroundColor: color),
//                                                     tintColor: AVSColor.accentForeground.color))
                                                     tintColor: AVSColor.colorMilitaryGreen.color))
            titlePriority = true
        }

        convenience init(categoryService: CategoryService) {
            let image = categoryService.category?.icon ?? Asset.Images.Concierge.icPackageSm.image
            let color = AVSColor.accentBackground.color

            self.init(title: categoryService.service.categoryName,
                      subject: categoryService.service.serviceName,
                      avatarNode: Self.defaultAvatar(nameFallback: .icon(image.withRenderingMode(.alwaysTemplate), backgroundColor: color),
                                                     tintColor: AVSColor.colorMilitaryGreen.color))
//                                                   tintColor: AVSColor.accentForeground.color))
            titlePriority = true
            setSubjectAlignment()
        }

        convenience init(categoryServiceProvider: CategoryService) {
            let image = categoryServiceProvider.category?.icon ?? Asset.Images.Concierge.icPackageSm.image
            let color = AVSColor.accentBackground.color

            let subject = [categoryServiceProvider.service.serviceName, categoryServiceProvider.service.phoneNumber].joined(separator: "\n")

            self.init(title: categoryServiceProvider.service.fullName,
                      subject: subject,
                      avatarNode: Self.defaultAvatar(nameFallback: .icon(image.withRenderingMode(.alwaysTemplate), backgroundColor: color),
//                                                     tintColor: AVSColor.accentForeground.color))
                                                     tintColor: AVSColor.colorMilitaryGreen.color))
            titlePriority = true
            setSubjectAlignment()
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            avatarNode.style.preferredSize = CGSize(width: RequestCell.avatarSize, height: RequestCell.avatarSize)

            var titleSubjectStack = [
                titleNode,
                subjectNode
            ].hStacked().spaced(15)

            if titlePriority {
                titleNode.style.flexGrow = 1
                titleNode.style.flexShrink = 1
                titleSubjectStack = titleSubjectStack.justified(.spaceBetween)
            } else {
                titleNode.style.flexBasis = ASDimensionMakeWithPoints(20)
                subjectNode.style.flexBasis = ASDimensionMakeWithPoints(20)
                titleNode.style.flexGrow = 1
                subjectNode.style.flexGrow = 1
            }

            return [
                [
                    hidesAvatar ? nil : avatarNode.margin(right: 15),
                    titleSubjectStack.filling(),
                    rightIcon
                ].hStacked().margin(15),
                line
            ].vStacked()
        }
    }

    class SummaryNode: ASDisplayNode {
        struct Config {
            struct Item {
                let title: String?
                var subtitle: String? = nil
                var attributedSubtitle: NSAttributedString? = nil
                var subtitleColor: UIColor? = nil
                var customNode: ASDisplayNode? = nil
            }

            let matrix: [[Item]]
            var skipBlankSubtitles = false

            static func matrix(_ matrix: [[Item]], skipBlankSubtitles: Bool = false) -> Config {
                Config(matrix: matrix, skipBlankSubtitles: skipBlankSubtitles)
            }
        }

        var nodeMatrix: [[ASLayoutElement]]

        init(config: Config) {
            nodeMatrix = config.matrix.map { items in
                items.filter { item in
                    if config.skipBlankSubtitles {
                        return item.subtitle != nil || item.attributedSubtitle != nil
                    }

                    return true
                }.map { item in
                    if let customNode = item.customNode {
                        return TitleSubtitle(title: item.title, subtitleNode: customNode)
                    } else if let attributedSubtitle = item.attributedSubtitle {
                        return TitleSubtitle(title: item.title, subtitleNode: ASTextNode2(with: attributedSubtitle))
                    } else {
                        return TitleSubtitle(title: item.title, subtitle: item.subtitle, subtitleColor: item.subtitleColor)
                    }
                }
            }.filter { $0.count > 0 }

            super.init()
            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            nodeMatrix.map { $0.stackEqualSizes().hStacked().aligned(.start) }.vStacked().spaced(30)
        }
    }

    class SummaryCellNode: ASCellNode {
        var summaryNode: SummaryNode
        var actionButton: ASButtonNode?
        var didTapAction: (() -> Void)?

        init(config: SummaryNode.Config, actionTitle: String? = nil) {
            summaryNode = SummaryNode(config: config)

            super.init()

            if let title = actionTitle {
                let actionButton = ASButtonNode.greenPillButton(title: title)
                actionButton.addTarget(self, action: #selector(tappedActionButton), forControlEvents: .touchUpInside)
                self.actionButton = actionButton
            }

            backgroundColor = AVSColor.secondaryBackground.color
            automaticallyManagesSubnodes = true
        }

        convenience init(request: UserRequest) {
            let requestItems: [[SummaryNode.Config.Item]] = [
                [.init(title: "SUBJECT", subtitle: request.name)],
                [
                    .init(title: "TENANT NAME", subtitle: request.fullName),
                    .init(title: "APT #", subtitle: request.apartmentNo)
                ],
                [.init(title: "REQUESTED DATE", subtitle: request.createdAt)],
                [.init(title: "STATUS", subtitle: request.status?.displayString, subtitleColor: request.status?.conciergeStatusColor)]
            ]

            var serviceItems = [[SummaryNode.Config.Item]]()
            if !String.isBlank(request.serviceProviderName) {
                serviceItems = [
                    [
                        .init(title: "SERVICE PROVIDER",
                              customNode: ConciergePagerListNodes.AvatarNameNode(size: 36,
                                                                                 url: request.serviceProviderImageURL,
                                                                                 names: request.serviceProviderNameSplit)),
                        .init(title: "FULFILLMENT TIME",
                              subtitle: request.serviceFulfillmentTime?.avsTimestampString())]
                ]
            }

            self.init(config: .matrix(requestItems + serviceItems))
        }

        convenience init(categoryUserRequest: CategoryUserRequest) {
            let image = categoryUserRequest.category?.icon ?? Asset.Images.Concierge.icPackageSm.image
            let color = categoryUserRequest.request.status?.color ?? UserRequest.Status.pending.color
            let request = categoryUserRequest.request

            let requestItems: [[SummaryNode.Config.Item]] = [
                [.init(title: nil,
                       customNode: AvatarNameNode(size: 36,
                                                  image: image.withRenderingMode(.alwaysTemplate),
                                                  imageBackground: color,
//                                                  imageTint: AVSColor.accentForeground.color,
                                                  imageTint: AVSColor.colorMilitaryGreen.color,
                                                  text: request.name))],

                [.init(title: "COMMENTS", subtitle: request.body)],

                [
                    .init(title: "STATUS", subtitle: request.status?.displayString),
                    .init(title: "DUE DATE", subtitle: request.dueDate?.avsTimestampStringShort)
                ]
            ]

            var serviceItems = [[SummaryNode.Config.Item]]()
            if !String.isBlank(request.serviceProviderName) {
                serviceItems = [
                    [
                        .init(title: "SERVICE PROVIDER",
                              customNode: ConciergePagerListNodes.AvatarNameNode(size: 36,
                                                                                 url: request.serviceProviderImageURL,
                                                                                 names: request.serviceProviderNameSplit)),
                        .init(title: "FULFILLMENT TIME",
                              subtitle: request.serviceFulfillmentTime?.avsTimestampString())
                    ]
                ]
            }

            self.init(config: .matrix(requestItems + serviceItems))
        }

        convenience init(reservation: AmenityReservation) {
            if reservation.startTime != nil {
                self.init(config: .matrix(
                        [
                            [
                                .init(title: "TENANT NAME", subtitle: reservation.fullName),
                                .init(title: "APT #", subtitle: nil)
                            ],

                            [.init(title: "RESERVATION DATE", subtitle: reservation.startTime?.avsDateString())],

                            [
                                .init(title: "TIME FROM", subtitle: reservation.startTime?.avsTimeString()),
                                .init(title: "TIME TO", subtitle: reservation.endTime?.avsTimeString())
                            ],

                            [.init(title: "STATUS", subtitle: reservation.status?.string, subtitleColor: reservation.status?.conciergeStatusColor)]
                        ], skipBlankSubtitles: true))
            } else {
                self.init(config: .matrix(
                        [
                            [
                                .init(title: "TENANT NAME", subtitle: reservation.fullName),
                                .init(title: "APT #", subtitle: nil)
                            ],

                            [
                                .init(title: "GUEST NAME", subtitle: reservation.guestName),
                                .init(title: "GUEST PHONE", subtitle: reservation.guestPhone),
                            ],

                            [
                                .init(title: "DATE FROM", subtitle: reservation.startDate?.avsDateString()),
                                .init(title: "DATE TO", subtitle: reservation.endDate?.avsDateString())
                            ],

                            [.init(title: "STATUS", subtitle: reservation.status?.string, subtitleColor: reservation.status?.conciergeStatusColor)]
                        ], skipBlankSubtitles: true))
            }
        }

        convenience init(order: OrderData) {
            self.init(config: .matrix(
                    [
                        [.init(title: "ORDER ITEM", subtitle: order.orderItemsDescription)],
                        [
                            .init(title: "TENANT NAME", subtitle: order.authorName),
                            .init(title: "APT #", subtitle: nil)
                        ],
                        [
                            .init(title: "ORDER AMOUNT", subtitle: order.amountWithTax.currencyDollarClean),
                            .init(title: "ORDER DATE", subtitle: order.createdAt?.avsTimestampString())
                        ],
                        [.init(title: "STATUS", subtitle: order.orderType.displayString, subtitleColor: order.orderType.conciergeOrderTypeColor)]
                    ], skipBlankSubtitles: true), actionTitle: order.orderType.conciergeAction)
        }

        convenience init(guest: ResidentGuest) {
            self.init(config: .matrix(
                    [
                        [.init(title: "GUEST NAME", subtitle: guest.name)],
                        [
                            .init(title: "PHONE NUMBER", subtitle: guest.phoneNumber),
                            .init(title: "APT #", subtitle: guest.apartment),
                        ],
                        [.init(title: "FREQUENCY", subtitle: guest.frequency?.rawValue)],
                    ]), actionTitle: "Log Visit")
        }

        convenience init(visitor: Visitor) {
            self.init(config: .matrix(
                    [
                        [
                            .init(title: "VISITOR NAME", subtitle: visitor.name),
                            .init(title: "APT #", subtitle: visitor.apartment),
                        ],
                        [.init(title: "VISIT DATE", subtitle: visitor.created?.avsTimestampString()), ],
                        [.init(title: "REASONS", subtitle: visitor.reason)],
                    ]))
        }

        convenience init(activityLog: ActivityLog, activityLogDescription: String?) {
            self.init(config: .matrix(
                    [
                        [.init(title: "SHIFT", subtitle: activityLog.shift), .init(title: "DATE", subtitle: activityLog.createdAt?.avsTimeAgo)],
                        [.init(title: "DESCRIPTION", subtitle: activityLogDescription)],
                    ]))
        }

        convenience init(faq: ConciergeFAQ, faqDescription: NSAttributedString?) {
            self.init(config: .matrix(
                    [
                        [.init(title: "DATE", subtitle: faq.createdAt?.avsTimeAgo)],
                        [.init(title: "TITLE", subtitle: faq.name)],
                        [.init(title: "DESCRIPTION", attributedSubtitle: faqDescription)],
                    ]))
        }

        convenience init(subscription: ConciergeSubscription, user: User?) {
            self.init(config: .matrix(
                    [
                        [.init(title: "RESIDENT", subtitle: user?.fullName), .init(title: "PACKAGE", subtitle: subscription.package.name)],
                        [.init(title: "AMOUNT", subtitle: "$\(subscription.amount)"), .init(title: "STATUS", subtitle: subscription.status)],
                        [.init(title: "PERIOD START", subtitle: subscription.periodStart?.avsTimeAgo), .init(title: "PERIOD END", subtitle: subscription.periodEnd?.avsTimeAgo)],
                        [.init(title: "CANCEL AT PERIOD END", subtitle: subscription.cancel ? "Yes" : "No")],
                    ]))
        }

        convenience init(package: Package) {
            self.init(config: .matrix(
                    [
                        [
                            .init(title: "TO", subtitle: package.fullName),
                            .init(title: "FROM", subtitle: package.from),
                        ],
                        [.init(title: "APT #", subtitle: package.apartment)],
                        [.init(title: "STATUS", subtitle: package.status.displayString, subtitleColor: package.status.color)],
                    ]))
        }

        @objc func tappedActionButton() {
            didTapAction?()
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            [summaryNode, actionButton].vStacked().spaced(30).margin(top: 30, left: 20, bottom: 40, right: 20)
        }
    }

    class TitleSubtitle: ASDisplayNode {
        private var titleNode: ASTextNode2?
        private var subtitleNode: ASDisplayNode?

        init(title: String?, subtitleNode: ASDisplayNode?) {
            if let title = title {
                titleNode = .init(with: title.attributed(.note, .secondaryLabel))
            }
            self.subtitleNode = subtitleNode
            super.init()
            automaticallyManagesSubnodes = true
        }

        convenience init(title: String?, subtitle: String?, subtitleColor: UIColor? = nil) {
            let subtitleColor = subtitleColor ?? AVSColor.primaryLabel.color
            let subtitleNode = ASTextNode2(with: subtitle?.attributed(AVSFont.body.font, subtitleColor))
            self.init(title: title, subtitleNode: subtitleNode)
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            [titleNode, subtitleNode].vStacked().spaced(3)
        }
    }
}

fileprivate extension OrderResponseItem {
    var itemWithQuantityDescription: String {
        if itemType == .discountCode {
            return name
        }
        return "\(quantity)x \(name)"
    }
}

fileprivate extension OrderData {
    var orderItemsDescription: String {
        orderItems.map(\.itemWithQuantityDescription).joined(separator: ", ")
    }
}

extension UserRequest.Status {
    var conciergeStatusColor: UIColor {
        switch self {
        case .done:
            return AVSColor.colorGreen.color
        case .accepted:
            return AVSColor.colorBlue.color
        case .pending:
            return AVSColor.accentBackground.color
        }
    }

    var conciergeAction: String? {
        switch self {
        case .pending:
            return "Accept"
        case .accepted:
            return "Done"
        case .done:
            return nil
        }
    }

    var nextStatus: UserRequest.Status? {
        switch self {
        case .done:
            return nil
        case .accepted:
            return .done
        case .pending:
            return .accepted
        }
    }
}

extension OrderData.OrderType {
    var conciergeOrderTypeColor: UIColor {
        switch self {
        case .done:
            return AVSColor.colorGreen.color
        case .pending:
            return AVSColor.accentBackground.color
        }
    }

    var conciergeAction: String? {
        switch self {
        case .pending:
            return "Mark Completed"
        case .done:
            return nil
        }
    }

    var nextOrderType: OrderData.OrderType? {
        switch self {
        case .done:
            return nil
        case .pending:
            return .done
        }
    }
}
