//
// Created by Andrei Stoicescu on 08.03.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//


import Foundation
import UIKit
import SwiftyUserDefaults
import AsyncDisplayKit
import SVProgressHUD
import PromiseKit
import Combine
import SwiftUI

class RequestServiceProviderListViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

    lazy var emptyNode = EmptyNode(emptyNodeType: .serviceProvider, showAction: false)
    lazy var refreshControl = UIRefreshControl()

    var servicesProvider: ConciergeServiceListProvider
    var request: UserRequest
    var buildingId: String
    var serviceId: String

    var collectionNode: ASCollectionNode

    var didAssignRequest: ((_ to: String) -> Void)?

    init(buildingId: String, serviceId: String, request: UserRequest) {
        self.request = request
        self.buildingId = buildingId
        self.serviceId = serviceId
        servicesProvider = .init(listType: .serviceList(buildingId: buildingId, serviceId: serviceId))
        collectionNode = .avsCollection()
        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        replaceSystemBackButton()

        navigationItem.title = "Assign Service Provider"

        showEmptyScreen(false)
        collectionNode.view.backgroundView = emptyNode.view
        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)
        collectionNode.view.addSubview(refreshControl)

        SVProgressHUD.show()
        refreshItems()
    }

    @objc func refreshItems() {
        servicesProvider.reload().ensure { [weak self] in
            guard let self = self else { return }
            SVProgressHUD.dismiss()
            self.refreshControl.endRefreshing()
            self.collectionNode.reloadData()
            self.showEmptyScreen(self.servicesProvider.categoryServices.count == 0)
        }.catch { error in
            SVProgressHUD.showError(withStatus: error.localizedDescription)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let service = servicesProvider.categoryServices[indexPath.item]
        return {
            ConciergePagerListNodes.RequestCell(categoryServiceProvider: service)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        servicesProvider.categoryServices.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        let service = servicesProvider.categoryServices[indexPath.item].service

        let view = RequestAssignServiceProviderView(requestId: request.userRequestID, providerUserId: service.userId, requestTitle: service.fullName) { [weak self] in
            guard let self = self else { return }
            self.didAssignRequest?(service.fullName)
        }

        navigationController?.pushViewController(UIHostingController(rootView: view), animated: true)
    }

    func showEmptyScreen(_ show: Bool) {
        emptyNode.isHidden = !show
    }
}