//
// Created by Andrei Stoicescu on 08/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import Combine

class ConciergeUserRequestDetailViewController: ASDKViewController<ASDisplayNode>, ASCollectionDataSource, ASCollectionDelegate {

    enum Items: Int, CaseIterable {
        case info
        case accept
        case providers
    }

    var collectionNode: ASCollectionNode

    var request: UserRequest

    var editingPublisher: PassthroughSubject<(UserRequest, to: UserRequest.Status), Never>

    init(request: UserRequest, editingPublisher: PassthroughSubject<(UserRequest, to: UserRequest.Status), Never>) {
        self.editingPublisher = editingPublisher
        self.request = request
        collectionNode = .avsCollection()
        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()
        navigationItem.title = "Request Details"
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        Items.allCases.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        guard let item = Items(rawValue: indexPath.item) else { return { ASCellNode() } }
        let request = self.request
        switch item {
        case .info:
            return { ConciergePagerListNodes.SummaryCellNode(request: request) }
        case .accept:
            return {
                if request.status?.conciergeAction != nil {
                    let cell = ASCellNode.withNode(ASButtonNode.primaryButton(title: request.status?.conciergeAction ?? ""), margin: UIEdgeInsets(top: 0, left: 15, bottom: 15, right: 15))
                    cell.backgroundColor = AVSColor.secondaryBackground.color
                    return cell
                }

                return ASCellNode()
            }
        case .providers:
            if request.status == .pending, !String.isBlank(request.serviceID) {
                return {
                    let cell = ASCellNode.withNode(ASButtonNode.primaryButton(title: "Assign Service Provider"), margin: UIEdgeInsets(top: 0, left: 15, bottom: 15, right: 15))
                    cell.backgroundColor = AVSColor.secondaryBackground.color
                    return cell
                }
            } else {
                return { ASCellNode() }
            }
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        guard let item = Items(rawValue: indexPath.item) else { return }

        switch item {
        case .info:
            ()
        case .accept:
            guard let status = request.status,
                  let nextStatus = status.nextStatus else { return }

            switch nextStatus {
            case .accepted, .done:
                SVProgressHUD.show()
                ConciergeService().updateUserRequest(requestID: request.userRequestID, status: nextStatus).done { [weak self] in
                    SVProgressHUD.dismiss()
                    self?.requestDidChange(status: nextStatus)
                }.catch { error in
                    SVProgressHUD.showError(withStatus: error.localizedDescription)
                    Log.thisError(error)
                }
            default:
                ()
            }
        case .providers:
            guard let serviceID = request.serviceID, request.status == .pending else { return }

            BuildingDetails.getBuildingID().done { [weak self] buildingId in
                guard let self = self else { return }
                let vc = RequestServiceProviderListViewController(buildingId: buildingId, serviceId: serviceID, request: self.request)
                vc.didAssignRequest = { [weak self] assignedTo in
                    guard let self = self else { return }
                    SVProgressHUD.showInfo(withStatus: "The request was assigned to \(assignedTo)")
                    // A request can only be assigned and accepted in this step
                    self.requestDidChange(status: .accepted)
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }.cauterize()
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }

    func requestDidChange(status: UserRequest.Status) {
        editingPublisher.send((request, to: status))
    }
}

