//
// Created by Andrei Stoicescu on 11/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import PromiseKit
import SwiftyUserDefaults

class ConciergeThingsToKnowListViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

    class ArticleCell: ASCellNode {
        var titleNode: ASTextNode2
        var bodyNode: ASTextNode2
        var thumbnailNode: ASNetworkImageNode

        var htmlBody: String?
        var bottomNode: ASDisplayNode?
        var thumbAspectRatio: CGFloat?
        
        var collectionNode: ASCollectionNode!
        lazy var navigationNode = UserNotificationsNodes.Navigation()

        init(title: String?, body: String?, url: URL?) {
            titleNode = ASTextNode2(with: title?.attributed(.bodyTitleHeavy, .primaryLabel))
            bodyNode = ASTextNode2(with: body?.attributed(.body, .primaryLabel))
            thumbnailNode = ASNetworkImageNode()
            thumbnailNode.url = url
            thumbnailNode.cornerRadius = 4

            collectionNode = .avsCollection()

            let mainNode = ASDisplayNode()
            mainNode.automaticallyManagesSubnodes = true

            super.init()
            
            mainNode.layoutSpecBlock = { [weak self] node, range in
                guard let self = self else { return ASDisplayNode().wrapped() }
                return [
                    self.navigationNode,
                    self.collectionNode.growing()
                ].vStacked()
            }
            
            automaticallyManagesSubnodes = true
        }

        init(title: String?, htmlBody: String?, url: URL?) {
            self.htmlBody = htmlBody
            titleNode = ASTextNode2(with: title?.attributed(.bodyTitleHeavy, .primaryLabel))
            bodyNode = ASTextNode2(with: htmlBody?.html)
            thumbnailNode = ASNetworkImageNode()
            thumbnailNode.url = url
            thumbnailNode.cornerRadius = 4

            super.init()
            automaticallyManagesSubnodes = true
        }

        override func asyncTraitCollectionDidChange(withPreviousTraitCollection previousTraitCollection: ASPrimitiveTraitCollection) {
            super.asyncTraitCollectionDidChange(withPreviousTraitCollection: previousTraitCollection)
            if asyncTraitCollection().userInterfaceStyle != previousTraitCollection.userInterfaceStyle {
                if let body = htmlBody {
                    bodyNode.attributedText = body.html
                }
            }
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            [
                [
                    thumbnailNode.ratio(thumbAspectRatio ?? 10 / 16),
                    [titleNode, bodyNode].vStacked().spaced(10),
                ].vStacked().spaced(20),
                bottomNode
            ].vStacked().spaced(30)
             .cardWrapped().margin(bottom: 10)
        }
    }

    class HalfCell: ArticleCell {
        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            [
                thumbnailNode.ratio(10 / 16),
                titleNode
            ].vStacked().spaced(20).cardWrapped().margin(10)
        }
    }

    class HorizontalModeCell: ArticleCell {
        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            let thumbWidth = constrainedSize.max.width * 0.35
            let thumbHeight = thumbWidth * (thumbAspectRatio ?? 10 / 13)

            return [
                thumbnailNode.size(CGSize(width: thumbWidth, height: thumbHeight)),
                [titleNode, bodyNode.filling(), bottomNode].vStacked().spaced(10).filling().maxHeight(.points(thumbHeight))
            ].hStacked().spaced(20).aligned(.stretch).cardWrapped().margin(top: 0, left: 15, bottom: 15, right: 15)
        }
    }

//    var collectionNode: ASCollectionNode {
//        node as! ASCollectionNode
//    }

    var articles: [Article] = []
    lazy var refreshControl = UIRefreshControl()
    
    var collectionNode: ASCollectionNode
    lazy var navigationNode = UserNotificationsNodes.Navigation()

    override init() {
//        let flow = TopAlignedCollectionViewFlowLayout()
//        flow.minimumInteritemSpacing = 0
//        flow.minimumLineSpacing = 0
//        let collectionNode = ASCollectionNode.avsCollection(flowLayout: flow)
        collectionNode = .avsCollection()
        collectionNode.backgroundColor = AVSColor.secondaryBackground.color
        
        let mainNode = ASDisplayNode()
        mainNode.automaticallyManagesSubnodes = true
//        super.init(node: collectionNode)
        super.init(node: mainNode)
        
        mainNode.layoutSpecBlock = { [weak self] node, range in
            guard let self = self else { return ASDisplayNode().wrapped() }
            return [
                self.navigationNode,
                self.collectionNode.growing()
            ].vStacked()
        }
        
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tabBar = BottonTabBarView(frame: CGRect(x: 0, y: UIScreen.main.bounds.height-83, width: UIScreen.main.bounds.width, height: 83))
        self.view.addSubview(tabBar)
        let lateralMenu = LateralMenuView(frame: CGRect(x: 80, y: 0, width: 347, height: UIScreen.main.bounds.height))
        let myView = HeaderTabBarView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 190), tabBarVC: self, lm: lateralMenu)
        myView.backButton.addTarget(self, action: #selector(tappedButtonActivator), for: .touchUpInside)
        myView.backButton.isHidden = false
        lateralMenu.isHidden = true
        lateralMenu.header = myView
        lateralMenu.rootView = self
        self.view.addSubview(myView)
        self.view.addSubview(lateralMenu)
        
        
        replaceSystemBackButton()
        navigationItem.title = "Things to Know"
        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)
        collectionNode.view.addSubview(refreshControl)

        if let articles = CodableCache.get([Article].self, key: "things_to_know") {
            self.articles = articles
            collectionNode.reloadData()
        } else {
            SVProgressHUD.show()
        }

        refreshItems()
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        navigationController?.setNavigationBarHidden(true, animated: false)
//    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        navigationController?.setNavigationBarHidden(false, animated: true)
//    }

    @objc func tappedButtonActivator() {
        AppState.switchTo(viewController: MyBuildingViewController())
    }
    
    @objc func refreshItems() {
        let apiService = ResidentService()
        firstly {
            BuildingDetails.getBuildingID()
        }.then { buildingID in
            apiService.getBuildingThingsToKnow(buildingID: buildingID)
        }.done { [weak self] articles in
            guard let self = self else { return }
            if self.articles != articles {
                CodableCache.set(articles, key: "things_to_know")
                self.articles = articles
                self.collectionNode.reloadData()
            }
        }.ensure { [weak self] in
            self?.refreshControl.endRefreshing()
            SVProgressHUD.dismiss()
        }.catch { error in
            SVProgressHUD.showError(withStatus: error.localizedDescription)
            Log.thisError(error)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        articles.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let article = articles[indexPath.item]
        let subtitle = article.teaser?.html?.string

        return {
            switch article.displayMode {
            case .mode1:
                return ArticleCell(title: article.articleTitle, body: subtitle, url: article.articleThumbnailURL)
            case .mode2:
                return HorizontalModeCell(title: article.articleTitle, body: subtitle, url: article.articleThumbnailURL)
            case .mode3:
                return HalfCell(title: article.articleTitle, body: subtitle, url: article.articleThumbnailURL)
            }
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        let article = articles[indexPath.item]
        switch article.displayMode {

        case .mode1, .mode2:
            return ASSizeRange.fullWidth(collectionNode: collectionNode)
        case .mode3:
            return ASSizeRange.halfWidth(collectionNode: collectionNode)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        let article = articles[indexPath.item]

        let vc = ArticleWebViewController(url: article.articleNodeURL)
        vc.closeBlock = { [weak self] in
            self?.dismiss(animated: true)
        }
        let nav = AVSNavigationController(rootViewController: vc)
        present(nav, animated: true)
    }
}
