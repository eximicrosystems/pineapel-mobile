//
// Created by Andrei Stoicescu on 16/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import Combine

class ResidentDetailsViewController: ASDKViewController<ASDisplayNode>, ASCollectionDataSource, ASCollectionDelegate {

    class GuestListHeaderCell: ASCellNode {
        let header: ConciergeGenericNodes.TitleSubtitleNode
        let headerLeftLabel: ASTextNode2
        let headerRightLabel: ASTextNode2
        let line = ASDisplayNode.divider()
        let line2 = ASDisplayNode.divider()
        let backgroundNode = ASDisplayNode()

        override init() {
            header = .init(title: "Resident's Guest List", subtitle: "Record of tenant's guest list")
            headerLeftLabel = .init(with: "GUEST NAME".attributed(.note, .secondaryLabel))
            headerRightLabel = .init(with: "PHONE NUMBER".attributed(.note, .secondaryLabel))
            backgroundNode.backgroundColor = AVSColor.secondaryBackground.color
            super.init()
            self.automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            [
                header.margin(15),
                line,
                [
                    headerLeftLabel,
                    headerRightLabel
                ].stackEqualSizes().hStacked().spaced(15).margin(horizontal: 15).margin(vertical: 10),
                line2
            ].vStacked().background(backgroundNode).margin(top: 10)
        }
    }

    class ResidentGuestCell: ASCellNode {
        let nameLabel: ASTextNode2
        let phoneLabel: ASTextNode2
        let line = ASDisplayNode.divider()

        init(name: String?, phone: String?) {
            nameLabel = .init(with: name?.attributed(.body, .primaryLabel))
            phoneLabel = .init(with: phone?.attributed(.body, .primaryLabel))
            super.init()
            self.backgroundColor = AVSColor.secondaryBackground.color
            self.automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            [
                [nameLabel, phoneLabel].stackEqualSizes().hStacked().spaced(15).margin(15),
                line
            ].vStacked()
        }
    }

    enum Sections: Int, CaseIterable {
        case details
        case guestListHeader
        case guestList
    }

    var collectionNode: ASCollectionNode
    var user: User
    var guestList: ObservablePaginatedDataSource<ResidentGuest>
    private var subscriptions = Set<AnyCancellable>()

    init(user: User) {
        self.user = user
        collectionNode = .avsCollection()

        guestList = ObservablePaginatedDataSource<ResidentGuest> { per, page in
            ConciergeService().getResidentList(userID: user.uid, per: per, page: page)
        }

        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()
        navigationItem.title = "Resident details"
        setupBindings()
        guestList.loadMoreContentIfNeeded()
        loadUser()
    }

    func setupBindings() {
        guestList.$items
                .receive(on: DispatchQueue.main)
                .sink { [weak self] items in
                    self?.collectionNode.reloadSections(.init(integer: Sections.guestList.rawValue))
                }.store(in: &subscriptions)

        guestList.$showLoadingIndicator
                .receive(on: DispatchQueue.main)
                .sink { loading in
                    if loading {
                        SVProgressHUD.show()
                    } else {
                        SVProgressHUD.dismiss()
                    }
                }.store(in: &subscriptions)
    }

    func loadUser() {
        ConciergeService().getUser(userID: user.uid).done { user in
            self.user = user
            self.collectionNode.reloadData()
        }.catch { error in
            Log.thisError(error)
        }
    }

    func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
        Sections.allCases.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        guard let section = Sections(rawValue: section) else { return 0 }
        switch section {
        case .details, .guestListHeader:
            return 1
        case .guestList:
            return guestList.items.count
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        guard let section = Sections(rawValue: indexPath.section) else { return { ASCellNode() } }
        switch section {
        case .details:
            return {
                ConciergePagerListNodes.SummaryCellNode(config: .matrix(
                        [
                            [.init(title: "TENANT NAME", customNode: ConciergePagerListNodes.AvatarNameNode(size: 36, url: self.user.userPictureURL, names: [self.user.firstName, self.user.lastName]))],
                            [.init(title: "CONTACT", subtitle: self.user.phoneNumber ?? "-"), .init(title: "APT #", subtitle: self.user.apartmentNo ?? "-")],
                            [.init(title: "GENDER", subtitle: self.user.gender?.rawValue ?? "-"), .init(title: "JOB", subtitle: self.user.job ?? "-")],
                            [.init(title: "APP VERSION", subtitle: self.user.appVersion)]
                        ]))
            }
        case .guestListHeader:
            return {
                GuestListHeaderCell()
            }
        case .guestList:
            let resident = guestList.items[indexPath.item]
            return {
                ResidentGuestCell(name: resident.name, phone: resident.phoneNumber)
            }
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        guard let section = Sections(rawValue: indexPath.section) else { return }
        if section == .guestList {
            let vc = ResidentGuestDetailsViewController(guest: guestList.items[indexPath.item])
            vc.didLogGuest = { [weak self] in
                guard let self = self else { return }
                self.navigationController?.popToViewController(self, animated: true)
            }
            show(vc, sender: self)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        ASSizeRange.fullWidth(collectionNode: collectionNode)
    }

    func collectionView(_ collectionView: ASCollectionView, willDisplayNodeForItemAt indexPath: IndexPath) {
        if indexPath.section == Sections.guestList.rawValue {
            guestList.loadMoreContentIfNeeded(currentItem: guestList.items[indexPath.item])
        }
    }
}
