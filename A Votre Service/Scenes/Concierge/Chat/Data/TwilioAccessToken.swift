//
// Created by Andrei Stoicescu on 29/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation

struct TwilioAccessToken: Codable {
    let token: String
    let expire: Int

    enum CodingKeys: String, CodingKey {
        case token
        case expire
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)

        self.token = try map.decode(.token)
        self.expire = try map.decodeLossless(.expire)
    }
}