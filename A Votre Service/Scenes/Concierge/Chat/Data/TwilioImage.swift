//
// Created by Andrei Stoicescu on 29/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import Combine
import UIKit
import TwilioChatClient

class TwilioImageManager {
    static var shared = TwilioImageManager()
    var images = [String: TwilioImage]()

    func imageForMessage(_ chatMessage: ConciergeChat.Message) -> TwilioImage {
        if let img = images[chatMessage.id] {
            return img
        } else {
            let twilioImage = TwilioImage(message: chatMessage)
            images[chatMessage.id] = twilioImage
            return twilioImage
        }
    }

    func clearImage(_ image: TwilioImage) {
        images.removeValue(forKey: image.message.id)
    }

    func clearAll() {
        images.removeAll()
    }
}

class TwilioImage: ObservableObject, Identifiable {
    var id: String { message.id }

    @Published var image: UIImage? = nil
    @Published var loading: Bool = false
    @Published var error: Error?

    var twilioHelper = TwilioHelper()

    var message: ConciergeChat.Message

    init(message: ConciergeChat.Message) {
        self.message = message
    }

    func getImage() {
        guard !loading, image == nil else { return }
        loading = true

        twilioHelper.getImage(from: message).done { [weak self] image in
            self?.image = image
        }.catch { [weak self] error in
            Log.thisError(error)
            self?.error = error
        }.finally { [weak self] in
            self?.loading = false
        }
    }

    deinit {
        image = nil
    }
}