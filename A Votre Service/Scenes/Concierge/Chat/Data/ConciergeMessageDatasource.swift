//
// Created by Andrei Stoicescu on 30/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import SwiftUI
import TwilioChatClient
import PromiseKit

struct ConciergeChat {}

extension ConciergeChat {

    enum RegroupMessageOperation {
        case appendMultiple([Message])
        case prepend(Message)
        case replace(Message, previousId: String? = nil)
        case replaceMultiple([Message])
        case remove(Message)
        case removeAll
    }

    struct Typing {
        let twilioUser: User?
        let isTyping: Bool

        static var notTyping: Typing {
            Typing(twilioUser: nil, isTyping: false)
        }
    }

    class MessageDatasource: ObservableObject {

        class MessageStore {
            private (set) var messages = [Message]()
            var conversationId: String
            var perPage: UInt

            init(conversationId: String, perPage: UInt) {
                self.conversationId = conversationId
                self.perPage = perPage
            }

            func indexOfMessage(_ message: Message) -> Int? {
                messages.firstIndex { $0.id == message.id }
            }

            func replaceMessage(_ message: Message) -> Bool {
                guard let idx = indexOfMessage(message) else { return false }
                messages[idx] = message
                if message.cacheable {
                    Log.this("replaceMessage - cache")
                    cacheMessages()
                }
                return true
            }

            func replaceMessage(_ message: Message, at index: Int) {
                messages[index] = message
                if message.cacheable {
                    Log.this("replaceMessageAtIndex - cache")
                    cacheMessages()
                }
            }

            func prependMessage(_ message: Message) {
                messages.insert(message, at: 0)
                if message.cacheable {
                    Log.this("prependMessage - cache")
                    cacheMessages()
                }
            }

            func append(_ messages: [Message]) {
                self.messages.append(contentsOf: messages)
                // Appending messages is only done from loading twilio messages. No caching done in this case
            }

            func replaceMessages(_ messages: [Message], cached: Bool) {
                self.messages = messages
                if !cached {
                    Log.this("replaceMessages - cache")
                    cacheMessages()
                }
            }

            func remove(_ message: Message) -> Bool {
                guard let messageIndex = messages.firstIndex(where: { $0.id == message.id }) else { return false }
                messages.remove(at: messageIndex)
                if message.cacheable {
                    Log.this("removeMessage - cache")
                    cacheMessages()
                }
                return true
            }

            private func cacheMessages() {
                // Only cache the first "page" of cacheable messages
                // Not using filter since it goes over all and we just need to break at perPage

                var messagesToCache = [Message]()
                for message in messages {
                    if messagesToCache.count >= perPage {
                        break
                    }

                    if message.cacheable {
                        messagesToCache.append(message)
                    }
                }

                messagesToCache.cache(.twilioConversation(conversationId))
            }
        }

        var messageStore: MessageStore
        var messages: [Message] { messageStore.messages }

        var active: Bool = false
        var hasNextPage = true
        var twilioChannel: TwilioChannel
        var tchChannel: TCHChannel? {
            didSet {
                messageProvider.tchChannel = tchChannel
                if oldValue == nil && tchChannel != nil {
                    didSetTCHChannel?()
                    processSendingMessages()
                }
            }
        }
        var didSetTCHChannel: (() -> Void)?

        @Published var isLoadingPage = false {
            didSet {
                Log.this("chat is loading page == \(isLoadingPage)")
            }
        }
        @Published var messageGroups = [MessageGroup]()
        @Published var lastReadMessage: Message?
        @Published var typing: Typing = .notTyping
        @Published var twilioClientLoading: Bool = false

        // The logged in AVS user
        private var currentUser: User
        private var messageProvider: ChatMessageProvider
        private var threshold: Int = 5
        private var channelMembers: [User]
        private var unsentStore: UnsentMessageStore

        init(twilioChannel: TwilioChannel, tchChannel: TCHChannel?, currentUser: User, channelMembers: [User]) {
            self.tchChannel = tchChannel
            self.twilioChannel = twilioChannel
            self.currentUser = currentUser
            self.channelMembers = channelMembers
            messageProvider = .init(twilioChannel: twilioChannel, channel: tchChannel)
            messageStore = .init(conversationId: twilioChannel.channelID, perPage: messageProvider.perPage)
            unsentStore = .init(conversationID: twilioChannel.channelID)
            Log.this("Loaded \(unsentStore.messages.count) unsent messages")
        }

        func groupForMessage(_ message: Message) -> MessageGroup? {
            messageGroups.first { $0.messages.contains(message.id) }
        }

        @discardableResult
        func loadCachedMessages() -> Bool {
            if let messages = [Message].readCached(.twilioConversation(twilioChannel.channelID)), messages.count > 0 {
                replaceMessages(unsentStore.messages + messages, cached: true)
                return true
            }

            return false
        }

        func loadMoreContentIfNeeded(currentMessage message: Message? = nil) {
            if let message = message {
                let thresholdIndex = messages.index(messages.endIndex, offsetBy: -threshold) // for 20 messages this will be 15
                let messageIndex = messages.firstIndex(where: { $0.id == message.id })
                if messageIndex == thresholdIndex {
                    loadMoreContent()
                }
            } else {
                loadMoreContent(reset: true)
            }
        }

        func replaceMessage(_ message: Message) {
            if messageStore.replaceMessage(message) {
                regroupMessages(.replace(message))
            }
        }

        func prepareResendMessage(_ message: Message) -> Message {
            var message = message
            message.outgoingState = .sending
            message.date = Date()
            replaceMessage(message)
            return message
        }

        func appendMessages(_ messages: [TCHMessage]) {
            appendMessages(mapMessages(messages))
        }

        func prependMessage(_ message: Message) {
            messageStore.prependMessage(message)
            regroupMessages(.prepend(message))
        }

        func prependMessage(_ message: TCHMessage) {
            prependMessage(mapMessage(message))
        }

        func appendMessages(_ messages: [Message]) {
            messageStore.append(messages)
            regroupMessages(.appendMultiple(messages))
        }

        func replaceMessages(_ messages: [Message], cached: Bool = false) {
            if !cached {
                // >= to include possible cases with unsent messages, where the messages list would contain those too
                hasNextPage = messages.count >= messageProvider.perPage
            }

            messageStore.replaceMessages(messages, cached: cached)
            lastReadMessage = getLastReadMessage()
            regroupMessages(.replaceMultiple(messages))
        }

        func removeMessage(_ message: Message) {
            if messageStore.remove(message) {
                unsentStore.removeMessageID(message.id)
                regroupMessages(.remove(message))
            }
        }

        func replaceByLocalId(_ tchMessage: TCHMessage) -> Bool {
            guard let attributes = tchMessage.attributes()?.dictionary,
                  let localID = attributes["local_id"] as? String,
                  let messageIndex = messages.firstIndex(where: { $0.id == localID }) else {
                return false
            }

            let message = messages[messageIndex]

            if let newMediaId = tchMessage.mediaSid {
                if let currentMediaId = message.mediaId,
                   newMediaId != currentMediaId {
                    TwilioHelper.swapImages(currentMediaId, newMediaId)
                }
            }

            var replacedMessage = mapMessage(tchMessage)

            // replacing the real id with the "sending" generated one so it won't flicker after reload
            replacedMessage.id = message.id

            messageStore.replaceMessage(replacedMessage, at: messageIndex)
            regroupMessages(.replace(replacedMessage))

            return true
        }

        func regroupMessages(_ operation: RegroupMessageOperation) {
            switch operation {
            case .appendMultiple(let messages):
                Log.this("[GROUPING] Appending multiple messages. Count \(messages.count)")
                // Multiple messages were added last. Regroup those and merge groups
                // The FIRST group in the new messages should be merged in the LAST group from the current messages
                // if they have the same message type
                var groups = Self.groupMessages(messages)

                if let firstNewGroup = groups.first, var lastExistingGroup = messageGroups.last,
                   firstNewGroup.belongsToAdjacentGroup(lastExistingGroup) {
                    // Add the message IDs to the last group
                    lastExistingGroup.messages.append(contentsOf: firstNewGroup.messages)
                    // Remove the group from the new groups
                    groups.remove(at: 0)
                    // Replace the message groups' last group with the modified one
                    messageGroups[messageGroups.count - 1] = lastExistingGroup
                    // Add the rest of the groups
                    messageGroups.append(contentsOf: groups)
                } else {
                    messageGroups.append(contentsOf: groups)
                }

                return
            case .prepend(let message):
                Log.this("[GROUPING] Prepending message: \(message.body)")
                // New message was inserted
                if var firstExistingGroup = messageGroups.first, firstExistingGroup.messageBelongsInGroup(message) {
                    firstExistingGroup.id = message.id
                    firstExistingGroup.messages.insert(message.id, at: 0)
                    messageGroups[0] = firstExistingGroup
                } else {
                    messageGroups.insert(MessageGroup(id: message.id, messages: [message.id], groupType: message.messageType, userId: message.user?.uid, date: message.date), at: 0)
                }

                return
            case .replace(let message, let previousId):
                Log.this("[GROUPING] Replacing message: \(message.body)")

                if let previousId = previousId {
                    if let index = messageGroups.firstIndex(where: { $0.messages.contains(previousId) }) {
                        var group = messageGroups[index]
                        if let messageIndex = group.messages.firstIndex(of: previousId) {
                            group.messages[messageIndex] = message.id
                            if messageIndex == 0 {
                                group.id = message.id
                            }
                            messageGroups[index] = group
                        }
                    }
                } else {
                    // Replacing the same message id. Manually trigger a change
                    if let index = messageGroups.firstIndex(where: { $0.messages.contains(message.id) }) {
                        let group = messageGroups[index]
                        messageGroups[index] = group
                    }
                }

                return
            case .replaceMultiple(let messages):
                Log.this("[GROUPING] Replacing multiple messages. Count: \(messages.count)")
                // Replacing all messages requires redoing all groups
                Log.this(messages.first)
                messageGroups = Self.groupMessages(messages)
                return
            case .remove(let message):
                Log.this("[GROUPING] Removing message: \(message.body)")

                if let index = messageGroups.firstIndex(where: { $0.messages.contains(message.id) }) {
                    var group = messageGroups[index]

                    if group.messages.count == 1 {
                        // 1 single message in group to remove - remove the group
                        messageGroups.remove(at: index)
                    } else {
                        // 2 messages minimum
                        if let idx = group.messages.firstIndex(of: message.id) {
                            group.messages.remove(at: idx)

                            if idx == 0 {
                                group.id = group.messages[0]
                            }

                            messageGroups[index] = group
                        }
                    }

                    return
                }
            case .removeAll:
                Log.this("[GROUPING] Remove all messages")
                messageGroups = []
                return
            }

            Log.thisError(APIError(message: "Could not properly regroup messages. Regrouping all messages by default"))
            messageGroups = Self.groupMessages(messages)
        }

        private static func groupMessages(_ messages: [Message]) -> [MessageGroup] {
            var groups = messages.split { $0.messageType != $1.messageType || $0.user?.uid != $1.user?.uid || $0.shouldSplitByTimestamp(message: $1) }.map { messages in
                MessageGroup(id: messages[0].id, messages: messages.map(\.id), groupType: messages[0].messageType, userId: messages[0].user?.uid, date: messages[0].date)
            }

            groups.enumerated().forEach { index, group in
                if let prevGroup = groups[safe: index - 1], let prevDate = prevGroup.date, let currentDate = group.date {
                    groups[index - 1].daySplit = !prevDate.isSameDay(date: currentDate)
                }
            }

            return groups
        }

        func mapMessages(_ messages: [TCHMessage]) -> [Message] {
            messages.reversed().map { (tchMessage: TCHMessage) -> Message in
                mapMessage(tchMessage)
            }
        }

        func mapMessage(_ message: TCHMessage) -> Message {
            let messageType: MessageType
            let messageAttributes = message.mappedAttributes
            // Message attributes user id has priority over the message author id
            let userId = messageAttributes?.userId ?? message.author

            if currentUser.uid == userId {
                messageType = .outgoing
            } else {
                messageType = .incoming
            }

            let channelUser: User?

            if let user = channelMembers.first(where: { $0.uid == userId }) {
                // User is present in channel members
                // the empty concierge is ignored in this list since it doesn't have User metadata in the descriptor
                channelUser = user
            } else if let uid = userId {
                let pictureURL = Configuration.default.aVotreService.apis.main.baseURL
                        .appendingPathComponent("api")
                        .appendingPathComponent("user")
                        .appendingPathComponent(uid)
                        .appendingPathComponent("picture")

                channelUser = .basicUser(uid: uid,
                                         firstName: messageAttributes?.firstName,
                                         lastName: messageAttributes?.lastName,
                                         userPictureURL: pictureURL)
            } else {
                channelUser = nil
            }

            return .init(message: message, messageType: messageType, user: channelUser)
        }

        private func loadMoreContent(reset: Bool = false) {
            guard !isLoadingPage && hasNextPage && messageProvider.tchChannel != nil else { return }

            if !reset, let lastIndex = messages.last?.messageIndex {
                isLoadingPage = true

                Log.this("getting another page")

                messageProvider.getOlderMessages(lastMessageIndex: lastIndex).done { [weak self] messages in
                    guard let self = self else { return }
                    let messageCount = messages.count
                    var messages = messages
                    // If the list contains the index used to query the next page, drop it before mapping
                    if let idx = messages.last?.index, self.messages.last?.messageIndex == idx.uintValue {
                        messages = messages.dropLast(1)
                    }

                    self.hasNextPage = messageCount >= self.messageProvider.perPage
                    self.isLoadingPage = false
                    self.appendMessages(messages)

                }.catch { [weak self] error in
                    self?.hasNextPage = false
                    self?.isLoadingPage = false
                    Log.thisError(error)
                }
            } else {
                isLoadingPage = true

                Log.this("getting first page")

                messageProvider.getMessages().done { [weak self] messages in
                    guard let self = self else { return }
                    Log.this("first page got \(messages.count) new messages")
                    let mappedMessages = self.mapMessages(messages)
                    self.isLoadingPage = false
                    self.replaceMessages(self.unsentStore.messages + mappedMessages)
                }.catch { [weak self] error in
                    self?.isLoadingPage = false
                    self?.hasNextPage = false
                    Log.thisError(error)
                }
            }
        }

        // MARK Text Sending functions

        func sendTextMessage(_ text: String) {
            let message = Message(id: UUID().uuidString, body: text, message: nil, date: Date(), messageType: .outgoing, outgoingState: .sending, user: currentUser, messageIndex: nil)
            unsentStore.addMessage(message)
            prependMessage(message)
            sendTextMessageObject(message)
        }

        func processSendingMessages() {
            unsentStore.sendingMessages.forEach { message in
                sendMessageObject(message)
            }
        }

        func sendMessageObject(_ message: Message) {
            switch message.messageContentType {
            case .image:
                sendImageMessageObject(message)
            case .text:
                sendTextMessageObject(message)
            }
        }

        private func sendTextMessageObject(_ message: Message) {
            var message = message

            // If the channel is not present, we're still loading the initial chat, leave the message as .sending
            guard tchChannel != nil else { return }

            guard let messages = tchChannel?.messages else {
                message.outgoingState = .failed
                replaceMessage(message)
                return
            }

            // Build TCHMessage to send
            var messageOptions = TCHMessageOptions().withBody(message.body)
            let attributes = messageOptions.withAttributes(TCHJsonAttributes(dictionary: [
                "local_id": message.id,
                "uid": currentUser.uid,
                "name": currentUser.fullName
            ]))

            if let attributes = attributes {
                messageOptions = attributes
            }

            messages.sendMessage(with: messageOptions) { [weak self] result, tchMessage in
                guard let self = self else { return }

                if result.isSuccessful() {
                    // If successful, we don't replace the message, some processing will be done in channel did receive message
                    self.unsentStore.removeMessageID(message.id)
                } else {
                    // If failed, we replace the message with the failed status, did receive message won't be called
                    message.outgoingState = .failed
                    self.replaceMessage(message)
                }
            }
        }

        // MARK Image Sending functions

        func sendImageMessage(_ image: UIImage) {
            var message = Message(id: UUID().uuidString, body: "", message: nil, date: Date(), messageType: .outgoing, outgoingState: .sending, user: currentUser, messageIndex: nil)

            let mediaId = message.id

            guard saveImage(image, messageID: mediaId),
                  let data = image.jpegData(compressionQuality: 0.8) else { return }

            message.mediaId = mediaId
            unsentStore.addMessage(message)

            prependMessage(message)

            sendImageData(data, localID: message.id).done { [weak self] tchMessage in
                // If successful, we don't replace the message, some processing will be done in channel did receive message
                guard let self = self else { return }
                self.unsentStore.removeMessageID(message.id)
            }.catch { error in
                // If failed, we replace the message with the failed status, did receive message won't be called
                Log.thisError(error)
                message.outgoingState = .failed
                self.replaceMessage(message)
            }
        }

        // TODO unify these, drop localImageIdentifier, use Message#id
        private func sendImageMessageObject(_ message: Message) {
            var message = message

            guard let identifier = message.mediaId,
                  let data = message.imageMessageData else {
                message.outgoingState = .failed
                replaceMessage(message)
                return
            }

            sendImageData(data, localID: identifier).done { [weak self] tchMessage in
                // If successful, we don't replace the message, some processing will be done in channel did receive message
                guard let self = self else { return }
                self.unsentStore.removeMessageID(message.id)
            }.catch { error in
                // If failed, we replace the message with the failed status, did receive message won't be called
                Log.thisError(error)
                message.outgoingState = .failed
                self.replaceMessage(message)
            }
        }

        // This sends the image data. Do not update Datasource logic here, respond to the promise if needed
        private func sendImageData(_ data: Data, localID: String) -> Promise<TCHMessage> {
            Promise<TCHMessage> { seal in
                guard let messages = tchChannel?.messages else {
                    seal.reject(APIError(message: "Error sending message"))
                    return
                }

                let inputStream = InputStream(data: data)
                var options = TCHMessageOptions().withMediaStream(inputStream,
                                                                  contentType: "image/jpeg",
                                                                  defaultFilename: "\(UUID().uuidString).jpg",
                                                                  onStarted: { Log.this("Media upload started") },
                                                                  onProgress: { (bytes) in Log.this("Media upload progress: \(bytes)") }) { mediaSid in
                    Log.this("Media upload completed")
                }

                let attributes = options.withAttributes(TCHJsonAttributes(dictionary: [
                    "local_id": localID,
                    "uid": currentUser.uid,
                    "name": currentUser.fullName
                ]))
                if let attributes = attributes {
                    options = attributes
                }

                messages.sendMessage(with: options) { result, message in
                    if result.isSuccessful() {
                        if let message = message {
                            Log.this("IMAGE MESSAGE SENT SUCCESSFULLY")
                            seal.fulfill(message)
                        } else {
                            seal.reject(APIError(message: "Error sending message"))
                        }
                    } else {
                        seal.reject(APIError.from(result.error))
                        Log.thisError(result.error)
                    }
                }
            }
        }

        func saveImage(_ image: UIImage, messageID: String) -> Bool {
            if let data = image.jpegData(compressionQuality: 0.8) {
                return TwilioHelper.saveImageData(data, identifier: messageID)
            }

            return false
        }

        // Get my newest read message from other users. Doesn't matter the user, matters to be the newest. Returns the newest message that belongs to the current user
        // Important: only use this at initial load, the member doesn't change later. updates will be set through memberReadChannel
        private func getLastReadMessage() -> Message? {
            // Last index someone else read. The newest possible index from all the other people in the chat

            guard let lastReadIndex = twilioChannel.lastReadMessageIndex else { return nil }
            Log.this(lastReadIndex)
            var found = false
            for message in messages {
                // we found the last read message. it might not be our user's message
                if !found && message.messageIndex == lastReadIndex {
                    // First find the newest read message index
                    found = true
                    Log.this("FOUND = true")
                }

                // only search through messages if we have found an index (if we haven't it means the messages are newer than the last read)
                if found && message.user?.uid == currentUser.uid {
                    Log.this("returning message id: \(message.id)")
                    return message
                }
            }

            return nil
        }

        func findMember(_ member: TCHMember) -> User? {
            channelMembers.first { $0.uid == member.identity }
        }

        func memberReadChannel(_ member: TCHMember) -> Message? {
            // make sure someone else read the channel
            guard !member.isMe(currentUserId: currentUser.uid, channelUniqueName: twilioChannel.channelUniqueName) else { return lastReadMessage }

            // last read message will be our last successfully sent message
            let lastReadMessage = messages.first(where: { $0.messageType.isOutgoing && $0.outgoingState == .none })

            // only change it if it's different than the current selected one
            if lastReadMessage?.id != self.lastReadMessage?.id {
                self.lastReadMessage = lastReadMessage
            }

            return self.lastReadMessage
        }

        func markAsReadIfActive(_ completion: @escaping (UInt) -> Void) {
            guard active else { return }

            tchChannel?.messages?.setAllMessagesConsumedWithCompletion { result, int in
                if result.isSuccessful() {
                    completion(int)
                }
            }
        }
    }

}