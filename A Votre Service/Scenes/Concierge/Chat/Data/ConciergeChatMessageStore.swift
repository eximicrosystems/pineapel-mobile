//
// Created by Andrei Stoicescu on 05/10/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import Disk

extension CodableCacheKey {
    static func unsentStore(_ conversationId: String) -> CodableCacheKey { .init(key: "unsent_\(conversationId)") }
}

// Add a message while it's sending, remove it after it's sent. If not removed, it will be shown as unsent on app reboot
extension ConciergeChat {
    class UnsentMessageStore {

        private var _messages = [Message]() { didSet { save() } }

        var messages: [Message] {
            _messages.map {
                var message = $0
                if sendingMessageIds.contains(message.id) {
                    message.outgoingState = .sending
                }
                return message
            }.propertySorted({ $0.date }, descending: true)
        }

        var sendingMessages: [Message] {
            messages.filter { $0.outgoingState == .sending }
        }

        // Holds any message IDs that were sent before having a tchChannel
        private(set) var sendingMessageIds = Set<String>()

        let conversationID: String

        init(conversationID: String) {
            self.conversationID = conversationID
            loadMessages()
        }

        private func loadMessages() {
            _messages = [Message].readCached(.unsentStore(conversationID)) ?? []
        }

        func addMessage(_ message: Message) {
            guard _messages.first(where: { $0.id == message.id }) == nil else { return }
            sendingMessageIds.insert(message.id)
            var message = message
            message.outgoingState = .failed
            _messages.insert(message, at: 0)
        }

        func removeMessageID(_ messageID: String) {
            guard let index = _messages.firstIndex(where: { $0.id == messageID }) else { return }
            _messages.remove(at: index)
            sendingMessageIds.remove(messageID)
        }

        func save() {
            _messages.cache(.unsentStore(conversationID))
        }

        func clear() {
            _messages = .init()
            CodableCache.remove(.unsentStore(conversationID))
        }
    }
}
