//
// Created by Andrei Stoicescu on 30/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import TwilioChatClient
import PromiseKit
import Disk
import Alamofire
import AlamofireImage

fileprivate extension String {
    var jpg: String {
        "\(self).jpg"
    }
}

extension Disk {
    static func url(for path: TwilioHelper.Path) throws -> URL {
        try url(for: path.string, in: path.directory)
    }

    static func save(_ value: Data, as path: TwilioHelper.Path) throws {
        try save(value, to: path.directory, as: path.string)
    }

    static func doNotBackup(_ path: TwilioHelper.Path) throws {
        try doNotBackup(path.string, in: path.directory)
    }

    static func retrieveImage(_ path: TwilioHelper.Path) throws -> UIImage {
        try retrieve(path.string, from: path.directory, as: UIImage.self)
    }

    static func exists(_ path: TwilioHelper.Path) -> Bool {
        Disk.exists(path.string, in: path.directory)
    }

    static func clear(_ path: TwilioHelper.Path) throws {
        try? Disk.remove(path.folder, from: path.directory)
    }
}

class TwilioHelper {
    enum Path {
        case images(_ fileName: String)

        var string: String {
            switch self {
            case .images(let fileName):
                return "\(folder)/\(fileName.jpg)"
            }
        }

        var folder: String {
            switch self {
            case .images(_):
                return "TwilioImages"
            }
        }

        var directory: Disk.Directory {
            switch self {
            case .images(_):
                return .caches
            }
        }

        var url: URL? {
            switch self {
            case .images:
                return try? Disk.url(for: string, in: directory)
            }
        }
    }

    enum TwilioHelperError: Error {
        case localAndTchImageMissing
        case mediaIDMissing
        case tempFileURLError
        case outputStreamError
        case documentsFolderAccessError
        case receivedLocalImageMissing
    }

    let queue = DispatchQueue(label: "TwilioHelper")

    static func imageUrl(identifier: String?) -> URL? {
        guard let fileName = identifier else { return nil }
        return try? Disk.url(for: .images(fileName))
    }

    static func getCachedLocalImage(from message: ConciergeChat.Message) -> UIImage? {
        if message.mediaId != nil,
           let url = TwilioHelper.imageUrl(identifier: message.mediaId),
           let data = try? Data(contentsOf: url),
           let image = UIImage(data: data) {
            return image
        }

        return nil
    }

    @discardableResult
    static func saveImageData(_ data: Data, identifier: String) -> Bool {
        let path = Path.images(identifier)

        if (try? Disk.save(data, as: path)) != nil {
            try? Disk.doNotBackup(path)
            return true
        }
        return false
    }

    func getImage(from message: ConciergeChat.Message) -> Promise<UIImage> {
        Promise<UIImage?> { seal in
            Log.this("GET IMAGE FOR MESSAGE")
            Log.this(message)
            seal.fulfill(TwilioHelper.getCachedLocalImage(from: message))
        }.then { image -> Promise<UIImage> in
            if let image = image {
                return .value(image)
            } else if let tchMessage = message.message {
                return self.getImage(from: tchMessage)
            } else {
                throw TwilioHelperError.localAndTchImageMissing
            }
        }
    }

    func getImage(from message: TCHMessage) -> Promise<UIImage> {
        getCachedTwilioImage(from: message).then(on: DispatchQueue.global(qos: .background)) { image -> Promise<UIImage> in
            if let image = image {
                return .value(image)
            } else {
                return self.downloadTwilioImage(message: message)
            }
        }
    }

    func downloadTwilioImage(message: TCHMessage) -> Promise<UIImage> {
        Promise<UIImage> { seal in
            guard let mediaId = message.mediaSid else {
                seal.reject(APIError(message: "Invalid Media ID"))
                return
            }

            let path: Path = .images(mediaId)

            guard let folder = try? Disk.url(for: path.folder, in: path.directory) else {
                seal.reject(TwilioHelperError.documentsFolderAccessError)
                return
            }

            if !Disk.exists(folder) {
                try? FileManager.default.createDirectory(at: folder, withIntermediateDirectories: true)
            }

            message.getMediaContentTemporaryUrl { result, urlString in
                guard result.isSuccessful() else {
                    Log.thisError(result.error)
                    seal.reject(APIError.from(result.error))
                    return
                }

                guard let urlString = urlString,
                      let url = URL(string: urlString),
                      let mediaId = message.mediaSid else {
                    seal.reject(APIError(message: "Invalid Image URL"))
                    return
                }

                AF.request(url).responseImage { response in
                    switch response.result {
                    case .success(let image):
                        if let data = image.jpegData(compressionQuality: 0.8) {
                            TwilioHelper.saveImageData(data, identifier: mediaId)
                        }
                        seal.fulfill(image)
                    case .failure(let error):
                        seal.reject(error)
                    }
                }
            }
        }
    }

    private func getCachedTwilioImage(from message: TCHMessage) -> Promise<UIImage?> {
        Promise<UIImage?> { seal in
            guard let messageID = message.mediaSid else {
                Log.this("No message ID given", type: .error)
                seal.reject(TwilioHelperError.mediaIDMissing)
                return
            }

            let path: Path = .images(messageID)

            if let image = try? Disk.retrieveImage(path) {
                seal.fulfill(image)
                return
            }

            seal.fulfill(nil)
        }
    }

    static func clear() {
        try? Disk.clear(.images(""))
    }

    static func swapImages(_ oldId: String, _ newId: String) {
        guard oldId != newId else {
            Log.thisError(APIError(message: "Old image has the same name as new image"))
            return
        }

        let oldPath: Path = .images(oldId)
        let newPath: Path = .images(newId)

        guard Disk.exists(oldPath) else {
            Log.thisError(APIError(message: "Old image path does not exist"))
            return
        }

        guard !Disk.exists(newPath) else {
            Log.thisError(APIError(message: "New image path already exists"))
            return
        }

        guard let oldPathUrl = oldPath.url, let newPathUrl = newPath.url else {
            Log.thisError(APIError(message: "Cannot build old image url or new image url"))
            return
        }

        Log.this("ATTEMPTING TO MOVE \(oldPathUrl) to \(newPathUrl)")

        do {
            try Disk.move(oldPathUrl, to: newPathUrl)
            Log.this("MOVED \(oldPathUrl) to \(newPathUrl)")
        } catch (let error) {
            Log.thisError(error)
        }
    }
}
