//
// Created by Andrei Stoicescu on 24/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import TwilioChatClient
import PromiseKit

extension ConciergeChat {

    class ChannelListManager: ObservableObject {

        struct CreateChannelResolver {
            var resolver: Resolver<TwilioChannel>
            var user: User
        }

        private var items = [TwilioChannel]() {
            didSet {
                items.cache(.twilioChannels)
                sortedItems = items.propertySorted({ $0.lastMessageDate }, descending: true)
            }
        }

        func tchChannel(for twilioChannel: TwilioChannel) -> TCHChannel? {
            Log.this("finding local channel id \(twilioChannel.channelID) in [\(tchChannels.map { $0.sid })]")
            return tchChannels.first { $0.sid == twilioChannel.channelID }
        }

        @Published var sortedItems = [TwilioChannel]()
        @Published var isLoading = false
        @Published var totalUnreadCount: UInt = 0
        // Triggers changes only when the array itself changes (channel gets added or removed, array replaced etc)
        @Published var tchChannels = [TCHChannel]()

        private var createChannelResolver: CreateChannelResolver?

        var currentUser: User?
        var buildingID: String?

        var channelsList: TCHChannels? {
            didSet { reloadData() }
        }

        func reloadTotalUnreadCount() {
            totalUnreadCount = sortedItems.map(\.unreadCount).reduce(0, +)
        }

        func reloadData() {
            Log.this("CHANNEL LIST RELOAD DATA")
            if isLoading { return }

            guard let channels = channelsList else {
                isLoading = false
                return
            }

            isLoading = true

            var tchChannels = channels.subscribedChannelsSorted(by: .lastMessage, order: .descending)

            if let buildingID = buildingID {
                tchChannels = tchChannels.filter { channel in
                    let attributes = try? TCHMessage.Attributes(jsonDictionary: channel.attributes()?.dictionary)
                    return attributes?.buildingID == buildingID
                }
            }

            self.tchChannels = tchChannels
            Log.this("DID SET CHANNELS")

            when(resolved: tchChannels.map { $0.fetchChannelMetadata(currentUser: currentUser) }).done { results in
                Log.this("MODIFYING ITEMS FROM ChannelListManager.reloadData")
                self.items = results.compactMap { result in
                    switch result {
                    case .fulfilled(let channel):
                        return channel
                    case .rejected(_):
                        return nil
                    }
                }

                self.isLoading = false
                self.reloadTotalUnreadCount()
            }
        }

        func readChannel(_ channel: TCHChannel) {
            reloadTotalUnreadCount()
        }

        func channelAdded(_ channel: TCHChannel) {
            channel.fetchChannelMetadata(currentUser: currentUser).done { [weak self] twilioChannel in
                guard let self = self else { return }
                self.items.append(twilioChannel)

                if let resolver = self.createChannelResolver {
                    if twilioChannel.residentUser.uid == resolver.user.uid {
                        resolver.resolver.fulfill(twilioChannel)
                        self.createChannelResolver = nil
                    }
                }
            }.catch { error in
                Log.thisError(error)
            }
        }

        func channelAddedMessage(_ channel: TCHChannel, message: TCHMessage, channelActive: Bool) {
            guard let idx = findChannelIndex(channel) else { return }

            var item = items[idx]

            if !isMe(message.member, channel: channel) && !channelActive {
                item.unreadCount += 1
            }

            item.lastMessageString = message.body
            item.lastMessageDate = message.dateUpdatedAsDate
            Log.this("MODIFYING ITEMS FROM channelAddedMessage")
            items[idx] = item
            reloadTotalUnreadCount()
        }

        func channelRead(_ channel: TCHChannel, member: TCHMember, lastReadMessageIndex: UInt?) {
            if let idx = findChannelIndex(channel) {
                if isMe(member, channel: channel) {
                    Log.this("MODIFYING ITEMS FROM channelRead")
                    var item = items[idx]
                    item.unreadCount = 0
                    item.lastReadMessageIndex = lastReadMessageIndex
                    items[idx] = item

                    reloadTotalUnreadCount()
                }
            }
        }

        func findChannel(_ channel: TCHChannel) -> TwilioChannel? {
            items.first { $0.channelID == channel.sid }
        }

        private func findChannelIndex(_ channel: TCHChannel) -> Int? {
            items.firstIndex { $0.channelID == channel.sid }
        }

        private func isMe(_ member: TCHMember?, channel: TCHChannel) -> Bool {
            guard let member = member else { return false }
            return member.isMe(currentUserId: currentUser?.uid, channelUniqueName: channel.uniqueName)
        }

        func channelForResident(_ user: User) -> Promise<TwilioChannel> {
            if let channel = items.first(where: { $0.residentUser.uid == user.uid }) {
                return .value(channel)
            }

            return Promise<TwilioChannel> { seal in
                createChannelResolver = .init(resolver: seal, user: user)
                ResidentService().createTwilioChannel(userID: user.uid).catch { [weak self] error in
                    seal.reject(error)
                    guard let resolver = self?.createChannelResolver else { return }
                    if resolver.user.uid == user.uid {
                        self?.createChannelResolver = nil
                    }
                }
            }
        }

        func shutdown() {
            currentUser = nil
            isLoading = false
            totalUnreadCount = 0
            items.removeAll()
            channelsList = nil
        }

    }
}
