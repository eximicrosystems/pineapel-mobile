//
// Created by Andrei Stoicescu on 24/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import Combine

extension ConciergeChat.MessageType {
    var textColor: AVSColor {
        switch self {
        case .outgoing:
            return .accentBackground
        case .incoming:
//            return .primaryLabel
            return .colorMilitaryGreen
        }
    }

    var backgroundColor: AVSColor {
        switch self {
        case .outgoing:
//            return .accentBackground
            return .colorMilitaryGreen
        case .incoming:
            return .chatMessageFill
//            return .colorMilitaryGreen
        }
    }
}

extension ConciergeChat {

    class NewRecipientCell: ASCellNode {
        var userNameNode = ASTextNode2()
        var plusNode: ASImageNode
        var avatarNode: GenericNodes.AvatarNode
        var line = ASDisplayNode.divider()

        init(user: User) {
            userNameNode.attributedText = user.fullName.attributed(.body, .primaryLabel)
            plusNode = ASImageNode(image: Asset.Images.Concierge.icPlus.image.withRenderingMode(.alwaysTemplate))
            plusNode.tintColor = AVSColor.primaryIcon.color
            avatarNode = .init(size: 36, shadow: false, nameFallback: .initials([user.firstName, user.lastName]))
            avatarNode.url = user.userPictureURL

            super.init()
            backgroundColor = AVSColor.secondaryBackground.color
            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            [
                [
                    [
                        avatarNode.square(36),
                        userNameNode.filling()
                    ].hStacked().spaced(10).filling(),
                    plusNode
                ].hStacked().spaced(10).justified(.spaceBetween)
                 .margin(20),
                line
            ].vStacked()
        }
    }

    class ConversationCell: ASCellNode {

        var userNameNode = ASTextNode2()
        var lastMessageNode: ASTextNode2?

        var timestampNode: ASTextNode2?
        var unreadCountNode: ConciergeGenericNodes.BadgeNode?

        var avatarNode: GenericNodes.AvatarNode
        var line = ASDisplayNode.divider()

        var channel: TwilioChannel

        init(channel: TwilioChannel) {
            userNameNode.attributedText = channel.residentUser.fullName.attributed(.bodyHeavy, .primaryLabel)

            self.channel = channel

            if let message = channel.lastMessageString {
                lastMessageNode = ASTextNode2(with: channel.unreadCount > 0 ? message.attributed(.bodyHeavy, .primaryLabel) : message.attributed(.body, .primaryLabel))
            }

            if channel.unreadCount > 0 {
                unreadCountNode = .init(value: Int(channel.unreadCount), backgroundColor: AVSColor.accentBackground.color, style: .small)
            }

            if let lastMessageDate = channel.lastMessageDate {
                timestampNode = ASTextNode2(with: lastMessageDate.timeAgoSinceNow.attributed(.note, .secondaryLabel))
            }

            avatarNode = .init(size: 36, shadow: false, nameFallback: .initials([channel.residentUser.firstName, channel.residentUser.lastName]))
            avatarNode.url = channel.residentUser.userPictureURL

            super.init()
            backgroundColor = AVSColor.secondaryBackground.color
            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            lastMessageNode?.maximumNumberOfLines = 1

            return [
                [
                    avatarNode.square(36),
                    [
                        userNameNode,
                        lastMessageNode
                    ].vStacked().filling(),

                    [
                        timestampNode,
                        unreadCountNode
                    ].vStacked().spaced(5).aligned(.end)
                ].hStacked().spaced(10).margin(20),
                line
            ].vStacked()
        }
    }

    class MessageCell: ASCellNode {
        var messageBody: ASTextNode2
        var messageBackground: ASDisplayNode
        var timestamp: ASTextNode2?
        var dayTimestampNode: ASTextNode2?
        var messageType: MessageType
        var chatCellImage: ChatCellImage
        var isImageMessage: Bool
        var twilioImage: TwilioImage?
        var messageModel: MessageViewModel
        var avatarNode: GenericNodes.AvatarNode
        var chatStateNode: ChatStateNode

        private var subscriptions = Set<AnyCancellable>()

        init(messageModel: MessageViewModel) {
            self.messageModel = messageModel
            messageType = messageModel.message.messageType
            isImageMessage = messageModel.message.hasImage

            messageBody = .init(with: messageModel.message.body.attributed(.body, messageType.textColor))
            messageBackground = .init()
            messageBackground.cornerRadius = 8
            messageBackground.backgroundColor = messageType.backgroundColor.color

            chatCellImage = .init()

            chatStateNode = .init(state: ChatStateNode.chatState(message: messageModel.message, lastRead: messageModel.lastReadMessage))

            avatarNode = GenericNodes.AvatarNode(size: 36, shadow: false, border: false, nameFallback: .initials([messageModel.message.user?.firstName, messageModel.message.user?.lastName]))
            avatarNode.url = messageModel.message.user?.userPictureURL

            avatarNode.isHidden = !messageModel.showsAvatar

            if messageModel.showsTimestamp {
                timestamp = .init(with: messageModel.message.date?.avsTimeAgoChat.attributed(.note, .tertiaryLabel))
            }

            if messageModel.showsDay {
                dayTimestampNode = .init(with: messageModel.message.date?.avsDateString().attributed(.bodyHeavy, .tertiaryLabel, alignment: .center))
            }

            super.init()

            if messageModel.message.hasImage {
                DispatchQueue.main.async {
                    let twilioImage = TwilioImageManager.shared.imageForMessage(messageModel.message)

                    self.chatCellImage.state = .loading

                    twilioImage.$image.receive(on: DispatchQueue.main).sink { [weak self] image in
                        guard let self = self else { return }
                        if image != nil {
                            self.chatCellImage.imageNode.image = image
                            self.chatCellImage.state = .image
                        }
                    }.store(in: &self.subscriptions)

                    twilioImage.$error.receive(on: DispatchQueue.main).sink { [weak self] error in
                        guard let error = error else { return }
                        Log.thisError(error)
                        self?.chatCellImage.state = .error
                    }.store(in: &self.subscriptions)

                    self.twilioImage = twilioImage
                }
            }

            automaticallyManagesSubnodes = true
        }

        override func cellNodeVisibilityEvent(_ event: ASCellNodeVisibilityEvent, in scrollView: UIScrollView?, withCellFrame cellFrame: CGRect) {
            switch event {
            case .visible:
                if messageModel.message.hasImage {
                    DispatchQueue.main.async { [weak self] in
                        guard let self = self else { return }
                        self.twilioImage?.getImage()
                    }
                }
            default:
                ()
            }
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            let body: ASLayoutElement

            if isImageMessage {
                body = chatCellImage.maxWidth(.points(constrainedSize.max.width)).ratio(3 / 4).filling()
            } else {
                body = messageBody.margin(10).background(messageBackground).filling()
            }

            let messageAndTimestampElements: [ASLayoutElement?] = messageType.isOutgoing ? [timestamp, body, chatStateNode.alignSelf(.end)] : [avatarNode.square(36).alignSelf(.end), body, timestamp]
            let messageAndTimestampStack = messageAndTimestampElements.hStacked().spaced(10).maxWidth(.fraction(0.8))
            let cappedMessageStack = messageType.isOutgoing ? [ASDisplayNode.spacer(), messageAndTimestampStack].hStacked() : [messageAndTimestampStack, ASDisplayNode.spacer()].hStacked()

            return [
                dayTimestampNode?.margin(horizontal: 15),
                cappedMessageStack.margin(bottom: 5).margin(horizontal: 10)
            ].vStacked().spaced(15).margin(top: messageModel.firstMessageInGroup ? 15 : 0)
        }
    }

}
