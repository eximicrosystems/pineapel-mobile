//
// Created by Andrei Stoicescu on 12/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import UIKit
import Foundation
import AsyncDisplayKit
import SwiftyUserDefaults
import PromiseKit
import SVProgressHUD
import ImageViewer
import TwilioChatClient
import DifferenceKit
import Combine

extension ConciergeChat {
    class ResidentChatLoaderViewController: ASDKViewController<ASDisplayNode> {
        private var subscriptions = Set<AnyCancellable>()
        private var dataSource: MessageDatasource?
        private var loadedFromCache = false

        override init() {
            super.init(node: .init())

            User.fetch().get { user in
                MessagingManager.shared.user = user
                self.loadFromCache(user: user)
            }.then { user in
                self.createChannelIfNeeded(user: user).map { user }
            }.done { user in
                self.listenForChannels(user: user)
                MessagingManager.shared.connect()
            }.cauterize()
        }

        func listenForChannels(user: User) {
            MessagingManager.shared.channelListManager.$sortedItems
                    .receive(on: DispatchQueue.main)
                    .dropFirst() // First time there's an empty array sent which we need to ignore
                    .sink { [weak self] items in
                        if items.count > 0 {
                            items.cache(.twilioChannels)
                        }
                        self?.setupDataSourceIfNeeded(user: user)
                    }.store(in: &subscriptions)
        }

        func loadFromCache(user: User) {
            let channels = [TwilioChannel].readCached(.twilioChannels) ?? []
            guard let channel = channels.first(where: { $0.residentUser.uid == user.uid }) else { return }
            let dataSource = MessagingManager.shared.messageDataSources.findOrCreateSource(channel: channel, tchChannel: nil, currentUser: user)
            self.dataSource = dataSource
            navigationController?.viewControllers = [ChatViewController(title: "Chat", dataSource: dataSource, autoUpdateBadge: true)]
            loadedFromCache = true
        }

        func createChannelIfNeeded(user: User) -> Promise<Void> {
            guard !loadedFromCache else { return .value(()) }

            return Promise<Void> { seal in
                ResidentService().createTwilioChannel(userID: user.uid)
                                 .ensure { seal.fulfill(()) }
                                 .catch { Log.thisError($0) }
            }
        }

        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }

        func setupDataSourceIfNeeded(user: User) {
            guard dataSource == nil else { return }

            MessagingManager.shared.channelListManager.channelForResident(user).map { ($0, user) }.done { [weak self] channel, user in
                guard let self = self else { return }
                let tchChannel = MessagingManager.shared.channelListManager.tchChannel(for: channel)
                let dataSource = MessagingManager.shared.messageDataSources.findOrCreateSource(channel: channel, tchChannel: tchChannel, currentUser: user)
                self.dataSource = dataSource
                self.navigationController?.viewControllers = [ChatViewController(title: "Chat", dataSource: dataSource, autoUpdateBadge: true)]
            }.catch { error in
                Log.thisError(error)
                SVProgressHUD.showError(withStatus: error.localizedDescription)
            }
        }

        func updateBadge(_ badge: UInt) {
            navigationController?.tabBarItem.badgeValue = badge > 0 ? String(badge) : nil
        }

        override func viewDidLoad() {
            super.viewDidLoad()
            navigationController?.setNavigationBarHidden(true, animated: false)
            view.backgroundColor = AVSColor.secondaryBackground.color
            
            MessagingManager.shared.$isLoading
                    .receive(on: DispatchQueue.main)
                    .sink { [weak self] loading in
                        self?.handleLoadingIndicator()
                    }.store(in: &subscriptions)
        }

        override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(animated)
            handleLoadingIndicator()
        }

        override func viewDidDisappear(_ animated: Bool) {
            super.viewDidDisappear(animated)
            handleLoadingIndicator()
        }
        
//        override func viewWillAppear(_ animated: Bool) {
//            super.viewWillAppear(animated)
//            navigationController?.setNavigationBarHidden(true, animated: false)
//        }
//
//        override func viewWillDisappear(_ animated: Bool) {
//            super.viewWillDisappear(animated)
//            navigationController?.setNavigationBarHidden(false, animated: true)
//        }

        func handleLoadingIndicator() {
            let chatVisible = viewIfLoaded?.window != nil

            let loading = MessagingManager.shared.isLoading || dataSource?.isLoadingPage == true

            if loading && chatVisible && !loadedFromCache {
                SVProgressHUD.show()
            } else {
                SVProgressHUD.dismiss()
            }
        }
    }

}
