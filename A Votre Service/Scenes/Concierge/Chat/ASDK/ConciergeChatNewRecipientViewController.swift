//
// Created by Andrei Stoicescu on 01/10/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import PromiseKit
import Combine
import Fakery
import TwilioChatClient
import SwiftyUserDefaults

extension ConciergeChat {
    class ConciergeChatNewRecipientViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

        lazy var emptyNode = EmptyNode(emptyNodeType: .generic, showAction: false)

        var collectionNode: ASCollectionNode

        var users = [User]()
        var didSelectUser: ((User) -> Void)?

        override init() {
            collectionNode = .avsCollection()
            super.init(node: collectionNode)
            collectionNode.delegate = self
            collectionNode.dataSource = self
        }

        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }

        override func viewDidLoad() {
            super.viewDidLoad()
            replaceSystemBackButton()
            showEmptyScreen(false)
            collectionNode.view.backgroundView = emptyNode.view
            navigationItem.title = "Select Recipient"

            let btn = UIButton(type: .custom)
            btn.setImage(Asset.Images.Concierge.icClose.image.withRenderingMode(.alwaysTemplate), for: .normal)
            btn.tintColor = AVSColor.primaryIcon.color
            btn.addTarget(self, action: #selector(tapClose), for: .touchUpInside)
            navigationItem.leftBarButtonItem = UIBarButtonItem(customView: btn)

            SVProgressHUD.show()
            refreshItems()
        }

        func refreshItems() {
            ConciergeService().getAllUsers().done { [weak self] users in
                SVProgressHUD.dismiss()
                guard let self = self else { return }
                self.users = users
                self.collectionNode.reloadData()
            }.catch { error in
                SVProgressHUD.showError(withStatus: error.localizedDescription)
            }
        }

        @objc func tapClose() {
            guard let nav = navigationController as? AVSNavigationController else { return }
            nav.dismissNavigation?()
        }

        func showEmptyScreen(_ show: Bool) {
            emptyNode.isHidden = !show
        }

        func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
            users.count
        }

        func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
            let user = users[indexPath.item]
            return {
                NewRecipientCell(user: user)
            }
        }

        func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
            didSelectUser?(users[indexPath.item])
        }

        func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
            .fullWidth(collectionNode: collectionNode)
        }
    }
}