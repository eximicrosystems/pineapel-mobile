// //
// //  ConciergeChatInput.swift
// //  A Votre Service
// //
// //  Created by Andrei Stoicescu on 30/09/2020.
// //  Copyright © 2020 A Votre Service LLC. All rights reserved.
// //
//
// import SwiftUI
//
// extension AnyTransition {
//     static var opacityScaled: AnyTransition {
//         AnyTransition.scale.combined(with: opacity)
//     }
// }
//
// struct ConciergeChatInput: View {
//
//     @Binding var text: String
//     var didTapSend: () -> ()
//     var didSelectImage: (UIImage) -> ()
//
//     @State private var showImagePicker = false
//     @State private var showCameraPicker = false
//     @State private var pickedImage: UIImage?
//
//     var body: some View {
//         HStack(spacing: 10) {
//             DynamicHeightTextView(text: $text, minHeight: 22, maxHeight: 200, placeholder: "Type your message here".darkGrayBody)
//
//             Group {
//                 if text.count == 0 {
//                     HStack {
//                         if UIImagePickerController.isSourceTypeAvailable(.camera) {
//                             Button {
//                                 showCameraPicker = true
//                             } label: {
//                                 Image(systemName: "camera")
//                             }.sheet(isPresented: $showCameraPicker) {
//                                 ImagePickerView(image: $pickedImage, isShown: $showCameraPicker, sourceType: .camera) { image in
//                                     guard let image = image else { return }
//                                     didSelectImage(image)
//                                 }.edgesIgnoringSafeArea(.bottom)
//                             }
//                         }
//
//                         Button {
//                             showImagePicker = true
//                         } label: {
//                             Image(systemName: "photo")
//                         }.sheet(isPresented: $showImagePicker) {
//                             ImagePickerView(image: $pickedImage, isShown: $showImagePicker, sourceType: .photoLibrary) { image in
//                                 guard let image = image else { return }
//                                 didSelectImage(image)
//                             }.edgesIgnoringSafeArea(.bottom)
//                         }
//
//                     }.foregroundColor(Asset.Colors.Concierge.darkGray)
//                 } else {
//                     Button {
//                         didTapSend()
//                     } label: {
//                         Text("Send")
//                                 .conciergeFont(.BodyHead)
//                                 .foregroundColor(Asset.Colors.Concierge.primary)
//                     }
//                 }
//             }.transition(.opacityScaled)
//         }.padding(10)
//          .background(Color.white)
//          .border(Asset.Colors.Concierge.lightGray.sColor)
//          .padding(10)
//          .background(Asset.Colors.Concierge.background.sColor)
//     }
// }
//
// struct ConciergeChatInput_Previews: PreviewProvider {
//     static var previews: some View {
//         VStack {
//             ConciergeChatMessagesView_Previews.previews
//             ConciergeChatInput(text: .constant("")) {} didSelectImage: { _ in }
//         }
//     }
// }
