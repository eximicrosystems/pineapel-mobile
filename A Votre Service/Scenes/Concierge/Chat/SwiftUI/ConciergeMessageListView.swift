// //
// // Created by Andrei Stoicescu on 28/09/2020.
// // Copyright (c) 2020 A Votre Service LLC. All rights reserved.
// //
//
// import Foundation
// import SwiftUI
// import SVProgressHUD
// import TwilioChatClient
// import Fakery
// import Combine
//
// struct KeyboardIgnoreSafeArea: ViewModifier {
//     func body(content: Content) -> some View {
//         Group {
//             if #available(iOS 14.0, *) {
//                 content.ignoresSafeArea(.keyboard, edges: .bottom)
//             } else {
//                 content
//             }
//         }
//     }
// }
//
// extension View {
//     func compatibleIgnoreKeyboardSafeArea() -> some View {
//         self.modifier(KeyboardIgnoreSafeArea())
//     }
// }
//
// struct AdaptsToSoftwareKeyboard: ViewModifier {
//     @State var keyboardHeight: CGFloat = 0
//
//     var tabBarHeight: CGFloat {
//         if let vc = UIApplication.shared.windows.filter({ $0.isKeyWindow }).first?.rootViewController as? UITabBarController {
//             return vc.tabBar.frame.height
//         }
//         return 0
//     }
//
//     func body(content: Content) -> some View {
//         content
//                 .padding(.bottom, keyboardHeight == 0 ? 0 : keyboardHeight - tabBarHeight)
//                 .animation(.easeOut(duration: 0.16))
//                 .edgesIgnoringSafeArea(keyboardHeight == 0 ? [] : .bottom)
//                 .onAppear(perform: subscribeToKeyboardEvents)
//     }
//
//     private func subscribeToKeyboardEvents() {
//         NotificationCenter.Publisher(
//                 center: NotificationCenter.default,
//                 name: UIResponder.keyboardWillShowNotification
//         ).compactMap { notification in
//             notification.userInfo?["UIKeyboardFrameEndUserInfoKey"] as? CGRect
//         }.map { rect in
//             rect.height
//         }.subscribe(Subscribers.Assign(object: self, keyPath: \.keyboardHeight))
//
//         NotificationCenter.Publisher(
//                 center: NotificationCenter.default,
//                 name: UIResponder.keyboardWillHideNotification
//         ).compactMap { notification in
//             CGFloat.zero
//         }.subscribe(Subscribers.Assign(object: self, keyPath: \.keyboardHeight))
//     }
// }
//
// struct ImageMessageView: View {
//     @ObservedObject var image: TwilioImage
//
//     var body: some View {
//         Group {
//             if image.loading || (image.error == nil && image.image == nil) {
//                 ActivityIndicator(style: .medium)
//                         .frame(width: 200, height: 200)
//                         .overlay(
//                                 RoundedRectangle(cornerSize: CGSize(width: 8, height: 8), style: .continuous)
//                                         .stroke(Asset.Colors.Concierge.lightGray.sColor)
//                         )
//             } else if let uiImage = image.image {
//                 Image(uiImage: uiImage)
//                         .resizable()
//                         .aspectRatio(contentMode: .fill)
//                         .frame(width: 200, height: 200)
//                         .clipShape(RoundedRectangle(cornerSize: CGSize(width: 8, height: 8)))
//             } else if image.error != nil {
//                 Image(systemName: "xmark.octagon")
//                         .resizable()
//                         .foregroundColor(Asset.Colors.Concierge.red.sColor)
//                         .frame(width: 40, height: 40)
//                         .padding(80)
//                         .overlay(
//                                 RoundedRectangle(cornerSize: CGSize(width: 8, height: 8), style: .continuous)
//                                         .stroke(Asset.Colors.Concierge.lightGray.sColor)
//                         )
//             }
//         }
//     }
// }
//
// struct TextMessageView: View {
//     var message: ConciergeChat.Message
//
//     var body: some View {
//         Text(message.body)
//                 .conciergeFont(.Body)
//                 .foregroundColor(foregroundColor)
//                 .padding(.all, 10)
//                 .background(backgroundColor)
//                 .clipShape(RoundedRectangle(cornerSize: CGSize(width: 8, height: 8)))
//     }
//
//     var foregroundColor: Color {
//         message.messageType.isOutgoing ? .white : Asset.Colors.Concierge.primary.sColor
//     }
//
//     var backgroundColor: Color {
//         message.messageType.isOutgoing ? Asset.Colors.primary.sColor : Asset.Colors.Concierge.lightGray.sColor
//     }
// }
//
// struct MessageView: View {
//     var message: ConciergeChat.Message
//     var showTimestamp: Bool
//     var maxWidth: CGFloat
//
//     var body: some View {
//         if message.hasImage, let image = TwilioImageManager.shared.imageForMessage(message) {
//             ImageMessageView(image: image)
//                     .padding(.horizontal, 15)
//                     .modifier(MessageTimestampModifier(message: message, show: showTimestamp))
//                     .modifier(MessageOutgoingStateModifier(message: message))
//                     // .frame(maxWidth: maxWidth - 100, maxHeight: 220, alignment: message.messageType.isOutgoing ? .trailing : .leading)
//                     .modifier(MessageSpacingModifier(message: message))
//                     .onAppear {
//                         image.getImage()
//                     }.onDisappear {
//                         TwilioImageManager.shared.clearImage(image)
//                     }
//         } else {
//             TextMessageView(message: message)
//                     .padding(.horizontal, 15)
//                     .modifier(MessageTimestampModifier(message: message, show: showTimestamp))
//                     .modifier(MessageOutgoingStateModifier(message: message))
//                     .frame(maxWidth: maxWidth - 100, maxHeight: .infinity, alignment: message.messageType.isOutgoing ? .trailing : .leading)
//                     .modifier(MessageSpacingModifier(message: message))
//         }
//     }
// }
//
// struct ConciergeMessageListView: View {
//     @ObservedObject var dataSource: ConciergeChat.MessageDatasource
//     var resident: User
//
//     @State private var currentMessageText: String = ""
//
//     init(resident: User, dataSource: ConciergeChat.MessageDatasource) {
//         UIScrollView.appearance().keyboardDismissMode = .onDrag
//         self.resident = resident
//         self.dataSource = dataSource
//     }
//
//     var body: some View {
//         // TODO: for ios 13 try using List with this modifiers: https://stackoverflow.com/a/58474518/278843
//         GeometryReader { proxy in
//             VStack(spacing: 0) {
//                 #if DEBUG
//                 Button {
//                     dataSource.clearChat()
//                 } label: {
//                     Text("Clear Chat")
//                 }
//                 #endif
//                 ScrollView {
//                     Color.clear.padding()
//                     CompatibleLazyVStack(spacing: 0) {
//                         ForEach(dataSource.messageGroups) { group in
//                             ForEach(group.messages) { message in
//                                 MessageView(message: message, showTimestamp: group.messageShowsTimestamp(message), maxWidth: proxy.size.width)
//                                         .flippedVertically()
//                                         .onAppear {
//                                             dataSource.loadMoreContentIfNeeded(currentMessage: message)
//                                         }.padding(.bottom, 5)
//                             }
//                             Color.clear.padding(.bottom, 15)
//                         }
//                     }
//                     Color.clear.padding()
//                 }.flippedVertically()
//
//                 ConciergeChatInput(text: $currentMessageText) {
//                     dataSource.sendMessage(currentMessageText)
//                     currentMessageText = ""
//                 } didSelectImage: { image in
//                     dataSource.sendImage(image)
//                 }
//             }
//         }.modifier(AdaptsToSoftwareKeyboard())
//          .compatibleIgnoreKeyboardSafeArea()
//          .navigationBarTitle(Text(resident.fullName), displayMode: .inline)
//          .onAppear {
//              if dataSource.messages.count == 0 {
//                  dataSource.loadMoreContentIfNeeded()
//              }
//          }
//     }
// }
//
// fileprivate extension ConciergeMessageListView {
//     init(messages: [ConciergeChat.Message]) {
//         self.resident = User.default
//         self.dataSource = .init(channel: TCHChannel(), currentUser: User.default)
//         self.dataSource.hasNextPage = false
//         self.dataSource.appendMessages(messages)
//     }
// }
//
// fileprivate extension ConciergeChat.Message {
//     static func build(messageType: ConciergeChat.MessageType, long: Bool) -> ConciergeChat.Message {
//         let faker = Faker()
//         return ConciergeChat.Message(
//                 body: faker.lorem.sentences(amount: long ? 5 : 1),
//                 message: TCHMessage(),
//                 date: 1.hours.earlier,
//                 messageType: messageType)
//     }
// }
//
// class ConciergeChatMessagesView_Previews: PreviewProvider {
//     static var previews: some View {
//         NavigationView {
//             ConciergeMessageListView(messages:
//                                      [
//                                          .build(messageType: .outgoing, long: true),
//                                          .build(messageType: .outgoing, long: false),
//                                          .build(messageType: .outgoing, long: true),
//                                          .build(messageType: .incoming, long: false),
//                                          .build(messageType: .incoming, long: true),
//                                          .build(messageType: .outgoing, long: false),
//                                      ])
//         }
//
//     }
// }
