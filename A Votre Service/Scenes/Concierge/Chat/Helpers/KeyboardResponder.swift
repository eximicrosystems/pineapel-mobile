// //
// // Created by Andrei Stoicescu on 30/09/2020.
// // Copyright (c) 2020 A Votre Service LLC. All rights reserved.
// //
//
// import SwiftUI
//
// final class KeyboardResponder: ObservableObject {
//     private var notificationCenter: NotificationCenter
//     @Published private(set) var currentHeight: CGFloat = 0
//
//     init(center: NotificationCenter = .default) {
//         notificationCenter = center
//         notificationCenter.addObserver(self, selector: #selector(keyBoardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
//         notificationCenter.addObserver(self, selector: #selector(keyBoardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
//     }
//
//     deinit {
//         notificationCenter.removeObserver(self)
//     }
//
//     @objc func keyBoardWillShow(notification: Notification) {
//         if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
//             currentHeight = keyboardSize.height
//         }
//     }
//
//     @objc func keyBoardWillHide(notification: Notification) {
//         currentHeight = 0
//     }
// }

import Foundation
import UIKit
import Combine

class KeyboardResponder: ObservableObject {
    struct KeyboardInfo {
        let height: CGFloat
        let animationDuration: Double

        static var zero: KeyboardInfo {
            KeyboardInfo(height: 0, animationDuration: 0)
        }

        static func fromNotification(_ notification: Notification) -> KeyboardInfo {
            guard let rect = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect,
                  let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double else {
                return .zero
            }

            return KeyboardInfo(height: rect.height, animationDuration: duration)
        }
    }

    @Published var keyboardInfo: KeyboardInfo = .zero

    init() {
        NotificationCenter.Publisher(
                center: NotificationCenter.default,
                name: UIResponder.keyboardWillShowNotification
        ).map { notification in
            KeyboardInfo.fromNotification(notification)
        }.subscribe(Subscribers.Assign(object: self, keyPath: \.keyboardInfo))

        NotificationCenter.Publisher(
                center: NotificationCenter.default,
                name: UIResponder.keyboardWillHideNotification
        ).map { notification in
            let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double
            return KeyboardInfo(height: 0, animationDuration: duration ?? 0.3)
        }.subscribe(Subscribers.Assign(object: self, keyPath: \.keyboardInfo))
    }
}