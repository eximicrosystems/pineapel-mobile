// //
// // Created by Andrei Stoicescu on 30/09/2020.
// // Copyright (c) 2020 A Votre Service LLC. All rights reserved.
// //
//
// import Foundation
// import SwiftUI
//
// struct MessageTimestampModifier: ViewModifier {
//     var message: ConciergeChat.Message
//     var show: Bool
//
//     func body(content: Content) -> some View {
//         HStack(spacing: 0) {
//             if show {
//                 if let date = message.date, message.messageType == .outgoing {
//                     timestamp(date: date)
//                 }
//                 content
//                 if let date = message.date, message.messageType == .incoming {
//                     timestamp(date: date)
//                 }
//             } else {
//                 content
//             }
//         }
//     }
//
//     func timestamp(date: Date) -> some View {
//         Text(date.avsTimeString())
//                 .conciergeFont(.Note)
//                 .foregroundColor(Asset.Colors.darkGray)
//                 .layoutPriority(1)
//     }
// }
//
// struct VerticalFlipModifier: ViewModifier {
//     func body(content: Content) -> some View {
//         content.rotationEffect(.radians(.pi))
//                .scaleEffect(x: -1, y: 1, anchor: .center)
//     }
// }
//
// struct MessageSpacingModifier: ViewModifier {
//     var message: ConciergeChat.Message
//
//     func body(content: Content) -> some View {
//         HStack {
//             if message.messageType == .outgoing {
//                 Spacer()
//             }
//             content
//             if message.messageType == .incoming {
//                 Spacer()
//             }
//         }
//     }
// }
//
// struct MessageAvatarModifier: ViewModifier {
//     var message: ConciergeChat.Message
//
//     func body(content: Content) -> some View {
//         HStack {
//             if message.messageType == .incoming {
//                 Spacer()
//             }
//             content
//         }
//     }
// }
//
// struct MessageOutgoingStateModifier: ViewModifier {
//     var message: ConciergeChat.Message
//
//     func body(content: Content) -> some View {
//         HStack(spacing: 5) {
//             content
//             if message.outgoingState == .sending {
//                 ActivityIndicator(style: .medium)
//                         .frame(width: 20, height: 20)
//                         .padding(.trailing, 15)
//             }
//         }.transition(.offset(x: 20, y: 0))
//     }
// }
//
// extension View {
//     func flippedVertically() -> some View {
//         self.modifier(VerticalFlipModifier())
//     }
// }