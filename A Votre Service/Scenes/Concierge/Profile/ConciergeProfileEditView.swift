//
// Created by Andrei Stoicescu on 23/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import SwiftUI
import Alamofire
import PromiseKit
import SVProgressHUD

class ConciergeProfileForm: ObservableObject {

    var genderOptions: [ConciergeForm.Option<User.Gender>] = {
        User.Gender.allCases.map { ConciergeForm.Option<User.Gender>(id: $0.rawValue, label: $0.rawValue, underlyingValue: $0) }
    }()

    @Published var firstName: String = ""
    @Published var lastName: String = ""
    @Published var phoneNumber: String = ""
    @Published var gender: [ConciergeForm.Option<User.Gender>] = []
    @Published var bio: String = ""

    var userID: String?

    var asParams: UserProfile {
        switch AppState.userType {
        case .concierge:
            return UserProfile(userID: userID, phoneNumber: phoneNumber, gender: gender.first?.underlyingValue, about: bio)
        case .resident:
            return UserProfile(userID: userID, firstName: firstName, lastName: lastName, phoneNumber: phoneNumber)
        }
    }

    var isValid: Bool { true }

    func configure(_ user: User) {
        userID = user.uid
        phoneNumber = user.phoneNumber ?? ""
        bio = user.about?.html?.string ?? ""
        firstName = user.firstName ?? ""
        lastName = user.lastName ?? ""

        if let gender = user.gender {
            self.gender = [.init(id: gender.rawValue, label: gender.rawValue, underlyingValue: gender)]
        }
    }
}

struct ConciergeProfileEditView: View {
    @ObservedObject var form = ConciergeProfileForm()

    var user: User
    var dismissBlock: ((_ requiresRefresh: Bool) -> Void)?

    var residentInputs: some View {
        VStack(spacing: 15) {
            ConciergeForm.TextInput(label: "First Name", value: $form.firstName)
            ConciergeForm.TextInput(label: "Last Name", value: $form.lastName)
            ConciergeForm.TextInput(label: "Phone Number", value: $form.phoneNumber)
        }
    }

    var conciergeInputs: some View {
        VStack(spacing: 15) {
            ConciergeForm.TextInput(label: "Phone Number", value: $form.phoneNumber)

            ConciergeForm.SelectInput<User.Gender>(
                    label: "Gender",
                    placeholder: "Select Gender",
                    options: form.genderOptions,
                    value: $form.gender)

            ConciergeForm.TextArea(label: "Bio", value: $form.bio)
        }
    }

    var body: some View {
        ScrollView(showsIndicators: false) {
            Group {
                switch AppState.userType {
                case .resident:
                    residentInputs
                case .concierge:
                    conciergeInputs
                }
            }.padding(.top, 15)
             .padding(.horizontal, 15)
        }.navigationBarItems(
                 leading:
                 Button(action: {
                     dismissBlock?(false)
                 }) {
                     Image(uiImage: Asset.Images.Concierge.icClose.image.withRenderingMode(.alwaysTemplate))
                 },
                 trailing:
                 Button(action: {
                     SVProgressHUD.show()

                     ResidentService().updateUserProfile(userID: user.uid, profile: form.asParams).done { profile in
                         SVProgressHUD.dismiss()
                         dismissBlock?(true)
                     }.catch { error in
                         Log.thisError(error)
                         SVProgressHUD.showError(withStatus: error.localizedDescription)
                     }
                 }) {
                     Text("Save").navigationSaveButton()
                 }.disabled(!form.isValid))
         .navigationBarTitle(Text("Edit Profile"), displayMode: .inline)
         .onAppear {
             form.configure(user)
         }
    }
}