//
// Created by Andrei Stoicescu on 18.11.2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import PromiseKit
import Combine
import SVProgressHUD
import SwiftUI

extension CodableCacheKey {
    static var conciergeAllUsers = CodableCacheKey(key: "concierge_all_users")
    static var conciergeSubscriptions = CodableCacheKey(key: "concierge_subscriptions")
}

class ConciergeSubscriptionListViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

    enum Sections: Int, CaseIterable {
        case header
        case items
    }

    lazy var emptyNode = EmptyNode(emptyNodeType: .generic, showAction: false)

    lazy var refreshControl = UIRefreshControl()
    var collectionNode: ASCollectionNode

    var dataSource: ObservablePaginatedDataSource<ConciergeSubscription>
    var items = [ConciergeSubscription]()
    private var subscriptions = Set<AnyCancellable>()
    var users = [User]() { didSet { users.cache(.conciergeAllUsers) } }

    override init() {
        collectionNode = .avsCollection()
        dataSource = .init { per, page in
            ConciergeService().getSubscriptionList(per: per, page: page)
        }
        dataSource.cacheKey = CodableCacheKey.conciergeSubscriptions.key
        dataSource.cacheable = true

        super.init(node: collectionNode)

        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupBindings()

        navigationItem.title = "Subscriptions"

        replaceSystemBackButton()
        showEmptyScreen(false)
        collectionNode.view.backgroundView = emptyNode.view
        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)

        collectionNode.view.addSubview(refreshControl)

        loadCached()
        refreshItems()
    }

    func openItem(_ item: ConciergeSubscription, user: User?) {
        navigationController?.pushViewController(ConciergeSubscriptionDetailsViewController(subscription: item, user: user), animated: true)
    }

    func setupBindings() {
        dataSource.$items
                .receive(on: DispatchQueue.main)
                .dropFirst()
                .sink { [weak self] items in
                    guard let self = self else { return }
                    guard self.items != items else { return }

                    self.items = items
                    self.collectionNode.reloadData()
                }.store(in: &subscriptions)

        dataSource.$hasNoItemsAfterLoading
                .receive(on: DispatchQueue.main)
                .sink { [weak self] hasNoItemsAfterLoading in
                    self?.showEmptyScreen(hasNoItemsAfterLoading)
                }.store(in: &subscriptions)

        dataSource.$showLoadingIndicator
                .receive(on: DispatchQueue.main)
                .sink { [weak self] loading in
                    guard let self = self else { return }

                    if loading {
                        if !self.refreshControl.isRefreshing {
                            SVProgressHUD.show()
                        }
                    } else {
                        SVProgressHUD.dismiss()
                        self.refreshControl.endRefreshing()
                    }
                }.store(in: &subscriptions)
    }

    func loadCached() {
        let users = [User].readCached(.conciergeAllUsers) ?? []
        let items = [ConciergeSubscription].readCached(.conciergeSubscriptions) ?? []
        if users.count > 0 && items.count > 0 {
            self.users = users
            self.items = items
            collectionNode.reloadData()
        }
    }

    @objc func refreshItems() {
        ConciergeService().getAllUsers().done { [weak self] users in
            guard let self = self else { return }
            self.users = users
            self.dataSource.loadMoreContentIfNeeded()
        }.catch { error in
            Log.thisError(error)
            SVProgressHUD.showError(withStatus: error.localizedDescription)
        }
    }

    func showEmptyScreen(_ show: Bool) {
        emptyNode.isHidden = !show
    }

    func collectionView(_ collectionView: ASCollectionView, willDisplayNodeForItemAt indexPath: IndexPath) {
        if indexPath.section == Sections.items.rawValue {
            dataSource.loadMoreContentIfNeeded(currentItem: items[indexPath.item])
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case Sections.header.rawValue:
            return items.count > 0 ? 1 : 0
        case Sections.items.rawValue:
            return items.count
        default:
            return 0
        }
    }

    func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
        Sections.allCases.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        switch indexPath.section {
        case Sections.header.rawValue:
            return { ConciergePagerListNodes.Header(title: "Resident", subject: "Package") }
        case Sections.items.rawValue:
            let subscription = items[indexPath.item]
            let user = users.first { $0.uid == subscription.userID }
            return {
                ConciergePagerListNodes.RequestCell(conciergeSubscription: subscription, user: user)
            }
        default:
            return { ASCellNode() }
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        let item = items[indexPath.item]
        openItem(item, user: users.first { $0.uid == item.userID })
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }
}
