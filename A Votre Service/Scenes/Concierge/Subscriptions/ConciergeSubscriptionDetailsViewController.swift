//
// Created by Andrei Stoicescu on 18.11.2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import SwiftUI
import Combine

class ConciergeSubscriptionDetailsViewController: ASDKViewController<ASDisplayNode>, ASCollectionDataSource, ASCollectionDelegate {
    var collectionNode: ASCollectionNode

    var subscription: ConciergeSubscription
    var user: User?

    init(subscription: ConciergeSubscription, user: User?) {
        self.subscription = subscription
        self.user = user
        self.collectionNode = .avsCollection()
        super.init(node: collectionNode)
        self.collectionNode.delegate = self
        self.collectionNode.dataSource = self
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.replaceSystemBackButton()
        self.navigationItem.title = "Subscription Details"
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        1
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        { ConciergePagerListNodes.SummaryCellNode(subscription: self.subscription, user: self.user) }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        ASSizeRange.fullWidth(collectionNode: collectionNode)
    }
}
