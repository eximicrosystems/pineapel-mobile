//
// Created by Andrei Stoicescu on 07/10/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import SwiftUI
import SwiftyUserDefaults

struct NoShiftView: View {
    @State private var loading: Bool = false

    let pub = NotificationCenter.default.publisher(for: UIApplication.willEnterForegroundNotification)

    var body: some View {
        ZStack {
            Rectangle().foregroundColor(.primaryBackground)
            VStack(spacing: 20) {
                Image(uiImage: Asset.Images.appLogo.image)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(maxHeight: 200)

                if let firstName = Defaults.user?.firstName {
                    Text("Hello, \(firstName)")
                            .font(.H2)
                            .multilineTextAlignment(.center)
                            .foregroundColor(.primaryLabel)
                }

                Text("You are currently not assigned to any shifts")
                        .conciergeFont(.H4)
                        .multilineTextAlignment(.center)
                        .foregroundColor(.secondaryLabel)
                        .padding(.horizontal)

                Button {
                    retry()
                } label: {
                    HStack {
                        if loading {
                            Text("Please Wait..")
                            ActivityIndicator(style: .medium)
                        } else {
                            Text("Tap to Retry")
                        }
                    }.foregroundColor(.white)
                     .padding(.horizontal)
                     .padding(.vertical, 10)
                     .background(.colorGreen)
                     .clipShape(Capsule())
                     .animation(.default)
                }.padding(.vertical)
                 .disabled(loading)

            }.padding(30)

        }.edgesIgnoringSafeArea(.all)
         .onReceive(pub) { _ in
             retry()
         }
    }

    func retry() {
        loading = true
        ConciergeService().getCurrentShift().done { response in
            AppState.switchToMainViewController()
        }.catch { error in
            Log.thisError(error)
        }.finally {
            loading = false
        }
    }
}

struct NoShiftView_Previews: PreviewProvider {

    static var previews: some View {
        Defaults.user = User.default
        return NoShiftView()
    }
}
