//
// Created by Andrei Stoicescu on 10.03.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import PromiseKit
import Combine
import SVProgressHUD
import SwiftUI
import DifferenceKit

class ConciergeFAQListViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

    lazy var emptyNode = EmptyNode(emptyNodeType: .generic, showAction: false)

    lazy var refreshControl = UIRefreshControl()
    var collectionNode: ASCollectionNode

    var dataSource: ObservablePaginatedDataSource<ConciergeFAQ>
    var faqs = [ConciergeFAQ]()
    private var subscriptions = Set<AnyCancellable>()

    private var searchTerm: String = "" {
        didSet {
            if searchTerm != oldValue {
                dataSource.loadMoreContentIfNeeded()
            }
        }
    }

    let searchController = UISearchController(searchResultsController: nil)

    override init() {
        collectionNode = .avsCollection()
        dataSource = .init()

        super.init(node: collectionNode)

        dataSource.paginate = { [weak self] per, page in
            guard let term = self?.searchTerm else { return .value([]) }
            return ConciergeService().getConciergeFAQ(per: per, page: page, term: term.count > 0 ? term : nil)
        }

        dataSource.dynamicCacheable = { [weak self] in
            String.isBlank(self?.searchTerm)
        }

        dataSource.cacheKey = "concierge_faq"

        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.searchController = searchController
        searchController.obscuresBackgroundDuringPresentation = false

        searchController.searchBar.searchTextField.searchPublisher().sink { [weak self] term in
            self?.searchTerm = term
        }.store(in: &subscriptions)

        setupBindings()
        navigationItem.title = "Concierge FAQ"
        replaceSystemBackButton()
        showEmptyScreen(false)
        collectionNode.view.backgroundView = emptyNode.view
        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)
        collectionNode.view.addSubview(refreshControl)
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(tapAdd))
        refreshItems()
    }

    @objc func tapAdd() {
        let view = ConciergeFAQFormView(faq: nil, dismissBlock: { [weak self] dismiss in
            self?.navigationController?.dismiss(animated: true)
            switch dismiss {
            case .created:
                self?.refreshItems()
            default:
                ()
            }
        })

        let nav = AVSNavigationController(rootViewController: UIHostingController(rootView: view))
        nav.isModalInPresentation = true
        navigationController?.present(nav, animated: true)
    }

    func reloadData() {
        let source = faqs
        let target = dataSource.items

        let stagedChangeSet = StagedChangeset(source: source, target: target)
        for changeSet in stagedChangeSet {
            faqs = changeSet.data
            collectionNode.performDiffBatchUpdates(changeSet: changeSet)
        }
    }

    func setupBindings() {
        dataSource.$items
                .receive(on: DispatchQueue.main)
                .sink { [weak self] items in
                    self?.reloadData()
                }.store(in: &subscriptions)

        dataSource.$hasNoItemsAfterLoading
                .receive(on: DispatchQueue.main)
                .sink { [weak self] hasNoItemsAfterLoading in
                    self?.showEmptyScreen(hasNoItemsAfterLoading)
                }.store(in: &subscriptions)

        dataSource.$showLoadingIndicator
                .receive(on: DispatchQueue.main)
                .sink { [weak self] loading in
                    guard let self = self else { return }

                    if loading {
                        if !self.refreshControl.isRefreshing {
                            SVProgressHUD.show()
                        }
                    } else {
                        SVProgressHUD.dismiss()
                        self.refreshControl.endRefreshing()
                    }
                }.store(in: &subscriptions)
    }

    @objc func refreshItems() {
        dataSource.loadMoreContentIfNeeded()
    }

    func showEmptyScreen(_ show: Bool) {
        emptyNode.isHidden = !show
    }

    func collectionView(_ collectionView: ASCollectionView, willDisplayNodeForItemAt indexPath: IndexPath) {
        dataSource.loadMoreContentIfNeeded(currentItem: dataSource.items[indexPath.item])
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        faqs.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let faq = faqs[indexPath.item]
        return {
            ConciergePagerListNodes.RequestCell(conciergeFAQ: faq)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        let faq = faqs[indexPath.item]
        let vc = ConciergeFAQDetailsViewController(faq: faq)
        vc.didChangeFAQ = { [weak self] action in
            guard let self = self else { return }

            switch action {
            case .delete(let faqID):
                if self.dataSource.removeItem(faq) != nil {
                    self.reloadData()
                }
                if let idx = self.faqs.firstIndex(where: { $0.id == faqID }) {
                    self.dataSource.items.remove(at: idx)
                    self.reloadData()
                }
                self.navigationController?.popToViewController(self, animated: true)
            case .update(let log):
                if let idx = self.dataSource.items.firstIndex(where: { $0.id == log.id }) {
                    self.dataSource.items[idx] = log
                    self.reloadData()
                }
            }
        }
        navigationController?.pushViewController(vc, animated: true)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }
}
