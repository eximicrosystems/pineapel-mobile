// //
// // Created by Andrei Stoicescu on 29/08/2020.
// // Copyright (c) 2020 A Votre Service LLC. All rights reserved.
// //
//
// import SwiftUI
// import FetchImage
// import SwiftyUserDefaults
//
// struct ConciergeTitleSubtitleView: View {
//     var title: String
//     var subtitle: String?
//
//     var body: some View {
//         VStack(alignment: .leading) {
//             Text(self.title)
//                     .conciergeFont(.Body)
//                     .foregroundColor(Asset.Colors.Concierge.primary)
//             Unwrap(self.subtitle) {
//                 Text($0)
//                         .conciergeFont(.Note)
//                         .foregroundColor(Asset.Colors.Concierge.darkGray)
//             }
//         }
//     }
//
// }
//
// struct ConciergeMoreMenu: View {
//     var menuItem: ConciergeMoreViewController.MoreMenu
//
//     var body: some View {
//         HStack {
//             HStack {
//                 Image(uiImage: self.menuItem.icon)
//                         .renderingMode(.original)
//                         .aspectRatio(contentMode: .fit)
//                         .frame(width: 40)
//                 ConciergeTitleSubtitleView(title: self.menuItem.title, subtitle: self.menuItem.subTitle)
//             }
//             Spacer()
//         }
//                 .modifier(CardModifier())
//     }
// }
//
// struct ConciergeProfileView: View {
//     var name: String
//     var url: URL?
//
//     var body: some View {
//         HStack {
//             AvatarView(url: url)
//                     .padding(.trailing, 10)
//             ConciergeTitleSubtitleView(title: name, subtitle: "See your profile")
//             Spacer()
//         }
//     }
// }
//
// struct AvatarView: View {
//     var url: URL?
//
//     var body: some View {
//         Circle().fill(Color.white)
//                 .frame(width: 40, height: 40)
//                 .overlay(Unwrap(url) { url in
//                     ImageView(image: FetchImage(url: url))
//                             .clipShape(Circle())
//                 })
//     }
// }
//
// struct AvatarNameView: View {
//     var url: URL?
//     var initials: String?
//
//     var body: some View {
//         ZStack {
//             Circle()
//                     .fill(Asset.Colors.Concierge.blue.sColor)
//
//             Text(initials ?? "")
//                     .conciergeFont(.Body)
//                     .foregroundColor(.white)
//
//             if let url = url {
//                 ImageView(image: FetchImage(url: url))
//                         .clipShape(Circle())
//             }
//         }.frame(width: 40, height: 40)
//     }
// }
//
// struct ConciergeMoreView: View {
//     @EnvironmentObject var userProvider: EntityProvider<User>
//
//     var body: some View {
//         VStack(alignment: .leading) {
//             Text("Menu")
//                     .conciergeFont(.H2)
//                     .foregroundColor(Asset.Colors.Concierge.primary)
//                     .padding(.leading, 15)
//
//             ScrollView {
//
//                 Unwrap(userProvider.entity.wrapped) {
//                     ConciergeProfileView(name: $0.fullName, url: $0.userPictureURL)
//                 }.padding(.horizontal, 15)
//
//                 Divider().padding(.horizontal, 15)
//
//                 VStack(alignment: .leading) {
//                     ForEach(ConciergeMoreViewController.MoreMenu.allCases, id: \.self) { item in
//                         NavigationLink(destination: DummyView()) {
//                             ConciergeMoreMenu(menuItem: item)
//                         }
//                     }
//                 }.padding(.horizontal, 15)
//             }.modifier(HideNavigationBarModifier())
//         }
//                 .background(
//                         Color.asset(Asset.Colors.Concierge.background)
//                              .edgesIgnoringSafeArea(.vertical))
//                 .onAppear {
//                     self.userProvider.loadEntity()
//                 }
//     }
// }
//
// class ConciergeMoreView_Previews: PreviewProvider {
//     static let userProvider = EntityProvider<User>(entity: .default)
//
//     static var previews: some View {
//         NavigationView {
//             ConciergeMoreView().environmentObject(userProvider)
//         }
//     }
// }
