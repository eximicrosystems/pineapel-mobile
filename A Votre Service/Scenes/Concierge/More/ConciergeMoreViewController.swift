//
// Created by Andrei Stoicescu on 25/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import SwiftyUserDefaults
import PromiseKit

class ConciergeMoreViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

    var packageListPagerDataSource = PackageListPagerDataSource()
    var amenityReservationPagerDataSource = AmenityReservationsPagerDataSource()

    enum Sections: Int, CaseIterable {
        case profile
        case menu
    }

    enum MoreMenu: Int {
        case buildingInformation
        case residents
        case feedback
        case amenityManagement
        case amenityReservations
        case aLaCarte
        case visitors
        case packageDelivery
        case subscriptions
        case sentNotifications
        case incidentReport
        case activityLogs
        case reports
        case contacts
        case faq

        var icon: UIImage {
            switch self {

            case .buildingInformation:
                return Asset.Images.Concierge.icBuilding.image
            case .residents:
                return Asset.Images.Concierge.icResidents.image
            case .feedback:
                return Asset.Images.Concierge.icChatNav.image
            case .amenityManagement:
                return Asset.Images.Concierge.icAmenity.image
            case .amenityReservations:
                return Asset.Images.Concierge.icReservations.image
            case .visitors:
                return Asset.Images.Concierge.icVisitors.image
            case .packageDelivery:
                return Asset.Images.Concierge.icPackage.image
            case .incidentReport:
                return Asset.Images.Concierge.icAssignmentLg.image
            case .sentNotifications:
                return Asset.Images.Concierge.icPaperPlane.image
            case .activityLogs:
                return Asset.Images.Concierge.icInfo.image
            case .subscriptions:
                return Asset.Images.icCartPlus.image
            case .aLaCarte:
                return Asset.Images.Concierge.icAlacarte.image
            case .reports:
                return Asset.Images.Concierge.icAssignmentLg.image
            case .contacts:
                return Asset.Images.Concierge.icResidents.image
            case .faq:
                return Asset.Images.Concierge.icQuestionCircle.image
            }
        }

        var title: String {
            switch self {
            case .buildingInformation:
                return "Building Information"
            case .residents:
                return "Residents"
            case .feedback:
                return "Feedback"
            case .amenityManagement:
                return "Amenity Management"
            case .amenityReservations:
                return "Amenity Reservations"
            case .visitors:
                return "Visitors"
            case .packageDelivery:
                return "Package Delivery"
            case .incidentReport:
                return "Incident Reports"
            case .sentNotifications:
                return "Sent Notifications"
            case .activityLogs:
                return "Activity Logs"
            case .subscriptions:
                return "Subscriptions"
            case .aLaCarte:
                return "A La Carte"
            case .reports:
                return "Reports"
            case .contacts:
                return "Contacts"
            case .faq:
                return "Concierge FAQ"
            }
        }

        var subTitle: String? {
            switch self {
            case .buildingInformation:
                return "See the building info you were assigned"
            case .residents:
                return "See list of residents"
            case .feedback:
                return "See feedback from residents"
            case .amenityManagement:
                return "Manage building amenities"
            case .amenityReservations:
                return "Manage amenity reservations"
            case .visitors:
                return "Manage Visitors"
            case .packageDelivery:
                return "Manage Package Deliveries"
            case .incidentReport:
                return "Manage Incident Reports"
            case .sentNotifications:
                return "Manage Resident Notifications"
            case .activityLogs:
                return "View activity logs"
            case .subscriptions:
                return "View Resident Subscription Details"
            case .aLaCarte:
                return "Manage A La Carte Products"
            case .reports:
                return "View different reports"
            case .contacts:
                return "View contacts"
            case .faq:
                return "View concierge FAQ"
            }
        }
    }

    var collectionNode: ASCollectionNode
    var user: User?
    var menuItems = [MoreMenu]()

    override init() {
        collectionNode = .avsCollection()
        collectionNode.contentInset = .collectionBottomInset
        collectionNode.backgroundColor = .clear

        let navigationNode = ConciergeGenericNodes.LargeNavigationNode(title: "Menu", rightButtonType: .logout)

        let mainNode = ASDisplayNode()
        mainNode.backgroundColor = AVSColor.primaryBackground.color
        mainNode.automaticallyManagesSubnodes = true

        super.init(node: mainNode)

        mainNode.layoutSpecBlock = { node, range in
            self.collectionNode.style.flexGrow = 1
            return [navigationNode, self.collectionNode].vStacked()
        }

        navigationNode.didTapRightButton = { [weak self] in
            self?.confirmLogout()
        }

        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    func rebuildMenu() {
        if AppState.conciergeShift?.conciergeAdmin == true {
            menuItems = [
                .buildingInformation,
                .faq,
                .residents,
                .feedback,
                .amenityManagement,
                .amenityReservations,
                .aLaCarte,
                .visitors,
                .packageDelivery,
                .subscriptions,
                .sentNotifications,
                .incidentReport,
                .activityLogs,
                .reports,
                .contacts
            ]
        } else {
            menuItems = [
                .buildingInformation,
                .faq,
                .residents,

                .amenityManagement,
                .amenityReservations,
                .visitors,
                .packageDelivery,
                .subscriptions,
                .sentNotifications,
                .incidentReport,
                .activityLogs,
                .contacts
            ]
        }

        collectionNode.reloadData()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()
        loadUser()
        rebuildMenu()
    }

//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        navigationController?.setNavigationBarHidden(true, animated: false)
//    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        navigationController?.setNavigationBarHidden(false, animated: true)
//    }

    func loadUser() {
        if let user = Defaults.user {
            Log.this(user)
            self.user = user
            collectionNode.reloadData()
            return
        }

        SVProgressHUD.show()

        ResidentService().getUser().done { [weak self] user in
            SVProgressHUD.dismiss()
            guard let self = self else { return }
            self.user = user
            self.collectionNode.reloadData()
        }.catch { error in
            SVProgressHUD.showError(withStatus: error.localizedDescription)
            Log.thisError(error)
        }
    }

    func confirmLogout() {
        let alertController = UIAlertController(title: nil, message: L10n.MoreScreen.Actions.Logout.confirm, preferredStyle: .alert)

        alertController.addAction(UIAlertAction(title: L10n.MoreScreen.Actions.Logout.ok, style: .default, handler: { action in
            AppState.logout(userAction: true)
        }))

        alertController.addAction(UIAlertAction(title: L10n.MoreScreen.Actions.Logout.cancel, style: .cancel, handler: nil))

        present(alertController, animated: true, completion: nil)
    }

    func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
        Sections.allCases.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        guard let section = Sections(rawValue: section) else { return 0 }
        switch section {
        case .profile:
            return user != nil ? 1 : 0
        case .menu:
            return menuItems.count
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let blankCell = { ASCellNode() }
        guard let section = Sections(rawValue: indexPath.section) else { return blankCell }

        switch section {
        case .profile:
            guard let user = user else { return blankCell }
            return { ConciergeMoreNodes.ProfileCell(user: user) }
        case .menu:
            let menu = menuItems[indexPath.item]
            return { ConciergeGenericNodes.MenuCell(icon: menu.icon.withRenderingMode(.alwaysTemplate), title: menu.title, subtitle: menu.subTitle, menuStyle: .resident) }
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        guard let section = Sections(rawValue: indexPath.section) else { return }

        switch section {
        case .profile:
            guard let user = user else { return }
            openViewController(ConciergeProfileViewController(user: user))
        case .menu:
            let item = menuItems[indexPath.item]
            switch item {
            case .buildingInformation:
                openViewController(ConciergeBuildingListViewController())
            case .residents:
                openViewController(ResidentListViewController())
            case .visitors:
                openViewController(VisitorListViewController())
            case .packageDelivery:
                openViewController(PackageDeliveryListPagerViewController(dataSource: packageListPagerDataSource))
            case .amenityManagement:
                openViewController(AmenityManagementListViewController())
            case .amenityReservations:
                openViewController(AmenityReservationsPagerViewController(dataSource: amenityReservationPagerDataSource))
            case .incidentReport:
                openViewController(IncidentReportListViewController())
            case .sentNotifications:
                openViewController(ConciergeNotificationsListViewController())
            case .activityLogs:
                openViewController(ActivityLogListViewController())
            case .subscriptions:
                openViewController(ConciergeSubscriptionListViewController())
            case .feedback:
                openViewController(ConciergeFeedbackViewController())
            case .aLaCarte:
                openViewController(ConciergeALaCarteCategoryListViewController())
            case .reports:
                openViewController(ReportCategoryListViewController())
            case .contacts:
                openViewController(ContactListViewController())
            case .faq:
                openViewController(ConciergeFAQListViewController())
            }
        }
    }

    // TODO: refactor this. used in dashboard
    func externalOpenAmenityReservations() {
        navigationController?.popToRootViewController(animated: false)
        openViewController(AmenityReservationsPagerViewController(dataSource: amenityReservationPagerDataSource))
    }

    func openViewController(_ viewController: UIViewController) {
        show(viewController, sender: self)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }

}

