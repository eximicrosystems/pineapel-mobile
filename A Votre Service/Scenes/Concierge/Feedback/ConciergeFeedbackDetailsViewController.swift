//
// Created by Andrei Stoicescu on 24.02.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import Combine
import PromiseKit
import SwiftyUserDefaults
import SwiftUI

class ConciergeFeedbackDetailsViewController: ASDKViewController<ASDisplayNode>, ASCollectionDataSource, ASCollectionDelegate {
    var collectionNode: ASCollectionNode

    var feedback: ConciergeFeedback

    init(feedback: ConciergeFeedback) {
        self.feedback = feedback
        collectionNode = .avsCollection()
        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()
        navigationItem.title = "Feedback"
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        1
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let feedback = self.feedback


        return {
            let summary = ConciergePagerListNodes.SummaryNode(config: .matrix(
                    [
                        [.init(title: "Details", subtitle: feedback.displayAbout)],
                        [
                            .init(title: "Rating", subtitle: feedback.displayRating),
                            .init(title: "Date", subtitle: feedback.created)
                        ],
                        [.init(title: "Comments", subtitle: feedback.comment)],
                    ]))

            let contentNode = ConciergeGenericNodes.BioProfileContent(title: feedback.fullName, subtitle: feedback.displayApartment, bodyNode: summary)

            let cell = ConciergeGenericNodes.BioProfileCell(url: feedback.userPicture, imageSize: 150, contentNode: contentNode)

            return cell
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        ASSizeRange.fullWidth(collectionNode: collectionNode)
    }
}
