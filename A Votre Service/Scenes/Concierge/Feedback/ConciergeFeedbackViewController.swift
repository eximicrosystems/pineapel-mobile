//
// Created by Andrei Stoicescu on 24.02.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import PromiseKit
import SwiftyUserDefaults
import Combine
import DifferenceKit

class ConciergeFeedbackViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {
    lazy var emptyNode = EmptyNode(emptyNodeType: .generic, showAction: false)

    var collectionNode: ASCollectionNode

    var dataSource = ObservablePaginatedDataSource<ConciergeFeedback>()
    private var subscriptions = Set<AnyCancellable>()
    var items = [ConciergeFeedback]()

    lazy var refreshControl = UIRefreshControl()

    override init() {
        collectionNode = .avsCollection()
        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self

        dataSource.cacheable = true
        dataSource.cacheKey = "concierge_feedback"
        dataSource.paginate = { per, page in
            ConciergeService().getConciergeFeedback(per: per, page: page)
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupBindings()
        showEmptyScreen(false)
        replaceSystemBackButton()
        collectionNode.view.backgroundView = emptyNode.view
        navigationItem.title = "Feedback"
        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)
        collectionNode.view.addSubview(refreshControl)
        refreshItems()
    }

    @objc func refreshItems() {
        dataSource.loadMoreContentIfNeeded()
    }

    func setupBindings() {
        dataSource.$items
                .receive(on: DispatchQueue.main)
                .sink { [weak self] items in
                    self?.reloadData()
                }.store(in: &subscriptions)

        dataSource.$hasNoItemsAfterLoading
                .receive(on: DispatchQueue.main)
                .sink { [weak self] hasNoItemsAfterLoading in
                    self?.showEmptyScreen(hasNoItemsAfterLoading)
                }.store(in: &subscriptions)

        dataSource.$showLoadingIndicator
                .receive(on: DispatchQueue.main)
                .sink { [weak self] loading in
                    guard let self = self else { return }

                    if loading {
                        if !self.refreshControl.isRefreshing {
                            SVProgressHUD.show()
                        }
                    } else {
                        SVProgressHUD.dismiss()
                        self.refreshControl.endRefreshing()
                    }
                }.store(in: &subscriptions)
    }

    func showEmptyScreen(_ show: Bool) {
        emptyNode.isHidden = !show
    }

    func reloadData() {
        let source = items
        let target = dataSource.items

        let stagedChangeSet = StagedChangeset(source: source, target: target)
        for changeSet in stagedChangeSet {
            items = changeSet.data
            collectionNode.performDiffBatchUpdates(changeSet: changeSet)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        items.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let feedback = items[indexPath.item]
        let title = [feedback.fullName, feedback.displayApartment].compactMap { $0 }.joined(separator: "\n")
        let subject = [feedback.displayRating, feedback.created].joined(separator: "\n")

        return {
            ConciergePagerListNodes.RequestCell(title: title, subject: subject, imageUrl: feedback.userPicture)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        let feedback = dataSource.items[indexPath.item]
        navigationController?.pushViewController(ConciergeFeedbackDetailsViewController(feedback: feedback), animated: true)
    }

    func collectionView(_ collectionView: ASCollectionView, willDisplayNodeForItemAt indexPath: IndexPath) {
        dataSource.loadMoreContentIfNeeded(currentItem: dataSource.items[indexPath.item])
    }
}
