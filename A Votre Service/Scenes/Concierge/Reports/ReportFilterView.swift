//
// Created by Andrei Stoicescu on 03.03.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//

import SwiftUI

struct ReportFilterParams: Encodable {
    var startDate: Date
    var endDate: Date?
    var amenityId: String?
    var categoryId: String?

    enum CodingKeys: String, CodingKey {
        case startDate = "start_date"
        case endDate = "end_date"
        case amenityId = "amenity"
        case categoryId = "category"
    }

    static var `default`: ReportFilterParams {
        .init(startDate: 1.months.earlier, endDate: nil, amenityId: nil, categoryId: nil)
    }

    func toParams(_ reportType: ConciergeReport.ReportType) -> [String: Any] {
        var params = ["start_date": startDate.avsDateFormatted]

        if let endDate = endDate {
            params["end_date"] = endDate.avsDateFormatted
        }

        if reportType.allowsAmenitySelection, let amenityId = amenityId {
            params["amenity"] = amenityId
        }

        if reportType.allowsCategorySelection, let categoryId = categoryId {
            params["category"] = categoryId
        }

        return params
    }
}

class ReportFilterForm: ObservableObject {
    @Published var startDate: Date? = nil
    @Published var endDate: Date? = nil

    @Published var amenity: [ConciergeForm.Option<Amenity>] = [] {
        didSet {
            if amenity.count > 0 {
                category = []
            }
        }
    }
    @Published var amenityList: [ConciergeForm.Option<Amenity>] = []

    @Published var category: [ConciergeForm.Option<ConciergeALaCarteCategory>] = [] {
        didSet {
            if category.count > 0 {
                amenity = []
            }
        }
    }
    @Published var categoryList: [ConciergeForm.Option<ConciergeALaCarteCategory>] = []

    var reportType: ConciergeReport.ReportType
    var filterDataStore: ReportCategoryListViewController.FilterDataStore

    init(reportType: ConciergeReport.ReportType, filterDataStore: ReportCategoryListViewController.FilterDataStore) {
        self.reportType = reportType
        startDate = filterDataStore.filter.startDate
        endDate = filterDataStore.filter.endDate

        self.filterDataStore = filterDataStore

        amenityList = filterDataStore.amenities.map { amenity in
            .init(id: amenity.id, label: amenity.name ?? "", underlyingValue: amenity)
        }

        if let amenity = amenityList.first(where: { $0.id == filterDataStore.filter.amenityId }) {
            self.amenity = [amenity]
        }

        categoryList = filterDataStore.categories.map { category in
            .init(id: category.id, label: category.name, underlyingValue: category)
        }

        if let category = categoryList.first(where: { $0.id == filterDataStore.filter.categoryId }) {
            self.category = [category]
        }
    }

    var asParams: ReportFilterParams? {
        guard let startDate = startDate else { return nil }

        return ReportFilterParams(startDate: startDate,
                                   endDate: endDate,
                                   amenityId: reportType.allowsAmenitySelection ? amenity.first?.id : nil,
                                   categoryId: reportType.allowsCategorySelection ? category.first?.id : nil)
    }

    var valid: Bool {
        startDate != nil
    }
}

struct ReportFilterView: View {
    enum DismissMode {
        case cancel
        case filter(ReportFilterParams)
    }

    @ObservedObject var form: ReportFilterForm
    var dismissBlock: (DismissMode) -> Void

    var body: some View {
        ScrollView(showsIndicators: false) {
            VStack(spacing: 20) {
                ConciergeForm.DateField(label: "Date Range From",
                                        formatter: .avsDateFormatter,
                                        value: $form.startDate,
                                        config: .dateOnly)

                VStack(alignment: .leading, spacing: 5) {
                    ConciergeForm.DateField(label: "Date Range To",
                                            formatter: .avsDateFormatter,
                                            value: $form.endDate,
                                            config: .dateOnly)

                    if form.endDate != nil {
                        Button(action: {
                            form.endDate = nil
                        }, label: {
                            Text("Clear").font(.noteMedium).foregroundColor(.primaryLabel)
                        })
                    }
                }

                ScrollView(.horizontal) {
                    HStack {
                        Text("Quick Selection:").foregroundColor(.secondaryLabel)

                        Button(action: {
                            form.startDate = Date()
                            form.endDate = Date()
                        }, label: {
                            Text("Today")
                        })

                        Button(action: {
                            form.startDate = 1.days.earlier
                            form.endDate = 1.days.earlier
                        }, label: {
                            Text("Yesterday")
                        })

                        Button(action: {
                            form.startDate = Date().start(of: .month)
                            form.endDate = Date().end(of: .month)
                        }, label: {
                            Text("This Month")
                        })

                        Button(action: {
                            form.startDate = 1.months.earlier.start(of: .month)
                            form.endDate = 1.months.earlier.end(of: .month)
                        }, label: {
                            Text("Last Month")
                        })

                    }.font(.noteMedium).foregroundColor(.primaryLabel)
                }

                if form.reportType.allowsAmenitySelection {
                    ConciergeForm.SelectInput(
                            label: "Amenity",
                            placeholder: "Select Amenity",
                            options: form.amenityList,
                            value: $form.amenity)
                }

                if form.reportType.allowsCategorySelection {
                    ConciergeForm.SelectInput(
                            label: "Category",
                            placeholder: "Select Category",
                            options: form.categoryList,
                            value: $form.category)
                }
            }.padding(.top, 20)
             .padding(.horizontal, 20)
        }.fullViewBackgroundColor()
         .navigationBarItems(
                 leading: Button(action: {
                     dismissBlock(.cancel)
                 }) {
                     Image(uiImage: Asset.Images.Concierge.icClose.image.withRenderingMode(.alwaysTemplate))
                 },

                 trailing: Button(action: {
                     if let params = form.asParams {
                         dismissBlock(.filter(params))
                     }
                 }) {
                     Text("Apply").navigationSaveButton()
                 }.disabled(!form.valid)
         )
         .navigationBarTitle(Text("Filter Reports"), displayMode: .inline)
    }
}