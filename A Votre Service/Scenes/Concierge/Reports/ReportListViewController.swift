//
// Created by Andrei Stoicescu on 01.03.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import PromiseKit
import Combine
import SVProgressHUD
import SwiftUI
import SafariServices
import DifferenceKit

struct ConciergePerformanceReport: Decodable {
    struct Performance: Decodable {
        var name: String
        var amount: String

        enum CodingKeys: String, CodingKey {
            case name, amount
        }

        init(from decoder: Decoder) throws {
            let map = try decoder.container(keyedBy: CodingKeys.self)
            name = try map.decode(.name)
            if let amountInt = try? map.decode(Int.self, forKey: .amount) {
                amount = String(amountInt)
            } else {
                amount = (try? map.decodeIfPresent(.amount)) ?? ""
            }
        }
    }

    enum CodingKeys: String, CodingKey {
        case pdfUrl = "downloadPdfUrl"
        case xlsUrl = "downloadXlsxUrl"
        case rows
    }

    let pdfUrl: URL?
    let xlsUrl: URL?
    let rows: [Performance]

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        pdfUrl = try? map.decodeIfPresent(.pdfUrl)
        xlsUrl = try? map.decodeIfPresent(.xlsUrl)
        rows = try map.decode(.rows)
    }
}

struct ConciergeReport: Codable {

    struct ModelRow: Identifiable, Hashable, Differentiable {
        var id = UUID()
        let items: [ReportItem]
        let fields: [ReportField]

        func getItem(_ property: ReportField) -> ReportItem? {
            guard let idx = fields.firstIndex(of: property) else { return nil }
            guard let data = items[safe: idx] else { return nil }
            return data
        }

        subscript(_ property: ReportField) -> ReportItem? {
            get { getItem(property) }
        }

        static func build(rows: [Row], fields: [ReportField]) -> [ModelRow] {
            rows.map { row in
                ModelRow(items: row.header.enumerated().map { idx, element in
                    ReportItem(name: element, value: row.data[safe: idx] ?? "")
                }, fields: fields)
            }
        }

        func hash(into hasher: inout Hasher) { hasher.combine(id) }

        var differenceIdentifier: Int { hashValue }
    }

    enum ReportField: String {
        case firstName
        case lastName
        case floor
        case apartment
        case gender
        case phone
        case job
        case about
        case email
        case created
        case resident
        case date
        case body
        case concierge
        case rating
        case type
        case author
        case shift
        case subject
        case importance
        case recipient
        case from
        case receivedBy
        case guestName
        case reason
        case creationDate
        case amenity
        case reservationStart
        case reservationEnd
        case note
        case name
        case description
        case category
        case price
        case tax
        case inventoryCount
        case orderId
        case productName
        case unitPrice
        case quantity
        case priceWithoutTax
        case taxAmount
        case priceWithTax
        case lineItemType
        case amount
    }

    struct ReportItem: Hashable {
        let name: String
        let value: String
    }

    struct Row: Codable {
        let header: [String]
        let data: [String]
    }

    enum ReportType: String, Codable {
        case residents
        case feedback
        case activityLog
        case incidents
        case requests
        case packages
        case visitors
        case reservations
        case inventory
        case orders
        case performance

        var title: String {
            switch self {

            case .residents:
                return "Residents"
            case .feedback:
                return "Feedback"
            case .activityLog:
                return "Activity Log"
            case .incidents:
                return "Incidents"
            case .requests:
                return "Requests"
            case .packages:
                return "Packages"
            case .visitors:
                return "Visitors"
            case .reservations:
                return "Reservations"
            case .inventory:
                return "Inventory"
            case .orders:
                return "Orders"
            case .performance:
                return "Performance"
            }
        }

        var fields: [ReportField] {
            switch self {

            case .residents:
                return [.firstName, .lastName, .floor, .apartment, .gender, .phone, .job, .about, .email, .created]
            case .feedback:
                return [.resident, .apartment, .date, .body, .concierge, .rating, .type]
            case .activityLog:
                return [.date, .author, .shift, .subject, .body, .importance]
            case .incidents:
                return [.date, .author, .shift, .subject, .body]
            case .requests:
                return [.date, .author, .apartment, .subject, .body]
            case .packages:
                return [.date, .recipient, .apartment, .subject, .from, .receivedBy]
            case .visitors:
                return [.guestName, .apartment, .reason, .date]
            case .reservations:
                return [.creationDate, .amenity, .reservationStart, .reservationEnd, .resident, .apartment, .note]
            case .inventory:
                return [.name, .description, .category, .price, .tax, .inventoryCount]
            case .orders:
                return [.date, .orderId, .resident, .productName, .unitPrice, .quantity, .priceWithoutTax, .taxAmount, .priceWithTax, .lineItemType]
            case .performance:
                return [.name, .amount]
            }
        }

        var allowsAmenitySelection: Bool {
            self == .reservations || self == .orders
        }

        var allowsCategorySelection: Bool {
            self == .orders
        }

        struct CustomReportField {
            var field: ReportField
            var header: Bool

            static func header(_ field: ReportField) -> CustomReportField {
                CustomReportField(field: field, header: true)
            }

            static func field(_ field: ReportField) -> CustomReportField {
                CustomReportField(field: field, header: false)
            }
        }

        struct FieldGroup {
            var left: [[CustomReportField]]
            var right: [[CustomReportField]]

            func customReportString(_ list: [[CustomReportField]], row: ModelRow) -> String {
                list.map { horizontalArray -> String in
                    horizontalArray.compactMap { field -> String? in
                        let item = row.getItem(field.field)
                        if field.header {
                            return join([item?.name, item?.value ?? " - "], ": ")
                        } else {
                            return item?.value
                        }
                    }.joined(separator: " ")
                }.joined(separator: "\n")
            }

            func join(_ strings: [String?], _ separator: String = " ") -> String? {
                let str = strings.compactMap { $0 }.joined(separator: separator)
                guard !String.isBlank(str) else { return nil }
                return str
            }
        }

        var reportListFieldConfiguration: FieldGroup {
            switch self {
            case .residents:
                return .init(left: [
                    [.field(.firstName), .field(.lastName)],
                    [.header(.floor), .header(.apartment)]
                ], right: [
                    [.field(.created)]
                ])
            case .feedback:
                return .init(left: [
                    [.field(.resident)],
                    [.header(.apartment)]
                ], right: [
                    [.field(.date)],
                    [.header(.rating), .header(.type)]
                ])
            case .activityLog, .incidents:
                return .init(left: [
                    [.field(.author)],
                    [.field(.shift)]
                ], right: [
                    [.field(.date)],
                    [.field(.subject)]
                ])
            case .requests:
                return .init(left: [
                    [.field(.author)],
                    [.header(.apartment)]
                ], right: [
                    [.field(.date)],
                    [.field(.subject)]
                ])
            case .packages:
                return .init(left: [
                    [.header(.recipient)],
                    [.header(.apartment)]
                ], right: [
                    [.header(.receivedBy)],
                    [.field(.date)]
                ])
            case .visitors:
                return .init(left: [
                    [.field(.guestName)],
                    [.header(.apartment)]
                ], right: [
                    [.field(.date)],
                    [.header(.reason)]
                ])
            case .reservations:
                return .init(left: [
                    [.field(.resident)],
                    [.header(.apartment)],
                ], right: [
                    [.field(.amenity)],
                    [.header(.reservationStart)]
                ])
            case .inventory:
                return .init(left: [
                    [.field(.name)],
                    [.header(.category)]
                ], right: [
                    [.header(.price)],
                    [.header(.inventoryCount)]
                ])
            case .orders:
                return .init(left: [
                    [.header(.resident)],
                    [.field(.productName)]
                ], right: [
                    [.field(.date)],
                    [.header(.orderId)],
                    [.header(.priceWithTax)]
                ])
            case .performance:
                // There's a custom presenter for performance reports
                return .init(left: [
                    [.field(.name)]
                ], right: [
                    [.field(.amount)]
                ])
            }
        }
    }

    enum CodingKeys: String, CodingKey {
        case pdfUrl = "downloadPdfUrl"
        case xlsUrl = "downloadXlsxUrl"
        case rows
    }

    let pdfUrl: URL?
    let xlsUrl: URL?

    let rows: [Row]

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        pdfUrl = try? map.decodeIfPresent(.pdfUrl)
        xlsUrl = try? map.decodeIfPresent(.xlsUrl)

        var rows = [Row]()
        var rowsContainer = try map.nestedUnkeyedContainer(forKey: .rows)

        var headers = [String]()

        while !rowsContainer.isAtEnd {
            let row = try rowsContainer.decode([String?].self).map { s -> String in
                if let s = s { return s }
                return ""
            }

            if headers.count > 0 {
                rows.append(Row(header: headers, data: row))
            } else {
                headers = row
            }
        }

        self.rows = rows
    }
}

class ReportListViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

    enum Sections: Int, CaseIterable, Differentiable {
        case filter
        case reports
    }

    lazy var emptyNode = EmptyNode(emptyNodeType: .generic, showAction: false)

    lazy var refreshControl = UIRefreshControl()
    var collectionNode: ASCollectionNode

    var dataSource = ObservablePaginatedDataSource<ConciergeReport.ModelRow>()

    private var subscriptions = Set<AnyCancellable>()

    var reportType: ConciergeReport.ReportType
    var filterDataStore: ReportCategoryListViewController.FilterDataStore
    var filterPills = [ConciergePagerListNodes.PillTag]()

    private var sections = [ArraySection<Sections, AnyDifferentiable>]()

    var currentPdfUrl: URL? {
        didSet { buildNavigationBarItems() }
    }
    var currentXlsUrl: URL? {
        didSet { buildNavigationBarItems() }
    }

    init(reportType: ConciergeReport.ReportType, filterDataStore: ReportCategoryListViewController.FilterDataStore) {
        self.reportType = reportType
        self.filterDataStore = filterDataStore

        collectionNode = .avsCollection()

        super.init(node: collectionNode)

        dataSource.paginate = { per, page in
            if reportType == .performance {
                return ConciergeService().getPerformanceReports(filterDataStore.filter).get { [weak self] report in
                    if page == 0 {
                        self?.currentPdfUrl = report.pdfUrl
                        self?.currentXlsUrl = report.xlsUrl
                    }
                }.map { report in
                    ConciergeReport.ModelRow.build(rows: report.rows.map { .init(header: ["Name", "Amount"], data: [$0.name, $0.amount]) },
                                                   fields: reportType.fields)
                }
            } else {
                return ConciergeService().getReports(per: per, page: page, reportType: reportType, otherParams: filterDataStore.filter).get { [weak self] report in
                    if page == 0 {
                        self?.currentPdfUrl = report.pdfUrl
                        self?.currentXlsUrl = report.xlsUrl
                    }
                }.map { report in
                    ConciergeReport.ModelRow.build(rows: report.rows, fields: reportType.fields)
                }
            }
        }

        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupBindings()
        navigationItem.title = reportType.title
        replaceSystemBackButton()
        showEmptyScreen(false)
        collectionNode.view.backgroundView = emptyNode.view
        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)
        collectionNode.view.addSubview(refreshControl)
        buildNavigationBarItems()
        refreshItems()
    }

    func reloadData() {
        filterPills = buildFilterPills()
        let filterPillsHash = filterPills.map(\.label).joined(separator: "").hashValue
        let filterSectionItems = filterPills.count > 0 ? [AnyDifferentiable(filterPillsHash)] : []

        let reportItems = dataSource.items.map { AnyDifferentiable($0) }

        let target: [ArraySection<Sections, AnyDifferentiable>] = [
            ArraySection(model: .filter, elements: filterSectionItems),
            ArraySection(model: .reports, elements: reportItems)
        ]

        let stagedChangeSet = StagedChangeset(source: sections, target: target)
        for changeSet in stagedChangeSet {
            sections = changeSet.data
            collectionNode.performDiffBatchUpdates(changeSet: changeSet)
        }
    }

    func buildNavigationBarItems() {
        var buttonItems = [UIBarButtonItem(image: Asset.Images.Concierge.icFilter.image.withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(tapFilterButton))]

        if currentXlsUrl != nil || currentPdfUrl != nil {
            buttonItems.append(
                    UIBarButtonItem(image: Asset.Images.Concierge.icFileDownload.image.withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(presentDownloadMenu))
            )
        }

        navigationItem.rightBarButtonItems = buttonItems
    }

    @objc func presentDownloadMenu() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        if let url = currentPdfUrl {
            alertController.addAction(UIAlertAction(title: "Download PDF", style: .default) { [weak self] _ in
                guard let self = self else { return }
                self.navigationController?.present(SFSafariViewController(url: url, configuration: SFSafariViewController.Configuration()), animated: true)
            })
        }

        if let url = currentXlsUrl {
            alertController.addAction(UIAlertAction(title: "Download XLSX", style: .default) { [weak self] _ in
                guard let self = self else { return }
                self.navigationController?.present(SFSafariViewController(url: url, configuration: SFSafariViewController.Configuration()), animated: true)
            })
        }

        alertController.addAction(UIAlertAction(title: L10n.General.cancel, style: .cancel, handler: nil))

        alertController.fixIpadIfNeeded(view: view)
        present(alertController, animated: true)
    }

    @objc func tapFilterButton() {

        let form = ReportFilterForm(reportType: reportType, filterDataStore: filterDataStore)

        let view = ReportFilterView(form: form, dismissBlock: { [weak self] dismiss in
            guard let self = self else { return }
            self.navigationController?.dismiss(animated: true)
            switch dismiss {
            case .filter(let params):
                self.filterDataStore.filter = params
                self.refreshItems()
            default:
                ()
            }
        })

        let nav = AVSNavigationController(rootViewController: UIHostingController(rootView: view))
        nav.isModalInPresentation = true
        navigationController?.present(nav, animated: true)
    }

    func setupBindings() {
        dataSource.$items
                .receive(on: DispatchQueue.main)
                .sink { [weak self] items in
                    self?.reloadData()
                    // self?.collectionNode.reloadData()
                }.store(in: &subscriptions)

        dataSource.$hasNoItemsAfterLoading
                .receive(on: DispatchQueue.main)
                .sink { [weak self] hasNoItemsAfterLoading in
                    self?.showEmptyScreen(hasNoItemsAfterLoading)
                }.store(in: &subscriptions)

        dataSource.$showLoadingIndicator
                .receive(on: DispatchQueue.main)
                .sink { [weak self] loading in
                    guard let self = self else { return }

                    if loading {
                        if !self.refreshControl.isRefreshing {
                            SVProgressHUD.show()
                        }
                    } else {
                        SVProgressHUD.dismiss()
                        self.refreshControl.endRefreshing()
                    }
                }.store(in: &subscriptions)
    }

    @objc func refreshItems() {
        filterPills = buildFilterPills()
        dataSource.loadMoreContentIfNeeded()
    }

    func buildFilterPills() -> [ConciergePagerListNodes.PillTag] {
        let filter = filterDataStore.filter
        var pills = [ConciergePagerListNodes.PillTag]()
        pills.append(.init(id: "start_date", label: "From: \(filter.startDate.avsDateShortMonthYearString())", allowClose: false))

        if let endDate = filter.endDate {
            pills.append(.init(id: "end_date", label: "To: \(endDate.avsDateShortMonthYearString())", allowClose: false))
        }

        if reportType.allowsAmenitySelection,
           let amenityId = filter.amenityId,
           let amenity = filterDataStore.amenities.first(where: { $0.amenityID == amenityId }) {
            pills.append(.init(id: "amenity", label: amenity.name ?? "", allowClose: false))
        }

        if reportType.allowsCategorySelection,
           let categoryId = filter.categoryId,
           let category = filterDataStore.categories.first(where: { $0.categoryId == categoryId }) {
            pills.append(.init(id: "category", label: category.name, allowClose: false))
        }

        return pills
    }

    func showEmptyScreen(_ show: Bool) {
        emptyNode.isHidden = !show
    }

    func collectionView(_ collectionView: ASCollectionView, willDisplayNodeForItemAt indexPath: IndexPath) {
        guard indexPath.section == Sections.reports.rawValue else { return }
        dataSource.loadMoreContentIfNeeded(currentItem: dataSource.items[indexPath.item])
    }

    func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
        sections.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        sections[section].elements.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        guard let section = Sections(rawValue: indexPath.section) else { return { ASCellNode() } }

        switch section {

        case .filter:
            let pills = filterPills
            return {
                let cell = ConciergePagerListNodes.PillTagCollection(items: pills)
                cell.onTap = { [weak self] pill in
                    self?.tapFilterButton()
                }
                return cell
            }
        case .reports:
            let row = dataSource.items[indexPath.item]
            let fieldGroup = reportType.reportListFieldConfiguration
            let leftSideString = fieldGroup.customReportString(fieldGroup.left, row: row)
            let rightSideString = fieldGroup.customReportString(fieldGroup.right, row: row)

            if reportType == .performance {
                return {
                    ConciergeDashboardNodes.InfoCard(title: leftSideString, value: rightSideString)
                }
            } else {
                return {
                    let cell = ConciergePagerListNodes.RequestCell(title: leftSideString, subject: rightSideString, imageUrl: nil)
                    cell.hidesAvatar = true
                    return cell
                }
            }
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        guard let section = Sections(rawValue: indexPath.section) else { return }

        switch section {
        case .filter:
            ()
        case .reports:
            let item = dataSource.items[indexPath.item]
            let vc = ReportDetailViewController(item: item)
            navigationController?.pushViewController(vc, animated: true)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }
}
