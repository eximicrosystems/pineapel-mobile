//
// Created by Andrei Stoicescu on 27/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit

struct RunOfShowNodes {

    class MenuCell: ASCellNode {

        enum Selection {
            case none
            case selected
            case unselected
        }

        var labelsNode: ConciergeGenericNodes.TitleSubtitleNode

        var iconNode: ASImageNode?
        var cardNode: CardNode
        var selection: Selection = .none
        var line = ASDisplayNode()

        init(selection: Selection, title: String, subtitle: String?, progress: CGFloat = 0, progressBackground: UIColor = .clear) {
            self.selection = selection
            labelsNode = ConciergeGenericNodes.TitleSubtitleNode(title: title, subtitle: subtitle)
            cardNode = CardNode(progressPercent: progress, progressBackground: progressBackground, rounded: false, shadow: false)
            line.backgroundColor = AVSColor.divider.color

            switch selection {
            case .none:
                ()
            case .selected:
                iconNode = ASImageNode(image: Asset.Images.Concierge.icCircleSelected.image)
            case .unselected:
                iconNode = ASImageNode(image: Asset.Images.Concierge.icCircleUnselected.image)
            }

            super.init()
            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            labelsNode.style.flexGrow = 1
            labelsNode.style.flexShrink = 1
            line.style.height = ASDimensionMakeWithPoints(1)

            let card = [iconNode, labelsNode].hStacked().spaced(15).cardWrapped(cardNode)

            switch selection {
            case .none:
                return card
            default:
                return [card, line].vStacked()
            }
        }
    }

}