//
// Created by Andrei Stoicescu on 27/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import SwiftyUserDefaults
import PromiseKit

protocol RunOfShowViewControllerProtocol: class {
    func didUpdateRunOfShow(_ runOfShow: RunOfShow)
}

class RunOfShowViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

    enum Section: Int, CaseIterable {
        case header
        case items
    }

    weak var delegate: RunOfShowViewControllerProtocol?
    var collectionNode: ASCollectionNode
    lazy var refreshControl = UIRefreshControl()
    var runOfShow = RunOfShow(items: [])

    override init() {
        collectionNode = ASCollectionNode.avsCollection()

        super.init(node: collectionNode)

        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()

        navigationItem.title = "Run of Show"

        collectionNode.view.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)

        SVProgressHUD.show()
        refreshItems()
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        navigationController?.setNavigationBarHidden(true, animated: false)
//    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        navigationController?.setNavigationBarHidden(false, animated: true)
//    }

    @objc func refreshItems() {
        ConciergeService().getRunOfShow().done { items in
            self.runOfShow.items = items
            self.collectionNode.reloadData()
        }.ensure {
            self.refreshControl.endRefreshing()
            SVProgressHUD.dismiss()
        }.catch { error in
            Log.thisError(error)
        }
    }

    func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
        Section.allCases.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case Section.header.rawValue:
            return 1
        case Section.items.rawValue:
            return runOfShow.items.count
        default:
            return 0
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        switch indexPath.section {
        case Section.header.rawValue:
            return {
                RunOfShowNodes.MenuCell(selection: .none,
                                        title: "Tasks for today", 
                                        subtitle: "\(self.runOfShow.completedItems.count) of \(self.runOfShow.items.count) tasks completed",
                                        progress: CGFloat(self.runOfShow.percent),
                                        progressBackground: AVSColor.primaryBackground.color)
            }
        case Section.items.rawValue:
            let item = runOfShow.items[indexPath.item]
            return {
                RunOfShowNodes.MenuCell(selection: item.status ? .selected : .unselected, title: item.name, subtitle: "Scheduled for today")
            }
        default:
            return { ASCellNode() }
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.section {
        case Section.header.rawValue:
            ()
        case Section.items.rawValue:
            let item = runOfShow.items[indexPath.item]
            let status = toggleItem(item, indexPath: indexPath)

            ConciergeService().updateRunOfShow(RunOfShowRequest(id: item.id, status: status)).catch { error in
                Log.thisError(error)
                self.toggleItem(item, indexPath: indexPath)
            }
        default:
            ()
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        ASSizeRange.fullWidth(collectionNode: collectionNode)
    }

    @discardableResult
    func toggleItem(_ item: RunOfShowItem, indexPath: IndexPath) -> Bool {
        var item = item
        item.status.toggle()
        runOfShow.items[indexPath.item] = item
        collectionNode.reloadItems(at: [IndexPath(item: 0, section: Section.header.rawValue), indexPath])
        delegate?.didUpdateRunOfShow(runOfShow)
        return item.status
    }
}
