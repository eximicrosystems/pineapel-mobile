//
// Created by Andrei Stoicescu on 16.11.2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import SwiftUI
import Combine

class ActivityLogDetailsViewController: ASDKViewController<ASDisplayNode>, ASCollectionDataSource, ASCollectionDelegate {

    enum ActivityAction {
        case update(ActivityLog)
        case delete(_ logID: String)
    }

    var collectionNode: ASCollectionNode

    var log: ActivityLog
    var didChangeLog: ((ActivityAction) -> Void)?

    init(log: ActivityLog) {
        self.log = log
        collectionNode = ASCollectionNode.avsCollection()
        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()
        navigationItem.title = "Activity Log Details"
        if log.isEditable {
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: Asset.Images.Concierge.icCompose.image.withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(tapEdit))
        }
    }

    @objc func tapEdit() {
        let alertController = UIAlertController(title: nil, message: "Options", preferredStyle: .actionSheet)
        let deleteAction = UIAlertAction(title: "Delete Log", style: .destructive) { [weak self] _ in
            guard let logID = self?.log.id else { return }
            self?.confirmDelete(logID)
        }

        let editAction = UIAlertAction(title: "Edit Log", style: .default) { [weak self] _ in
            guard let self = self else { return }

            let view = ActivityLogFormView(activityLog: self.log, dismissBlock: { [weak self] dismiss in
                guard let self = self else { return }
                self.navigationController?.dismiss(animated: true)

                switch dismiss {
                case .updated(let log):
                    self.log = log
                    self.didChangeLog?(.update(log))
                    self.collectionNode.reloadData()
                default:
                    ()
                }
            })

            let nav = AVSNavigationController(rootViewController: UIHostingController(rootView: view))
            nav.isModalInPresentation = true
            self.navigationController?.present(nav, animated: true)
        }

        alertController.addAction(editAction)
        alertController.addAction(deleteAction)
        alertController.addAction(UIAlertAction(title: L10n.General.close, style: .cancel, handler: nil))
        alertController.fixIpadIfNeeded(view: self.view)
        present(alertController, animated: true)
    }

    func confirmDelete(_ logID: String) {
        let deleteAlert = UIAlertController(title: nil, message: "Delete this log?", preferredStyle: .alert)

        deleteAlert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { action in
            SVProgressHUD.show()
            ConciergeService().deleteActivityLog(activityLogID: logID).done { [weak self] in
                SVProgressHUD.dismiss()
                self?.didChangeLog?(.delete(logID))
            }.catch { error in
                SVProgressHUD.showError(withStatus: error.localizedDescription)
            }
        }))

        deleteAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        present(deleteAlert, animated: true, completion: nil)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        1
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let log = self.log
        let description = self.log.body?.html?.string
        return {
            ConciergePagerListNodes.SummaryCellNode(activityLog: log, activityLogDescription: description)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        ASSizeRange.fullWidth(collectionNode: collectionNode)
    }
}
