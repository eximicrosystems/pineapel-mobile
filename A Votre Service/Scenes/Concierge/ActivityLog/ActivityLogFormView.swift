//
// Created by Andrei Stoicescu on 16.11.2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import SwiftUI
import Alamofire
import PromiseKit
import SVProgressHUD

class ActivityLogForm: ObservableObject {
    @Published var subject: String = ""
    @Published var body: String = ""
    var buildingID: String = ""
    var id: String?

    var asParams: ActivityLogRequest {
        ActivityLogRequest(id: id, body: body, subject: subject, buildingID: buildingID)
    }

    func updatedActivityLog(_ log: ActivityLog) -> ActivityLog {
        ActivityLog(id: log.id,
                    shift: log.shift,
                    createdAt: log.createdAt,
                    subject: subject,
                    body: body,
                    buildingID: log.buildingID,
                    type: log.type)
    }

    var isValid: Bool {
        !String.isBlank(subject) &&
        !String.isBlank(body) &&
        !String.isBlank(buildingID)
    }
}

struct ActivityLogFormView: View {
    enum DismissMode {
        case canceled
        case created
        case updated(ActivityLog)
    }

    @ObservedObject var form = ActivityLogForm()
    var activityLog: ActivityLog?
    var dismissBlock: (DismissMode) -> Void

    var body: some View {
        ScrollView(showsIndicators: false) {
            VStack(spacing: 20) {
                ConciergeForm.TextInput(label: "SUBJECT", value: $form.subject)
                ConciergeForm.TextArea(label: "DETAILS", value: $form.body)
            }.padding(.top, 20)
             .padding(.horizontal, 20)
        }.onAppear {

            if let activityLog = activityLog {
                form.body = activityLog.body?.html?.string ?? ""
                form.subject = activityLog.subject?.html?.string ?? ""
                form.buildingID = activityLog.buildingID ?? ""
            } else {
                BuildingDetails.getBuildingID().done { buildingID in
                    form.buildingID = buildingID
                }.catch { error in
                    Log.thisError(error)
                    SVProgressHUD.showError(withStatus: error.localizedDescription)
                }
            }
        }.fullViewBackgroundColor()
         .navigationBarItems(
                leading: Button(action: {
                    dismissBlock(.canceled)
                }) {
                    Image(uiImage: Asset.Images.Concierge.icClose.image.withRenderingMode(.alwaysTemplate))
                },
                trailing: Button(action: {

                    SVProgressHUD.show()
                    let promise: Promise<Void>

                    if let activityLogID = activityLog?.id {
                        promise = ConciergeService().updateActivityLog(form.asParams, activityLogID: activityLogID)
                    } else {
                        promise = ConciergeService().createActivityLog(form.asParams)
                    }

                    promise.done {
                        SVProgressHUD.dismiss()

                        if let activityLog = activityLog {
                            let updatedActivityLog = form.updatedActivityLog(activityLog)
                            dismissBlock(.updated(updatedActivityLog))
                        } else {
                            dismissBlock(.created)
                        }

                    }.catch { error in
                        Log.thisError(error)
                        SVProgressHUD.showError(withStatus: error.localizedDescription)
                    }
                }) {
                    Text("Save").navigationSaveButton()
                }.disabled(!form.isValid)
        ).navigationBarTitle(Text("\(activityLog == nil ? "Create" : "Update") Activity Log"), displayMode: .inline)

    }
}