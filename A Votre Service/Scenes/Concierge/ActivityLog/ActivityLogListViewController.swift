//
// Created by Andrei Stoicescu on 16.11.2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import PromiseKit
import Combine
import SVProgressHUD
import SwiftUI
import DifferenceKit

extension UISearchTextField {
    func searchNotificationPublisher(_ notification: NSNotification.Name) -> AnyPublisher<String, Never> {
        NotificationCenter.default
                .publisher(for: notification, object: self)
                .compactMap { ($0.object as? UISearchTextField)?.text }
                .eraseToAnyPublisher()
    }

    func searchPublisher() -> AnyPublisher<String, Never> {
        Publishers.Merge(searchNotificationPublisher(UISearchTextField.textDidChangeNotification)
                                 .debounce(for: .milliseconds(500), scheduler: RunLoop.main),
                         searchNotificationPublisher(UISearchTextField.textDidEndEditingNotification)
                  ).removeDuplicates()
                  .eraseToAnyPublisher()
    }
}

class ActivityLogListViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

    lazy var emptyNode = EmptyNode(emptyNodeType: .generic, showAction: false)

    lazy var refreshControl = UIRefreshControl()
    var collectionNode: ASCollectionNode

    var dataSource: ObservablePaginatedDataSource<ActivityLog>
    var logs = [ActivityLog]()
    private var subscriptions = Set<AnyCancellable>()

    private var searchTerm: String = "" {
        didSet {
            if searchTerm != oldValue {
                dataSource.loadMoreContentIfNeeded()
            }
        }
    }

    let searchController = UISearchController(searchResultsController: nil)

    override init() {
        collectionNode = .avsCollection()
        dataSource = .init()

        super.init(node: collectionNode)

        dataSource.paginate = { [weak self] per, page in
            guard let term = self?.searchTerm else { return .value([]) }

            if term.count > 0 {
                return ConciergeService().searchActivityLogs(per: per, page: page, term: term)
            } else {
                return ConciergeService().getActivityLogs(per: per, page: page)
            }
        }

        dataSource.cacheKey = "activity_logs"
        dataSource.dynamicCacheable = { [weak self] in
            String.isBlank(self?.searchTerm)
        }

        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.searchController = searchController
        searchController.obscuresBackgroundDuringPresentation = false

        searchController.searchBar.searchTextField.searchPublisher().sink { [weak self] term in
            self?.searchTerm = term
        }.store(in: &subscriptions)

        setupBindings()
        navigationItem.title = "Activity Logs"
        replaceSystemBackButton()
        showEmptyScreen(false)
        collectionNode.view.backgroundView = emptyNode.view
        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)
        collectionNode.view.addSubview(refreshControl)
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(tapAdd))
        refreshItems()
    }

    @objc func tapAdd() {
        let view = ActivityLogFormView(activityLog: nil, dismissBlock: { [weak self] dismiss in
            self?.navigationController?.dismiss(animated: true)
            switch dismiss {
            case .created:
                self?.refreshItems()
            default:
                ()
            }
        })

        let nav = AVSNavigationController(rootViewController: UIHostingController(rootView: view))
        nav.isModalInPresentation = true
        navigationController?.present(nav, animated: true)
    }

    func reloadData() {
        let source = logs
        let target = dataSource.items

        let stagedChangeSet = StagedChangeset(source: source, target: target)
        for changeSet in stagedChangeSet {
            logs = changeSet.data
            collectionNode.performDiffBatchUpdates(changeSet: changeSet)
        }
    }

    func setupBindings() {
        dataSource.$items
                .receive(on: DispatchQueue.main)
                .dropFirst()
                .sink { [weak self] items in
                    self?.reloadData()
                }.store(in: &subscriptions)

        dataSource.$hasNoItemsAfterLoading
                .receive(on: DispatchQueue.main)
                .sink { [weak self] hasNoItemsAfterLoading in
                    self?.showEmptyScreen(hasNoItemsAfterLoading)
                }.store(in: &subscriptions)

        dataSource.$showLoadingIndicator
                .receive(on: DispatchQueue.main)
                .sink { [weak self] loading in
                    guard let self = self else { return }

                    if loading {
                        if !self.refreshControl.isRefreshing {
                            SVProgressHUD.show()
                        }
                    } else {
                        SVProgressHUD.dismiss()
                        self.refreshControl.endRefreshing()
                    }
                }.store(in: &subscriptions)
    }

    @objc func refreshItems() {
        dataSource.loadMoreContentIfNeeded()
    }

    func showEmptyScreen(_ show: Bool) {
        emptyNode.isHidden = !show
    }

    func collectionView(_ collectionView: ASCollectionView, willDisplayNodeForItemAt indexPath: IndexPath) {
        dataSource.loadMoreContentIfNeeded(currentItem: dataSource.items[indexPath.item])
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        logs.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let log = logs[indexPath.item]
        return {
            ConciergePagerListNodes.RequestCell(activityLog: log)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        let log = logs[indexPath.item]
        let vc = ActivityLogDetailsViewController(log: log)
        vc.didChangeLog = { [weak self] action in
            guard let self = self else { return }

            switch action {
            case .delete(let logID):
                if self.dataSource.removeItem(log) != nil {
                    self.reloadData()
                }
                if let idx = self.logs.firstIndex(where: { $0.id == logID }) {
                    self.dataSource.items.remove(at: idx)
                    self.reloadData()
                }
                self.navigationController?.popToViewController(self, animated: true)
            case .update(let log):
                if let idx = self.dataSource.items.firstIndex(where: { $0.id == log.id }) {
                    self.dataSource.items[idx] = log
                    self.reloadData()
                }
            }
        }
        navigationController?.pushViewController(vc, animated: true)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }
}
