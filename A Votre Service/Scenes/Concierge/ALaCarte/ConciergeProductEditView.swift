//
// Created by Andrei Stoicescu on 25.02.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import SwiftUI
import SVProgressHUD
import PromiseKit

class ConciergeProductEditForm: ObservableObject {
    @Published var category: [ConciergeForm.Option<ConciergeALaCarteCategory>] = []
    @Published var availableCategories: [ConciergeForm.Option<ConciergeALaCarteCategory>] = []
    @Published var name: String = ""
    @Published var description: String = "" {
        didSet {
            Log.this(description)
        }
    }
    @Published var published = false
    @Published var weight = ""
    @Published var hasInventory = false
    @Published var inventoryCount = ""
    @Published var file = [String]()
    @Published var price = ""
    @Published var taxPercent = ""

    var initialImageMediaFiles = [FeedNotificationMediaFile]()

    var buildingID: String = ""
    var id: String?

    init(product: ConciergeALaCarteProduct?, category: ConciergeALaCarteCategory?, allCategories: [ConciergeALaCarteCategory]) {
        self.product = product
        if let category = category {
            self.category = [.init(id: category.id, label: category.name, underlyingValue: category)]
        } else {
            self.category = []
        }

        availableCategories = allCategories.map {
            .init(id: $0.id, label: $0.name, underlyingValue: $0)
        }

        if let product = product {
            if let fid = product.fileId, let url = product.productImageURL {
                initialImageMediaFiles = [.generateMediaWithURL(url, targetID: fid)]
            }

            name = product.name ?? ""
            description = product.description?.html?.string ?? ""
            weight = product.weight ?? ""
            published = product.published
            hasInventory = product.hasInventory
            inventoryCount = product.inventoryCount ?? ""
            price = product.price ?? ""
            taxPercent = product.taxPercent ?? ""
        }
    }

    var product: ConciergeALaCarteProduct?

    var asParams: ConciergeProductRequest? {
        guard let categoryId = category.first?.id,
              let file = file.first
                else { return nil }

        return .init(categoryId: categoryId,
                     description: description,
                     hasInventory: hasInventory,
                     imageId: file,
                     inventoryCount: inventoryCount,
                     price: price,
                     taxPercent: taxPercent,
                     weight: weight,
                     id: id,
                     name: name,
                     published: published)
    }

    var isValid: Bool {
        let fieldsMissing = [
            name,
            description,
            category.first?.id,
            file.first
        ].map { String.isBlank($0) }.contains(true)

        guard !fieldsMissing else { return false }
        return true
    }
}

struct ConciergeProductEditView: View {

    struct ProductUploader: AssetMediaUploadBuilder {
        func createUploader() -> ConciergeForm.AssetMediaUploader.MediaUploaderClosure {
            { data, fileType, progress, completion in
                ConciergeService().uploadALaCarteImage(imageData: data, progress: progress).done { result in
                    completion(.success(String(result.fileID)))
                }.catch { error in
                    completion(.failure(APIError.from(error)))
                }
            }
        }
    }

    enum DismissMode {
        case canceled
        case created
        case updated
    }

    @ObservedObject var form: ConciergeProductEditForm
    var dismissBlock: (DismissMode) -> Void

    var body: some View {
        ScrollView(showsIndicators: false) {
            VStack(spacing: 20) {
                ConciergeForm.SelectInput<ConciergeALaCarteCategory>(
                        label: "Category",
                        placeholder: "Select Category",
                        options: form.availableCategories,
                        value: $form.category)

                ConciergeForm.TextInput(label: "Name",
                                        value: $form.name)

                ConciergeForm.TextArea(label: "Description",
                                       value: $form.description)

                ConciergeForm.ToggleInput(label: "Published",
                                          value: $form.published)

                ConciergeForm.TextInput(label: "Weight",
                                        value: $form.weight)

                ConciergeForm.TextInput(label: "Price",
                                        value: $form.price)

                ConciergeForm.TextInput(label: "Tax Percent",
                                        value: $form.taxPercent)

                ConciergeForm.ToggleInput(label: "Has Inventory",
                                          value: $form.hasInventory)

                if form.hasInventory {
                    ConciergeForm.TextInput(label: "Inventory Count",
                                            value: $form.inventoryCount)
                }

                ConciergeForm.MediaFile(
                        label: "Media File",
                        placeholder: "Tap to upload media file",
                        value: $form.file,
                        maximumSelectionsAllowed: 1,
                        uploadBuilder: ProductUploader(),
                        initialMediaFiles: form.initialImageMediaFiles
                )

            }.padding(.top, 20)
             .padding(.horizontal, 20)
        }.fullViewBackgroundColor()
         .navigationBarItems(
                 leading: Button(action: {
                     dismissBlock(.canceled)
                 }) {
                     Image(uiImage: Asset.Images.Concierge.icClose.image.withRenderingMode(.alwaysTemplate))
                 },
                 trailing: Button(action: {
                     guard let params = form.asParams else { return }
                     SVProgressHUD.show()
                     let promise: Promise<Void>

                     if let productId = form.product?.id {
                         promise = ConciergeService().updateALaCarteProduct(params, productId: productId)
                     } else {
                         promise = ConciergeService().createALaCarteProduct(params)
                     }

                     promise.done {
                         SVProgressHUD.dismiss()
                         if form.product != nil {
                             dismissBlock(.updated)
                         } else {
                             dismissBlock(.created)
                         }
                     }.catch { error in
                         Log.thisError(error)
                         SVProgressHUD.showError(withStatus: error.localizedDescription)
                     }
                 }) {
                     Text("Save").navigationSaveButton()
                 }.disabled(!form.isValid)
         ).navigationBarTitle(Text("\(form.product == nil ? "Create" : "Update") Product"), displayMode: .inline)

    }
}
