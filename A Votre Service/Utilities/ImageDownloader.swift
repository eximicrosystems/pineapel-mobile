//
// Created by Andrei on 25.03.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import PromiseKit
import PINRemoteImage
import AsyncDisplayKit

extension URL {
    func downloadAsImage() -> Promise<UIImage> {
        Promise<UIImage> { seal in
            PINRemoteImageManager.shared().downloadImage(with: self) { result in
                seal.resolve(result.image, result.error)
            }
        }
    }
}