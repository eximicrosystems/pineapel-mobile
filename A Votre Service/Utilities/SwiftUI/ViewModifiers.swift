//
// Created by Andrei Stoicescu on 31/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import SwiftUI

struct HideNavigationBarModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
                .navigationBarBackButtonHidden(true)
                .navigationBarHidden(true)
                .navigationBarTitle("")
    }
}

struct CustomBackgroundColor: ViewModifier {
    var color: ColorAsset

    func body(content: Content) -> some View {
        content.background(Color(color.color))
    }
}

struct CustomForegroundColor: ViewModifier {
    var color: ColorAsset

    func body(content: Content) -> some View {
        content.foregroundColor(Color(color.color))
    }
}

struct CardModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
                .padding(15)
                .background(
                        RoundedRectangle(cornerRadius: 4)
                                .fill(Color.white)
                                .shadow(color: Color.black.opacity(0.2), radius: 1, x: 0, y: 0))
    }
}

struct CustomFont: ViewModifier {
    var font: UIFont

    func body(content: Content) -> some View {
        content.font(Font.custom(font.fontName, size: font.pointSize))
    }
}

struct NavigationSaveButton: ViewModifier {
    func body(content: Content) -> some View {
        content.accentColor(.avsColor(.primaryIcon)).font(.button)
    }
}

struct FullViewBackgroundModifier: ViewModifier {
    var color: AVSColor

    func body(content: Content) -> some View {
        ZStack {
            Color.avsColor(color).edgesIgnoringSafeArea(.all)
            content
        }
    }
}

extension View {
    func fullViewBackgroundColor(_ color: AVSColor = .secondaryBackground) -> some View {
        modifier(FullViewBackgroundModifier(color: color))
    }

    func navigationSaveButton() -> some View {
        modifier(NavigationSaveButton())
    }

    func hideNavigationBar() -> some View {
        modifier(HideNavigationBarModifier())
    }

    func conciergeFont(_ font: Asset.ConciergeFonts) -> some View {
        modifier(CustomFont(font: font.font))
    }

    func font(_ avsFont: AVSFont) -> some View {
        modifier(CustomFont(font: avsFont.font))
    }

    func foregroundColor(_ color: ColorAsset) -> some View {
        modifier(CustomForegroundColor(color: color))
    }

    func foregroundColor(_ color: AVSColor) -> some View {
        foregroundColor(Color(color.color))
    }

    func backgroundColor(_ color: ColorAsset) -> some View {
        modifier(CustomBackgroundColor(color: color))
    }

    func background(_ avsColor: AVSColor) -> some View {
        background(Color(avsColor.color))
    }

    func border(_ avsColor: AVSColor) -> some View {
        border(Color(avsColor.color))
    }
}

extension Color {
    static func asset(_ asset: ColorAsset) -> Color {
        Color(asset.color)
    }
}
