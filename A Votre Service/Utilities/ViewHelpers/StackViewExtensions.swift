//
// Created by Andrei Stoicescu on 03/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import UIKit

extension UIStackView {
    func addArrangedSubviews(subviews: [UIView]) {
        subviews.forEach { addArrangedSubview($0) }
    }

    /// Takes all arranged subviews and calls `removeFromSuperview` on them.
    /// - Discussion: Passing them into
    /// `removeArrangedSubview(_:) will just remove them from the `arrangedSubviews` array, but not
    /// from `subviews`. They will also remain in the view hierarchy.
    func removeArrangedSubviews() {
        let asv = arrangedSubviews
        for subview in asv {
            subview.removeFromSuperview()
        }
    }
}