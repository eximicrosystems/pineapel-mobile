//
// Created by Andrei Stoicescu on 03/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI

extension Asset {
    enum Fonts {
        case H1
        case H1Normal
        case H1Regular
        case H2
        case H2Normal
        case H3
        case H4
        case H5
        case Note
        case NoteHead
        case SubNote
        case SubNoteHead
        case NormalText
        case NormalTextHead
        case Body
        case BodyHead

        var uiFont: UIFont {
            let font: UIFont
            switch self {
            case .H1Normal:
                font = UIFont.systemFont(ofSize: 30, weight: .semibold)
            case .H1Regular:
                font = UIFont.systemFont(ofSize: 30, weight: .regular)
            case .H1:
                font = UIFont.systemFont(ofSize: 30, weight: .bold)
            case .H2:
                font = UIFont.systemFont(ofSize: 24, weight: .bold)
            case .H2Normal:
                font = UIFont.systemFont(ofSize: 24, weight: .semibold)
            case .H3:
                font = UIFont.systemFont(ofSize: 20, weight: .bold)
            case .H4:
                font = UIFont.systemFont(ofSize: 18, weight: .bold)
            case .H5:
                font = UIFont.systemFont(ofSize: 16, weight: .bold)
            case .Note:
                font = UIFont.systemFont(ofSize: 12)
            case .NoteHead:
                font = UIFont.systemFont(ofSize: 12, weight: .semibold)
            case .SubNote:
                font = UIFont.systemFont(ofSize: 10)
            case .SubNoteHead:
                font = UIFont.systemFont(ofSize: 10, weight: .bold)
            case .Body:
                font = UIFont.systemFont(ofSize: 14)
            case .BodyHead:
                font = UIFont.systemFont(ofSize: 14, weight: .semibold)
            case .NormalText:
                font = UIFont.systemFont(ofSize: 16)
            case .NormalTextHead:
                font = UIFont.systemFont(ofSize: 16, weight: .semibold)
            }

            return font
        }

        // SwiftUI somehow shows it as serif if using UIFont.systemFont
        var font: Font {
            let font: Font
            switch self {
            case .H1Normal:
                font = Font.system(size: 30, weight: .semibold)
            case .H1Regular:
                font = Font.system(size: 30, weight: .regular)
            case .H1:
                font = Font.system(size: 30, weight: .bold)
            case .H2:
                font = Font.system(size: 24, weight: .bold)
            case .H2Normal:
                font = Font.system(size: 24, weight: .semibold)
            case .H3:
                font = Font.system(size: 20, weight: .bold)
            case .H4:
                font = Font.system(size: 18, weight: .bold)
            case .H5:
                font = Font.system(size: 16, weight: .bold)
            case .Note:
                font = Font.system(size: 12)
            case .NoteHead:
                font = Font.system(size: 12, weight: .semibold)
            case .SubNote:
                font = Font.system(size: 10)
            case .SubNoteHead:
                font = Font.system(size: 10, weight: .bold)
            case .Body:
                font = Font.system(size: 14)
            case .BodyHead:
                font = Font.system(size: 14, weight: .semibold)
            case .NormalText:
                font = Font.system(size: 16)
            case .NormalTextHead:
                font = Font.system(size: 16, weight: .semibold)
            }
            return font
        }
    }

    enum ConciergeFonts {
        case Body
        case BodyHead
        case BodyHeadSm
        case Button
        case Caption
        case Note
        case NoteHead
        case SubNote
        case H1
        case H1Large
        case H2
        case H3
        case H4

        var font: UIFont {
            switch self {
            case .Body:
                return UIFont(name: "Avenir-Book", size: 14)!
            case .BodyHead:
                return UIFont(name: "Avenir-Medium", size: 16)!
            case .BodyHeadSm:
                return UIFont(name: "Avenir-Medium", size: 14)!
            case .Note:
                return UIFont(name: "Avenir-Light", size: 12)!
            case .NoteHead:
                return UIFont(name: "Avenir-Medium", size: 12)!
            case .SubNote:
                return UIFont(name: "Avenir-Medium", size: 10)!
            case .Caption:
                return UIFont(name: "Avenir-Book", size: 14)!
            case .H1Large:
                return UIFont(name: "Avenir-Medium", size: 60)!
            case .H1:
                return UIFont(name: "Avenir-Heavy", size: 36)!
            case .H2:
                return UIFont(name: "Avenir-Heavy", size: 28)!
            case .H3:
                return UIFont(name: "Avenir-Heavy", size: 22)!
            case .H4:
                return UIFont(name: "Avenir-Heavy", size: 20)!
            case .Button:
                return UIFont(name: "Avenir-Medium", size: 19)!
            }
        }
    }
}
