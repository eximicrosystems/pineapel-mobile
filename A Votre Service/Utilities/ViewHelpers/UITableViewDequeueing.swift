//
// Created by Andrei Stoicescu on 05/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import UIKit.UITableView

// The old-school way of creating and dequeuing UITableViewCells make view controllers a lot more complex as the table
// grows. We can use Swift's type system to avoid this clutter. See the following article for more details:
// http://techblog.thescore.com/2016/04/04/typed-uitableview-uicollectionview-dequeuing-in-swift/

// Providing the identifier
extension UITableViewCell {
    public class func defaultIdentifier() -> String {
        "\(self)"
    }
}

extension UITableViewHeaderFooterView {
    public class func defaultIdentifier() -> String {
        "\(self)"
    }
}

// Registering Cells
extension UITableView {
    public func register<T: UITableViewCell>(cellClass `class`: T.Type) {
        register(`class`, forCellReuseIdentifier: `class`.defaultIdentifier())
    }

    public func register<T: UITableViewCell>(nib: UINib, forClass `class`: T.Type) {
        register(nib, forCellReuseIdentifier: `class`.defaultIdentifier())
    }

    public func registerWithCorrespondingNib<T: UITableViewCell>(forClass `class`: T.Type) {
        guard let className = `class`.defaultIdentifier().components(separatedBy: ".").last else {
            return
        }

        let nib = UINib(nibName: className, bundle: nil)
        register(nib: nib, forClass: `class`)
    }
}

// Header / Footer
extension UITableView {
    public func register<T: UITableViewHeaderFooterView>(headerFooterClass `class`: T.Type) {
        register(`class`, forHeaderFooterViewReuseIdentifier: `class`.defaultIdentifier())
    }

    public func register<T: UITableViewHeaderFooterView>(nib: UINib, forHeaderFooterClass `class`: T.Type) {
        register(nib, forHeaderFooterViewReuseIdentifier: `class`.defaultIdentifier())
    }

    public func registerWithCorrespondingNib<T: UITableViewHeaderFooterView>(forHeaderFooterClass `class`: T.Type) {
        guard let className = `class`.defaultIdentifier().components(separatedBy: ".").last else {
            return
        }

        let nib = UINib(nibName: className, bundle: nil)
        register(nib: nib, forHeaderFooterClass: `class`)
    }
}

// Dequeueing
extension UITableView {
    func dequeueReusableCell<T: UITableViewCell>(withClass `class`: T.Type) -> T? {
        return dequeueReusableCell(withIdentifier: `class`.defaultIdentifier()) as? T
    }

    func dequeueReusableCell<T: UITableViewCell>(withClass `class`: T.Type, forIndexPath indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: `class`.defaultIdentifier(), for: indexPath) as? T else {
            fatalError("Error: cell with identifier: \(`class`.defaultIdentifier()) for index path: \(indexPath) is not \(T.self)")
        }
        return cell
    }

    func dequeueReusableHeaderFooterView<T: UITableViewHeaderFooterView>(withClass `class`: T.Type) -> T? {
        return dequeueReusableHeaderFooterView(withIdentifier: `class`.defaultIdentifier()) as? T
    }
}
