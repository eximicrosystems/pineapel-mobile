//
// Created by Andrei Stoicescu on 15.01.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//

import UIKit

extension UIView {

    /// Flip view horizontally.
    func flipX() {
        transform = CGAffineTransform(scaleX: -transform.a, y: transform.d)
    }

    /// Flip view vertically.
    func flipY() {
        transform = CGAffineTransform(scaleX: transform.a, y: -transform.d)
    }
}
