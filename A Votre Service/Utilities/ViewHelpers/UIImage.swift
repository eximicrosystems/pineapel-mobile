//
// Created by Andrei Stoicescu on 05/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import UIKit

extension UIImage {
    class func pixelImage(color: UIColor) -> UIImage {
        let rect = CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: 1.0, height: 1.0))

        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()

        context!.setFillColor(color.cgColor)
        context!.fill(rect)

        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return image!
    }

    static func createSelectionIndicator(color: UIColor, size: CGSize, lineHeight: CGFloat) -> UIImage? {
        let size = CGSize(width: size.width, height: size.height + 1)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()

        let lineWidth = size.width * 2 / 3
        let lineX = (size.width - lineWidth) / 2

        UIRectFill(CGRect(x: lineX, y: 0, width: lineWidth, height: lineHeight))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return image
    }
}