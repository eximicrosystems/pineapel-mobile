//
// Created by Andrei Stoicescu on 10/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func replaceSystemBackButton() {
//         navigationItem.backBarButtonItem = UIBarButtonItem(image: Asset.Images.icBackArrow.image.withTintColor(AVSColor.primaryIcon.color, renderingMode: .alwaysTemplate), style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}
