//
// Created by Andrei Stoicescu on 10/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit

class TopAlignedCollectionViewFlowLayout: UICollectionViewFlowLayout {
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let attributes = super.layoutAttributesForElements(in: rect)?.map {
            $0.copy()
        } as? [UICollectionViewLayoutAttributes]

        attributes?.reduce([CGFloat: (CGFloat, [UICollectionViewLayoutAttributes])]()) {
            guard $1.representedElementCategory == .cell else {
                return $0
            }
            return $0.merging([ceil($1.center.y): ($1.frame.origin.y, [$1])]) {
                ($0.0<$1.0 ? $0.0 : $1.0, $0.1 + $1.1)
            }
        }.values.forEach { minY, line in
            line.forEach {
                var frame = $0.frame
                let offset = minY - $0.frame.origin.y
                frame = frame.offsetBy(
                        dx: 0,
                        dy: offset
                )

                if offset != 0 {
                    // this also sets the items to have equal heights if they're offset
                    $0.frame = CGRect(x: frame.origin.x, y: frame.origin.y, width: frame.size.width, height: frame.size.height + abs(2 * offset))
                } else {
                    $0.frame = frame
                }
            }
        }

        return attributes
    }
}