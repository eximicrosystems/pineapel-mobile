//
// Created by Andrei Stoicescu on 17/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit

class AvatarShadowView: UIView {

    lazy var imageView = UIImageView()

    init() {
        super.init(frame: .zero)
        imageView.clipsToBounds = true

        addSubview(imageView)

        imageView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        layer.shadowRadius = 2
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 2)
        layer.shadowOpacity = 0.1
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        imageView.layer.cornerRadius = frame.size.height / 2
        imageView.layer.borderColor = UIColor.white.cgColor
        imageView.layer.borderWidth = 1
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}