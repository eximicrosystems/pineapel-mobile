//
// Created by Andrei Stoicescu on 08/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import Validator

protocol AVSFormValidatable {
    var isValid: Bool { get set }
    var validation: ((String?) -> ValidationResult?)? { get set }
    var isMandatory: Bool { get set }
    func checkValidity()
}

/// Conform the view receiver to be updated with a form item
protocol AVSFormUpdatable: class {
    func update(with formItem: AVSForm.FormItem)
    var formItemValueUpdate: ((AVSForm.FormItem) -> Void)? { get set }
}

/// Conform receiver to have a form item property
protocol AVSFormConformity {
    var formItem: AVSForm.FormItem? { get set }
}