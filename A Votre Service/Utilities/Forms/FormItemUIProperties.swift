//
// Created by Andrei Stoicescu on 08/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import UIKit

extension AVSForm {

    struct PickerProperties {
        var options: [String] = []
    }

    struct FormItemUIProperties {
        var keyboardType = UIKeyboardType.default
        var cellType: FormItemCellType = .textField
        var readonly: Bool = false
        var secureText: Bool = false
        var picker = PickerProperties()
    }
}