//
// Created by Andrei Stoicescu on 03/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit

class InsetTextField: UITextField {
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        bounds.insetBy(dx: 10, dy: 10)
    }

    override func textRect(forBounds bounds: CGRect) -> CGRect {
        bounds.insetBy(dx: 10, dy: 10)
    }
}

class AVSTextFieldContainer: UIView {

    var readonly: Bool = false {
        didSet {
            textField.isUserInteractionEnabled = !readonly
        }
    }

    var placeholder: String? {
        didSet {
            topPlaceholderLabel.text = placeholder?.uppercased()
        }
    }

    var noteText: String? {
        didSet {
            noteLabel.text = noteText
            if (oldValue == nil && noteText != nil) || (oldValue != nil && noteText == nil) {
                updateStack()
            }
        }
    }

    var text: String? {
        get { textField.text }
        set(newText) {
            textField.text = newText
        }
    }

    lazy var stackView: UIStackView = {
        let view = UIStackView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.axis = .vertical
        view.alignment = .fill
        view.distribution = .fill
        view.spacing = 8
        return view
    }()

    lazy var textField: InsetTextField = {
        let textField = InsetTextField()
        textField.translatesAutoresizingMaskIntoConstraints = false

        textField.textColor = AVSColor.primaryLabel.color
        textField.font = AVSFont.bodyRoman.font
        textField.autocorrectionType = .no
        textField.autocapitalizationType = .none
        textField.spellCheckingType = .no
        textField.backgroundColor = AVSColor.inputFill.color
        textField.layer.cornerRadius = 3
        textField.layer.borderColor = AVSColor.inputBorder.color.cgColor
        textField.layer.borderWidth = 1

        return textField
    }()

    lazy var topPlaceholderLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = AVSColor.accentForeground.color
        label.text = placeholder?.uppercased()
        //label.font = AVSFont.note.font
        label.font = AVSFont.H5.font
        label.textAlignment = NSTextAlignment.center
        label.numberOfLines = 1
        return label
    }()

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        if (traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection)) {
            textField.layer.borderColor = AVSColor.inputBorder.color.cgColor
        }
    }

    lazy var noteLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = Asset.Colors.Semantic.accentLabel.color//AVSColor.secondaryLabel.color
        label.text = noteText
        label.font = AVSFont.noteMedium.font
        label.numberOfLines = 2
        return label
    }()

    private func updateStack() {
        stackView.removeArrangedSubviews()

        if placeholder != nil {
            stackView.addArrangedSubview(topPlaceholderLabel)
        }

        stackView.addArrangedSubview(textField)

        if noteText != nil {
            stackView.addArrangedSubview(noteLabel)
        }
    }

    func configureLayout() {
        addSubview(stackView)
        stackView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

    convenience init(placeholder: String? = nil, noteText: String? = nil, text: String? = nil) {
        self.init(frame: .zero)
        translatesAutoresizingMaskIntoConstraints = false
        self.placeholder = placeholder
        self.noteText = noteText
        self.text = text

        configureLayout()

        updateStack()
    }
}
