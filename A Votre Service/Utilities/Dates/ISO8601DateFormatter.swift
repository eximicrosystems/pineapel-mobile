//
// Created by Andrei Stoicescu on 09/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation

/**
 Wrapper around some Foundation ISO8601DateFormatters which first tries to parse an ISO 8601
 date string with fractional seconds using the first formatter and if that fails tries it without them using the
 others. All of them are only able to parse one of the formats exclusively
*/

extension DateFormatter {
    static var basicISO8601Date: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        return dateFormatter
    }()
}

class AVSISO8601DateFormatter: DateFormatter {

    static func formattedString(date: Date, timeZone: TimeZone) -> String {
        let formatter = ISO8601DateFormatter()
        formatter.timeZone = timeZone
        formatter.formatOptions = [.withInternetDateTime]
        return formatter.string(from: date)
    }

    private static var formatterWithFractionalSeconds: ISO8601DateFormatter {
        let formatter = ISO8601DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]

        return formatter
    }
    private static var formatterWithoutFractionalSeconds: ISO8601DateFormatter {
        let formatter = ISO8601DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.formatOptions = [.withInternetDateTime]

        return formatter
    }

    private static var formatterWithFullDate: ISO8601DateFormatter {
        let formatter = ISO8601DateFormatter()
        formatter.formatOptions = [.withFullDate]

        return formatter
    }

    override func string(from date: Date) -> String {
        type(of: self).formatterWithoutFractionalSeconds.string(from: date)
    }

    override func date(from string: String) -> Date? {
        if let date = type(of: self).formatterWithFractionalSeconds.date(from: string) {
            return date
        }

        if let date = type(of: self).formatterWithoutFractionalSeconds.date(from: string) {
            return date
        }

        if let date = type(of: self).formatterWithFullDate.date(from: string) {
            return date
        }

        return nil
    }

    override func string(for obj: Any?) -> String? {
        if let date = obj as? Date {
            return string(from: date)
        }

        return nil
    }

    override func getObjectValue(_ obj: AutoreleasingUnsafeMutablePointer<AnyObject?>?,
                                 for string: String,
                                 errorDescription error: AutoreleasingUnsafeMutablePointer<NSString?>?) -> Bool {
        if let date = date(from: string) {
            obj?.pointee = date as AnyObject?
            return true
        }

        return false
    }
}
