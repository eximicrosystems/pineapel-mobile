//
// Created by Andrei Stoicescu on 10/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import DateToolsSwift
import SwiftyUserDefaults

extension DateFormatter {
    convenience init(format: String, timeZone: TimeZone? = nil) {
        self.init()
        self.dateFormat = format
        if let tz = timeZone {
            self.timeZone = tz
        }
    }

    static var avsDateFormatter: DateFormatter = {
        DateFormatter(format: "MMMM dd, yyyy")
    }()

    static var avsTimeFormatter: DateFormatter = {
        DateFormatter(format: "h:mm a")
    }()

    static var avsDateTimeFormatter: DateFormatter = {
        DateFormatter(format: "MMMM dd, yyyy - h:mm a")
    }()
}

extension Date {

    // TODO maybe add translations or use DateTools
    var avsTimeAgo: String {
        if isTomorrow {
            return "Tomorrow at \(self.format(with: "hh:mm a", locale: Locale.current))"
        }

        if isToday {
            return "Today at \(self.format(with: "hh:mm a", locale: Locale.current))"
        }

        if isYesterday {
            return "Yesterday at \(self.format(with: "hh:mm a", locale: Locale.current))"
        }

        let dateString = self.format(with: "MMM d", locale: Locale.current)
        let timeString = self.format(with: "hh:mm a", locale: Locale.current)
        return "\(dateString) at \(timeString)"
    }

    // Today, Yesterday, or December 24, 2020
    var avsDateAgo: String {
        if isToday {
            return "Today"
        }

        if isYesterday {
            return "Yesterday"
        }

        return avsDateString()
    }

    var avsTimeAgoChat: String {
        format(with: "hh:mm a")
    }

    // December 24, 2020 at 10:40 AM
    func avsTimestampString(utc: Bool = false) -> String {
        "\(avsDateString(utc: utc)) at \(avsTimeString(utc: utc))"
    }

    // December 24, 2020
    func avsDateString(utc: Bool = false) -> String {
        let dateFormat = "MMMM dd, yyyy"
        let tz = utc ? TimeZone(abbreviation: "UTC")! : TimeZone.autoupdatingCurrent
        return self.format(with: dateFormat, timeZone: tz)
    }

    // Dec 24
    func avsDateShortMonthString(utc: Bool = false) -> String {
        let dateFormat = "MMM dd"
        let tz = utc ? TimeZone(abbreviation: "UTC")! : TimeZone.autoupdatingCurrent
        return self.format(with: dateFormat, timeZone: tz)
    }

    // Dec 24, 2020
    func avsDateShortMonthYearString(utc: Bool = false) -> String {
        let dateFormat = "MMM dd, yyyy"
        let tz = utc ? TimeZone(abbreviation: "UTC")! : TimeZone.autoupdatingCurrent
        return self.format(with: dateFormat, timeZone: tz)
    }

    // 10:40 AM
    func avsTimeString(utc: Bool = false) -> String {
        let timeFormat = "h:mm a"
        let tz = utc ? TimeZone(abbreviation: "UTC")! : TimeZone.autoupdatingCurrent
        return self.format(with: timeFormat, timeZone: tz)
    }

    // 10:40 AM
    func avsTimeString(timeZone: TimeZone) -> String {
        self.format(with: "h:mm a", timeZone: timeZone)
    }

    // December 24, 2020 at 10:40 AM
    func avsTimestampString(timeZone: TimeZone) -> String {
        "\(avsDateString(timeZone: timeZone)) at \(avsTimeString(timeZone: timeZone))"
    }

    // Dec 24, 2020
    func avsDateShortMonthYearString(timeZone: TimeZone) -> String {
        self.format(with: "MMM dd, yyyy", timeZone: timeZone)
    }

    // December 24, 2020
    func avsDateString(timeZone: TimeZone) -> String {
        self.format(with: "MMMM dd, yyyy", timeZone: timeZone)
    }

    // 2020-12-24
    var avsDateFormatted: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: self)
    }

    // 2020-12-24 - 10:40 AM
    var avsTimestampStringShort: String {
        "\(avsDateFormatted) - \(avsTimeString())"
    }

    func convertedFromHourUTCToSameHourInTimeZone(_ timeZone: TimeZone) -> Date {
        var calendar = Calendar(identifier: .gregorian)
        calendar.timeZone = timeZone

        let utcCalendar = Calendar.utcCalendar

        let date = self

        let hour = utcCalendar.component(.hour, from: date)
        let minute = utcCalendar.component(.minute, from: date)
        let year = utcCalendar.component(.year, from: date)
        let month = utcCalendar.component(.month, from: date)
        let day = utcCalendar.component(.day, from: date)
        let second = utcCalendar.component(.second, from: date)

        var components = DateComponents()
        components.year = year
        components.month = month
        components.day = day
        components.hour = hour
        components.minute = minute
        components.second = second

        return calendar.date(from: components)!
    }

    func convertedToSameTimeZone(_ timeZone: TimeZone) -> Date {
        var calendar = Calendar(identifier: .gregorian)
        calendar.timeZone = timeZone

        let date = self

        let hour = date.component(.hour)
        let minute = date.component(.minute)
        let year = date.component(.year)
        let month = date.component(.month)
        let day = date.component(.day)
        let second = date.component(.second)

        var components = DateComponents()
        components.year = year
        components.month = month
        components.day = day
        components.hour = hour
        components.minute = minute
        components.second = second

        return calendar.date(from: components)!
    }

    enum BuildingTimeZoneConversion {
        case same
        case endOfDay
        case startOfDay
    }

    func convertedToBuildingTimeZone(_ conversion: BuildingTimeZoneConversion = .same) -> Date {
        var newDate = add(AppState.buildingTimeZone.secondsFromGMT().seconds)
        switch conversion {
        case .endOfDay:
            newDate = newDate.end(of: .day)
        case .startOfDay:
            newDate = newDate.start(of: .day)
        case .same:
            ()
        }
        return newDate.convertedToSameTimeZone(AppState.buildingTimeZone)
    }

    func convertedToSameTimeUTC() -> Date {
        convertedToSameTimeZone(TimeZone(abbreviation: "UTC")!)
    }

    var timeIntervalString: String {
        String(Int(timeIntervalSince1970))
    }
}

extension Calendar {
    static var utcCalendar: Calendar {
        var calendar = Calendar(identifier: .gregorian)
        calendar.timeZone = TimeZone(abbreviation: "UTC")!
        return calendar
    }

    static var buildingTimeZoneCalendar: Calendar {
        var calendar = Calendar(identifier: .gregorian)
        calendar.timeZone = AppState.buildingTimeZone
        return calendar
    }
}