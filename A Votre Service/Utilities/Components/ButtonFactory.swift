//
// Created by Andrei Stoicescu on 04/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit
import AsyncDisplayKit
import SwiftUI

extension ASButtonNode {
    static func primaryButton(title: String) -> ASButtonNode {
        let button = Self()
        button.backgroundColor = AVSColor.colorMilitaryGreen.color
        button.cornerRadius = 25
        button.style.height = ASDimensionMakeWithPoints(50)
        button.setPrimaryButtonTitle(title)
        return button
    }

    static func secondaryButton(title: String) -> ASButtonNode {
        let button = Self()
        button.backgroundColor = AVSColor.accentForeground.color
        button.cornerRadius = 25
        button.style.height = ASDimensionMakeWithPoints(50)
        button.setSecondaryButtonTitle(title)
        return button
    }

    func setPrimaryButtonTitle(_ title: String) {
        setAttributedTitle(title.attributed(.bodyTitleHeavy, .accentForeground, alignment: .center), for: .normal)
    }
    
    func setSecondaryButtonTitle(_ title: String) {
        setAttributedTitle(title.attributed(.bodyTitleHeavy, .colorMilitaryGreen, alignment: .center), for: .normal)
    }

    static func pillButton(title: String, color: UIColor) -> ASButtonNode {
        let button = ASButtonNode()
//        button.setAttributedTitle(title.attributed(.bodyTitleHeavy, .accentForeground), for: .normal)
        button.setAttributedTitle(title.attributed(.bodyTitleHeavy, .colorMilitaryGreen), for: .normal)
        button.contentEdgeInsets = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
        button.cornerRadius = 25
        button.backgroundColor = color
        return button
    }

    static func greenPillButton(title: String) -> ASButtonNode {
        pillButton(title: title, color: AVSColor.colorGreen.color)
    }
}

extension UIButton {
    static func primaryButton(title: String) -> UIButton {
        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = AVSColor.accentForeground.color
//        button.backgroundColor = AVSColor.accentBackground.color
//        button.backgroundColor = AVSColor.colorMilitaryGreen.color
        button.setTitle(title, for: .normal)
        button.setTitleColor(AVSColor.colorMilitaryGreen.color, for: .normal)
        button.titleLabel?.font = AVSFont.bodyTitleHeavy.font
        button.layer.cornerRadius = 25
        return button
    }
    
    static func secondaryButton(title: String) -> UIButton {
        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = AVSColor.colorMilitaryGreen.color
        button.setTitle(title, for: .normal)
        button.setTitleColor(AVSColor.accentForeground.color, for: .normal)
        button.titleLabel?.font = AVSFont.bodyTitleHeavy.font
        button.layer.cornerRadius = 25
        return button
    }

    static func labelButton(title: String, color: UIColor) -> UIButton {
        let button = UIButton(type: .custom)
        button.setAttributedTitle(title.attributed(AVSFont.button.font, color), for: .normal)
        return button
    }
}

struct BlockButtonModifier: ViewModifier {
    func body(content: Content) -> some View {
//        content.foregroundColor(.accentForeground)
        content.foregroundColor(.colorMilitaryGreen)
               .frame(maxWidth: .infinity)
               .padding()
               .background(Rectangle().fill(Color.avsColor(.accentBackground)))
               .clipShape(Capsule())
    }
}

struct BlockButtonStyle: ButtonStyle {

    struct Btn: View {
        let configuration: ButtonStyle.Configuration
        @Environment(\.isEnabled) private var isEnabled: Bool
        var body: some View {
            configuration.label
//                    .foregroundColor(.accentForeground)
                    .foregroundColor(.colorMilitaryGreen)
                    .frame(maxWidth: .infinity)
                    .padding()
                    .background(Rectangle().fill(buttonFill(configuration: configuration)))
                    .clipShape(Capsule())
        }

        func buttonFill(configuration: ButtonStyle.Configuration) -> Color {
            if !isEnabled {
                return .avsColor(.accentBackground, alpha: 0.5)
            }

            if configuration.isPressed {
                return .avsColor(.accentBackground, alpha: 0.8)
            } else {
                return .avsColor(.accentBackground)
            }
        }
    }

    func makeBody(configuration: ButtonStyle.Configuration) -> some View {
        Btn(configuration: configuration)
    }
}
