//
// Created by Andrei Stoicescu on 26/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit

struct BadgeFactory {
    static func accentConciergeBadge(_ value: Int) -> ConciergeGenericNodes.BadgeNode {
        ConciergeGenericNodes.BadgeNode(value: value, backgroundColor: AVSColor.accentBackground.color)
    }
}