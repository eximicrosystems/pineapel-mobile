//
//  Log.swift
//  A Votre Service
//
//  Created by Andrei Stoicescu on 02/06/2020.
//  Copyright © 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import Alamofire
import Disk

enum LogType {
    case error
    case info
    case warning
    case debug
    case verbose
}

struct Log {
    static var dateFileName: String = "Logs/\(String(Int(Date().timeIntervalSince1970)))"

    static func time() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        let result = formatter.string(from: date)
        return result
    }

    static func this(_ message: Any?, file: String = #file, function: String = #function, line: Int = #line, type: LogType = .debug) {
        #if DEBUG
        let path = file.split(separator: "/")
        let file = path.last?.split(separator: ".")
        var icon = "📝"
        switch type {
        case .debug:
            icon = "💚 DEBUG"
        case .info:
            icon = "💙 INFO"
        case .warning:
            icon = "💛 WARNING"
        case .error:
            icon = "❤️ ERROR"
        case .verbose:
            icon = "💜 VERBOSE"
        }

        var messageString = ""

        if let message: String = message as? String {
            messageString = message
        } else if let message = message {
            messageString = String(describing: message)
        } else {
            messageString = String(describing: message)
        }
        let str = "\(time()) \(icon) \(file?.first ?? "").\(function):\(line) - \t\(messageString)"
        print(str)

        if let logFolder = try? Disk.url(for: "Logs", in: .caches),
           let logFile = try? Disk.url(for: dateFileName, in: .caches),
           let data = "\(str)\n".data(using: .utf8) {
            let fileManager = FileManager.default

            if !Disk.exists(logFolder) {
                try? fileManager.createDirectory(at: logFolder, withIntermediateDirectories: true)
            }

            if fileManager.fileExists(atPath: logFile.path) {
                if let fileHandle = try? FileHandle(forWritingTo: logFile) {
                    fileHandle.seekToEndOfFile()
                    fileHandle.write(data)
                    fileHandle.closeFile()
                }
            } else {
                try? data.write(to: logFile, options: .atomicWrite)
            }
        }
        #endif
    }

    static func thisCall(_ call: URLRequestConvertible) {
        #if DEBUG
        let url = call.urlRequest?.url?.absoluteString ?? ""
        let headers = call.urlRequest?.allHTTPHeaderFields ?? [:]
        let method = call.urlRequest?.httpMethod ?? ""
        let params = String(data: call.urlRequest?.httpBody ?? Data(), encoding: .utf8)
        print("------------------------------------------")
        print("➡️ \(method) \(url) ")
        print("HEADERS: \(headers)")
        print("PARAMETERS: \(params ?? "")")
        print("------------------------------------------")
        #endif
    }

    static func thisResponse<Decodable>(_ response: AFDataResponse<Decodable>) {
        #if DEBUG
        let code = response.response?.statusCode ?? 0
        let url = response.response?.url?.absoluteString ?? ""

        let result = String(describing: response.value)

        if let error = response.error {
            thisError(error)
        }

        var icon = "✅"
        if case .failure = response.result {
            icon = "❌"
        }

        print("------------------------------------------")
        print("\(icon) 🔽 [\(code)] \(url)")
        print("VALUE: \(result)")
        print("\(icon) 🔼 [\(code)] \(url)")
        print("------------------------------------------")
        #endif
    }

    static func thisError(_ error: Error?, file: String = #file, function: String = #function, line: Int = #line) {
        #if DEBUG
        let path = file.split(separator: "/")
        let file = path.last?.split(separator: ".")
        print("------------------------------------------")
        print("❌ \(self.time()) \(file?.first ?? "").\(function):\(line)")

        if let error = error as? APIError {
            if let message = error.message {
                print("ERROR: \(message)")
            } else {
                print("ERROR: \(error)")
            }
            print(error.error)
        } else {
            print("ERROR: \(error)")
        }
        print("------------------------------------------")
        #endif
    }

    static func thisError(_ message: String, file: String = #file, function: String = #function, line: Int = #line) {
        #if DEBUG
        let path = file.split(separator: "/")
        let file = path.last?.split(separator: ".")
        print("------------------------------------------")
        print("❌ \(time()) \(file?.first ?? "").\(function):\(line)")
        print("ERROR: \(message)")
        print("------------------------------------------")
        #endif
    }
}
