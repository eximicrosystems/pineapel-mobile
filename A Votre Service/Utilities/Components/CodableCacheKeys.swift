//
// Created by Andrei Stoicescu on 18.03.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//

import Foundation

struct CodableCacheKey {
    var key: String
}

extension CodableCacheKey {
    static var twilioChannels = CodableCacheKey(key: "twilio_channels")
    static func twilioConversation(_ id: String) -> CodableCacheKey {
        CodableCacheKey(key: "twilio_conversation_\(id)")
    }
}