//
// Created by Andrei Stoicescu on 18.02.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import Disk

struct CodableCache {}

extension CodableCache {
    static func set<T: Encodable>(_ item: T, key: String? = nil) {
        let file = "\(fileName(T.self, key: key)).json"
        Log.this("saving cache to \(file)")
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .iso8601
        try? Disk.save(item, to: .caches, as: file, encoder: encoder)
    }

    static func get<T: Decodable>(_: T.Type, key: String? = nil) -> T? {
        let file = "\(fileName(T.self, key: key)).json"
        Log.this("loading cache from \(file)")
        do {
            return try Disk.retrieve(file, from: .caches, as: T.self)
        } catch let error {
            Log.thisError(error)
        }

        return nil
    }

    static func fileName<T>(_: T.Type, key: String? = nil) -> String {
        "Codable/\(key ?? String(describing: T.self))"
    }

    static func clear() {
        try? Disk.remove("Codable", from: .caches)
    }

    static func remove(_ key: String) {
        try? Disk.remove("Codable/\(key).json", from: .caches)
    }

    static func remove(_ key: CodableCacheKey) {
        try? Disk.remove("Codable/\(key.key).json", from: .caches)
    }
}

extension Encodable {
    func cache(key: String? = nil) {
        CodableCache.set(self, key: key)
    }

    func cache(_ key: CodableCacheKey? = nil) {
        CodableCache.set(self, key: key?.key)
    }
}

extension Decodable {
    static func readCached(key: String? = nil) -> Self? {
        CodableCache.get(Self.self, key: key)
    }

    static func readCached(_ key: CodableCacheKey? = nil) -> Self? {
        CodableCache.get(Self.self, key: key?.key)
    }

    func readCached(key: String? = nil) -> Self? {
        CodableCache.get(Self.self, key: key)
    }

    func readCached(_ key: CodableCacheKey? = nil) -> Self? {
        CodableCache.get(Self.self, key: key?.key)
    }
}