//
//  DefaultsKeys.swift
//  A Votre Service
//
//  Created by Andrei Stoicescu on 01/06/2020.
//  Copyright © 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import SwiftyUserDefaults

extension DefaultsKeys {
    var accessToken: DefaultsKey<String?> { .init("accessToken") }
    var refreshToken: DefaultsKey<String?> { .init("refreshToken") }
    var accessTokenEstimatedExpireDate: DefaultsKey<Date?> { .init("accessTokenEstimatedExpireDate") }
    var user: DefaultsKey<User?> { .init("user") }
    var buildingID: DefaultsKey<String?> { .init("buildingID") }
    var buildingName: DefaultsKey<String?> { .init("buildingName") }
    var buildingTimeZone: DefaultsKey<String?> { .init("buildingTimeZone") }
    var stripeCustomerID: DefaultsKey<String?> { .init("stripe_customer_id") }
    var useBiometrics: DefaultsKey<Bool?> { .init("use_biometrics") }
    var userType: DefaultsKey<UserType> { .init("user_type", defaultValue: .resident) }
    var maintenanceRequestUrl: DefaultsKey<URL?> { .init("maintenance_request_url") }
}
