//
// Created by Andrei Stoicescu on 28/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation

class TimerTrigger {
    private var timer: Timer?
    let time: TimeInterval
    var trigger: (() -> Void)?

    init(time: TimeInterval) {
        self.time = time
    }

    func start() {
        guard timer == nil else { return }
        self.timer = Timer.scheduledTimer(withTimeInterval: time, repeats: true) { [weak self] timer in
            self?.trigger?()
        }
    }

    func stop() {
        self.timer?.invalidate()
        self.timer = nil
    }

    deinit {
        self.timer?.invalidate()
        self.timer = nil
        self.trigger = nil
    }
}