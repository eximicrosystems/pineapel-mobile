//
//  AppStateSwitcher.swift
//  A Votre Service
//
//  Created by Andrei Stoicescu on 02/06/2020.
//  Copyright © 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import SwiftyUserDefaults
import Disk
import SwiftUI
import PromiseKit
import AsyncDisplayKit
import PINCache
import PINRemoteImage
import Combine

class AppState {
    static var firebaseFlags: FirebaseFlags = .init()

    static var features: FeatureFlags = {
        FeatureFlags.cached() ?? .none
    }() {
        didSet {
            features.save()
            featureFlagsChanged?()
        }
    }

    static var featureFlagsChanged: (() -> Void)?

    static var pushToken: String = ""
    static var conciergeShift: ConciergeShift? = nil
    static var pushMessage: PushMessage?

    static var refreshTokenCredentials: RefreshTokenCredentials? = {
        if let refreshToken = Defaults.refreshToken,
           let accessToken = Defaults.accessToken,
           let expiresAt = Defaults.accessTokenEstimatedExpireDate {
            return RefreshTokenCredentials(accessToken: accessToken, refreshToken: refreshToken, expiresAt: expiresAt)
        }
        return nil
    }() {
        didSet {
            if let credentials = refreshTokenCredentials {
                Log.this("NEW ACCESS TOKEN \(credentials.accessToken)")
                Log.this("NEW REFRESH TOKEN \(credentials.refreshToken)")
                Log.this("NEW EXPIRATION DATE \(credentials.expiresAt)")

                Defaults.accessToken = credentials.accessToken
                Defaults.refreshToken = credentials.refreshToken
                Defaults.accessTokenEstimatedExpireDate = credentials.expiresAt
            }
        }
    }

    static var buildingName: String? {
        Defaults.buildingName
    }

    static var buildingTimeZone: TimeZone = {
        if let tzid = Defaults.buildingTimeZone, let tz = TimeZone(identifier: tzid) {
            return tz
        }

        return TimeZone(abbreviation: "UTC")!
    }()

    public static let appVersion: String = {
        let info = Bundle.main.infoDictionary
        let appVersion = info?["CFBundleShortVersionString"] as? String ?? "Unknown Version"

        let osNameVersion: String = {
            let version = ProcessInfo.processInfo.operatingSystemVersion
            let versionString = "\(version.majorVersion).\(version.minorVersion).\(version.patchVersion)"
            return "iOS \(versionString)"
        }()

        return "\(osNameVersion); \(appVersion)"
    }()

    static var isLoggedIn: Bool {
        refreshTokenCredentials != nil
    }

    static var userType: UserType {
        Defaults.userType
    }

    static func reloadBuildingTimeZone() -> Promise<Void> {
        Promise<Void> { seal in
            BuildingDetails.getBuildingTimeZone().done { zone in
                AppState.buildingTimeZone = zone
            }.ensure {
                seal.fulfill(())
            }.cauterize()
        }
    }

    static func switchToSessionsViewController() {
        Appearance.applyForSessions()
        // let hostingVC = UIHostingController(rootView: LoginView())
        // let navigationController = UINavigationController(rootViewController: hostingVC)
        let navigationController = UINavigationController(rootViewController: SessionsInitialViewController())
        switchTo(viewController: navigationController)
    }

    static func switchToMainViewController() {
        Appearance.apply()
        switch Defaults.userType {
        case .resident:
            switchTo(viewController: WelcomePageViewController())
        case .concierge:
            switchTo(viewController: ConciergeTabBarViewController())
        }
    }

    static func switchToUpdateViewController() {
        switchTo(viewController: UpdateAppViewController())
    }

    static func switchToNoShiftViewController() {
        switchTo(viewController: UIHostingController(rootView: NoShiftView()))
    }

    static func switchTo(viewController: UIViewController) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = viewController
//        appDelegate.window?.backgroundColor = AVSColor.tertiaryBackground.color
        appDelegate.window?.backgroundColor = AVSColor.colorMilitaryGreen.color
    }

    static func switchToDefaultViewController() {
        if isLoggedIn {
            switchToMainViewController()
        } else {
            switchToSessionsViewController()
        }
    }

    static func logout(userAction: Bool = false) {
        features = .none
        Defaults.removeAll()

        if userAction {
            KeychainHelper().clearAll()
        }

        if !userAction {
            let biometrics = Biometrics()
            if biometrics.canUseBiometricsHardware {
                biometrics.isEnabled = true
            }
        }

        ConciergeChat.MessagingManager.shared.shutdown()

        FeedVideoResolutionManager.shared.cleanup()
        AppState.switchToSessionsViewController()
        CartProductManager.shared.cleanup()
        ASPINRemoteImageDownloader.shared().sharedPINRemoteImageManager().cache.removeAllObjects()
        VideoCache.clear()
        AVSPushNotifications.shared.logout()

        CodableCache.clear()
    }

    static var shared = AppState()
    private var subscriptions = Set<AnyCancellable>()

    private init() {
        Bootstrapper.shared.$allowedVersion
                .receive(on: DispatchQueue.main)
                .sink { allowed in
                    if !allowed {
                        Self.switchToUpdateViewController()
                    }
                }.store(in: &subscriptions)
    }

    func loadBootstrap() {
        Bootstrapper.shared.loadBootstrap()
    }
}
