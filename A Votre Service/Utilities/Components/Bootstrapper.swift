//
// Created by Andrei Stoicescu on 17.02.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import Combine
import PromiseKit
import Semver_swift
import Firebase

struct FirebaseFlags: Decodable {
    var autoVideoResolution: Bool = false
    var editableFlatReservation: Bool = false

    enum CodingKeys: String, CodingKey {
        case autoVideoResolution = "auto_video_resolution"
        case editableFlatReservation = "editable_flat_reservation"
    }
}

class Bootstrapper: ObservableObject {

    static var shared = Bootstrapper()

    @Published var allowedVersion = true

    private lazy var ref = Database.database().reference()
    private var loading = false
    private var didBootstrap = false

    func loadBootstrap() {
        if didBootstrap { return }
        Log.this("----- BOOTSTRAPING -----")

        guard !loading else { return }
        loading = true

        refreshTokenIfNeeded()

        when(resolved: [
            loadMinVersion(),
            loadFirebaseFlags(),
            reloadBuildingTimeZone()
        ]).done { [weak self] _ in
            self?.loading = false
            self?.didBootstrap = true
            Log.this("BOOTSTRAP COMPLETE")
        }
    }

    private func loadMinVersion() -> Promise<Void> {
        Promise<Void> { seal in
            let currentVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
            let semver = try? Semver(string: currentVersion)

            AuthService().getMinAppVersion().done { version in
                guard let v = version.version,
                      let localSemver = semver,
                      let serverSemver = try? Semver(string: v) else {
                    seal.fulfill(())
                    return
                }

                if localSemver >= serverSemver {
                    seal.fulfill(())
                } else {
                    self.allowedVersion = false
                    seal.fulfill(())
                }
            }.catch { error in
                Log.thisError(error)
                seal.fulfill(())
            }
        }
    }

    private func refreshTokenIfNeeded() {
        if AppState.isLoggedIn, RefreshTokenManager.shared.shouldRefresh {
            Log.this(AppState.refreshTokenCredentials)
            RefreshTokenManager.shared.refresh()
        }
    }

    private func loadFirebaseFlags() -> Promise<Void> {
        Promise<Void> { seal in
            var didFulfill = false

            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                guard !didFulfill else { return }
                didFulfill = true
                seal.fulfill(())
            }

            ref.child(Configuration.default.aVotreService.app.environment.rawValue).child("ios").observeSingleEvent(of: .value, with: { snapshot in
                guard !didFulfill else { return }
                didFulfill = true

                if let flagsJson = snapshot.value as? [AnyHashable: Any]? {
                    if let flags = try? FirebaseFlags(jsonDictionary: flagsJson) {
                        AppState.firebaseFlags = flags
                    }
                    seal.fulfill(())
                } else {
                    seal.fulfill(())
                }
            })
        }
    }

    private func reloadBuildingTimeZone() -> Promise<Void> {
        if AppState.isLoggedIn {
            return AppState.reloadBuildingTimeZone()
        } else {
            return .value(())
        }
    }
}