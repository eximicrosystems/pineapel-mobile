//
// Created by Andrei Stoicescu on 11/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation

extension Float {
    var currencyClean: String {
        self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(format: "%.2f", self)
    }

    var currencyCentClean: String {
        (self / 100).currencyDollarClean
    }

    var currencyDollarClean: String {
        if self >= 0 {
            return "$\((self).currencyClean)"
        } else {
            return "-$\((-self).currencyClean)"
        }
    }
}