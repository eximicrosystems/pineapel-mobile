//
// Created by Andrei Stoicescu on 25/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation

class NSLinkAttributedString {
    let textAttributes: [NSAttributedString.Key: Any]
    var attributedString: NSMutableAttributedString

    private func linkAttributes(url: URL?) -> [NSAttributedString.Key: Any] {
        var linkAttributes = self.textAttributes
        linkAttributes[.link] = url
        return linkAttributes
    }

    init(textAttributes: [NSAttributedString.Key: Any]) {
        self.textAttributes = textAttributes
        self.attributedString = NSMutableAttributedString()
    }

    func append(text: String, url: URL? = nil) {
        if let url = url {
            attributedString.append(NSAttributedString(string: text, attributes: linkAttributes(url: url)))
        } else {
            attributedString.append(NSAttributedString(string: text, attributes: textAttributes))
        }
    }
}
