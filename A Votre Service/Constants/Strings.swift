// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name vertical_whitespace_opening_braces
internal enum L10n {

  internal enum ALaCarte {
    internal enum Menu {
      /// Car Wash
      internal static let carWash = L10n.tr("Localizable", "a-la-carte.menu.car-wash")
      /// Dog Walking
      internal static let dogWalking = L10n.tr("Localizable", "a-la-carte.menu.dog-walking")
      /// Gourmet Cart
      internal static let gourmetCart = L10n.tr("Localizable", "a-la-carte.menu.gourmet-cart")
      /// Helping Hand
      internal static let helpingHand = L10n.tr("Localizable", "a-la-carte.menu.helping-hand")
      /// Maid Service
      internal static let maidService = L10n.tr("Localizable", "a-la-carte.menu.maid-service")
      /// Personal Trainer
      internal static let personalTrainer = L10n.tr("Localizable", "a-la-carte.menu.personal-trainer")
    }
    internal enum Navigation {
      /// YOUR CONCIERGE:
      internal static let title = L10n.tr("Localizable", "a-la-carte.navigation.title")
    }
  }

  internal enum AllowedGuestEditScreen {
    /// Allowed guest has been updated
    internal static let guestUpdated = L10n.tr("Localizable", "allowed-guest-edit-screen.guest-updated")
    internal enum Buttons {
      internal enum Save {
        /// Save Guest
        internal static let title = L10n.tr("Localizable", "allowed-guest-edit-screen.buttons.save.title")
      }
    }
    internal enum Errors {
      /// You must select a time interval
      internal static let frequencyIntervalMissing = L10n.tr("Localizable", "allowed-guest-edit-screen.errors.frequency-interval-missing")
    }
    internal enum Fields {
      internal enum Frequency {
        /// Frequency
        internal static let title = L10n.tr("Localizable", "allowed-guest-edit-screen.fields.frequency.title")
        internal enum Options {
          internal enum AllowedOnce {
            /// Allow Once
            internal static let title = L10n.tr("Localizable", "allowed-guest-edit-screen.fields.frequency.options.allowed-once.title")
          }
          internal enum Always {
            /// Always
            internal static let title = L10n.tr("Localizable", "allowed-guest-edit-screen.fields.frequency.options.always.title")
          }
          internal enum OneMonth {
            /// One Month
            internal static let title = L10n.tr("Localizable", "allowed-guest-edit-screen.fields.frequency.options.one-month.title")
          }
          internal enum OneWeek {
            /// One Week
            internal static let title = L10n.tr("Localizable", "allowed-guest-edit-screen.fields.frequency.options.one-week.title")
          }
        }
      }
      internal enum Info {
        /// Info
        internal static let title = L10n.tr("Localizable", "allowed-guest-edit-screen.fields.info.title")
      }
      internal enum Name {
        /// Full Name
        internal static let title = L10n.tr("Localizable", "allowed-guest-edit-screen.fields.name.title")
      }
      internal enum Phone {
        /// Phone Number
        internal static let title = L10n.tr("Localizable", "allowed-guest-edit-screen.fields.phone.title")
      }
    }
    internal enum Labels {
      /// Information
      internal static let info = L10n.tr("Localizable", "allowed-guest-edit-screen.labels.info")
      /// Select Interval
      internal static let selectInterval = L10n.tr("Localizable", "allowed-guest-edit-screen.labels.select-interval")
    }
    internal enum Navigation {
      /// Add Guest
      internal static let title = L10n.tr("Localizable", "allowed-guest-edit-screen.navigation.title")
    }
  }

  internal enum AllowedGuestListScreen {
    internal enum Actions {
      internal enum RemoveGuest {
        /// Cancel
        internal static let cancel = L10n.tr("Localizable", "allowed-guest-list-screen.actions.remove-guest.cancel")
        /// this resident guest
        internal static let general = L10n.tr("Localizable", "allowed-guest-list-screen.actions.remove-guest.general")
        /// OK
        internal static let ok = L10n.tr("Localizable", "allowed-guest-list-screen.actions.remove-guest.ok")
        /// Are you sure you want to remove %@?
        internal static func title(_ p1: Any) -> String {
          return L10n.tr("Localizable", "allowed-guest-list-screen.actions.remove-guest.title", String(describing: p1))
        }
      }
    }
    internal enum Buttons {
      internal enum AddGuest {
        /// Add Guest
        internal static let title = L10n.tr("Localizable", "allowed-guest-list-screen.buttons.add-guest.title")
      }
      internal enum Cancel {
        /// Cancel
        internal static let title = L10n.tr("Localizable", "allowed-guest-list-screen.buttons.cancel.title")
      }
      internal enum EditGuest {
        /// Edit Guest
        internal static let title = L10n.tr("Localizable", "allowed-guest-list-screen.buttons.edit-guest.title")
      }
      internal enum RemoveGuest {
        /// Remove Guest
        internal static let title = L10n.tr("Localizable", "allowed-guest-list-screen.buttons.remove-guest.title")
      }
    }
    internal enum Navigation {
      /// Allowed Guests
      internal static let title = L10n.tr("Localizable", "allowed-guest-list-screen.navigation.title")
    }
  }

  internal enum AmenityListScreen {
    internal enum Navigation {
      /// RESERVE
      internal static let tabBarTitle = L10n.tr("Localizable", "amenity-list-screen.navigation.tab-bar-title")
      /// Reservation
      internal static let title = L10n.tr("Localizable", "amenity-list-screen.navigation.title")
    }
  }

  internal enum AmenityReservation {
    internal enum Buttons {
      internal enum CancelReservation {
        /// Cancel Reservation
        internal static let title = L10n.tr("Localizable", "amenity-reservation.buttons.cancel-reservation.title")
      }
      internal enum CheckAvailability {
        /// Check Availability
        internal static let title = L10n.tr("Localizable", "amenity-reservation.buttons.check-availability.title")
      }
      internal enum Reserve {
        /// Reserve
        internal static let title = L10n.tr("Localizable", "amenity-reservation.buttons.reserve.title")
      }
      internal enum ReserveAndPay {
        /// Reserve & Pay
        internal static let title = L10n.tr("Localizable", "amenity-reservation.buttons.reserve-and-pay.title")
      }
    }
    internal enum Fields {
      internal enum Comment {
        /// Add Your Comment
        internal static let placeholder = L10n.tr("Localizable", "amenity-reservation.fields.comment.placeholder")
        /// Add a Comment
        internal static let title = L10n.tr("Localizable", "amenity-reservation.fields.comment.title")
      }
      internal enum EndTime {
        /// To
        internal static let placeholder = L10n.tr("Localizable", "amenity-reservation.fields.end-time.placeholder")
      }
      internal enum ReserveDate {
        /// Select Date
        internal static let datepickerTitle = L10n.tr("Localizable", "amenity-reservation.fields.reserve-date.datepicker-title")
        /// Reservation Date
        internal static let placeholder = L10n.tr("Localizable", "amenity-reservation.fields.reserve-date.placeholder")
      }
      internal enum StartTime {
        /// Start Time
        internal static let placeholder = L10n.tr("Localizable", "amenity-reservation.fields.start-time.placeholder")
      }
    }
    internal enum Labels {
      /// Available From:
      internal static let availableFrom = L10n.tr("Localizable", "amenity-reservation.labels.available-from")
      /// Are you sure that you want to cancel this reservation?
      internal static let cancelConfirmation = L10n.tr("Localizable", "amenity-reservation.labels.cancel-confirmation")
      /// Not Available
      internal static let notAvailable = L10n.tr("Localizable", "amenity-reservation.labels.not-available")
      internal enum ThankYou {
        /// View Reservation
        internal static let button = L10n.tr("Localizable", "amenity-reservation.labels.thank-you.button")
        /// Someone will be in touch with you soon.
        internal static let subtitle = L10n.tr("Localizable", "amenity-reservation.labels.thank-you.subtitle")
        /// Thank you for your reservation
        internal static let title = L10n.tr("Localizable", "amenity-reservation.labels.thank-you.title")
      }
    }
    internal enum ReservationSummary {
      /// Date
      internal static let date = L10n.tr("Localizable", "amenity-reservation.reservation-summary.date")
      /// Duration
      internal static let duration = L10n.tr("Localizable", "amenity-reservation.reservation-summary.duration")
      /// Edit
      internal static let edit = L10n.tr("Localizable", "amenity-reservation.reservation-summary.edit")
      /// Price
      internal static let price = L10n.tr("Localizable", "amenity-reservation.reservation-summary.price")
      /// Time
      internal static let time = L10n.tr("Localizable", "amenity-reservation.reservation-summary.time")
    }
  }

  internal enum BuildingInfoScreen {
    internal enum Navigation {
      /// Building Info
      internal static let title = L10n.tr("Localizable", "building-info-screen.navigation.title")
    }
    internal enum Sections {
      internal enum AmenityInfo {
        /// Amenities
        internal static let title = L10n.tr("Localizable", "building-info-screen.sections.amenity-info.title")
      }
    }
  }

  internal enum ChangeEmailScreen {
    internal enum Buttons {
      internal enum Save {
        /// Save
        internal static let title = L10n.tr("Localizable", "change-email-screen.buttons.save.title")
      }
    }
    internal enum Fields {
      internal enum Email {
        /// Email
        internal static let placeholder = L10n.tr("Localizable", "change-email-screen.fields.email.placeholder")
      }
      internal enum Password {
        /// Your password is required in order to change the email
        internal static let note = L10n.tr("Localizable", "change-email-screen.fields.password.note")
        /// Password
        internal static let placeholder = L10n.tr("Localizable", "change-email-screen.fields.password.placeholder")
      }
    }
    internal enum Messages {
      /// Your email was changed successfully
      internal static let emailChanged = L10n.tr("Localizable", "change-email-screen.messages.email-changed")
    }
    internal enum Navigation {
      /// Change Email
      internal static let title = L10n.tr("Localizable", "change-email-screen.navigation.title")
    }
  }

  internal enum ChangePasswordScreen {
    internal enum Buttons {
      internal enum Save {
        /// Save
        internal static let title = L10n.tr("Localizable", "change-password-screen.buttons.save.title")
      }
    }
    internal enum Fields {
      internal enum ConfirmPassword {
        /// Confirm New Password
        internal static let placeholder = L10n.tr("Localizable", "change-password-screen.fields.confirm-password.placeholder")
      }
      internal enum NewPassword {
        /// New Password
        internal static let placeholder = L10n.tr("Localizable", "change-password-screen.fields.new-password.placeholder")
      }
      internal enum Password {
        /// Current Password
        internal static let placeholder = L10n.tr("Localizable", "change-password-screen.fields.password.placeholder")
      }
    }
    internal enum Messages {
      /// Your password was changed successfully
      internal static let passwordChanged = L10n.tr("Localizable", "change-password-screen.messages.password-changed")
    }
    internal enum Navigation {
      /// Change Password
      internal static let title = L10n.tr("Localizable", "change-password-screen.navigation.title")
    }
  }

  internal enum ChatScreen {
    internal enum Buttons {
      internal enum Resend {
        /// Resend
        internal static let title = L10n.tr("Localizable", "chat-screen.buttons.resend.title")
      }
      internal enum Send {
        /// Send
        internal static let title = L10n.tr("Localizable", "chat-screen.buttons.send.title")
      }
    }
    internal enum Inputs {
      internal enum MessageInput {
        /// Type Something...
        internal static let placeholder = L10n.tr("Localizable", "chat-screen.inputs.message-input.placeholder")
      }
    }
    internal enum Navigation {
      /// CHAT
      internal static let tabBarTitle = L10n.tr("Localizable", "chat-screen.navigation.tab-bar-title")
      /// YOUR CONCIERGE:
      internal static let title = L10n.tr("Localizable", "chat-screen.navigation.title")
    }
  }

  internal enum ConciergeListScreen {
    internal enum Navigation {
      /// Concierges
      internal static let title = L10n.tr("Localizable", "concierge-list-screen.navigation.title")
    }
  }

  internal enum CreateFeedbackScreen {
    /// Feedback has been sent
    internal static let feedbackSent = L10n.tr("Localizable", "create-feedback-screen.feedback-sent")
    internal enum Buttons {
      internal enum Save {
        /// Submit
        internal static let title = L10n.tr("Localizable", "create-feedback-screen.buttons.save.title")
      }
    }
    internal enum Fields {
      internal enum Comment {
        /// Add Your Comment
        internal static let placeholder = L10n.tr("Localizable", "create-feedback-screen.fields.comment.placeholder")
        /// Leave a Comment
        internal static let title = L10n.tr("Localizable", "create-feedback-screen.fields.comment.title")
      }
      internal enum Concierge {
        /// Select Concierge
        internal static let title = L10n.tr("Localizable", "create-feedback-screen.fields.concierge.title")
      }
      internal enum FeedbackType {
        /// Type of Feedback
        internal static let title = L10n.tr("Localizable", "create-feedback-screen.fields.feedback-type.title")
        internal enum Options {
          /// Building
          internal static let building = L10n.tr("Localizable", "create-feedback-screen.fields.feedback-type.options.building")
          /// Concierge
          internal static let concierge = L10n.tr("Localizable", "create-feedback-screen.fields.feedback-type.options.concierge")
          /// Other
          internal static let other = L10n.tr("Localizable", "create-feedback-screen.fields.feedback-type.options.other")
        }
      }
      internal enum Rating {
        /// Rating
        internal static let title = L10n.tr("Localizable", "create-feedback-screen.fields.rating.title")
      }
    }
    internal enum Navigation {
      /// Feedback
      internal static let title = L10n.tr("Localizable", "create-feedback-screen.navigation.title")
    }
  }

  internal enum Empty {
    internal enum AllowedGuests {
      /// Add Guest
      internal static let action = L10n.tr("Localizable", "empty.allowed-guests.action")
      /// Easily add guests for single\nor multiple visits.
      internal static let subtitle = L10n.tr("Localizable", "empty.allowed-guests.subtitle")
      /// Expecting a Visitor?
      internal static let title = L10n.tr("Localizable", "empty.allowed-guests.title")
    }
    internal enum AmenityList {
      /// Chat
      internal static let action = L10n.tr("Localizable", "empty.amenity-list.action")
      /// For more information\nchat with the concierge.
      internal static let subtitle = L10n.tr("Localizable", "empty.amenity-list.subtitle")
      /// Amenity Reservations\nare not Currently Available
      internal static let title = L10n.tr("Localizable", "empty.amenity-list.title")
    }
    internal enum BellNotifications {
      /// Chat
      internal static let action = L10n.tr("Localizable", "empty.bell-notifications.action")
      /// If you need assistance\nchat with the concierge.
      internal static let subtitle = L10n.tr("Localizable", "empty.bell-notifications.subtitle")
      /// Everything is quiet here.
      internal static let title = L10n.tr("Localizable", "empty.bell-notifications.title")
    }
    internal enum Cart {
      /// Close
      internal static let action = L10n.tr("Localizable", "empty.cart.action")
      /// Add items from the A La Carte Menu
      internal static let subtitle = L10n.tr("Localizable", "empty.cart.subtitle")
      /// You cart is empty
      internal static let title = L10n.tr("Localizable", "empty.cart.title")
    }
    internal enum Chat {
      /// Chat
      internal static let action = L10n.tr("Localizable", "empty.chat.action")
      /// We are happy to help \nwith your needs.
      internal static let subtitle = L10n.tr("Localizable", "empty.chat.subtitle")
      /// Need Assistance? Let’s Chat.
      internal static let title = L10n.tr("Localizable", "empty.chat.title")
    }
    internal enum ConciergeAmenityReservations {
      /// Amenity Reservations for today will show up here
      internal static let subtitle = L10n.tr("Localizable", "empty.concierge-amenity-reservations.subtitle")
      /// Everything is quiet here.
      internal static let title = L10n.tr("Localizable", "empty.concierge-amenity-reservations.title")
    }
    internal enum LeaseOffice {
      /// Chat
      internal static let action = L10n.tr("Localizable", "empty.lease-office.action")
      /// If you need assistance\nchat with the concierge.
      internal static let subtitle = L10n.tr("Localizable", "empty.lease-office.subtitle")
      /// Everything is quiet here.
      internal static let title = L10n.tr("Localizable", "empty.lease-office.title")
    }
    internal enum Notes {
      /// Create a Note
      internal static let action = L10n.tr("Localizable", "empty.notes.action")
      /// Your Notes will show up here
      internal static let subtitle = L10n.tr("Localizable", "empty.notes.subtitle")
      /// Everything is quiet here.
      internal static let title = L10n.tr("Localizable", "empty.notes.title")
    }
    internal enum OrderList {
      /// Close
      internal static let action = L10n.tr("Localizable", "empty.order-list.action")
      /// Amenity Reservations and A La Carte orders will show up here
      internal static let subtitle = L10n.tr("Localizable", "empty.order-list.subtitle")
      /// No Orders or Reservations Here!
      internal static let title = L10n.tr("Localizable", "empty.order-list.title")
    }
    internal enum ProductList {
      /// Chat
      internal static let action = L10n.tr("Localizable", "empty.product-list.action")
      /// Speak with the concierge for more info.
      internal static let subtitle = L10n.tr("Localizable", "empty.product-list.subtitle")
      /// Product & Services\nComing Soon
      internal static let title = L10n.tr("Localizable", "empty.product-list.title")
    }
    internal enum Request {
      /// Request
      internal static let action = L10n.tr("Localizable", "empty.request.action")
      /// Submit a request and let us \ntake care of the rest.
      internal static let subtitle = L10n.tr("Localizable", "empty.request.subtitle")
      /// How Can We Help?
      internal static let title = L10n.tr("Localizable", "empty.request.title")
    }
    internal enum Reservation {
      /// Reserve
      internal static let action = L10n.tr("Localizable", "empty.reservation.action")
      /// Ensure what you want is\nready when you want it.
      internal static let subtitle = L10n.tr("Localizable", "empty.reservation.subtitle")
      /// Make a Reservation.
      internal static let title = L10n.tr("Localizable", "empty.reservation.title")
    }
    internal enum Subscriptions {
      /// Chat
      internal static let action = L10n.tr("Localizable", "empty.subscriptions.action")
      /// If you need assistance\nchat with the concierge.
      internal static let subtitle = L10n.tr("Localizable", "empty.subscriptions.subtitle")
      /// Everything is quiet here.
      internal static let title = L10n.tr("Localizable", "empty.subscriptions.title")
    }
    internal enum UserNotifications {
      /// Chat
      internal static let action = L10n.tr("Localizable", "empty.user-notifications.action")
      /// If you need assistance\nchat with the concierge.
      internal static let subtitle = L10n.tr("Localizable", "empty.user-notifications.subtitle")
      /// we are here to serve you.
      internal static let title = L10n.tr("Localizable", "empty.user-notifications.title")
    }
  }

  internal enum Errors {
    /// %@ can't be blank
    internal static func cantBeBlank(_ p1: Any) -> String {
      return L10n.tr("Localizable", "errors.cant-be-blank", String(describing: p1))
    }
    /// Email address is invalid
    internal static let emailInvalid = L10n.tr("Localizable", "errors.email-invalid")
    /// There was an error while loading this image
    internal static let loadingImage = L10n.tr("Localizable", "errors.loading-image")
    /// %@ must be present
    internal static func mustBePresent(_ p1: Any) -> String {
      return L10n.tr("Localizable", "errors.must-be-present", String(describing: p1))
    }
    /// Password is invalid
    internal static let passwordInvalid = L10n.tr("Localizable", "errors.password-invalid")
    /// Password must match confirmation
    internal static let passwordMustMatch = L10n.tr("Localizable", "errors.password-must-match")
    /// Unknown error
    internal static let unknown = L10n.tr("Localizable", "errors.unknown")
    /// There was an error while updating your profile
    internal static let updatingProfile = L10n.tr("Localizable", "errors.updating-profile")
  }

  internal enum ForceUpdate {
    /// Tap here to update
    internal static let subtitle = L10n.tr("Localizable", "force-update.subtitle")
    /// You are running an old version of\nA Votre Service
    internal static let title = L10n.tr("Localizable", "force-update.title")
  }

  internal enum General {
    /// and
    internal static let and = L10n.tr("Localizable", "general.and")
    /// Cancel
    internal static let cancel = L10n.tr("Localizable", "general.cancel")
    /// Close
    internal static let close = L10n.tr("Localizable", "general.close")
    /// Delete
    internal static let delete = L10n.tr("Localizable", "general.delete")
    /// Done
    internal static let done = L10n.tr("Localizable", "general.done")
    /// Edit
    internal static let edit = L10n.tr("Localizable", "general.edit")
    /// hour
    internal static let hour = L10n.tr("Localizable", "general.hour")
    /// hours
    internal static let hours = L10n.tr("Localizable", "general.hours")
    /// minute
    internal static let minute = L10n.tr("Localizable", "general.minute")
    /// minutes
    internal static let minutes = L10n.tr("Localizable", "general.minutes")
    /// No
    internal static let no = L10n.tr("Localizable", "general.no")
    /// Yes
    internal static let yes = L10n.tr("Localizable", "general.yes")
  }

  internal enum HelpScreen {
    internal enum Fields {
      internal enum Faq {
        /// Search...
        internal static let placeholder = L10n.tr("Localizable", "help-screen.fields.faq.placeholder")
        /// FAQ
        internal static let title = L10n.tr("Localizable", "help-screen.fields.faq.title")
      }
    }
    internal enum Navigation {
      /// HELP
      internal static let title = L10n.tr("Localizable", "help-screen.navigation.title")
    }
  }

  internal enum Models {
    internal enum Concierge {
      internal enum ConciergeType {
        /// Concierge
        internal static let concierge = L10n.tr("Localizable", "models.concierge.concierge_type.concierge")
        /// Head Concierge
        internal static let headConcierge = L10n.tr("Localizable", "models.concierge.concierge_type.head_concierge")
      }
    }
  }

  internal enum MoreScreen {
    internal enum Actions {
      internal enum Logout {
        /// Cancel
        internal static let cancel = L10n.tr("Localizable", "more-screen.actions.logout.cancel")
        /// Are you sure you want to log out?
        internal static let confirm = L10n.tr("Localizable", "more-screen.actions.logout.confirm")
        /// OK
        internal static let ok = L10n.tr("Localizable", "more-screen.actions.logout.ok")
      }
    }
    internal enum Items {
      /// A La Carte
      internal static let aLaCarte = L10n.tr("Localizable", "more-screen.items.a-la-carte")
      /// Allowed Guests
      internal static let allowedGuestList = L10n.tr("Localizable", "more-screen.items.allowed-guest-list")
      /// My Reservations
      internal static let amenityReservations = L10n.tr("Localizable", "more-screen.items.amenity-reservations")
      /// Building Info
      internal static let buildingInfo = L10n.tr("Localizable", "more-screen.items.building-info")
      /// Concierges
      internal static let concierges = L10n.tr("Localizable", "more-screen.items.concierges")
      /// Feedback
      internal static let feedback = L10n.tr("Localizable", "more-screen.items.feedback")
      /// Help
      internal static let help = L10n.tr("Localizable", "more-screen.items.help")
      /// LOG OUT
      internal static let logout = L10n.tr("Localizable", "more-screen.items.logout")
      /// Maintenance Request
      internal static let maintenanceRequest = L10n.tr("Localizable", "more-screen.items.maintenance-request")
      /// Notifications
      internal static let notifications = L10n.tr("Localizable", "more-screen.items.notifications")
      /// Order History
      internal static let orderHistory = L10n.tr("Localizable", "more-screen.items.order-history")
      /// Payment
      internal static let payment = L10n.tr("Localizable", "more-screen.items.payment")
      /// Privacy Policy
      internal static let privacy = L10n.tr("Localizable", "more-screen.items.privacy")
      /// Profile
      internal static let profile = L10n.tr("Localizable", "more-screen.items.profile")
      /// Submit Concierge Request
      internal static let requests = L10n.tr("Localizable", "more-screen.items.requests")
      /// Terms & Conditions
      internal static let terms = L10n.tr("Localizable", "more-screen.items.terms")
      /// Things To Know
      internal static let thingsToKnow = L10n.tr("Localizable", "more-screen.items.things-to-know")
    }
    internal enum Navigation {
      /// More
      internal static let tabBarTitle = L10n.tr("Localizable", "more-screen.navigation.tab-bar-title")
      /// More
      internal static let title = L10n.tr("Localizable", "more-screen.navigation.title")
    }
  }

  internal enum PaymentsScreen {
    internal enum Navigation {
      /// PAYMENT
      internal static let title = L10n.tr("Localizable", "payments-screen.navigation.title")
    }
  }

  internal enum ProfileScreen {
    internal enum Buttons {
      internal enum Save {
        /// Save
        internal static let title = L10n.tr("Localizable", "profile-screen.buttons.save.title")
      }
    }
    internal enum Fields {
      internal enum Email {
        /// Email
        internal static let placeholder = L10n.tr("Localizable", "profile-screen.fields.email.placeholder")
      }
      internal enum FirstName {
        /// First Name
        internal static let placeholder = L10n.tr("Localizable", "profile-screen.fields.first_name.placeholder")
      }
      internal enum LastName {
        /// Last Name
        internal static let placeholder = L10n.tr("Localizable", "profile-screen.fields.last_name.placeholder")
      }
      internal enum Password {
        /// Password
        internal static let placeholder = L10n.tr("Localizable", "profile-screen.fields.password.placeholder")
      }
      internal enum Phone {
        /// Phone Number
        internal static let placeholder = L10n.tr("Localizable", "profile-screen.fields.phone.placeholder")
      }
    }
    internal enum Messages {
      /// Your profile was updated successfully
      internal static let profileChanged = L10n.tr("Localizable", "profile-screen.messages.profile-changed")
    }
    internal enum Navigation {
      /// PROFILE
      internal static let title = L10n.tr("Localizable", "profile-screen.navigation.title")
    }
  }

  internal enum SessionsScreen {
    /// Create Password.
    internal static let createPassword = L10n.tr("Localizable", "sessions-screen.create-password")
    /// Enter Password.
    internal static let enterPassword = L10n.tr("Localizable", "sessions-screen.enter-password")
    /// Always At Your Service
    internal static let subtitle = L10n.tr("Localizable", "sessions-screen.subtitle")
    /// GATEWAY TO SWEET LIVING
    internal static let title = L10n.tr("Localizable", "sessions-screen.title")
    internal enum Buttons {
      /// Login with %@
      internal static func loginWith(_ p1: Any) -> String {
        return L10n.tr("Localizable", "sessions-screen.buttons.login-with", String(describing: p1))
      }
      internal enum CreateAccount {
        /// Don't have an account? Create one.
        internal static let title = L10n.tr("Localizable", "sessions-screen.buttons.create-account.title")
      }
      internal enum CreateAccountSubmit {
        /// Create Account
        internal static let title = L10n.tr("Localizable", "sessions-screen.buttons.create-account-submit.title")
      }
      internal enum ForgotPassword {
        /// Forgot Password?
        internal static let title = L10n.tr("Localizable", "sessions-screen.buttons.forgot-password.title")
      }
      internal enum Login {
        /// ACCESS
        internal static let title = L10n.tr("Localizable", "sessions-screen.buttons.login.title")
      }
      internal enum Submit {
        /// Submit
        internal static let title = L10n.tr("Localizable", "sessions-screen.buttons.submit.title")
      }
    }
    internal enum Fields {
      internal enum EmailAddress {
        /// Enter the email address associated with your lease
        internal static let note = L10n.tr("Localizable", "sessions-screen.fields.email-address.note")
        /// Email
        internal static let placeholder = L10n.tr("Localizable", "sessions-screen.fields.email-address.placeholder")
      }
      internal enum Password {
        /// For your security a verification code has been sent to your email, then create new password.
        internal static let note = L10n.tr("Localizable", "sessions-screen.fields.password.note")
        /// Password
        internal static let placeholder = L10n.tr("Localizable", "sessions-screen.fields.password.placeholder")
      }
      internal enum VerificationCode {
        /// Verification Code
        internal static let placeholder = L10n.tr("Localizable", "sessions-screen.fields.verification-code.placeholder")
      }
    }
  }

  internal enum SettingsScreen {
    internal enum Fields {
      internal enum ChatMessages {
        /// Concierge Chat Messages
        internal static let title = L10n.tr("Localizable", "settings-screen.fields.chat-messages.title")
      }
      internal enum Deliveries {
        /// Deliveries
        internal static let title = L10n.tr("Localizable", "settings-screen.fields.deliveries.title")
      }
      internal enum Fitness {
        /// Fitness
        internal static let title = L10n.tr("Localizable", "settings-screen.fields.fitness.title")
      }
      internal enum Information {
        /// Information
        internal static let title = L10n.tr("Localizable", "settings-screen.fields.information.title")
      }
      internal enum MailDelivery {
        /// Mail Delivery
        internal static let title = L10n.tr("Localizable", "settings-screen.fields.mail-delivery.title")
      }
      internal enum Packages {
        /// Packages
        internal static let title = L10n.tr("Localizable", "settings-screen.fields.packages.title")
      }
    }
    internal enum Navigation {
      /// Notification Settings
      internal static let title = L10n.tr("Localizable", "settings-screen.navigation.title")
    }
  }

  internal enum Terms {
    /// By continuing, you agree to A Votre Service's 
    internal static let byContinuing = L10n.tr("Localizable", "terms.by-continuing")
    ///  and confirm that you have read A Votre Service's 
    internal static let confirmRead = L10n.tr("Localizable", "terms.confirm-read")
    /// Privacy Policy
    internal static let privacyPolicy = L10n.tr("Localizable", "terms.privacy-policy")
    /// Terms of Use
    internal static let terms = L10n.tr("Localizable", "terms.terms")
  }

  internal enum ThingsToKnowScreen {
    internal enum Navigation {
      /// Things to Know
      internal static let tabBarTitle = L10n.tr("Localizable", "things-to-know-screen.navigation.tab-bar-title")
      /// Things to Know
      internal static let title = L10n.tr("Localizable", "things-to-know-screen.navigation.title")
    }
  }

  internal enum UserNotifications {
    internal enum Labels {
      /// Category
      internal static let category = L10n.tr("Localizable", "user-notifications.labels.category")
      /// Delete this notification?
      internal static let deleteNotificationConfirm = L10n.tr("Localizable", "user-notifications.labels.delete-notification-confirm")
      /// Delivery time
      internal static let deliveryTime = L10n.tr("Localizable", "user-notifications.labels.delivery-time")
      /// Duration
      internal static let duration = L10n.tr("Localizable", "user-notifications.labels.duration")
      /// Event Date
      internal static let eventDate = L10n.tr("Localizable", "user-notifications.labels.event-date")
      /// Details
      internal static let eventDetails = L10n.tr("Localizable", "user-notifications.labels.event-details")
      /// Host
      internal static let eventHost = L10n.tr("Localizable", "user-notifications.labels.event-host")
      /// Event Name
      internal static let eventName = L10n.tr("Localizable", "user-notifications.labels.event-name")
      /// Information
      internal static let information = L10n.tr("Localizable", "user-notifications.labels.information")
      /// Instructor
      internal static let instructor = L10n.tr("Localizable", "user-notifications.labels.instructor")
      /// Item for Delivery
      internal static let itemForDelivery = L10n.tr("Localizable", "user-notifications.labels.item-for-delivery")
      /// Number
      internal static let number = L10n.tr("Localizable", "user-notifications.labels.number")
      /// On Duty
      internal static let onDuty = L10n.tr("Localizable", "user-notifications.labels.on-duty")
      /// Signed By
      internal static let signedBy = L10n.tr("Localizable", "user-notifications.labels.signed-by")
      /// Start Date
      internal static let startDate = L10n.tr("Localizable", "user-notifications.labels.start-date")
      /// Type
      internal static let type = L10n.tr("Localizable", "user-notifications.labels.type")
    }
  }

  internal enum UserNotificationsScreen {
    internal enum Navigation {
      /// FEED
      internal static let tabBarTitle = L10n.tr("Localizable", "user-notifications-screen.navigation.tab-bar-title")
    }
  }

  internal enum UserRequest {
    internal enum Labels {
      /// Create a Request
      internal static let createRequest = L10n.tr("Localizable", "user-request.labels.create-request")
      /// Request sent successfully
      internal static let requestSuccess = L10n.tr("Localizable", "user-request.labels.request-success")
    }
    internal enum Status {
      /// Accepted
      internal static let accepted = L10n.tr("Localizable", "user-request.status.accepted")
      /// Completed
      internal static let done = L10n.tr("Localizable", "user-request.status.done")
      /// Submitted
      internal static let pending = L10n.tr("Localizable", "user-request.status.pending")
    }
  }

  internal enum UserRequestEditScreen {
    internal enum Buttons {
      internal enum Submit {
        /// Submit Concierge Request
        internal static let title = L10n.tr("Localizable", "user-request-edit-screen.buttons.submit.title")
      }
    }
    internal enum Fields {
      internal enum Comment {
        /// Comment
        internal static let placeholder = L10n.tr("Localizable", "user-request-edit-screen.fields.comment.placeholder")
        /// Add your Comment
        internal static let title = L10n.tr("Localizable", "user-request-edit-screen.fields.comment.title")
      }
      internal enum Name {
        /// Request Subject
        internal static let placeholder = L10n.tr("Localizable", "user-request-edit-screen.fields.name.placeholder")
      }
    }
    internal enum Labels {
      /// Request was created
      internal static let requestCreated = L10n.tr("Localizable", "user-request-edit-screen.labels.request-created")
    }
    internal enum Navigation {
      /// ADD REQUEST
      internal static let title = L10n.tr("Localizable", "user-request-edit-screen.navigation.title")
    }
  }

  internal enum UserRequestsScreen {
    internal enum Navigation {
      /// REQUESTS
      internal static let title = L10n.tr("Localizable", "user-requests-screen.navigation.title")
    }
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name vertical_whitespace_opening_braces

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = BundleToken.bundle.localizedString(forKey: key, value: nil, table: table)
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
