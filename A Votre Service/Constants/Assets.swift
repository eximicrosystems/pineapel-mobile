// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(macOS)
  import AppKit
#elseif os(iOS)
  import UIKit
#elseif os(tvOS) || os(watchOS)
  import UIKit
#endif

// Deprecated typealiases
@available(*, deprecated, renamed: "ColorAsset.Color", message: "This typealias will be removed in SwiftGen 7.0")
internal typealias AssetColorTypeAlias = ColorAsset.Color
@available(*, deprecated, renamed: "ImageAsset.Image", message: "This typealias will be removed in SwiftGen 7.0")
internal typealias AssetImageTypeAlias = ImageAsset.Image

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Asset Catalogs

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
internal enum Asset {
  internal enum Colors {
    internal enum Semantic {
      internal static let accentFill = ColorAsset(name: "Semantic/accentFill")
      internal static let accentLabel = ColorAsset(name: "Semantic/accentLabel")
      internal static let borderPrimary = ColorAsset(name: "Semantic/borderPrimary")
      internal static let calendarSelectionFill = ColorAsset(name: "Semantic/calendarSelectionFill")
      internal static let chatInputFill = ColorAsset(name: "Semantic/chatInputFill")
      internal static let chatMessageFill = ColorAsset(name: "Semantic/chatMessageFill")
      internal static let colorAccentLight = ColorAsset(name: "Semantic/colorAccentLight")
      internal static let colorBlue = ColorAsset(name: "Semantic/colorBlue")
      internal static let colorGreen = ColorAsset(name: "Semantic/colorGreen")
      internal static let colorMilitary = ColorAsset(name: "Semantic/colorMilitary")
      internal static let colorOrange = ColorAsset(name: "Semantic/colorOrange")
      internal static let colorPurple = ColorAsset(name: "Semantic/colorPurple")
      internal static let colorTeal = ColorAsset(name: "Semantic/colorTeal")
      internal static let colorYellow = ColorAsset(name: "Semantic/colorYellow")
      internal static let colorYellowRating = ColorAsset(name: "Semantic/colorYellowRating")
      internal static let divider = ColorAsset(name: "Semantic/divider")
      internal static let dividerSecondary = ColorAsset(name: "Semantic/dividerSecondary")
      internal static let inputBorder = ColorAsset(name: "Semantic/inputBorder")
      internal static let inputFill = ColorAsset(name: "Semantic/inputFill")
      internal static let primaryBackground = ColorAsset(name: "Semantic/primaryBackground")
      internal static let primaryIcon = ColorAsset(name: "Semantic/primaryIcon")
      internal static let primaryLabel = ColorAsset(name: "Semantic/primaryLabel")
      internal static let secondaryBackground = ColorAsset(name: "Semantic/secondaryBackground")
      internal static let secondaryLabel = ColorAsset(name: "Semantic/secondaryLabel")
      internal static let selectSheetFill = ColorAsset(name: "Semantic/selectSheetFill")
      internal static let tertiaryBackground = ColorAsset(name: "Semantic/tertiaryBackground")
      internal static let tertiaryLabel = ColorAsset(name: "Semantic/tertiaryLabel")
    }
  }
  internal enum Images {
    internal static let alluvionLasOlas = ImageAsset(name: "Alluvion Las Olas")
    internal static let carWash = ImageAsset(name: "Car Wash")
    internal enum Concierge {
      internal static let dimGradient = ImageAsset(name: "Concierge/dim-gradient")
      internal static let editButton = ImageAsset(name: "Concierge/edit-button")
      internal static let icAlacarte = ImageAsset(name: "Concierge/ic-alacarte")
      internal static let icAmenity = ImageAsset(name: "Concierge/ic-amenity")
      internal static let icArrowDown = ImageAsset(name: "Concierge/ic-arrow-down")
      internal static let icAssignmentLg = ImageAsset(name: "Concierge/ic-assignment-lg")
      internal static let icBell = ImageAsset(name: "Concierge/ic-bell")
      internal static let icBuilding = ImageAsset(name: "Concierge/ic-building")
      internal static let icCalendar = ImageAsset(name: "Concierge/ic-calendar")
      internal static let icCartNav = ImageAsset(name: "Concierge/ic-cart-nav")
      internal static let icCart = ImageAsset(name: "Concierge/ic-cart")
      internal static let icChatBubbles = ImageAsset(name: "Concierge/ic-chat-bubbles")
      internal static let icChatNav = ImageAsset(name: "Concierge/ic-chat-nav")
      internal static let icCheck = ImageAsset(name: "Concierge/ic-check")
      internal static let icCircleDown = ImageAsset(name: "Concierge/ic-circle-down")
      internal static let icCircleSelected = ImageAsset(name: "Concierge/ic-circle-selected")
      internal static let icCircleUnselected = ImageAsset(name: "Concierge/ic-circle-unselected")
      internal static let icCircleUp = ImageAsset(name: "Concierge/ic-circle-up")
      internal static let icClipboard = ImageAsset(name: "Concierge/ic-clipboard")
      internal static let icCloseSm = ImageAsset(name: "Concierge/ic-close-sm")
      internal static let icClose = ImageAsset(name: "Concierge/ic-close")
      internal static let icCog = ImageAsset(name: "Concierge/ic-cog")
      internal static let icCompose = ImageAsset(name: "Concierge/ic-compose")
      internal static let icDashboard = ImageAsset(name: "Concierge/ic-dashboard")
      internal static let icDocumentLibrary = ImageAsset(name: "Concierge/ic-document-library")
      internal static let icDocument = ImageAsset(name: "Concierge/ic-document")
      internal static let icDownload = ImageAsset(name: "Concierge/ic-download")
      internal static let icEdit = ImageAsset(name: "Concierge/ic-edit")
      internal static let icFileDownload = ImageAsset(name: "Concierge/ic-file-download")
      internal static let icFilter = ImageAsset(name: "Concierge/ic-filter")
      internal static let icGroupChat = ImageAsset(name: "Concierge/ic-group-chat")
      internal static let icHorizontalDots = ImageAsset(name: "Concierge/ic-horizontal-dots")
      internal static let icInfo = ImageAsset(name: "Concierge/ic-info")
      internal static let icLogout = ImageAsset(name: "Concierge/ic-logout")
      internal static let icMore = ImageAsset(name: "Concierge/ic-more")
      internal static let icNote = ImageAsset(name: "Concierge/ic-note")
      internal static let icNotificationRequest = ImageAsset(name: "Concierge/ic-notification-request")
      internal static let icPackageSm = ImageAsset(name: "Concierge/ic-package-sm")
      internal static let icPackage = ImageAsset(name: "Concierge/ic-package")
      internal static let icPaperPlane = ImageAsset(name: "Concierge/ic-paper-plane")
      internal static let icPayment = ImageAsset(name: "Concierge/ic-payment")
      internal static let icPencilSmall = ImageAsset(name: "Concierge/ic-pencil-small")
      internal static let icPencil = ImageAsset(name: "Concierge/ic-pencil")
      internal static let icPineHome = ImageAsset(name: "Concierge/ic-pineHome")
      internal static let icPlus = ImageAsset(name: "Concierge/ic-plus")
      internal static let icQuestionCircle = ImageAsset(name: "Concierge/ic-question-circle")
      internal static let icReadMore = ImageAsset(name: "Concierge/ic-read-more")
      internal static let icReceipt = ImageAsset(name: "Concierge/ic-receipt")
      internal static let icReservations = ImageAsset(name: "Concierge/ic-reservations")
      internal static let icResidents = ImageAsset(name: "Concierge/ic-residents")
      internal static let icRight = ImageAsset(name: "Concierge/ic-right")
      internal static let icSettings = ImageAsset(name: "Concierge/ic-settings")
      internal static let icShield = ImageAsset(name: "Concierge/ic-shield")
      internal static let icTasks = ImageAsset(name: "Concierge/ic-tasks")
      internal static let icUpload = ImageAsset(name: "Concierge/ic-upload")
      internal static let icUserGroup = ImageAsset(name: "Concierge/ic-user-group")
      internal static let icVideoLibrary = ImageAsset(name: "Concierge/ic-video-library")
      internal static let icVisitors = ImageAsset(name: "Concierge/ic-visitors")
      internal static let icVolumeOff = ImageAsset(name: "Concierge/ic-volume-off")
      internal static let icVolumeUp = ImageAsset(name: "Concierge/ic-volume-up")
      internal static let sendNotification = ImageAsset(name: "Concierge/send-notification")
      internal static let trashButton = ImageAsset(name: "Concierge/trash-button")
    }
    internal static let dogWalking = ImageAsset(name: "Dog Walking")
    internal static let gourmetCart = ImageAsset(name: "Gourmet Cart")
    internal static let alcoholic = ImageAsset(name: "alcoholic")
    internal static let drinks = ImageAsset(name: "drinks")
    internal static let jelly = ImageAsset(name: "jelly")
    internal static let savory = ImageAsset(name: "savory")
    internal static let sweet = ImageAsset(name: "sweet")
    internal static let helpingHand = ImageAsset(name: "Helping Hand")
    internal static let maidService = ImageAsset(name: "Maid Service")
    internal static let pineapelIconOnly = ImageAsset(name: "Pineapel icon only")
    internal static let pineapelLogoFINALWHITERGB1 = ImageAsset(name: "Pineapel-Logo-FINAL-WHITE-RGB-1")
    internal static let pineapelLogoGreen = ImageAsset(name: "Pineapel-Logo-Green")
    internal static let privateChef = ImageAsset(name: "Private Chef")
    internal static let profilePicture = ImageAsset(name: "Profile Picture")
    internal static let reserved = ImageAsset(name: "Reserved")
    internal static let sweetLiving™Green = ImageAsset(name: "SWEET-LIVING™-GREEN")
    internal static let appLogo = ImageAsset(name: "app_logo")
    internal static let appLogoNavbar = ImageAsset(name: "app_logo_navbar")
    internal static let appLogoNoText = ImageAsset(name: "app_logo_no_text")
    internal static let appLogoSmall = ImageAsset(name: "app_logo_small")
    internal static let applePay = ImageAsset(name: "apple-pay")
    internal static let bgSplash = ImageAsset(name: "bg_splash")
    internal static let btnVideoSm = ImageAsset(name: "btn-video-sm")
    internal static let btnVideo = ImageAsset(name: "btn-video")
    internal static let chatIncomingFull1 = ImageAsset(name: "chat-incoming-full-1")
    internal static let chatIncomingFull = ImageAsset(name: "chat-incoming-full")
    internal static let chatIncoming = ImageAsset(name: "chat-incoming")
    internal static let chatOutgoingFull = ImageAsset(name: "chat-outgoing-full")
    internal static let chatOutgoing = ImageAsset(name: "chat-outgoing")
    internal static let dashedLine = ImageAsset(name: "dashed-line")
    internal static let emptyFeedPlaceholder = ImageAsset(name: "empty-feed-placeholder")
    internal static let feedNavbar = ImageAsset(name: "feed_navbar")
    internal static let frame = ImageAsset(name: "frame")
    internal static let headerBackground = ImageAsset(name: "header_background")
    internal static let icAmenityEmptyLarge = ImageAsset(name: "ic-amenity-empty-large")
    internal static let icArrowDown = ImageAsset(name: "ic-arrow-down")
    internal static let icArrowRightBlue = ImageAsset(name: "ic-arrow-right-blue")
    internal static let icBackArrow = ImageAsset(name: "ic-back-arrow")
    internal static let icBell = ImageAsset(name: "ic-bell")
    internal static let icBook = ImageAsset(name: "ic-book")
    internal static let icBoxes = ImageAsset(name: "ic-boxes")
    internal static let icBuilding = ImageAsset(name: "ic-building")
    internal static let icBuildings = ImageAsset(name: "ic-buildings")
    internal static let icButtons = ImageAsset(name: "ic-buttons")
    internal static let icCalendarPlus = ImageAsset(name: "ic-calendar-plus")
    internal static let icCalendarSmall = ImageAsset(name: "ic-calendar-small")
    internal static let icCamera = ImageAsset(name: "ic-camera")
    internal static let icCarSmall = ImageAsset(name: "ic-car-small")
    internal static let icCarWashLg = ImageAsset(name: "ic-car-wash-lg")
    internal static let icCarWash = ImageAsset(name: "ic-car-wash")
    internal static let icCartPlus = ImageAsset(name: "ic-cart-plus")
    internal static let icCartSmall2 = ImageAsset(name: "ic-cart-small-2")
    internal static let icCartSmall = ImageAsset(name: "ic-cart-small")
    internal static let icCart = ImageAsset(name: "ic-cart")
    internal static let icChatLarge = ImageAsset(name: "ic-chat-large")
    internal static let icChat = ImageAsset(name: "ic-chat")
    internal static let icCheckmarkCircle = ImageAsset(name: "ic-checkmark-circle")
    internal static let icCheckmarkNoCircle = ImageAsset(name: "ic-checkmark-no-circle")
    internal static let icCheckmark = ImageAsset(name: "ic-checkmark")
    internal static let icChefSmall = ImageAsset(name: "ic-chef-small")
    internal static let icCleanTools = ImageAsset(name: "ic-clean-tools")
    internal static let icCleaningSmall = ImageAsset(name: "ic-cleaning-small")
    internal static let icCog = ImageAsset(name: "ic-cog")
    internal static let icConversation = ImageAsset(name: "ic-conversation")
    internal static let icDeliver = ImageAsset(name: "ic-deliver")
    internal static let icDeliveryBox = ImageAsset(name: "ic-delivery-box")
    internal static let icDislike = ImageAsset(name: "ic-dislike")
    internal static let icDocument = ImageAsset(name: "ic-document")
    internal static let icDog2 = ImageAsset(name: "ic-dog-2")
    internal static let icDogSmall = ImageAsset(name: "ic-dog-small")
    internal static let icDog = ImageAsset(name: "ic-dog")
    internal static let icDollarCircle = ImageAsset(name: "ic-dollar-circle")
    internal static let icDollarPaper = ImageAsset(name: "ic-dollar-paper")
    internal static let icDotsHorizontal = ImageAsset(name: "ic-dots-horizontal")
    internal static let icEditFile = ImageAsset(name: "ic-edit-file")
    internal static let icEnvelope = ImageAsset(name: "ic-envelope")
    internal static let icFeedback = ImageAsset(name: "ic-feedback")
    internal static let icFlower = ImageAsset(name: "ic-flower")
    internal static let icForwardArrow = ImageAsset(name: "ic-forward-arrow")
    internal static let icGourmetCart = ImageAsset(name: "ic-gourmet-cart")
    internal static let icHamburger2 = ImageAsset(name: "ic-hamburger-2")
    internal static let icHamburger = ImageAsset(name: "ic-hamburger")
    internal static let icHandPointer = ImageAsset(name: "ic-hand-pointer")
    internal static let icHeart = ImageAsset(name: "ic-heart")
    internal static let icHome = ImageAsset(name: "ic-home")
    internal static let icImage = ImageAsset(name: "ic-image")
    internal static let icInfoCircle2 = ImageAsset(name: "ic-info-circle-2")
    internal static let icInfoCircle = ImageAsset(name: "ic-info-circle")
    internal static let icLike = ImageAsset(name: "ic-like")
    internal static let icMinusCircle = ImageAsset(name: "ic-minus-circle")
    internal static let icMore = ImageAsset(name: "ic-more")
    internal static let icNoSound = ImageAsset(name: "ic-no-sound")
    internal static let icPlusCircle = ImageAsset(name: "ic-plus-circle")
    internal static let icPlusFill = ImageAsset(name: "ic-plus-fill")
    internal static let icPlus = ImageAsset(name: "ic-plus")
    internal static let icReadMore = ImageAsset(name: "ic-read-more")
    internal static let icRequestLarge = ImageAsset(name: "ic-request-large")
    internal static let icReservationLarge = ImageAsset(name: "ic-reservation-large")
    internal static let icSearch = ImageAsset(name: "ic-search")
    internal static let icSelected = ImageAsset(name: "ic-selected")
    internal static let icShield = ImageAsset(name: "ic-shield")
    internal static let icSound2 = ImageAsset(name: "ic-sound-2")
    internal static let icSound = ImageAsset(name: "ic-sound")
    internal static let icStarSelected = ImageAsset(name: "ic-star-selected")
    internal static let icStarUnselected = ImageAsset(name: "ic-star-unselected")
    internal static let icTaskList = ImageAsset(name: "ic-task-list")
    internal static let icTrash = ImageAsset(name: "ic-trash")
    internal static let icUnselected = ImageAsset(name: "ic-unselected")
    internal static let icUserLgWhite = ImageAsset(name: "ic-user-lg-white")
    internal static let icUserLg = ImageAsset(name: "ic-user-lg")
    internal static let icUser = ImageAsset(name: "ic-user")
    internal static let icVisitorLarge = ImageAsset(name: "ic-visitor-large")
    internal static let icWrenchSmall = ImageAsset(name: "ic-wrench-small")
    internal static let maintenanceRequest = ImageAsset(name: "maintenance-request")
    internal static let sendButton = ImageAsset(name: "sendButton")
    internal static let welcome = ImageAsset(name: "welcome")
  }
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

// MARK: - Implementation Details

internal final class ColorAsset {
  internal fileprivate(set) var name: String

  #if os(macOS)
  internal typealias Color = NSColor
  #elseif os(iOS) || os(tvOS) || os(watchOS)
  internal typealias Color = UIColor
  #endif

  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, macOS 10.13, *)
  internal private(set) lazy var color: Color = {
    guard let color = Color(asset: self) else {
      fatalError("Unable to load color asset named \(name).")
    }
    return color
  }()

  fileprivate init(name: String) {
    self.name = name
  }
}

internal extension ColorAsset.Color {
  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, macOS 10.13, *)
  convenience init?(asset: ColorAsset) {
    let bundle = BundleToken.bundle
    #if os(iOS) || os(tvOS)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    self.init(named: NSColor.Name(asset.name), bundle: bundle)
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

internal struct ImageAsset {
  internal fileprivate(set) var name: String

  #if os(macOS)
  internal typealias Image = NSImage
  #elseif os(iOS) || os(tvOS) || os(watchOS)
  internal typealias Image = UIImage
  #endif

  internal var image: Image {
    let bundle = BundleToken.bundle
    #if os(iOS) || os(tvOS)
    let image = Image(named: name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    let name = NSImage.Name(self.name)
    let image = (bundle == .main) ? NSImage(named: name) : bundle.image(forResource: name)
    #elseif os(watchOS)
    let image = Image(named: name)
    #endif
    guard let result = image else {
      fatalError("Unable to load image asset named \(name).")
    }
    return result
  }
}

internal extension ImageAsset.Image {
  @available(macOS, deprecated,
    message: "This initializer is unsafe on macOS, please use the ImageAsset.image property")
  convenience init?(asset: ImageAsset) {
    #if os(iOS) || os(tvOS)
    let bundle = BundleToken.bundle
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    self.init(named: NSImage.Name(asset.name))
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
