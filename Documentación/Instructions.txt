ADOBE INDESIGN PRINTING INSTRUCTIONS FOR SERVICE PROVIDER REPORT

PUBLICATION NAME: PINEAPEL - MOBILE APP COMPS - 2021.07.13.indd

PACKAGE DATE: 7/13/21 10:33 PM
Creation Date: 7/13/21
Modification Date: 7/13/21

CONTACT INFORMATION

Company Name: 
Contact: 
Address: 





Phone: 
Fax: 
Email: bill@liquidbold.com

SPECIAL INSTRUCTIONS AND OTHER NOTES






External Plug-ins 0
Non Opaque Objects :On Page1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18

FONTS
8 Fonts Used; 0 Missing, 1 Embedded, 0 Incomplete, 0 Protected

Fonts Packaged
- Name: DaxWide-Light; Type: OpenType Type 1, Status: OK
- Name: Moon-Bold; Type: TrueType, Status: OK
- Name: Moon-Light; Type: TrueType, Status: Embedded
- Name: Moon-Light; Type: TrueType, Status: OK
- Name: SFProDisplay-Bold; Type: OpenType Type 1, Status: OK
- Name: SFProDisplay-Light; Type: OpenType Type 1, Status: OK
- Name: SFProDisplay-Medium; Type: OpenType Type 1, Status: OK
- Name: SFProDisplay-Regular; Type: OpenType Type 1, Status: OK


COLORS AND INKS
4 Process Inks; 2 Spot Inks

- Name and Type: Process Cyan; Angle: 75.000; Lines/Inch: 70.000
- Name and Type: Process Magenta; Angle: 15.000; Lines/Inch: 70.000
- Name and Type: Process Yellow; Angle: 0.000; Lines/Inch: 70.000
- Name and Type: Process Black; Angle: 45.000; Lines/Inch: 70.000
- Name and Type: 51-64-54; Angle: 45.000; Lines/Inch: 70.000
- Name and Type: WHITE; Angle: 45.000; Lines/Inch: 70.000


LINKS AND IMAGES
(Missing & Embedded Links Only)
Links and Images: 141 Links Found; 0 Modified, 0 Missing, 0 Inaccessible
Images: 0 Embedded, 101 use RGB color space


PRINT SETTINGS
PPD: Device Independent, (PostScript® File)
Printing To: Prepress File
Number of Copies: 1
Reader Spreads: No
Even/Odd Pages: Both
Pages: All
Proof: No
Tiling: None
Scale: 100%, 100%
Page Position: Upper Left
Print Layers: Visible & Printable Layers
Printer's Marks: None
Bleed: 0 px, 0 px, 0 px, 0 px
Color: Composite CMYK
Trapping Mode: None
Send Image Data: All
OPI/DCS Image Replacement: No
Page Size: Custom: 1170 px x 2532 px
Paper Dimensions: 0 px x 0 px
Orientation: Portrait
Negative: No
Flip Mode: Off


FILE PACKAGE LIST

1. PINEAPEL - MOBILE APP COMPS - 2021.07.13.indd; type: Adobe InDesign publication; size: 7364K
2. PINEAPEL - MOBILE APP COMPS - 2021.07.13.idml; type: InDesign Markup Document; size: 342K
3. PINEAPEL - MOBILE APP COMPS - 2021.07.13.pdf; type: Adobe Acrobat Document; size: 4495K
4. DaxWide-Light.otf; type: Font file; size: 26K
5. Moon Bold.otf; type: Font file; size: 24K
6. Moon Light.otf; type: Font file; size: 23K
7. SF-Pro-Display-Bold.otf; type: Font file; size: 1341K
8. SF-Pro-Display-Light.otf; type: Font file; size: 1338K
9. SF-Pro-Display-Medium.otf; type: Font file; size: 1356K
10. SF-Pro-Display-Regular.otf; type: Font file; size: 1269K
