// Default Sessions Screen
"sessions-screen.title" = "GATEWAY TO SWEET LIVING";
"sessions-screen.subtitle" = "Always At Your Service";
"sessions-screen.fields.email-address.placeholder" = "Email";
"sessions-screen.fields.email-address.note" = "Enter the email address associated with your lease";
"sessions-screen.fields.password.placeholder" = "Password";
"sessions-screen.fields.verification-code.placeholder" = "Verification Code";
"sessions-screen.fields.password.placeholder" = "Password";
"sessions-screen.fields.password.note" = "For your security a verification code has been sent to your email, then create new password.";

"sessions-screen.buttons.create-account.title" = "Don't have an account? Create one.";
"sessions-screen.buttons.login.title" = "ENTER";
"sessions-screen.buttons.login-with" = "Login with %@";
"sessions-screen.buttons.create-account-submit.title" = "Create Account";
"sessions-screen.buttons.forgot-password.title" = "Forgot Password?";
"sessions-screen.buttons.submit.title" = "Submit";

"sessions-screen.create-password" = "Create Password.";
"sessions-screen.enter-password" = "Enter Password.";

// More Screen
"more-screen.navigation.title" = "More";
"more-screen.navigation.tab-bar-title" = "More";
"more-screen.items.profile" = "Profile";
"more-screen.items.building-info" = "Building Info";
"more-screen.items.things-to-know" = "Things To Know";
"more-screen.items.concierges" = "Concierges";
"more-screen.items.allowed-guest-list" = "Allowed Guests";
"more-screen.items.payment" = "Payment";
"more-screen.items.order-history" = "Order History";
"more-screen.items.feedback" = "Feedback";
"more-screen.items.notifications" = "Notifications";
"more-screen.items.a-la-carte" = "A La Carte";
"more-screen.items.help" = "Help";
"more-screen.items.terms" = "Terms & Conditions";
"more-screen.items.privacy" = "Privacy Policy";
"more-screen.items.amenity-reservations" = "My Reservations";
"more-screen.items.requests" = "Submit Concierge Request";
"more-screen.items.maintenance-request" = "Maintenance Request";
"more-screen.items.logout" = "LOG OUT";
"more-screen.actions.logout.confirm" = "Are you sure you want to log out?";
"more-screen.actions.logout.ok" = "OK";
"more-screen.actions.logout.cancel" = "Cancel";

// Profile Screen
"profile-screen.navigation.title" = "PROFILE";
"profile-screen.fields.first_name.placeholder" = "First Name";
"profile-screen.fields.last_name.placeholder" = "Last Name";
"profile-screen.fields.email.placeholder" = "Email";
"profile-screen.fields.phone.placeholder" = "Phone Number";
"profile-screen.fields.password.placeholder" = "Password";
"profile-screen.buttons.save.title" = "Save";
"profile-screen.messages.profile-changed" = "Your profile was updated successfully";

// Change Email Screen
"change-email-screen.navigation.title" = "Change Email";
"change-email-screen.fields.email.placeholder" = "Email";
"change-email-screen.fields.password.placeholder" = "Password";
"change-email-screen.fields.password.note" = "Your password is required in order to change the email";
"change-email-screen.buttons.save.title" = "Save";
"change-email-screen.messages.email-changed" = "Your email was changed successfully";

// Change Password Screen
"change-password-screen.navigation.title" = "Change Password";
"change-password-screen.fields.password.placeholder" = "Current Password";
"change-password-screen.fields.new-password.placeholder" = "New Password";
"change-password-screen.fields.confirm-password.placeholder" = "Confirm New Password";
"change-password-screen.buttons.save.title" = "Save";
"change-password-screen.messages.password-changed" = "Your password was changed successfully";

// Concierge List Screen
"concierge-list-screen.navigation.title" = "Concierges";

// Building Info Screen
"building-info-screen.navigation.title" = "Building Info";
"building-info-screen.sections.amenity-info.title" = "Amenities";

// Chat Screen
"chat-screen.navigation.title" = "YOUR CONCIERGE:";
"chat-screen.navigation.tab-bar-title" = "CHAT";
"chat-screen.buttons.send.title" = "Send";
"chat-screen.buttons.resend.title" = "Resend";
"chat-screen.inputs.message-input.placeholder" = "Type Something...";

// Things To Know Screen
"things-to-know-screen.navigation.title" = "Things to Know";
"things-to-know-screen.navigation.tab-bar-title" = "Things to Know";

// Allowed Guest List Screen
"allowed-guest-list-screen.navigation.title" = "Allowed Guests";
"allowed-guest-list-screen.buttons.add-guest.title" = "Add Guest";
"allowed-guest-list-screen.buttons.remove-guest.title" = "Remove Guest";
"allowed-guest-list-screen.buttons.edit-guest.title" = "Edit Guest";
"allowed-guest-list-screen.buttons.cancel.title" = "Cancel";
"allowed-guest-list-screen.actions.remove-guest.title" = "Are you sure you want to remove %@?";
"allowed-guest-list-screen.actions.remove-guest.general" = "this resident guest";
"allowed-guest-list-screen.actions.remove-guest.ok" = "OK";
"allowed-guest-list-screen.actions.remove-guest.cancel" = "Cancel";

// Allowed Guest Edit Screen
"allowed-guest-edit-screen.navigation.title" = "Add Guest";
"allowed-guest-edit-screen.fields.frequency.title" = "Frequency";
"allowed-guest-edit-screen.fields.frequency.options.always.title" = "Always";
"allowed-guest-edit-screen.fields.frequency.options.allowed-once.title" = "Allow Once";
"allowed-guest-edit-screen.fields.frequency.options.one-week.title" = "One Week";
"allowed-guest-edit-screen.fields.frequency.options.one-month.title" = "One Month";
"allowed-guest-edit-screen.fields.info.title" = "Info";
"allowed-guest-edit-screen.fields.name.title" = "Full Name";
"allowed-guest-edit-screen.fields.phone.title" = "Phone Number";
"allowed-guest-edit-screen.buttons.save.title" = "Save Guest";
"allowed-guest-edit-screen.guest-updated" = "Allowed guest has been updated";
"allowed-guest-edit-screen.labels.info" = "Information";
"allowed-guest-edit-screen.labels.select-interval" = "Select Interval";
"allowed-guest-edit-screen.errors.frequency-interval-missing" = "You must select a time interval";

// Create Feedback Screen
"create-feedback-screen.navigation.title" = "Feedback";
"create-feedback-screen.fields.feedback-type.title" = "Type of Feedback";
"create-feedback-screen.fields.feedback-type.options.building" = "Building";
"create-feedback-screen.fields.feedback-type.options.concierge" = "Concierge";
"create-feedback-screen.fields.feedback-type.options.other" = "Other";
"create-feedback-screen.fields.rating.title" = "Rating";
"create-feedback-screen.fields.comment.title" = "Leave a Comment";
"create-feedback-screen.fields.comment.placeholder" = "Add Your Comment";
"create-feedback-screen.fields.concierge.title" = "Select Concierge";
"create-feedback-screen.buttons.save.title" = "Submit";
"create-feedback-screen.feedback-sent" = "Feedback has been sent";

// Help Screen
"help-screen.navigation.title" = "HELP";
"help-screen.fields.faq.title" = "FAQ";
"help-screen.fields.faq.placeholder" = "Search...";

// Settings Screen
"settings-screen.navigation.title" = "Notification Settings";
"settings-screen.fields.chat-messages.title" = "Concierge Chat Messages";
"settings-screen.fields.deliveries.title" = "Deliveries";
"settings-screen.fields.fitness.title" = "Fitness";
"settings-screen.fields.information.title" = "Information";
"settings-screen.fields.mail-delivery.title" = "Mail Delivery";
"settings-screen.fields.packages.title" = "Packages";

// Amenity List Screen
"amenity-list-screen.navigation.title" = "Reservation";
"amenity-list-screen.navigation.tab-bar-title" = "RESERVE";


// Amenity Reservation
"amenity-reservation.buttons.check-availability.title" = "Check Availability";
"amenity-reservation.buttons.cancel-reservation.title" = "Cancel Reservation";
"amenity-reservation.buttons.reserve.title" = "Reserve";
"amenity-reservation.buttons.reserve-and-pay.title" = "Reserve & Pay";
"amenity-reservation.fields.reserve-date.placeholder" = "Reservation Date";
"amenity-reservation.fields.reserve-date.datepicker-title" = "Select Date";
"amenity-reservation.fields.comment.placeholder" = "Add Your Comment";
"amenity-reservation.fields.comment.title" = "Add a Comment";
"amenity-reservation.fields.start-time.placeholder" = "Start Time";
"amenity-reservation.fields.end-time.placeholder" = "To";

"amenity-reservation.reservation-summary.date" = "Date";
"amenity-reservation.reservation-summary.time" = "Time";
"amenity-reservation.reservation-summary.duration" = "Duration";
"amenity-reservation.reservation-summary.price" = "Price";
"amenity-reservation.reservation-summary.edit" = "Edit";
"amenity-reservation.labels.available-from" = "Available From:";
"amenity-reservation.labels.not-available" = "Not Available";
"amenity-reservation.labels.thank-you.title" = "Thank you for your reservation";
"amenity-reservation.labels.thank-you.subtitle" = "Someone will be in touch with you soon.";
"amenity-reservation.labels.thank-you.button" = "View Reservation";
"amenity-reservation.labels.cancel-confirmation" = "Are you sure that you want to cancel this reservation?";

// User Notifications Screen
"user-notifications-screen.navigation.tab-bar-title" = "FEED";

// User Notifications
"user-notifications.labels.on-duty" = "On Duty";
"user-notifications.labels.delivery-time" = "Delivery time";
"user-notifications.labels.category" = "Category";
"user-notifications.labels.information" = "Information";
"user-notifications.labels.number" = "Number";
"user-notifications.labels.signed-by" = "Signed By";
"user-notifications.labels.duration" = "Duration";
"user-notifications.labels.instructor" = "Instructor";
"user-notifications.labels.start-date" = "Start Date";
"user-notifications.labels.type" = "Type";
"user-notifications.labels.item-for-delivery" = "Item for Delivery";
"user-notifications.labels.delete-notification-confirm" = "Delete this notification?";
"user-notifications.labels.event-name" = "Event Name";
"user-notifications.labels.event-date" = "Event Date";
"user-notifications.labels.event-host" = "Host";
"user-notifications.labels.event-details" = "Details";

// User Requests Screen
"user-requests-screen.navigation.title" = "REQUESTS";

// User Request Edit Screen
"user-request-edit-screen.navigation.title" = "ADD REQUEST";
"user-request-edit-screen.fields.name.placeholder" = "Request Subject";
"user-request-edit-screen.fields.comment.placeholder" = "Comment";
"user-request-edit-screen.fields.comment.title" = "Add your Comment";
"user-request-edit-screen.buttons.submit.title" = "Submit Concierge Request";
"user-request-edit-screen.labels.request-created" = "Request was created";

// User Requests
"user-request.status.done" = "Completed";
"user-request.status.accepted" = "Accepted";
"user-request.status.pending" = "Submitted";
"user-request.labels.create-request" = "Create a Request";
"user-request.labels.request-success" = "Request sent successfully";

// Payments
"payments-screen.navigation.title" = "PAYMENT";

// Force Update
"force-update.title" = "You are running an old version of\nA Votre Service";
"force-update.subtitle" = "Tap here to update";

// Models - Concierge Specific
"models.concierge.concierge_type.concierge" = "Concierge";
"models.concierge.concierge_type.head_concierge" = "Head Concierge";

// General
"general.hour" = "hour";
"general.hours" = "hours";
"general.minute" = "minute";
"general.minutes" = "minutes";
"general.and" = "and";
"general.delete" = "Delete";
"general.cancel" = "Cancel";
"general.done" = "Done";
"general.yes" = "Yes";
"general.no" = "No";
"general.edit" = "Edit";
"general.close" = "Close";

// Errors
"errors.must-be-present" = "%@ must be present";
"errors.cant-be-blank" = "%@ can't be blank";
"errors.loading-image" = "There was an error while loading this image";
"errors.unknown" = "Unknown error";
"errors.email-invalid" = "Email address is invalid";
"errors.password-invalid" = "Password is invalid";
"errors.password-must-match" = "Password must match confirmation";
"errors.updating-profile" = "There was an error while updating your profile";

// Terms
"terms.by-continuing" = "By continuing, you agree to A Votre Service's ";
"terms.terms" = "Terms of Use";
"terms.confirm-read" = " and confirm that you have read A Votre Service's ";
"terms.privacy-policy" = "Privacy Policy";

// Empty screens
"empty.user-notifications.title" = "we are here to serve you.";
"empty.user-notifications.subtitle" = "If you need assistance\nchat with the concierge.";
"empty.user-notifications.action" = "Chat";
"empty.reservation.title" = "Make a Reservation.";
"empty.reservation.subtitle" = "Ensure what you want is\nready when you want it.";
"empty.reservation.action" = "Reserve";
"empty.chat.title" = "Need Assistance? Let’s Chat.";
"empty.chat.subtitle" = "We are happy to help \nwith your needs.";
"empty.chat.action" = "Chat";
"empty.allowed-guests.title" = "Expecting a Visitor?";
"empty.allowed-guests.subtitle" = "Easily add guests for single\nor multiple visits.";
"empty.allowed-guests.action" = "Add Guest";
"empty.request.title" = "How Can We Help?";
"empty.request.subtitle" = "Submit a request and let us \ntake care of the rest.";
"empty.request.action" = "Request";
"empty.amenity-list.title" = "Amenity Reservations\nare not Currently Available";
"empty.amenity-list.subtitle" = "For more information\nchat with the concierge.";
"empty.amenity-list.action" = "Chat";
"empty.cart.title" = "You cart is empty";
"empty.cart.subtitle" = "Add items from the A La Carte Menu";
"empty.cart.action" = "Close";
"empty.product-list.title" = "Product & Services\nComing Soon";
"empty.product-list.subtitle" = "Speak with the concierge for more info.";
"empty.product-list.action" = "Chat";
"empty.order-list.title" = "No Orders or Reservations Here!";
"empty.order-list.subtitle" = "Amenity Reservations and A La Carte orders will show up here";
"empty.order-list.action" = "Close";
"empty.bell-notifications.title" = "Everything is quiet here.";
"empty.bell-notifications.subtitle" = "If you need assistance\nchat with the concierge.";
"empty.bell-notifications.action" = "Chat";
"empty.lease-office.title" = "Everything is quiet here.";
"empty.lease-office.subtitle" = "If you need assistance\nchat with the concierge.";
"empty.lease-office.action" = "Chat";
"empty.subscriptions.title" = "Everything is quiet here.";
"empty.subscriptions.subtitle" = "If you need assistance\nchat with the concierge.";
"empty.subscriptions.action" = "Chat";
"empty.notes.title" = "Everything is quiet here.";
"empty.notes.subtitle" = "Your Notes will show up here";
"empty.notes.action" = "Create a Note";
"empty.concierge-amenity-reservations.title" = "Everything is quiet here.";
"empty.concierge-amenity-reservations.subtitle" = "Amenity Reservations for today will show up here";

// A La Carte
"a-la-carte.menu.car-wash" = "Car Wash";
"a-la-carte.menu.dog-walking" = "Dog Walking";
"a-la-carte.menu.maid-service" = "Maid Service";
"a-la-carte.menu.personal-trainer" = "Personal Trainer";
"a-la-carte.menu.gourmet-cart" = "Gourmet Cart";
"a-la-carte.menu.helping-hand" = "Helping Hand";
"a-la-carte.navigation.title" = "YOUR CONCIERGE:";
