//
//  Configuration.swift
//  A Votre Service
//
//  Created by Andrei Stoicescu on 02/06/2020.
//  Copyright © 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import Disk
import Semver_swift

struct Configuration: Codable {
    struct AVotreService: Codable {
        struct APIs: Codable {
            struct API: Codable {
                var baseURL: URL
                var clientSecret: String
                var clientId: String
            }

            var main: API
        }

        struct App: Codable {
            enum Environment: String, Codable {
                case development
                case production
            }

            var scheme: String
            var environment: Environment
            var applePayMerchantID: String
        }

        struct Stripe: Codable {
            let publishableKey: String
            let apiVersion: String
        }

        var apis: APIs
        var app:  App
        var stripe: Stripe
    }

    var aVotreService: AVotreService

    // This loads the default configuration file named `configuration.json` found in the bundle
    // Include different versions of the configuration.json in their bundle due to different target memberships of the files.
    static var `default`: Configuration {
        configuration(contentsOfFile: path)
    }

    static var path: String {
        guard let path = Bundle.main.path(forResource: "configuration", ofType: "json") else {
            fatalError("Configuration file not found")
        }

        return path
    }

    static func configuration(contentsOfFile path: String) -> Configuration {
        guard let jsonString = try? String(contentsOfFile: path) else {
            fatalError("Couldn't load configuration file: \(path)")
        }

        return configuration(jsonString: jsonString)
    }

    static func configuration(jsonString: String) -> Configuration {
        guard let data = jsonString.data(using: .utf8) else {
            fatalError("Unknown configuration encoding")
        }

        do {
            return try JSONDecoder().decode(Configuration.self, from: data)
        } catch let error as DecodingError {
            switch error {
            case .dataCorrupted(let context), .keyNotFound(_, let context), .typeMismatch(_, let context), .valueNotFound(_, let context):
                fatalError("Configuration is invalid: \(context.debugDescription)")
            @unknown default:
                fatalError("Configuration is invalid: \(error)")
            }
        } catch let error {
            fatalError("Configuration is invalid: \(error)")
        }
    }
}
