//
// Created by Andrei Stoicescu on 16/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit
import SwiftyUserDefaults

class AVSNavigationController: UINavigationController {
    var dismissNavigation: (() -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        // switch Defaults.userType {
        // case .resident:
        //     self.navigationBar.applyShadow(.resident)
        // case .concierge:
        //     self.navigationBar.applyShadow(.concierge)
        // }

        // navigationBar.applyShadow(.concierge)
    }

    // SwiftUI fix
    // https://stackoverflow.com/questions/62662313/uitabbar-containing-swiftui-view
    override var title: String? {
        get { super.title }
        set { navigationItem.title = newValue }
    }
}