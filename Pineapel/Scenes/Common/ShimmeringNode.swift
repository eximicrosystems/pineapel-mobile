//
// Created by Andrei Stoicescu on 15.02.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import Shimmer.FBShimmeringView

class ShimmeringNode: ASDisplayNode {

    convenience init(didLoadBlock: ((ShimmeringNode) -> Void)? = nil) {
        self.init(viewBlock: {
            FBShimmeringView()
        }) { node in
            didLoadBlock?(node as! ShimmeringNode)
        }
        isOpaque = false
    }

    var contentNode: ASDisplayNode? {
        didSet {
            if contentNode != nil {
                applyContentNode()
            } else {
                contentNode?.removeFromSupernode()
            }
        }
    }

    override var view: FBShimmeringView {
        super.view as! FBShimmeringView
    }

    override func didLoad() {
        super.didLoad()
        applyContentNode()
    }

    fileprivate func applyContentNode() {
        guard let contentNode = contentNode else { return }
        addSubnode(contentNode)
        if isNodeLoaded {
            view.contentView = contentNode.view
        }
    }
}

public extension ASDisplayNode {

    func startShimmeringOnNode(_ node: ASDisplayNode) {
        if (node.shimmeringNode != nil) {
            node.shimmeringNode?.view.isShimmering = true;
        }
    }

    func stopShimmeringOnNode(_ node: ASDisplayNode) {
        if (node.shimmeringNode != nil) {
            node.shimmeringNode?.view.isShimmering = false;
        }
    }

    func addShimmerToNode(_ node: ASDisplayNode) {
        if node.shimmeringNode == nil {
            let frame = node.frame
            node.shimmeringNode = ShimmeringNode(didLoadBlock: { shimmeringNode in
                shimmeringNode.view.shimmeringSpeed = frame.size.width
                shimmeringNode.view.shimmeringPauseDuration = 1
                shimmeringNode.view.shimmeringAnimationOpacity = 0.2
                shimmeringNode.view.shimmeringEndFadeDuration = 0
                shimmeringNode.view.shimmeringBeginFadeDuration = 0
                shimmeringNode.view.isShimmering = true
            })
            node.shimmeringNode?.debugName = "Shimmering Node"
            node.shimmeringNode?.frame = frame
            node.shimmeringNode?.contentNode = node

            DispatchQueue.main.async {
                self.addSubnode(node.shimmeringNode!)
            }
        }
    }

}

private var shimmeringNodeKey: UInt = 1

extension ASDisplayNode {
    var shimmeringNode: ShimmeringNode? {
        get {
            objc_getAssociatedObject(self, &shimmeringNodeKey) as? ShimmeringNode
        }

        set(newValue) {
            objc_setAssociatedObject(self, &shimmeringNodeKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
}
