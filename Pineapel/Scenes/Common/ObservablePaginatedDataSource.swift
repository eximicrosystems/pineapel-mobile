//
// Created by Andrei Stoicescu on 08/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import PromiseKit
import Disk

class ObservablePaginatedDataSource<ListItem: Identifiable>: ObservableObject {

    typealias PaginateClosure = (_ per: Int, _ page: Int) -> Promise<[ListItem]>

    @Published var items: [ListItem]
    @Published var showLoadingIndicator = false
    @Published var hasNoItemsAfterLoading = false
    @Published var lastError: Error? = nil
    var loading = false {
        didSet {
            if loading == false {
                showLoadingIndicator = false
            }
        }
    }
    private (set) var currentPage = 0
    private (set) var hasNextPage = true
    var paginate: PaginateClosure?
    var per = Page.defaultPerPage
    var threshold: Int = 5

    /// Cache the first page of items if possible
    var cacheable = false
    var dynamicCacheable: (() -> Bool)?

    var disableServer = false

    private var isCacheable: Bool {
        dynamicCacheable?() ?? false || cacheable
    }

    var cacheKey: String? = nil

    /// Defaults to true. If false, it will only try to load a single page
    var isPaginated: Bool

    init(paginate: PaginateClosure? = nil, items: [ListItem] = [], isPaginated: Bool = true) {
        self.paginate = paginate
        self.items = items
        self.isPaginated = isPaginated
    }

    func loadMoreContentIfNeeded(currentItem item: ListItem? = nil) {
        guard let item = item else {
            hasNextPage = true
            hasNoItemsAfterLoading = false
            currentPage = 0
            loadMoreContent()
            return
        }

        let thresholdIndex = items.index(items.endIndex, offsetBy: -threshold)
        if items.firstIndex(where: { $0.id == item.id }) == thresholdIndex {
            loadMoreContent()
        }
    }

    func loadMoreContent() {
        guard !loading && hasNextPage else { return }
        guard let paginate = paginate else {
            assertionFailure("trying to load more content without defining a paginate closure")
            return
        }

        loading = true
        showLoadingIndicator = true

        paginate(per, currentPage).done { [weak self] items in
            guard let self = self else { return }
            let replace = self.currentPage == 0
            self.currentPage += 1
            self.hasNextPage = self.isPaginated && items.count == self.per
            Log.this("current page increased \(self.currentPage)")
            Log.this("has next page \(self.hasNextPage)")
            if replace {
                self.items = items
            } else {
                self.items.append(contentsOf: items)
            }
            Log.this("total items count \(self.items.count)")
        }.catch { error in
            self.lastError = error
            Log.thisError(error)
        }.finally {
            self.loading = false
            self.hasNoItemsAfterLoading = self.items.count == 0
        }
    }
}

extension ObservablePaginatedDataSource where ListItem: Identifiable & Codable {

    func loadMoreContentIfNeeded(currentItem item: ListItem? = nil) {
        guard let item = item else {
            hasNextPage = true
            hasNoItemsAfterLoading = false
            currentPage = 0
            loadMoreContent()
            return
        }

        let thresholdIndex = items.index(items.endIndex, offsetBy: -threshold)
        if items.firstIndex(where: { $0.id == item.id }) == thresholdIndex {
            loadMoreContent()
        }
    }

    func loadMoreContent() {
        guard !loading && hasNextPage else { return }
        guard let paginate = paginate else {
            assertionFailure("trying to load more content without defining a paginate closure")
            return
        }

        loading = true

        let replace = currentPage == 0

        // self.items.count will be 0 only the first time when loading the cache.
        // Reloading from scratch subsequently will keep the items before replacing them
        if replace, isCacheable, items.count == 0 {
            if let items = [ListItem].readCached(key: cacheKey), items.count > 0 {
                self.items = items
            }
        }

        if items.count == 0 {
            showLoadingIndicator = true
        }

        if !disableServer {
            paginate(per, currentPage).done { [weak self] items in
                guard let self = self else { return }
                self.currentPage += 1
                self.hasNextPage = self.isPaginated && items.count == self.per
                Log.this("current page increased \(self.currentPage)")
                Log.this("has next page \(self.hasNextPage)")
                if replace {
                    self.items = items
                    if self.isCacheable {
                        CodableCache.set(items, key: self.cacheKey)
                    }
                } else {
                    self.items.append(contentsOf: items)
                }
                Log.this("total items count \(self.items.count)")
            }.catch { error in
                Log.thisError(error)
            }.finally {
                self.loading = false
                self.hasNoItemsAfterLoading = self.items.count == 0
            }
        } else {
            loading = false
            hasNoItemsAfterLoading = items.count == 0
        }
    }
}

extension ObservablePaginatedDataSource {
    @discardableResult
    func removeItem(_ item: ListItem) -> Int? {
        removeItemId(item.id)
    }

    @discardableResult
    func removeItemId(_ id: ListItem.ID) -> Int? {
        guard let index = findIndexOfItemId(id) else { return nil }
        items.remove(at: index)
        return index
    }

    func findIndexOfItemId(_ id: ListItem.ID?) -> Int? {
        items.firstIndex { $0.id == id }
    }

    func findItemById(_ id: ListItem.ID?) -> ListItem? {
        items.first { $0.id == id }
    }

    func insertItem(_ item: ListItem, at index: Int) {
        items.insert(item, at: index)
    }

    func replaceItem(_ item: ListItem) {
        let firstIndex = items.firstIndex { $0.id == item.id }

        if let firstIndex = firstIndex {
            items[firstIndex] = item
        }
    }
}