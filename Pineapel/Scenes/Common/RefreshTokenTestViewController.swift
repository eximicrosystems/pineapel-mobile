//
// Created by Andrei Stoicescu on 13/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import PromiseKit
import SwiftyUserDefaults

class RefreshTokenTestViewController: ASDKViewController<ASDisplayNode> {
    var interceptor = AVSInterceptor()

    override init() {
        let node = ASDisplayNode()
        node.automaticallyManagesSubnodes = true
        let button = ASButtonNode()
        button.setTitle("Reload", with: Asset.Fonts.NormalText.uiFont, with: .black, for: .normal)

        let loginButton = ASButtonNode()
        loginButton.setTitle("Login", with: Asset.Fonts.NormalText.uiFont, with: .black, for: .normal)

        let testButton = ASButtonNode()
        testButton.setTitle("Test Request", with: Asset.Fonts.NormalText.uiFont, with: .black, for: .normal)



        super.init(node: node)
        button.addTarget(self, action: #selector(tap), forControlEvents: .touchUpInside)
        loginButton.addTarget(self, action: #selector(tapLogin), forControlEvents: .touchUpInside)
        testButton.addTarget(self, action: #selector(tapTest), forControlEvents: .touchUpInside)
        node.layoutSpecBlock = { node, range in
            ASStackLayoutSpec(direction: .vertical, spacing: 20, justifyContent: .start, alignItems: .center, children: [button, loginButton, testButton]).centered()
        }
    }

    @objc func tap() {
        self.testInterceptor()
    }

    @objc func tapLogin() {
        self.login()
    }

    @objc func tapTest() {
        self.test()
    }

    func login() {
        AuthService().login(user: "andrei4003@gmail.com", password: "andrei123").cauterize()
    }

    func testInterceptor() {
        // (1...500).forEach { i in
        //     interceptor.refreshToken { result in
        //
        //     }
        // }
    }

    func refresh() -> Promise<Void> {
        let currentRefreshToken = AppState.refreshTokenCredentials?.refreshToken
        return AuthService().refreshToken().get { response in
                    if response.refreshToken != currentRefreshToken {
                        Log.this("Refresh token changed")
                    } else {
                        Log.this("Same refresh token")
                    }
                }
                .then { _ in after(seconds: 1) }
                .then { response in
                    self.refresh()
                }
    }

    func reload() {
        self.refresh().catch { error in
            Log.thisError(error)
        }
    }

    func test() {
        ResidentService().getAmenityList().done { amenities in
            Log.this("done")
        }.catch { error in
            Log.thisError(error)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
