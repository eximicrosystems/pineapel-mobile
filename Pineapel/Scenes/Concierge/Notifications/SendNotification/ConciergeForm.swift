//
// Created by Andrei Stoicescu on 02/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import SwiftUI
import Alamofire
import Photos
import PromiseKit
import Combine

extension View {
    @ViewBuilder
    func `if`<Transform: View>(_ condition: Bool, transform: (Self) -> Transform) -> some View {
        if condition {
            transform(self)
        } else {
            self
        }
    }

    @ViewBuilder
    func ifLet<V, Transform: View>(_ value: V?, transform: (Self, V) -> Transform) -> some View {
        if let value = value {
            transform(self, value)
        } else {
            self
        }
    }
}

protocol AssetMediaUploadBuilder {
    func createUploader() -> ConciergeForm.AssetMediaUploader.MediaUploaderClosure
}

struct ConciergeForm {
    static let textColor = AVSColor.primaryLabel

    struct SheetViewButtonStyle: ButtonStyle {
        var preselected: Bool = false

        func selected(_ configuration: Self.Configuration) -> Bool {
            configuration.isPressed || preselected
        }

        func makeBody(configuration: Self.Configuration) -> some View {
            HStack {
                configuration.label
                        .font(.body)
                        .padding()
                Spacer()
            }.background(selected(configuration) ? Color.avsColor(.secondaryBackground) : .avsColor(.primaryBackground))
             .foregroundColor(selected(configuration) ? Color.avsColor(.primaryLabel) : .avsColor(.secondaryLabel))
        }
    }

    class FilterStore<T>: ObservableObject {
        @Published var filters = [Filter<T>]()
        var filterString: String = ""

        init(filters: [Filter<T>] = []) {
            self.filters = filters
        }

        func clearFilters() {
            for filter in filters {
                filter.option = nil
            }
            self.objectWillChange.send()
        }

        func applyFilters(all: [Option<T>]) -> [Option<T>] {
            all.filter { option in
                for filter in filters where filter.option != nil {
                    if !filter.compare(option.underlyingValue, filter.option) {
                        return false
                    }
                }
                return true
            }
        }

        var selectedOptions: [PickerOption] {
            filters.compactMap { $0.option }
        }
    }

    class Filter<T>: ObservableObject, Identifiable, Equatable {
        static func ==(lhs: Filter, rhs: Filter) -> Bool {
            lhs.id == rhs.id
        }

        var id = UUID()
        var name: String // ex floor
        var options: [PickerOption] // ex 22, 23, 24
        @Published var option: PickerOption?
        var compare: (T, PickerOption?) -> Bool

        init(name: String, options: [PickerOption], compare: @escaping (T, PickerOption?) -> Bool) {
            self.compare = compare
            self.name = name
            self.options = options
        }
    }

    struct SheetView<T>: View {
        @Binding var value: [Option<T>]
        var options: [Option<T>]
        var allowBlank: Bool
        var multipleSelect = false
        @Binding var presented: Bool
        @ObservedObject var filterStore: FilterStore<T>
        var canSelectItem: ((Option<T>) -> Bool)? = nil
        var canDeselectItem: ((Option<T>) -> Bool)? = nil

        var clearDisabled: Bool {
            if !allowBlank {
                return true
            }

            if filterStore.filters.count > 0 {
                return value.count == 0 && filterStore.selectedOptions.count == 0
            }

            return value.count == 0
        }

        var body: some View {
            ScrollView {
                if filterStore.filters.count > 0 {
                    HStack {
                        ForEach(filterStore.filters.indices) { idx in
                            FilterPickerField(label: filterStore.filters[idx].name, filter: self.$filterStore.filters[idx])
                        }
                    }.padding(.top)
                }

                VStack(spacing: 0) {
                    if filterStore.filterString.count > 0 {
                        Text(filterStore.filterString)
                                .foregroundColor(.primaryLabel)
                                .font(.body)
                                .padding()
                    }

                    ForEach(options) { option in
                        Button(action: {
                            let canSelect = canSelectItem?(option) ?? true
                            let canDeselect = canDeselectItem?(option) ?? true

                            if multipleSelect {
                                if let idx = value.firstIndex(of: option) {
                                    if canDeselect {
                                        self.value.remove(at: idx)
                                    }
                                } else {
                                    if canSelect {
                                        self.value.append(option)
                                    }
                                }
                            } else {
                                if canSelect {
                                    self.value.removeAll()
                                    self.value.append(option)
                                    self.presented = false
                                }
                            }
                        }) {
                            Text(option.label)
                        }.buttonStyle(SheetViewButtonStyle(preselected: value.contains(option)))
                    }
                }
            }.navigationBarItems(
                    leading: Button(action: {
                        self.presented = false
                    }) {
                        Image(uiImage: Asset.Images.Concierge.icClose.image.withRenderingMode(.alwaysTemplate))
                    },
                    trailing: Button(action: {
                        self.value.removeAll()
                        filterStore.clearFilters()
                    }) {
                        Text("Clear")
                    }.disabled(clearDisabled)
            )
        }
    }

    struct RightIconModifier: ViewModifier {
        var icon: UIImage?

        func body(content: Content) -> some View {
            content.ifLet(icon) { (c: Content, icon: UIImage) in
                c.overlay(HStack {
                    Spacer()
                    Image(uiImage: icon.withRenderingMode(.alwaysTemplate)).foregroundColor(.primaryIcon).padding()
                })
            }
        }
    }

    struct InputModifier: ViewModifier {
        var label: String
        var dashed = false
        var readonly = false
        var rightIcon: UIImage? = nil
        var note: String? = nil

        func body(content: Content) -> some View {
            VStack(alignment: .leading, spacing: 5) {
                if label.count > 0 {
                    Text(label)
                            .font(.note)
                            .foregroundColor(.secondaryLabel)
                }

                content
                        .padding()
                        .background(
                                RoundedRectangle(cornerRadius: 4)
                                        .strokeBorder(style: dashed ? StrokeStyle(lineWidth: 1, dash: [4, 3]) : StrokeStyle(lineWidth: 1))
                                        .foregroundColor(.inputBorder)
                                        .background(.inputFill)
                                        .clipShape(RoundedRectangle(cornerRadius: 4))
                                        .opacity(readonly ? 0.3 : 1))
                        .modifier(RightIconModifier(icon: rightIcon))

                if let note = note {
                    Text(note)
                            .font(.noteMedium)
                            .foregroundColor(.secondaryLabel)
                }
            }.contentShape(Rectangle())
        }
    }

    struct TextInput: View {
        var label: String
        var placeholder: String? = nil
        @Binding var value: String

        var body: some View {
            TextField(placeholder ?? "", text: $value)
                    .font(.body)
                    .foregroundColor(ConciergeForm.textColor)
                    .disableAutocorrection(true)
                    .modifier(InputModifier(label: label))
        }
    }

    struct NumericInput<Value: Numeric>: View {
        var label: String
        var placeholder: String? = nil
        @Binding var value: Value
        @State private var numericString: String = ""

        var body: some View {
            TextField(placeholder ?? "", value: $value, formatter: NumberFormatter())
                    .keyboardType(.decimalPad)
                    .font(.body)
                    .foregroundColor(ConciergeForm.textColor)
                    .disableAutocorrection(true)
                    .modifier(InputModifier(label: label))
        }
    }

    struct SecureTextInput: View {
        var label: String
        var placeholder: String? = nil
        @Binding var value: String

        var body: some View {
            SecureField(placeholder ?? "", text: $value)
                    .font(.body)
                    .foregroundColor(ConciergeForm.textColor)
                    .disableAutocorrection(true)
                    .modifier(InputModifier(label: label))
        }
    }

    struct ToggleInput: View {
        var label: String
        @Binding var value: Bool

        var body: some View {
            Toggle(isOn: $value) {
                Text(label)
            }.font(.body)
             .foregroundColor(ConciergeForm.textColor)
             .modifier(InputModifier(label: ""))
        }
    }

    struct ReadonlyTextInput: View {
        var label: String
        var value: String

        var body: some View {
            HStack {
                Text(value)
                Spacer()
            }.font(.body)
             .foregroundColor(ConciergeForm.textColor)
             .modifier(InputModifier(label: label))
        }
    }

    struct ReadonlyDynamicTextInput: View {
        var label: String
        @Binding var value: String

        var body: some View {
            HStack {
                Text(value)
                Spacer()
            }.font(.body)
             .foregroundColor(ConciergeForm.textColor)
             .modifier(InputModifier(label: label))
        }
    }

    struct ExternalSelectInput<T>: View {
        var label: String
        var placeholder: String? = nil
        @Binding var value: T
        var stringValueTransformer: (T) -> String
        var rightIcon: UIImage?

        var body: some View {
            HStack {
                Text(stringValueTransformer(value))
                Spacer()
            }.font(.body)
             .foregroundColor(ConciergeForm.textColor)
             .modifier(InputModifier(label: label, rightIcon: rightIcon))
        }
    }

    struct Option<T>: Identifiable, Equatable {
        static func ==(lhs: Option<T>, rhs: Option<T>) -> Bool {
            lhs.id == rhs.id
        }

        var id: String
        var label: String
        var underlyingValue: T
    }

    struct SelectInput<T>: View {

        var label: String
        var placeholder: String? = nil
        var options: [Option<T>]
        @Binding var value: [Option<T>]
        var allowBlank: Bool = true
        var multipleSelect = false
        var readonly = false
        @ObservedObject var filterStore = FilterStore<T>()
        @State private var showSheet = false
        var canSelectItem: ((Option<T>) -> Bool)? = nil
        var canDeselectItem: ((Option<T>) -> Bool)? = nil
        var stringValueTransformer: (([Option<T>]) -> String)? = nil

        private var currentText: String {
            if let valueTransformer = stringValueTransformer {
                return valueTransformer(value)
            }

            if value.count > 0 {
                return value.map(\.label).joined(separator: ", ")
            }

            if let placeholder = placeholder {
                return placeholder
            }

            return ""
        }

        var body: some View {
            HStack {
                Text(currentText)
                        .font(.body)
                        .foregroundColor(ConciergeForm.textColor)
                Spacer()
            }.modifier(InputModifier(label: label, readonly: readonly, rightIcon: Asset.Images.Concierge.icArrowDown.image))
             .onTapGesture {
                 guard !readonly else { return }
                 self.showSheet = true
             }
             .sheet(isPresented: $showSheet) {
                 NavigationView {
                     SheetView(value: self.$value,
                               options: options,
                               allowBlank: allowBlank,
                               multipleSelect: multipleSelect,
                               presented: self.$showSheet,
                               filterStore: filterStore,
                               canSelectItem: canSelectItem,
                               canDeselectItem: canDeselectItem
                     ).fullViewBackgroundColor()
                      .navigationBarTitle(Text(label), displayMode: .inline)
                 }
             }
        }
    }

    struct TextArea: View {
        var label: String
        @Binding var value: String

        var body: some View {
            TextEditView(text: $value)
                    .frame(minHeight: 180, maxHeight: .infinity)
                    .modifier(InputModifier(label: label))
        }
    }

    class AssetMediaUploader: ObservableObject, Identifiable {

        typealias MediaUploaderClosure = (Data, FileType, _ progress: @escaping MediaProgressClosure, _ completion: @escaping MediaCompletionClosure) -> Void
        typealias MediaProgressClosure = (Alamofire.Progress) -> Void
        typealias MediaCompletionClosure = (Swift.Result<String, Error>) -> Void
        typealias ValueChangeClosure = (AssetMediaUploader) -> Void

        enum FileType {
            case video
            case image
        }

        enum MediaUploaderError: Error {
            case noData
            case imageThumbnailError
            case noUploader
            case exportVideo
            case exportVideoSession
            case readVideoFileUrl
        }

        enum MediaAsset {
            case asset(PHAsset)
            case external(FeedNotificationMediaFile)
            case image(UIImage)
        }

        var id = UUID()
        private var asset: MediaAsset
        private var thumbnail: UIImage?

        @Published var isUploading: Bool = false
        @Published var progressFraction: Double = 0
        @Published var error: Error?
        @Published var value: String? {
            didSet {
                self.onValueChange(self)
            }
        }

        private var onValueChange: ValueChangeClosure
        private var uploadClosure: AssetMediaUploader.MediaUploaderClosure

        init(asset: PHAsset, uploadBuilder: AssetMediaUploadBuilder, onValueChange: @escaping ValueChangeClosure) {
            self.asset = .asset(asset)
            self.uploadClosure = uploadBuilder.createUploader()
            self.onValueChange = onValueChange
        }

        init(image: UIImage, uploadBuilder: AssetMediaUploadBuilder, onValueChange: @escaping ValueChangeClosure) {
            self.asset = .image(image)
            self.uploadClosure = uploadBuilder.createUploader()
            self.onValueChange = onValueChange
        }

        init(existingFile: FeedNotificationMediaFile, onValueChange: @escaping ValueChangeClosure) {
            self.asset = .external(existingFile)
            // Upload closure not required on existing files
            self.uploadClosure = { data, type, closure, closure2 in () }
            self.onValueChange = onValueChange
            self.value = existingFile.targetID
        }

        var inUploadingState: Bool {
            isUploading || error != nil
        }

        func getImageData() -> Promise<Data> {
            isUploading = true

            switch asset {
            case .asset(let asset):
                return Promise<Data> { seal in
                    PHImageManager.default().requestImageDataAndOrientation(for: asset, options: nil) { data, s, orientation, dictionary in
                        guard let data = data,
                              let image = UIImage(data: data),
                              let jpegData = image.jpegData(compressionQuality: 0.8) else {
                            seal.reject(MediaUploaderError.noData)
                            return
                        }

                        seal.fulfill(jpegData)
                    }
                }
            case .external:
                // No data is required on external files. they can only be removed
                return .value(Data())
            case .image(let image):
                return Promise<Data> { seal in
                    guard let data = image.jpegData(compressionQuality: 0.8) else {
                        seal.reject(MediaUploaderError.noData)
                        return
                    }

                    seal.fulfill(data)
                }
            }
        }

        func getImage() -> Promise<UIImage> {
            if let image = self.thumbnail {
                return .value(image)
            }

            switch asset {
            case .asset(let asset):
                return Promise<UIImage> { seal in
                    PHImageManager.default().requestImage(for: asset, targetSize: CGSize(width: 500, height: 500), contentMode: .aspectFill, options: nil) { image, dict in
                        if let image = image {
                            self.thumbnail = image
                            seal.fulfill(image)
                        } else {
                            seal.reject(MediaUploaderError.imageThumbnailError)
                        }
                    }
                }
            case .external(let mediaFile):
                return Promise<UIImage> { seal in

                    switch mediaFile.type {
                    case .image:
                        mediaFile.mediaURL.downloadAsImage().done { image in
                            self.thumbnail = image
                            seal.fulfill(image)
                        }.catch { error in
                            seal.reject(error)
                        }
                    case .video:
                        imageFromVideo(url: mediaFile.mediaURL, at: 0).done { image in
                            self.thumbnail = image
                            seal.fulfill(image)
                        }.catch { error in
                            Log.thisError(error)
                            seal.reject(error)
                        }
                    }
                }
            case .image(let image):
                return .value(image)
            }
        }

        private func imageFromVideo(url: URL, at time: TimeInterval) -> Promise<UIImage> {
            Promise<UIImage> { seal in
                DispatchQueue.global(qos: .background).async {
                    let asset = AVURLAsset(url: url)

                    let assetIG = AVAssetImageGenerator(asset: asset)
                    assetIG.appliesPreferredTrackTransform = true
                    assetIG.apertureMode = AVAssetImageGenerator.ApertureMode.encodedPixels

                    let cmTime = CMTime(seconds: time, preferredTimescale: 60)
                    let thumbnailImageRef: CGImage
                    do {
                        thumbnailImageRef = try assetIG.copyCGImage(at: cmTime, actualTime: nil)
                        DispatchQueue.main.async {
                            let image = UIImage(cgImage: thumbnailImageRef)
                            seal.fulfill(image)
                        }
                    } catch let error {
                        seal.reject(error)
                    }
                }
            }
        }

        func getVideoData() -> Promise<Data> {
            isUploading = true

            switch asset {
            case .asset(let asset):
                return Promise<Data> { seal in
                    let filename = UUID().uuidString.appending(".mp4")

                    guard let documentsUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first,
                          let fileUrl = try? documentsUrl.absoluteString.appending(filename).asURL().standardizedFileURL else {
                        seal.reject(MediaUploaderError.exportVideo)
                        return
                    }

                    let options = PHVideoRequestOptions()
                    options.deliveryMode = .mediumQualityFormat
                    options.isNetworkAccessAllowed = true
                    try? FileManager.default.removeItem(at: fileUrl)

                    PHImageManager.default().requestExportSession(forVideo: asset, options: options, exportPreset: AVAssetExportPreset1280x720) {
                        (exportSession: AVAssetExportSession?, _) in

                        guard let exportSession = exportSession else {
                            seal.reject(MediaUploaderError.exportVideoSession)
                            return
                        }

                        exportSession.outputURL = fileUrl
                        exportSession.outputFileType = .mp4
                        exportSession.exportAsynchronously() {
                            guard let data = try? Data(contentsOf: fileUrl) else {
                                seal.reject(MediaUploaderError.readVideoFileUrl)
                                return
                            }

                            seal.fulfill(data)
                        }
                    }
                }
            case .external, .image:
                // Image and External videos don't need to be uploaded
                return .value(Data())
            }

        }

        var fileType: FileType {
            switch asset {
            case .asset(let phAsset):
                switch phAsset.mediaType {
                case .image:
                    return .image
                case .video:
                    return .video
                default:
                    return .image
                }
            case .external(let mediaFile):
                switch mediaFile.type {
                case .image:
                    return .image
                case .video:
                    return .video
                }
            case .image:
                return .image
            }
        }

        func startUploading() {
            guard !isUploading else { return }
            var promise: Promise<Data>? = nil

            switch asset {
            case .asset(let asset):
                switch asset.mediaType {
                case .image:
                    promise = getImageData()
                case .video:
                    promise = getVideoData()
                default:
                    ()
                }
            case .external:
                ()
            case .image:
                promise = getImageData()
            }

            guard let getDataPromise = promise else {
                isUploading = false
                return
            }

            isUploading = true
            error = nil

            getDataPromise.then { data in
                self.uploadData(data, fileType: self.fileType, upload: self.uploadClosure)
            }.done { result in
                self.value = result
            }.catch { error in
                Log.thisError(error)
                self.error = error
            }.finally {
                self.progressFraction = 0
                self.isUploading = false
            }
        }

        func uploadData(_ data: Data, fileType: FileType, upload: @escaping MediaUploaderClosure) -> Promise<String> {
            Promise<String> { seal in
                upload(data, fileType, { progress in
                    Log.this(progress)
                    self.progressFraction = progress.fractionCompleted
                }, { result in
                           switch result {
                           case .success(let mediaID):
                               seal.fulfill(mediaID)
                           case .failure(let error):
                               seal.reject(error)
                           }
                       })
            }
        }

        func removeUpload() {
            progressFraction = 0
            isUploading = false
            error = nil
            value = nil
        }
    }

    struct MediaFile: View {
        enum PickerType {
            case camera
            case library
        }

        enum SheetMenu: Identifiable {
            case imagePicker
            case imageUploader(AssetMediaUploader)

            var id: String {
                switch self {
                case .imagePicker:
                    return "imagePicker"
                case .imageUploader:
                    return "imageUploader"
                }
            }
        }

        struct SingleMediaProgressView: View {
            @ObservedObject var uploader: AssetMediaUploader
            @State private var thumbnail = UIImage()

            var body: some View {
                ZStack {
                    Image(uiImage: thumbnail)
                            .resizable()
                            .aspectRatio(1, contentMode: .fit)
                            .cornerRadius(8)
                            .frame(width: 80, height: 80)
                    VStack {
                        if uploader.isUploading || uploader.progressFraction > 0 {
                            Spacer()
                            ActivityIndicator(style: .medium, color: .white)
                            ProgressBar(value: $uploader.progressFraction,
                                        color: uploader.error == nil ? AVSColor.colorGreen.color : AVSColor.accentBackground.color)
                                    .frame(height: 5)
                                    .padding(10)
                        } else {
                            if uploader.error != nil {
                                Image(systemName: "xmark.octagon.fill")
                                        .font(.largeTitle)
                                        .foregroundColor(.accentBackground)
                            } else if uploader.value != nil {
                                Image(systemName: "checkmark.circle.fill")
                                        .font(.largeTitle)
                                        .foregroundColor(.colorGreen)
                            }
                        }
                    }
                }.padding(.trailing, 5)
                 .onAppear {
                     uploader.getImage().done { image in
                         thumbnail = image
                     }.catch { error in
                         Log.thisError(error)
                     }
                 }
            }
        }

        var label: String
        var placeholder: String? = nil

        @State private var showPicker = false
        @State private var showPickerType: PickerType = .library
        @State private var imageActionSheet = false
        @State private var errorActionSheet = false
        @State private var sheetMenu: SheetMenu? = nil

        @Binding var value: [String]

        var maximumSelectionsAllowed = 5
        var allowedMediaTypes: Set<PHAssetMediaType> = [.image, .video]

        var uploadBuilder: AssetMediaUploadBuilder

        private var maximumRemainingSelections: Int {
            maximumSelectionsAllowed > 0 ? maximumSelectionsAllowed - uploaders.count : 999
        }

        @State private var uploaders = [AssetMediaUploader]()

        var initialMediaFiles = [FeedNotificationMediaFile]()

        func actionSheetItems(uploader: AssetMediaUploader) -> [ActionSheet.Button] {
            var items = [ActionSheet.Button]()
            if !uploader.isUploading {
                items.append(.destructive(Text("Remove File")) {
                    uploader.removeUpload()
                    if let idx = uploaders.firstIndex(where: { $0.id == uploader.id }) {
                        Log.this(idx)
                        self.uploaders.remove(at: idx)
                    }
                })
            }
            if uploader.error != nil {
                items.append(.default(Text("Retry")) {
                    uploader.removeUpload()
                    uploader.startUploading()
                })
            }

            if items.count > 0 {
                items.append(.cancel())
            }

            return items
        }

        var body: some View {
            VStack {
                if uploaders.count > 0 {
                    ScrollView(.horizontal, showsIndicators: false) {
                        HStack {
                            ForEach(uploaders) { uploader in
                                SingleMediaProgressView(uploader: uploader).onTapGesture {
                                    if actionSheetItems(uploader: uploader).count > 0 {
                                        Log.this("tapped \(uploader.id)")
                                        sheetMenu = .imageUploader(uploader)
                                    }
                                }
                            }
                        }.animation(.default)
                    }.frame(height: 80)
                } else {
                    HStack {
                        Spacer()
                        Text(placeholder ?? "")
                                .font(.body)
                                .foregroundColor(ConciergeForm.textColor)
                                .padding()
                        Spacer()
                    }
                }
            }.modifier(InputModifier(label: label, dashed: true))
             .onAppear {
                 if initialMediaFiles.count > 0 {
                     let newUploaders = initialMediaFiles.map {
                         AssetMediaUploader(existingFile: $0) { uploader in
                             value = uploaders.compactMap { $0.value }
                             Log.this("CURRENT FILE ID VALUES CHANGED \(value)")
                         }
                     }

                     uploaders.append(contentsOf: newUploaders)
                     value = uploaders.compactMap { $0.value }
                 }
             }.onTapGesture {
                 if maximumRemainingSelections > 0 {
                     self.sheetMenu = .imagePicker
                 }
             }.sheet(isPresented: $showPicker) {
                 currentPickerView.edgesIgnoringSafeArea(.bottom)
             }.actionSheet(item: $sheetMenu) { (sheetMenu: SheetMenu) in
                 switch sheetMenu {
                 case .imagePicker:
                     var buttons: [ActionSheet.Button] = []

                     if UIImagePickerController.isSourceTypeAvailable(.camera) {
                         buttons.append(.default(Text("Take a Photo")) {
                             showPickerType = .camera
                             showPicker = true
                         })
                     }

                     buttons.append(contentsOf: [
                         .default(Text("Choose from Library")) {
                             showPickerType = .library
                             showPicker = true
                         },
                         .cancel()
                     ])

                     return ActionSheet(title: Text("Choose Media"), buttons: buttons)
                 case .imageUploader(let uploader):
                     return ActionSheet(title: Text("Media File Options"), buttons: actionSheetItems(uploader: uploader))
                 }
             }
        }

        var currentPickerView: AnyView {
            switch showPickerType {
            case .library:
                return MultiImagePickerView(isShown: $showPicker, maximumSelectionsAllowed: maximumRemainingSelections, allowedMediaTypes: allowedMediaTypes) { assets in
                    let newUploaders = assets.map {
                        AssetMediaUploader(asset: $0, uploadBuilder: uploadBuilder) { uploader in
                            value = uploaders.compactMap { $0.value }
                            Log.this("CURRENT FILE ID VALUES CHANGED \(value)")
                        }
                    }

                    uploaders.append(contentsOf: newUploaders)

                    for uploader in newUploaders {
                        uploader.startUploading()
                    }

                }.eraseToAnyView()
            case .camera:
                guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
                    return Text("Camera is not available").eraseToAnyView()
                }

                return ImagePickerView(image: .constant(nil), isShown: $showPicker, sourceType: .camera) { image in
                    guard let image = image else { return }
                    let uploader = AssetMediaUploader(image: image, uploadBuilder: uploadBuilder) { uploader in
                        value = uploaders.compactMap { $0.value }
                        Log.this("CURRENT FILE ID VALUES CHANGED \(value)")
                    }
                    uploaders.append(uploader)
                    uploader.startUploading()
                }.eraseToAnyView()
            }
        }
    }

    struct DateField: View {
        var label: String
        var placeholder: String? = nil
        var formatter: DateFormatter = .avsDateTimeFormatter
        @Binding var value: Date?
        var config: DateTextField.DatePickerConfig = .default

        var body: some View {
            DatePickerField(placeholder ?? "", date: $value, formatter: formatter, config: config)
                    .font(.body)
                    .foregroundColor(ConciergeForm.textColor)
                    .modifier(InputModifier(label: label))
        }
    }

    struct PickerField: View {
        var label: String
        var placeholder: String? = nil
        @Binding var value: PickerOption?
        var options: [PickerOption]

        var body: some View {
            PickerView(placeholder ?? "", option: $value, options: options)
                    .font(.body)
                    .foregroundColor(ConciergeForm.textColor)
                    .modifier(InputModifier(label: label))
        }
    }

    struct FilterPickerField<T>: View {
        var label: String
        var placeholder: String? = nil
        @Binding var filter: Filter<T>

        var body: some View {
            PickerView(placeholder ?? "", option: $filter.option, options: filter.options)
                    .font(.body)
                    .foregroundColor(ConciergeForm.textColor)
                    .modifier(InputModifier(label: label))
        }
    }

}
