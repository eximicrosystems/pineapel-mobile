//
// Created by Andrei Stoicescu on 02/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import SwiftUI
import Combine

extension FeedNotificationItem.NotificationType {
    var notificationFormSelectOption: ConciergeForm.Option<FeedNotificationItem.NotificationType> {
        ConciergeForm.Option(id: self.rawValue, label: self.string, underlyingValue: self)
    }
}

extension SendNotificationsView {
    class NotificationForm: ObservableObject {
        enum Mode {
            case new
            case edit
        }

        static var notificationTypes = FeedNotificationItem.NotificationType.allCases.map(\.notificationFormSelectOption)

        @Published var notificationType: [ConciergeForm.Option<FeedNotificationItem.NotificationType>] = [NotificationForm.notificationTypes[0]] {
            // reset the recipients when notification changes since there can be single/multiple recipients selectable depending on the notification type
            didSet {
                self.recipient.removeAll()
            }
        }
        @Published var subject: String = ""
        @Published var details: String = ""
        @Published var externalLink: String = ""

        @Published var internalLink: [ConciergeForm.Option<Article>] = []
        @Published var internalLinkOptions: [ConciergeForm.Option<Article>] = []

        @Published var file = [String]()

        @Published var recipient: [ConciergeForm.Option<User>] = []
        @Published var recipientOptions: [ConciergeForm.Option<User>] = []

        @Published var signedBy: [ConciergeForm.Option<User>] = []
        @Published var signedByOptions: [ConciergeForm.Option<User>] = []

        @Published var deliveryTime: Date?
        @Published var deliveryItem: String = ""
        @Published var fitnessDuration: String = ""
        @Published var fitnessInstructor: String = ""
        @Published var startDate: Date?
        @Published var fitnessType: String = ""
        @Published var packagesNumber: String = ""

        @Published var eventName: String = ""
        @Published var eventHost: String = ""

        @Published var recipientFilterStore = ConciergeForm.FilterStore<User>()

        private var cancellable: AnyCancellable?

        var buildingID: String?

        var conciergeNotification: ConciergeNotification?

        var editMode: Mode {
            conciergeNotification == nil ? .new : .edit
        }

        convenience init(conciergeNotification: ConciergeNotification) {
            self.init()
            self.conciergeNotification = conciergeNotification
        }

        func setupRecipientFilterStoreListening() {
            cancellable = recipientFilterStore.objectWillChange.sink {
                self.recipientFilterStore.filterString = ""
                guard self.recipientFilterStore.selectedOptions.count > 0 else { return }

                let results = self.recipientFilterStore.applyFilters(all: self.recipientOptions)

                let appliedFilterNames = self.recipientFilterStore.filters.compactMap {
                    if let opt = $0.option?.label {
                        return "\($0.name) - \(opt)"
                    }
                    return nil
                }.joined(separator: ", ")

                if appliedFilterNames.count > 0 {
                    self.recipientFilterStore.filterString = "\(results.count) result\(results.count == 1 ? "" : "s") matching \(appliedFilterNames)"
                }

                self.recipient.removeAll()
                self.recipient.append(contentsOf: results)
            }
        }

        func isFilled(_ value: Any?) -> Bool {
            guard value != nil else { return false }
            // Handle strings separately
            if let stringValue = value as? String {
                return !String.isBlank(stringValue)
            }

            if let arrayValue = value as? Array<Any> {
                return !arrayValue.isEmpty
            }

            return true
        }

        private func mandatoryFields(_ fields: [Any?]) -> Bool {
            for field in fields {
                if !isFilled(field) {
                    return false
                }
            }
            return true
        }

        var isValid: Bool {
            guard let notificationType = self.notificationType.first?.underlyingValue else { return false }
            switch notificationType {
            case .info:
                return mandatoryFields([subject, details, recipient])
            case .packages:
                return mandatoryFields([subject, deliveryTime, packagesNumber, recipient])
            case .mailDelivery:
                return mandatoryFields([subject, deliveryTime, recipient])
            case .fitness:
                return mandatoryFields([subject, details, fitnessDuration, fitnessInstructor, startDate, recipient])
            case .delivery:
                return mandatoryFields([subject, deliveryTime, deliveryItem, recipient])
            case .event:
                return mandatoryFields([subject, details, recipient, startDate])
            }
        }

        func displayField(_ field: ConciergeNotificationRequest.CodingKeys) -> Bool {
            guard let notificationType = self.notificationType.first?.underlyingValue else { return false }
            return ConciergeNotificationRequest.codingKeys(notificationType: notificationType).contains(field)
        }

        var asParams: ConciergeNotificationRequest? {
            guard let notificationType = self.notificationType.first?.underlyingValue else { return nil }
            guard let buildingID = self.buildingID else { return nil }
            guard recipient.count > 0 else { return nil }

            return ConciergeNotificationRequest(id: conciergeNotification?.notification.notificationID,
                                                notificationType: notificationType,
                                                name: subject,
                                                buildingID: buildingID,
                                                recipientIDs: recipient.map { $0.underlyingValue.uid },
                                                mediaFileID: displayField(.mediaFileID) ? file : [],
                                                body: displayField(.body) ? details : nil,
                                                articleID: displayField(.articleID) ? internalLink.first?.underlyingValue.articleNodeID : nil,
                                                externalLink: displayField(.externalLink) ? externalLink : nil,
                                                deliveryTime: displayField(.deliveryTime) ? deliveryTime : nil,
                                                deliveryItem: displayField(.deliveryItem) ? deliveryItem : nil,
                                                startDate: displayField(.startDate) ? startDate : nil,
                                                fitnessType: displayField(.fitnessType) ? fitnessType : nil,
                                                fitnessInstructor: displayField(.fitnessInstructor) ? fitnessInstructor : nil,
                                                fitnessDuration: displayField(.fitnessDuration) ? fitnessDuration : nil,
                                                packagesNumber: displayField(.packagesNumber) ? packagesNumber : nil,
                                                packagesSignedByConciergeID: displayField(.packagesSignedByConciergeID) ? signedBy.first?.underlyingValue.uid : nil,
                                                eventName: displayField(.eventName) ? eventName : nil,
                                                eventHost: displayField(.eventHost) ? eventHost : nil)
        }

        func setupConciergeNotification() {
            guard let conciergeNotification = self.conciergeNotification else { return }
            let notification = conciergeNotification.notification

            notificationType = [notification.notificationType.notificationFormSelectOption]
            subject = notification.name ?? ""
            buildingID = conciergeNotification.buildingID

            // mediaFileID
            details = notification.bodyText ?? ""
            externalLink = notification.externalLink?.url?.absoluteString ?? ""
            deliveryTime = notification.deliveryTime
            deliveryItem = notification.itemForDelivery ?? ""
            startDate = notification.startDate
            fitnessType = notification.type ?? ""
            fitnessInstructor = notification.instructor ?? ""
            fitnessDuration = notification.duration ?? ""
            packagesNumber = notification.number ?? ""
            // signedBy
            eventName = notification.eventName ?? ""
            eventHost = notification.eventHost ?? ""

            recipient = recipientOptions.filter { conciergeNotification.recipientIDs.contains($0.underlyingValue.uid) }
            internalLink = internalLinkOptions.filter { $0.id == notification.internalLink?.articleID }
            signedBy = signedByOptions.filter { $0.id == notification.signedBy?.uid }
            file = notification.mediaFile.map(\.targetID)
        }

    }
}
