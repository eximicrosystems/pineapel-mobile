//
// Created by Andrei Stoicescu on 16/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import Combine

class ResidentGuestDetailsViewController: ASDKViewController<ASDisplayNode>, ASCollectionDataSource, ASCollectionDelegate {

    var collectionNode: ASCollectionNode

    var guest: ResidentGuest

    var didLogGuest: (() -> Void)?

    init(guest: ResidentGuest) {
        self.guest = guest
        collectionNode = ASCollectionNode.avsCollection()
        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()
        navigationItem.title = "Guest Details"
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        1
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let guest = self.guest
        return {
            let cell = ConciergePagerListNodes.SummaryCellNode(guest: guest)
            cell.didTapAction = { [weak self] in
                let guestID = guest.guestID

                SVProgressHUD.show()
                ConciergeService().logResidentGuest(guestID: guestID, guest: guest).done { [weak self] in
                    SVProgressHUD.showInfo(withStatus: "Access Log Updated")
                    SVProgressHUD.dismiss(withDelay: 1)
                    self?.didLogGuest?()
                }.catch { error in
                    Log.thisError(error)
                    SVProgressHUD.showError(withStatus: error.localizedDescription)
                }
            }
            return cell
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        ASSizeRange.fullWidth(collectionNode: collectionNode)
    }
}
