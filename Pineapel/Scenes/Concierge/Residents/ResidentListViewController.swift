//
// Created by Andrei Stoicescu on 11/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import Combine
import SVProgressHUD
import DifferenceKit

class SearchDebouncer {
    typealias Completion = () -> ()

    var searchClosure: ((String?, @escaping Completion) -> ())?
    var currentQuery: String? {
        didSet {
            timer?.invalidate()
            timer = Timer.scheduledTimer(withTimeInterval: 0.3, repeats: false) { [weak self] timer in
                guard let self = self else { return }
                self.timer?.invalidate()
                self.timer = nil
                if (self.inProgressQuery == nil) {
                    self.search()
                }
            }
        }
    }
    var timer: Timer?
    var inProgressQuery: String?

    func search() {
        inProgressQuery = currentQuery
        searchClosure?(currentQuery) { [weak self] in
            guard let self = self else { return }

            if self.inProgressQuery != self.currentQuery {
                self.inProgressQuery = nil
                self.search()
            } else {
                self.inProgressQuery = nil
            }
        }
    }

    deinit {
        timer?.invalidate()
        timer = nil
    }
}

class SearchableDataSource<T> {
    var allItems: [T]
    var filteredItems = [T]()
    var currentItems: [T] {
        searchQuery == nil ? allItems : filteredItems
    }

    var filter: (T, _ query: String) -> Bool

    var searchQuery: String? {
        didSet {
            if let query = searchQuery, query.count > 0 {
                filteredItems = allItems.filter { filter($0, query) }
            } else {
                filteredItems.removeAll()
            }
        }
    }

    init(items: [T], filter: @escaping (T, String) -> Bool) {
        allItems = items
        self.filter = filter
    }
}

class ResidentListViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

    enum Sections: Int, CaseIterable, Differentiable {
        case header
        case items
    }

    lazy var emptyNode = EmptyNode(emptyNodeType: .generic, showAction: false)

    lazy var refreshControl = UIRefreshControl()
    private var collectionNode: ASCollectionNode
    private var dataSource: ObservablePaginatedDataSource<User>
    private var users = [User]()
    private var sections = [ArraySection<Sections, AnyDifferentiable>]()
    private var subscriptions = Set<AnyCancellable>()

    let searchController = UISearchController(searchResultsController: nil)

    private var searchTerm: String? {
        didSet { resetUsers() }
    }

    private func resetUsers() {
        if let query = searchTerm, query.count > 0 {
            users = dataSource.items.filter { user in
                user.fullName.lowercased().contains(query.lowercased()) || (user.apartmentNo?.contains(query) ?? false)
            }
            reloadData()
        } else {
            users = dataSource.items
            reloadData()
        }
    }

    override init() {
        collectionNode = .avsCollection()
        dataSource = .init { per, page in
            ConciergeService().getAllUsers()
        }

        super.init(node: collectionNode)

        dataSource.dynamicCacheable = { [weak self] in
            String.isBlank(self?.searchTerm)
        }

        dataSource.cacheKey = "concierge_residents"

        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func enableRefresh() {
        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)
        collectionNode.view.addSubview(refreshControl)
    }

    func disableRefresh() {
        refreshControl.removeFromSuperview()
    }

    func reloadData() {
        let header = users.count > 0 ? [AnyDifferentiable("header")] : []
        let usersTarget = users.map { AnyDifferentiable($0) }

        let target: [ArraySection<Sections, AnyDifferentiable>] = [
            ArraySection(model: .header, elements: header),
            ArraySection(model: .items, elements: usersTarget)
        ]

        let stagedChangeSet = StagedChangeset(source: sections, target: target)
        for changeSet in stagedChangeSet {
            sections = changeSet.data
            collectionNode.performDiffBatchUpdates(changeSet: changeSet)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupBindings()
        view.backgroundColor = AVSColor.primaryBackground.color

        navigationItem.searchController = searchController
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.searchTextField.searchPublisher().sink { [weak self] term in
            self?.searchTerm = term
        }.store(in: &subscriptions)

        navigationItem.title = "Residents"

        replaceSystemBackButton()
        showEmptyScreen(false)
        collectionNode.view.backgroundView = emptyNode.view

        enableRefresh()

        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(tapSearch))

        refreshItems()
    }

    @objc func tapSearch() {
        navigationItem.searchController?.searchBar.becomeFirstResponder()
    }

    func setupBindings() {
        dataSource.$items
                .receive(on: DispatchQueue.main)
                .sink { [weak self] items in
                    self?.resetUsers()
                }.store(in: &subscriptions)

        dataSource.$showLoadingIndicator
                .receive(on: DispatchQueue.main)
                .sink { [weak self] loading in
                    guard let self = self else { return }
                    if loading {
                        if !self.refreshControl.isRefreshing {
                            SVProgressHUD.show()
                        }
                    } else {
                        SVProgressHUD.dismiss()
                        self.refreshControl.endRefreshing()
                    }
                }.store(in: &subscriptions)
    }

    @objc func refreshItems() {
        dataSource.loadMoreContentIfNeeded()
    }

    func showEmptyScreen(_ show: Bool) {
        emptyNode.isHidden = !show
    }

    func collectionView(_ collectionView: ASCollectionView, willDisplayNodeForItemAt indexPath: IndexPath) {
        if indexPath.section == Sections.items.rawValue,
           let user = sections[indexPath.section].elements[indexPath.item].base as? User {
            dataSource.loadMoreContentIfNeeded(currentItem: user)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        sections[section].elements.count
    }

    func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
        sections.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let section = sections[indexPath.section].model

        switch section {
        case Sections.header:
            return { ConciergePagerListNodes.Header(title: "Tenant Name", subject: "Apt. #") }
        case Sections.items:
            guard let user = sections[indexPath.section].elements[indexPath.item].base as? User else { return { ASCellNode() } }
            return { ConciergePagerListNodes.RequestCell(user: user) }
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        let section = sections[indexPath.section].model

        switch section {
        case .header:
            ()
        case .items:
            guard let user = sections[indexPath.section].elements[indexPath.item].base as? User else { return }
            show(ResidentDetailsViewController(user: user), sender: self)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }
}
