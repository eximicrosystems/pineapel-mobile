//
// Created by Andrei Stoicescu on 11/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import PromiseKit
import SwiftyUserDefaults
import Fakery

class ConciergeBuildingFAQViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource, UINavigationBarDelegate {

    var tabBar: BottonTabBarView!
    
    class HelpViewModel {
        var article: Article
        var open: Bool = false

        // HTML normalized string
        var body: String?

        init(article: Article, body: String?) {
            self.article = article
            self.body = body
        }
    }
    
    class HelpCell: ASCellNode {
        var backgroundNode = ASDisplayNode()
        var titleNode = ASTextNode2()
        var bodyNode = ASTextNode2()
        var toggleButton = ASButtonNode()

        var bottomLineNode = ASDisplayNode()
        var bodyLine = ASDisplayNode()
        var bodyBackground = ASDisplayNode()

        var isFirst: Bool {
            get { indexPath?.item == 0 }
        }

        var open: Bool = false

        init(vm: HelpViewModel) {
            
            
            backgroundNode.backgroundColor = AVSColor.secondaryBackground.color

            bottomLineNode.backgroundColor = AVSColor.divider.color
            bodyNode.attributedText = vm.body?.attributed(.bodyRoman, .primaryLabel)
            titleNode.attributedText = vm.article.title?.attributed(.bodyTitleHeavy, .primaryLabel)
            bodyLine.backgroundColor = AVSColor.accentBackground.color
            bodyBackground.backgroundColor = AVSColor.primaryBackground.color

            open = vm.open

            let image = open ? Asset.Images.Concierge.icCircleUp.image : Asset.Images.Concierge.icCircleDown.image
            toggleButton.setImage(image, for: .normal)

            super.init()

            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            bottomLineNode.style.height = ASDimensionMakeWithPoints(1)
            bodyLine.style.width = ASDimensionMakeWithPoints(4)
            bodyLine.style.alignSelf = .stretch

            let bodyStack = [
                bodyLine,
                bodyNode.margin(20).filling()
            ].hStacked().background(bodyBackground)

            return [
                [
                    [titleNode.filling(), toggleButton].hStacked().spaced(20).margin(20),
                    open ? bodyStack : nil
                ].vStacked(),
                bottomLineNode
            ].vStacked().background(backgroundNode)
        }
    }

    var collectionNode: ASCollectionNode {
        node as! ASCollectionNode
    }

    lazy var refreshControl = UIRefreshControl()
    var screenTitle: String

    var debouncer = SearchDebouncer()

    var dataSource: SearchableDataSource<HelpViewModel>

    init(title: String = "Building FAQ") {
        screenTitle = title
        let collectionNode = ASCollectionNode.avsCollection()

        dataSource = .init(items: []) { article, query in
            (article.body?.lowercased().contains(query.lowercased()) ?? false) ||
            (article.article.title?.contains(query) ?? false)
        }

        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.barTintColor = Asset.Colors.Semantic.colorMilitary.color
        navigationController?.navigationBar.backgroundColor = Asset.Colors.Semantic.colorMilitary.color
                
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search"
        searchController.delegate = self
        searchController.searchBar.backgroundColor = Asset.Colors.Semantic.colorMilitary.color
        navigationItem.searchController = searchController

        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)
        replaceSystemBackButton()
        navigationItem.title = screenTitle
        navigationController?.navigationBar.barTintColor = Asset.Colors.Semantic.colorMilitary.color
        collectionNode.view.backgroundColor = AVSColor.colorMilitaryGreen.color
        collectionNode.view.addSubview(refreshControl)

        debouncer.searchClosure = { [weak self] query, completion in
            if let query = query, query.count > 0 {
                self?.dataSource.searchQuery = query
            } else {
                self?.dataSource.searchQuery = nil
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.collectionNode.reloadSections(.init(integer: 0))
                completion()
            }
        }

        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(tapSearch))
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "chevron.left"), style: .plain, target: self, action: #selector(backView))

        if !loadCached() {
            SVProgressHUD.show()
        }

        refreshItems()
    }

    func loadCached() -> Bool {
        if let buildingId = Defaults.buildingID {
            if let articles = CodableCache.get([Article].self, key: "building_faq_\(buildingId)") {
                dataSource.allItems = articles.map { .init(article: $0, body: $0.body?.html?.string) }
                collectionNode.reloadData()
                return true
            }
        }
        return false
    }

    @objc func tapSearch() {
        navigationItem.searchController?.searchBar.becomeFirstResponder()
    }
    
    @objc func backView(){
        AppState.switchTo(viewController: ResidentTabBarViewController(page: nil))
    }

    @objc func refreshItems() {
//        tabBar.frame.origin.y += 40
//        navigationController?.toolbar.barTintColor = Asset.Colors.Semantic.colorMilitary.color
//        navigationItem.searchController?.searchBar.barTintColor = Asset.Colors.Semantic.colorMilitary.color
        
        let apiService = ResidentService()
        BuildingDetails.getBuildingID().then { buildingID in
            Promise<([Article], String)> { seal in
                apiService.getBuildingFAQ(buildingID: buildingID).done { articles in
                    seal.fulfill((articles, buildingID))
                }.catch { error in
                    seal.reject(error)
                }
            }
        }.done { [weak self] (articles, buildingID) in
            SVProgressHUD.dismiss()
            guard let self = self else { return }
            self.dataSource.allItems = articles.map {
                HelpViewModel(article: $0, body: $0.body?.html?.string)
            }

            CodableCache.set(articles, key: "building_faq_\(buildingID)")

            self.collectionNode.reloadData()
        }.ensure { [weak self] in
            self?.refreshControl.endRefreshing()
        }.catch { error in
            SVProgressHUD.showError(withStatus: error.localizedDescription)
            Log.thisError(error)
        }
    }

    func enableRefresh() {
        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)
        collectionNode.view.addSubview(refreshControl)
    }

    func disableRefresh() {
        refreshControl.removeFromSuperview()
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        dataSource.currentItems.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let article = dataSource.currentItems[indexPath.item]
        return { HelpCell(vm: article) }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        ASSizeRange.fullWidth(collectionNode: collectionNode)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        let articleVM = dataSource.currentItems[indexPath.item]
        articleVM.open = !articleVM.open
        self.collectionNode.reloadItems(at: [indexPath])
    }
}

extension ConciergeBuildingFAQViewController: UISearchControllerDelegate, UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        debouncer.currentQuery = searchController.searchBar.text
    }

    public func willDismissSearchController(_ searchController: UISearchController) {
        debouncer.currentQuery = nil
        enableRefresh()
    }

    public func willPresentSearchController(_ searchController: UISearchController) {
        disableRefresh()
    }
}
