//
// Created by Andrei Stoicescu on 10/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import PromiseKit
import Combine
import DifferenceKit
import SVProgressHUD

class ConciergeBuildingListViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

    lazy var emptyNode = EmptyNode(emptyNodeType: .generic, showAction: false)

    lazy var refreshControl = UIRefreshControl()
    var collectionNode: ASCollectionNode

    var dataSource: ObservablePaginatedDataSource<BuildingDetails>
    var buildings = [BuildingDetails]()
    private var subscriptions = Set<AnyCancellable>()

    override init() {
        collectionNode = .avsCollection()
        dataSource = .init { per, page in
            ConciergeService().getBuildingList(per: per, page: page)
        }
        dataSource.cacheable = true
        dataSource.cacheKey = "concierge_buildings"
        super.init(node: collectionNode)

        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Building Info"
        replaceSystemBackButton()
        showEmptyScreen(false)
        collectionNode.view.backgroundView = emptyNode.view
        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)
        collectionNode.view.addSubview(refreshControl)

        setupBindings()
        refreshItems()
    }

    func showEmptyScreen(_ show: Bool) {
        emptyNode.isHidden = !show
    }

    @objc func refreshItems() {
        dataSource.loadMoreContentIfNeeded()
    }

    func setupBindings() {
        dataSource.$items
                .receive(on: DispatchQueue.main)
                .sink { [weak self] items in
                    self?.reloadData()
                }.store(in: &subscriptions)

        dataSource.$hasNoItemsAfterLoading
                .receive(on: DispatchQueue.main)
                .sink { [weak self] hasNoItemsAfterLoading in
                    self?.showEmptyScreen(hasNoItemsAfterLoading)
                }.store(in: &subscriptions)

        dataSource.$showLoadingIndicator
                .receive(on: DispatchQueue.main)
                .sink { [weak self] loading in
                    guard let self = self else { return }

                    if loading {
                        if !self.refreshControl.isRefreshing {
                            SVProgressHUD.show()
                        }
                    } else {
                        SVProgressHUD.dismiss()
                        self.refreshControl.endRefreshing()
                    }
                }.store(in: &subscriptions)
    }

    func reloadData() {
        let source = buildings
        let target = dataSource.items

        let stagedChangeSet = StagedChangeset(source: source, target: target)
        for changeSet in stagedChangeSet {
            buildings = changeSet.data
            collectionNode.performDiffBatchUpdates(changeSet: changeSet)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        buildings.count
    }

    func collectionView(_ collectionView: ASCollectionView, willDisplayNodeForItemAt indexPath: IndexPath) {
        dataSource.loadMoreContentIfNeeded(currentItem: dataSource.items[indexPath.item])
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let item = dataSource.items[indexPath.item]
        let attr = item.teaser?.html?.string
        return {
            ConciergeThingsToKnowListViewController.HorizontalModeCell(title: item.name, body: attr, url: item.images?.first)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        show(BuildingDetailsViewController(buildingDetails: dataSource.items[indexPath.item]), sender: self)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }
}