//
// Created by Andrei Stoicescu on 09/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import PromiseKit
import Combine

class OrderListPagerDataSource: ObservableObject {
    static var statuses = [OrderData.OrderType.pending, .done]
    typealias DataSource = ObservablePaginatedDataSource<OrderData>

    @Published var sources: [OrderData.OrderType: DataSource] = {
        OrderListPagerDataSource.statuses.reduce(into: [OrderData.OrderType: DataSource]()) { dict, orderType in
            dict[orderType] = .init(paginate: { per, page in
                ConciergeService().getOrderList(per: per, page: page, orderType: orderType)
            })
        }
    }()

    @Published var pendingCount: Int = 0

    var editingOrderPublisher = PassthroughSubject<(OrderData, to: OrderData.OrderType), Never>()

    func moveRequestIfNeeded(_ order: OrderData, to: OrderData.OrderType) {
        guard let fromSource = self.sources[order.orderType],
              order.orderType != to,
              let toSource = self.sources[to] else { return }

        guard let fromIndex = fromSource.items.firstIndex(where: { $0.id == order.id }) else { return }

        var mutatedOrder = order
        mutatedOrder.orderType = to

        fromSource.items.remove(at: fromIndex)
        toSource.items.insert(mutatedOrder, at: 0)

        if order.orderType == .pending {
            reloadPendingBadge()
        }
    }

    func reloadPendingBadge() {
        guard let dataSource = sources[.pending] else { return }
        self.pendingCount = dataSource.items.count
    }
}

class ConciergeOrderListPagerViewContoller: ASDKViewController<ASDisplayNode>, ASPagerDataSource, ASPagerDelegate {
    var pager = ASPagerNode()

    let pageItems = OrderListPagerDataSource.statuses
    var segmentedBar: SegmentedDisplayNode

    var dataSource: OrderListPagerDataSource
    private var subscriptions = Set<AnyCancellable>()

    init(dataSource: OrderListPagerDataSource) {
        self.dataSource = dataSource
        segmentedBar = SegmentedDisplayNode(items: pageItems.map(\.displayString), selection: 0)

        let mainNode = ASDisplayNode()
        mainNode.automaticallyManagesSubnodes = true
        mainNode.automaticallyRelayoutOnSafeAreaChanges = true

        super.init(node: mainNode)

        mainNode.layoutSpecBlock = { node, range in
            [
                self.segmentedBar.margin(top: node.safeAreaInsets.top),
                self.pager.growing()
            ].vStacked()
        }

        segmentedBar.didSelect = { [weak self] index in
            self?.pager.scrollToPage(at: index, animated: true)
        }

        self.pager.setDataSource(self)
        self.pager.setDelegate(self)

        self.setupBindings()
        // Preload the pending datasource in order to have the badge populated
        self.dataSource.sources[.pending]?.loadMoreContentIfNeeded()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func numberOfPages(in pagerNode: ASPagerNode) -> Int {
        pageItems.count
    }

    func setupBindings() {
        dataSource.editingOrderPublisher
                .receive(on: DispatchQueue.main)
                .sink { [weak self] request, toStatus in
                    guard let self = self else { return }
                    self.dataSource.moveRequestIfNeeded(request, to: toStatus)
                    self.navigationController?.popToViewController(self, animated: true)
                }.store(in: &subscriptions)

        // Pending badge count was removed from orders
        // dataSource.$pendingCount
        //         .receive(on: DispatchQueue.main)
        //         .sink { [weak self] val in
        //             self?.updateBadge(val)
        //         }.store(in: &subscriptions)

        dataSource.sources[.pending]?.$items
                .receive(on: DispatchQueue.main)
                .sink { [weak self] items in
                    self?.dataSource.reloadPendingBadge()
                }.store(in: &subscriptions)
    }

    // Pending badge count was removed from orders
    private func updateBadge(_ count: Int) {

        let pendingCountString: String?
        let max = dataSource.sources[.pending]?.per ?? 25
        let finalCount = min(max, count)

        if finalCount == 0 {
            pendingCountString = nil
        } else if finalCount == max {
            pendingCountString = "\(finalCount)+"
        } else {
            pendingCountString = "\(finalCount)"
        }

        self.segmentedBar.pendingBadgeCount = finalCount
        self.navigationController?.tabBarItem.badgeValue = pendingCountString
    }

    func pagerNode(_ pagerNode: ASPagerNode, nodeBlockAt index: Int) -> ASCellNodeBlock {
        let item = pageItems[index]
        let dataSource = self.dataSource.sources[item]!
        return {
            ASCellNode(viewControllerBlock: { [weak self] in
                guard let self = self else { return UIViewController() }
                let vc = ConciergeOrderListViewController(status: item, dataSource: dataSource)
                vc.didSelectRequest = { [weak self] order in
                    guard let self = self else { return }
                    self.navigationController?.pushViewController(ConciergeOrderDetailViewController(order: order, editingPublisher: self.dataSource.editingOrderPublisher), animated: true)
                }
                vc.didReloadDataSource = { [weak self] in
                    guard let self = self else { return }
                    self.reloadPendingBadge()
                }
                return vc
            })
        }
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.segmentedBar.selection = self.pager.currentPageIndex
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Orders"
        self.replaceSystemBackButton()
    }

    func reloadPendingBadge() {
        dataSource.reloadPendingBadge()
    }
}
