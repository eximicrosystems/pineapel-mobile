//
// Created by Andrei Stoicescu on 08/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SwiftyUserDefaults

class SegmentedButton: ASControlNode, Identifiable {
    let id = UUID()
    let title: String
    let titleNode = ASTextNode2()
    let selection = ASDisplayNode()
    var badgeNode = BadgeFactory.accentConciergeBadge(0)
    var badge: Int = 0 {
        didSet {
            badgeNode.badge = badge
            setNeedsLayout()
        }
    }

    struct Config {
        let font: UIFont
        let selectedTextColor: UIColor
        let unselectedTextColor: UIColor
        let selectionBarColor: UIColor
        let buttonVerticalPadding: CGFloat
        var horizontalPadding: CGFloat = 0

        static func config(userType: UserType) -> Config {
            switch userType {

            case .resident:
                return Config(font: AVSFont.body.font,
                              selectedTextColor: AVSColor.accentBackground.color,
                              unselectedTextColor: AVSColor.secondaryLabel.color,
                              selectionBarColor: AVSColor.accentBackground.color,
                              buttonVerticalPadding: 3)
            case .concierge:
                return Config(font: AVSFont.body.font,
                              selectedTextColor: AVSColor.accentBackground.color,
                              unselectedTextColor: AVSColor.secondaryLabel.color,
                              selectionBarColor: AVSColor.accentBackground.color,
                              buttonVerticalPadding: 0)

            }
        }
    }

    var config: Config

    init(title: String, selected: Bool = false) {
        self.title = title
        config = .config(userType: Defaults.userType)
        super.init()
        automaticallyManagesSubnodes = true
        isSelected = selected
    }

    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let items: [ASLayoutElement] = badge > 0 ? [titleNode, badgeNode] : [titleNode]
        return [
            ASDisplayNode.spacer(),
            items.hStacked().justified(.center).spaced(5).margin(horizontal: config.horizontalPadding),
            selection.height(.points(2))
        ].vStacked().justified(.end).spaced(5).margin(top: 5).margin(horizontal: 10)
    }

    private func updateTitle(selected: Bool = false) {
        let color = selected ? config.selectedTextColor : config.unselectedTextColor
        titleNode.attributedText = NSAttributedString.string(text: title, font: config.font, color: color, alignment: .center)
    }

    private func updateSelection(selected: Bool = false) {
        let color = selected ? config.selectionBarColor : .clear
        selection.backgroundColor = color
    }

    override var isSelected: Bool {
        get { super.isSelected }
        set {
            super.isSelected = newValue
            updateTitle(selected: newValue)
            updateSelection(selected: newValue)
        }
    }
}

class SegmentedDisplayNode: ASDisplayNode {
    let items: [String]
    var selection: Int = 0 {
        didSet {
            for (index, node) in nodes.enumerated() {
                if let button = node as? SegmentedButton {
                    button.isSelected = index == selection
                }
            }
        }
    }

    var nodes = [ASLayoutElement]()
    var didSelect: ((Int) -> Void)?
    var line = ASDisplayNode.divider()

    var pendingBadgeCount: Int = 0 {
        didSet {
            guard let node = nodes[safe: pendingBadgeMenuIndex] as? SegmentedButton else { return }
            node.badge = pendingBadgeCount
        }
    }

    var pendingBadgeMenuIndex: Int = 0

    init(items: [String], selection: Int = 0) {
        self.items = items
        self.selection = selection

        super.init()
        backgroundColor = AVSColor.tertiaryBackground.color

        nodes = items.enumerated().map { index, item in
            let button = SegmentedButton(title: item, selected: index == selection)
            button.addTarget(self, action: #selector(tappedButton), forControlEvents: .touchUpInside)
            return button
        }

        automaticallyManagesSubnodes = true
    }

    @objc func tappedButton(_ sender: SegmentedButton) {
        guard let nodes = nodes as? [SegmentedButton] else { return }
        if let index = nodes.firstIndex(where: { $0.id == sender.id }) {
            selection = index
            didSelect?(index)
        }
    }

    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        [nodes.stackEqualSizes().hStacked(), line].vStacked()
    }
}

public protocol SegmentedDisplayNodePresentable {
    var title: String { get }
}

class CollectionSegmentedDisplayNode<T: Identifiable & SegmentedDisplayNodePresentable>: ASDisplayNode, ASCollectionDataSource, ASCollectionDelegate {

    class SegmentedCell: ASCellNode {
        var button: SegmentedButton

        init(title: String) {
            button = SegmentedButton(title: title)
            button.config.horizontalPadding = 10
            super.init()
            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            button.wrapped()
        }

        override var isSelected: Bool {
            get { super.isSelected }
            set {
                super.isSelected = newValue
                button.isSelected = newValue
            }
        }
    }

    var items = [T]() {
        didSet {
            collectionNode.reloadData()
            let item = selectedItem
            selectedItem = item
        }
    }
    var selectedItem: T? {
        didSet {
            if let item = selectedItem,
               let idx = items.firstIndex(where: { $0.id == item.id }) {
                collectionNode.selectItem(at: IndexPath(item: idx, section: 0), animated: true, scrollPosition: .centeredHorizontally)
            }
        }
    }

    func isSelected(_ item: T?) -> Bool {
        guard let item = item else { return false }
        return item.id == selectedItem?.id
    }

    var didSelect: ((T, Int) -> Void)?

    private var collectionNode: ASCollectionNode

    override init() {
        collectionNode = ASCollectionNode(collectionViewLayout: UICollectionViewFlowLayout.horizontal)
        collectionNode.showsHorizontalScrollIndicator = false
        collectionNode.backgroundColor = AVSColor.tertiaryBackground.color

        super.init()

        collectionNode.allowsSelection = true
        collectionNode.allowsMultipleSelection = false
        collectionNode.delegate = self;
        collectionNode.dataSource = self

        automaticallyManagesSubnodes = true
    }

    func selectItem(at index: Int) {
        guard let item = items[safe: index],
              item.id != selectedItem?.id else {
            return
        }

        selectedItem = item
        collectionNode.scrollToItem(at: IndexPath(item: index, section: 0), at: .centeredHorizontally, animated: true)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        items.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        let item = items[indexPath.item]
        selectedItem = item
        didSelect?(item, indexPath.item)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let item = items[indexPath.item]
        return { SegmentedCell(title: item.title) }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        ASSizeRangeMake(CGSize(width: 0, height: 36), CGSize(width: CGFloat.infinity, height: 36))
    }

    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        collectionNode.height(.points(36)).wrapped()
    }
}

