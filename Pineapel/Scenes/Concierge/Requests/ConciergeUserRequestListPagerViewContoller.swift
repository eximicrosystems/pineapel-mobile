//
// Created by Andrei Stoicescu on 08/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import PromiseKit
import Combine

class UserRequestsPagerDataSource: ObservableObject {
    static var statuses = [UserRequest.Status.pending, .accepted, .done]
    typealias DataSource = ObservablePaginatedDataSource<UserRequest>
    typealias Status = UserRequest.Status

    @Published var sources: [Status: DataSource] = {
        UserRequestsPagerDataSource.statuses.reduce(into: [Status: DataSource]()) { dict, status in
            dict[status] = .init(paginate: { per, page in
                ConciergeService().getUserRequests(per: per, page: page, status: status)
            })
        }
    }()

    @Published var pendingCount: Int = 0

    var editingRequestPublisher = PassthroughSubject<(UserRequest, to: UserRequest.Status), Never>()

    func moveRequestIfNeeded(_ request: UserRequest, to: UserRequest.Status) {
        guard let fromStatus = request.status,
              let fromSource = sources[fromStatus],
              request.status != to,
              let toSource = sources[to] else { return }

        guard let fromIndex = fromSource.items.firstIndex(where: { $0.id == request.id }) else { return }

        var request = request
        request.status = to

        fromSource.items.remove(at: fromIndex)
        toSource.items.insert(request, at: 0)

        if fromStatus == .pending {
            pendingCount = max(pendingCount - 1, 0)
        }
    }

    func reloadPendingBadge() {
        ConciergeService().getPendingRequestsCount().done { response in
            self.pendingCount = response.count
        }.catch { error in
            Log.thisError(error)
        }
    }
}

class ConciergeUserRequestListPagerViewContoller: ASDKViewController<ASDisplayNode>, ASPagerDataSource, ASPagerDelegate {
    var pager = ASPagerNode()

    let pageItems = [UserRequest.Status.pending, .accepted, .done]
    var segmentedBar: SegmentedDisplayNode

    var dataSource: UserRequestsPagerDataSource
    private var subscriptions = Set<AnyCancellable>()

    private var didAppearOnce = false

    init(dataSource: UserRequestsPagerDataSource) {
        self.dataSource = dataSource
        segmentedBar = SegmentedDisplayNode(items: pageItems.map(\.displayString), selection: 1)
        segmentedBar.pendingBadgeMenuIndex = 1

        let mainNode = ASDisplayNode()
        mainNode.automaticallyManagesSubnodes = true
        mainNode.automaticallyRelayoutOnSafeAreaChanges = true

        super.init(node: mainNode)

        mainNode.layoutSpecBlock = { node, range in
            [
                self.segmentedBar.margin(top: node.safeAreaInsets.top),
                self.pager.growing()
            ].vStacked()
        }

        segmentedBar.didSelect = { [weak self] index in
            self?.pager.scrollToPage(at: index, animated: true)
        }

        pager.setDataSource(self)
        pager.setDelegate(self)
        setupBindings()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func numberOfPages(in pagerNode: ASPagerNode) -> Int {
        pageItems.count
    }

    func setupBindings() {
        dataSource.editingRequestPublisher
                .receive(on: DispatchQueue.main)
                .sink { [weak self] request, toStatus in
                    guard let self = self else { return }
                    self.dataSource.moveRequestIfNeeded(request, to: toStatus)
                    self.navigationController?.popToViewController(self, animated: true)
                }.store(in: &subscriptions)

        dataSource.$pendingCount
                .receive(on: DispatchQueue.main)
                .sink { [weak self] val in
                    self?.updateBadge(val)
                }
                .store(in: &subscriptions)
    }

    func updateBadge(_ val: Int) {
        segmentedBar.pendingBadgeCount = val
        navigationController?.tabBarItem.badgeValue = val > 0 ? String(val) : nil
    }

    func pagerNode(_ pagerNode: ASPagerNode, nodeBlockAt index: Int) -> ASCellNodeBlock {
        let item = pageItems[index]
        let dataSource = self.dataSource.sources[item]!
        return {
            ASCellNode(viewControllerBlock: { [weak self] in
                guard let self = self else { return UIViewController() }
                let vc = ConciergeUserRequestListViewController(status: item, dataSource: dataSource)
                vc.didSelectRequest = { [weak self] request in
                    guard let self = self else { return }
                    self.navigationController?.pushViewController(ConciergeUserRequestDetailViewController(request: request, editingPublisher: self.dataSource.editingRequestPublisher), animated: true)
                }
                vc.didReloadDataSource = { [weak self] in
                    guard let self = self else { return }
                    self.reloadPendingBadge()
                }
                return vc
            })
        }
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        segmentedBar.selection = pager.currentPageIndex
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Requests"
        replaceSystemBackButton()
    }

//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        if !didAppearOnce {
//            pager.scrollToPage(at: 1, animated: false)
//            didAppearOnce = true
//        }
//    }

    func reloadPendingBadge() {
        dataSource.reloadPendingBadge()
    }

    func openRequestId(_ requestID: String) {

    }
}
