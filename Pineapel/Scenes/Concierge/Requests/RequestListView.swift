//
// Created by Andrei Stoicescu on 07/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//
//
// import SwiftUI
// import Alamofire
// import PromiseKit
// import SVProgressHUD
//
// class ObservablePaginatedDataSource<ListItem: Identifiable>: ObservableObject {
//     typealias PaginateClosure = (_ per: Int, _ page: Int) -> Promise<[ListItem]>
//     @Published var items: [ListItem]
//     @Published var isLoadingPage = false
//     private var currentPage = 0
//     private var hasNextPage = true
//     var paginate: PaginateClosure
//     var per = Page.defaultPerPage
//     var threshold: Int = 5
//
//     init(paginate: @escaping PaginateClosure, items: [ListItem] = []) {
//         self.paginate = paginate
//         self.items = items
//         loadMoreContent()
//     }
//
//     func loadMoreContentIfNeeded(currentItem item: ListItem?) {
//         guard let item = item else {
//             loadMoreContent()
//             return
//         }
//
//         let thresholdIndex = items.index(items.endIndex, offsetBy: -threshold)
//         if items.firstIndex(where: { $0.id == item.id }) == thresholdIndex {
//             loadMoreContent()
//         }
//     }
//
//     func loadMoreContent() {
//         guard !isLoadingPage && hasNextPage else { return }
//         // guard let paginate = paginate else { return }
//
//         isLoadingPage = true
//
//         paginate(per, currentPage).done { [weak self] items in
//             guard let self = self else { return }
//             let replace = self.currentPage == 0
//             self.currentPage += 1
//             self.hasNextPage = items.count == self.per
//             if replace {
//                 self.items = items
//             } else {
//                 self.items.append(contentsOf: items)
//             }
//         }.catch { error in
//             Log.thisError(error)
//         }.finally {
//             self.isLoadingPage = false
//         }
//     }
// }
//
// class UserRequestsDataSource: ObservableObject {
//     static var statuses = [UserRequest.Status.pending, .accepted, .done]
//     typealias DataSource = ObservablePaginatedDataSource<UserRequest>
//     typealias Status = UserRequest.Status
//
//     @Published var sources: [Status: DataSource] = {
//         UserRequestsDataSource.statuses.reduce(into: [Status: DataSource]()) { dict, status in
//             dict[status] = .init(paginate: { per, page in
//                 ConciergeService().getUserRequests(per: per, page: page, status: status)
//             })
//         }
//     }()
// }
//
// struct RequestTabListView: View {
//
//     @ObservedObject var dataSources = UserRequestsDataSource()
//
//     var tabs: [UserRequest.Status] = [.pending, .accepted, .done]
//     @State var currentPage: PageNumber = 0
//
//     var body: some View {
//         VStack(spacing: 0) {
//             SegmentedPicker(items: tabs.map(\.displayString), selection: $currentPage.value)
//             Rectangle()
//                     .fill(Asset.Colors.Concierge.background.sColor)
//                     .frame(height: 5)
//             PageView([
//                          RequestListView(status: .pending, dataSource: dataSources.sources[.pending]!),
//                          RequestListView(status: .accepted, dataSource: dataSources.sources[.accepted]!),
//                          RequestListView(status: .done, dataSource: dataSources.sources[.done]!),
//                      ], currentPage: $currentPage)
//         }
//                 .navigationBarTitle(Text("Requests"))
//     }
// }
//
// struct RequestListView: View {
//     var status: UserRequest.Status
//
//     @ObservedObject var dataSource: ObservablePaginatedDataSource<UserRequest>
//
//     var body: some View {
//         List {
//             ForEach(dataSource.items) { request in
//                 Text(request.name ?? "")
//                         .onAppear {
//                             self.dataSource.loadMoreContentIfNeeded(currentItem: request)
//                         }
//                         .padding()
//             }
//
//             if dataSource.isLoadingPage {
//                 Text("Loading...")
//             }
//         }
//     }
// }
//
// struct RequestTabListView_Previews: PreviewProvider {
//     static var previews: some View {
//         Appearance.Concierge.apply()
//
//         return NavigationView {
//             RequestTabListView()
//                     .navigationBarTitle(Text("Requests"), displayMode: .inline)
//         }
//     }
// }
