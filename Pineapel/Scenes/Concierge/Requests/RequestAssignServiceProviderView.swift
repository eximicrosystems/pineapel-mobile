//
// Created by Andrei Stoicescu on 08.03.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//

import SwiftUI
import Alamofire
import PromiseKit
import SVProgressHUD

struct RequestProviderAssignParams: Encodable {
    let date: Date
    let requestId: String
    let providerId: String

    enum CodingKeys: String, CodingKey {
        case date = "field_fulfilment_time"
        case requestId = "field_request"
        case providerId = "field_service_provider"
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(AVSISO8601DateFormatter().string(from: date), forKey: .date)
        try container.encode(requestId, forKey: .requestId)
        try container.encode(providerId, forKey: .providerId)
    }
}

struct RequestAssignServiceProviderView: View {

    @State private var date: Date?
    var requestId: String
    var providerUserId: String
    var requestTitle: String

    var dismissBlock: () -> Void

    var body: some View {
        ScrollView(showsIndicators: false) {
            VStack(spacing: 20) {
                ConciergeForm.ReadonlyTextInput(label: "Service Provider", value: requestTitle)
                ConciergeForm.DateField(label: "Fulfillment Date", value: $date)
            }.padding(.top, 20)
             .padding(.horizontal, 20)
        }.fullViewBackgroundColor()
         .navigationBarItems(trailing: Button(action: {
             guard let date = date else { return }

             SVProgressHUD.show()

             ConciergeService().assignRequestProvider(params: .init(date: date, requestId: requestId, providerId: providerUserId)).done {
                 SVProgressHUD.dismiss()
                 dismissBlock()
             }.catch { error in
                 Log.thisError(error)
                 SVProgressHUD.showError(withStatus: error.localizedDescription)
             }
         }) {
             Text("Save").navigationSaveButton()
         }.disabled(date == nil)
         ).navigationBarTitle(Text("Assign Service Provider"), displayMode: .inline)
    }
}