//
// Created by Andrei Stoicescu on 08/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import PromiseKit
import Combine

class ConciergeUserRequestListViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

    enum Sections: Int, CaseIterable {
        case header
        case items
    }

    lazy var emptyNode = EmptyNode(emptyNodeType: .generic, showAction: false)

    let status: UserRequest.Status

    lazy var refreshControl = UIRefreshControl()
    var collectionNode: ASCollectionNode

    var dataSource: ObservablePaginatedDataSource<UserRequest>
    private var subscriptions = Set<AnyCancellable>()

    var didSelectRequest: ((UserRequest) -> Void)?
    var didReloadDataSource: (() -> Void)?

    init(status: UserRequest.Status, dataSource: ObservablePaginatedDataSource<UserRequest>) {
        collectionNode = .avsCollection()
        self.dataSource = dataSource
        self.dataSource.cacheable = true
        self.dataSource.cacheKey = "concierge_user_requests_\(status.rawValue)"
        self.status = status
        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupBindings()
        replaceSystemBackButton()
        showEmptyScreen(false)
        collectionNode.view.backgroundView = emptyNode.view
        collectionNode.view.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)
        refreshItems()
    }

    func setupBindings() {
        dataSource.$items
                .receive(on: DispatchQueue.main)
                .sink { [weak self] items in
                    self?.collectionNode.reloadData()
                }.store(in: &subscriptions)

        dataSource.$hasNoItemsAfterLoading
                .receive(on: DispatchQueue.main)
                .sink { [weak self] hasNoItemsAfterLoading in
                    self?.showEmptyScreen(hasNoItemsAfterLoading)
                }.store(in: &subscriptions)

        dataSource.$showLoadingIndicator
                .receive(on: DispatchQueue.main)
                .sink { [weak self] loading in
                    guard let self = self else { return }
                    if loading && self.dataSource.currentPage == 0 {
                        self.didReloadDataSource?()
                    }

                    if loading {
                        if !self.refreshControl.isRefreshing {
                            SVProgressHUD.show()
                        }
                    } else {
                        SVProgressHUD.dismiss()
                        self.refreshControl.endRefreshing()
                    }
                }.store(in: &subscriptions)
    }

    @objc func refreshItems() {
        dataSource.loadMoreContentIfNeeded()
    }

    func showEmptyScreen(_ show: Bool) {
        emptyNode.isHidden = !show
    }

    func collectionView(_ collectionView: ASCollectionView, willDisplayNodeForItemAt indexPath: IndexPath) {
        if indexPath.section == Sections.items.rawValue {
            dataSource.loadMoreContentIfNeeded(currentItem: self.dataSource.items[indexPath.item])
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case Sections.header.rawValue:
            return dataSource.items.count > 0 ? 1 : 0
        case Sections.items.rawValue:
            return dataSource.items.count
        default:
            return 0
        }
    }

    func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
        Sections.allCases.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        switch indexPath.section {
        case Sections.header.rawValue:
            return { ConciergePagerListNodes.Header(title: "Tenant Name", subject: "Subject") }
        case Sections.items.rawValue:
            let item = dataSource.items[indexPath.item]
            return {
                ConciergePagerListNodes.RequestCell(request: item)
            }
        default:
            return { ASCellNode() }
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        let request = dataSource.items[indexPath.item]
        didSelectRequest?(request)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        ASSizeRange.fullWidth(collectionNode: collectionNode)
    }
}

