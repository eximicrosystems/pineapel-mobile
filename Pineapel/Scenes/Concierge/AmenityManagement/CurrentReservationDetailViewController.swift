//
// Created by Andrei Stoicescu on 21/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import Combine

class CurrentReservationDetailViewController: ASDKViewController<ASDisplayNode>, ASCollectionDataSource, ASCollectionDelegate {

    var collectionNode: ASCollectionNode

    var reservation: AmenityReservation

    var didChangeReservation: ((AmenityReservation) -> Void)?

    init(reservation: AmenityReservation) {
        self.reservation = reservation
        collectionNode = .avsCollection()
        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()
        navigationItem.title = "Reservation Details"

        if reservation.status == .pending || reservation.status == .accepted {
            let btn = UIButton(type: .custom)
            btn.setImage(Asset.Images.Concierge.icHorizontalDots.image.withRenderingMode(.alwaysTemplate), for: .normal)
            btn.addTarget(self, action: #selector(tapDots), for: .touchUpInside)
            btn.tintColor = AVSColor.primaryIcon.color
            navigationItem.rightBarButtonItem = UIBarButtonItem(customView: btn)
        }
    }

    func refreshItems() {
        if reservation.status == .rejected {
            navigationItem.rightBarButtonItem = nil
        }
        collectionNode.reloadData()
    }

    @objc func tapDots() {
        guard let alertController = buildReservationActionsAlert(reservation: reservation) else { return }
        alertController.fixIpadIfNeeded(view: view)
        present(alertController, animated: true)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        1
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let reservation = self.reservation
        return {
            ConciergePagerListNodes.SummaryCellNode(reservation: reservation)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }

    func buildReservationActionsAlert(reservation: AmenityReservation) -> UIAlertController? {
        guard let status = reservation.status,
              status != .rejected,
              let reservationID = reservation.reservationID else { return nil }

        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        func addAction(_ title: String, style: UIAlertAction.Style, status: AmenityReservation.Status) {
            alertController.addAction(UIAlertAction(title: title, style: style) { [weak self] _ in
                SVProgressHUD.show()
                ConciergeService().updateAmenityReservationStatus(status: status, reservationID: reservationID).done { [weak self] in
                    SVProgressHUD.dismiss()
                    guard let self = self else { return }
                    self.reservation.status = status
                    self.didChangeReservation?(self.reservation)
                    self.refreshItems()
                }.catch { error in
                    Log.thisError(error)
                    SVProgressHUD.showError(withStatus: error.localizedDescription)
                }
            })
        }

        switch status {
        case .pending:
            addAction("Approve", style: .default, status: .accepted)
            addAction("Decline", style: .destructive, status: .rejected)
        case .accepted:
            addAction("Decline", style: .destructive, status: .rejected)
        case .rejected:
            ()
        }

        alertController.addAction(UIAlertAction(title: L10n.General.cancel, style: .cancel, handler: nil))

        return alertController
    }
}

