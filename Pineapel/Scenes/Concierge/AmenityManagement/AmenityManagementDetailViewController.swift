//
// Created by Andrei Stoicescu on 18/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit

class AmenityManagementDetailViewController: ASDKViewController<ASDisplayNode>, ASCollectionDataSource, ASCollectionDelegate {

    enum Items: Int, CaseIterable {
        case main
        case reservations
    }

    var collectionNode: ASCollectionNode {
        node as! ASCollectionNode
    }

    var amenity: Amenity

    init(amenity: Amenity) {
        self.amenity = amenity
        let collectionNode = ASCollectionNode.avsCollection()
        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
        collectionNode.showsVerticalScrollIndicator = false
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()
        navigationItem.title = "Amenity Details"
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        Items.allCases.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        switch indexPath.item {
        case Items.main.rawValue:
            let amenity = self.amenity
            let articleBody = amenity.articleBody?.html?.string

            return {
                let backgroundNode = ASDisplayNode()
                backgroundNode.backgroundColor = AVSColor.secondaryBackground.color
                let carousel = ConciergeGenericNodes.ImagePager(images: amenity.images, ratio: 3 / 5)

                let summary = ConciergePagerListNodes.SummaryNode(config: .matrix(
                        [
                            [.init(title: "MAXIMUM PAX CAPACITY", subtitle: String(amenity.maxPeople))],
                            [
                                .init(title: "PRICE", subtitle: (amenity.price ?? 0).currencyDollarClean),
                                .init(title: "TAX PERCENT", subtitle: "\(amenity.taxPercent.currencyClean)%")
                            ],
                            [.init(title: "OPEN HOURS", subtitle: amenity.openHoursStrings.joined(separator: "\n"))]
                        ]))


                let titleNode = ASTextNode2(with: amenity.name?.attributed(.bodyTitleHeavy, .primaryLabel))
                let subtitleNode = ASTextNode2(with: articleBody?.attributed(.bodyRoman, .primaryLabel))

                let cell = ASCellNode()
                cell.automaticallyManagesSubnodes = true
                cell.layoutSpecBlock = { node, range in
                    [
                        carousel,
                        [titleNode, subtitleNode].vStacked().spaced(10).margin(15),
                        summary.margin(horizontal: 15)
                    ].vStacked().margin(bottom: 15).background(backgroundNode).margin(bottom: 15)
                }
                return cell
            }
        case Items.reservations.rawValue:
            return { ConciergeGenericNodes.MenuCell(icon: nil, title: "Current Reservations", subtitle: "See current reservations for this amenity", disclosure: true) }
        default:
            return { ASCellNode() }
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.item {
        case Items.reservations.rawValue:
            show(CurrentReservationListViewController(amenityID: amenity.amenityID, date: Date()), sender: self)
        default:
            ()
        }
    }

}
