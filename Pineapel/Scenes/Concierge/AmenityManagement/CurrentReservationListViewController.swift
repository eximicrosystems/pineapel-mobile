//
// Created by Andrei Stoicescu on 21/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import PromiseKit
import Combine

class CurrentReservationListViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

    enum Sections: Int, CaseIterable {
        case header
        case items
    }

    lazy var emptyNode = EmptyNode(emptyNodeType: .generic, showAction: false)

    var date: Date {
        didSet {
            refreshItems()
        }
    }

    lazy var refreshControl = UIRefreshControl()
    var collectionNode: ASCollectionNode
    var amenityID: String

    var dataSource: ObservablePaginatedDataSource<AmenityReservation>
    private var subscriptions = Set<AnyCancellable>()

    init(amenityID: String, date: Date) {
        self.amenityID = amenityID
        self.date = date

        collectionNode = ASCollectionNode.avsCollection()

        dataSource = ObservablePaginatedDataSource<AmenityReservation>()

        super.init(node: collectionNode)

        dataSource.paginate = { [weak self] per, page in
            guard let self = self else { return .value([]) }
            return ConciergeService().getAmenityReservationList(page: page,
                                                                per: per,
                                                                startDate: self.date.start(of: .day),
                                                                endDate: self.date.end(of: .day),
                                                                amenityID: self.amenityID)
        }

        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        let btn = UIButton(type: .custom)
        btn.setImage(Asset.Images.Concierge.icCalendar.image.withRenderingMode(.alwaysTemplate), for: .normal)
        btn.addTarget(self, action: #selector(tapCalendar), for: .touchUpInside)
        btn.tintColor = AVSColor.primaryIcon.color
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: btn)

        setupBindings()

        replaceSystemBackButton()
        showEmptyScreen(false)
        collectionNode.view.backgroundView = emptyNode.view
        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)

        navigationItem.title = "Current Reservations"

        collectionNode.view.addSubview(refreshControl)

        refreshItems()
    }

    @objc func tapCalendar() {
        let dateRangePicker = CalendarDateRangePickerViewController(collectionViewLayout: UICollectionViewFlowLayout())
        dateRangePicker.delegate = self
        dateRangePicker.selectionMode = .single
        dateRangePicker.minimumDate = Date()
        dateRangePicker.selectedStartDate = date
        dateRangePicker.maximumDate = 6.months.later
        dateRangePicker.firstDayOfWeek = .monday
        dateRangePicker.titleText = "Select Date"

        present(AVSNavigationController(rootViewController: dateRangePicker), animated: true, completion: nil)
    }

    func setupBindings() {
        dataSource.$items
                .receive(on: DispatchQueue.main)
                .sink { [weak self] items in
                    self?.collectionNode.reloadData()
                }.store(in: &subscriptions)

        dataSource.$showLoadingIndicator
                .receive(on: DispatchQueue.main)
                .sink { [weak self] loading in
                    guard let self = self else { return }

                    if loading {
                        if !self.refreshControl.isRefreshing {
                            SVProgressHUD.show()
                        }
                    } else {
                        SVProgressHUD.dismiss()
                        self.refreshControl.endRefreshing()
                    }
                }.store(in: &subscriptions)

        dataSource.$hasNoItemsAfterLoading.receive(on: DispatchQueue.main)
                                          .sink { [weak self] noItems in
                                              guard let self = self else { return }
                                              self.showEmptyScreen(noItems)
                                          }.store(in: &subscriptions)
    }

    @objc func refreshItems() {
        dataSource.loadMoreContentIfNeeded()
    }

    func showEmptyScreen(_ show: Bool) {
        emptyNode.isHidden = !show
    }

    func collectionView(_ collectionView: ASCollectionView, willDisplayNodeForItemAt indexPath: IndexPath) {
        if indexPath.section == Sections.items.rawValue {
            dataSource.loadMoreContentIfNeeded(currentItem: self.dataSource.items[indexPath.item])
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case Sections.header.rawValue:
            return dataSource.items.count > 0 ? 1 : 0
        case Sections.items.rawValue:
            return dataSource.items.count
        default:
            return 0
        }
    }

    func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
        Sections.allCases.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        switch indexPath.section {
        case Sections.header.rawValue:
            return { ConciergePagerListNodes.Header(title: "TENANT NAME", subject: "RESERVED DATE") }
        case Sections.items.rawValue:
            let item = dataSource.items[indexPath.item]
            return {
                ConciergePagerListNodes.RequestCell(reservation: item)
            }
        default:
            return { ASCellNode() }
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        let reservation = dataSource.items[indexPath.item]
        show(CurrentReservationDetailViewController(reservation: reservation), sender: self)

    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        ASSizeRange.fullWidth(collectionNode: collectionNode)
    }
}

extension CurrentReservationListViewController: CalendarDateRangePickerViewControllerDelegate {
    func didCancelPickingDateRange() {
        navigationController?.dismiss(animated: true)
    }

    func didPickSingleDate(date: Date) {
    }

    func didPickDateRange(startDate: Date!, endDate: Date!) {
    }

    func didSelectStartDate(startDate: Date!) {
        date = startDate
        navigationController?.dismiss(animated: true)
    }

    func didSelectEndDate(endDate: Date!) {
    }
}
