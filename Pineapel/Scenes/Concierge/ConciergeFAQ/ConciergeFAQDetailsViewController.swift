//
// Created by Andrei Stoicescu on 10.03.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import SwiftUI
import Combine

class ConciergeFAQDetailsViewController: ASDKViewController<ASDisplayNode>, ASCollectionDataSource, ASCollectionDelegate {

    enum Action {
        case update(ConciergeFAQ)
        case delete(_ faqID: String)
    }

    var collectionNode: ASCollectionNode

    var faq: ConciergeFAQ
    var didChangeFAQ: ((Action) -> Void)?

    init(faq: ConciergeFAQ) {
        self.faq = faq
        collectionNode = .avsCollection()
        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()
        navigationItem.title = "FAQ Details"
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: Asset.Images.Concierge.icCompose.image.withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(tapEdit))
    }

    @objc func tapEdit() {
        let alertController = UIAlertController(title: nil, message: "Options", preferredStyle: .actionSheet)
        let deleteAction = UIAlertAction(title: "Delete", style: .destructive) { [weak self] _ in
            guard let faqID = self?.faq.id else { return }
            self?.confirmDelete(faqID)
        }

        let editAction = UIAlertAction(title: "Edit", style: .default) { [weak self] _ in
            guard let self = self else { return }

            let view = ConciergeFAQFormView(faq: self.faq, dismissBlock: { [weak self] dismiss in
                guard let self = self else { return }
                self.navigationController?.dismiss(animated: true)

                switch dismiss {
                case .updated(let log):
                    self.faq = log
                    self.didChangeFAQ?(.update(log))
                    self.collectionNode.reloadData()
                default:
                    ()
                }
            })

            let nav = AVSNavigationController(rootViewController: UIHostingController(rootView: view))
            nav.isModalInPresentation = true
            self.navigationController?.present(nav, animated: true)
        }

        alertController.addAction(editAction)
        alertController.addAction(deleteAction)
        alertController.addAction(UIAlertAction(title: L10n.General.close, style: .cancel, handler: nil))
        alertController.fixIpadIfNeeded(view: view)
        present(alertController, animated: true)
    }

    func confirmDelete(_ faqID: String) {
        let deleteAlert = UIAlertController(title: nil, message: "Delete this FAQ?", preferredStyle: .alert)

        deleteAlert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { action in
            SVProgressHUD.show()
            ConciergeService().deleteConciergeFAQ(id: faqID).done { [weak self] in
                SVProgressHUD.dismiss()
                self?.didChangeFAQ?(.delete(faqID))
            }.catch { error in
                SVProgressHUD.showError(withStatus: error.localizedDescription)
            }
        }))

        deleteAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        present(deleteAlert, animated: true, completion: nil)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        1
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let faq = self.faq
        let description = faq.body?.html
        return {
            ConciergePagerListNodes.SummaryCellNode(faq: faq, faqDescription: description)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if traitCollection.userInterfaceStyle != previousTraitCollection?.userInterfaceStyle {
            collectionNode.reloadData()
        }
    }
}