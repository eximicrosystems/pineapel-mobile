//
// Created by Andrei Stoicescu on 10.03.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//


import SwiftUI
import Alamofire
import PromiseKit
import SVProgressHUD

class ConciergeFAQForm: ObservableObject {
    @Published var subject: String = ""
    @Published var body: String = ""
    var buildingID: String = ""
    var id: String?

    var asParams: ConciergeFAQRequest {
        ConciergeFAQRequest(id: id, body: body, name: subject, buildingID: buildingID)
    }

    func updatedFAQ(_ faq: ConciergeFAQ) -> ConciergeFAQ {
        ConciergeFAQ(id: faq.id,
                     buildingId: faq.buildingId,
                     body: body,
                     name: subject,
                     createdAt: faq.createdAt)
    }

    var isValid: Bool {
        !String.isBlank(subject) &&
        !String.isBlank(body) &&
        !String.isBlank(buildingID)
    }
}

struct ConciergeFAQFormView: View {
    enum DismissMode {
        case canceled
        case created
        case updated(ConciergeFAQ)
    }

    @ObservedObject var form = ConciergeFAQForm()
    var faq: ConciergeFAQ?
    var dismissBlock: (DismissMode) -> Void

    var body: some View {
        ScrollView(showsIndicators: false) {
            VStack(spacing: 20) {
                ConciergeForm.TextInput(label: "SUBJECT", value: $form.subject)
                ConciergeForm.TextArea(label: "BODY", value: $form.body)
            }.padding(.top, 20)
             .padding(.horizontal, 20)
        }.onAppear {

             if let faq = faq {
                 form.body = faq.body?.html?.string ?? ""
                 form.subject = faq.name ?? ""
                 form.buildingID = faq.buildingId
             } else {
                 BuildingDetails.getBuildingID().done { buildingID in
                     form.buildingID = buildingID
                 }.catch { error in
                     Log.thisError(error)
                     SVProgressHUD.showError(withStatus: error.localizedDescription)
                 }
             }
         }.fullViewBackgroundColor()
         .navigationBarItems(
                 leading: Button(action: {
                     dismissBlock(.canceled)
                 }) {
                     Image(uiImage: Asset.Images.Concierge.icClose.image.withRenderingMode(.alwaysTemplate))
                 },
                 trailing: Button(action: {

                     SVProgressHUD.show()
                     let promise: Promise<Void>

                     if let faqID = faq?.id {
                         promise = ConciergeService().updateConciergeFAQ(params: form.asParams, faqID: faqID)
                     } else {
                         promise = ConciergeService().createConciergeFAQ(params: form.asParams)
                     }

                     promise.done {
                         SVProgressHUD.dismiss()

                         if let faq = faq {
                             let updatedFAQ = form.updatedFAQ(faq)
                             dismissBlock(.updated(updatedFAQ))
                         } else {
                             dismissBlock(.created)
                         }

                     }.catch { error in
                         Log.thisError(error)
                         SVProgressHUD.showError(withStatus: error.localizedDescription)
                     }
                 }) {
                     Text("Save").navigationSaveButton()
                 }.disabled(!form.isValid)
         ).navigationBarTitle(Text("\(faq == nil ? "Create" : "Update") Concierge FAQ"), displayMode: .inline)

    }
}