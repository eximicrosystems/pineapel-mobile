//
// Created by Andrei Stoicescu on 16/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import Combine

class VisitorDetailsViewController: ASDKViewController<ASDisplayNode>, ASCollectionDataSource, ASCollectionDelegate {

    var collectionNode: ASCollectionNode

    var visitor: Visitor

    init(visitor: Visitor) {
        self.visitor = visitor
        self.collectionNode = ASCollectionNode.avsCollection()
        super.init(node: collectionNode)
        self.collectionNode.delegate = self
        self.collectionNode.dataSource = self
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.replaceSystemBackButton()
        self.navigationItem.title = "Visitor Details"
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        1
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let visitor = self.visitor
        return {
            ConciergePagerListNodes.SummaryCellNode(visitor: visitor)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        ASSizeRange.fullWidth(collectionNode: collectionNode)
    }
}
