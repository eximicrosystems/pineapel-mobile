//
// Created by Andrei Stoicescu on 16/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import Combine
import SVProgressHUD
import SwiftUI

class VisitorListViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

    enum Sections: Int, CaseIterable {
        case header
        case items
    }

    lazy var emptyNode = EmptyNode(emptyNodeType: .generic, showAction: false)

    lazy var refreshControl = UIRefreshControl()
    var collectionNode: ASCollectionNode

    var dataSource: ObservablePaginatedDataSource<Visitor>
    private var subscriptions = Set<AnyCancellable>()

    override init() {
        collectionNode = ASCollectionNode.avsCollection()
        dataSource = .init { per, page in
            ConciergeService().getVisitors(per: per, page: page)
        }
        dataSource.cacheable = true
        dataSource.cacheKey = "concierge_visitors"

        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func enableRefresh() {
        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)
        collectionNode.view.addSubview(refreshControl)
    }

    func disableRefresh() {
        refreshControl.removeFromSuperview()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupBindings()
        navigationItem.title = "Visitors"
        replaceSystemBackButton()
        showEmptyScreen(false)
        collectionNode.view.backgroundView = emptyNode.view
        enableRefresh()
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(tapAdd))
        refreshItems()
    }

    @objc func tapAdd() {
        let view = LogVisitFormView(form: .init(), dismissBlock: { [weak self] requiresRefresh in
            self?.navigationController?.dismiss(animated: true)
            if requiresRefresh {
                self?.refreshItems()
            }
        })

        let nav = AVSNavigationController(rootViewController: UIHostingController(rootView: view))
        nav.isModalInPresentation = true
        navigationController?.present(nav, animated: true)
    }

    func setupBindings() {
        dataSource.$items
                .receive(on: DispatchQueue.main)
                .sink { [weak self] items in
                    self?.collectionNode.reloadData()
                }.store(in: &subscriptions)

        dataSource.$showLoadingIndicator
                .receive(on: DispatchQueue.main)
                .sink { [weak self] loading in
                    guard let self = self else { return }
                    if loading {
                        if !self.refreshControl.isRefreshing {
                            SVProgressHUD.show()
                        }
                    } else {
                        SVProgressHUD.dismiss()
                        self.refreshControl.endRefreshing()
                    }
                }.store(in: &subscriptions)
    }

    @objc func refreshItems() {
        dataSource.loadMoreContentIfNeeded()
    }

    func showEmptyScreen(_ show: Bool) {
        emptyNode.isHidden = !show
    }

    func collectionView(_ collectionView: ASCollectionView, willDisplayNodeForItemAt indexPath: IndexPath) {
        if indexPath.section == Sections.items.rawValue {
            dataSource.loadMoreContentIfNeeded(currentItem: dataSource.items[indexPath.item])
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        guard let section = Sections(rawValue: section) else { return 0 }
        switch section {
        case Sections.header:
            return dataSource.items.count > 0 ? 1 : 0
        case Sections.items:
            return dataSource.items.count
        }
    }

    func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
        Sections.allCases.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        guard let section = Sections(rawValue: indexPath.section) else { return { ASCellNode() } }

        switch section {
        case Sections.header:
            return { ConciergePagerListNodes.Header(title: "Tenant Name", subject: "Apt. #") }
        case Sections.items:
            let item = dataSource.items[indexPath.item]
            return { ConciergePagerListNodes.RequestCell(visitor: item) }
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        guard let section = Sections(rawValue: indexPath.section) else { return }

        switch section {
        case .header:
            ()
        case .items:
            let item = dataSource.items[indexPath.item]
            show(VisitorDetailsViewController(visitor: item), sender: self)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        ASSizeRange.fullWidth(collectionNode: collectionNode)
    }
}
