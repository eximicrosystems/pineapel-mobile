//
// Created by Andrei Stoicescu on 17/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import SwiftUI
import Alamofire
import PromiseKit
import SVProgressHUD

class LogVisitForm: ObservableObject {
    @Published var name: String = ""
    @Published var apartment: String = ""
    @Published var reason: String = ""

    var asParams: VisitorRequest {
        VisitorRequest(name: name, apartment: apartment, reason: reason)
    }

    var isValid: Bool {
        !String.isBlank(name)
    }
}

struct LogVisitFormView: View {
    @ObservedObject var form: LogVisitForm
    var dismissBlock: ((_ requiresRefresh: Bool) -> Void)?

    var body: some View {
        ScrollView(showsIndicators: false) {
            VStack(spacing: 20) {
                ConciergeForm.TextInput(label: "VISITOR NAME", value: $form.name)
                ConciergeForm.TextInput(label: "APARTMENT NUMBER", value: $form.apartment)
                ConciergeForm.TextArea(label: "REASONS", value: $form.reason)
            }.padding(.top, 20)
             .padding(.horizontal, 20)
        }.fullViewBackgroundColor()
         .navigationBarItems(
                 leading: Button(action: {
                     dismissBlock?(false)
                 }) {
                     Image(uiImage: Asset.Images.Concierge.icClose.image.withRenderingMode(.alwaysTemplate))
                 },
                 trailing: Button(action: {
                     SVProgressHUD.show()
                     ConciergeService().logVisitorRequest(form.asParams).done {
                         SVProgressHUD.dismiss()
                         dismissBlock?(true)
                     }.catch { error in
                         Log.thisError(error)
                         SVProgressHUD.showError(withStatus: error.localizedDescription)
                     }
                 }) {
                     Text("Save").navigationSaveButton()
                 }.disabled(!form.isValid))
         .navigationBarTitle(Text("Log New Visit"), displayMode: .inline)
    }
}