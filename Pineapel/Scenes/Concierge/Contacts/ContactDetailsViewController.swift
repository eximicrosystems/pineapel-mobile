//
// Created by Andrei Stoicescu on 05.03.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import Combine
import PromiseKit
import SwiftyUserDefaults
import SwiftUI

class ContactDetailsViewController: ASDKViewController<ASDisplayNode>, ASCollectionDataSource, ASCollectionDelegate {

    enum Sections: Int, CaseIterable {
        case main
        case menu
    }

    enum Menu: Int, CaseIterable {
        case services
        case notes
    }

    var collectionNode: ASCollectionNode

    var user: User

    init(user: User) {
        self.user = user
        collectionNode = .avsCollection()
        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()
        navigationItem.title = user.firstName
    }

    func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
        Sections.allCases.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        guard let section = Sections(rawValue: section) else { return 0 }
        switch section {
        case .main:
            return 1
        case .menu:
            return Menu.allCases.count
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        guard let section = Sections(rawValue: indexPath.section) else { return { ASCellNode() } }

        switch section {
        case .main:
            let user = self.user
            let about = user.about?.html?.string

            return {
                let summary = ConciergePagerListNodes.SummaryNode(config: .matrix(
                        [
                            [.init(title: "About", subtitle: about)],
                            [
                                .init(title: "Phone Number", subtitle: user.phoneNumber),
                                .init(title: "Email", subtitle: user.email)
                            ],
                            [
                                .init(title: "Gender", subtitle: user.gender?.rawValue),
                                .init(title: "Job", subtitle: user.job)
                            ]
                        ]))

                let contentNode = ConciergeGenericNodes.BioProfileContent(title: user.fullName, subtitle: nil, bodyNode: summary)

                let cell = ConciergeGenericNodes.BioProfileCell(url: user.userPictureURL, imageSize: 150, contentNode: contentNode)

                return cell
            }
        case .menu:
            guard let menuItem = Menu(rawValue: indexPath.item) else { return { ASCellNode() } }
            let user = self.user
            let userName = user.firstName ?? user.fullName

            switch menuItem {
            case .notes:
                return { ConciergeGenericNodes.MenuCell(icon: nil, title: "Notes", subtitle: "Notes about \(userName)", disclosure: true) }
            case .services:
                return { ConciergeGenericNodes.MenuCell(icon: nil, title: "Services", subtitle: "Services provided by \(userName)", disclosure: true) }
            }
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        ASSizeRange.fullWidth(collectionNode: collectionNode)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        guard let section = Sections(rawValue: indexPath.section) else { return }
        switch section {
        case .menu:
            guard let item = Menu(rawValue: indexPath.item) else { return }
            switch item {
            case .services:
                navigationController?.pushViewController(ConciergeServiceListViewController(userId: user.uid), animated: true)
            case .notes:
                navigationController?.pushViewController(ConciergeUserNoteListViewController(userId: user.uid), animated: true)
            }

        case .main:
            ()
        }
    }
}
