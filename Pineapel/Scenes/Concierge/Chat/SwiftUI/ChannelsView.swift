// //
// // Created by Andrei Stoicescu on 25/09/2020.
// // Copyright (c) 2020 A Votre Service LLC. All rights reserved.
// //
//
// import Foundation
// import SwiftUI
// import TwilioChatClient
// import SVProgressHUD
//
// struct ConciergeChat {}
//
// extension ConciergeChat {
//
//     struct ChannelsView: View {
//
//         @ObservedObject var channelList = MessagingManager.shared.channelListManager
//         @State var presentRecipientView: Bool = false
//         @State var presentChatView: Bool = false
//         @State var activeChannel: TwilioChannel? = nil
//
//         var body: some View {
//             VStack {
//                 ScrollView {
//                     Color.clear
//
//                     CompatibleLazyVStack(spacing: 0) {
//                         ForEach(channelList.items) { channel in
//                             VStack(spacing: 0) {
//                                 HStack(spacing: 10) {
//                                     AvatarNameView(url: channel.resident.user.userPictureURL,
//                                                    initials: channel.resident.user.initials)
//
//                                     VStack(alignment: .leading) {
//                                         Text(channel.resident.user.fullName)
//                                                 .foregroundColor(Asset.Colors.Concierge.primary)
//                                                 .conciergeFont(.Body)
//
//                                         if let lastMessageString = channel.lastMessageString {
//                                             Text(lastMessageString)
//                                                     .foregroundColor(Asset.Colors.Concierge.primary)
//                                                     .conciergeFont(channel.unreadCount > 0 ? .NoteHead : .Note)
//
//                                         }
//                                     }
//
//                                     Spacer()
//
//                                     VStack(alignment: .trailing, spacing: 3) {
//                                         if let timestamp = channel.lastMessageDate?.timeAgoSinceNow {
//                                             Text(timestamp)
//                                                     .foregroundColor(Asset.Colors.Concierge.darkGray)
//                                                     .conciergeFont(.Note)
//                                         }
//
//                                         if channel.unreadCount > 0 {
//                                             Text(String(channel.unreadCount))
//                                                     .conciergeFont(.Note)
//                                                     .foregroundColor(.white)
//                                                     .padding(.horizontal, 8)
//                                                     .background(Asset.Colors.Concierge.red.sColor)
//                                                     .clipShape(Capsule())
//                                         }
//                                     }
//                                 }.lineLimit(1)
//                                  .padding(.horizontal, 20)
//                                  .padding(.vertical, 10)
//
//                                 Divider()
//                             }.contentShape(Rectangle())
//                              .onTapGesture {
//                                  activeChannel = channel
//                                  presentChatView = true
//                              }
//                         }.background(Color.white)
//                     }
//                 }
//
//                 NavigationLink(
//                         destination: ValidMessageListView(channel: activeChannel, user: MessagingManager.shared.user),
//                         isActive: $presentChatView
//                 ) { EmptyView() }
//
//             }.background(Asset.Colors.Concierge.background.sColor)
//              .navigationViewStyle(StackNavigationViewStyle())
//              .navigationBarTitle("Chat", displayMode: .inline)
//              .navigationBarItems(trailing: Button {
//                  presentRecipientView = true
//              } label: {
//                  Image(uiImage: Asset.Images.Concierge.icPaperPlane.image)
//              })
//              .sheet(isPresented: $presentRecipientView) {
//                  NavigationView {
//                      NewRecipientView(isPresented: $presentRecipientView) { user in
//                          presentRecipientView = false
//                          SVProgressHUD.show()
//                          channelList.channelForResident(user).done { model in
//                              SVProgressHUD.dismiss()
//                              activeChannel = model
//                              presentChatView = true
//                          }.catch { error in
//                              Log.thisError(error)
//                              SVProgressHUD.showError(withStatus: error.localizedDescription)
//                          }
//                      }
//                  }
//              }
//              .onReceive(MessagingManager.shared.$isLoading) { loading in
//                  if loading {
//                      SVProgressHUD.show()
//                  } else {
//                      SVProgressHUD.dismiss()
//                  }
//              }
//         }
//     }
//
//     struct ValidMessageListView: View {
//         var channel: TwilioChannel?
//         var user: User?
//
//         var body: some View {
//             if let channel = channel, let user = user {
//                 ConciergeMessageListView(resident: channel.resident.user,
//                                          dataSource: MessagingManager.shared.messageDataSources.findOrCreateSource(channel: channel,
//                                                                                                                    currentUser: user))
//             } else {
//                 Text("")
//             }
//         }
//     }
// }
//
// class ConciergeChannelsView_Previews: PreviewProvider {
//     static func makeChannel(_ unread: Int = 3) -> TwilioChannel {
//         .default(unread: unread, lastMessageString: "Some message goes hereSome message goes hereSome message goes hereSome message goes hereSome message goes here")
//     }
//
//     static var previews: some View {
//         NavigationView {
//             ConciergeChat.ChannelsView(channelList: ConciergeChat.ChannelListManager(items: [
//                 makeChannel(),
//                 makeChannel(0),
//                 makeChannel(0)
//             ]))
//         }
//
//     }
// }
