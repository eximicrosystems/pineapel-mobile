// //
// // Created by Andrei Stoicescu on 28/09/2020.
// // Copyright (c) 2020 A Votre Service LLC. All rights reserved.
// //
//
// import Foundation
// import SwiftUI
// import SVProgressHUD
//
// extension ConciergeChat {
//     struct NewRecipientView: View {
//
//         @State var users = [User]()
//         @Binding var isPresented: Bool
//         var didSelectUser: (User) -> Void
//
//         var body: some View {
//             ScrollView {
//                 Color.clear
//
//                 CompatibleLazyVStack(spacing: 0) {
//                     ForEach(users) { user in
//                         VStack(spacing: 0) {
//                             HStack(spacing: 10) {
//                                 AvatarNameView(url: user.userPictureURL,
//                                                initials: user.initials)
//
//                                 Text(user.fullName)
//                                         .foregroundColor(Asset.Colors.Concierge.primary)
//                                         .conciergeFont(.Body)
//
//                                 Spacer()
//
//                                 Image(uiImage: Asset.Images.Concierge.icPlus.image)
//                             }.lineLimit(1)
//                              .padding(.horizontal, 20)
//                              .padding(.vertical, 10)
//
//                             Divider()
//                         }.contentShape(Rectangle())
//                          .onTapGesture {
//                              didSelectUser(user)
//                          }
//                     }.background(Color.white)
//                 }
//
//             }.background(Asset.Colors.Concierge.background.sColor)
//              .navigationBarItems(leading: Button(action: {
//                  isPresented = false
//              }, label: {
//                  Image(uiImage: Asset.Images.Concierge.icClose.image)
//              }))
//              .navigationBarTitle("Select Recipient", displayMode: .inline)
//              .onAppear {
//                  SVProgressHUD.show()
//                  ConciergeService().getAllUsers().done { users in
//                      SVProgressHUD.dismiss()
//                      self.users = users
//                  }.catch { error in
//                      SVProgressHUD.showError(withStatus: error.localizedDescription)
//                  }
//              }
//         }
//     }
// }
//
// class ConciergeChatNewRecipientView_Previews: PreviewProvider {
//     static var previews: some View {
//         NavigationView {
//             ConciergeChat.NewRecipientView(users: [User.default, User.default, User.default], isPresented: .constant(true)) { user in  }
//         }
//     }
// }
