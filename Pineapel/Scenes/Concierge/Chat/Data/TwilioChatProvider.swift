//
// Created by Andrei Stoicescu on 25/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import TwilioChatClient
import PromiseKit
import Combine
import DifferenceKit

struct TwilioChannel: Identifiable, Differentiable, Hashable {
    // func isContentEqual(to source: TwilioChannel) -> Bool {
    //     id == source.id &&
    //     lastMessageString == source.lastMessageString &&
    //     lastMessageDate == source.lastMessageDate &&
    //     unreadCount == source.unreadCount &&
    //     lastReadMessageIndex == source.lastReadMessageIndex
    // }

    var differenceIdentifier: String { id }
    func hash(into hasher: inout Hasher) { hasher.combine(id) }

    var id: String { channelID }
    let channelID: String
    let channelUniqueName: String
    let residentUser: User
    var otherMembers: [User]
    var allMembers: [User]
    var lastMessageString: String?
    var lastMessageDate: Date?
    var unreadCount: UInt
    var lastReadMessageIndex: UInt?
}

extension TwilioChannel: Codable {
    enum CodingKeys: String, CodingKey {
        case channelID,
             channelUniqueName,
             residentUser,
             otherMembers,
             allMembers,
             lastMessageString,
             lastMessageDate,
             unreadCount,
             lastReadMessageIndex
    }

    public init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        channelID = try map.decode(.channelID)
        channelUniqueName = try map.decode(.channelUniqueName)
        residentUser = try map.decode(.residentUser)
        otherMembers = try map.decode(.otherMembers)
        allMembers = try map.decode(.allMembers)
        lastMessageString = try? map.decodeIfPresent(.lastMessageString)
        lastMessageDate = map.decodeDateIfPresent(.lastMessageDate)
        unreadCount = try map.decode(.unreadCount)
        lastReadMessageIndex = try map.decodeIfPresent(.lastReadMessageIndex)
    }
}

class ChatMessageProvider {
    var tchChannel: TCHChannel?
    var twilioChannel: TwilioChannel
    var perPage: UInt = 50

    init(twilioChannel: TwilioChannel, channel: TCHChannel?) {
        self.tchChannel = channel
        self.twilioChannel = twilioChannel
    }

    func getCachedMessages() -> [ConciergeChat.Message] {
        [ConciergeChat.Message].readCached(.twilioConversation(twilioChannel.channelID)) ?? []
    }

    func getMessages() -> Promise<[TCHMessage]> {
        guard let messages = tchChannel?.messages else { return .value([]) }

        return Promise<[TCHMessage]> { seal in
            let perPage = self.perPage
            return messages.getLastWithCount(perPage) { result, messages in
                if result.isSuccessful() {
                    seal.fulfill(messages ?? [])
                } else {
                    seal.reject(APIError.from(result.error))
                }
            }
        }
    }

    func getOlderMessages(lastMessageIndex: UInt) -> Promise<[TCHMessage]> {
        guard let messages = tchChannel?.messages else { return .value([]) }

        return Promise<[TCHMessage]> { seal in
            messages.getBefore(lastMessageIndex, withCount: self.perPage) { result, messages in
                if result.isSuccessful() {
                    seal.fulfill(messages ?? [])
                } else {
                    seal.reject(APIError.from(result.error))
                }
            }
        }
    }
}

class ChannelMetadataProvider {

    private var channel: TCHChannel
    private var message: TCHMessage?
    private var users = [User]()
    private var members = [TCHMember]()
    private var unreadCount: UInt = 0
    private var currentUser: User?

    init(channel: TCHChannel, currentUser: User?) {
        self.channel = channel
        self.currentUser = currentUser
    }

    // This cannot fail
    private func fetchLastMessage() -> Promise<Void> {
        Promise<Void> { seal in
            channel.fetchLastMessage().map { self.message = $0 }.ensure { seal.fulfill(()) }.cauterize()
        }
    }

    // This can fail
    private func fetchChannelUsers() -> Promise<Void> {
        channel.fetchUsers().map {
            self.users = $0.twilioUsers
            self.members = $0.twilioMembers
        }
    }

    // This cannot fail
    private func fetchUnreadCount() -> Promise<Void> {
        Promise<Void> { seal in
            channel.fetchUnreadCount().map { self.unreadCount = $0 }.ensure { seal.fulfill(()) }.cauterize()
        }
    }

    func fetchMetadata() -> Promise<TwilioChannel> {
        when(fulfilled:
             fetchChannelUsers(),
             fetchLastMessage(),
             fetchUnreadCount()
        ).compactMap { _ in
            guard let user = self.users.first(where: { $0.uid == self.channel.uniqueName }) else { return nil }
            guard let channelId = self.channel.sid,
                  let channelUniqueName = self.channel.uniqueName else { return nil }

            let otherUsers = self.users.filter { $0.uid != user.uid }

            return TwilioChannel(channelID: channelId,
                                 channelUniqueName: channelUniqueName,
                                 residentUser: user,
                                 otherMembers: otherUsers,
                                 allMembers: self.users,
                                 lastMessageString: self.message?.lastMessageString,
                                 lastMessageDate: self.channel.lastMessageDate,
                                 unreadCount: self.unreadCount,
                                 lastReadMessageIndex: self.lastReadMessageIndex)
        }
    }

    var lastReadMessageIndex: UInt? {
        guard let currentUser = currentUser else { return nil }
        return members.filter { $0.isMe(currentUserId: currentUser.uid, channelUniqueName: channel.uniqueName) }.compactMap { $0.lastConsumedMessageIndex?.uintValue }.sorted { $0 > $1 }.first
    }
}

fileprivate extension TCHMessage {
    var lastMessageString: String? {
        hasMedia() ? "Image" : body
    }
}

extension TCHMember {
    func isMe(currentUserId: String?, channelUniqueName: String?) -> Bool {
        switch AppState.userType {
        case .resident:
            return identity == currentUserId
        case .concierge:
            return identity != channelUniqueName
        }
    }
}