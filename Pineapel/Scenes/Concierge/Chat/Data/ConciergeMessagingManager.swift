//
// Created by Andrei Stoicescu on 24/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import TwilioChatClient
import PromiseKit
import Combine
import UIKit

extension ConciergeChat {
    class ChannelMessageDataSources {
        var sources = [String: MessageDatasource]()
        var twilioClientLoading: Bool = false {
            didSet {
                sources.forEach { key, source in
                    source.twilioClientLoading = twilioClientLoading
                }
            }
        }

        func findOrCreateSource(channel: TwilioChannel, tchChannel: TCHChannel?, currentUser: User) -> MessageDatasource {
            if let source = sources[channel.id] { return source }

            let source = MessageDatasource(twilioChannel: channel, tchChannel: tchChannel, currentUser: currentUser, channelMembers: channel.allMembers)
            sources[channel.id] = source
            return source
        }

        @discardableResult
        func memberReadChannel(member: TCHMember, channel: TwilioChannel) -> Message? {
            sources[channel.id]?.memberReadChannel(member)
        }

        func sourceForChannel(_ channel: TCHChannel) -> MessageDatasource? {
            sources.values.first { $0.twilioChannel.channelID == channel.sid }
        }

        func isChannelActive(_ channel: TCHChannel) -> Bool {
            sourceForChannel(channel)?.active ?? false
        }

        func update(_ channels: [TCHChannel]) {
            channels.forEach { channel in
                if let sid = channel.sid, let source = sources[sid], source.tchChannel == nil {
                    source.tchChannel = channel
                }
            }
        }

        func shutdown() {
            sources.removeAll()
        }

    }

    class MessagingManager: NSObject, ObservableObject {

        static let shared = MessagingManager()

        var channelListManager = ChannelListManager()

        var messageDataSources = ChannelMessageDataSources()

        var client: TwilioChatClient?

        @Published var isLoading = false

        private var connecting = false {
            didSet { isLoading = loading }
        }

        private var loading: Bool {
            connecting || channelListManager.isLoading || twilioClientLoading
        }

        private var twilioClientLoading: Bool = false {
            didSet { isLoading = loading }
        }

        private var subscriptions = Set<AnyCancellable>()

        var user: User? {
            didSet { channelListManager.currentUser = user }
        }

        var buildingID: String? {
            didSet { channelListManager.buildingID = buildingID }
        }

        override init() {
            super.init()
            channelListManager.$isLoading
                    .receive(on: DispatchQueue.main)
                    .sink { loading in
                        self.isLoading = self.loading
                    }.store(in: &subscriptions)

            channelListManager.$tchChannels
                    .receive(on: DispatchQueue.main)
                    .sink { channels in
                        MessagingManager.shared.messageDataSources.update(channels)
                    }.store(in: &subscriptions)
        }

        // MARK: Twilio Client

        func connect() {
            connectClient().cauterize()
        }

        private func connectClient() -> Promise<Void> {
            if client?.connectionState == .connected {
                return .value(())
            }

            connecting = true
            return ResidentService().getTwilioAccessToken().then { token in
                self.initializeClientWithToken(token: token.token)
            }
        }

        func initializeClientWithToken(token: String) -> Promise<Void> {
            Promise<Void> { seal in
                TwilioChatClient.chatClient(withToken: token, properties: nil, delegate: self) { [weak self] result, chatClient in
                    guard (result.isSuccessful()) else {
                        seal.reject(APIError.from(result.error))
                        return
                    }

                    Log.this("CHAT CLIENT CONNECTED")

                    self?.client = chatClient
                    seal.fulfill(())
                }
            }
        }

        private func refreshAccessToken() {
            ResidentService().getTwilioAccessToken().done { token in
                self.client?.updateToken(token.token) { result in
                    if (result.isSuccessful()) {
                        Log.this("Access token refreshed")
                    } else {
                        Log.this("Unable to refresh access token")
                    }
                }
            }.catch { error in
                Log.thisError(error)
            }
        }

        func shutdown() {
            if let client = client {
                client.delegate = nil
                client.shutdown()
                self.client = nil
            }

            messageDataSources.shutdown()
            channelListManager.shutdown()
            TwilioImageManager.shared.clearAll()
            TwilioHelper.clear()
            connecting = false
            isLoading = false
        }
    }

}

// MARK: - TwilioChatClientDelegate
extension ConciergeChat.MessagingManager: TwilioChatClientDelegate {
    public func chatClient(_ client: TwilioChatClient, connectionStateUpdated state: TCHClientConnectionState) {
        switch state {
        case .connecting:
            twilioClientLoading = true
        default:
            twilioClientLoading = false
        }

        messageDataSources.twilioClientLoading = twilioClientLoading
    }

    func chatClient(_ client: TwilioChatClient, channelAdded channel: TCHChannel) {
        channelListManager.channelAdded(channel)
    }

    func chatClient(_ client: TwilioChatClient, channel: TCHChannel, updated: TCHChannelUpdate) {

    }

    func chatClient(_ client: TwilioChatClient, channelDeleted channel: TCHChannel) {

    }

    func chatClient(_ client: TwilioChatClient, synchronizationStatusUpdated status: TCHClientSynchronizationStatus) {
        Log.this("CHAT SYNC STATUS CHANGED: \(status)")

        if status == TCHClientSynchronizationStatus.completed {
            Log.this("CHAT SYNC COMPLETED")
            connecting = false

            if let list = client.channelsList() {
                channelListManager.channelsList = list
            }
        }
    }

    func chatClientTokenWillExpire(_ client: TwilioChatClient) {
        Log.this("Chat Client Token will expire.")
        refreshAccessToken()
    }

    func chatClientTokenExpired(_ client: TwilioChatClient) {
        Log.this("Chat Client Token will expire.")
        refreshAccessToken()
    }

    func chatClient(_ client: TwilioChatClient, channel: TCHChannel, member: TCHMember, updated: TCHMemberUpdate) {
        switch updated {
        case .lastConsumedMessageIndex:
            if let twilioChannel = channelListManager.findChannel(channel) {
                // This manually sets the lastReadMessage on the datasource, but doesn't change the channel's cached lastReadIndex
                messageDataSources.memberReadChannel(member: member, channel: twilioChannel)
            }

            channelListManager.channelRead(channel, member: member, lastReadMessageIndex: member.lastConsumedMessageIndex?.uintValue)
        default:
            ()
        }
    }

    func chatClient(_ client: TwilioChatClient, typingEndedOn channel: TCHChannel, member: TCHMember) {
        if let twilioChannel = channelListManager.findChannel(channel) {
            if let source = messageDataSources.sources[twilioChannel.id] {
                source.typing = .init(twilioUser: source.findMember(member), isTyping: false)
            }
        }
    }

    func chatClient(_ client: TwilioChatClient, typingStartedOn channel: TCHChannel, member: TCHMember) {
        if let twilioChannel = channelListManager.findChannel(channel) {
            if let source = messageDataSources.sources[twilioChannel.id] {
                source.typing = .init(twilioUser: source.findMember(member), isTyping: true)
            }
        }
    }

    // Called whenever a channel we've joined receives a new message
    func chatClient(_ client: TwilioChatClient, channel: TCHChannel, messageAdded message: TCHMessage) {
        channelListManager.channelAddedMessage(channel, message: message, channelActive: messageDataSources.isChannelActive(channel))

        if let twilioChannel = channelListManager.findChannel(channel) {
            if let source = messageDataSources.sources[twilioChannel.id] {

                if !source.replaceByLocalId(message) {
                    source.prependMessage(message)
                }
            }
        }
    }
}
