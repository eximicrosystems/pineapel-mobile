//
// Created by Andrei Stoicescu on 30/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import TwilioChatClient
import DifferenceKit

extension ConciergeChat {
    enum MessageType: String, Codable {
        case outgoing
        case incoming

        var isOutgoing: Bool {
            self == .outgoing
        }
    }

    struct MessageGroup: Identifiable, Equatable {
        static func ==(lhs: MessageGroup, rhs: MessageGroup) -> Bool {
            lhs.id == rhs.id && lhs.messages.count == rhs.messages.count && lhs.groupType == rhs.groupType && lhs.userId == rhs.userId
        }

        var id: String
        var messages: [String]
        var groupType: MessageType
        var userId: String?
        var date: Date?
        var daySplit = false

        /// used in group merging
        func belongsToAdjacentGroup(_ group: MessageGroup) -> Bool {
            groupType == group.groupType &&
            userId == group.userId
        }

        func messageBelongsInGroup(_ message: Message) -> Bool {
            groupType == message.messageType &&
            userId == message.user?.uid
        }
    }

    enum OutgoingState: String, Codable {
        case none
        case sending
        case failed
    }

    enum MessageContentType: String, Codable {
        case text
        case image
    }

    // Twilio main message object
    // Note that if you add a new prop here, this has a custom Codable implementation and you'll need to add the CodingKeys and enc/dec implementation
    struct Message: Identifiable {
        var id: String
        var body: String
        var message: TCHMessage?
        var date: Date?
        var messageType: MessageType
        var outgoingState: OutgoingState = .none
        var user: User?
        var messageIndex: UInt?
        var mediaId: String?

        func timestampDiff(message: Message) -> Int {
            guard let lhsDate = date, let rhsDate = message.date else { return 0 }
            return Int(abs(lhsDate.distance(to: rhsDate)))
        }

        func isInDifferentDay(message: Message) -> Bool {
            guard let lhsDate = date, let rhsDate = message.date else { return false }
            return !lhsDate.isSameDay(date: rhsDate)
        }

        func shouldSplitByTimestamp(message: Message) -> Bool {
            timestampDiff(message: message) > 600 || isInDifferentDay(message: message)
        }

        var cacheable: Bool {
            message != nil || outgoingState == .none
        }

        var messageContentType: MessageContentType {
            mediaId == nil ? .text : .image
        }

        var imageMessageData: Data? {
            TwilioHelper.getCachedLocalImage(from: self)?.jpegData(compressionQuality: 0.8)
        }
    }
}

extension ConciergeChat.Message {
    init(message: TCHMessage, messageType: ConciergeChat.MessageType, user: User?) {
        id = message.sid ?? UUID().uuidString
        self.message = message
        self.messageType = messageType
        self.user = user
        body = message.body ?? ""
        date = message.dateCreatedAsDate
        messageIndex = message.index?.uintValue
        mediaId = message.mediaSid
    }

    var hasImage: Bool {
        mediaId != nil
    }
}

extension ConciergeChat.Message: Codable {

    enum CodingKeys: String, CodingKey {
        case id
        case body
        case date
        case messageType
        case outgoingState
        case localImageIdentifier
        case user
        case messageIndex
        case mediaId
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)

        id = try map.decode(.id)
        body = try map.decode(.body)
        date = map.decodeDateIfPresent(.date)
        messageType = try map.decode(.messageType)
        outgoingState = try map.decode(.outgoingState)
        user = try? map.decode(.user)
        messageIndex = try? map.decodeIfPresent(.messageIndex)
        mediaId = try? map.decodeIfPresent(.mediaId)
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try container.encode(id, forKey: .id)
        try container.encode(body, forKey: .body)
        if let date = date {
            try? container.encode(AVSISO8601DateFormatter().string(from: date), forKey: .date)
        }
        try container.encode(messageType, forKey: .messageType)
        try container.encode(outgoingState, forKey: .outgoingState)
        try? container.encode(user, forKey: .user)
        try? container.encode(messageIndex, forKey: .messageIndex)
        try? container.encode(mediaId, forKey: .mediaId)
    }
}

extension ConciergeChat.Message: Hashable, Differentiable {
    static func ==(lhs: ConciergeChat.Message, rhs: ConciergeChat.Message) -> Bool {
        lhs.id == rhs.id &&
        lhs.body == rhs.body &&
        lhs.date == rhs.date &&
        lhs.messageType == rhs.messageType &&
        lhs.outgoingState == rhs.outgoingState &&
        lhs.messageIndex == rhs.messageIndex &&
        lhs.mediaId == rhs.mediaId
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

    var differenceIdentifier: Int {
        hashValue
    }
}
