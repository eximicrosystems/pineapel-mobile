//
// Created by Andrei Stoicescu on 30/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import TwilioChatClient
import PromiseKit

extension Decodable {
    init?(jsonDictionary: [AnyHashable: Any]?) throws {
        guard let dict = jsonDictionary,
              let data = try? JSONSerialization.data(withJSONObject: dict, options: []) else { return nil }
        self = try JSONDecoder().decode(Self.self, from: data)
    }
}

extension TCHMessage {
    struct Attributes: Codable {
        let localId: String?
        let userId: String?
        let fullName: String?
        let buildingID: String?

        enum CodingKeys: String, CodingKey {
            case localId = "local_id"
            case userId = "uid"
            case fullName = "name"
            case buildingID = "building"
        }

        var firstName: String? {
            fullName?.components(separatedBy: " ").first
        }

        var lastName: String? {
            fullName?.components(separatedBy: " ").suffix(from: 1).joined(separator: " ")
        }
    }

    var mappedAttributes: Attributes? {
        try? Attributes(jsonDictionary: attributes()?.dictionary)
    }
}

extension TCHChannel {

    struct TwilioUserMembers {
        let twilioUsers: [User]
        let twilioMembers: [TCHMember]
    }

    func fetchChannelMetadata(currentUser: User?) -> Promise<TwilioChannel> {
        ChannelMetadataProvider(channel: self, currentUser: currentUser).fetchMetadata()
    }

    func fetchLastMessage() -> Promise<TCHMessage?> {
        guard let messages = messages,
              let lastIndex = lastMessageIndex else { return .value(nil) }

        return Promise<TCHMessage?> { seal in
            messages.message(withIndex: lastIndex) { result, message in
                seal.fulfill(message)
            }
        }
    }

    func fetchUnreadCount() -> Promise<UInt> {
        Promise<UInt> { seal in
            getUnconsumedMessagesCount { result, count in
                seal.fulfill(count?.uintValue ?? 0)
            }
        }
    }

    func fetchUsers() -> Promise<TwilioUserMembers> {
        firstly {
            fetchMembers()
        }.then { members in
            self.fetchUserDescriptors(members: members)
        }.map { descriptors -> TwilioUserMembers in
            let twilioUsers = descriptors.compactMap { (member, descriptor) -> User? in
                if let user = try? User(jsonDictionary: descriptor.attributes()?.dictionary),
                   user.uid != "0",
                   user.uid != "" {
                    return user
                } else {
                    guard let userID = member.identity else { return nil }

                    // if the descriptor doesn't contain the user's name, try getting it from the channel's friendly name,
                    // if the identity matches with the channel unique name (ie: it's the resident)
                    var userName = descriptor.friendlyName
                    if userID == self.uniqueName, userName == nil {
                        userName = self.friendlyName
                    }

                    let generatedUser = User.basicUser(uid: userID, firstName: userName, lastName: nil, userPictureURL: nil)
                    return generatedUser
                }
            }

            let tchMembers = descriptors.compactMap { $0.0 }

            return TwilioUserMembers(twilioUsers: twilioUsers, twilioMembers: tchMembers)
        }
    }

    private func fetchMembers() -> Promise<[TCHMember]> {
        Promise<[TCHMember]> { seal in
            guard let members = members else {
                seal.reject(APIError(message: "Channel has no members object"))
                return
            }
            
            seal.fulfill(members.membersList())
        }
    }

    private func fetchUserDescriptors(members: [TCHMember]) -> Promise<[(TCHMember, TCHUserDescriptor)]> {
        when(resolved: members.map {
            fetchUserDescriptor(member: $0)
        }).map { results in
            results.compactMap { result in
                switch result {
                case .fulfilled(let descriptor):
                    return descriptor
                case .rejected(_):
                    return nil
                }
            }
        }
    }

    private func fetchUserDescriptor(member: TCHMember) -> Promise<(TCHMember, TCHUserDescriptor)> {
        Promise<(TCHMember, TCHUserDescriptor)> { seal in
            member.userDescriptor { result, descriptor in
                if result.isSuccessful() {
                    guard let descriptor = descriptor else {
                        seal.reject(APIError(message: "Channel member user descriptor is missing"))
                        return
                    }
                    seal.fulfill((member, descriptor))
                } else {
                    seal.reject(APIError.from(result.error))
                }
            }
        }
    }
}
