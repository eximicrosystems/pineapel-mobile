//
// Created by Andrei Stoicescu on 11/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import Fakery
import Combine

enum ChatMessageType {
    case text
    case image
}

extension ConciergeChat {
    class ChatCellImage: ASDisplayNode {
        enum State {
            case image
            case loading
            case error
        }

        var imageNode = ASNetworkImageNode()
        var spinner = SpinnerLoadingNode(size: .medium, color: AVSColor.colorMilitaryGreen.color)
        var errorLabel = ASTextNode2()
        var state: State = .loading {
            didSet {
                switch state {
                case .image:
                    spinner.loading = false
                    errorLabel.isHidden = true
                case .loading:
                    spinner.loading = true
                    errorLabel.isHidden = true
                case .error:
                    spinner.loading = false
                    errorLabel.isHidden = false
                }
            }
        }

        override init() {
            imageNode.cornerRadius = 4
            imageNode.backgroundColor = AVSColor.tertiaryBackground.color
            errorLabel.attributedText = L10n.Errors.loadingImage.attributed(.body, .primaryLabel, alignment: .center)
            errorLabel.isHidden = true

            super.init()
            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            errorLabel.centered()
                      .margin(horizontal: 30)
                      .overlay(spinner.overlay(imageNode.ratio(3 / 4)))
        }
    }

    class ChatStateNode: ASDisplayNode {
        enum State {
            case none
            case sending
            case failed
            case read
        }

        var spinner = SpinnerLoadingNode(size: .small, color: AVSColor.primaryIcon.color, scale: 0.7)
        var errorStateNode = ASTextNode2()
        var readNode = ASTextNode2()

        var state: State = .none {
            didSet { updateState(state) }
        }

        init(state: State) {

            errorStateNode.attributedText = "!".attributed(.noteMedium, .colorMilitaryGreen, alignment: .center)
//            errorStateNode.attributedText = "!".attributed(.noteMedium, .accentForeground, alignment: .center)
            errorStateNode.backgroundColor = AVSColor.accentBackground.color
            errorStateNode.cornerRadius = 8
            errorStateNode.clipsToBounds = true
            errorStateNode.hitTestSlop = UIEdgeInsets(top: -20, left: -20, bottom: -20, right: -20)

            readNode.attributedText = "✓".attributed(.noteMedium, .colorMilitaryGreen, alignment: .center)
//            readNode.attributedText = "✓".attributed(.noteMedium, .accentForeground, alignment: .center)
            readNode.backgroundColor = AVSColor.colorGreen.color
            readNode.cornerRadius = 8
            readNode.clipsToBounds = true

            super.init()
            self.state = state
            updateState(state)
            automaticallyManagesSubnodes = true
        }

        func reload() {
            updateState(state)
        }

        func updateState(_ state: State) {
            spinner.loading = false
            spinner.isHidden = true
            errorStateNode.isHidden = true
            readNode.isHidden = true

            switch state {
            case .none:
                ()
            case .sending:
                spinner.loading = true
                spinner.isHidden = false
            case .failed:
                errorStateNode.isHidden = false
            case .read:
                readNode.isHidden = false
            }
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            spinner.square(16)
                   .background(errorStateNode.square(16))
                   .background(readNode.square(16))
        }
    }

    class ChatInputSend: ASDisplayNode {
        struct Config {
            var camera: UIImage?
            var photos: UIImage?
            var send: NSAttributedString?
            var spacing: CGFloat

            static var `default`: Config {
                .init(camera: Asset.Images.icCamera.image,
                      photos: Asset.Images.icImage.image,
                      send: L10n.ChatScreen.Buttons.Send.title.attributed(.body, .primaryIcon),
                      spacing: 20)
            }
        }

        enum SendAction {
            case send
            case camera
            case photos
        }

        enum ButtonMode {
            case send
            case images
        }

        var sendButton = ASButtonNode()
        var cameraButton = ASButtonNode()
        var photosButton = ASButtonNode()
        var buttonMode = ButtonMode.images {
            didSet {
                if buttonMode != oldValue {
                    invalidateCalculatedLayout()
                    setNeedsLayout()
                    layoutIfNeeded()
                }
            }
        }

        var tapped: ((SendAction) -> Void)?
        var config: Config

        init(config: Config) {
            self.config = config
            sendButton.setImage(UIImage(named: "sendButton"), for: .normal)
            
            cameraButton.tintColor = AVSColor.colorMilitaryGreen.color
            cameraButton.setImage(config.camera?.withRenderingMode(.alwaysTemplate), for: .normal)
            cameraButton.tintColor = AVSColor.colorMilitaryGreen.color
            photosButton.setImage(config.photos?.withRenderingMode(.alwaysTemplate), for: .normal)
            photosButton.tintColor = AVSColor.colorMilitaryGreen.color

            super.init()

            sendButton.addTarget(self, action: #selector(tapSend), forControlEvents: .touchUpInside)
            cameraButton.addTarget(self, action: #selector(tapCamera), forControlEvents: .touchUpInside)
            photosButton.addTarget(self, action: #selector(tapPhotos), forControlEvents: .touchUpInside)
            backgroundColor = .clear
            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            switch buttonMode {
            case .send:
                return sendButton.wrapped()
            case .images:
                return
//                    [
                        [cameraButton, photosButton].hStacked().spaced(config.spacing)
//                        [sendButton].hStacked().aligned(.start)
//                    ].hStacked()
            }
        }

        
        
        
        @objc func tapSend() {
            tapped?(.send)
        }

        @objc func tapCamera() {
            tapped?(.camera)
        }

        @objc func tapPhotos() {
            tapped?(.photos)
        }
    }
    
    class ChatInputMedia: ASDisplayNode {
        struct Config {
            var send: NSAttributedString?
            var spacing: CGFloat

            static var `default`: Config {
                .init(send: L10n.ChatScreen.Buttons.Send.title.attributed(.body, .primaryIcon),
                      spacing: 20)
            }
        }

        enum SendAction {
            case send
        }

        enum ButtonMode {
            case send
        }

        var sendButton = ASButtonNode()
        var buttonMode = ButtonMode.send {
            didSet {
                if buttonMode != oldValue {
                    invalidateCalculatedLayout()
                    setNeedsLayout()
                    layoutIfNeeded()
                }
            }
        }

        var tapped: ((SendAction) -> Void)?
        var config: Config

        init(config: Config) {
            self.config = config
            sendButton.setImage(UIImage(named: "sendButton"), for: .normal)
            super.init()

            sendButton.addTarget(self, action: #selector(tapSend), forControlEvents: .touchUpInside)
            backgroundColor = .clear
            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            switch buttonMode {
            case .send:
                return [sendButton].hStacked().spaced(config.spacing)
            }
        }

        @objc func tapSend() {
            tapped?(.send)
        }
    }

    class ConciergeChatInputContainer<Content: ASDisplayNode>: ASDisplayNode {
        var box = ASDisplayNode()
        let content: Content

        override func asyncTraitCollectionDidChange(withPreviousTraitCollection previousTraitCollection: ASPrimitiveTraitCollection) {
            super.asyncTraitCollectionDidChange(withPreviousTraitCollection: previousTraitCollection)
            if asyncTraitCollection().userInterfaceStyle != previousTraitCollection.userInterfaceStyle {
                box.borderColor = AVSColor.inputBorder.color.cgColor
            }
        }

        init(_ content: () -> Content) {
            self.content = content()
            super.init()
            box.backgroundColor = AVSColor.inputFill.color
            box.borderWidth = 1
            box.borderColor = AVSColor.inputBorder.color.cgColor
            box.cornerRadius = 2
            automaticallyManagesSubnodes = true
            backgroundColor = AVSColor.chatInputFill.color
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            content.margin(horizontal: 10).background(box).margin(15)
        }
    }

    class ChatInputBox: ASDisplayNode, ASEditableTextNodeDelegate {

        var textNode = ASEditableTextNode()
        var sendButton: ChatInputSend
        var mediaButton: ChatInputMedia

        var onFrameChanged: ((CGRect) -> Void)?
        var didSendMessage: ((String) -> Void)?
        var didSelectCameraAction: (() -> Void)?
        var didSelectPhotosAction: (() -> Void)?
        var typing: (() -> Void)?

        override init() {
            sendButton = .init(config: .default)
            mediaButton = .init(config: .default)
            textNode.textContainerInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
            textNode.onDidLoad { node in
                let attributes = NSAttributedString.attributes(
                        font: AVSFont.body.font,
                    color: AVSColor.colorMilitaryGreen.color,
                        alignment: .left
                )
                let node = node as! ASEditableTextNode
                node.textView.typingAttributes = attributes

                // remove the bar with the Done button
                node.textView.inputAccessoryView = UIView()
            }

            textNode.attributedPlaceholderText = L10n.ChatScreen.Inputs.MessageInput.placeholder.attributed(.body, .tertiaryLabel)
            textNode.maximumLinesToDisplay = 3

            super.init()

            sendButton.tapped = { [weak self] action in
                self?.handleSendButton(action: action)
            }
            
            mediaButton.tapped = { [weak self] action in
                self?.handleMediaButton(action: action)
            }

            backgroundColor = .clear
            textNode.delegate = self
            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            [
                sendButton,
                textNode.alignSelf(.stretch).filling().minHeight(.points(40)),
                mediaButton,
            ].hStacked().spaced(10)
        }

        func editableTextNodeDidUpdateText(_ editableTextNode: ASEditableTextNode) {
            if let text = editableTextNode.attributedText?.string {
                if text.count == 0 {
                    sendButton.buttonMode = .images
                } else {
                    sendButton.buttonMode = .send
                }
            } else {
                sendButton.buttonMode = .images
            }

            typing?()
            supernode?.invalidateCalculatedLayout()
            invalidateCalculatedLayout()
            setNeedsLayout()
            supernode?.setNeedsLayout()
        }

        func clearTextInput() {
            textNode.attributedText = nil
            sendButton.buttonMode = .images
        }

        func handleSendButton(action: ChatInputSend.SendAction) {
            switch action {

            case .send:
                if let msg = textNode.attributedText?.string.trimmingCharacters(in: .whitespacesAndNewlines) {
                    if msg.count > 0 {
                        Log.this("sending message \(msg)")
                        didSendMessage?(msg)
                        clearTextInput()
                    }
                }
            case .camera:
                didSelectCameraAction?()
            case .photos:
                didSelectPhotosAction?()
            }
        }
        
        func handleMediaButton(action: ChatInputMedia.SendAction) {
            switch action {
            case .send:
                if let msg = textNode.attributedText?.string.trimmingCharacters(in: .whitespacesAndNewlines) {
                    if msg.count > 0 {
                        Log.this("sending message \(msg)")
                        didSendMessage?(msg)
                        clearTextInput()
                    }
                }
            }
        }

        override func layout() {
            super.layout()
            onFrameChanged?(frame)
        }

        func dismissKeyboardIfNeeded() {
            if textNode.isFirstResponder() {
                textNode.resignFirstResponder()
            }
        }

    }
}

extension ConciergeChat.ChatStateNode {
    static func chatState(message: ConciergeChat.Message, lastRead: ConciergeChat.Message?) -> State {
        if lastRead?.id == message.id {
            return .read
        }

        switch message.outgoingState {
        case .none:
            return .none
        case .sending:
            return .sending
        case .failed:
            return .failed
        }
    }
}
