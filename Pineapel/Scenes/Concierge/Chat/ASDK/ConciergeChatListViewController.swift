//
// Created by Andrei Stoicescu on 24/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import PromiseKit
import Combine
import Fakery
import TwilioChatClient
import SwiftyUserDefaults
import DifferenceKit

extension ConciergeChat {
    class ConciergeChatListViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

        lazy var emptyNode = EmptyNode(emptyNodeType: .generic, showAction: false)

        var collectionNode: ASCollectionNode

        var channels = [TwilioChannel]()

        var conversationIdToOpen: String?

        lazy var titleView = TitleView()

        private var subscriptions = Set<AnyCancellable>()

        override init() {
            collectionNode = .avsCollection()
            channels = channels.readCached(.twilioChannels) ?? []
            super.init(node: collectionNode)
            collectionNode.delegate = self
            collectionNode.dataSource = self

            Log.this("STARTED CHAT BOOTSTRAP")

            when(fulfilled: BuildingDetails.getBuildingID(), User.fetch()).map { buildingID, user in
                MessagingManager.shared.user = user
                MessagingManager.shared.buildingID = buildingID
            }.done {
                MessagingManager.shared.connect()
            }.cauterize()

            MessagingManager.shared.channelListManager.$totalUnreadCount
                    .receive(on: DispatchQueue.main)
                    .sink { [weak self] count in
                        self?.updateBadge(count)
                    }.store(in: &subscriptions)

            MessagingManager.shared.channelListManager.$sortedItems
                    .receive(on: DispatchQueue.main)
                    .dropFirst() // First time there's an empty array sent which we need to ignore
                    .sink { [weak self] items in
                        if items.count > 0 {
                            items.cache(.twilioChannels)
                        }
                        self?.reloadData(items)
                    }.store(in: &subscriptions)
        }

        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }

        func reloadData(_ newChannels: [TwilioChannel]) {
            let stagedChangeSet = StagedChangeset(source: channels, target: newChannels)
            for changeSet in stagedChangeSet {
                channels = changeSet.data
                collectionNode.performDiffBatchUpdates(changeSet: changeSet)
            }

            if let conversationId = conversationIdToOpen {
                openConversationId(conversationId)
            }
        }

        override func viewDidLoad() {
            super.viewDidLoad()
            collectionNode.reloadData()
            setupBindings()
            replaceSystemBackButton()
            showEmptyScreen(false)
            collectionNode.view.backgroundView = emptyNode.view
            navigationItem.titleView = titleView
            titleView.mainTitle = "Chat"

            let btn = UIButton(type: .custom)
            btn.setImage(Asset.Images.Concierge.icPaperPlane.image.withRenderingMode(.alwaysTemplate), for: .normal)
            btn.tintColor = AVSColor.primaryIcon.color
            btn.addTarget(self, action: #selector(tapAddRecipient), for: .touchUpInside)
            navigationItem.rightBarButtonItem = UIBarButtonItem(customView: btn)
        }

        @objc func tapAddRecipient() {
            let vc = ConciergeChatNewRecipientViewController()
            let nav = AVSNavigationController(rootViewController: vc)
            nav.isModalInPresentation = true
            navigationController?.present(nav, animated: true)
            nav.dismissNavigation = { [weak self] in
                self?.navigationController?.dismiss(animated: true, completion: nil)
            }
            vc.didSelectUser = { [weak self] user in
                self?.navigationController?.dismiss(animated: true, completion: nil)
                SVProgressHUD.show()
                MessagingManager.shared.channelListManager.channelForResident(user).done { [weak self] channel in
                    SVProgressHUD.dismiss()
                    self?.openConversation(channel)
                }.catch { error in
                    Log.thisError(error)
                    SVProgressHUD.showError(withStatus: error.localizedDescription)
                }
            }
        }

        func setupBindings() {
            MessagingManager.shared.$isLoading
                    .receive(on: DispatchQueue.main)
                    .sink { [weak self] loading in
                        guard let self = self else { return }
                        if loading {
                            if self.channels.count == 0 {
                                SVProgressHUD.show()
                            }
                            self.titleView.typing = "updating..."
                        } else {
                            SVProgressHUD.dismiss()
                            self.titleView.typing = nil
                        }
                    }.store(in: &subscriptions)
        }

        func updateBadge(_ val: UInt) {
            navigationController?.tabBarItem.badgeValue = val > 0 ? String(val) : nil
        }

        func showEmptyScreen(_ show: Bool) {
            emptyNode.isHidden = !show
        }

        func openConversation(_ twilioChannel: TwilioChannel) {
            let tchChannel = MessagingManager.shared.channelListManager.tchChannel(for: twilioChannel)

            User.fetch().done { [weak self] user in
                guard let self = self else { return }

                let source = MessagingManager.shared.messageDataSources.findOrCreateSource(channel: twilioChannel, tchChannel: tchChannel, currentUser: user)
                self.show(ChatViewController(title: twilioChannel.residentUser.fullName, dataSource: source), sender: self)
            }.catch { error in
                Log.thisError(error)
            }
        }

        func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
            channels.count
        }

        func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
            let channel = channels[indexPath.item]
            return {
                ConversationCell(channel: channel)
            }
        }

        func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
            let channel = channels[indexPath.item]
            openConversation(channel)
        }

        func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
            .fullWidth(collectionNode: collectionNode)
        }

        func openConversationId(_ conversationId: String) {
            conversationIdToOpen = nil

            guard let channel = channels.first(where: { $0.channelUniqueName == conversationId }) else {
                conversationIdToOpen = conversationId
                return
            }
            openConversation(channel)
        }
    }

}

