//
// Created by Andrei Stoicescu on 01/10/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import ImageViewer
import PromiseKit
import Combine
import DifferenceKit
import TwilioChatClient

extension ConciergeChat {
    struct MessageViewModel: Hashable, Differentiable {
        var id: String { message.id }
        var message: Message
        var lastReadMessage: Message?
        var showsTimestamp: Bool
        var showsAvatar: Bool
        var showsDay: Bool
        var firstMessageInGroup: Bool

        func hash(into hasher: inout Hasher) { hasher.combine(id) }

        var differenceIdentifier: Int { hashValue }

        init(message: Message, group: MessageGroup?, lastRead: Message?) {
            self.message = message
            lastReadMessage = lastRead
            firstMessageInGroup = message.id == group?.messages.last
            showsAvatar = message.id == group?.messages.last
            showsTimestamp = message.id == group?.messages.first
            showsDay = firstMessageInGroup && (group?.daySplit ?? false)
        }
    }

    class TitleView: UIView {
        lazy var stackView: UIStackView = {
            let stackView = UIStackView()
            stackView.translatesAutoresizingMaskIntoConstraints = false
            stackView.axis = .vertical
            stackView.distribution = .fill
            stackView.alignment = .fill
            // The font label has a huge height compared to the actual text
            stackView.spacing = -5
            return stackView
        }()

        lazy var typingLabel: UILabel = {
            let typingLabel = UILabel()
            typingLabel.textAlignment = .center
            typingLabel.font = AVSFont.note.font
            typingLabel.textColor = AVSColor.secondaryLabel.color
            return typingLabel
        }()

        var typing: String? = nil {
            didSet {
                if typing != nil {
                    if !stackView.arrangedSubviews.contains(typingLabel) {
                        typingLabel.text = typing
                        stackView.addArrangedSubview(typingLabel)
                    }
                } else {
                    typingLabel.removeFromSuperview()
                }
            }
        }

        lazy var mainLabel: UILabel = {
            let label = UILabel()
            label.textAlignment = .center
            return label
        }()

        var mainTitle: String? {
            didSet {
                mainLabel.attributedText = NSAttributedString(string: mainTitle, attributes: Appearance.navigationBarTitleTextAttributes)
            }
        }
        
        lazy var mainLogo: UIImageView = {
            let image = UIImageView(image: UIImage.init(named: "app_logo_small.jgp"))
            image.frame.size.width = 40
            image.frame.size.height = 40
            return image
        }()
        
        init() {
            super.init(frame: .zero)
            stackView.addArrangedSubview(mainLogo)
            addSubview(stackView)
            stackView.snp.makeConstraints { make in
                make.edges.equalToSuperview()
            }
        }

        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }

    class ChatViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

        enum Sections: Int, CaseIterable, Differentiable {
            case bottomLoading
            case messages
        }

        lazy var emptyNode = EmptyNode(emptyNodeType: .chat)

        var collectionNode: ASCollectionNode
        var keyboardHeight: CGFloat = 0

        var keyboardInfo: KeyboardResponder.KeyboardInfo = .zero {
            didSet {
                if keyboardInfo.height == 0 {
                    keyboardHeight = 0
                } else {
                    let tabBarHeight = navigationController?.tabBarController?.tabBar.frame.height ?? 0
                    keyboardHeight = keyboardInfo.height - view.safeAreaInsets.bottom - tabBarHeight
                }

                node.setNeedsLayout()
                UIView.animate(withDuration: keyboardInfo.animationDuration) {
                    self.view.layoutIfNeeded()
                }
            }
        }

        var chatInput: ChatInputBox
        var chatInputWrapper: ASDisplayNode

        lazy var imagePicker = ImagePicker(presentationController: self, delegate: self)
        var imagePickerSelectedImage: UIImage?

        var dataSource: MessageDatasource

        var sections = [ArraySection<Sections, AnyDifferentiable>]()
        lazy var keyboardResponder = KeyboardResponder()

        var loading: Bool {
            if dataSource.tchChannel == nil {
                Log.this("chat is still loading due to tchChannel being nil")
                return true
            }

            if dataSource.messages.count == 0 && dataSource.isLoadingPage {
                Log.this("chat is still loading due 0 messages and datasource.isLoadingPage == true")
                return true
            }

            if dataSource.twilioClientLoading {
                Log.this("chat is still loading due to twilioClientLoading")
                return true
            }

            Log.this("chat is not loading anymore")

            return false
        }

        var unreadCount: UInt = 0 {
            didSet {
                updateBadge(unreadCount)
            }
        }

        lazy var titleView = TitleView()

        private var subscriptions = Set<AnyCancellable>()
        private var headerTitle: String

        // if set to true, subscribes to badge changes (used in resident mode)
        var autoUpdateBadge = false

        init(title: String, dataSource: MessageDatasource, autoUpdateBadge: Bool = false) {
            self.dataSource = dataSource
            self.autoUpdateBadge = autoUpdateBadge
            headerTitle = title

            collectionNode = .avsCollection()
            collectionNode.backgroundColor = .clear

            let chatInput = ChatInputBox()
            self.chatInput = chatInput

            chatInputWrapper = ConciergeChatInputContainer { chatInput }

            let mainNode = ASDisplayNode()
            super.init(node: mainNode)

            self.dataSource.didSetTCHChannel = { [weak self] in
                guard let self = self else { return }
                self.dataSource.loadMoreContentIfNeeded()
            }

            collectionNode.delegate = self
            collectionNode.dataSource = self
            collectionNode.inverted = true

            setupBindings()

            navigationController?.tabBarItem.badgeColor = AVSColor.accentBackground.color

            mainNode.automaticallyManagesSubnodes = true
            mainNode.layoutSpecBlock = { [weak self] node, range in
                guard let self = self else { return ASDisplayNode().wrapped() }
                return [
                    self.collectionNode.growing(),
                    self.chatInputWrapper
                ].vStacked().margin(bottom: self.keyboardHeight > 0 ? self.keyboardHeight : 0)
            }

            chatInput.didSendMessage = { [weak self] message in
                self?.dataSource.sendTextMessage(message)
            }

            chatInput.didSelectPhotosAction = { [weak self] in
                guard let self = self else { return }
                self.imagePicker.present(from: self.view, action: .photoLibrary)
            }

            chatInput.didSelectCameraAction = { [weak self] in
                guard let self = self else { return }
                self.imagePicker.present(from: self.view, action: .camera)
            }

            chatInput.typing = { [weak self] in
                self?.dataSource.tchChannel?.typing()
            }
        }

        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }

        func setupBindings() {
            dataSource.$messageGroups.receive(on: DispatchQueue.main).sink { [weak self] groups in
                guard let self = self else { return }
                self.reloadData()
                self.markChatAsRead()
            }.store(in: &subscriptions)

            dataSource.$lastReadMessage.receive(on: DispatchQueue.main).sink { [weak self] lastReadMessage in
                guard let self = self else { return }
                self.reloadData()
                self.markChatAsRead()
            }.store(in: &subscriptions)

            dataSource.$typing.receive(on: DispatchQueue.main).sink { [weak self] typing in
                guard let self = self else { return }
                let user = typing.twilioUser?.firstName ?? "Someone"
                let typingString = typing.isTyping ? "\(user) is typing..." : nil
                if typingString != self.titleView.typing {
                    self.titleView.typing = typingString
                }
            }.store(in: &subscriptions)

            dataSource.$twilioClientLoading.receive(on: DispatchQueue.main).sink { [weak self] loading in
                guard let self = self else { return }
                self.reloadData()
            }.store(in: &subscriptions)

            MessagingManager.shared.channelListManager.$totalUnreadCount
                    .receive(on: DispatchQueue.main)
                    .sink { [weak self] count in
                        if self?.autoUpdateBadge == true {
                            self?.updateBadge(count)
                        }
                    }.store(in: &subscriptions)

            dataSource.loadCachedMessages()
            dataSource.loadMoreContentIfNeeded()
        }

        func updateBadge(_ badge: UInt) {
            navigationController?.tabBarItem.badgeValue = badge > 0 ? String(badge) : nil
        }

        func reloadData() {
            let newMessages = dataSource.messages.map { MessageViewModel(message: $0, group: dataSource.groupForMessage($0), lastRead: dataSource.lastReadMessage) }

            let target: [ArraySection<Sections, AnyDifferentiable>] = [
                ArraySection(model: .bottomLoading, elements: loading ? [AnyDifferentiable(String(loading))] : []),
                ArraySection(model: .messages, elements: newMessages.map { AnyDifferentiable($0) })
            ]

            let stagedChangeSet = StagedChangeset(source: sections, target: target)
            for changeSet in stagedChangeSet {
                sections = changeSet.data
                collectionNode.performDiffBatchUpdates(changeSet: changeSet)
            }
        }

        override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(animated)
            dataSource.active = true
            markChatAsRead()
        }

        func markChatAsRead() {
            dataSource.markAsReadIfActive { [weak self] int in
                self?.unreadCount = int
            }
        }

        override func viewDidDisappear(_ animated: Bool) {
            super.viewDidDisappear(animated)
            dataSource.active = false
        }

        override func viewDidLoad() {
            super.viewDidLoad()
            let tabBar = BottonTabBarView(frame: CGRect(x: 0, y: 761, width: UIScreen.main.bounds.width, height: 83))
            self.view.addSubview(tabBar)
            let lateralMenu = LateralMenuView(frame: CGRect(x: 80, y: 0, width: 347, height: 844))
            let myView = HeaderTabBarView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 190), tabBarVC: self, lm: lateralMenu)
            lateralMenu.isHidden = true
            lateralMenu.header = myView
            self.view.addSubview(myView)
            self.view.addSubview(lateralMenu)
            
            keyboardResponder.$keyboardInfo.subscribe(Subscribers.Assign(object: self, keyPath: \.keyboardInfo))

            edgesForExtendedLayout = []
            showEmptyScreen(false)
            collectionNode.view.backgroundView = emptyNode.view
            emptyNode.view.flipY()
            emptyNode.actionButtonTapped = { [weak self] in
                self?.chatInput.textNode.becomeFirstResponder()
            }

            view.backgroundColor = AVSColor.secondaryBackground.color
        }
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            navigationController?.setNavigationBarHidden(true, animated: false)
        }
        
        override func viewWillDisappear(_ animated: Bool) {
            super.viewWillDisappear(animated)
            navigationController?.setNavigationBarHidden(false, animated: true)
        }

        func showEmptyScreen(_ show: Bool) {
            emptyNode.isHidden = !show
        }

        func collectionView(_ collectionView: ASCollectionView, willDisplayNodeForItemAt indexPath: IndexPath) {
            guard let section = Sections(rawValue: indexPath.section), section == .messages else { return }
            guard let messageModel = sections[indexPath.section].elements[indexPath.item].base as? MessageViewModel else { return }
            dataSource.loadMoreContentIfNeeded(currentMessage: messageModel.message)
        }

        func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
            .fullWidth(collectionNode: collectionNode)
        }

        func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
            guard sections[indexPath.section].model == .messages else { return }
            guard let messageModel = sections[indexPath.section].elements[indexPath.item].base as? MessageViewModel else { return }

            if messageModel.message.outgoingState == .failed {
                handleFailedMessage(messageModel.message, indexPath: indexPath)
            } else if messageModel.message.hasImage {
                presentImage(message: messageModel.message, indexPath: indexPath)
            }
        }

        func presentImage(message: Message, indexPath: IndexPath) {
            guard message.hasImage,
                  let node = collectionNode.nodeForItem(at: indexPath) as? MessageCell,
                  let image = node.chatCellImage.imageNode.image else {
                return
            }

            imagePickerSelectedImage = image
            presentImageGallery(GalleryViewController(startIndex: 0, itemsDataSource: self, configuration: GalleryHelpers.defaultConfiguration))
        }

        func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
            sections.count
        }

        func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
            sections[section].elements.count
        }

        func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
            let section = sections[indexPath.section].model
            switch section {
            case .bottomLoading:
                return {
                    ASCellNode.withNode(SpinnerLoadingNode(size: .medium, color: AVSColor.primaryIcon.color, loading: true))
                }
            case .messages:
                guard let message = sections[indexPath.section].elements[indexPath.item].base as? MessageViewModel else { return { ASCellNode() } }
                return {
                    MessageCell(messageModel: message)
                }
            }
        }

        func scrollViewDidScroll(_ scrollView: UIScrollView) {
            if scrollView.panGestureRecognizer.velocity(in: scrollView).y < 0 {
                chatInput.dismissKeyboardIfNeeded()
            }
        }

        func handleFailedMessage(_ message: Message, indexPath: IndexPath) {
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            alertController.addAction(UIAlertAction(title: L10n.General.delete, style: .destructive) { [weak self] _ in
                guard let self = self else { return }
                self.dataSource.removeMessage(message)
            })

            alertController.addAction(UIAlertAction(title: L10n.ChatScreen.Buttons.Resend.title, style: .default) { [weak self] _ in
                guard let self = self else { return }
                let message = self.dataSource.prepareResendMessage(message)
                self.dataSource.sendMessageObject(message)
            })

            alertController.addAction(UIAlertAction(title: L10n.General.cancel, style: .cancel, handler: nil))
            alertController.fixIpadIfNeeded(view: view)

            present(alertController, animated: true)
        }
    }
}

extension ConciergeChat.ChatViewController: ImagePickerDelegate {
    public func didSelect(image: UIImage?) {
        guard let image = image else { return }
        dataSource.sendImageMessage(image)
    }
}

extension ConciergeChat.ChatViewController: GalleryItemsDataSource {
    func itemCount() -> Int {
        imagePickerSelectedImage == nil ? 0 : 1
    }

    func provideGalleryItem(_ index: Int) -> GalleryItem {
        .image {
            $0(self.imagePickerSelectedImage)
        }
    }
}
