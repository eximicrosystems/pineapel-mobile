//
// Created by Andrei Stoicescu on 23/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import Combine
import PromiseKit
import SwiftyUserDefaults
import SwiftUI

class ConciergeProfileViewController: ASDKViewController<ASDisplayNode>, ASCollectionDataSource, ASCollectionDelegate {
    var collectionNode: ASCollectionNode

    var user: User {
        didSet {
            Defaults.user = user
            reloadData()
        }
    }

    lazy var imagePicker = ImagePicker(presentationController: self, delegate: self)

    init(user: User) {
        self.user = user
        collectionNode = .avsCollection()
        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()
        navigationItem.title = "Profile"

        let btn = UIButton(type: .custom)
        btn.setImage(Asset.Images.Concierge.icPencil.image.withRenderingMode(.alwaysTemplate), for: .normal)
        btn.addTarget(self, action: #selector(tapEdit), for: .touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: btn)
    }

    func reloadUser() {
        SVProgressHUD.show()
        ResidentService().getUser().done { [weak self] user in
            SVProgressHUD.dismiss()
            guard let self = self else { return }
            self.user = user
        }.catch { error in
            SVProgressHUD.dismiss()
            Log.thisError(error)
        }
    }

    func reloadData() {
        collectionNode.reloadItems(at: [IndexPath(item: 0, section: 0)])
    }

    @objc func tapEdit() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        alertController.addAction(UIAlertAction(title: "Edit Profile", style: .default) { [weak self] _ in
            guard let self = self else { return }
            let view = ConciergeProfileEditView(user: self.user, dismissBlock: { [weak self] requiresRefresh in
                guard let self = self else { return }
                self.navigationController?.dismiss(animated: true)
                if requiresRefresh {
                    self.reloadUser()
                }
            })

            let nav = AVSNavigationController(rootViewController: UIHostingController(rootView: view))
            nav.isModalInPresentation = true
            self.navigationController?.present(nav, animated: true)
        })

        alertController.addAction(UIAlertAction(title: "Change Picture", style: .default) { [weak self] _ in
            guard let self = self else { return }
            self.imagePicker.present(from: self.view)
        })

        alertController.addAction(UIAlertAction(title: "Change Password", style: .default) { [weak self] _ in
            guard let self = self else { return }
            let view = ConciergeChangePasswordView(user: self.user, dismissBlock: { [weak self] in
                self?.navigationController?.dismiss(animated: true)
            })

            let nav = AVSNavigationController(rootViewController: UIHostingController(rootView: view))
            nav.isModalInPresentation = true
            self.navigationController?.present(nav, animated: true)
        })

        if AppState.userType == .resident {
            alertController.addAction(UIAlertAction(title: "Change Email", style: .default) { [weak self] _ in
                guard let self = self else { return }
                let view = ConciergeChangeEmailView(user: self.user, dismissBlock: { [weak self] requiresRefresh in
                    self?.navigationController?.dismiss(animated: true)
                    if requiresRefresh {
                        self?.reloadUser()
                    }
                })

                let nav = AVSNavigationController(rootViewController: UIHostingController(rootView: view))
                nav.isModalInPresentation = true
                self.navigationController?.present(nav, animated: true)
            })
        }

        alertController.addAction(UIAlertAction(title: L10n.General.cancel, style: .cancel, handler: nil))

        alertController.fixIpadIfNeeded(view: view)
        present(alertController, animated: true)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        1
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let user = self.user
        let bio = user.about?.html?.string
        let userType = AppState.userType
        return {
            let summary: ConciergePagerListNodes.SummaryNode

            switch userType {
            case .concierge:
                summary = .init(config: .matrix(
                        [
                            [
                                .init(title: "Phone Number", subtitle: user.phoneNumber),
                                .init(title: "Gender", subtitle: user.gender?.rawValue),
                            ],
                            [.init(title: "Bio", subtitle: bio)],
                        ]))
            case .resident:
                summary = .init(config: .matrix(
                        [
                            [.init(title: "Email", subtitle: user.email)],
                            [.init(title: "Phone Number", subtitle: user.phoneNumber)],
                        ]))
            }

            let contentNode = ConciergeGenericNodes.BioProfileContent(title: user.fullName, subtitle: nil, bodyNode: summary)

            let cell = ConciergeGenericNodes.BioProfileCell(url: user.userPictureURL, imageSize: 150, contentNode: contentNode)
            cell.didTapImage = { [weak self] in
                self?.tapEdit()
            }

            return cell
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        ASSizeRange.fullWidth(collectionNode: collectionNode)
    }
}

extension ConciergeProfileViewController: ImagePickerDelegate {
    public func didSelect(image: UIImage?) {
        if let image = image {

            if let data = image.jpegData(compressionQuality: 0.8) {
                let userID = user.uid
                let apiService = ResidentService()

                SVProgressHUD.show()
                ResidentService().uploadImage(imageData: data).then { (fileUploadResponse: FileUploadResponse) -> Promise<UserProfile> in
                    let profile = UserProfile(userID: userID, userPicture: UserPicture(targetID: fileUploadResponse.fileID, width: nil, height: nil, url: nil))
                    return apiService.updateUserProfile(userID: userID, profile: profile)
                }.done { [weak self] userProfile in
                    guard let self = self else { return }
                    SVProgressHUD.dismiss()
                    self.reloadUser()
                }.catch { error in
                    SVProgressHUD.showError(withStatus: error.localizedDescription)
                    Log.thisError(error)
                }
            }
        }
    }
}
