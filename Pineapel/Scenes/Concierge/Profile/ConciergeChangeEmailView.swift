//
// Created by Andrei Stoicescu on 18.01.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//

import SwiftUI
import Alamofire
import PromiseKit
import SVProgressHUD
import Validator

struct AnyValidationError: ValidationError {
    var message: String
}

class ConciergeChangeEmailForm: ObservableObject {

    @Published var email: String = ""
    @Published var password: String = ""

    var userID: String?

    var asParams: UserProfile {
        UserProfile(userID: userID,
                    email: email,
                    password: .init(password: password, newPassword: nil, confirmNewPassword: nil))
    }

    var isValid: Bool {
        let emailRule = ValidationRulePattern(pattern: EmailValidationPattern.standard, error: AnyValidationError(message: "Email is invalid"))
        let result = email.validate(rule: emailRule)
        return [email, password].allSatisfy { !String.isBlank($0) } && result.isValid
    }

}

struct ConciergeChangeEmailView: View {
    @ObservedObject var form = ConciergeChangeEmailForm()

    var user: User
    var dismissBlock: ((_ requiresRefresh: Bool) -> Void)?

    var body: some View {
        ScrollView(showsIndicators: false) {
            VStack(spacing: 15) {
                ConciergeForm.TextInput(label: "Email", value: $form.email)
                ConciergeForm.SecureTextInput(label: "Password", value: $form.password)
            }.padding(.top, 15)
             .padding(.horizontal, 15)
        }.navigationBarItems(
                 leading: Button(action: {
                     dismissBlock?(false)
                 }) {
                     Image(uiImage: Asset.Images.Concierge.icClose.image.withRenderingMode(.alwaysTemplate))
                 },
                 trailing: Button(action: {
                     SVProgressHUD.show()

                     ResidentService().updateUserProfile(userID: user.uid, profile: form.asParams).done { profile in
                         SVProgressHUD.showInfo(withStatus: "Your email was changed")
                         dismissBlock?(true)
                     }.catch { error in
                         Log.thisError(error)
                         SVProgressHUD.showError(withStatus: error.localizedDescription)
                     }
                 }) {
                     Text("Save").navigationSaveButton()
                 }.disabled(!form.isValid))
         .navigationBarTitle(Text("Change Email"), displayMode: .inline)
         .onAppear {
             form.userID = user.id
         }
    }
}