//
// Created by Andrei Stoicescu on 23/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import SwiftUI
import Alamofire
import PromiseKit
import SVProgressHUD

class ConciergeChangePasswordForm: ObservableObject {

    @Published var currentPassword: String = ""
    @Published var newPassword: String = ""
    @Published var confirmPassword: String = ""

    var userID: String?

    var asParams: UserProfile {
        UserProfile(userID: userID,
                    password: UserPassword(
                            password: currentPassword,
                            newPassword: newPassword,
                            confirmNewPassword: confirmPassword))
    }

    var isValid: Bool {
        [currentPassword, newPassword, confirmPassword].allSatisfy { !String.isBlank($0) } && newPassword == confirmPassword
    }

}

struct ConciergeChangePasswordView: View {
    @ObservedObject var form = ConciergeChangePasswordForm()

    var user: User
    var dismissBlock: (() -> Void)?

    var body: some View {
        ScrollView(showsIndicators: false) {
            VStack(spacing: 15) {
                ConciergeForm.SecureTextInput(label: "Current Password", value: $form.currentPassword)
                ConciergeForm.SecureTextInput(label: "New Password", value: $form.newPassword)
                ConciergeForm.SecureTextInput(label: "Confirm New Password", value: $form.confirmPassword)
            }.padding(.top, 15)
             .padding(.horizontal, 15)
        }.navigationBarItems(
                 leading: Button(action: {
                     dismissBlock?()
                 }) {
                     Image(uiImage: Asset.Images.Concierge.icClose.image.withRenderingMode(.alwaysTemplate))
                 },
                 trailing: Button(action: {
                     SVProgressHUD.show()

                     ResidentService().updateUserProfile(userID: user.uid, profile: form.asParams).done { profile in
                         SVProgressHUD.showInfo(withStatus: "Your password was changed")
                         dismissBlock?()
                     }.catch { error in
                         Log.thisError(error)
                         SVProgressHUD.showError(withStatus: error.localizedDescription)
                     }
                 }) {
                     Text("Save").navigationSaveButton()
                 }.disabled(!form.isValid))
         .navigationBarTitle(Text("Change Password"), displayMode: .inline)
         .onAppear {
             form.userID = user.id
         }
    }
}