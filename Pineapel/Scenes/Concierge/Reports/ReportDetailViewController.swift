//
// Created by Andrei Stoicescu on 03.03.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import Combine
import PromiseKit
import SwiftyUserDefaults
import SwiftUI

class ReportDetailViewController: ASDKViewController<ASDisplayNode>, ASCollectionDataSource, ASCollectionDelegate {
    var collectionNode: ASCollectionNode

    var item: ConciergeReport.ModelRow

    init(item: ConciergeReport.ModelRow) {
        self.item = item
        collectionNode = .avsCollection()
        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()
        navigationItem.title = "Report Details"
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        1
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let item = self.item

        return {
            let summary = ConciergePagerListNodes.SummaryNode(config: .matrix(
                    item.items.map { fieldItem in
                        [.init(title: fieldItem.name, subtitle: fieldItem.value)]
                    }
            ))

            let cell = ASCellNode.withNode(summary, margin: UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15))
            cell.backgroundColor = AVSColor.secondaryBackground.color
            return cell
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }
}
