//
// Created by Andrei Stoicescu on 01.03.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import SwiftyUserDefaults
import PromiseKit

class ReportCategoryListViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

    class FilterDataStore {
        var categories = [ConciergeALaCarteCategory]()
        var amenities = [Amenity]()
        var filter = ReportFilterParams.default
    }

    enum Menu: Int, CaseIterable {
        case residents
        case feedback
        case activityLog
        case incidents
        case requests
        case packages
        case visitors
        case reservations
        case inventory
        case orders
        case performance

        var conciergeReportType: ConciergeReport.ReportType {
            switch self {
            case .residents:
                return .residents
            case .feedback:
                return .feedback
            case .activityLog:
                return .activityLog
            case .incidents:
                return .incidents
            case .requests:
                return .requests
            case .packages:
                return .packages
            case .visitors:
                return .visitors
            case .reservations:
                return .reservations
            case .inventory:
                return .inventory
            case .orders:
                return .orders
            case .performance:
                return .performance
            }
        }

        var title: String {
            switch self {
            case .residents:
                return "Residents"
            case .feedback:
                return "Feedback"
            case .activityLog:
                return "Activity Log"
            case .incidents:
                return "Incidents"
            case .requests:
                return "Requests"
            case .packages:
                return "Packages"
            case .visitors:
                return "Visitors"
            case .reservations:
                return "Reservations"
            case .inventory:
                return "Inventory"
            case .orders:
                return "Orders"
            case .performance:
                return "Performance"
            }
        }
    }

    var collectionNode: ASCollectionNode
    var filterDataStore = FilterDataStore()

    override init() {
        collectionNode = .avsCollection()
        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Reports"
        replaceSystemBackButton()

        SVProgressHUD.show()
        loadDataStore().ensure {
            SVProgressHUD.dismiss()
        }.cauterize()
    }

    func loadDataStore() -> Promise<Void> {
        when(fulfilled: [
            ConciergeService().getConciergeALaCarteCategories().get { categories in
                self.filterDataStore.categories = categories
            }.map { _ in
                ()
            },

            ConciergeService().getAmenityList(per: 50, page: 0).get { amenities in
                self.filterDataStore.amenities = amenities
            }.map { _ in
                ()
            }
        ])
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        Menu.allCases.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        guard let item = Menu(rawValue: indexPath.item) else { return { ASCellNode() } }

        return {
            ConciergeGenericNodes.MenuCell(icon: .none, title: item.title, subtitle: nil, menuStyle: .resident)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        guard let item = Menu(rawValue: indexPath.item) else { return }
        let reportType = item.conciergeReportType
        let vc = ReportListViewController(reportType: reportType, filterDataStore: filterDataStore)
        navigationController?.pushViewController(vc, animated: true)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }

}

