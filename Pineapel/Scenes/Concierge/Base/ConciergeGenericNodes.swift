//
// Created by Andrei Stoicescu on 26/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit

struct ConciergeGenericNodes {
    class LargeNavigationNode: ASDisplayNode {
        enum RightButtonType {
            case send
            case logout
        }

        var titleNode = ASTextNode2()
        var rightButton: ASButtonNode?
        var didTapRightButton: (() -> Void)?

        init(title: String, rightButtonType: RightButtonType? = nil) {
            super.init()

            titleNode.attributedText = title.attributed(.H2, .colorMilitaryGreen)

            if let rightButtonType = rightButtonType {
                let button = ASButtonNode()
                switch rightButtonType {
                case .send:
                    button.setImage(Asset.Images.Concierge.sendNotification.image, for: .normal)
                case .logout:
                    button.setImage(Asset.Images.Concierge.icLogout.image.withRenderingMode(.alwaysTemplate), for: .normal)
                    button.tintColor = AVSColor.primaryIcon.color
                }

                button.addTarget(self, action: #selector(tappedRightButton), forControlEvents: .touchUpInside)
                rightButton = button
            }

            automaticallyManagesSubnodes = true
            automaticallyRelayoutOnSafeAreaChanges = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            [titleNode, rightButton].hStacked().spaced(20).justified(.spaceBetween)
                                    .margin(top: safeAreaInsets.top, left: 15, bottom: 15, right: 15)
        }

        @objc private func tappedRightButton() {
            didTapRightButton?()
        }
    }

    class BadgeNode: ASDisplayNode {
        enum Style {
            case normal
            case small

            var font: AVSFont {
                switch self {
                case .normal:
                    return .body
                case .small:
                    return .note
                }
            }

            var insets: UIEdgeInsets {
                switch self {
                case .normal:
                    return UIEdgeInsets(top: 2, left: 8, bottom: 2, right: 8)
                case .small:
                    return UIEdgeInsets(top: 1, left: 7, bottom: 1, right: 7)
                }
            }

            var cornerRadius: CGFloat {
                switch self {
                case .normal:
                    return 10
                case .small:
                    return 8
                }
            }
        }

        var badge: Int = 0 {
            didSet {
                updateBadge(badge)
            }
        }
        var textNode = ASTextNode2()
        var badgeStyle: Style

        init(value: Int, backgroundColor: UIColor, style: Style = .normal) {
            badgeStyle = style

            super.init()

            updateBadge(value)

            self.backgroundColor = backgroundColor
            cornerRadius = badgeStyle.cornerRadius
            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            textNode.margin(badgeStyle.insets)
        }

        func updateBadge(_ value: Int) {
//            textNode.attributedText = String(value).attributed(badgeStyle.font, .accentForeground)
            textNode.attributedText = String(value).attributed(badgeStyle.font, .colorMilitaryGreen)
            isHidden = value == 0
        }
    }

    class TitleSubtitleNode: ASDisplayNode {
        var titleNode: ASTextNode2
        var subtitleNode: ASTextNode2?

        static var titleAttributes: [NSAttributedString.Key: Any] = {
            NSAttributedString.avsAttributes(font: .body, color: .primaryLabel)
        }()

        static var subtitleAttributes: [NSAttributedString.Key: Any] = {
            NSAttributedString.avsAttributes(font: .note, color: .secondaryLabel)
        }()

        static var highlightedTitleAttributes: [NSAttributedString.Key: Any] = {
            NSAttributedString.avsAttributes(font: .bodyHeavy, color: .primaryLabel)
        }()

        convenience init(title: String, subtitle: String?) {
            self.init(attributedTitle: NSAttributedString(string: title, attributes: TitleSubtitleNode.titleAttributes),
                      attributedSubtitle: NSAttributedString(string: subtitle, attributes: TitleSubtitleNode.subtitleAttributes))
        }

        init(attributedTitle: NSAttributedString?, attributedSubtitle: NSAttributedString?) {
            titleNode = ASTextNode2(with: attributedTitle)
            if let attributedSubtitle = attributedSubtitle {
                subtitleNode = ASTextNode2(with: attributedSubtitle)
            }

            super.init()
            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            [titleNode, subtitleNode].vStacked().spaced(2)
        }
    }

    class AvatarNode: ASControlNode {
        var imageNode = ASNetworkImageNode()
        var backgroundImageBorder = ASDisplayNode()
        var imageSize: CGFloat
        var border: CGFloat
        var imageBorderSize: CGFloat {
            imageSize + 2 * border
        }

        convenience init(url: URL?, imageSize: CGFloat, border: CGFloat) {
            self.init(imageSize: imageSize, border: border)
            imageNode.url = url
        }

        convenience init(image: UIImage, imageSize: CGFloat, border: CGFloat, imageBackground: UIColor? = .clear, imageTint: UIColor? = nil) {
            self.init(imageSize: imageSize, border: border, imageBackground: imageBackground)
            imageNode.image = image
            imageNode.contentMode = .center
            if let tint = imageTint {
                imageNode.tintColor = tint
            }
        }

        init(imageSize: CGFloat, border: CGFloat, imageBackground: UIColor? = .clear) {
            self.border = border
            self.imageSize = imageSize
            super.init()
            imageNode.cornerRadius = imageSize / 2
            imageNode.backgroundColor = imageBackground

            if border > 0 {
                backgroundImageBorder.backgroundColor = AVSColor.borderPrimary.color
            }

            backgroundImageBorder.cornerRadius = imageBorderSize / 2

            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            imageNode.square(imageSize)
                     .centered()
                     .overlay(backgroundImageBorder.square(imageBorderSize))
        }
    }

    class BioProfileContent: ASDisplayNode {
        var titleNode: ASTextNode2
        var subtitleNode: ASTextNode2
        var bodyNode: ASDisplayNode?

        convenience init(title: String?, subtitle: String?, body: String?) {
            let body = ASTextNode2(with: body?.attributed(.body, .primaryLabel))
            self.init(title: title, subtitle: subtitle, bodyNode: body)
        }

        init(title: String?, subtitle: String?, bodyNode: ASDisplayNode?) {
            titleNode = .init(with: title?.attributed(.H4, .primaryLabel))
            subtitleNode = .init(with: subtitle?.attributed(.bodyTitleMedium, .secondaryLabel))
            self.bodyNode = bodyNode
            super.init()
            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            [
                [titleNode, subtitleNode].vStacked().aligned(.center),
                bodyNode
            ].vStacked().spaced(30)
        }
    }

    class BioProfileCell: ASCellNode {
        var imageNode: AvatarNode
        var contentNode: ASDisplayNode
        var backgroundNode = ASDisplayNode()
        var imageSize: CGFloat
        var didTapImage: (() -> Void)?

        init(url: URL?, imageSize: CGFloat, border: CGFloat = 7, contentNode: ASDisplayNode) {
            imageNode = .init(url: url, imageSize: imageSize, border: border)
            self.imageSize = imageSize
            self.contentNode = contentNode
            backgroundNode.backgroundColor = AVSColor.secondaryBackground.color
            super.init()
            imageNode.addTarget(self, action: #selector(tapEdit), forControlEvents: .touchUpInside)
            automaticallyManagesSubnodes = true
        }

        @objc func tapEdit() {
            didTapImage?()
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            [
                imageNode.alignSelf(.center),
                contentNode
            ].vStacked().spaced(20)
             .margin(20)
             .background(backgroundNode.margin(top: imageSize / 3 + 30))
             .margin(bottom: 20)
        }
    }

    class MenuCellIcon: ASDisplayNode {
        var iconNode: ASDisplayNode?

        init(icon: MenuCell.MenuIcon) {

            switch icon {
            case .icon(let image, let tintColor):
                let iconNode = ASImageNode()
                iconNode.image = image
                iconNode.tintColor = tintColor
                self.iconNode = iconNode
            case .url(let url):
                iconNode = ConciergeGenericNodes.AvatarNode(url: url, imageSize: 44, border: 2)
            case .backgroundIcon(let image, let backgroundColor, let tintColor):
                iconNode = ConciergeGenericNodes.AvatarNode(image: image, imageSize: 36, border: 0, imageBackground: backgroundColor, imageTint: tintColor)
            default:
                ()
            }

            super.init()
            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            (iconNode ?? ASDisplayNode()).wrapped()
        }
    }

    class MenuCell: ASCellNode {
        enum MenuStyle {
            case `default`
            case resident

            var backgroundColor: UIColor {
                switch self {
                case .default, .resident:
                    return AVSColor.secondaryBackground.color
                }
            }

            var textColor: UIColor {
                switch self {
                case .default, .resident:
                    return AVSColor.primaryLabel.color
                }
            }

            var leftIconColor: UIColor {
                switch self {
                case .resident, .default:
                    return AVSColor.tertiaryLabel.color
                }
            }

            var cardVerticalPadding: CGFloat {
                switch self {
                case .resident:
                    return 15
                case .default:
                    return 20
                }
            }

            var bottomMargin: CGFloat {
                switch self {
                case .resident:
                    return 5
                case .default:
                    return 15
                }
            }

            var hasShadow: Bool {
                switch self {
                case .resident:
                    return false
                case .default:
                    return true
                }
            }
        }

        enum MenuIcon {
            case none
            case icon(UIImage?, UIColor?)
            case url(URL?)
            case backgroundIcon(_ icon: UIImage, _ backgroundColor: UIColor, _ tintColor: UIColor)
        }

        var titleNode: TitleSubtitleNode
        var leftIconNode: MenuCellIcon?
        var rightIndicatorNode: ASImageNode?
        var cardNode: CardNode
        var badgeNode: ConciergeGenericNodes.BadgeNode?
        var menuStyle: MenuStyle

        init(icon: MenuIcon, title: String, subtitle: String?, progress: CGFloat = 0, badge: String? = nil, disclosure: Bool = false, menuStyle: MenuStyle = .default) {
            cardNode = CardNode(progressPercent: progress, shadow: menuStyle.hasShadow, backgroundColor: menuStyle.backgroundColor)
            var titleAttributes = TitleSubtitleNode.titleAttributes
            titleAttributes[.foregroundColor] = menuStyle.textColor

            var subTitleAttributes = TitleSubtitleNode.subtitleAttributes
            subTitleAttributes[.foregroundColor] = menuStyle.textColor

            titleNode = TitleSubtitleNode(attributedTitle: NSAttributedString(string: title, attributes: titleAttributes),
                                          attributedSubtitle: NSAttributedString(string: subtitle, attributes: subTitleAttributes))

            self.menuStyle = menuStyle

            switch icon {
            case .none:
                ()
            default:
                leftIconNode = .init(icon: icon)
            }

            if disclosure {
                rightIndicatorNode = ASImageNode(image: Asset.Images.Concierge.icRight.image.withRenderingMode(.alwaysTemplate))
                rightIndicatorNode?.tintColor = AVSColor.primaryIcon.color
            }

            if let badge = badge, let badgeInt = Int(badge) {
                badgeNode = BadgeFactory.accentConciergeBadge(badgeInt)
            }

            super.init()
            automaticallyManagesSubnodes = true
        }

        // Deprecated
        convenience init(icon: UIImage? = nil, iconURL: URL? = nil, title: String, subtitle: String?, progress: CGFloat = 0, badge: String? = nil, disclosure: Bool = false, menuStyle: MenuStyle = .default) {
            let menuIcon: MenuIcon
            if icon != nil {
                menuIcon = .icon(icon, menuStyle.leftIconColor)
            } else if iconURL != nil {
                menuIcon = .url(iconURL)
            } else {
                menuIcon = .none
            }

            self.init(icon: menuIcon, title: title, subtitle: subtitle, progress: progress, badge: badge, disclosure: disclosure, menuStyle: menuStyle)
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            [
                leftIconNode,
                titleNode.filling(),
                [badgeNode, rightIndicatorNode].hStacked().spaced(15)
            ].hStacked()
             .spaced(20)
             .justified(.spaceBetween)
             .aligned(.center)
             .margin(horizontal: 20)
             .margin(vertical: menuStyle.cardVerticalPadding)
             .background(cardNode)
             .margin(left: 15, bottom: menuStyle.bottomMargin, right: 15)
        }
    }

    class ImagePager: ASDisplayNode, ASCollectionDelegate, ASCollectionDataSource {
        var thumbnailCollection: ASCollectionNode
        var images: [URL] = []
        var ratio: CGFloat

        private var indexOfCellBeforeDragging = 0

        init(images: [URL]?, ratio: CGFloat = 1) {
            self.images = images ?? []
            self.ratio = ratio

            let flowLayout = UICollectionViewFlowLayout()
            flowLayout.scrollDirection = .horizontal
            flowLayout.minimumInteritemSpacing = 0
            flowLayout.minimumLineSpacing = 0

            let collectionNode = ASCollectionNode(collectionViewLayout: flowLayout)
            collectionNode.backgroundColor = AVSColor.secondaryBackground.color
            collectionNode.alwaysBounceHorizontal = true
            collectionNode.showsHorizontalScrollIndicator = false

            collectionNode.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 20)
            thumbnailCollection = collectionNode

            super.init()

            thumbnailCollection.dataSource = self
            thumbnailCollection.delegate = self

            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            thumbnailCollection.ratio(ratio)
        }

        func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
            images.count
        }

        func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
            ASSizeRangeMake(
                    CGSize(width: collectionNode.bounds.size.width - 40, height: 0),
                    CGSize(width: collectionNode.bounds.size.width - 40, height: CGFloat.greatestFiniteMagnitude)
            )
        }

        func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
            let imageURL = images[indexPath.item]
            return {
                let imageNode = ASNetworkImageNode()
                imageNode.url = imageURL
                imageNode.contentMode = .scaleAspectFill
                imageNode.cornerRadius = 4
                imageNode.backgroundColor = AVSColor.tertiaryBackground.color

                let cell = ASCellNode()
                cell.automaticallyManagesSubnodes = true
                cell.layoutSpecBlock = { node, range in
                    imageNode.ratio(self.ratio).margin(left: 20)
                }
                return cell
            }
        }

        func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
            let pageWidth = scrollView.frame.width - 40
            let proportionalOffset = scrollView.contentOffset.x / pageWidth
            indexOfCellBeforeDragging = Int(round(proportionalOffset))
        }

        func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
            // Stop scrolling
            targetContentOffset.pointee = scrollView.contentOffset

            // Calculate conditions
            let pageWidth = scrollView.frame.width - 40
            let collectionViewItemCount = images.count
            let proportionalOffset = scrollView.contentOffset.x / pageWidth
            let indexOfMajorCell = Int(round(proportionalOffset))
            let swipeVelocityThreshold: CGFloat = 0.5
            let hasEnoughVelocityToSlideToTheNextCell = indexOfCellBeforeDragging + 1 < collectionViewItemCount && velocity.x > swipeVelocityThreshold
            let hasEnoughVelocityToSlideToThePreviousCell = indexOfCellBeforeDragging - 1 >= 0 && velocity.x < -swipeVelocityThreshold
            let majorCellIsTheCellBeforeDragging = indexOfMajorCell == indexOfCellBeforeDragging
            let didUseSwipeToSkipCell = majorCellIsTheCellBeforeDragging && (hasEnoughVelocityToSlideToTheNextCell || hasEnoughVelocityToSlideToThePreviousCell)

            if didUseSwipeToSkipCell {
                // Animate so that swipe is just continued
                let snapToIndex = indexOfCellBeforeDragging + (hasEnoughVelocityToSlideToTheNextCell ? 1 : -1)
                let toValue = pageWidth * CGFloat(snapToIndex)
                UIView.animate(
                        withDuration: 0.3,
                        delay: 0,
                        usingSpringWithDamping: 1,
                        initialSpringVelocity: velocity.x,
                        options: .allowUserInteraction,
                        animations: {
                            scrollView.contentOffset = CGPoint(x: toValue, y: 0)
                            scrollView.layoutIfNeeded()
                        },
                        completion: nil
                )
            } else {
                // Pop back (against velocity)
                let indexPath = IndexPath(row: indexOfMajorCell, section: 0)
                thumbnailCollection.scrollToItem(at: indexPath, at: .left, animated: true)
            }
        }
    }

    class PurchaseButton: ASControlNode {

        var labelNode: ASTextNode2

        var iconNode: ASImageNode?
        var leftLabelNode: ASTextNode2?

        var onTap: (() -> Void)? {
            didSet {
                addTarget(self, action: #selector(_tapped), forControlEvents: .touchUpInside)
            }
        }

        init(icon: UIImage?, label: String, leftLabel: String? = nil) {
            if let icon = icon {
                iconNode = .init(image: icon.withRenderingMode(.alwaysTemplate))
                iconNode?.tintColor = AVSColor.primaryIcon.color
            } else if let leftLabel = leftLabel {
                leftLabelNode = .init(with: leftLabel.attributed(.bodyTitleMedium, .primaryIcon))
            }

            labelNode = .init(with: label.attributed(.bodyTitleHeavy, .accentBackground))
            super.init()
            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            if iconNode != nil {
                return [iconNode, labelNode].hStacked().spaced(10)
            } else {
                return [leftLabelNode, labelNode].hStacked().spaced(10).justified(.spaceBetween)
            }
        }

        @objc private func _tapped() {
            onTap?()
        }
    }
}
