//
// Created by Andrei Stoicescu on 25/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI
import SwiftyUserDefaults
import PromiseKit
import Combine

class ConciergeTabBarViewController: UITabBarController, UINavigationControllerDelegate, PushNotificationHandler, PushNotificationActionable {
    var userRequestsDataSource = UserRequestsPagerDataSource()
    var ordersDataSource = OrderListPagerDataSource()

    private var subscriptions = Set<AnyCancellable>()

    enum ControllerType: Int, CaseIterable {
        case dashboard
        case requests
        case chat
        case cart
        case more

        var icon: UIImage {
            switch self {
            case .dashboard:
                return Asset.Images.Concierge.icDashboard.image
            case .chat:
                return Asset.Images.Concierge.icChatNav.image
            case .cart:
                return Asset.Images.Concierge.icCartNav.image
            case .requests:
                return Asset.Images.Concierge.icTasks.image
            case .more:
                return Asset.Images.Concierge.icMore.image
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        updateAppVersion()

        subscribeToShiftNotifications()

        viewControllers = ControllerType.allCases.map { type -> UIViewController in
            switch type {
            case .requests:
                let vc = ConciergeUserRequestListPagerViewContoller(dataSource: userRequestsDataSource)
                userRequestsDataSource.reloadPendingBadge()
                return createTabBarController(icon: type.icon, controller: vc, title: "Requests")
            case .cart:
                let vc = ConciergeOrderListPagerViewContoller(dataSource: ordersDataSource)
                ordersDataSource.reloadPendingBadge()
                return createTabBarController(icon: type.icon, controller: vc, title: "Orders")
            case .chat:
                let vc = ConciergeChat.ConciergeChatListViewController()
                return createTabBarController(icon: type.icon, controller: vc, title: "Chat")
            case .dashboard:
                return createTabBarController(icon: type.icon, controller: ConciergeDashboardViewController(), title: "Home")
            case .more:
                return createTabBarController(icon: type.icon, controller: ConciergeMoreViewController(), title: "More")
            }
        }

        selectedViewController = viewControllers?.first

        AVSPushNotifications.shared.registerForPush()

        handlePushMessage()

        ConciergeService().getCurrentShift().catch { error in
            AppState.switchToNoShiftViewController()
        }
    }

    func subscribeToShiftNotifications() {
        NotificationCenter.default
                .publisher(for: UIApplication.willEnterForegroundNotification)
                .receive(on: DispatchQueue.main)
                .sink { _ in
                    ConciergeService().getCurrentShift().catch { error in
                        AppState.switchToNoShiftViewController()
                    }
                }.store(in: &subscriptions)
    }

    func handlePushMessage() {
        if let pushMessage = AppState.pushMessage {
            AppState.pushMessage = nil
            processActionable(pushMessage)
        }
    }

    func updateAppVersion() {
        ResidentService().updateSettings(Settings(appVersion: AppState.appVersion)).cauterize()
    }

    func createTabBarController(icon: UIImage, controller: UIViewController, title: String) -> UINavigationController {
        let navController = AVSNavigationController(rootViewController: controller)
        navController.tabBarItem = UITabBarItem(title: title, image: icon, selectedImage: icon)
        return navController
    }

    @discardableResult
    func switchTo(controllerType: ControllerType) -> UIViewController? {
        self.selectedIndex = controllerType.rawValue
        return (selectedViewController as? UINavigationController)?.viewControllers.first
    }

    func controllerForType(_ controllerType: ControllerType) -> UIViewController? {
        guard let nav = viewControllers?[safe: controllerType.rawValue] as? UINavigationController else { return nil }
        return nav.viewControllers.first
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        guard let items = tabBar.items else { return }

        // This is done here because we don't get the correct tab bar frame in viewDidLoad
        let tabBarButtonSize = CGSize(width: tabBar.frame.width / CGFloat(items.count), height: tabBar.frame.height)
        if let image = UIImage.createSelectionIndicator(color: AVSColor.accentBackground.color, size: tabBarButtonSize, lineHeight: 2) {
            tabBar.selectionIndicatorImage = image
        }
    }

    func processNotification(_ message: PushMessage, completion: PushMessageCompletion) {
        completion(.noData)
    }

    func processActionable(_ message: PushMessage) {
        guard let category = message.clickAction else { return }

        switch category.type {
        case .channel:
            if let chatViewController = controllerForType(.chat) as? ConciergeChat.ConciergeChatListViewController {
                switchTo(controllerType: .chat)
                chatViewController.openConversationId(category.id)
            }
        case .request:
            if let requestsViewController = controllerForType(.requests) as? ConciergeUserRequestListPagerViewContoller {
                switchTo(controllerType: .requests)
                requestsViewController.openRequestId(category.id)
            }
        case .notification:
            ()
        }
    }

    #if DEBUG
    override func becomeFirstResponder() -> Bool {
        true
    }

    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?){
        if motion == .motionShake {
            NetworkActivityLogger.shared.toggleLogging()
        }
    }
    #endif
}

