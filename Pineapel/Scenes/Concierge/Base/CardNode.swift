//
// Created by Andrei Stoicescu on 25/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit

private class CardProgressNode: ASDisplayNode {
    var progressPercent: CGFloat

    var color = AVSColor.colorGreen.color {
        didSet {
            bar.backgroundColor = color
        }
    }

    var bar = ASDisplayNode()
    var spacer = ASDisplayNode()

    init(percent: CGFloat, backgroundColor: UIColor = .clear) {
        progressPercent = max(0, min(1, percent))
        bar.backgroundColor = color

        spacer.backgroundColor = backgroundColor

        super.init()
        self.backgroundColor = backgroundColor
        automaticallyManagesSubnodes = true
    }

    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        [
            bar.height(.points(3)).width(.fraction(progressPercent)),
            spacer.filling()
        ].hStacked().aligned(.stretch)
    }
}

class CardNode: ASDisplayNode {

    private var progressNode: CardProgressNode

    var backgroundNode = ASDisplayNode()

    init(progressPercent: CGFloat = 0, progressBackground: UIColor = .clear, rounded: Bool = true, shadow: Bool = false, backgroundColor: UIColor = AVSColor.secondaryBackground.color) {
        let progressNode = CardProgressNode(percent: progressPercent, backgroundColor: progressBackground)
        self.progressNode = progressNode

        backgroundNode.backgroundColor = backgroundColor
        backgroundNode.clipsToBounds = true
        backgroundNode.automaticallyManagesSubnodes = true

        if rounded {
            backgroundNode.cornerRadius = 4
        }

        backgroundNode.layoutSpecBlock = { node, range in
            [ASDisplayNode.spacer(), progressNode].vStacked()
        }

        super.init()
        automaticallyManagesSubnodes = true

        self.backgroundColor = .clear
        if shadow {
            shadowColor = UIColor.black.cgColor
            shadowRadius = 2
            shadowOffset = .zero
            shadowOpacity = 0.1
        }
    }

    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        backgroundNode.wrapped()
    }
}

extension ASLayoutElement {
    func cardWrapped(_ card: CardNode = CardNode(), padding: CGFloat = 20) -> ASLayoutSpec {
        self.margin(padding).background(card)
    }
}