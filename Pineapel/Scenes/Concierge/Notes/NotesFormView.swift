//
// Created by Andrei Stoicescu on 21.01.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//

import SwiftUI
import Alamofire
import PromiseKit
import SVProgressHUD

class NotesForm: ObservableObject {
    @Published var subject: String = ""
    @Published var body: String = ""
    var id: String?
    var residentId: String?

    var asParams: ConciergeNoteRequest {
        ConciergeNoteRequest(id: id, note: body, name: subject, residentId: residentId)
    }

    var isValid: Bool {
        !subject.isEmpty && !body.isEmpty
    }
}

struct NotesFormView: View {
    @ObservedObject var form = NotesForm()
    var note: ConciergeNote?
    var residentId: String? = nil
    var dismissBlock: (_ requiresReload: Bool) -> Void

    var body: some View {
        ScrollView(showsIndicators: false) {
            VStack(spacing: 20) {
                ConciergeForm.TextInput(label: "Subject", value: $form.subject)
                ConciergeForm.TextArea(label: "Note", value: $form.body)
            }.padding(.top, 15)
             .padding(.horizontal, 15)
        }.fullViewBackgroundColor()
         .onAppear {
             if let note = note {
                 form.body = note.note?.html?.string ?? ""
                 form.subject = note.name?.html?.string ?? ""
             }
             form.residentId = residentId
         }.navigationBarItems(
                 leading: Button(action: {
                     dismissBlock(false)
                 }) {
                     Image(uiImage: Asset.Images.Concierge.icClose.image.withRenderingMode(.alwaysTemplate))
                 },
                 trailing: Button(action: {
                     SVProgressHUD.show()
                     let promise: Promise<Void>

                     if let noteID = note?.noteID {
                         promise = ConciergeService().updateNote(params: form.asParams, noteID: noteID)
                     } else {
                         promise = ConciergeService().createNote(params: form.asParams)
                     }

                     promise.done {
                         SVProgressHUD.dismiss()
                         dismissBlock(true)
                     }.catch { error in
                         Log.thisError(error)
                         SVProgressHUD.showError(withStatus: error.localizedDescription)
                     }
                 }) {
                     Text("Save").navigationSaveButton()
                 }.disabled(!form.isValid)
         ).navigationBarTitle(Text("\(note == nil ? "Create" : "Update") Note"), displayMode: .inline)
    }
}