//
// Created by Andrei Stoicescu on 28/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import SwiftyUserDefaults
import PromiseKit
import SwiftUI
import Combine
import DifferenceKit

class NotesListViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

    lazy var emptyNode = EmptyNode(emptyNodeType: .notes, showAction: false)
    lazy var refreshControl = UIRefreshControl()
    var collectionNode: ASCollectionNode

    var dataSource: ObservablePaginatedDataSource<ConciergeNote>
    var notes = [ConciergeNote]()
    private var subscriptions = Set<AnyCancellable>()

    private var searchTerm: String = "" {
        didSet {
            if searchTerm != oldValue {
                dataSource.loadMoreContentIfNeeded()
            }
        }
    }

    let searchController = UISearchController(searchResultsController: nil)

    override init() {
        collectionNode = .avsCollection()
        dataSource = .init()

        super.init(node: collectionNode)

        dataSource.dynamicCacheable = { [weak self] in
            String.isBlank(self?.searchTerm)
        }

        dataSource.cacheKey = "concierge_notes"

        dataSource.paginate = { [weak self] per, page in
            guard let term = self?.searchTerm else { return .value([]) }

            if term.count > 0 {
                return ConciergeService().searchNotes(per: per, page: page, term: term)
            } else {
                return ConciergeService().getNotes(page: page, per: per)
            }
        }

        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.searchController = searchController
        searchController.obscuresBackgroundDuringPresentation = false

        searchController.searchBar.searchTextField.searchPublisher().sink { [weak self] term in
            self?.searchTerm = term
        }.store(in: &subscriptions)

        setupBindings()
        navigationItem.title = "My Notes"
        replaceSystemBackButton()
        showEmptyScreen(false)
        collectionNode.view.backgroundView = emptyNode.view
        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)
        collectionNode.view.addSubview(refreshControl)
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: Asset.Images.Concierge.icCompose.image.withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(tappedCreateButton))
        refreshItems()
    }

    func setupBindings() {
        dataSource.$items
                .receive(on: DispatchQueue.main)
                .sink { [weak self] items in
                    self?.reloadData()
                }.store(in: &subscriptions)

        dataSource.$hasNoItemsAfterLoading
                .receive(on: DispatchQueue.main)
                .sink { [weak self] hasNoItemsAfterLoading in
                    self?.showEmptyScreen(hasNoItemsAfterLoading)
                }.store(in: &subscriptions)

        dataSource.$showLoadingIndicator
                .receive(on: DispatchQueue.main)
                .sink { [weak self] loading in
                    guard let self = self else { return }

                    if loading {
                        if !self.refreshControl.isRefreshing {
                            SVProgressHUD.show()
                        }
                    } else {
                        SVProgressHUD.dismiss()
                        self.refreshControl.endRefreshing()
                    }
                }.store(in: &subscriptions)
    }

    func reloadData() {
        let source = notes
        let target = dataSource.items

        let stagedChangeSet = StagedChangeset(source: source, target: target)
        for changeSet in stagedChangeSet {
            notes = changeSet.data
            collectionNode.performDiffBatchUpdates(changeSet: changeSet)
        }
    }

    @objc func refreshItems() {
        dataSource.loadMoreContentIfNeeded()
    }

    @objc func tappedCreateButton() {
        startEditing(note: nil)
    }

    func showEmptyScreen(_ show: Bool) {
        emptyNode.isHidden = !show
    }

    func collectionView(_ collectionView: ASCollectionView, willDisplayNodeForItemAt indexPath: IndexPath) {
        dataSource.loadMoreContentIfNeeded(currentItem: dataSource.items[indexPath.item])
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        notes.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let note = notes[indexPath.item]
        return {
            let cell = NoteNodes.MenuCell(title: note.name ?? "", subtitle: note.created)

            cell.didSwipe = { [weak self, weak cell] swiped in
                guard let cell = cell else { return }
                if swiped {
                    self?.swipeCloseAll(except: cell)
                }
            }

            cell.didTapAction = { [weak self] action in
                guard let self = self else { return }
                switch action {
                case .edit:
                    self.swipeCloseAll()
                    self.startEditing(note: note)
                case .delete:
                    self.swipeCloseAll()
                    guard let index = self.dataSource.removeItem(note) else { return }
                    self.reloadData()

                    ConciergeService().deleteNote(noteID: note.noteID).catch { error in
                        Log.thisError(error)
                        DispatchQueue.main.async { [weak self] in
                            guard let self = self else { return }
                            SVProgressHUD.showError(withStatus: "There was an error while deleting your note")
                            self.dataSource.insertItem(note, at: index)
                            self.reloadData()
                        }
                    }
                }
            }
            return cell
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        swipeCloseAll()
        startEditing(note: notes[indexPath.item])
    }

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        swipeCloseAll()
    }

    func swipeCloseAll(except exceptNode: NoteNodes.MenuCell? = nil) {
        for node in collectionNode.visibleNodes {
            guard let node = node as? NoteNodes.MenuCell else { continue }
            if node == exceptNode { continue }
            if node.swiped { node.swiped = false }
        }
    }

    func startEditing(note: ConciergeNote?) {
        let view = NotesFormView(note: note, dismissBlock: { [weak self] requiresReload in
            guard let self = self else { return }
            self.navigationController?.dismiss(animated: true)
            if requiresReload {
                self.refreshItems()
            }
        })

        let nav = AVSNavigationController(rootViewController: UIHostingController(rootView: view))
        nav.isModalInPresentation = true
        navigationController?.present(nav, animated: true)
    }
}