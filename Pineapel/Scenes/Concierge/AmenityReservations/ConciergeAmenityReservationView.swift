//
// Created by Andrei Stoicescu on 22/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import SwiftUI
import Alamofire
import PromiseKit
import SVProgressHUD

extension ConciergeAmenityReservationView {
    class DataProvider: ObservableObject {
        @Published var users: [User] = []
        @Published var amenities: [Amenity] = []

        @Published var isLoading = false
        @Published var error: Error? = nil

        func loadData() -> Promise<Void> {
            guard !isLoading else { return .value(()) }

            isLoading = true

            return when(fulfilled:
                        ConciergeService().getAllUsers(),
                        ResidentService().getAllAmenities()
            ).map { users, amenities in
                self.users = users
                self.amenities = amenities
            }.ensure {
                self.isLoading = false
            }
        }
    }
}

struct ConciergeAmenityReservation: Encodable {
    let amenityID: String
    let note: String?
    let startTime: Date?
    let endTime: Date?
    let date: Date?

    // Daily reservations:
    let startDate: Date?
    let endDate: Date?

    let userID: String?
    let status: AmenityReservation.Status
    let reservationID: String?
    let guestName: String?
    let guestPhone: String?

    enum CodingKeys: String, CodingKey {
        case amenityID = "field_amenity"
        case note = "field_note"
        case startTime = "field_start_time"
        case endTime = "field_end_time"
        case startDate = "field_start_date"
        case endDate = "field_end_date"
        case date = "field_date"
        case reservationID = "id"
        case status = "field_status"
        case userID = "user_id"
        case guestName = "field_guest_name"
        case guestPhone = "field_guest_phone_number"
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encodeValueCodableItemArrayIfPresent(reservationID, forKey: .reservationID)
        try container.encodeSingleItemArray(amenityID, paramName: "target_id", forKey: .amenityID)
        try container.encodeSingleItemArray(userID, paramName: "target_id", forKey: .userID)
        try? container.encodeValueCodableItemArrayIfPresent(reservationHourKey(date: startTime), forKey: .startTime)
        try? container.encodeValueCodableItemArrayIfPresent(reservationHourKey(date: endTime), forKey: .endTime)
        try? container.encodeValueCodableItemArrayIfPresent(reservationDateFormatted(date: date), forKey: .date)
        try? container.encodeValueCodableItemArrayIfPresent(reservationDateFormatted(date: startDate), forKey: .startDate)
        try? container.encodeValueCodableItemArrayIfPresent(reservationDateFormatted(date: endDate), forKey: .endDate)
        try container.encodeValueCodableItemArray(status.rawValue, forKey: .status)
        try? container.encodeValueCodableItemArrayIfPresent(guestName, forKey: .guestName)
        try? container.encodeValueCodableItemArrayIfPresent(guestPhone, forKey: .guestPhone)
        try? container.encodeValueCodableItemArrayIfPresent(note, forKey: .note)
    }

    func reservationHourKey(date: Date?) -> String? {
        guard let date = date else { return nil }
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minute = calendar.component(.minute, from: date)
        return String(hour * 60 + minute)
    }

    func reservationDateFormatted(date: Date?) -> String? {
        date?.format(with: "yyyy-MM-dd")
    }
}

class ConciergeAmenityReservationForm: ObservableObject {
    @Published var resident: [ConciergeForm.Option<User>] = []
    @Published var residentOptions: [ConciergeForm.Option<User>] = []

    @Published var amenity: [ConciergeForm.Option<Amenity>] = [] {
        didSet {
            reloadAvailability()
        }
    }
    @Published var amenityOptions: [ConciergeForm.Option<Amenity>] = []

    @Published var note: String = ""

    @Published var date: Date? {
        didSet {
            reloadAvailability()
        }
    }

    @Published var guestName: String = ""
    @Published var guestPhone: String = ""

    // For daily amenities only:
    @Published var startDate: Date?
    @Published var endDate: Date?

    @Published var disabledDates = [Date]()

    var dailyRangeString: String {
        [startDate?.avsDateFormatted, endDate?.avsDateFormatted].compactMap { $0 }.joined(separator: " - ")
    }

    var availableSlotsCount: Int = 0

    func reloadAvailability() {
        selectedSlots = []
        availableSlots = []
        disabledDates = []
        availableSlotsCount = 0

        guard let amenity = amenity.first?.underlyingValue else { return }

        if amenity.priceType == .day {
            ResidentService().getAmenityDaysAvailability(amenityID: amenity.amenityID, startDate: Date(), endDate: AmenityReservation.availabilityCheckEndDate).done { days in
                self.disabledDates = days.notAvailableDates
            }.cauterize()
        } else {
            guard let date = date else { return }

            ResidentService().getAmenityAvailability(amenityID: amenity.amenityID, date: date).done { hours in
                self.availableSlotsCount = hours.availableSlots
                self.availableSlots = hours.availableTimeSlots(date: date).map { slot in
                    ConciergeForm.Option<AvailableHours.TimeSlot>(id: UUID().uuidString,
                                                                  label: "\(slot.startDate.avsTimeString()) - \(slot.endDate.avsTimeString()) (\(slot.maxSeats == 0 ? "Full" : "\(slot.maxSeats) seats"))",
                                                                  underlyingValue: slot)
                }
            }.cauterize()
        }
    }

    @Published var selectedSlots: [ConciergeForm.Option<AvailableHours.TimeSlot>] = []
    @Published var availableSlots: [ConciergeForm.Option<AvailableHours.TimeSlot>] = []

    var hasAvailableSlots: Bool {
        availableSlots.count > 0 && availableSlots.first { $0.underlyingValue.maxSeats > 0 } != nil
    }

    var asParams: ConciergeAmenityReservation? {
        guard let amenity = amenity.first?.underlyingValue, let resident = resident.first else { return nil }

        // TODO: convert to building timezone?

        if amenity.priceType == .day {
            return ConciergeAmenityReservation(amenityID: amenity.amenityID,
                                               note: note,
                                               startTime: nil,
                                               endTime: nil,
                                               date: nil,
                                               startDate: startDate,
                                               endDate: endDate,
                                               userID: resident.underlyingValue.uid,
                                               status: .accepted,
                                               reservationID: nil,
                                               guestName: guestName,
                                               guestPhone: guestPhone)
        } else {
            guard let date = date,
                  let startAt = selectedSlots.min(by: { $0.underlyingValue.startDate < $1.underlyingValue.startDate }),
                  let endAt = selectedSlots.max(by: { $0.underlyingValue.endDate < $1.underlyingValue.endDate })
                    else { return nil }

            return ConciergeAmenityReservation(amenityID: amenity.amenityID,
                                               note: note,
                                               startTime: startAt.underlyingValue.startDate,
                                               endTime: endAt.underlyingValue.endDate,
                                               date: date,
                                               startDate: nil,
                                               endDate: nil,
                                               userID: resident.underlyingValue.uid,
                                               status: .accepted,
                                               reservationID: nil,
                                               guestName: nil,
                                               guestPhone: nil)
        }
    }

    var isValid: Bool {
        guard resident.first != nil, let amenity = amenity.first?.underlyingValue else { return false }

        if amenity.priceType == .day {
            guard dailyDateRangeValid, guestPhone.count > 0, guestName.count > 0 else { return false }
        } else {
            guard date != nil, selectedSlots.count > 0 else { return false }
        }

        return true
    }

    var dailyDateRangeValid: Bool {
        let minimumDays = max(amenity.first?.underlyingValue.slotDuration ?? 1, 1) // default to minimum 1 night, reject server response with less than 1
        guard let startDate = startDate,
              let endDate = endDate,
              endDate.daysLater(than: startDate) >= minimumDays else {
            return false
        }
        return true
    }

    func canSelectSlotOption(_ option: ConciergeForm.Option<AvailableHours.TimeSlot>) -> Bool {
        // deny selecting if slot doesn't even have seats available
        if option.underlyingValue.maxSeats == 0 {
            return false
        }

        // deny selecting if there aren't any available slots remaining
        if availableSlotsCount <= selectedSlots.count {
            return false
        }

        // allow selecting the first slot
        if selectedSlots.count == 0 {
            return true
        }

        // allow selecting only adjacent slots
        if let idx = self.availableSlots.firstIndex(of: option) {
            if let prev = self.availableSlots[safe: idx - 1],
               selectedSlots.contains(prev) {
                return true
            }

            if let next = self.availableSlots[safe: idx + 1],
               selectedSlots.contains(next) {
                return true
            }
        }

        return false
    }

    func canDeselectSlotOption(_ option: ConciergeForm.Option<AvailableHours.TimeSlot>) -> Bool {
        // allow deselecting only slots that are not in-between other selected slots
        if let idx = availableSlots.firstIndex(of: option) {
            if let prev = availableSlots[safe: idx - 1],
               let next = availableSlots[safe: idx + 1] {
                return !(selectedSlots.contains(prev) && selectedSlots.contains(next))
            }
        }

        return true
    }
}

struct ConciergeAmenityReservationView: View {
    @ObservedObject var form = ConciergeAmenityReservationForm()
    @ObservedObject var dataProvider = DataProvider()
    @State var showCalendar: Bool = false
    @State var showRangeCalendar: Bool = false

    var dismissBlock: ((_ requiresRefresh: Bool) -> Void)?

    var body: some View {
        ScrollView(showsIndicators: false) {
            VStack(spacing: 20) {

                ConciergeForm.SelectInput<User>(
                        label: "Resident",
                        placeholder: "Select Resident",
                        options: form.residentOptions,
                        value: $form.resident)

                ConciergeForm.TextArea(label: "Resident Note", value: $form.note)

                ConciergeForm.SelectInput<Amenity>(
                        label: "Amenity",
                        placeholder: "Select Amenity",
                        options: form.amenityOptions,
                        value: $form.amenity)

                if let amenity = form.amenity.first {
                    if amenity.underlyingValue.priceType == .day {
                        VStack {
                            ConciergeForm.ReadonlyTextInput(label: "PERIOD", value: form.dailyRangeString)
                                         .onTapGesture { showRangeCalendar = true }
                                         .sheet(isPresented: $showRangeCalendar) {
                                             NavigationView {
                                                 CalendarWrapperView(
                                                         isShown: $showCalendar,
                                                         startDate: $form.startDate,
                                                         endDate: $form.endDate,
                                                         maximumDate: AmenityReservation.availabilityCheckEndDate,
                                                         minimumDate: Date(),
                                                         title: "Select Date",
                                                         selectionMode: .range,
                                                         disabledDates: form.disabledDates
                                                 ).navigationBarItems(leading:
                                                                      Button(action: {
                                                                          showRangeCalendar = false
                                                                          if form.startDate == nil || form.endDate == nil {
                                                                              form.startDate = nil
                                                                              form.endDate = nil
                                                                          }
                                                                      }) {
                                                                          Text("Cancel").navigationSaveButton()
                                                                      }, trailing:
                                                                      Button(action: {
                                                                          let minimumDays = max(amenity.underlyingValue.slotDuration ?? 1, 1) // default to minimum 1 night, reject server response with less than 1
                                                                          if !form.dailyDateRangeValid {
                                                                              SVProgressHUD.showError(withStatus: "You must select a minimum of \(minimumDays) nights")
                                                                              return
                                                                          }

                                                                          showRangeCalendar = false
                                                                      }) {
                                                                          Text("Done").navigationSaveButton()
                                                                      })
                                                  .navigationBarTitle(Text("Select Date"), displayMode: .inline)
                                             }
                                         }

                            ConciergeForm.TextInput(label: "GUEST NAME", value: $form.guestName)
                            ConciergeForm.TextInput(label: "GUEST PHONE", value: $form.guestPhone)
                        }

                    } else {
                        ConciergeForm.ExternalSelectInput<Date?>(
                                             label: "Date",
                                             placeholder: "Select Date",
                                             value: $form.date,
                                             stringValueTransformer: { date in date?.avsDateFormatted ?? "" },
                                             rightIcon: Asset.Images.Concierge.icCalendar.image)
                                     .onTapGesture { showCalendar = true }
                                     .sheet(isPresented: $showCalendar) {
                                         NavigationView {
                                             CalendarWrapperView(
                                                     isShown: $showCalendar,
                                                     startDate: $form.date,
                                                     endDate: .constant(nil),
                                                     maximumDate: 6.months.later,
                                                     minimumDate: 3.days.earlier,
                                                     title: "Select Date"
                                             ).navigationBarItems(leading:
                                                                  Button(action: {
                                                                      showCalendar = false
                                                                  }) {
                                                                      Text("Cancel").navigationSaveButton()
                                                                  })
                                              .navigationBarTitle(Text("Select Date"), displayMode: .inline)
                                         }
                                     }

                        if let date = form.date {
                            if form.hasAvailableSlots {
                                ConciergeForm.SelectInput<AvailableHours.TimeSlot>(
                                        label: "Time Slots",
                                        placeholder: "Select Time Slots",
                                        options: form.availableSlots,
                                        value: $form.selectedSlots,
                                        multipleSelect: true,
                                        canSelectItem: form.canSelectSlotOption,
                                        canDeselectItem: form.canDeselectSlotOption,
                                        stringValueTransformer: { options in
                                            guard let startAt = options.min(by: { $0.underlyingValue.startDate < $1.underlyingValue.startDate }),
                                                  let endAt = options.max(by: { $0.underlyingValue.endDate < $1.underlyingValue.endDate }) else {
                                                return ""
                                            }

                                            return "\(startAt.underlyingValue.startDate.avsTimeString()) - \(endAt.underlyingValue.endDate.avsTimeString())"
                                        })
                            } else {
                                ConciergeForm.ReadonlyTextInput(label: "Time Slots", value: "No time slots available on \(date.avsDateString())")
                            }
                        }
                    }
                }

            }.padding(.top, 20)
             .padding(.horizontal, 20)
        }.navigationBarItems(
                 leading: Button(action: {
                     dismissBlock?(false)
                 }) {
                     Image(uiImage: Asset.Images.Concierge.icClose.image.withRenderingMode(.alwaysTemplate)).foregroundColor(.primaryIcon)
                 },
                 trailing: Button(action: {
                     guard let params = form.asParams else { return }
                     SVProgressHUD.show()

                     ConciergeService().createAmenityReservation(params).done {
                         SVProgressHUD.dismiss()
                         dismissBlock?(true)
                     }.catch { error in
                         Log.thisError(error)
                         SVProgressHUD.showError(withStatus: error.localizedDescription)
                     }
                 }) {
                     Text("Save").navigationSaveButton()
                 }.disabled(!form.isValid))
         .fullViewBackgroundColor()
         .navigationBarTitle(Text("New Reservation"), displayMode: .inline)
         .onAppear {
             dataProvider.loadData().done {

                 form.amenityOptions = dataProvider.amenities.map { amenity in
                     .init(id: amenity.id, label: amenity.name ?? "", underlyingValue: amenity)
                 }

                 form.residentOptions = dataProvider.users.map { user in
                     .init(id: user.uid, label: user.fullName, underlyingValue: user)
                 }

             }.catch { error in
                 Log.thisError(error)
             }
         }
    }
}