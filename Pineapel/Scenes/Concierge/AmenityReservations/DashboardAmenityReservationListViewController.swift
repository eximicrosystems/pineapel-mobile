//
// Created by Andrei Stoicescu on 31/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import SwiftyUserDefaults
import PromiseKit
import Combine
import DifferenceKit

class DashboardAmenityReservationListViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

    lazy var emptyNode = EmptyNode(emptyNodeType: .conciergeAmenityReservations, showAction: false)
    lazy var refreshControl = UIRefreshControl()
    var collectionNode: ASCollectionNode
    var dataSource: ObservablePaginatedDataSource<AmenityReservation>
    var reservations = [AmenityReservation]()
    private var subscriptions = Set<AnyCancellable>()

    override init() {
        collectionNode = .avsCollection()
        dataSource = .init()
        dataSource.paginate = { per, page in
            ConciergeService().getAmenityReservationList(
                    page: page,
                    per: per,
                    startDate: Date().convertedToBuildingTimeZone(.startOfDay),
                    endDate: Date().convertedToBuildingTimeZone(.endOfDay)
            )
        }
        dataSource.cacheKey = "concierge_dashboard_amenity_reservations_\(Date().convertedToBuildingTimeZone(.startOfDay).avsDateFormatted)"
        dataSource.cacheable = true

        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Amenity Reservations"
        replaceSystemBackButton()

        setupBindings()

        collectionNode.view.backgroundView = emptyNode.view
        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)
        collectionNode.view.addSubview(refreshControl)

        showEmptyScreen(false)
        collectionNode.view.backgroundView = emptyNode.view
        refreshItems()
    }

    func setupBindings() {
        dataSource.$items
                .receive(on: DispatchQueue.main)
                .sink { [weak self] items in
                    self?.reloadData()
                }.store(in: &subscriptions)

        dataSource.$hasNoItemsAfterLoading
                .receive(on: DispatchQueue.main)
                .sink { [weak self] hasNoItemsAfterLoading in
                    self?.showEmptyScreen(hasNoItemsAfterLoading)
                }.store(in: &subscriptions)

        dataSource.$showLoadingIndicator
                .receive(on: DispatchQueue.main)
                .sink { [weak self] loading in
                    guard let self = self else { return }

                    if loading {
                        if !self.refreshControl.isRefreshing {
                            SVProgressHUD.show()
                        }
                    } else {
                        SVProgressHUD.dismiss()
                        self.refreshControl.endRefreshing()
                    }
                }.store(in: &subscriptions)
    }

    func reloadData() {
        let source = reservations
        let target = dataSource.items

        let stagedChangeSet = StagedChangeset(source: source, target: target)
        for changeSet in stagedChangeSet {
            reservations = changeSet.data
            collectionNode.performDiffBatchUpdates(changeSet: changeSet)
        }
    }

    @objc func refreshItems() {
        dataSource.loadMoreContentIfNeeded()
    }

    func showEmptyScreen(_ show: Bool) {
        emptyNode.isHidden = !show
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        reservations.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let item = reservations[indexPath.item]
        return {
            DashboardAmenityReservationListNodes.ReservationCell(reservation: item)
        }
    }

    func collectionView(_ collectionView: ASCollectionView, willDisplayNodeForItemAt indexPath: IndexPath) {
        dataSource.loadMoreContentIfNeeded(currentItem: dataSource.items[indexPath.item])
    }

    func buildReservationActionsAlert(reservation: AmenityReservation) -> UIAlertController? {
        guard let status = reservation.status,
              status != .rejected,
              let reservationID = reservation.reservationID else { return nil }

        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        func addAction(_ title: String, style: UIAlertAction.Style, status: AmenityReservation.Status) {
            alertController.addAction(UIAlertAction(title: title, style: style) { [weak self] _ in
                ConciergeService().updateAmenityReservationStatus(status: status, reservationID: reservationID).done {
                    self?.refreshItems()
                }.catch { error in
                    Log.thisError(error)
                    SVProgressHUD.showError(withStatus: error.localizedDescription)
                }
            })
        }

        switch status {
        case .pending:
            addAction("Approve", style: .default, status: .accepted)
            addAction("Decline", style: .destructive, status: .rejected)
        case .accepted:
            addAction("Decline", style: .destructive, status: .rejected)
        case .rejected:
            ()
        }

        alertController.addAction(UIAlertAction(title: L10n.General.cancel, style: .cancel, handler: nil))

        return alertController
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        let reservation = reservations[indexPath.item]
        guard let alertController = buildReservationActionsAlert(reservation: reservation) else { return }
        alertController.fixIpadIfNeeded(view: view)
        present(alertController, animated: true)
    }
}