//
// Created by Andrei Stoicescu on 25/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import SwiftyUserDefaults
import PromiseKit
import SwiftUI
import Combine

class ConciergeDashboardViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

    enum Items: Int, CaseIterable {
        case header
        case requestsOrders
        case messages
        case runOfShow
        case notes
        case amenityReservation
    }

    var collectionNode: ASCollectionNode
    var provider = ConciergeDashboardProvider()
    lazy var refreshControl = UIRefreshControl()
    var willPushViewController: Bool = false

    private var subscriptions = Set<AnyCancellable>()

    override init() {
        collectionNode = ASCollectionNode.avsCollection()
        collectionNode.contentInset = .collectionBottomInset
        collectionNode.backgroundColor = .clear

        let navigationNode = ConciergeGenericNodes.LargeNavigationNode(title: "Dashboard", rightButtonType: .send)

        let mainNode = ASDisplayNode()
        mainNode.backgroundColor = AVSColor.primaryBackground.color
        mainNode.automaticallyManagesSubnodes = true

        super.init(node: mainNode)

        mainNode.layoutSpecBlock = { node, range in
            self.collectionNode.style.flexGrow = 1
            return [navigationNode, self.collectionNode].vStacked()
        }

        collectionNode.delegate = self
        collectionNode.dataSource = self

        navigationNode.didTapRightButton = { [weak self] in
            guard let self = self else { return }
            let view = SendNotificationsView(form: .init(), dismissBlock: { [weak self] in
                self?.navigationController?.dismiss(animated: true)
            })

            let nav = AVSNavigationController(rootViewController: UIHostingController(rootView: view))
            nav.isModalInPresentation = true
            self.navigationController?.present(nav, animated: true)
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()
        collectionNode.view.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)

        ConciergeChat.MessagingManager.shared.channelListManager.$totalUnreadCount
                .receive(on: DispatchQueue.main)
                .sink { [weak self] count in
                    self?.collectionNode.reloadData()
                }.store(in: &subscriptions)

        if !provider.loadCached() {
            SVProgressHUD.show()
        } else {
            refreshControl.beginRefreshing()
        }

        refreshItems()
    }

//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        navigationController?.setNavigationBarHidden(true, animated: false)
//    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        navigationController?.setNavigationBarHidden(false, animated: true)
//    }

    @objc func refreshItems() {
        provider.reloadData().done {
            self.collectionNode.reloadData()
        }.ensure {
            SVProgressHUD.dismiss()
            self.refreshControl.endRefreshing()
        }.catch { error in
            Log.thisError(error)
        }
    }

    func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
        1
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        provider.isLoading && !provider.didLoadCached ? 0 : Items.allCases.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        switch indexPath.item {
        case Items.header.rawValue:
            return {
                ConciergeDashboardNodes.HeaderCell(userName: self.provider.user?.firstName,
                                                   buildingName: self.provider.conciergeStats?.buildingName,
                                                   userCount: self.provider.conciergeStats?.residents ?? 0)
            }
        case Items.requestsOrders.rawValue:
            return {
                let cell = ConciergeDashboardNodes.LineCardsCell(cards: [
                    .val(.newRequests, self.provider.requestsCount.count),
                    .val(.newReservations, self.provider.reservationsCount.count),
                ])

                cell.didSelectCard = { [weak self] card in
                    switch card.type {
                    case .newRequests:
                        let tabBarController = self?.tabBarController as? ConciergeTabBarViewController
                        tabBarController?.switchTo(controllerType: .requests)
                    case .newReservations:
                        let tabBarController = self?.tabBarController as? ConciergeTabBarViewController
                        let moreVC = tabBarController?.switchTo(controllerType: .more) as? ConciergeMoreViewController

                        // TODO: refactor this. is done for when the more vc is not loaded (has some issues with hiding the navbar)
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .milliseconds(100)) {
                            moreVC?.externalOpenAmenityReservations()
                        }
                    case .newMessages:
                        ()
                    }
                }

                return cell
            }
        case Items.messages.rawValue:
            let count = ConciergeChat.MessagingManager.shared.channelListManager.totalUnreadCount
            return { ConciergeDashboardNodes.LineCardsCell(cards: [.val(.newMessages, Int(count))]) }
        case Items.runOfShow.rawValue:
            return {
                ConciergeGenericNodes.MenuCell(title: "Run of Show",
                                               subtitle: "\(self.provider.runOfShow.completedItems.count) of \(self.provider.runOfShow.items.count) tasks completed today",
                                               progress: CGFloat(self.provider.runOfShow.percent),
                                               disclosure: true)
            }
        case Items.notes.rawValue:
            return {
                ConciergeGenericNodes.MenuCell(title: "My Notes",
                                               subtitle: "Check your personal daily notes",
                                               disclosure: true)
            }
        case Items.amenityReservation.rawValue:
            return {
                ConciergeGenericNodes.MenuCell(title: "Amenity Reservation",
                                               subtitle: "Check amenity reservations from your guests",
                                               disclosure: true)
            }
        default:
            return { ASCellNode() }
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        guard let item = Items(rawValue: indexPath.item) else { return }

        let tabBarController = self.tabBarController as? ConciergeTabBarViewController

        switch item {
        case .notes:
            willPushViewController = true
            navigationController?.pushViewController(NotesListViewController(), animated: true)
        case .runOfShow:
            willPushViewController = true
            let vc = RunOfShowViewController()
            vc.delegate = self
            navigationController?.pushViewController(vc, animated: true)
        case .amenityReservation:
            willPushViewController = true
            navigationController?.pushViewController(DashboardAmenityReservationListViewController(), animated: true)
        case .messages:
            tabBarController?.switchTo(controllerType: .chat)
        default:
            ()
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        ASSizeRange.fullWidth(collectionNode: collectionNode)
    }
}

extension ConciergeDashboardViewController: RunOfShowViewControllerProtocol {
    func didUpdateRunOfShow(_ runOfShow: RunOfShow) {
        self.provider.runOfShow = runOfShow
        self.collectionNode.reloadItems(at: [IndexPath(item: Items.runOfShow.rawValue, section: 0)])
    }

}
