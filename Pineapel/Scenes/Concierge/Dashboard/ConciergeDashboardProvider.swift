//
// Created by Andrei Stoicescu on 26/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import PromiseKit
import SwiftyUserDefaults

extension CodableCacheKey {
    enum ConciergeDashboard: String {
        case user, reservations, stats, requests, runOfShow
    }

    static func conciergeDashboard(_ type: ConciergeDashboard) -> CodableCacheKey {
        CodableCacheKey(key: type.rawValue)
    }
}

class ConciergeDashboardProvider {

    var user: User? { didSet { user?.cache(.conciergeDashboard(.user)) } }
    var reservationsCount: CountResponse = CountResponse(count: 0) { didSet { reservationsCount.cache(.conciergeDashboard(.reservations)) } }
    var requestsCount: CountResponse = CountResponse(count: 0) { didSet { requestsCount.cache(.conciergeDashboard(.requests)) } }
    var runOfShow = RunOfShow(items: []) { didSet { runOfShow.items.cache(.conciergeDashboard(.runOfShow)) } }
    var conciergeStats: ConciergeStats? { didSet { conciergeStats?.cache(.conciergeDashboard(.stats)) } }

    let api = ResidentService()
    let conciergeApi = ConciergeService()
    var isLoading = false
    var didLoadCached = false

    func loadCached() -> Bool {
        guard let user = User.readCached(.conciergeDashboard(.user)),
              let reservationsCount = CountResponse.readCached(.conciergeDashboard(.reservations)),
              let conciergeStats = ConciergeStats.readCached(.conciergeDashboard(.stats)),
              let requestsCount = CountResponse.readCached(.conciergeDashboard(.requests)),
              let items = [RunOfShowItem].readCached(.conciergeDashboard(.runOfShow)) else {
            return false
        }

        self.user = user
        self.reservationsCount = reservationsCount
        self.conciergeStats = conciergeStats
        self.requestsCount = requestsCount
        runOfShow = RunOfShow(items: items)

        didLoadCached = true
        return didLoadCached
    }

    func reloadData() -> Promise<Void> {
        isLoading = true

        return when(resolved: [
            api.getUser().map { self.user = $0 }.asVoid(),
            conciergeApi.getConciergeStats().map { self.conciergeStats = $0 }.asVoid(),
            conciergeApi.getReservationsCount().map { self.reservationsCount = $0 }.asVoid(),
            conciergeApi.getRequestsCount().map { self.requestsCount = $0 }.asVoid(),
            conciergeApi.getRunOfShow().map { self.runOfShow = .init(items: $0) }.asVoid(),
        ]).map { results in
            self.isLoading = false
        }
    }
}