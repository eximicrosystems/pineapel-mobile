//
// Created by Andrei Stoicescu on 26/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit

struct ConciergeDashboardNodes {
    struct CardValue {
        let type: CardType
        let value: Int

        static func val(_ type: CardType, _ value: Int) -> CardValue {
            CardValue(type: type, value: value)
        }
    }

    enum CardType {
        case newRequests
        case newReservations
        case newMessages

        var lineColor: UIColor {
            switch self {
            case .newRequests:
                return AVSColor.colorBlue.color
            case .newReservations:
                return AVSColor.colorYellow.color
            case .newMessages:
                return AVSColor.colorPurple.color
            }
        }

        var title: String {
            switch self {
            case .newRequests:
                return "NEW REQUESTS"
            case .newReservations:
                return "NEW RESERVATIONS"
            case .newMessages:
                return "NEW MESSAGES"
            }
        }

        var icon: UIImage {
            switch self {
            case .newRequests:
                return Asset.Images.Concierge.icClipboard.image
            case .newReservations:
                return Asset.Images.Concierge.icCart.image
            case .newMessages:
                return Asset.Images.Concierge.icGroupChat.image
            }
        }
    }

    class InfoCard: ASCellNode {
        var titleNode = ASTextNode2()
        var valueNode = ASTextNode()

        init(title: String, value: String) {
            super.init()
            titleNode.attributedText = title.attributed(.note, .primaryLabel)
            valueNode.attributedText = "\(value)".attributed(.H1Large, .primaryLabel)
            valueNode.pointSizeScaleFactors = [0.9, 0.7, 0.5]
            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            [
                titleNode,
                valueNode.margin(bottom: -20)
            ].vStacked().justified(.spaceBetween).spaced(30).cardWrapped().margin(horizontal: 10).margin(bottom: 10)
        }
    }

    class DashboardInfoCard: ASControlNode {

        var lineNode = ASDisplayNode()
        var titleNode = ASTextNode2()
        var valueNode = ASTextNode2()
        var iconNode = ASImageNode()
        var onTap: (() -> Void)?
        var icon: UIImage

        init(title: String, lineColor: UIColor, icon: UIImage, value: Int) {
            self.icon = icon
            super.init()

            lineNode.backgroundColor = lineColor

            titleNode.attributedText = title.attributed(.note, .primaryLabel)
            valueNode.attributedText = "\(value)".attributed(.H1Large, .primaryLabel)
            setIconForCurrentStyle()
            iconNode.tintColor = AVSColor.primaryIcon.color

            automaticallyManagesSubnodes = true
            addTarget(self, action: #selector(tappedCard), forControlEvents: .touchUpInside)
        }

        func setIconForCurrentStyle() {
            switch asyncTraitCollection().userInterfaceStyle {
            case .dark:
                iconNode.image = icon.withRenderingMode(.alwaysTemplate)
            default:
                iconNode.image = icon.withRenderingMode(.alwaysOriginal)
            }
        }

        override func asyncTraitCollectionDidChange(withPreviousTraitCollection previousTraitCollection: ASPrimitiveTraitCollection) {
            super.asyncTraitCollectionDidChange(withPreviousTraitCollection: previousTraitCollection)
            if asyncTraitCollection().userInterfaceStyle != previousTraitCollection.userInterfaceStyle {
                setIconForCurrentStyle()
            }
        }

        convenience init(cardValue: CardValue) {
            self.init(title: cardValue.type.title, lineColor: cardValue.type.lineColor, icon: cardValue.type.icon, value: cardValue.value)
        }

        @objc func tappedCard() {
            onTap?()
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            lineNode.style.preferredSize = CGSize(width: 30, height: 2)
            lineNode.style.alignSelf = .start
            return [
                [lineNode, titleNode].vStacked().spaced(10),
                // The bottom -20 is due to the font that has huge whitespace with bigger size. can't align it to baseline
                [valueNode.margin(bottom: -20), iconNode].hStacked().justified(.spaceBetween).aligned(.end).spaced(20)
            ].vStacked().justified(.spaceBetween).spaced(30).cardWrapped()
        }
    }

    class LineCardsCell: ASCellNode {
        var cardNodes = [ASLayoutElement]()
        var didSelectCard: ((CardValue) -> Void)?

        init(cards: [CardValue]) {
            super.init()

            let cardNodes = cards.map { card -> DashboardInfoCard in
                let node = DashboardInfoCard(cardValue: card)
                node.onTap = { [weak self] in
                    self?.didSelectCard?(card)
                }
                return node
            }

            self.cardNodes = cardNodes

            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            for node in cardNodes {
                node.style.flexGrow = 1
                node.style.flexShrink = 1
                node.style.flexBasis = ASDimensionMakeWithPoints(20)
            }
            return cardNodes.hStacked().spaced(15).margin(left: 15, bottom: 15, right: 15)
        }
    }

    class HeaderCell: ASCellNode {
        var titleNode = ASTextNode2()
        var subTitleNode = ASTextNode2()

        init(userName: String?, buildingName: String?, userCount: Int = 0) {
            super.init()

            titleNode.attributedText = titleAttributedString(name: userName)
            subTitleNode.attributedText = subtitleAttributedString(buildingName: buildingName, userCount: userCount)

            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            [titleNode, subTitleNode].vStacked().margin(15)
        }

        func titleAttributedString(name: String?) -> NSAttributedString {
            let font = AVSFont.H1.font

            let normal = NSAttributedString.attributes(font: font, color: AVSColor.primaryLabel.color, alignment: .left)
            let highlighted = NSAttributedString.attributes(font: font, color: AVSColor.accentBackground.color, alignment: .left)

            if String.isBlank(name) {
                return NSAttributedString(string: "Hello!", attributes: normal)
            }

            return NSAttributedString(string: "Hello, ", attributes: normal)
                    .appendingString(name, attributes: highlighted)
                    .appendingString("!", attributes: normal)
        }

        func subtitleAttributedString(buildingName: String?, userCount: Int) -> NSAttributedString {
            let font = AVSFont.body.font

            let normal = NSAttributedString.attributes(font: font, color: AVSColor.secondaryLabel.color, alignment: .left)
            let highlighted = NSAttributedString.attributes(font: font, color: AVSColor.colorBlue.color, alignment: .left)

            let secondLine = NSAttributedString(string: "Here's what's going on in the last 24 hours.", attributes: normal)
            guard let buildingName = buildingName else { return secondLine }

            return NSAttributedString(string: "Your assigned building is ", attributes: normal)
                    .appendingString(buildingName, attributes: highlighted)
                    .appendingString(" with \(userCount) guests. ", attributes: normal)
                    .appendingAttributedString(secondLine)
        }
    }
}