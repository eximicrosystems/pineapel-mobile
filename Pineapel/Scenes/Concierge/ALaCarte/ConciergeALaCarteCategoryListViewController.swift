//
// Created by Andrei Stoicescu on 25.02.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import Combine
import DifferenceKit
import SwiftUI

class ConciergeALaCarteDataSource {
    var categories = [ConciergeALaCarteCategory]()

    func subCategories(_ category: ConciergeALaCarteCategory?) -> [ConciergeALaCarteCategory] {
        guard let category = category else { return [] }
        return categories.filter { $0.parentId == category.id }
    }

    var mainCategories: [ConciergeALaCarteCategory] {
        categories.filter { subCategories($0).count > 0 }
    }
}

class ConciergeALaCarteCategoryListViewController: ASDKViewController<ASDisplayNode> {
    var collectionNode: ASCollectionNode
    var items = [ConciergeALaCarteCategory]()
    lazy var refreshControl = UIRefreshControl()
    lazy var emptyNode = EmptyNode(emptyNodeType: .productList)

    private var subscriptions = Set<AnyCancellable>()
    private var dataSource: ConciergeALaCarteDataSource
    private var currentCategory: ConciergeALaCarteCategory?
    private var isMainCategory: Bool {
        currentCategory == nil
    }

    init(dataSource: ConciergeALaCarteDataSource? = nil, fromCategory: ConciergeALaCarteCategory? = nil) {
        collectionNode = .avsCollection()
        self.dataSource = dataSource ?? .init()

        if let category = fromCategory {
            currentCategory = fromCategory
            items = self.dataSource.subCategories(category)
        }

        super.init(node: collectionNode)

        collectionNode.showsVerticalScrollIndicator = false
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func reloadData() {
        let target = isMainCategory ? dataSource.mainCategories : dataSource.subCategories(currentCategory)
        let source = items

        let stagedChangeSet = StagedChangeset(source: source, target: target)
        for changeSet in stagedChangeSet {
            items = changeSet.data
            collectionNode.performDiffBatchUpdates(changeSet: changeSet)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()
        navigationItem.title = currentCategory?.name ?? "A La Carte"

        collectionNode.view.backgroundView = emptyNode.view

        showEmptyScreen(false)

        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(tapAdd))

        if isMainCategory {
            refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)
            collectionNode.view.addSubview(refreshControl)
            loadCachedItems()
            if items.count == 0 {
                SVProgressHUD.show()
            }
            refreshItems()
        }
    }

    func loadCachedItems() {
        if let items = CodableCache.get([ConciergeALaCarteCategory].self, key: "a_la_carte_concierge_categories") {
            dataSource.categories = items
            reloadData()
        }
    }

    @objc func refreshItems() {
        BuildingDetails.getBuildingID().then { buildingId in
            ConciergeService().getConciergeALaCarteCategories(buildingId: buildingId)
        }.done { [weak self] categories in
            guard let self = self else { return }
            SVProgressHUD.dismiss()
            self.dataSource.categories = categories
            self.reloadData()
            CodableCache.set(categories, key: "a_la_carte_concierge_categories")
        }.ensure { [weak self] in
            guard let self = self else { return }
            self.showEmptyScreen(self.items.count == 0)
            self.refreshControl.endRefreshing()
        }.catch { error in
            SVProgressHUD.showError(withStatus: error.localizedDescription)
            Log.this(error)
        }
    }

    func showEmptyScreen(_ show: Bool) {
        emptyNode.isHidden = !show
    }

    @objc func tapAdd() {
        let form = ConciergeProductEditForm(product: nil, category: nil, allCategories: dataSource.categories)

        let view = ConciergeProductEditView(form: form, dismissBlock: { [weak self] dismiss in
            self?.navigationController?.dismiss(animated: true)
        })

        let nav = AVSNavigationController(rootViewController: UIHostingController(rootView: view))
        nav.isModalInPresentation = true
        navigationController?.present(nav, animated: true)
    }
}

extension ConciergeALaCarteCategoryListViewController: ASCollectionDelegate, ASCollectionDataSource {

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        items.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let category = items[indexPath.item]

        return {
            ConciergePagerListNodes.RequestCell(title: category.name, subject: nil, imageUrl: category.imageUrl)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        let category = items[indexPath.item]

        if dataSource.subCategories(category).count > 0 {
            let vc = ConciergeALaCarteCategoryListViewController(dataSource: dataSource, fromCategory: category)
            navigationController?.pushViewController(vc, animated: true)
        } else {
            let categories = dataSource.categories
            BuildingDetails.getBuildingID().done { [weak self] buildingId in
                let vc = ConciergeALaCarteProductListViewController(category: category, buildingId: buildingId, allCategories: categories)
                self?.navigationController?.pushViewController(vc, animated: true)
            }.cauterize()

        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }
}

