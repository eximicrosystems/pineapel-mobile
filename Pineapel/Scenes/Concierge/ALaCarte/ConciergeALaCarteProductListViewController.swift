//
// Created by Andrei Stoicescu on 25.02.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import Combine
import DifferenceKit
import SwiftUI

class ConciergeALaCarteProductListViewController: ASDKViewController<ASDisplayNode> {
    var collectionNode: ASCollectionNode
    lazy var refreshControl = UIRefreshControl()
    lazy var emptyNode = EmptyNode(emptyNodeType: .productList)

    private var subscriptions = Set<AnyCancellable>()
    private var currentCategory: ConciergeALaCarteCategory

    var dataSource: ObservablePaginatedDataSource<ConciergeALaCarteProduct>
    var items = [ConciergeALaCarteProduct]()
    var allCategories: [ConciergeALaCarteCategory]
    var buildingId: String

    init(category: ConciergeALaCarteCategory, buildingId: String, allCategories: [ConciergeALaCarteCategory]) {
        collectionNode = .avsCollection()
        currentCategory = category
        self.allCategories = allCategories
        self.buildingId = buildingId

        dataSource = .init { per, page in
            ConciergeService().getConciergeALaCarteItems(per: per, page: page, categoryId: category.id, buildingId: buildingId)
        }
        dataSource.cacheKey = "a_la_carte_concierge_products_\(currentCategory.id)"
        dataSource.cacheable = true

        super.init(node: collectionNode)

        collectionNode.showsVerticalScrollIndicator = false
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func reloadData(target: [ConciergeALaCarteProduct]) {
        let source = items

        let stagedChangeSet = StagedChangeset(source: source, target: target)
        for changeSet in stagedChangeSet {
            items = changeSet.data
            collectionNode.performDiffBatchUpdates(changeSet: changeSet)
        }
    }

    func setupBindings() {
        dataSource.$items
                .receive(on: DispatchQueue.main)
                .sink { [weak self] items in
                    self?.reloadData(target: items)
                }.store(in: &subscriptions)

        dataSource.$showLoadingIndicator
                .receive(on: DispatchQueue.main)
                .sink { [weak self] loading in
                    guard let self = self else { return }

                    if loading {
                        if !self.refreshControl.isRefreshing {
                            SVProgressHUD.show()
                        }
                    } else {
                        SVProgressHUD.dismiss()
                        self.refreshControl.endRefreshing()
                    }
                }.store(in: &subscriptions)

        dataSource.$hasNoItemsAfterLoading.receive(on: DispatchQueue.main)
                                          .sink { [weak self] noItems in
                                              guard let self = self else { return }
                                              self.showEmptyScreen(noItems)
                                          }.store(in: &subscriptions)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()
        navigationItem.title = currentCategory.name

        collectionNode.view.backgroundView = emptyNode.view

        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(tapAdd))

        showEmptyScreen(false)

        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)
        collectionNode.view.addSubview(refreshControl)

        setupBindings()
        refreshItems()
    }

    @objc func tapAdd() {
        let form = ConciergeProductEditForm(product: nil, category: currentCategory, allCategories: allCategories)

        let view = ConciergeProductEditView(form: form, dismissBlock: { [weak self] dismiss in
            self?.navigationController?.dismiss(animated: true)
            switch dismiss {
            case .created, .updated:
                self?.refreshItems()
            default:
                ()
            }
        })

        let nav = AVSNavigationController(rootViewController: UIHostingController(rootView: view))
        nav.isModalInPresentation = true
        navigationController?.present(nav, animated: true)
    }

    @objc func refreshItems() {
        dataSource.loadMoreContentIfNeeded()
    }

    func showEmptyScreen(_ show: Bool) {
        emptyNode.isHidden = !show
    }
}

extension ConciergeALaCarteProductListViewController: ASCollectionDelegate, ASCollectionDataSource {

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        items.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let product = items[indexPath.item]
        let description = product.description?.html?.string

        return {
            ConciergeThingsToKnowListViewController.HorizontalModeCell(
                    title: product.name,
                    body: description,
                    url: product.productImageURL)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        let product = items[indexPath.item]

        let form = ConciergeProductEditForm(product: product, category: currentCategory, allCategories: allCategories)

        let view = ConciergeProductEditView(form: form, dismissBlock: { [weak self] dismiss in
            self?.navigationController?.dismiss(animated: true)
            switch dismiss {
            case .created, .updated:
                self?.refreshItems()
            default:
                ()
            }
        })

        let nav = AVSNavigationController(rootViewController: UIHostingController(rootView: view))
        nav.isModalInPresentation = true
        navigationController?.present(nav, animated: true)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }

    func collectionView(_ collectionView: ASCollectionView, willDisplayNodeForItemAt indexPath: IndexPath) {
        dataSource.loadMoreContentIfNeeded(currentItem: dataSource.items[indexPath.item])
    }
}
