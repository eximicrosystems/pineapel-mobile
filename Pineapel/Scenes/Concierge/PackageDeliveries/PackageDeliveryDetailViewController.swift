//
// Created by Andrei Stoicescu on 17/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import Combine

class PackageDeliveryDetailViewController: ASDKViewController<ASDisplayNode>, ASCollectionDataSource, ASCollectionDelegate {
    var collectionNode: ASCollectionNode

    var package: Package

    var editingPublisher: PassthroughSubject<(Package, to: Package.Status), Never>

    init(package: Package, editingPublisher: PassthroughSubject<(Package, to: Package.Status), Never>) {
        self.editingPublisher = editingPublisher
        self.package = package
        collectionNode = .avsCollection()
        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()
        navigationItem.title = "Package Details"

        if package.status == .pending {
            let btn = UIButton(type: .custom)
            btn.setImage(Asset.Images.Concierge.icHorizontalDots.image.withRenderingMode(.alwaysTemplate), for: .normal)
            btn.addTarget(self, action: #selector(tapDots), for: .touchUpInside)
            navigationItem.rightBarButtonItem = UIBarButtonItem(customView: btn)
        }
    }

    @objc func tapDots() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        alertController.addAction(UIAlertAction(title: "Notify Resident", style: .default) { [weak self] _ in
            guard let self = self else { return }
            SVProgressHUD.show()
            ConciergeService().notifyPackage(packageID: self.package.id).done {
                SVProgressHUD.showInfo(withStatus: "Notification Sent")
            }.catch { error in
                Log.thisError(error)
                SVProgressHUD.showError(withStatus: error.localizedDescription)
            }
        })

        if let nextStatus = package.status.nextStatus {
            alertController.addAction(UIAlertAction(title: "Package Delivered", style: .default) { [weak self] _ in
                guard let self = self else { return }
                let params = PackageDeliveryRequest(buildingID: self.package.buildingID, status: .delivered, id: self.package.id)
                SVProgressHUD.show()
                ConciergeService().deliverPackage(packageID: self.package.id, params: params).done { [weak self] in
                    SVProgressHUD.dismiss()
                    self?.packageDidChange(status: nextStatus)
                }.catch { error in
                    Log.thisError(error)
                    SVProgressHUD.showError(withStatus: error.localizedDescription)
                }
            })
        }

        alertController.addAction(UIAlertAction(title: L10n.General.cancel, style: .cancel, handler: nil))

        alertController.fixIpadIfNeeded(view: self.view)
        present(alertController, animated: true)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        1
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let package = self.package
        return {

            let backgroundNode = ASDisplayNode()
            backgroundNode.backgroundColor = AVSColor.secondaryBackground.color

            let imageNode = ASNetworkImageNode()
            imageNode.url = package.image
            imageNode.cornerRadius = 4

            let summary = ConciergePagerListNodes.SummaryNode(config: .matrix(
                    [
                        [
                            .init(title: "TO", subtitle: package.fullName),
                            .init(title: "FROM", subtitle: package.from),
                        ],
                        [.init(title: "APT #", subtitle: package.apartment)],
                        [.init(title: "STATUS", subtitle: package.status.detailsDisplayString, subtitleColor: package.status.color)],
                    ]))

            let titleNode = ASTextNode2(with: package.name.attributed(.bodyHeavy, .primaryLabel))

            let cell = ASCellNode()
            cell.automaticallyManagesSubnodes = true
            cell.layoutSpecBlock = { node, range in
                [
                    imageNode.ratio(3 / 4).margin(20),
                    [titleNode].vStacked().spaced(10).margin(20),
                    summary.margin(horizontal: 20)
                ].vStacked().margin(bottom: 20).background(backgroundNode).margin(bottom: 20)
            }
            return cell
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        ASSizeRange.fullWidth(collectionNode: collectionNode)
    }

    func packageDidChange(status: Package.Status) {
        editingPublisher.send((package, to: status))
    }
}
