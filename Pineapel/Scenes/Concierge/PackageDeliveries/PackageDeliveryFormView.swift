//
// Created by Andrei Stoicescu on 18/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import SwiftUI
import Alamofire
import PromiseKit
import SVProgressHUD

extension PackageDeliveryFormView {
    class DataProvider: ObservableObject {
        @Published var users: [User] = []
        @Published var concierges: [User] = []
        @Published var buildingID: String?

        @Published var isLoading = false
        @Published var error: Error? = nil

        func loadData() -> Promise<Void> {
            guard !isLoading else { return .value(()) }

            isLoading = true
            let service = ConciergeService()

            return when(fulfilled:
                        BuildingDetails.getBuildingID(),
                        service.getConciergeList(),
                        service.getAllUsers()
            ).map { buildingID, concierges, users in
                self.buildingID = buildingID
                self.concierges = concierges
                self.users = users
            }.ensure {
                self.isLoading = false
            }
        }
    }

    struct PackageDeliveryUploader: AssetMediaUploadBuilder {
        func createUploader() -> ConciergeForm.AssetMediaUploader.MediaUploaderClosure {
            { data, fileType, progress, completion in
                ConciergeService().uploadPackageImage(imageData: data, progress: progress).done { result in
                    completion(.success(String(result.fileID)))
                }.catch { error in
                    completion(.failure(APIError.from(error)))
                }
            }
        }
    }
}

class PackageDeliveryForm: ObservableObject {
    @Published var item: String = ""
    @Published var from: String = ""
    @Published var file = [String]()
    @Published var packageTo: [ConciergeForm.Option<User>] = []
    @Published var packageToOptions: [ConciergeForm.Option<User>] = []
    @Published var receivedBy: [ConciergeForm.Option<User>] = []
    @Published var conciergeOptions: [ConciergeForm.Option<User>] = []
    @Published var signedBy: [ConciergeForm.Option<User>] = []
    var apartmentNumber: String {
        if let user = packageTo.first {
            if let apartment = user.underlyingValue.apartmentNo {
                return apartment
            }
        }
        return ""
    }
    @Published var notify: Bool = false

    var buildingID: String?

    var asParams: PackageRequest? {
        guard let file = file.first,
              let receivedBy = receivedBy.first,
              let buildingID = buildingID,
              let recipient = packageTo.first,
              let signedBy = signedBy.first
                else { return nil }

        return PackageRequest(buildingID: buildingID,
                              from: from,
                              image: file,
                              receivedBy: receivedBy.underlyingValue.uid,
                              recipient: recipient.underlyingValue.uid,
                              signedBy: signedBy.underlyingValue.uid,
                              name: item)
    }

    var isValid: Bool {
        guard let file = file.first,
              let receivedBy = receivedBy.first,
              let recipient = packageTo.first,
              let signedBy = signedBy.first
                else { return false }

        return [buildingID,
                from,
                file,
                receivedBy.underlyingValue.uid,
                recipient.underlyingValue.uid,
                signedBy.underlyingValue.uid,
                item
        ].allSatisfy { !String.isBlank($0) }
    }
}

struct PackageDeliveryFormView: View {

    @ObservedObject var form: PackageDeliveryForm
    @ObservedObject var dataProvider = DataProvider()

    var dismissBlock: ((_ requiresRefresh: Bool) -> Void)?

    var body: some View {
        ScrollView(showsIndicators: false) {
            VStack(spacing: 20) {

                ConciergeForm.MediaFile(
                        label: "Package Photo",
                        placeholder: "Tap to upload media file",
                        value: $form.file,
                        maximumSelectionsAllowed: 1,
                        allowedMediaTypes: [.image],
                        uploadBuilder: PackageDeliveryUploader()
                )

                ConciergeForm.TextInput(label: "Package Item", value: $form.item)
                ConciergeForm.TextInput(label: "Package From", value: $form.from)

                ConciergeForm.SelectInput<User>(
                        label: "Package To",
                        placeholder: "Select Resident",
                        options: form.packageToOptions,
                        value: $form.packageTo)

                ConciergeForm.ReadonlyTextInput(label: "Apartment Number", value: form.apartmentNumber)

                ConciergeForm.SelectInput<User>(
                        label: "Received By",
                        placeholder: "Select Concierge",
                        options: form.conciergeOptions,
                        value: $form.receivedBy)

                ConciergeForm.SelectInput<User>(
                        label: "Signed By",
                        placeholder: "Select Concierge",
                        options: form.conciergeOptions,
                        value: $form.signedBy)

                ConciergeForm.ToggleInput(label: "Notify user about this delivery", value: $form.notify)

            }.padding(.top, 20)
             .padding(.horizontal, 20)
        }.fullViewBackgroundColor()
         .navigationBarItems(
                 leading: Button(action: {
                     dismissBlock?(false)
                 }) {
                     Image(uiImage: Asset.Images.Concierge.icClose.image.withRenderingMode(.alwaysTemplate))
                 },
                 trailing: Button(action: {
                     guard let params = form.asParams else { return }
                     SVProgressHUD.show()

                     ConciergeService().createPackageRequest(params).then { response -> Promise<Void> in
                         if form.notify {
                             return ConciergeService().notifyPackage(packageID: response.id)
                         } else {
                             return .value(())
                         }
                     }.done {
                         SVProgressHUD.dismiss()
                         dismissBlock?(true)
                     }.catch { error in
                         Log.thisError(error)
                         SVProgressHUD.showError(withStatus: error.localizedDescription)
                     }
                 }) {
                     Text("Save").navigationSaveButton()
                 }.disabled(!form.isValid))
         .navigationBarTitle(Text("New Package"), displayMode: .inline)
         .onAppear {
             dataProvider.loadData().done {
                 form.conciergeOptions = dataProvider.concierges.map { user in
                     .init(id: user.uid, label: user.fullName, underlyingValue: user)
                 }

                 form.packageToOptions = dataProvider.users.map { user in
                     .init(id: user.uid, label: user.fullName, underlyingValue: user)
                 }

                 form.buildingID = dataProvider.buildingID
             }.catch { error in
                 Log.thisError(error)
             }
         }
    }
}