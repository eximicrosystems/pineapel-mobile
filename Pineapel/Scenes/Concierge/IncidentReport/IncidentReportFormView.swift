//
// Created by Andrei Stoicescu on 29/10/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import SwiftUI
import Alamofire
import PromiseKit
import SVProgressHUD

class IncidentReportForm: ObservableObject {
    @Published var subject: String = ""
    @Published var body: String = ""
    var buildingID: String = ""
    var id: String?

    var asParams: IncidentReportRequest {
        IncidentReportRequest(id: id, body: body, subject: subject, buildingID: buildingID)
    }

    var isValid: Bool {
        !String.isBlank(subject) &&
        !String.isBlank(body) &&
        !String.isBlank(buildingID)
    }
}

struct IncidentReportFormView: View {
    @ObservedObject var form = IncidentReportForm()
    var incidentReport: IncidentReport?
    var dismissBlock: ((_ didCancel: Bool) -> Void)?

    var body: some View {
        ScrollView(showsIndicators: false) {
            VStack(spacing: 20) {
                ConciergeForm.TextInput(label: "SUBJECT", value: $form.subject)
                ConciergeForm.TextArea(label: "REPORT DETAILS", value: $form.body)
            }.padding(.top, 20)
             .padding(.horizontal, 20)
        }.fullViewBackgroundColor()
         .onAppear {

            if let report = incidentReport {
                form.body = report.body?.html?.string ?? ""
                form.subject = report.subject?.html?.string ?? ""
                form.buildingID = report.buildingID ?? ""
            } else {
                SVProgressHUD.show()
                BuildingDetails.getBuildingID().done { buildingID in
                    SVProgressHUD.dismiss()
                    form.buildingID = buildingID
                }.catch { error in
                    Log.thisError(error)
                    SVProgressHUD.showError(withStatus: error.localizedDescription)
                }
            }

        }.navigationBarItems(
                leading: Button(action: {
                    dismissBlock?(true)
                }) {
                    Image(uiImage: Asset.Images.Concierge.icClose.image.withRenderingMode(.alwaysTemplate))
                },
                trailing: Button(action: {
                    SVProgressHUD.show()
                    let promise: Promise<Void>

                    if let reportID = incidentReport?.id {
                        promise = ConciergeService().updateIncidentReport(form.asParams, incidentReportID: reportID)
                    } else {
                        promise = ConciergeService().createIncidentReport(form.asParams)
                    }

                    promise.done {
                        SVProgressHUD.dismiss()
                        dismissBlock?(false)
                    }.catch { error in
                        Log.thisError(error)
                        SVProgressHUD.showError(withStatus: error.localizedDescription)
                    }
                }) {
                    Text("Save").navigationSaveButton()
                }.disabled(!form.isValid)
        ).navigationBarTitle(Text("\(incidentReport == nil ? "Create" : "Update") Incident Report"), displayMode: .inline)

    }
}