//
// Created by Andrei Stoicescu on 25/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit

struct ConciergeMoreNodes {
    class ProfileCell: ASCellNode {
        static var avatarSize: CGFloat = 36
        var avatarNode = GenericNodes.AvatarNode(size: ProfileCell.avatarSize, shadow: false)
        var userNameNode = ASTextNode2()
        var subtitleNode = ASTextNode2()
        var lineNode = ASDisplayNode.hLine(color: AVSColor.dividerSecondary.color)

        init(user: User) {
            userNameNode.attributedText = user.fullName.attributed(.bodyTitleHeavy, .primaryLabel)
            subtitleNode.attributedText = "See your profile".attributed(.body, .secondaryLabel)
            avatarNode.url = user.userPictureURL

            super.init()
            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            [
                [
                    avatarNode.square(ProfileCell.avatarSize),
                    [userNameNode, subtitleNode].vStacked()
                ].hStacked().spaced(15),
                lineNode
            ].vStacked().spaced(10).margin(top: 0, left: 15, bottom: 10, right: 15)
        }
    }
}
