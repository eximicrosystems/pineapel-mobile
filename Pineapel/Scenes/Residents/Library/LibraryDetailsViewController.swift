//
// Created by Andrei Stoicescu on 14/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import Combine
import AVFoundation
import AVKit

class LibraryDetailsViewController: ASDKViewController<ASDisplayNode>, ASCollectionDataSource, ASCollectionDelegate {

    class VideoPreviewNode: ASDisplayNode {
        var videoURL: URL?

        var imageNode: ASNetworkImageNode
        var placeholderNode: ASImageNode

        var didTapVideo: ((URL?) -> Void)?

        init(videoURL: URL?, placeholderImageUrl: URL?) {
            self.videoURL = videoURL

            imageNode = ASNetworkImageNode()
            imageNode.url = placeholderImageUrl
            imageNode.contentMode = .scaleAspectFill

            placeholderNode = ASImageNode(image: Asset.Images.btnVideo.image)
            placeholderNode.isUserInteractionEnabled = false

            super.init()
            imageNode.addTarget(self, action: #selector(tappedVideo), forControlEvents: .touchUpInside)
            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            placeholderNode.centered().overlay(imageNode.ratio(9 / 16))
        }

        @objc func tappedVideo() {
            didTapVideo?(videoURL)
        }

    }

    class SingleDocumentCell: ASCellNode {
        var titleNode = ASTextNode2()
        var infoNode = ASTextNode2()
        var videoNode: VideoPreviewNode?
        var card = CardNode(rounded: false, shadow: false)

        var didTapVideo: ((URL?) -> Void)? {
            didSet {
                videoNode?.didTapVideo = didTapVideo
            }
        }

        init(title: String?, info: String?, videoItem: LibraryContent?, imageItem: LibraryContent?) {
            titleNode.attributedText = title?.attributed(.bodyTitleHeavy, .primaryLabel)
            infoNode.attributedText = info?.attributed(.bodyRoman, .primaryLabel)

            if let videoItem = videoItem, let imageItem = imageItem {
                let videoNode = VideoPreviewNode(videoURL: videoItem.url, placeholderImageUrl: imageItem.url)
                videoNode.cornerRadius = 8
                videoNode.clipsToBounds = true
                self.videoNode = videoNode
            }

            super.init()

            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            [videoNode, titleNode, infoNode].vStacked().spaced(10).cardWrapped(card).margin(bottom: 20)
        }
    }

    enum Sections: Int, CaseIterable {
        case document
        case linksHeader
        case links
    }

    var collectionNode: ASCollectionNode
    var document: LibraryItem
    var type: LibraryItem.ItemType

    init(document: LibraryItem, type: LibraryItem.ItemType) {
        self.document = document
        self.type = type
        collectionNode = .avsCollection()
        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()
        switch type {
        case .document:
            navigationItem.title = "Document Details"
        case .video:
            navigationItem.title = "Video Details"
        }
    }

    func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
        Sections.allCases.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        guard let section = Sections(rawValue: section) else { return 0 }

        switch section {
        case .document:
            return 1
        case .links:
            return document.documents.count
        case .linksHeader:
            return document.documents.count > 0 ? 1 : 0
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        guard let section = Sections(rawValue: indexPath.section) else { return { ASCellNode() } }
        switch section {
        case .document:
            let document = self.document
            let body = document.body?.html?.string
            return {
                let cell = SingleDocumentCell(title: document.name, info: body, videoItem: document.video, imageItem: document.image)
                cell.didTapVideo = { [weak self] url in
                    DispatchQueue.main.async { [weak self] in
                        guard let self = self, let url = url else { return }
                        let player = AVPlayer(url: url)
                        let playerVC = AVPlayerViewController()
                        playerVC.player = player
                        self.present(playerVC, animated: true) {
                            playerVC.player?.play()
                        }
                    }
                }
                return cell
            }
        case .links:
            let document = self.document.documents[indexPath.item]
            return { LibraryListViewController.Cell(title: document.name, info: nil, imageURL: nil) }
        case .linksHeader:
            return {
                let titleNode = ASTextNode2(with: "Documents".attributed(.bodyTitleHeavy, .primaryLabel))
                let cell = ASCellNode()
                cell.automaticallyManagesSubnodes = true
                cell.layoutSpecBlock = { node, range in
                    titleNode.margin(top: 20, left: 20, bottom: 10)
                }
                return cell
            }
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        ASSizeRange.fullWidth(collectionNode: collectionNode)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        guard indexPath.section == Sections.links.rawValue else { return }
        let document = self.document.documents[indexPath.item]

        let vc = ArticleWebViewController(url: document.url)
        vc.closeBlock = { [weak self] in
            self?.dismiss(animated: true)
        }
        let nav = AVSNavigationController(rootViewController: vc)
        vc.title = document.name
        present(nav, animated: true)
    }
}
