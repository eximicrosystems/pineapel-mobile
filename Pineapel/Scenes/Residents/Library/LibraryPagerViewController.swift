//
// Created by Andrei Stoicescu on 14/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import PromiseKit
import Combine
import SVProgressHUD
import DifferenceKit

class LibraryItemSource: ObservablePaginatedDataSource<LibraryItem> {
    var category: LibraryCategory
    var type: LibraryItem.ItemType

    init(category: LibraryCategory, type: LibraryItem.ItemType) {
        self.category = category
        self.type = type
        super.init()

        paginate = { per, page in
            ResidentService().getLibraryItems(per: per, page: page, type: type, categoryID: category.id)
        }

        cacheable = true
        cacheKey = "library_\(type.rawValue)_\(category.id)"
    }
}

class LibraryItemSourceManager: ObservableObject {
    @Published var sources = [LibraryItemSource]()
    @Published private(set) var currentSource: LibraryItemSource?
    var isLoading: Bool = false
    @Published var showLoadingIndicator: Bool = false
    var type: LibraryItem.ItemType

    init(type: LibraryItem.ItemType) {
        self.type = type
    }

    func loadSources() {
        guard !isLoading else { return }
        isLoading = true

        if let categories = CodableCache.get([LibraryCategory].self, key: "LibraryItemSourceManager") {
            sources = categories.map { .init(category: $0, type: type) }
            currentSource = sources.first
        } else {
            showLoadingIndicator = true
        }

        ResidentService().getLibraryCategories().done { [weak self] categories in
            guard let self = self else { return }
            CodableCache.set(categories, key: "LibraryItemSourceManager")
            self.sources = categories.map { LibraryItemSource(category: $0, type: self.type) }
            self.currentSource = self.sources.first
        }.catch { error in
            Log.thisError(error)
        }.finally {
            self.isLoading = false
            self.showLoadingIndicator = false
        }
    }

    var count: Int {
        sources.count
    }

    var currentSourceIndex: Int? {
        sources.firstIndex { $0.category.id == currentSource?.category.id }
    }

    func setActiveCategory(_ category: LibraryCategory) {
        guard let source = sources.first(where: { $0.category.id == category.id }) else { return }
        currentSource = source
    }

    func setActiveSource(at index: Int) {
        guard let source = sources[safe: index] else { return }
        currentSource = source
    }

    subscript(index: Int) -> LibraryItemSource {
        sources[index]
    }
}

class LibraryPagerViewController: ASDKViewController<ASDisplayNode>, ASPagerDataSource, ASPagerDelegate {

    var sourceManager: LibraryItemSourceManager
    var items = [LibraryCategory]()

    var pager = ASPagerNode()

    var segmentedBar = CollectionSegmentedDisplayNode<LibraryCategory>()

    private var subscriptions = Set<AnyCancellable>()

    init(type: LibraryItem.ItemType) {
        sourceManager = .init(type: type)
        segmentedBar = .init()

        let mainNode = ASDisplayNode()
        mainNode.automaticallyManagesSubnodes = true
        mainNode.automaticallyRelayoutOnSafeAreaChanges = true

        super.init(node: mainNode)

        pager.setDataSource(self)
        pager.setDelegate(self)
        pager.backgroundColor = .clear

        mainNode.layoutSpecBlock = { node, range in
            [
                self.segmentedBar.margin(top: node.safeAreaInsets.top),
                self.pager.growing()
            ].vStacked()
        }

        segmentedBar.didSelect = { [weak self] category, index in
            guard let self = self else { return }
            self.sourceManager.setActiveCategory(category)
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func pagerNode(_ pagerNode: ASPagerNode, nodeBlockAt index: Int) -> ASCellNodeBlock {
        let category = items[index]
        guard let source = sourceManager.sources.first(where: { $0.category == category }) else { return { ASCellNode() }}

        return {
            ASCellNode(viewControllerBlock: { [weak self] in
                guard let self = self else { return UIViewController() }
                let vc = LibraryListViewController(dataSource: source)
                vc.didSelectItem = { [weak self] item in
                    guard let self = self else { return }
                    self.show(LibraryDetailsViewController(document: item, type: source.type), sender: self)
                }
                return vc
            })
        }
    }

    func reloadData() {
        let source = items
        let target = sourceManager.sources.map(\.category)

        let stagedChangeSet = StagedChangeset(source: source, target: target)
        for changeSet in stagedChangeSet {
            items = changeSet.data
            pager.performDiffBatchUpdates(changeSet: changeSet)
        }
    }


    func numberOfPages(in pagerNode: ASPagerNode) -> Int {
        items.count
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        sourceManager.setActiveSource(at: pager.currentPageIndex)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        switch sourceManager.type {

        case .document:
            navigationItem.title = "Document Library"
        case .video:
            navigationItem.title = "Video Library"
        }

        replaceSystemBackButton()
        view.backgroundColor = AVSColor.primaryBackground.color

        sourceManager.$currentSource.receive(on: DispatchQueue.main).sink { [weak self] value in
            guard let self = self,
                  let idx = self.sourceManager.currentSourceIndex else { return }

            self.pager.scrollToPage(at: idx, animated: true)
            self.segmentedBar.selectItem(at: idx)
        }.store(in: &subscriptions)

        sourceManager.$showLoadingIndicator.receive(on: DispatchQueue.main).sink { loading in
            loading ? SVProgressHUD.show() : SVProgressHUD.dismiss()
        }.store(in: &subscriptions)

        sourceManager.$sources.receive(on: DispatchQueue.main).sink { [weak self] items in
            guard let self = self else { return }

            self.segmentedBar.items = items.map(\.category)
            if let idx = self.sourceManager.currentSourceIndex {
                self.segmentedBar.selectItem(at: idx)
            }
            self.reloadData()
        }.store(in: &subscriptions)

        sourceManager.loadSources()
    }
}

extension LibraryCategory: SegmentedDisplayNodePresentable {
    var title: String { name }
}