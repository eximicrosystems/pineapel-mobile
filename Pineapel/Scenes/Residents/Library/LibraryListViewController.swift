//
// Created by Andrei Stoicescu on 14/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import PromiseKit
import Combine
import DifferenceKit

class LibraryListViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

    class Cell: ASCellNode {
        var imageNode = ASNetworkImageNode()
        var titleNode = ASTextNode2()
        var infoNode = ASTextNode2()
        var overlayImageNode: ASImageNode?
        var card = CardNode(rounded: false, shadow: false)

        init(title: String?, info: String?, imageURL: URL?, overlayImage: UIImage? = nil) {

            if let url = imageURL {
                imageNode.url = url
                imageNode.contentMode = .scaleAspectFill
                imageNode.backgroundColor = AVSColor.tertiaryBackground.color
            } else {
                imageNode.image = Asset.Images.icDocument.image.withRenderingMode(.alwaysTemplate)
                imageNode.contentMode = .center
//                imageNode.backgroundColor = AVSColor.accentForeground.color
                imageNode.backgroundColor = AVSColor.colorMilitaryGreen.color
                imageNode.tintColor = AVSColor.colorMilitaryGreen.color.withAlphaComponent(0.4)
            }

            if let image = overlayImage {
                let overlayNode = ASImageNode(image: image)
                overlayNode.contentMode = .center
                overlayImageNode = overlayNode
            }

            imageNode.cornerRadius = 4
            titleNode.attributedText = title?.attributed(.bodyTitleHeavy, .primaryLabel)
            infoNode.attributedText = info?.attributed(.body, .primaryLabel)
            super.init()
            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            let ratio = imageNode.ratio(11 / 13).maxWidth(.fraction(0.35))
            let centerOverlay = overlayImageNode?.centered().overlay(ratio)

            return [
                centerOverlay ?? ratio,
                [titleNode, infoNode].vStacked().spaced(10).filling()
            ].hStacked().spaced(20).aligned(.start).cardWrapped().margin(horizontal: 15).margin(bottom: 10)
        }
    }

    lazy var emptyNode = EmptyNode(emptyNodeType: .generic, showAction: false)

    lazy var refreshControl = UIRefreshControl()
    var collectionNode: ASCollectionNode

    var dataSource: LibraryItemSource
    var items = [LibraryItem]()

    private var subscriptions = Set<AnyCancellable>()

    var didSelectItem: ((LibraryItem) -> Void)?

    init(dataSource: LibraryItemSource) {
        collectionNode = .avsCollection()
        self.dataSource = dataSource
        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupBindings()

        view.backgroundColor = AVSColor.primaryBackground.color

        replaceSystemBackButton()
        showEmptyScreen(false)
        collectionNode.view.backgroundView = emptyNode.view
        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)

        collectionNode.view.addSubview(refreshControl)
        collectionNode.backgroundColor = .clear

        refreshItems()
    }

    func reloadData() {
        let source = items
        let target = dataSource.items

        let stagedChangeSet = StagedChangeset(source: source, target: target)
        for changeSet in stagedChangeSet {
            items = changeSet.data
            collectionNode.performDiffBatchUpdates(changeSet: changeSet)
        }
    }

    func setupBindings() {
        dataSource.$items
                .receive(on: DispatchQueue.main)
                .sink { [weak self] items in
                    self?.reloadData()
                }.store(in: &subscriptions)

        dataSource.$showLoadingIndicator
                .receive(on: DispatchQueue.main)
                .sink { [weak self] loading in
                    guard let self = self else { return }

                    if loading {
                        if !self.refreshControl.isRefreshing {
                            SVProgressHUD.show()
                        }
                    } else {
                        SVProgressHUD.dismiss()
                        self.refreshControl.endRefreshing()
                    }
                }.store(in: &subscriptions)

        dataSource.$hasNoItemsAfterLoading
                .receive(on: DispatchQueue.main)
                .sink { [weak self] hasNoItems in
                    self?.showEmptyScreen(hasNoItems)
                }.store(in: &subscriptions)
    }

    @objc func refreshItems() {
        dataSource.loadMoreContentIfNeeded()
    }

    func showEmptyScreen(_ show: Bool) {
        emptyNode.isHidden = !show
    }

    func collectionView(_ collectionView: ASCollectionView, willDisplayNodeForItemAt indexPath: IndexPath) {
        dataSource.loadMoreContentIfNeeded(currentItem: dataSource.items[indexPath.item])
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        items.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let item = items[indexPath.item]
        let overlayImage = dataSource.type == .video ? Asset.Images.btnVideoSm.image : nil
        return {
            Cell(title: item.name, info: item.teaser, imageURL: item.image?.url, overlayImage: overlayImage)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        didSelectItem?(items[indexPath.item])
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }
}
