//
// Created by Andrei Stoicescu on 12/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD

class SubscriptionDoneViewController: ASDKViewController<ASDisplayNode> {

    private typealias ThankYouCell = AmenityThankYouViewController.ThankYouCell
    private typealias ThankYouDoneCell = AmenityThankYouViewController.ThankYouDoneCell

    private enum Sections: Int, CaseIterable {
        case thankYou
        case done
    }

    private var collectionNode: ASCollectionNode
    var checkoutData: SubscriptionCheckoutData

    init(checkoutData: SubscriptionCheckoutData) {
        self.checkoutData = checkoutData
        let collectionNode = ASCollectionNode.avsCollection()
        self.collectionNode = collectionNode

        let bgImageNode = ASImageNode(image: Asset.Images.bgSplash.image)

        let mainNode = ASDisplayNode()
        mainNode.automaticallyManagesSubnodes = true
        mainNode.layoutSpecBlock = { node, range in
            collectionNode.background(bgImageNode)
        }

        super.init(node: mainNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.setHidesBackButton(true, animated: false)
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: L10n.General.close, style: .plain, target: self, action: #selector(didTapClose))
        navigationItem.title = "Subscription"

        // TODO: This might not get executed in time? the user can tap done in the meanwhile
        ResidentService().getUserSubscription().done { [weak self] subscription in
            self?.checkoutData.currentSubscription = subscription
            self?.checkoutData.flowFinalized?(subscription)
        }.catch { error in
            Log.thisError(error)
        }
    }

    @objc func didTapClose() {
        guard let nav = navigationController as? AVSNavigationController else { return }
        nav.dismissNavigation?()
    }
}

extension SubscriptionDoneViewController: ASCollectionDelegate, ASCollectionDataSource {
    public func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
        Sections.allCases.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        guard let section = Sections(rawValue: section) else { return 0 }
        switch section {
        case .thankYou:
            return 1
        case .done:
            return 1
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        guard let section = Sections(rawValue: indexPath.section) else { return { ASCellNode() } }

        switch section {
        case .thankYou:
            return { ThankYouCell(title: "Thank you for your purchase!", body: "You're now subscribed to \(self.checkoutData.subscription.name)") }
        case .done:
            return { ThankYouDoneCell() }
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == Sections.done.rawValue {
            didTapClose()
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        ASSizeRange.fullWidth(collectionNode: collectionNode)
    }
}
