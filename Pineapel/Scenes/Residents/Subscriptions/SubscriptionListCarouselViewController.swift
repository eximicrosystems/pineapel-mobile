//
// Created by Andrei Stoicescu on 28.01.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit
import AsyncDisplayKit
import ScalingCarousel
import SVProgressHUD
import PromiseKit

class SubscriptionCheckoutData {

    enum SubscriptionOption {
        case subscribe
        case cancel
        case renew
    }

    var subscription: Subscription
    var paymentMethod: PaymentMethodSelection?
    var currentSubscription: UserSubscription?

    var flowFinalized: ((UserSubscription?) -> Void)?

    var subscriptionOption: SubscriptionOption {
        Self.subscriptionOption(subscription, currentSubscription: currentSubscription)
    }

    static func subscriptionOption(_ subscription: Subscription, currentSubscription: UserSubscription?) -> SubscriptionOption {
        guard let currentSubscription = currentSubscription else { return .subscribe }
        if subscription.subscriptionID == currentSubscription.subscriptionID {
            if currentSubscription.canceled {
                return .renew
            } else {
                return .cancel
            }
        } else {
            return .subscribe
        }
    }

    init(subscription: Subscription, current: UserSubscription?) {
        self.subscription = subscription
        self.currentSubscription = current
    }
}

class SubscriptionCell: ScalingCarouselCell {

    var didSelectButton: (() -> Void)?

    lazy var borderView: UIView = {
        let view = UIView()
        view.layer.borderColor = AVSColor.inputBorder.color.cgColor
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 6
        return view
    }()

    lazy var recommendedView: PaddingLabel = {
        let view = PaddingLabel()
        view.leftInset = 15
        view.rightInset = 15
        view.topInset = 0
        view.bottomInset = 0

//        view.attributedText = "RECOMMENDED".attributed(.bodyHeavy, .accentForeground)
        view.attributedText = "RECOMMENDED".attributed(.bodyHeavy, .colorMilitaryGreen)
        view.backgroundColor = AVSColor.colorTeal.color
        view.layer.cornerRadius = 10
        view.clipsToBounds = true
        return view
    }()

    lazy var stackView: UIStackView = {
        buildStack(spacing: 20)
    }()

    lazy var titleLabel = UILabel()
    lazy var subtitleLabel = UILabel()
    lazy var priceLabel = UILabel()
    lazy var descriptionLabel: UITextView = {
        let label = UITextView()
        label.isEditable = false
        label.backgroundColor = .clear
        return label
    }()
    lazy var calloutButton = UIButton.secondaryButton(title: "Subscribe")

    lazy var divider: UIView = {
        let view = UIView()
        view.backgroundColor = AVSColor.divider.color
        return view
    }()

    var recommended = false {
        didSet {
            if recommended != oldValue {
                recommendedView.removeFromSuperview()

                if recommended {
                    mainView.addSubview(recommendedView)
                    recommendedView.snp.makeConstraints { make in
                        make.centerX.equalToSuperview()
                        make.top.equalToSuperview().offset(-10)
                    }
                }
            }
        }
    }

    func buildStack(spacing: CGFloat) -> UIStackView {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = spacing
        return stackView
    }

    var articleBody: String?

    override init(frame: CGRect) {
        super.init(frame: frame)

        mainView = UIView(frame: contentView.bounds)
        mainView.translatesAutoresizingMaskIntoConstraints = false
        mainView.backgroundColor = AVSColor.secondaryBackground.color

        cornerRadius = 0

        mainView.addSubview(borderView)
        mainView.addSubview(stackView)

        let labelsStack = buildStack(spacing: 5)
        labelsStack.addArrangedSubviews(subviews: [titleLabel])

        stackView.addArrangedSubviews(subviews: [labelsStack, priceLabel, divider, descriptionLabel, calloutButton])

        contentView.addSubview(mainView)

        mainView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        borderView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        stackView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(30)
            make.leading.equalToSuperview().offset(30)
            make.bottom.equalToSuperview().offset(-30)
            make.trailing.equalToSuperview().offset(-30)
        }

        calloutButton.snp.makeConstraints { make in
            make.height.equalTo(50)
        }

        divider.snp.makeConstraints { make in
            make.height.equalTo(1)
        }

        calloutButton.addTarget(self, action: #selector(selectCalloutButton), for: .touchUpInside)
    }

    @objc func selectCalloutButton() {
        didSelectButton?()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func configure(subscription: Subscription, currentSubscription: UserSubscription?) {
        recommended = subscription.recommended

        let subscriptionOption = SubscriptionCheckoutData.subscriptionOption(subscription, currentSubscription: currentSubscription)

        switch subscriptionOption {

        case .subscribe:
            calloutButton.setTitle("Subscribe", for: .normal)
            calloutButton.backgroundColor = AVSColor.colorMilitaryGreen.color
        case .cancel:
            calloutButton.setTitle("Subscribed", for: .normal)
            calloutButton.backgroundColor = AVSColor.colorTeal.color
        case .renew:
            calloutButton.setTitle("Renew Subscription", for: .normal)
            calloutButton.backgroundColor = AVSColor.accentBackground.color
        }

        titleLabel.attributedText = subscription.name.attributed(.H4, .primaryLabel)
        subtitleLabel.attributedText = "Recommended plan for service savvy".attributed(.body, .primaryLabel)
        let attributedPrice = subscription.price.currencyDollarClean.attributed(.H4, .colorTeal)?.appendingAttributedString(" / month".attributed(.body, .primaryLabel))
        priceLabel.attributedText = attributedPrice
        articleBody = subscription.articleBody
        descriptionLabel.attributedText = articleBody?.html
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if traitCollection.userInterfaceStyle != previousTraitCollection?.userInterfaceStyle {
            descriptionLabel.attributedText = articleBody?.html
            borderView.layer.borderColor = AVSColor.inputBorder.color.cgColor
        }
    }
}

class SubscriptionListCarouselViewController: UIViewController {
    var scalingCarousel: ScalingCarouselView!

    var subscriptions: [Subscription] = []
    var userSubscription: UserSubscription?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        addCarousel()
        
        let tabBar = BottonTabBarView(frame: CGRect(x: 0, y: UIScreen.main.bounds.height-83, width: UIScreen.main.bounds.width, height: 83))
        self.view.addSubview(tabBar)
        let lateralMenu = LateralMenuView(frame: CGRect(x: 80, y: 0, width: 347, height: 844))
        let myView = HeaderTabBarView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 190), tabBarVC: self, lm: lateralMenu)
        myView.backButton.isHidden = false
        lateralMenu.isHidden = true
        lateralMenu.header = myView
        self.view.addSubview(myView)
        self.view.addSubview(lateralMenu)
        
        replaceSystemBackButton()
        navigationItem.title = "Subscriptions"

        if !loadCached() {
            SVProgressHUD.show()
        }

        refreshItems()
    }

    func loadCached() -> Bool {
        if let subscriptions = CodableCache.get([Subscription].self, key: "subscriptions") {
            self.subscriptions = subscriptions
        }

        if let currentSubscription = CodableCache.get(UserSubscription.self, key: "current_user_subscription") {
            userSubscription = currentSubscription
        }

        if subscriptions.count > 0 {
            reloadData()
            return true
        }

        return false
    }

    @objc func refreshItems() {
        let apiService = ResidentService()

        let getUserSub = apiService.getUserSubscription().get { sub in
            if let sub = sub {
                CodableCache.set(sub, key: "current_user_subscription")
            }
        }.recover { error -> Promise<UserSubscription?> in
            .value(nil)
        }

        let getSubscriptionList = apiService.getSubscriptionList().get { subs in
            CodableCache.set(subs, key: "subscriptions")
        }

        when(fulfilled: getSubscriptionList, getUserSub).done { [weak self] subscriptions, subscription in
            guard let self = self else { return }
            self.subscriptions = subscriptions
            self.userSubscription = subscription
            self.reloadData()
        }.ensure {
            SVProgressHUD.dismiss()
        }.catch { error in
            Log.thisError(error)
        }
    }

    func reloadData() {
        scalingCarousel.reloadData()
        let index = subscriptions.firstIndex { $0.subscriptionID == userSubscription?.subscriptionID } ?? 1
        DispatchQueue.main.async { [weak self] in
            self?.scalingCarousel.scrollToItem(at: IndexPath(row: index, section: 0), at: .centeredHorizontally, animated: false)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if scalingCarousel != nil {
            scalingCarousel.deviceRotated()
        }
    }

    private func addCarousel() {

        let frame = CGRect(x: 0, y: 0, width: 0, height: 0)
        scalingCarousel = ScalingCarouselView(withFrame: frame, andInset: 40)
        scalingCarousel.scrollDirection = .horizontal
        scalingCarousel.dataSource = self
        scalingCarousel.delegate = self
        scalingCarousel.translatesAutoresizingMaskIntoConstraints = false
        scalingCarousel.backgroundColor = .clear
        scalingCarousel.clipsToBounds = false

        scalingCarousel.register(SubscriptionCell.self, forCellWithReuseIdentifier: "cell")

        view.addSubview(scalingCarousel)
        view.backgroundColor = AVSColor.secondaryBackground.color
        view.clipsToBounds = true

        scalingCarousel.snp.makeConstraints { make in
//            make.top.equalTo(view.safeAreaLayoutGuide.snp.topMargin).offset(60)
//            make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottomMargin).offset(-30)
            
            make.top.equalTo(view.safeAreaLayoutGuide.snp.topMargin).offset(190)
            make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottomMargin).offset(-83)
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
        }
    }

    func handleActionTap(_ subscription: Subscription) {
        let checkoutData = SubscriptionCheckoutData(subscription: subscription, current: userSubscription)

        checkoutData.flowFinalized = { [weak self] userSubscription in
            guard let self = self else { return }
            self.userSubscription = userSubscription
            self.scalingCarousel.reloadData()
        }

        switch checkoutData.subscriptionOption {
        case .subscribe, .renew:
            let vc = SubscriptionPaymentSelectionViewController(checkoutData: checkoutData)
            let nav = AVSNavigationController(rootViewController: vc)
            vc.isModalInPresentation = true
            nav.dismissNavigation = { [weak self] in
                self?.navigationController?.dismiss(animated: true)
            }
            navigationController?.present(nav, animated: true)
        case .cancel:
            handleCancelAsk(subscription)
        }
    }

    func handleCancelAsk(_ subscription: Subscription) {
        let alertController = UIAlertController(title: subscription.name, message: nil, preferredStyle: .actionSheet)

        alertController.addAction(.init(title: "Cancel Subscription", style: .destructive) { [weak self] _ in
            self?.handleCancelConfirm(subscription)
        })

        alertController.addAction(UIAlertAction(title: L10n.General.close, style: .cancel, handler: nil))
        alertController.fixIpadIfNeeded(view: view)
        present(alertController, animated: true)
    }

    func handleCancelConfirm(_ subscription: Subscription) {
        let alertController = UIAlertController(title: nil, message: "Are you sure that you want to cancel your \(subscription.name) subscription?", preferredStyle: .actionSheet)
        let deleteAction = UIAlertAction(title: "Cancel Subscription", style: .destructive) { [weak self] _ in
            guard let self = self else { return }
            guard let subscription = self.userSubscription else { return }

            SVProgressHUD.show()
            ResidentService().updateSubscription(params: SubscriptionManageRequest(operation: .cancel, subscriptionID: subscription.subscriptionID, paymentID: nil)).then {
                ResidentService().getUserSubscription()
            }.done { [weak self] userSubscription in
                SVProgressHUD.showInfo(withStatus: "Your subscription for \(subscription.name) has been canceled")
                Log.this("Subscription canceled")
                guard let self = self else { return }
                self.userSubscription = userSubscription
                self.scalingCarousel.reloadData()
            }.catch { error in
                SVProgressHUD.showError(withStatus: error.localizedDescription)
                Log.thisError(error)
            }
        }

        alertController.addAction(deleteAction)
        alertController.addAction(UIAlertAction(title: L10n.General.close, style: .cancel, handler: nil))
        alertController.fixIpadIfNeeded(view: view)
        present(alertController, animated: true)
    }
}

extension SubscriptionListCarouselViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        subscriptions.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)

        if let scalingCell = cell as? SubscriptionCell {
            let subscription = subscriptions[indexPath.row]
            scalingCell.configure(subscription: subscription, currentSubscription: userSubscription)
            scalingCell.didSelectButton = { [weak self] in
                self?.handleActionTap(subscription)
            }
        }

        DispatchQueue.main.async {
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
        }

        return cell
    }

}

extension SubscriptionListCarouselViewController: UICollectionViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scalingCarousel.didScroll()
    }
}

