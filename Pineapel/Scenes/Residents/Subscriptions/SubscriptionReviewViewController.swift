//
// Created by Andrei Stoicescu on 12/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import Stripe
import SVProgressHUD

class SubscriptionReviewViewController: ASDKViewController<ASDisplayNode> {

    private typealias OrderSummary = PaymentSummary.OrderSummary
    private typealias ReviewProduct = AmenityPaymentReviewManager.ReviewProduct

    private enum Sections: Int, CaseIterable {
        case items
        case summary
        case save
    }

    private var reviewItems = [ReviewProduct]()

    private var collectionNode: ASCollectionNode
    private var checkoutData: SubscriptionCheckoutData

    private var nextInvoice: SubscriptionNextInvoice?

    private var _forceSaveButtonDisabled = false

    private var saveButtonDisabled: Bool {
        _forceSaveButtonDisabled || nextInvoice == nil
    }

    init(checkoutData: SubscriptionCheckoutData) {
        let collectionNode = ASCollectionNode.avsCollection()
        self.collectionNode = collectionNode
        self.checkoutData = checkoutData

        super.init(node: collectionNode)
        collectionNode.backgroundColor = AVSColor.secondaryBackground.color
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    func createReviewItems() {
        reviewItems.removeAll()
        reviewItems.append(.titleSubtitle(title: checkoutData.subscription.name, subtitle: "Subscription"))

        if let paymentMethod = checkoutData.paymentMethod {
            reviewItems.append(.paymentMethod(method: paymentMethod))
        }

        if let line = nextInvoice?.lines.last, !line.period.start.isToday {
            reviewItems.append(.titleSubtitle(title: "Upcoming invoice", subtitle: "This is a preview of the invoice that will be billed on \(line.period.start.avsDateString()). It may change if the subscription is updated"))
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        replaceSystemBackButton()
        navigationItem.title = "Subscription"

        SVProgressHUD.show()

        ResidentService().nextInvoice(subscriptionID: self.checkoutData.subscription.subscriptionID).done { invoice in
            SVProgressHUD.dismiss()
            self.nextInvoice = invoice
            self.createReviewItems()
            self.collectionNode.reloadData()
        }.catch { error in
            SVProgressHUD.showError(withStatus: error.localizedDescription)
            Log.thisError(error)
        }
    }

}

extension SubscriptionReviewViewController: ASCollectionDelegate, ASCollectionDataSource {
    public func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
        Sections.allCases.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case Sections.items.rawValue:
            return reviewItems.count
        case Sections.save.rawValue:
            return nextInvoice != nil ? 1 : 0
        case Sections.summary.rawValue:
            return nextInvoice != nil ? 1 : 0
        default:
            return 0
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        guard let section = Sections(rawValue: indexPath.section) else { return { ASCellNode() } }
        switch section {
        case .items:
            let product = reviewItems[indexPath.item]
            return {
                switch product {
                case .discountCodes, .cartProduct:
                    return ASCellNode()
                case .titleSubtitle(let title, let subtitle, let rightLabel):
                    return GenericNodes.TitleSubtitleCell(title: title, subtitle: subtitle, rightLabel: rightLabel)
                case .paymentMethod(let method):
                    switch method {
                    case .applePay:
                        return GenericNodes.TitleSubtitleCell(title: "Payment", subtitle: "Apple Pay")
                    case .paymentMethod(let method):
                        guard let card = method.card else {
                            return ASCellNode()
                        }
                        let brandString = STPPaymentMethodCard.string(from: card.brand)
                        let endingIn = card.last4 != nil ? " ending in \(card.last4!)" : ""
                        return GenericNodes.TitleSubtitleCell(title: "Payment", subtitle: "\(brandString)\(endingIn)")
                    }
                }
            }
        case .save:
            return { GenericNodes.PrimaryButtonCellNode(title: "Continue") }
        case .summary:
            guard let nextInvoice = nextInvoice else { return { ASCellNode() } }

            return {

                var finalLines = [PaymentSummary.Element]()
                let groupedLines = nextInvoice.groupedLines

                let sortedPeriods = groupedLines.keys.sorted { period, period2 in
                    period.start < period2.start
                }

                for period in sortedPeriods {
                    if let lines = groupedLines[period] {
                        finalLines.append(.lineLabel(label: "\(period.start.avsDateString()) - \(period.end.avsDateString())"))
                        for line in lines {
                            finalLines.append(.data(label: line.description, value: line.amount.currencyCentClean))
                        }
                    }
                }

                finalLines.append(.line)
                finalLines.append(.data(label: "Order Total", value: nextInvoice.amountDue.currencyCentClean))

                return PaymentSummary.Cell(elements: finalLines)
            }
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.section {
        case Sections.save.rawValue:
            if !saveButtonDisabled {
                finalizeOrder()
            }
        default:
            ()
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        ASSizeRange.fullWidth(collectionNode: collectionNode)
    }

    func finalizeOrder() {
        guard let paymentMethod = self.checkoutData.paymentMethod else {
            SVProgressHUD.showError(withStatus: "Payment method error")
            return
        }

        _forceSaveButtonDisabled = true

        switch paymentMethod {

        case .applePay:
            ()
        case .paymentMethod(let method):
            SVProgressHUD.show()

            let request = SubscriptionManageRequest(operation: .edit, subscriptionID: self.checkoutData.subscription.subscriptionID, paymentID: method.stripeId)

            ResidentService().updateSubscription(params: request).done { [weak self] in
                SVProgressHUD.dismiss()
                self?.handlePaymentSucceeded()
            }.catch { error in
                SVProgressHUD.showError(withStatus: error.localizedDescription)
                Log.thisError(error)
            }.finally {
                self._forceSaveButtonDisabled = false
            }
        }
    }

    func handlePaymentSucceeded() {
        _forceSaveButtonDisabled = true
        SVProgressHUD.dismiss()
        self.navigationController?.pushViewController(SubscriptionDoneViewController(checkoutData: checkoutData), animated: true)
    }
}

extension SubscriptionReviewViewController: STPAuthenticationContext {
    func authenticationPresentingViewController() -> UIViewController {
        self
    }
}
