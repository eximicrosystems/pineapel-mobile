//
// Created by Andrei Stoicescu on 13/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit

class ArticleViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {
    var collectionNode: ASCollectionNode {
        node as! ASCollectionNode
    }

    var article: ArticleProtocol

    init(article: ArticleProtocol) {
        self.article = article
        let collectionNode = ASCollectionNode.avsCollection()
        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()
        self.navigationItem.title = article.articleTitle
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        1
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let article = self.article
        return {
            BuildingDetailsNodes.ArticleCell(
                    thumbnailURL: article.articleThumbnailURL,
                    title: article.articleTitle,
                    body: article.articleBody
            )
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        ASSizeRange.fullWidth(collectionNode: collectionNode)
    }
}
