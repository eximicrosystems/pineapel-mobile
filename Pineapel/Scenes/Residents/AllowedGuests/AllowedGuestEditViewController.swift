//
// Created by Andrei Stoicescu on 15/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import Validator
import SVProgressHUD

protocol AllowedGuestEditViewControllerProtocol: UIViewController {
    func allowedGuestEditViewController(
            _ controller: AllowedGuestEditViewController,
            operation: AllowedGuestEditViewController.GuestOperation)
}

class AllowedGuestEditViewController: UITableViewController {
    weak var delegate: AllowedGuestEditViewControllerProtocol?

    enum GuestOperation {
        case create
        case update
    }

    enum Fields: String, CaseIterable {
        case infoLabel
        case name
        case phone
        case frequency
        case save
    }

    class AllowedGuestForm: AVSForm {
        var frequency: String?
        var name: String?
        var phone: String?
        var frequencyStart: Date?
        var frequencyEnd: Date?

        var frequencyItem = FormItem(placeholder: L10n.AllowedGuestEditScreen.Fields.Frequency.title)
        var nameItem = FormItem(placeholder: L10n.AllowedGuestEditScreen.Fields.Name.title)
        var phoneItem = FormItem(placeholder: L10n.AllowedGuestEditScreen.Fields.Phone.title)

        override init() {
            super.init()

            frequencyItem.value = frequency
            frequencyItem.uiProperties.cellType = .singleSelect
            frequencyItem.isMandatory = true
            frequencyItem.selectOptions = [
                FormItem.SelectOption(value: ResidentGuest.Frequency.once.rawValue, label: L10n.AllowedGuestEditScreen.Fields.Frequency.Options.AllowedOnce.title),
                FormItem.SelectOption(value: ResidentGuest.Frequency.week.rawValue, label: L10n.AllowedGuestEditScreen.Fields.Frequency.Options.OneWeek.title),
                FormItem.SelectOption(value: ResidentGuest.Frequency.month.rawValue, label: L10n.AllowedGuestEditScreen.Fields.Frequency.Options.OneMonth.title),
                FormItem.SelectOption(value: ResidentGuest.Frequency.always.rawValue, label: L10n.AllowedGuestEditScreen.Fields.Frequency.Options.Always.title),
            ]
            frequencyItem.valueCompletion = { [weak self] value in
                if self?.frequencyItem.value != value {
                    self?.frequencyStart = nil
                    self?.frequencyEnd = nil
                }

                self?.frequency = value
                self?.frequencyItem.value = value
            }
            frequencyItem.validation = { value in
                if value != ResidentGuest.Frequency.always.rawValue {
                    if self.frequencyStart == nil || self.frequencyEnd == nil {
                        return .invalid([AVSFormSingleValidationError(message: L10n.AllowedGuestEditScreen.Errors.frequencyIntervalMissing)])
                    }
                }

                return .valid
            }

            nameItem.value = self.name
            nameItem.isMandatory = true
            nameItem.valueCompletion = { [weak self, weak nameItem] value in
                self?.name = value
                nameItem?.value = value
            }

            phoneItem.value = self.phone
            phoneItem.isMandatory = false
            phoneItem.valueCompletion = { [weak self, weak phoneItem] value in
                self?.phone = value
                phoneItem?.value = value
            }

            formItems = [frequencyItem, nameItem, phoneItem]
        }

        func update(with residentGuest: ResidentGuest?) {
            if let guest = residentGuest {
                frequency = guest.frequency?.rawValue
                name = guest.name
                phone = guest.phoneNumber

                frequencyStart = residentGuest?.startDate
                frequencyEnd = residentGuest?.endDate

                frequencyItem.value = frequency
                nameItem.value = name
                phoneItem.value = phone
            }
        }

        func item(for fieldType: Fields) -> FormItem? {
            switch fieldType {
            case .frequency:
                return frequencyItem
            case .name:
                return nameItem
            case .phone:
                return phoneItem
            default:
                return nil
            }
        }
    }

    var residentGuest: ResidentGuest? {
        didSet {
            form.update(with: residentGuest)
            tableView.reloadData()
        }
    }

    fileprivate var form = AllowedGuestForm()

    init(residentGuest: ResidentGuest? = nil) {
        super.init(nibName: nil, bundle: nil)
        self.residentGuest = residentGuest
        form.update(with: residentGuest)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()

        view.backgroundColor = AVSColor.secondaryBackground.color
        navigationItem.title = L10n.AllowedGuestEditScreen.Navigation.title

        tableView.contentInset = .collectionDefaultInset

        AVSForm.FormItemCellType.registerCells(for: tableView)

        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        Fields.allCases.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let fieldType = Fields.allCases[indexPath.row]
        if let item = form.item(for: fieldType) {
            let cell = item.uiProperties.cellType.dequeueCell(for: tableView, at: indexPath)
            if let formUpdatableCell = cell as? AVSFormUpdatable {
                formUpdatableCell.update(with: item)
            }

            if let selectCell = cell as? AVSForm.FormCells.SingleSelectCell<SingleSelectView> {
                selectCell.formItemValueUpdate = { [weak self] formItem in

                    if let value = formItem.value, let frequency = ResidentGuest.Frequency(rawValue: value) {

                        var minimumStartDate = Date()
                        let now = Date()

                        if let fStart = self?.form.frequencyStart {
                            if fStart.isEarlier(than: now) {
                                minimumStartDate = fStart
                            }
                        }

                        let selectedStartDate = self?.form.frequencyStart ?? Date()
                        var selectedEndDate: Date? = self?.form.frequencyEnd

                        var selectionMode: CalendarDateRangePickerViewController.SelectionMode = .range
                        var maximumDaysSelection = 0

                        switch frequency {
                        case .once:
                            selectedEndDate = selectedStartDate
                            selectionMode = .single
                        case .week:
                            if selectedEndDate == nil {
                                selectedEndDate = 6.days.later
                            }
                            maximumDaysSelection = 6
                        case .month:
                            if selectedEndDate == nil {
                                selectedEndDate = 30.days.later
                            }
                            maximumDaysSelection = 30
                        case .always:
                            selectedEndDate = nil
                        }

                        if let selectedEndDate = selectedEndDate {
                            let dateRangePicker = CalendarDateRangePickerViewController(collectionViewLayout: UICollectionViewFlowLayout())
                            dateRangePicker.delegate = self
                            dateRangePicker.selectionMode = selectionMode
                            dateRangePicker.minimumDate = minimumStartDate
                            dateRangePicker.selectedStartDate = selectedStartDate
                            dateRangePicker.maximumDaysSelection = maximumDaysSelection
                            dateRangePicker.maximumDate = 1.years.later

                            if selectionMode == .range {
                                dateRangePicker.selectedEndDate = selectedEndDate
                            }

                            dateRangePicker.titleText = L10n.AllowedGuestEditScreen.Labels.selectInterval
                            let navigationController = AVSNavigationController(rootViewController: dateRangePicker)
                            navigationController.isModalInPresentation = true
                            self?.navigationController?.present(navigationController, animated: true)
                        }

                    }

                }
            }
            return cell
        }

        switch fieldType {
        case .save:
            let cell = tableView.dequeueReusableCell(withClass: AVSForm.FormCells.SaveCell.self, forIndexPath: indexPath)
            cell.title = L10n.AllowedGuestEditScreen.Buttons.Save.title
            return cell
        case .infoLabel:
            let cell = tableView.dequeueReusableCell(withClass: AVSForm.FormCells.InfoLabelCell.self, forIndexPath: indexPath)
            cell.title = L10n.AllowedGuestEditScreen.Labels.info
            return cell
        default:
            return UITableViewCell()
        }
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let fieldType = Fields.allCases[indexPath.row]
        if fieldType == .save {
            updateForm()
        }
    }

    func updateForm() {
        let formValidationResult = form.isValid()

        switch formValidationResult {

        case .success(_):
            ()
        case .failure(let error):
            Log.thisError(error)
            let message = error.errors.map {
                $0.message
            }.joined(separator: "\n")

            SVProgressHUD.showError(withStatus: message)
            return;
        }

        let apiService = ResidentService()

        let residentGuest = ResidentGuest(
                guestID: UUID().uuidString,
                frequency: ResidentGuest.Frequency(rawValue: form.frequency ?? ""),
                phoneNumber: form.phone,
                startDate: form.frequencyStart,
                endDate: form.frequencyEnd,
                name: form.name,
                apartment: nil)

        let residentGuestParams = ResidentGuestRequest(guest: residentGuest)

        if let guestID = self.residentGuest?.guestID {
            SVProgressHUD.show()
            apiService.updateResidentGuest(guestID: guestID, residentGuest: residentGuestParams).done { [weak self] guestResponse in
                if let self = self {
                    SVProgressHUD.showInfo(withStatus: L10n.AllowedGuestEditScreen.guestUpdated)
                    self.delegate?.allowedGuestEditViewController(self, operation: .update)
                }
            }.catch { error in
                SVProgressHUD.showError(withStatus: error.localizedDescription)
                Log.thisError(error)
            }
        } else {
            SVProgressHUD.show()
            apiService.createResidentGuest(residentGuestParams).done { [weak self] guestResponse in
                if let self = self {
                    SVProgressHUD.showInfo(withStatus: L10n.AllowedGuestEditScreen.guestUpdated)
                    self.delegate?.allowedGuestEditViewController(self, operation: .create)
                }
            }.catch { error in
                SVProgressHUD.showError(withStatus: error.localizedDescription)
                Log.thisError(error)
            }
        }
    }
}

extension AllowedGuestEditViewController: CalendarDateRangePickerViewControllerDelegate {
    func didCancelPickingDateRange() {
        navigationController?.dismiss(animated: true)
    }

    func didPickDateRange(startDate: Date!, endDate: Date!) {
        navigationController?.dismiss(animated: true)
        form.frequencyStart = startDate
        form.frequencyEnd = endDate
    }

    public func didPickSingleDate(date: Date) {
        navigationController?.dismiss(animated: true)
        form.frequencyStart = date
        form.frequencyEnd = date
    }

    func didSelectStartDate(startDate: Date!) {

    }

    func didSelectEndDate(endDate: Date!) {

    }
}