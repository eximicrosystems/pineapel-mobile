//
// Created by Andrei Stoicescu on 13/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit
import SwiftyUserDefaults
import AsyncDisplayKit
import SVProgressHUD
import Combine
import DifferenceKit

class AllowedGuestListViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {
    lazy var emptyNode = EmptyNode(emptyNodeType: .allowedGuests)

    lazy var refreshControl = UIRefreshControl()
    var collectionNode: ASCollectionNode

    var dataSource: ObservablePaginatedDataSource<ResidentGuest>
    var guests = [ResidentGuest]()
    private var subscriptions = Set<AnyCancellable>()

    override init() {
        collectionNode = .avsCollection()
        dataSource = .init()
        super.init(node: collectionNode)
        dataSource.paginate = { per, page in
            ResidentService().getResidentGuests(per: per, page: page)
        }

        dataSource.cacheable = true
        dataSource.cacheKey = "resident_allowed_guest_list"

        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = L10n.AllowedGuestListScreen.Navigation.title
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(tapCreateButton))

        setupBindings()
        replaceSystemBackButton()
        showEmptyScreen(false)
        collectionNode.view.backgroundView = emptyNode.view
        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)
        collectionNode.view.addSubview(refreshControl)
        refreshItems()
        emptyNode.actionButtonTapped = { [weak self] in
            self?.tapCreateButton()
        }
    }

    func reloadData() {
        let source = guests
        let target = dataSource.items

        let stagedChangeSet = StagedChangeset(source: source, target: target)
        for changeSet in stagedChangeSet {
            guests = changeSet.data
            collectionNode.performDiffBatchUpdates(changeSet: changeSet)
        }
    }

    func setupBindings() {
        dataSource.$items
                .receive(on: DispatchQueue.main)
                .sink { [weak self] items in
                    self?.reloadData()
                }.store(in: &subscriptions)

        dataSource.$hasNoItemsAfterLoading
                .receive(on: DispatchQueue.main)
                .sink { [weak self] hasNoItemsAfterLoading in
                    self?.showEmptyScreen(hasNoItemsAfterLoading)
                }.store(in: &subscriptions)

        dataSource.$showLoadingIndicator
                .receive(on: DispatchQueue.main)
                .sink { [weak self] loading in
                    guard let self = self else { return }

                    if loading {
                        if !self.refreshControl.isRefreshing {
                            SVProgressHUD.show()
                        }
                    } else {
                        SVProgressHUD.dismiss()
                        self.refreshControl.endRefreshing()
                    }
                }.store(in: &subscriptions)
    }

    @objc func refreshItems() {
        dataSource.loadMoreContentIfNeeded()
    }

    func showEmptyScreen(_ show: Bool) {
        emptyNode.isHidden = !show
    }

    @objc func tapCreateButton() {
        startEditGuest(guest: nil)
    }

    func collectionView(_ collectionView: ASCollectionView, willDisplayNodeForItemAt indexPath: IndexPath) {
        dataSource.loadMoreContentIfNeeded(currentItem: dataSource.items[indexPath.item])
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let guest = guests[indexPath.item]
        return {
            ConciergePagerListNodes.RequestCell(visitor: guest)
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        guests.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        let guest = guests[indexPath.item]

        let alertController = UIAlertController(title: guest.name, message: nil, preferredStyle: .actionSheet)

        alertController.addAction(UIAlertAction(title: L10n.AllowedGuestListScreen.Buttons.EditGuest.title, style: .default) { [weak self] _ in
            self?.startEditGuest(guest: guest)
        })

        alertController.addAction(UIAlertAction(title: L10n.AllowedGuestListScreen.Buttons.RemoveGuest.title, style: .destructive) { [weak self] _ in
            self?.confirmDelete(guest: guest)
        })

        alertController.addAction(UIAlertAction(title: L10n.General.cancel, style: .cancel, handler: nil))

        alertController.fixIpadIfNeeded(view: view)

        present(alertController, animated: true)
    }

    func confirmDelete(guest: ResidentGuest) {
        let message = L10n.AllowedGuestListScreen.Actions.RemoveGuest.title(guest.name ?? L10n.AllowedGuestListScreen.Actions.RemoveGuest.general)
        let deleteAlert = UIAlertController(title: nil, message: message, preferredStyle: .alert)

        deleteAlert.addAction(UIAlertAction(title: L10n.AllowedGuestListScreen.Actions.RemoveGuest.ok, style: .default, handler: { [weak self] action in
            guard let self = self else { return }
            guard let idx = self.dataSource.removeItem(guest) else { return }

            ResidentService().deleteResidentGuest(guestID: guest.guestID).catch { error in
                self.dataSource.insertItem(guest, at: idx)
                SVProgressHUD.showError(withStatus: error.localizedDescription)
            }
        }))

        deleteAlert.addAction(UIAlertAction(title: L10n.AllowedGuestListScreen.Actions.RemoveGuest.cancel, style: .cancel, handler: nil))

        present(deleteAlert, animated: true, completion: nil)
    }

    func startEditGuest(guest: ResidentGuest?) {
        let guestEditViewController = AllowedGuestEditViewController(residentGuest: guest)
        guestEditViewController.delegate = self
        navigationController?.pushViewController(guestEditViewController, animated: true)
    }

}

extension AllowedGuestListViewController: AllowedGuestEditViewControllerProtocol {
    func allowedGuestEditViewController(_ controller: AllowedGuestEditViewController, operation: AllowedGuestEditViewController.GuestOperation) {
        navigationController?.popToViewController(self, animated: true)
        refreshItems()
    }
}