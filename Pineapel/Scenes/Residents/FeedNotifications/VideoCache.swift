//
// Created by Andrei Stoicescu on 08.02.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import AVFoundation
import MobileCoreServices
import Disk

final class VideoCache {
    var videoData: Data
    var mutableVideoData: NSMutableData?
    var loading: Bool
    var loadedRanges: [NSRange] = []
    let URL: URL

    static var videoFolderName = "Video"

    static func clear() {
        if Disk.exists(videoFolderName, in: .caches) {
            try? Disk.remove(Self.videoFolderName, from: .caches)
        }
    }

    static func cachePath(forFilename filename: String) -> String? {
        try? Disk.url(for: "\(videoFolderName)/\(filename)", in: .caches).path
    }

    init(URL: Foundation.URL) {
        Log.this("initvideocache")
        videoData = Data()
        loading = true
        self.URL = URL
        loadCachedVideoIfPossible()
    }

    // MARK: - Data Adding

    func receivedContentLength(_ contentLength: Int) {
        if loading == false { return }
        if mutableVideoData != nil { return }
        mutableVideoData = NSMutableData(length: contentLength)
        videoData = mutableVideoData! as Data
    }

    func receivedData(_ data: Data, atRange range: NSRange) {
        guard let mutableVideoData = mutableVideoData else {
            Log.thisError("Received data without having mutable video data")
            return
        }

        mutableVideoData.replaceBytes(in: range, withBytes: (data as NSData).bytes)
        loadedRanges.append(range)

        consolidateLoadedRanges()

        if loadedRanges.count == 1 {
            let range = loadedRanges[0]
            if range.location == 0 && range.length == mutableVideoData.length {
                saveCachedVideo()
            }
        }
    }

    // MARK: - Save / Load Cache

    var videoCachePath: String? {
        Self.cachePath(forFilename: URL.lastPathComponent)
    }

    func createVideoDirectoryIfNeeded() {
        guard !Disk.exists(Self.videoFolderName, in: .caches) else { return }
        guard let url = try? Disk.url(for: Self.videoFolderName, in: .caches) else { return }

        var isDirectory: ObjCBool = false
        if !FileManager.default.fileExists(atPath: url.path, isDirectory: &isDirectory) {
            if !isDirectory.boolValue {
                try? FileManager.default.createDirectory(at: url, withIntermediateDirectories: true, attributes: nil)
            }
        }
    }

    func saveCachedVideo() {
        let fileManager = FileManager.default

        guard let videoCachePath = videoCachePath else {
            Log.thisError("Couldn't save cache file")
            return
        }

        createVideoDirectoryIfNeeded()

        guard fileManager.fileExists(atPath: videoCachePath) == false else {
            Log.this("Cache file \(videoCachePath) already exists.")
            return
        }

        loading = false
        if mutableVideoData == nil {
            Log.thisError("Missing video data for save.")
            return
        }

        do {
            try mutableVideoData!.write(toFile: videoCachePath, options: .atomicWrite)

            mutableVideoData = nil
            videoData.removeAll()
        } catch let error {
            Log.thisError("Couldn't write cache file: \(error)")
        }
    }

    func loadCachedVideoIfPossible() {
        let fileManager = FileManager.default

        guard let videoCachePath = videoCachePath else {
            Log.thisError("Couldn't load cache file.")
            return
        }

        if fileManager.fileExists(atPath: videoCachePath) == false {
            return
        }

        guard let videoData = try? Data(contentsOf: Foundation.URL(fileURLWithPath: videoCachePath)) else {
            Log.thisError("NSData failed to load cache file \(videoCachePath)")
            return
        }

        self.videoData = videoData
        loading = false
        Log.this("cached video file with length: \(self.videoData.count)")
    }

    // MARK: - Fulfilling cache

    func fulfillLoadingRequest(_ loadingRequest: AVAssetResourceLoadingRequest) -> Bool {
        guard let dataRequest = loadingRequest.dataRequest else {
            Log.thisError("Missing data request for \(loadingRequest)")
            return false
        }

        let requestedOffset = Int(dataRequest.requestedOffset)
        let requestedLength = Int(dataRequest.requestedLength)

        let data = videoData.subdata(in: requestedOffset ..< requestedOffset + requestedLength)

        DispatchQueue.main.async { () -> Void in
            self.fillInContentInformation(loadingRequest)

            dataRequest.respond(with: data)
            loadingRequest.finishLoading()
        }

        return true
    }

    func fillInContentInformation(_ loadingRequest: AVAssetResourceLoadingRequest) {

        guard let contentInformationRequest = loadingRequest.contentInformationRequest else { return }

        let contentType: String = kUTTypeQuickTimeMovie as String

        contentInformationRequest.isByteRangeAccessSupported = true
        contentInformationRequest.contentType = contentType
        contentInformationRequest.contentLength = Int64(videoData.count)
    }

    // MARK: - Cache Checking

    // Whether the video cache can fulfill this request
    func canFulfillLoadingRequest(_ loadingRequest: AVAssetResourceLoadingRequest) -> Bool {

        if !loading {
            return true
        }

        guard let dataRequest = loadingRequest.dataRequest else {
            Log.thisError("Missing data request for \(loadingRequest)")
            return false
        }

        let requestedOffset = Int(dataRequest.requestedOffset)
        let requestedLength = Int(dataRequest.requestedLength)
        let requestedEnd = requestedOffset + requestedLength

        for range in loadedRanges {
            let rangeStart = range.location
            let rangeEnd = range.location + range.length

            if requestedOffset >= rangeStart && requestedEnd <= rangeEnd {
                return true
            }
        }

        return false
    }

    // MARK: - Consolidating

    func consolidateLoadedRanges() {
        var consolidatedRanges: [NSRange] = []

        let sortedRanges = loadedRanges.sorted { $0.location < $1.location }

        var previousRange: NSRange?
        var lastIndex: Int?
        for range in sortedRanges {
            if let lastRange: NSRange = previousRange {
                let lastRangeEndOffset = lastRange.location + lastRange.length

                // check if range can be consumed by lastRange
                // or if they're at each other's edges if it can be merged

                if lastRangeEndOffset >= range.location {
                    let endOffset = range.location + range.length

                    // check if this range's end offset is larger than lastRange's
                    if endOffset > lastRangeEndOffset {
                        previousRange!.length = endOffset - lastRange.location

                        // replace lastRange in array with new value
                        consolidatedRanges.remove(at: lastIndex!)
                        consolidatedRanges.append(previousRange!)
                        continue
                    } else {
                        // skip adding this to the array, previous range is already bigger
                        //                        debugLog("skipping add of \(range), previous: \(previousRange)")
                        continue
                    }
                }
            }

            lastIndex = consolidatedRanges.count
            previousRange = range
            consolidatedRanges.append(range)
        }
        loadedRanges = consolidatedRanges
    }
}