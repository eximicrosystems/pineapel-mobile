//
// Created by Andrei Stoicescu on 22/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit

protocol UserNotificationsFeedCellProtocol: class {
    func feedCellTapped(icon: UserNotificationsNodes.FeedCell.Icon, item: FeedNotificationItem) -> Void
    func feedCellMenuItemTapped(item: FeedNotificationItem) -> Void
    func feedCellMediaTapped(cell: UserNotificationsNodes.FeedCell, item: FeedNotificationMediaFile, list: [FeedNotificationMediaFile]) -> Void
    func feedCellVisible(item: FeedNotificationItem) -> Void
}

protocol UserRequestListProtocol: class {
    func userRequestCellTapped(item: UserRequest) -> Void
}

class GradientView: UIView {
    override class var layerClass: AnyClass {
        CAGradientLayer.self
    }
}

fileprivate extension ASCollectionNode {
    func addGradientMask() {
        let coverView = GradientView(frame: bounds)
        let coverLayer = coverView.layer as! CAGradientLayer
        coverLayer.colors = [UIColor.white.cgColor, UIColor.white.cgColor, UIColor.white.withAlphaComponent(0).cgColor]
        coverLayer.locations = [0.0, 0.9, 1.0]
        coverLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        coverLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        view.mask = coverView
    }
}

struct UserNotificationsNodes {

    class ConciergeBar: ASCellNode, UserRequestListProtocol {
        var concierges = [User]() {
            didSet {
                conciergeListNode.users = concierges

                if let text = labelText() {
                    labelTextNode = .init(with: text)
                } else {
                    labelTextNode = nil
                }

                setNeedsLayout()
            }
        }

        var requests = [CategoryUserRequest]() {
            didSet { requestsNode.requests = requests }
        }

        var conciergeListNode = ConciergeList()
        var requestsNode: UserRequestListCell
        var labelTextNode: ASTextNode2?

        var didTapConcierge: ((User) -> Void)?
        var didTapRequest: ((UserRequest) -> Void)?

        init(concierges: [User], requests: [CategoryUserRequest]) {
            self.concierges = concierges
            conciergeListNode.users = concierges
            requestsNode = .init(requests: requests)
            super.init()

            if concierges.count > 0 {
                labelTextNode = .init(with: labelText())
            }

            requestsNode.delegate = self
            conciergeListNode.selectedConcierge = { [weak self] user in
                self?.didTapConcierge?(user)
            }
            automaticallyManagesSubnodes = true
        }

        private func labelText() -> NSAttributedString? {
            guard concierges.count > 0 else { return nil }
            guard let separator = " & ".attributed(.note, .secondaryLabel) else { return nil }

            let userAttributedStrings = concierges.compactMap { user in
                user.firstName?.attributed(.note, .primaryLabel)
            }.joined(with: separator)

            return "\(L10n.UserNotifications.Labels.onDuty): ".attributed(.note, .secondaryLabel)?.appendingAttributedString(userAttributedStrings)
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            [
                [
                    requestsNode.growing(),
                    conciergeListNode
                ].hStacked().spaced(15),

                labelTextNode?.alignSelf(.end)
            ].vStacked(spacing: 5)
             .margin(bottom: 10, right: 10)
        }

        func userRequestCellTapped(item: UserRequest) {
            didTapRequest?(item)
        }
    }

    class ConciergeList: ASDisplayNode {
        var users: [User] = [] {
            didSet {
                configure(with: users)
                setNeedsLayout()
            }
        }

        var userNodes = [ASDisplayNode]()

        var selectedConcierge: ((User) -> Void)?

        override init() {
            super.init()
            automaticallyManagesSubnodes = true
        }

        @objc func selectedConciergeAction(node: ASNetworkImageNode) {
            guard let idx = userNodes.firstIndex(of: node) else { return }
            guard let user = users[safe: idx] else { return }
            Log.this("tapped concierge \(user)")
            selectedConcierge?(user)
        }

        private func configure(with users: [User]) {
            userNodes = users.map { user in
                let node = ConciergeGenericNodes.AvatarNode(url: user.userPictureURL, imageSize: 44, border: 2)
                node.isUserInteractionEnabled = true
                node.addTarget(self, action: #selector(selectedConciergeAction), forControlEvents: .touchUpInside)
                return node
            }
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            userNodes.map { $0 }.hStacked(spacing: 10)
        }

    }

    class UserRequestListCell: ASCellNode, ASCollectionDataSource, ASCollectionDelegate {
        static let requestCellSize: CGFloat = 40

        let collectionNode: ASCollectionNode

        var requests: [CategoryUserRequest] {
            didSet {
                DispatchQueue.main.async {
                    self.collectionNode.reloadData()
                }
            }
        }
        weak var delegate: UserRequestListProtocol?

        init(requests: [CategoryUserRequest]) {
            let flowLayout = UICollectionViewFlowLayout()
            flowLayout.scrollDirection = .horizontal
            flowLayout.minimumInteritemSpacing = 10

            collectionNode = ASCollectionNode(collectionViewLayout: flowLayout)
            collectionNode.contentInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
            collectionNode.showsHorizontalScrollIndicator = false

            self.requests = requests
            super.init()

            collectionNode.onDidLoad { node in
                self.collectionNode.addGradientMask()
            }

            collectionNode.delegate = self
            collectionNode.dataSource = self
            collectionNode.backgroundColor = .clear
            collectionNode.alwaysBounceHorizontal = true
            backgroundColor = .clear
            automaticallyManagesSubnodes = true
        }

        func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
            requests.count
        }

        func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
            let categoryRequest = requests[indexPath.item]
            return {
                let image = categoryRequest.category?.icon?.withRenderingMode(.alwaysTemplate) ?? Asset.Images.Concierge.icPackageSm.image.withRenderingMode(.alwaysTemplate)
//                return .withNode(ConciergeGenericNodes.MenuCellIcon(icon: .backgroundIcon(image, categoryRequest.request.status?.color ?? UserRequest.Status.pending.color, AVSColor.accentForeground.color)))
                return .withNode(ConciergeGenericNodes.MenuCellIcon(icon: .backgroundIcon(image, categoryRequest.request.status?.color ?? UserRequest.Status.pending.color, AVSColor.colorMilitaryGreen.color)))
            }
        }

        func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
            ASSizeRangeMake(CGSize(width: UserRequestListCell.requestCellSize, height: UserRequestListCell.requestCellSize))
        }

        func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
            delegate?.userRequestCellTapped(item: requests[indexPath.item].request)
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            collectionNode.height(.points(UserRequestListCell.requestCellSize)).wrapped()
        }

        func scrollViewDidScroll(_ scrollView: UIScrollView) {
            scrollView.mask?.frame = scrollView.bounds
        }
    }

    class Navigation: ASDisplayNode {
        class Badge: ASDisplayNode {
            var label = ASTextNode2()

            var count: Int = 0 {
                didSet {
//                    label.attributedText = String(count).attributed(.noteMedium, .accentForeground, alignment: .center)
                    label.attributedText = String(count).attributed(.noteMedium, .colorMilitaryGreen, alignment: .center)
                    isHidden = count <= 0
                    setNeedsLayout()
                }
            }

            override init() {
                super.init()
                cornerRadius = 10
                backgroundColor = AVSColor.accentBackground.color
                isHidden = true
                isUserInteractionEnabled = false
                automaticallyManagesSubnodes = true
            }

            override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
                label.margin(top: 1, left: 6, bottom: 1, right: 6)
            }
        }

        var buildingName: String? {
            didSet {
                if buildingName != oldValue {
                    buildingNameNode.attributedText = buildingName?.attributed(.note, .secondaryLabel)
                    setNeedsLayout()
                }
            }
        }

        var buildingNameVisible = true {
            didSet {
                setNeedsLayout()
            }
        }

        var didTapNotificationsButton: (() -> Void)?

        var backgroundImageNode: ASDisplayNode
        var logoImageNode: ASImageNode
        var buildingNameNode = ASTextNode2()
        var notificationsButton = ASButtonNode()
        var notificationsBadge = Badge()

        var appNameLogoText = ASTextNode2()
        var appSubtitleLogoText = ASTextNode2()
        
        override init() {
            backgroundImageNode = ASDisplayNode()
            backgroundImageNode.backgroundColor = .clear

            logoImageNode = ASImageNode(image: Asset.Images.frame.image)
            logoImageNode.contentMode = .scaleAspectFit

            buildingName = ""

            appNameLogoText.attributedText = "              ".attributed(.H2, .accentBackground)
            appSubtitleLogoText.attributedText = "".attributed(.bodyTitleHeavy, .accentBackground)

            notificationsButton.setImage(Asset.Images.Concierge.icMore.image.withRenderingMode(.alwaysTemplate), for: .normal)
            notificationsButton.tintColor = AVSColor.accentBackground.color
            
            super.init()
            backgroundColor = .clear
            notificationsButton.addTarget(self, action: #selector(tapNotificationsButton), forControlEvents: .touchUpInside)
            automaticallyManagesSubnodes = true
            automaticallyRelayoutOnSafeAreaChanges = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            [
                
                [
                    appNameLogoText,
                    logoImageNode.relative(.center)
                ].hStacked(spacing: 10).aligned(.end),

//                [
//                    notificationsButton,
//                    notificationsBadge.relative(.topRight).margin(top: -5, right: -5)
//                ].wrapped()
//                 .margin(top: 5, right: 5)

            ].hStacked().justified(.spaceBetween).aligned(.start)
             .margin(top: safeAreaInsets.top + 5, left: 15, bottom: 10, right: 15)
             .background(backgroundImageNode)

        }

        @objc func tapNotificationsButton() {
            didTapNotificationsButton?()
        }

        func makeTransparent(_ transparent: Bool) {
            UIView.animate(withDuration: 0.2) {
                if transparent {
                    self.buildingNameVisible = true
                    self.backgroundImageNode.backgroundColor = AVSColor.colorMilitaryGreen.color
                    self.backgroundImageNode.shadowOffset = CGSize(width: 0, height: 0)
                    self.backgroundImageNode.shadowRadius = 0
                    self.backgroundImageNode.shadowColor = UIColor.clear.cgColor
                    self.backgroundImageNode.shadowOpacity = 0
                } else {
                    self.buildingNameVisible = false
                    self.backgroundImageNode.backgroundColor = AVSColor.tertiaryBackground.color
                    self.backgroundImageNode.shadowOffset = CGSize(width: 0, height: 0)
                    self.backgroundImageNode.shadowRadius = 4
                    self.backgroundImageNode.shadowColor = UIColor.systemGray.cgColor
                    self.backgroundImageNode.shadowOpacity = 0.2
                }
            }
        }
    }

    class UserRequestIcon: ASDisplayNode {
        var iconImage: ASImageNode
        var circleNode = ASDisplayNode()

        let iconSize: CGFloat
        let request: UserRequest

        init(request: UserRequest, size: CGFloat) {
            iconSize = size
            self.request = request

            if String.isBlank(request.notificationID) {
                iconImage = ASImageNode(image: Asset.Images.icCheckmarkNoCircle.image.withRenderingMode(.alwaysTemplate))
            } else {
                iconImage = ASImageNode(image: Asset.Images.icDeliver.image.withRenderingMode(.alwaysTemplate))
            }

            iconImage.tintColor = AVSColor.primaryIcon.color

            circleNode.borderColor = request.status?.color.cgColor
            circleNode.borderWidth = 1
            circleNode.backgroundColor = .white
            circleNode.cornerRadius = iconSize / 2

            super.init()
            backgroundColor = .clear
            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            let centeredIcon = ASCenterLayoutSpec(centeringOptions: .XY, sizingOptions: .minimumXY, child: iconImage)
            circleNode.style.preferredSize = CGSize(width: iconSize, height: iconSize)
            return ASOverlayLayoutSpec(child: circleNode, overlay: centeredIcon)
        }
    }

    class FeedCellMediaImage: ASCellNode, ASNetworkImageNodeDelegate {
        var imageNode: ASNetworkImageNode
        var tappedMedia: (() -> Void)?
        var addedShimmer = false
        var requiresNetworkLoad = false

        init(mediaFile: FeedNotificationMediaFile) {
            imageNode = .init()
            imageNode.url = mediaFile.mediaURL
            imageNode.contentMode = .scaleAspectFill
            imageNode.cornerRadius = 5
            imageNode.backgroundColor = AVSColor.primaryBackground.color
            super.init()
            imageNode.delegate = self
            imageNode.addTarget(self, action: #selector(_tappedImage), forControlEvents: .touchUpInside)
            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            imageNode.margin(horizontal: 15)
        }

        @objc private func _tappedImage() {
            tappedMedia?()
        }

        override func layout() {
            super.layout()
            if !addedShimmer && requiresNetworkLoad {
                addedShimmer = true
                if imageNode.image == nil {
                    addShimmerToNode(imageNode)
                }
            }
        }

        func imageNode(_ imageNode: ASNetworkImageNode, didLoad image: UIImage) {
            stopShimmeringOnNode(imageNode)
        }

        func imageNodeWillLoadImage(fromNetwork imageNode: ASNetworkImageNode) {
            requiresNetworkLoad = true
        }

        func imageNodeDidFailToLoadImage(fromCache imageNode: ASNetworkImageNode) {
            stopShimmeringOnNode(imageNode)
        }

        func imageNodeDidLoadImage(fromCache imageNode: ASNetworkImageNode) {
            stopShimmeringOnNode(imageNode)
        }

        func imageNode(_ imageNode: ASNetworkImageNode, didFailWithError error: Error) {
            stopShimmeringOnNode(imageNode)
        }
    }

    class FeedCellMediaVideo: ASCellNode, ASVideoNodeDelegate {
        var videoNode: ASVideoNode
        var tappedMedia: (() -> Void)?
        var receivedResolution: ((CGSize) -> Void)?
        var fetchResolution = false
        var muteButton = ASButtonNode()
        var playing = false {
            didSet {
                if playing {
                    videoNode.play()
                } else {
                    videoNode.pause()
                }
            }
        }

        var addedShimmer = false

        var videoNodeWrapper: ASDisplayNode

        init(mediaFile: FeedNotificationMediaFile, fetchResolution: Bool) {
            self.fetchResolution = fetchResolution
            videoNode = .init()
            videoNode.gravity = AVLayerVideoGravity.resizeAspectFill.rawValue
            videoNode.contentMode = .scaleAspectFill
            videoNode.shouldAutoplay = false
            videoNode.shouldAutorepeat = true
            videoNode.muted = true
            videoNode.backgroundColor = AVSColor.tertiaryBackground.color
            videoNode.cornerRadius = 5

            videoNodeWrapper = .init()
            videoNodeWrapper.automaticallyManagesSubnodes = true

            super.init()

            muteButton.tintColor = AVSColor.primaryIcon.color
            muteButton.setImage(muteIcon, for: .normal)
            muteButton.addTarget(self, action: #selector(tappedMute), forControlEvents: .touchUpInside)

            videoNode.delegate = self
            videoNode.onDidLoad { [weak self] node in
                guard let self = self else { return }
                self.videoNode.asset = cachedOrCachingAsset(mediaFile.mediaURL)

                if self.playing {
                    self.videoNode.play()
                }

                if self.fetchResolution {
                    FeedVideoResolutionManager.shared.fetchResolution(asset: self.videoNode.asset, id: mediaFile.targetID).done { [weak self] resolution in
                        self?.receivedResolution?(resolution.size)
                    }.cauterize()
                }
            }

            videoNodeWrapper.layoutSpecBlock = { node, range in
                self.muteButton.margin(10)
                               .relative(.bottomRight)
                               .overlay(self.videoNode)
            }

            automaticallyManagesSubnodes = true
        }

        var muteIcon: UIImage {
            if videoNode.muted {
                return Asset.Images.Concierge.icVolumeOff.image.withRenderingMode(.alwaysTemplate)
            } else {
                return Asset.Images.Concierge.icVolumeUp.image.withRenderingMode(.alwaysTemplate)
            }
        }

        @objc func tappedMute() {
            videoNode.muted = !videoNode.muted
            muteButton.setImage(muteIcon, for: .normal)
        }

        func didTap(_ videoNode: ASVideoNode) {
            if videoNode.isPlaying() {
                videoNode.pause()
            }
            tappedMedia?()
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            videoNodeWrapper.margin(horizontal: 15)
        }

        func videoNode(_ videoNode: ASVideoNode, willChange state: ASVideoNodePlayerState, to toState: ASVideoNodePlayerState) {
            switch toState {
            case .playing, .readyToPlay:
                stopShimmeringOnNode(videoNodeWrapper)
                    // If not using the videoNodeWrapper, this is required:
                    // view.bringSubviewToFront(muteButton.view)
            default:
                ()
            }
        }

        override func layout() {
            super.layout()
            if !addedShimmer {
                addedShimmer = true
                addShimmerToNode(videoNodeWrapper)
            }
        }
    }

    class FeedCellMedia: ASDisplayNode, ASPagerDataSource, ASPagerDelegate {

        var mediaFiles: [FeedNotificationMediaFile]
        var collectionNode: ASPagerNode
        var ratio: CGFloat = 1 {
            didSet {
                if ratio > 1 {
                    ratio = 1
                }
            }
        }
        var tappedMedia: ((FeedNotificationMediaFile) -> Void)?
        var pageControlNode: ASDisplayNode?
        var pageControl: UIPageControl?
        var playing = false {
            didSet {
                guard let node = currentVideoNode else { return }
                node.playing = playing
            }
        }

        init(mediaFiles: [FeedNotificationMediaFile]) {
            self.mediaFiles = mediaFiles
            if let file = mediaFiles.first {
                if let ratio = file.ratio {
                    self.ratio = CGFloat(1 / ratio)
                } else if let resolution = FeedVideoResolutionManager.shared.resolution(file.targetID) {
                    ratio = resolution.size.height / resolution.size.width
                }
            }

            ratio = min(ratio, 1)

            collectionNode = .init()
            super.init()

            collectionNode.setDataSource(self)
            collectionNode.setDelegate(self)
            collectionNode.backgroundColor = AVSColor.secondaryBackground.color

            if mediaFiles.count > 1 {
                pageControlNode = ASDisplayNode { [weak self] () -> UIView in
                    let pageControl = UIPageControl()
                    pageControl.pageIndicatorTintColor = AVSColor.pageIndicatorTintColor.color
                    pageControl.currentPageIndicatorTintColor = AVSColor.currentPageIndicatorTintColor.color
                    pageControl.numberOfPages = mediaFiles.count
                    pageControl.isUserInteractionEnabled = false
                    self?.pageControl = pageControl
                    return pageControl
                }
            }

            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            if let pageControlNode = pageControlNode {
                pageControlNode.style.preferredSize = CGSize(width: constrainedSize.max.width, height: 10)
                let pagerNode = [ASDisplayNode.spacer(), pageControlNode].vStacked().aligned(.center).margin(bottom: 10)
                return pagerNode.overlay(collectionNode.ratio(ratio))
            }

            return collectionNode.ratio(ratio)
        }

        func numberOfPages(in pagerNode: ASPagerNode) -> Int {
            mediaFiles.count
        }

        func pagerNode(_ pagerNode: ASPagerNode, nodeBlockAt index: Int) -> ASCellNodeBlock {
            let item = mediaFiles[index]
            return {
                switch item.type {
                case .image:
                    let cell = FeedCellMediaImage(mediaFile: item)
                    cell.tappedMedia = { [weak self] in
                        self?.tappedMedia?(item)
                    }
                    return cell
                case .video:
                    let shouldFetchResolution = index == 0 && AppState.firebaseFlags.autoVideoResolution
                    let cell = FeedCellMediaVideo(mediaFile: item, fetchResolution: shouldFetchResolution)
                    cell.tappedMedia = { [weak self] in
                        self?.tappedMedia?(item)
                    }

                    if self.playing {
                        cell.playing = true
                    }

                    cell.receivedResolution = { [weak self] size in
                        guard let self = self else { return }
                        let ratio = size.height / size.width
                        if self.ratio != ratio {
                            self.ratio = ratio
                            self.setNeedsLayout()
                        }
                    }

                    return cell
                }
            }
        }

        func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
            ASSizeRangeMake(collectionNode.frame.size)
        }

        func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
            pageControl?.currentPage = collectionNode.currentPageIndex
        }

        func collectionNode(_ collectionNode: ASCollectionNode, willDisplayItemWith node: ASCellNode) {
            // This also happens for partial parent cells that render first time
            guard let cell = node as? FeedCellMediaVideo else { return }
            if playing {
                cell.playing = true
            }
        }

        func collectionNode(_ collectionNode: ASCollectionNode, didEndDisplayingItemWith node: ASCellNode) {
            guard let cell = node as? FeedCellMediaVideo else { return }
            cell.playing = false
        }

        func playVideo() {
            Log.this("play video true")
            playing = true
        }

        func stopVideo() {
            playing = false
        }

        var currentVideoNode: FeedCellMediaVideo? {
            collectionNode.nodeForPage(at: collectionNode.currentPageIndex) as? FeedCellMediaVideo
        }

        var currentItemIsVideo: Bool {
            let idx = max(collectionNode.currentPageIndex, 0)
            return mediaFiles.count > 0 && mediaFiles[idx].type == .video
        }
    }

    class FeedCell: ASCellNode {
        static let avatarSize = CGSize(width: 40, height: 40)

        class FeedCellImageNode: ASImageNode {
            var icon: Icon?
        }

        enum Icon {
            case chat
            case delivery
            case readMore

            var image: UIImage {
                switch self {
                case .chat:
                    return Asset.Images.Concierge.icChatBubbles.image
                case .delivery:
                    return Asset.Images.Concierge.icPackageSm.image
                case .readMore:
                    return Asset.Images.Concierge.icReadMore.image
                }
            }
        }

        var item: FeedNotificationItem

        weak var delegate: UserNotificationsFeedCellProtocol?

        var headerAvatar = ASNetworkImageNode()
        var headerName = ASTextNode2()
        var headerTimestamp = ASTextNode2()
        var headerMoreButton = ASButtonNode()
        var mediaNode: FeedCellMedia

        var iconNodes: [ASDisplayNode] = []
        var icons: [Icon] = [] {
            didSet {
                mapIconNodes()
                setNeedsLayout()
            }
        }

        var infoLabel = ASTextNode2()
        var bodyLabel = ASTextNode2()
        var backgroundNode = ASDisplayNode()
        var readonly: Bool

        init(item: FeedNotificationItem, readonly: Bool = false) {
            self.item = item
            self.readonly = readonly

            headerAvatar.url = item.author?.userPictureURL
            headerAvatar.contentMode = .scaleAspectFit
            headerAvatar.imageModificationBlock = ASImageNodeRoundBorderModificationBlock(0, nil)

            headerName.attributedText = item.author?.firstName?.attributed(.bodyHeavy, .primaryLabel)
            headerTimestamp.attributedText = item.createdAt?.avsTimeAgo.attributed(.note, .secondaryLabel)

            let icon = Asset.Images.icDotsHorizontal.image.withRenderingMode(.alwaysTemplate)
            headerMoreButton.tintColor = AVSColor.primaryIcon.color
            headerMoreButton.setImage(icon, for: .normal)
            headerMoreButton.hitTestSlop = UIEdgeInsets(top: -20, left: -20, bottom: -20, right: -20)

            bodyLabel.attributedText = item.bodyText?.attributed(.body, .primaryLabel)

            backgroundNode.backgroundColor = AVSColor.secondaryBackground.color

            infoLabel.maximumNumberOfLines = 0

            mediaNode = FeedCellMedia(mediaFiles: item.mediaFile)

            super.init()

            mediaNode.tappedMedia = { [weak self] media in
                guard let self = self else { return }
                self.delegate?.feedCellMediaTapped(cell: self, item: media, list: self.item.mediaFile)
            }

            headerMoreButton.addTarget(self, action: #selector(tappedMore), forControlEvents: .touchUpInside)

            switch item.notificationType {
            case .delivery, .packages:
                icons = [.delivery, .chat]
            default:
                icons = [.chat]
            }
            infoLabel.attributedText = item.notificationType.attributedLabel(for: item)

            if item.internalLink != nil || item.externalLink != nil {
                icons.append(.readMore)
            }

            mapIconNodes()

            automaticallyManagesSubnodes = true
        }

        func mapIconNodes() {
            let iconNodes = icons.map { icon -> FeedCellImageNode in
                let imageNode = FeedCellImageNode(image: icon.image, tintColor: AVSColor.primaryIcon.color)
                imageNode.icon = icon
                imageNode.addTarget(self, action: #selector(tappedIcon), forControlEvents: .touchUpInside)
                imageNode.hitTestSlop = UIEdgeInsets(top: -10, left: -10, bottom: -10, right: -10)
                return imageNode
            }
            self.iconNodes = iconNodes
        }

        override func cellNodeVisibilityEvent(_ event: ASCellNodeVisibilityEvent, in scrollView: UIScrollView?, withCellFrame cellFrame: CGRect) {
            switch event {
            case .visible:
                delegate?.feedCellVisible(item: self.item)
            default:
                ()
            }
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            let headerLabelsStack = ASStackLayoutSpec(
                    direction: .vertical,
                    spacing: 0,
                    justifyContent: .start,
                    alignItems: .start,
                    children: [headerName, headerTimestamp]
            )

            headerLabelsStack.style.flexGrow = 1

            headerAvatar.style.preferredSize = FeedCell.avatarSize

            let headerStack = ASStackLayoutSpec(
                    direction: .horizontal,
                    spacing: 10,
                    justifyContent: .start,
                    alignItems: .center,
                    children: readonly ? [headerAvatar, headerLabelsStack] : [headerAvatar, headerLabelsStack, headerMoreButton]
            )

            let actionButtonsStack = ASStackLayoutSpec(
                    direction: .horizontal,
                    spacing: 20,
                    justifyContent: .start,
                    alignItems: .center,
                    children: iconNodes
            )

            infoLabel.style.flexShrink = 1
            let allActionsStack = ASStackLayoutSpec(
                    direction: .horizontal,
                    spacing: 10,
                    justifyContent: .spaceBetween,
                    alignItems: .start,
                    children: [actionButtonsStack, infoLabel]
            )

            let insets = UIEdgeInsets(top: 10, left: 15, bottom: 10, right: 15)

            var allItems: [ASLayoutElement] = [
                headerStack.margin(insets),
                mediaNode,
                allActionsStack.margin(insets),
            ]

            if !String.isBlank(bodyLabel.attributedText?.string) {
                allItems.append(bodyLabel.margin(insets))
            }

            let allItemsStack = ASStackLayoutSpec(
                    direction: .vertical,
                    spacing: 0,
                    justifyContent: .start,
                    alignItems: .stretch,
                    children: allItems
            )

            return allItemsStack
                    .background(backgroundNode)
                    .margin(top: 0, left: 0, bottom: 20, right: 0)
        }

        @objc func tappedIcon(sender: FeedCellImageNode) {
            if let icon = sender.icon {
                delegate?.feedCellTapped(icon: icon, item: self.item)
            }
        }

        @objc func tappedMore() {
            delegate?.feedCellMenuItemTapped(item: self.item)
        }
    }

}

fileprivate extension FeedNotificationItem.NotificationType {

    struct FeedCellInfoLabel {
        let name: String?
        let value: String?
    }

    func attributedLabel(for item: FeedNotificationItem) -> NSAttributedString? {
        var labels: [FeedCellInfoLabel]
        let l = L10n.UserNotifications.Labels.self
        switch self {
        case .info:
            labels = [
                FeedCellInfoLabel(name: l.category, value: l.information),
            ]
        case .mailDelivery:
            labels = []
        case .packages:
            labels = [
                FeedCellInfoLabel(name: l.deliveryTime, value: item.deliveryTime?.avsTimeAgo),
                FeedCellInfoLabel(name: l.number, value: item.number),
                FeedCellInfoLabel(name: l.signedBy, value: item.signedBy?.firstName),
            ]
        case .fitness:
            labels = [
                FeedCellInfoLabel(name: l.duration, value: item.duration),
                FeedCellInfoLabel(name: l.instructor, value: item.instructor),
                FeedCellInfoLabel(name: l.startDate, value: item.startDate?.avsTimeAgo),
                FeedCellInfoLabel(name: l.type, value: item.type),
            ]
        case .delivery:
            labels = [
                FeedCellInfoLabel(name: l.deliveryTime, value: item.deliveryTime?.avsTimeAgo),
                FeedCellInfoLabel(name: l.itemForDelivery, value: item.itemForDelivery),
            ]
        case .event:
            labels = [
                FeedCellInfoLabel(name: l.eventName, value: item.eventName),
                FeedCellInfoLabel(name: l.eventDate, value: item.startDate?.avsTimeAgo),
                FeedCellInfoLabel(name: l.eventHost, value: item.eventHost)
            ]
        }

        guard let separator = NSAttributedString.string(text: " ", font: .Note) else {
            return nil
        }

        return labels.compactMap {
            infoAttributedString(label: $0.name, value: $0.value)
        }.joined(with: separator)
    }

    private func infoAttributedString(label: String?, value: String?) -> NSAttributedString? {
        guard let label = label, let value = value else { return nil }
        guard let lAttributed = "\(label): ".attributed(.note, .tertiaryLabel) else { return nil }
        guard let vAttributed = value.attributed(.note, .primaryLabel) else { return nil }

        let finalString = NSMutableAttributedString(attributedString: lAttributed)
        finalString.append(vAttributed)

        return finalString
    }
}
