//
// Created by Andrei Stoicescu on 26/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD

protocol RequestEditViewControllerProtocol: UIViewController {
    func requestEditViewControllerDidFinish(_ controller: UserRequestEditViewController)
}

class UserRequestEditViewController: UITableViewController {
    weak var delegate: RequestEditViewControllerProtocol?

    enum Sections: Int, CaseIterable {
        case formItems
        case saveButton
    }

    class RequestForm: AVSForm {
        var name: String?
        var comment: String?

        var nameItem = FormItem(placeholder: L10n.UserRequestEditScreen.Fields.Name.placeholder)
        var commentItem = FormItem(placeholder: L10n.UserRequestEditScreen.Fields.Comment.placeholder)

        override init() {
            super.init()

            nameItem.value = self.name
            nameItem.uiProperties.cellType = .textField
            nameItem.isMandatory = true

            nameItem.valueCompletion = { [weak self] value in
                self?.name = value
                self?.nameItem.value = value
            }

            commentItem.value = self.comment
            commentItem.title = L10n.UserRequestEditScreen.Fields.Comment.title
            commentItem.isMandatory = true
            commentItem.uiProperties.cellType = .textArea
            commentItem.valueCompletion = { [weak self] value in
                self?.comment = value
                self?.commentItem.value = value
            }

            self.formItems = [nameItem, commentItem]
        }
    }

    fileprivate var form = RequestForm()

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()

        view.backgroundColor = AVSColor.primaryBackground.color
        navigationItem.title = "Add Request"

        tableView.contentInset = .collectionDefaultInset

        AVSForm.FormItemCellType.registerCells(for: tableView)

        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        Sections.allCases.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == Sections.formItems.rawValue {
            return form.formItems.count
        } else if section == Sections.saveButton.rawValue {
            return 1
        }
        return 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == Sections.formItems.rawValue {
            let item = form.formItems[indexPath.item]
            let cell = item.uiProperties.cellType.dequeueCell(for: tableView, at: indexPath)
            if let formUpdatableCell = cell as? AVSFormUpdatable {
                formUpdatableCell.update(with: item)
            }
            return cell
        } else if indexPath.section == Sections.saveButton.rawValue {
            let cell = tableView.dequeueReusableCell(withClass: AVSForm.FormCells.SaveCell.self, forIndexPath: indexPath)
            cell.title = L10n.UserRequestEditScreen.Buttons.Submit.title
            return cell
        }

        return UITableViewCell()
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == Sections.saveButton.rawValue {
            updateForm()
        }
    }

    func updateForm() {
        let formValidationResult = form.isValid()

        switch formValidationResult {
        case .success(_):
            ()
        case .failure(let error):
            Log.thisError(error)
            let message = error.errors.map {
                $0.message
            }.joined(separator: "\n")

            SVProgressHUD.showError(withStatus: message)
            return;
        }

        let apiService = ResidentService()
        let request = UserRequestParams(dueDate: Date(), body: form.comment, name: form.name, type: .generic, notificationID: nil)

        SVProgressHUD.show()
        apiService.createUserRequest(params: request).done { [weak self] in
            if let self = self {
                SVProgressHUD.showInfo(withStatus: L10n.UserRequestEditScreen.Labels.requestCreated)
                self.delegate?.requestEditViewControllerDidFinish(self)
            }
        }.catch { error in
            SVProgressHUD.showError(withStatus: error.localizedDescription)
            Log.thisError(error)
        }
    }
}