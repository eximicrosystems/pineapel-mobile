//
// Created by Andrei Stoicescu on 17/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import Stripe
import SVProgressHUD
import AsyncDisplayKit

class AmenityCheckoutData {
    var reservation: AmenityReservation
    var amenity: Amenity
    var paymentMethod: PaymentMethodSelection?
    var cart: CartProductManager

    init(reservation: AmenityReservation, amenity: Amenity, cart: CartProductManager) {
        self.reservation = reservation
        self.amenity = amenity
        self.cart = cart
    }
}

class AmenityPaymentViewController: PaymentMethodListViewController {
    var navigator: AmenityReservationNavigator

    init(navigator: AmenityReservationNavigator) {
        self.navigator = navigator
        super.init()
    }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    override func paymentMethodSelected(method: PaymentMethodSelection) {
        navigator.paymentSelectionDidFinish(method)
    }
}
