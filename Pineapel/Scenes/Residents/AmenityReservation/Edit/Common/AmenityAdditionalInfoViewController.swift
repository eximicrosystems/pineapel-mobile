//
// Created by Andrei Stoicescu on 06/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit

struct FormNodes {
    static var margin = UIEdgeInsets(top: 0, left: 15, bottom: 15, right: 15)

    class SectionLabel: ASCellNode {
        var labelNode = ASTextNode2()

        init(label: String) {
            labelNode.attributedText = label.attributed(.bodyTitleHeavy, .primaryLabel)
            labelNode.maximumNumberOfLines = 1
            super.init()
            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            labelNode.margin(top: 15, left: 15, bottom: 15, right: 15)
        }
    }

    class TextArea: ASCellNode, ASEditableTextNodeDelegate {
        enum Mode {
            case textField
            case textArea
        }

        var labelNode: ASTextNode2?
        var textArea = ASEditableTextNode()

        var didChangeValue: ((String?) -> Void)?
        var mode: Mode

        override func asyncTraitCollectionDidChange(withPreviousTraitCollection previousTraitCollection: ASPrimitiveTraitCollection) {
            super.asyncTraitCollectionDidChange(withPreviousTraitCollection: previousTraitCollection)
            if asyncTraitCollection().userInterfaceStyle != previousTraitCollection.userInterfaceStyle {
                textArea.borderColor = AVSColor.inputBorder.color.cgColor
            }
        }

        init(label: String?, placeholder: String?, value: String?, mode: Mode = .textArea) {
            if let label = label {
                labelNode = ASTextNode2(with: label.attributed(.bodyTitleHeavy, .primaryLabel))
            }

            self.mode = mode

            textArea.borderColor = AVSColor.inputBorder.color.cgColor
            textArea.borderWidth = 1
            textArea.backgroundColor = AVSColor.inputFill.color
            textArea.textContainerInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
            textArea.onDidLoad { node in
                let attributes = NSAttributedString.attributes(
                        font: AVSFont.bodyRoman.font,
                        color: AVSColor.primaryLabel.color,
                        alignment: .left
                )
                guard let node = node as? ASEditableTextNode else { return }
                node.textView.typingAttributes = attributes
                node.textView.text = value
            }

            textArea.attributedPlaceholderText = placeholder?.attributed(.bodyRoman, .secondaryLabel)

            super.init()
            textArea.delegate = self
            automaticallyManagesSubnodes = true
        }

        func editableTextNodeDidUpdateText(_ editableTextNode: ASEditableTextNode) {
            didChangeValue?(editableTextNode.attributedText?.string)
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            [
                labelNode,
                mode == .textArea ? textArea.minHeight(.points(150)) : textArea
            ].vStacked(spacing: 10)
             .margin(FormNodes.margin)
        }
    }
}

class AmenityAdditionalInfoViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

    enum Fields: Int, CaseIterable {
        case info
        case guestName
        case guestPhone
    }

    enum Sections: Int, CaseIterable {
        case metadata
        case fields
    }

    var amenity: Amenity
    var startDate: Date
    var endDate: Date
    var selectionBar: AmenitySelectionBar
    var collectionNode: ASCollectionNode
    var priceFetcher: AmenityPriceFetcher

    var comment: String?
    var guestName: String? {
        didSet { updateSelectionBar() }
    }
    var guestPhone: String? {
        didSet { updateSelectionBar() }
    }
    var reservation: AmenityReservation?
    var price: AmenityPriceFetcher.Price?

    var fields: [Fields] {
        if amenity.priceType == .day {
            return Fields.allCases
        } else {
            return [.info]
        }
    }

    var isValid: Bool {
        if amenity.priceType == .day {
            return !String.isBlank(guestPhone) && !String.isBlank(guestName)
        }

        return true
    }

    init(startDate: Date, endDate: Date, navigator: AmenityReservationNavigator) {
        amenity = navigator.amenity
        reservation = navigator.reservation
        self.startDate = startDate
        self.endDate = endDate

        collectionNode = .avsCollection()
        collectionNode.backgroundColor = AVSColor.secondaryBackground.color

        let node = ASDisplayNode()
        node.automaticallyManagesSubnodes = true
        node.automaticallyRelayoutOnSafeAreaChanges = true

        let selectionBar = AmenitySelectionBar(buttonTitle: "Continue")

        self.selectionBar = selectionBar
        priceFetcher = .init(amenity: amenity)

        super.init(node: node)

        collectionNode.delegate = self
        collectionNode.dataSource = self

        selectionBar.tappedButton = { [weak self] in
            guard let self = self else { return }
            navigator.additionalInfoDidFinish(guestName: self.guestName, guestPhone: self.guestPhone, comment: self.comment)
        }

        node.layoutSpecBlock = { node, range in
            [
                self.collectionNode.growing(),
                selectionBar
            ].vStacked(spacing: 10)
        }
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()
        view.backgroundColor = AVSColor.secondaryBackground.color
        navigationItem.title = "Reservation"

        if amenity.isFree {
            updateSelectionBar()
        } else {
            priceFetcher.getPrice(query: .init(startTime: startDate, endTime: endDate, discountCodes: nil, products: [])).done { [weak self] price in
                guard let self = self else { return }
                self.price = price
                self.updateSelectionBar()
            }.catch { error in
                Log.thisError(error)
            }
        }
    }

    func updateSelectionBar() {
        var text: String = ""

        if amenity.isFree {
            text = "Free"
        } else if let price = price {
            text = "Total Fee: \(price.priceString)"
        }

        if isValid {
            selectionBar.state = .selected(text)
        } else {
            selectionBar.state = .empty(text)
        }
    }

    func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
        Sections.allCases.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        guard let section = Sections(rawValue: section) else { return 0 }
        switch section {
        case .metadata:
            return 1
        case .fields:
            return fields.count
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        guard let section = Sections(rawValue: indexPath.section) else { return { ASCellNode() } }

        switch section {
        case .metadata:
            let amenity = self.amenity
            let startDate = self.startDate
            let endDate = self.endDate

            return {
                let pager = AmenityReservationNodes.PriceImagePager(amenity: amenity, ratio: 0.5)

                let dateString = AmenityReservation.dateString(amenity: amenity, startDate: startDate, endDate: endDate)
                let timeString = AmenityReservation.timeString(amenity: amenity, startDate: startDate, endDate: endDate)

                let dateBar = AmenityReservationNodes.EditSummaryLineNode(leftLabel: "Date", rightLabel: dateString)
                var timeBar: AmenityReservationNodes.EditSummaryLineNode? = nil

                if amenity.priceType != .day {
                    timeBar = .init(leftLabel: "Time", rightLabel: timeString)
                }

                let cell = ASCellNode()
                cell.automaticallyManagesSubnodes = true
                cell.layoutSpecBlock = { node, range in
                    [
                        pager.margin(15),
                        dateBar.margin(horizontal: 15),
                        timeBar?.margin(horizontal: 15)
                    ].vStacked(spacing: 10).margin(bottom: 10)
                }
                return cell
            }
        case .fields:
            let field = fields[indexPath.item]

            switch field {
            case .info:
                let note = reservation?.note

                return {
                    let cell = FormNodes.TextArea(label: "Comment Info", placeholder: "Leave a note", value: note)
                    cell.didChangeValue = { [weak self] string in
                        self?.comment = string
                    }
                    return cell
                }
            case .guestName:
                let guestName = self.guestName
                return {
                    let cell = FormNodes.TextArea(label: "Guest Name", placeholder: nil, value: guestName, mode: .textField)
                    cell.didChangeValue = { [weak self] string in
                        self?.guestName = string
                    }
                    return cell
                }
            case .guestPhone:
                let guestPhone = self.guestPhone
                return {
                    let cell = FormNodes.TextArea(label: "Guest Phone", placeholder: nil, value: guestPhone, mode: .textField)
                    cell.didChangeValue = { [weak self] string in
                        self?.guestPhone = string
                    }
                    return cell
                }
            }
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }
}
