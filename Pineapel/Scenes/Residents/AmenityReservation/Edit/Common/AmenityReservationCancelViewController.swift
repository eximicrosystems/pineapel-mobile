//
// Created by Andrei Stoicescu on 02.12.2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import PromiseKit
import SwiftyUserDefaults

class AmenityReservationCancelViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

    enum Sections: Int, CaseIterable {
        case policy
        case itemsHeader
        case items
        case submit
    }

    var canceledReservation: (() -> Void)?

    var collectionNode: ASCollectionNode {
        node as! ASCollectionNode
    }

    var reservation: AmenityReservation
    var amenity: Amenity
    var refundData: OrderRefund?

    var refundDataItems: [OrderRefund.Item] {
        refundData?.items ?? []
    }

    init(amenity: Amenity, reservation: AmenityReservation) {
        self.reservation = reservation
        self.amenity = amenity
        let collectionNode = ASCollectionNode.avsCollection()
        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func loadRefundData() {
        guard let reservationID = reservation.reservationID else { return }

        SVProgressHUD.show()
        ResidentService().getRefundInfo(reservationID: reservationID).done { refund in
            self.refundData = refund
            self.collectionNode.reloadData()
        }.catch { error in
            Log.thisError(error)
        }.finally {
            SVProgressHUD.dismiss()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.leftBarButtonItem = UIBarButtonItem(title: L10n.General.close, style: .plain, target: self, action: #selector(didTapClose))
        replaceSystemBackButton()
        navigationItem.title = "Cancel Reservation"
        loadRefundData()
    }

    @objc func didTapClose() {
        guard let nav = navigationController as? AVSNavigationController else { return }
        nav.dismissNavigation?()
    }

    func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
        Sections.allCases.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case Sections.policy.rawValue:
            return String.isBlank(amenity.cancellationPolicy) ? 0 : 1
        case Sections.items.rawValue:
            return refundDataItems.count
        case Sections.submit.rawValue:
            return 1
        case Sections.itemsHeader.rawValue:
            return refundDataItems.count > 0 ? 1 : 0
        default:
            return 0
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        switch indexPath.section {
        case Sections.itemsHeader.rawValue:
            return { GenericNodes.HeaderCell(title: "Refunded items") }
        case Sections.policy.rawValue:
            let policyString = amenity.cancellationPolicy?.html?.string
            return {
                GenericNodes.TitleSubtitleCell(title: "Cancellation Policy", subtitle: policyString)
            }
        case Sections.items.rawValue:
            let item = refundDataItems[indexPath.item]
            return {
                GenericNodes.TitleSubtitleCell(title: item.name, subtitle: nil, rightLabel: item.amount.currencyDollarClean)
            }
        case Sections.submit.rawValue:
            return {
                let button = ASButtonNode.primaryButton(title: L10n.AmenityReservation.Buttons.CancelReservation.title)
                let cell = ASCellNode()
                cell.automaticallyManagesSubnodes = true
                cell.layoutSpecBlock = { node, range in
                    button.margin(15)
                }
                return cell
            }
        default:
            return { ASCellNode() }
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        canceledReservation?()
    }
}
