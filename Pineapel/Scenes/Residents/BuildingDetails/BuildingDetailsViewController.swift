//
// Created by Andrei Stoicescu on 10/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit
import SwiftyUserDefaults
import PromiseKit
import AsyncDisplayKit
import SVProgressHUD

class BuildingDetailsViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

    enum Sections: Int, CaseIterable {
        case main
        case menu
    }

    enum Menu {
        case things
        case amenities
        case faq
    }

    var collectionNode: ASCollectionNode {
        node as! ASCollectionNode
    }

    var buildingDetails: BuildingDetails?

    var menuItems: [Menu] = {
        switch AppState.userType {
        case .resident:
            return [.things, .amenities]
        case .concierge:
            return [.things, .faq]
        }
    }()

    init(buildingDetails: BuildingDetails? = nil) {
        self.buildingDetails = buildingDetails
        let collectionNode = ASCollectionNode.avsCollection()
        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()
        navigationItem.title = "Building Details"
        refreshItems()
    }

    @objc func refreshItems() {
        if buildingDetails != nil { return }

        buildingDetails = CodableCache.get(BuildingDetails.self, key: "current_building_details")

        if buildingDetails == nil {
            SVProgressHUD.show()
        } else {
            collectionNode.reloadData()
        }

        ResidentService().getBuildingDetails().done { [weak self] details in
            guard let self = self else { return }
            if details != self.buildingDetails {
                CodableCache.set(details, key: "current_building_details")
                self.buildingDetails = details
                self.collectionNode.reloadData()
            }
        }.catch { error in
            Log.thisError(error)
        }.finally {
            SVProgressHUD.dismiss()
        }
    }

    func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
        Sections.allCases.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        guard let section = Sections(rawValue: section) else { return 0 }
        switch section {
        case .main:
            return buildingDetails != nil ? 1 : 0
        case .menu:
            return menuItems.count
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        guard let section = Sections(rawValue: indexPath.section) else { return { ASCellNode() } }

        switch section {
        case .main:
            guard let building = buildingDetails else { return { ASCellNode() } }
            let articleBody = building.articleBody?.html?.string

            return {
                let backgroundNode = ASDisplayNode()
                backgroundNode.backgroundColor = AVSColor.secondaryBackground.color
                let carousel = ConciergeGenericNodes.ImagePager(images: building.images, ratio: 3 / 5)

                let summary = self.buildSummaryNode(building)

                let titleNode = ASTextNode2(with: building.name?.attributed(.bodyTitleHeavy, .primaryLabel))
                let subtitleNode = ASTextNode2(with: articleBody?.attributed(.bodyRoman, .primaryLabel))

                let cell = ASCellNode()
                cell.automaticallyManagesSubnodes = true
                cell.layoutSpecBlock = { node, range in
                    [
                        carousel,
                        [titleNode, subtitleNode].vStacked().spaced(10).margin(20),
                        summary.margin(horizontal: 20)
                    ].vStacked().margin(bottom: 20).background(backgroundNode).margin(bottom: 20)
                }
                return cell
            }
        case .menu:
            let item = menuItems[indexPath.item]

            switch item {
            case .things:
                return { ConciergeGenericNodes.MenuCell(icon: nil, title: "Things to Know", subtitle: "Other information about the building", disclosure: true) }
            case .amenities:
                return { ConciergeGenericNodes.MenuCell(icon: nil, title: "Building and Unit Amenities", subtitle: "List of building amenities and unit features", disclosure: true) }
            case .faq:
                return { ConciergeGenericNodes.MenuCell(icon: nil, title: "Building FAQ", subtitle: "Some faqs about the building", disclosure: true) }
            }
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        guard Sections(rawValue: indexPath.section) == .menu else { return }
        let item = menuItems[indexPath.item]

        switch item {
        case .things:
            navigationController?.pushViewController(ConciergeThingsToKnowListViewController(), animated: true)
        case .amenities:
            navigationController?.pushViewController(AmenityListViewController(requiresReservation: false), animated: true)
        case .faq:
            navigationController?.pushViewController(ConciergeBuildingFAQViewController(), animated: true)
        }
    }

    func buildSummaryNode(_ building: BuildingDetails) -> ConciergePagerListNodes.SummaryNode {
        switch AppState.userType {
        case .resident:
            return .init(config: .matrix(
                    [
                        [.init(title: "BUILDING ADDRESS", subtitle: building.address)],
                        [.init(title: "APARTMENT AMENITIES", subtitle: building.apartmentAmenities)],
                        [.init(title: "COMMUNITY AMENITIES", subtitle: building.communityAmenities)]
                    ]))
        case .concierge:
            return .init(config: .matrix(
                    [
                        [
                            .init(title: "BUILDING ADDRESS", subtitle: building.address),
                            .init(title: "BUILDING ROOMS", subtitle: building.rooms)
                        ],
                        [.init(title: "APARTMENT AMENITIES", subtitle: building.apartmentAmenities)],
                        [.init(title: "COMMUNITY AMENITIES", subtitle: building.communityAmenities)]
                    ]))
        }
    }
}
