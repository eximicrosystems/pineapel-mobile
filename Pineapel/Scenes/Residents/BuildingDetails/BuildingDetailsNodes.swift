//
// Created by Andrei Stoicescu on 10/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit

struct BuildingDetailsNodes {
    class ArticleCell: ASCellNode {
        static let margin: CGFloat = 20

        enum ArticleCellMode {
            case building
            case amenity
        }

        var thumbnail = ASNetworkImageNode()
        var title = ASTextNode2()
        var body = ASTextNode2()
        var backgroundNode = ASDisplayNode()
        var cellMode: ArticleCellMode
        var hasBottomMargin: Bool
        var hasTopMargin: Bool

        init(thumbnailURL: URL?,
             title: String?,
             body: String?,
             cellMode: ArticleCellMode = .building,
             hasBottomMargin: Bool = false,
             hasTopMargin: Bool = false) {

            thumbnail.url = thumbnailURL
            self.hasBottomMargin = hasBottomMargin
            self.hasTopMargin = hasTopMargin

            let titleFont: Asset.Fonts
            switch cellMode {
            case .building:
                titleFont = .H4
            case .amenity:
                titleFont = .NormalTextHead
            }

            self.title.attributedText = NSAttributedString.string(text: title, font: titleFont, color: .black)
            self.body.attributedText = HTMLTemplateAttributedString.from(string: body)
            backgroundNode.backgroundColor = .white
            self.cellMode = cellMode

            super.init()
            self.automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {

            let labelsStack = ASStackLayoutSpec(
                    direction: .vertical,
                    spacing: 20,
                    justifyContent: .start,
                    alignItems: .start,
                    children: [title, body]
            )

            var thumbnailRatio: ASLayoutSpec

            switch cellMode {
            case .building:
                thumbnailRatio = thumbnail.ratio(9 / 16)
            case .amenity:
                thumbnailRatio = thumbnail.ratio(1).margin(horizontal: 15)
            }

            return ASStackLayoutSpec(
                    direction: .vertical,
                    spacing: 0,
                    justifyContent: .start,
                    alignItems: .start,
                    children: [
                        thumbnailRatio,
                        labelsStack.margin(15)
                    ])
                    .background(backgroundNode)
                    .margin(
                            top: hasTopMargin ? ArticleCell.margin : 0,
                            bottom: hasBottomMargin ? ArticleCell.margin : 0)
        }
    }
}