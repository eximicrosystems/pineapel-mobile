//
// Created by Andrei Stoicescu on 29/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SVProgressHUD
import PromiseKit
import SwiftyUserDefaults
import ImageViewer
import SafariServices

protocol FeedNotificationViewControllerProtocol: class {
    func notificationViewControllerFeedCellTapped(icon: UserNotificationsNodes.FeedCell.Icon, item: FeedNotificationItem)
}

class FeedNotificationViewController: ASListViewController<FeedNotificationItem> {
    weak var delegate: FeedNotificationViewControllerProtocol?
    var notificationID: String

    init(notificationID: String) {
        self.notificationID = notificationID
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()
    }

    override func refreshItems() {
        ResidentService().getFeedNotification(notificationID: self.notificationID).done { [weak self] notification in
            SVProgressHUD.dismiss()
            guard let self = self else { return }
            self.items = [notification]
            self.collectionNode.reloadData()
        }.catch { error in
            SVProgressHUD.showError(withStatus: error.localizedDescription)
            Log.thisError(error)
        }
    }

    override func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let item = items[indexPath.item]
        return {
            let cell = UserNotificationsNodes.FeedCell(item: item, readonly: true)
            cell.delegate = self
            return cell
        }
    }
}

extension FeedNotificationViewController: UserNotificationsFeedCellProtocol {
    func feedCellTapped(icon: UserNotificationsNodes.FeedCell.Icon, item: FeedNotificationItem) {
        switch icon {
        case .chat:
            ()
        case .delivery:
            if String.isBlank(item.requestID) {
                // Mark request done here so we don't reload it the second time. Will be handled in the
                var item = item
                item.requestID = UUID().uuidString
                items[0] = item
                collectionNode.reloadData()
            }
        case .readMore:
            if let link = item.linkType() {
                switch link {
                case .url(let url):
                    navigationController?.present(SFSafariViewController(url: url, configuration: SFSafariViewController.Configuration()), animated: true)
                case .article(let id, let title):
                    let url = Article.articleNodeURL(id: id)
                    let vc = ArticleWebViewController(url: url)
                    vc.title = title
                    vc.closeBlock = { [weak self] in
                        guard let self = self else { return }
                        self.navigationController?.dismiss(animated: true)
                    }

                    navigationController?.present(AVSNavigationController(rootViewController: vc), animated: true)
                }
            }
        }

        switch icon {
        case .chat, .delivery:
            self.delegate?.notificationViewControllerFeedCellTapped(icon: icon, item: item)
        case .readMore:
            // Don't pass the read more event, we're opening the link here
            ()
        }

    }

    func feedCellMenuItemTapped(item: FeedNotificationItem) {
        // The --- Menu Item is disabled here
    }

    func feedCellMediaTapped(cell: UserNotificationsNodes.FeedCell, item: FeedNotificationMediaFile, list: [FeedNotificationMediaFile]) {
        // The Media Tap Action is disabled here
    }

    func feedCellVisible(item: FeedNotificationItem) {
        // No reaction when cell is visible
    }
}