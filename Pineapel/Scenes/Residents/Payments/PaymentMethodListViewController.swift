//
// Created by Andrei Stoicescu on 02/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import PassKit
import Stripe
import SVProgressHUD
import LocalAuthentication

struct PaymentsCellNodes {
    class BiometricsCell: ASCellNode {

        var switchNode = ASDisplayNode()
        var label = ASTextNode2()
        var background = ASDisplayNode()
        var biometrics: Biometrics

        init(biometrics: Biometrics) {
            self.biometrics = biometrics

            let text: String
            switch biometrics.biometryType {
            case .touchID:
                text = "Use Touch ID"
            case .faceID:
                text = "Use Face ID"
            default:
                text = ""
            }

            label.attributedText = text.attributed(.body, .primaryLabel)

            background.cornerRadius = 4
            background.borderColor = AVSColor.inputBorder.color.cgColor
            background.borderWidth = 1

            super.init()

            switchNode = ASDisplayNode { () -> UIView in
                let uiSwitch = UISwitch(frame: .zero)
                uiSwitch.setOn(self.biometrics.isEnabled, animated: false)
                uiSwitch.addTarget(self, action: #selector(self.biometricsSwitchChanged), for: .valueChanged)
                return uiSwitch
            }


            backgroundColor = AVSColor.secondaryBackground.color
            automaticallyManagesSubnodes = true
        }

        override func asyncTraitCollectionDidChange(withPreviousTraitCollection previousTraitCollection: ASPrimitiveTraitCollection) {
            super.asyncTraitCollectionDidChange(withPreviousTraitCollection: previousTraitCollection)
            if asyncTraitCollection().userInterfaceStyle != previousTraitCollection.userInterfaceStyle {
                background.borderColor = AVSColor.inputBorder.color.cgColor
            }
        }

        @objc func biometricsSwitchChanged(control: UISwitch) {
            if control.isOn {
                biometrics.authenticate(saveResult: true).done {
                    Log.this("auth done")
                }.catch { error in
                    control.setOn(false, animated: true)
                    Log.thisError(error)
                }
            } else {
                biometrics.isEnabled = false
            }
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            label.style.flexGrow = 1
            switchNode.style.preferredSize = CGSize(width: 51, height: 31)

            let logoStack = ASStackLayoutSpec(direction: .horizontal,
                                              spacing: 20,
                                              justifyContent: .start,
                                              alignItems: .center,
                                              children: [
                                                  label,
                                                  switchNode
                                              ])
            return logoStack.margin(15).background(background).margin(top: 0, left: 15, bottom: 15, right: 15)
        }
    }

    class PaymentMethodCell: ASCellNode {
        var logo: ASImageNode
        var label: ASTextNode2
        var background = ASDisplayNode()
        var marginTop = false

        init(logoImage: UIImage, cardLabel: String?, firstCard: Bool = false) {

            logo = ASImageNode(image: logoImage)
            logo.contentMode = .scaleAspectFit

            marginTop = firstCard

            label = ASTextNode2()

            if let cardLabel = cardLabel {
                label.attributedText = "**** \(cardLabel)".attributed(.body, .primaryLabel)
            }

            background.cornerRadius = 4
            background.borderColor = AVSColor.inputBorder.color.cgColor
            background.borderWidth = 1

            super.init()
            backgroundColor = AVSColor.secondaryBackground.color
            automaticallyManagesSubnodes = true
        }

        override func asyncTraitCollectionDidChange(withPreviousTraitCollection previousTraitCollection: ASPrimitiveTraitCollection) {
            super.asyncTraitCollectionDidChange(withPreviousTraitCollection: previousTraitCollection)
            if asyncTraitCollection().userInterfaceStyle != previousTraitCollection.userInterfaceStyle {
                background.borderColor = AVSColor.inputBorder.color.cgColor
            }
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            [
                logo.height(.points(20)).maxWidth(.points(50)),
                label.growing()
            ].hStacked().spaced(20)
             .margin(15)
             .background(background)
             .margin(top: marginTop ? 15 : 0, left: 15, bottom: 15, right: 15)
        }
    }
}

enum PaymentMethodSelection: Equatable {
    case applePay
    case paymentMethod(method: STPPaymentMethod)

    var typeString: String? {
        switch self {
        case .applePay:
            return "Apple Pay"
        case .paymentMethod(let method):
            guard let card = method.card else { return nil }
            let brandString = STPPaymentMethodCard.string(from: card.brand)
            let endingIn = card.last4 != nil ? " ending in \(card.last4 ?? "")" : ""
            return "\(brandString)\(endingIn)"
        }
    }
}

class PaymentMethodListViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {
    var collectionNode: ASCollectionNode
    lazy var refreshControl = UIRefreshControl()

    enum Sections: Int, CaseIterable {
        case header
        case headerLabel
        case applePay
        case cards
    }

    var headerCell: ASCellNode?
    var items: [STPPaymentMethod] = []
    var supportsApplePay: Bool = false
    var disableApplePay: Bool = false

    private var useApplePay: Bool {
        supportsApplePay && !disableApplePay
    }
    
    override init() {
        collectionNode = .avsCollection()
        collectionNode.backgroundColor = AVSColor.secondaryBackground.color

        super.init(node: collectionNode)
        
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        replaceSystemBackButton()
        navigationItem.title = "Payment"
        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addCard))
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "chevron.left"), style: .plain, target: self, action: #selector(backView))
        collectionNode.view.addSubview(refreshControl)
        SVProgressHUD.show()
        refreshItems()
    }
    
    @objc func backView(){
        AppState.switchTo(viewController: ResidentTabBarViewController(page: nil))
    }

    @objc func refreshItems() {
        supportsApplePay = Stripe.deviceSupportsApplePay()

        let stripeProvider = StripeProvider()

        stripeProvider.getPaymentMethods().done { methods in
            SVProgressHUD.dismiss()
            self.items = methods ?? []
            self.collectionNode.reloadData()
        }.ensure {
            self.refreshControl.endRefreshing()
        }.catch { error in
            SVProgressHUD.showError(withStatus: error.localizedDescription)
            Log.this(error)
        }
    }

    func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
        Sections.allCases.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        guard let section = Sections(rawValue: section) else { return 0 }

        switch section {
        case .header:
            return headerCell != nil ? 1 : 0
        case .headerLabel:
            return 1
        case .applePay:
            return useApplePay ? 1 : 0
        case .cards:
            return items.count
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        guard let section = Sections(rawValue: indexPath.section) else { return { ASCellNode() } }
        let supportsApplePay = self.supportsApplePay

        switch section {
        case .header:
            return {
                guard let cell = self.headerCell else { return ASCellNode() }
                return cell
            }
        case .headerLabel:
            return { FormNodes.SectionLabel(label: "Payments on Record")}
        case .applePay:
            return { PaymentsCellNodes.PaymentMethodCell(logoImage: Asset.Images.applePay.image, cardLabel: nil, firstCard: true) }
        case .cards:
            let item = items[indexPath.item]
            return { PaymentsCellNodes.PaymentMethodCell(logoImage: item.image, cardLabel: item.card?.last4, firstCard: indexPath.item == 0 && !supportsApplePay) }
        }
    }

    func paymentMethodSelected(method: PaymentMethodSelection) {
        fatalError("paymentMethodSelected must be implemented in subclass")
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        guard let section = Sections(rawValue: indexPath.section) else { return }

        switch section {
        case .applePay:
            paymentMethodSelected(method: .applePay)
        case .cards:
            let item = items[indexPath.item]
            paymentMethodSelected(method: .paymentMethod(method: item))
        default:
            ()
        }
    }

    @objc func addCard() {
        let cardVC = AddCardViewController()
        let nav = AVSNavigationController(rootViewController: cardVC)
        nav.isModalInPresentation = true
        cardVC.onCancelAction = { self.dismiss(animated: true) }

        cardVC.onAddedSuccessAction = {
            self.dismiss(animated: true)
            self.refreshItems()
        }

        present(nav, animated: true)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        ASSizeRange.fullWidth(collectionNode: collectionNode)
    }
}
