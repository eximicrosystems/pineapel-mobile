//
// Created by Andrei on 01.04.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//

import SwiftUI
import Alamofire
import PromiseKit
import SVProgressHUD

extension RandomAccessCollection {
    func indexed() -> Array<(offset: Int, element: Element)> {
        Array(enumerated())
    }
}

protocol DiscountCodesSelectable {
    func dismissCodesDidFinish(_ codes: [String])
}

struct DiscountCodeItemView: View {
    @State var code: String
    var onDelete: () -> ()
    var onChange: (String) -> ()

    var body: some View {
        VStack {
            ConciergeForm.TextInput(label: "Code", value: $code)

            Button(action: onDelete) {
                HStack {
                    Spacer()
                    Text("Remove")
                }
            }.foregroundColor(.primaryLabel)
             .font(.note)
        }
    }
}

struct DiscountCodesFormView: View {
    @State var codes: [String] = [""]
    var delegate: DiscountCodesSelectable

    var allCodeFieldsFilled: Bool {
        codes.first(where: { $0.isEmpty }) == nil
    }

    var currentCodes: [String] {
        codes.filter { !String.isBlank($0) }
    }

    var body: some View {
        ScrollView(showsIndicators: false) {
            VStack(spacing: 20) {
                ForEach(codes.indices, id: \.self) { idx in
                    ConciergeForm.TextInput(label: "Code", value: $codes[idx])
                }

                if allCodeFieldsFilled {
                    HStack {
                        Button(action: {
                            codes.append("")
                        }) {
                            HStack {
                                Image(systemSymbol: .plus)
                                Text("Add Another Code")
                            }
                        }.foregroundColor(.primaryLabel)
                         .disabled(!allCodeFieldsFilled)

                        Spacer()
                    }
                }

                Button(action: {
                    delegate.dismissCodesDidFinish(currentCodes)
                }) {
                    Text(codes.count == 1 ? "Apply Code" : "Apply Codes")
                }.buttonStyle(BlockButtonStyle())
            }.padding(.top, 20)
             .padding(.horizontal, 20)
        }.fullViewBackgroundColor()
         .navigationBarTitle(Text("Add Discount Codes"), displayMode: .inline)
    }
}