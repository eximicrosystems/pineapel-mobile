//
// Created by Andrei Stoicescu on 10/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SwiftyUserDefaults

protocol ProductCellProtocol: class {
    func selectedButton(action: CartProductNodes.ButtonType, product: ProductProtocol)
}

protocol CartProductCellProtocol: class {
    func selectedButton(action: CartProductNodes.ButtonType, cartProduct: CartProduct)
}

struct CartProductNodes {

    enum ButtonType {
        case addToCart
        case remove
        case plus
        case minus
    }

    class CartButton: ASButtonNode {
        var buttonType: ButtonType

        init(buttonType: ButtonType) {
            self.buttonType = buttonType
            super.init()
        }
    }

    class ProductDetailCell: ASCellNode {
        weak var delegate: CartProductCellProtocol?

        var backgroundNode = ASDisplayNode()
        var body = ASTextNode2()
        var thumbnail: AmenityReservationNodes.PriceImagePager
        var addToCartButton: ASButtonNode
        var quantityControl: QuantityControl?

        private var cartProduct: CartProduct
        private var currentQuantity: Int

        override func asyncTraitCollectionDidChange(withPreviousTraitCollection previousTraitCollection: ASPrimitiveTraitCollection) {
            super.asyncTraitCollectionDidChange(withPreviousTraitCollection: previousTraitCollection)
            if asyncTraitCollection().userInterfaceStyle != previousTraitCollection.userInterfaceStyle {
                addToCartButton.borderColor = AVSColor.inputBorder.color.cgColor
            }
        }

        init(product: ProductProtocol, htmlDescription: String?, currentQuantity: Int) {
            self.currentQuantity = currentQuantity
            cartProduct = .init(product: product)
            thumbnail = .init(product: product)

            addToCartButton = .primaryButton(title: "Add To Cart")
            addToCartButton.borderColor = AVSColor.inputBorder.color.cgColor
            addToCartButton.contentEdgeInsets = UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 0)
            addToCartButton.borderWidth = 1

            body.attributedText = htmlDescription?.attributed(.body, .primaryLabel)
            backgroundNode.backgroundColor = AVSColor.secondaryBackground.color

            super.init()

            addToCartButton.addTarget(self, action: #selector(tappedButton), forControlEvents: .touchUpInside)

            if !product.schedulable {
                let quantityControl = QuantityControl(quantity: cartProduct.quantity)
                quantityControl.tappedPlusBlock = { [weak self] in
                    self?.increaseQuantity()
                }

                quantityControl.tappedMinusBlock = { [weak self] in
                    self?.decreaseQuantity()
                }

                self.quantityControl = quantityControl
            }

            updateQuantity(1)

            automaticallyManagesSubnodes = true
        }

        private var canIncreaseQuantity: Bool {
            cartProduct.canIncreaseQuantity(by: cartProduct.quantity + 1, currentQuantity: currentQuantity)
        }

        private var canAddToCart: Bool {
            cartProduct.canSetQuantityToCart(currentQuantity + cartProduct.quantity)
        }

        func increaseQuantity() {
            guard canIncreaseQuantity else { return }
            updateQuantity(cartProduct.quantity + 1)
        }

        func decreaseQuantity() {
            updateQuantity(max(1, cartProduct.quantity - 1))
        }

        func updateQuantity(_ quantity: Int) {
            cartProduct.quantity = quantity

            if cartProduct.product.schedulable {
                addToCartButton.setPrimaryButtonTitle("Schedule and Add to Cart")
            } else {
                addToCartButton.setPrimaryButtonTitle("Add to Cart")
            }

            if canAddToCart {
                addToCartButton.alpha = 1
                addToCartButton.isUserInteractionEnabled = true
            } else {
                addToCartButton.alpha = 0.4
                addToCartButton.isUserInteractionEnabled = false
            }

            if let quantityControl = quantityControl {
                quantityControl.quantity = quantity
                quantityControl.canIncreaseQuantity = canIncreaseQuantity
            }
        }

        @objc func tappedButton(_ sender: ASButtonNode) {
            delegate?.selectedButton(action: .addToCart, cartProduct: cartProduct)
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            [
                thumbnail,
                body,
                [
                    quantityControl?.minWidth(.points(60)),
                    addToCartButton.growing()
                ].hStacked().justified(.spaceBetween).spaced(10).aligned(.stretch)
            ].vStacked().spaced(20)
             .margin(20)
             .background(backgroundNode)
        }
    }

    class QuantityControl: ASDisplayNode {
        var minusLabel = ASButtonNode()
        var plusLabel = ASButtonNode()
        var quantityLabel = ASTextNode2()

        var tappedMinusBlock: (() -> Void)?
        var tappedPlusBlock: (() -> Void)?
        var quantity: Int {
            didSet {
                updateQuantityText(String(quantity))
                handleDecreaseQuantityUI()
            }
        }

        var canIncreaseQuantity: Bool = true {
            didSet { handleIncreaseQuantityUI() }
        }

        private func updateQuantityText(_ quantity: String) {
            quantityLabel.attributedText = quantity.attributed(.body, .primaryIcon)
        }

        private func handleIncreaseQuantityUI() {
            plusLabel.isUserInteractionEnabled = canIncreaseQuantity
            plusLabel.alpha = canIncreaseQuantity ? 1 : 0.4
        }

        private func handleDecreaseQuantityUI() {
            minusLabel.isUserInteractionEnabled = quantity > 1
            minusLabel.alpha = quantity > 1 ? 1 : 0.4
        }

        init(quantity: Int) {
            self.quantity = quantity

            minusLabel.setAttributedTitle("-".attributed(.body, .primaryIcon), for: .normal)
            minusLabel.hitTestSlop = UIEdgeInsets(top: -10, left: -10, bottom: -10, right: -10)
            plusLabel.setAttributedTitle("+".attributed(.body, .primaryIcon), for: .normal)
            plusLabel.hitTestSlop = UIEdgeInsets(top: -10, left: -10, bottom: -10, right: -10)

            super.init()

            updateQuantityText(String(quantity))

            minusLabel.addTarget(self, action: #selector(tappedMinus), forControlEvents: .touchUpInside)
            plusLabel.addTarget(self, action: #selector(tappedPlus), forControlEvents: .touchUpInside)

            borderColor = AVSColor.inputBorder.color.cgColor
            borderWidth = 1

            handleIncreaseQuantityUI()

            automaticallyManagesSubnodes = true
        }

        override func asyncTraitCollectionDidChange(withPreviousTraitCollection previousTraitCollection: ASPrimitiveTraitCollection) {
            super.asyncTraitCollectionDidChange(withPreviousTraitCollection: previousTraitCollection)
            if asyncTraitCollection().userInterfaceStyle != previousTraitCollection.userInterfaceStyle {
                borderColor = AVSColor.inputBorder.color.cgColor
            }
        }

        @objc func tappedMinus() {
            tappedMinusBlock?()
        }

        @objc func tappedPlus() {
            tappedPlusBlock?()
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            [minusLabel, quantityLabel, plusLabel].hStacked().spaced(2).justified(.spaceAround).margin(5)
        }
    }

    class CartProductCell: ASCellNode {

        weak var delegate: CartProductCellProtocol?

        var thumbnail = ASNetworkImageNode()
        var backgroundNode = ASDisplayNode()
        var title = ASTextNode2()
        var body: ASTextNode2?
        var quantityControl: QuantityControl?

        var buttons = [ASLayoutElement]()

        override func asyncTraitCollectionDidChange(withPreviousTraitCollection previousTraitCollection: ASPrimitiveTraitCollection) {
            super.asyncTraitCollectionDidChange(withPreviousTraitCollection: previousTraitCollection)
            if asyncTraitCollection().userInterfaceStyle != previousTraitCollection.userInterfaceStyle {
                buttons.forEach { node in
                    (node as? ASDisplayNode)?.borderColor = AVSColor.inputBorder.color.cgColor
                }
            }
        }

        func createButton(title: String, buttonType: ButtonType) -> CartButton {
            let button = CartButton(buttonType: buttonType)
            configureButton(button, title: title)
            button.addTarget(self, action: #selector(tappedButton), forControlEvents: .touchUpInside)
            return button
        }

        func configureButton(_ button: ASButtonNode, title: String) {
            button.setTitle(title, with: AVSFont.body.font, with: AVSColor.primaryLabel.color, for: .normal)
            button.borderColor = AVSColor.inputBorder.color.cgColor
            button.contentEdgeInsets = UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 0)
            button.borderWidth = 1
            button.cornerRadius = 4
        }

        @objc func tappedButton(_ sender: CartButton) {
            delegate?.selectedButton(action: sender.buttonType, cartProduct: existingCartProduct)
        }

        var existingCartProduct: CartProduct
        var line = ASDisplayNode.divider()

        var addToCartButton: CartButton?

        init(cartProduct: CartProduct) {
            existingCartProduct = cartProduct

            let product = cartProduct.product

            thumbnail.url = product.productImageURL
            thumbnail.contentMode = .scaleAspectFill
            thumbnail.cornerRadius = 4

            title.attributedText = product.name?.attributed(.bodyHeavy, .primaryLabel)
            if let scheduledAt = cartProduct.scheduledAt {
                body = .init(with: scheduledAt.avsTimestampString(timeZone: AppState.buildingTimeZone).attributed(.body, .secondaryLabel))
            }

            backgroundNode.backgroundColor = AVSColor.secondaryBackground.color

            super.init()

            if product.schedulable {
                let scheduleButton = ASButtonNode()
                configureButton(scheduleButton, title: "Reschedule")
                scheduleButton.isUserInteractionEnabled = false

                buttons = [
                    scheduleButton,
                    createButton(title: "Remove", buttonType: .remove)
                ]
            } else {
                let quantityControl = QuantityControl(quantity: existingCartProduct.quantity)
                quantityControl.tappedMinusBlock = { [weak self] in
                    guard let self = self else { return }
                    self.delegate?.selectedButton(action: .minus, cartProduct: self.existingCartProduct)
                }
                quantityControl.tappedPlusBlock = { [weak self] in
                    guard let self = self else { return }
                    self.delegate?.selectedButton(action: .plus, cartProduct: self.existingCartProduct)
                }
                self.quantityControl = quantityControl

                buttons = [
                    quantityControl,
                    createButton(title: "REMOVE", buttonType: .remove)
                ]
            }

            automaticallyManagesSubnodes = true
        }

        private var canAddToCart: Bool {
            existingCartProduct.canSetQuantityToCart(existingCartProduct.quantity + 1)
        }

        var buttonsElement: ASLayoutElement {
            let buttonsElement: ASLayoutElement

            if buttons.count == 1 {
                buttonsElement = buttons[0]
            } else {
                buttons.forEach {
                    $0.style.flexGrow = 1
                    $0.style.flexBasis = ASDimensionMakeWithPoints(20)
                }

                buttonsElement = buttons.hStacked(spacing: 20).justified(.spaceBetween).aligned(.stretch)
            }

            return buttonsElement
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            let thumbSize = constrainedSize.max.width * 0.25
            return [
                [
                    thumbnail.size(thumbSize, thumbSize * 11 / 13),
                    [
                        title,
                        body?.filling(),
                        buttonsElement
                    ].vStacked(spacing: 10).justified(.spaceBetween)
                     .maxHeight(.points(thumbSize))
                     .filling()
                ].hStacked(spacing: 20).aligned(.stretch)
                 .margin(15)
                 .background(backgroundNode),

                line
            ].vStacked()
        }
    }

}