//
// Created by Andrei Stoicescu on 06/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SwiftyUserDefaults

class ProductScheduleEditViewController: ASDKViewController<ASDisplayNode>, ASCollectionDelegate, ASCollectionDataSource {

    enum Items: Int, CaseIterable {
        case pager
        case date
        case time
        case update
    }

    var collectionNode: ASCollectionNode
    var cartProduct: CartProduct
    private let textField = UITextField(frame: .zero)

    var didUpdateCartProduct: ((CartProduct) -> Void)?

    init(cartProduct: CartProduct) {
        self.cartProduct = cartProduct
        collectionNode = .avsCollection()
        collectionNode.backgroundColor = AVSColor.secondaryBackground.color
        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()
        view.backgroundColor = AVSColor.secondaryBackground.color
        navigationItem.title = "Edit Schedule"

        view.addSubview(textField)
        let datePicker = UIDatePicker.aLaCartePicker()
        if let date = cartProduct.scheduledAt {
            datePicker.date = date
        }

        textField.inputView = datePicker
        datePicker.addTarget(self, action: #selector(dateChanged), for: .valueChanged)
    }

    @objc func dateChanged(_ picker: UIDatePicker) {
        cartProduct.scheduledAt = picker.date
        collectionNode.reloadItems(at: [
            .init(item: Items.date.rawValue, section: 0),
            .init(item: Items.time.rawValue, section: 0),
        ])
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        textField.resignFirstResponder()
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        Items.allCases.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        guard let item = Items(rawValue: indexPath.item) else { return { ASCellNode() } }
        let cartProduct = self.cartProduct
        let cellInset = UIEdgeInsets(top: 0, left: 15, bottom: 15, right: 15)

        switch item {
        case .pager:
            return {
                .withNode(
                        AmenityReservationNodes.PriceImagePager(product: cartProduct.product),
                        margin: cellInset
                )
            }
        case .date:
            return {
                .withNode(
                        AmenityReservationNodes.EditSummaryLineNode(
                                leftLabel: "Date",
                                rightLabel: cartProduct.scheduledAt?.avsDateShortMonthYearString(timeZone: AppState.buildingTimeZone),
                                rightIcon: Asset.Images.Concierge.icPencilSmall.image
                        ), margin: UIEdgeInsets(top: 0, left: 15, bottom: 5, right: 15)
                )
            }
        case .time:
            return {
                .withNode(
                        AmenityReservationNodes.EditSummaryLineNode(
                                leftLabel: "Time",
                                rightLabel: cartProduct.scheduledAt?.avsTimeString(timeZone: AppState.buildingTimeZone),
                                rightIcon: Asset.Images.Concierge.icPencilSmall.image
                        ), margin: cellInset
                )
            }
        case .update:
            return {
                .withNode(ASButtonNode.primaryButton(title: "Update"), margin: cellInset)
            }
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        guard let item = Items(rawValue: indexPath.item) else { return }
        switch item {
        case .time, .date:
            textField.becomeFirstResponder()
        case .update:
            didUpdateCartProduct?(cartProduct)
        default:
            ()
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }
}
