//
// Created by Andrei Stoicescu on 10/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import Stripe
import Combine
import DifferenceKit

struct CartCheckoutFlow {
    class CheckoutData {
        var paymentMethod: PaymentMethodSelection?
        var cartManager: CartProductManager

        init(cartManager: CartProductManager) {
            self.cartManager = cartManager
        }
    }
}

class CartViewController: ASDKViewController<ASDisplayNode> {

    struct CartProductModel: Hashable, Differentiable {
        static func ==(lhs: CartProductModel, rhs: CartProductModel) -> Bool {
            lhs.cartProduct.id == rhs.cartProduct.id && lhs.cartProduct.quantity == rhs.cartProduct.quantity && lhs.cartProduct.scheduledAt == rhs.cartProduct.scheduledAt
        }

        let cartProduct: CartProduct

        func hash(into hasher: inout Hasher) { hasher.combine(cartProduct.id) }

        var differenceIdentifier: Int { hashValue }
    }

    var emptyNode: EmptyNode

    enum Sections: Int, CaseIterable, Differentiable {
        case cartItems
        case cartContinue
        case continueShopping
    }

    var collectionNode: ASCollectionNode

    var cartManager: CartProductManager

    var checkoutData: CartCheckoutFlow.CheckoutData

    private var subscriptions = Set<AnyCancellable>()

    var products = [CartProductModel]()

    var sections = [ArraySection<Sections, AnyDifferentiable>]()

    init(cartManager: CartProductManager) {
        collectionNode = .avsCollection()
        self.cartManager = cartManager
        checkoutData = CartCheckoutFlow.CheckoutData(cartManager: cartManager)
        emptyNode = .init(emptyNodeType: .cart, showAction: cartManager.config.cartEmptyCloseButton)

        super.init(node: collectionNode)
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isModalInPresentation = true
        collectionNode.view.backgroundView = emptyNode.view
        emptyNode.actionButtonTapped = { [weak self] in
            self?.didTapClose()
        }

        setupBindings()

        replaceSystemBackButton()
        if cartManager.config.openCartModal {
            navigationItem.leftBarButtonItem = UIBarButtonItem(title: L10n.General.close, style: .plain, target: self, action: #selector(didTapClose))
        }
        navigationItem.title = L10n.ALaCarte.Navigation.title
        showEmptyScreen(cartManager.totalProductsCount == 0)
    }

    func setupBindings() {
        cartManager.$products.receive(on: DispatchQueue.main).sink { [weak self] products in
            self?.reloadData()
            self?.showEmptyScreen(products.count == 0)
        }.store(in: &subscriptions)
    }

    func reloadData() {
        products = cartManager.products.map { CartProductModel(cartProduct: $0) }

        let anyProducts = products.map { AnyDifferentiable($0) }
        let cartContinue = products.count > 0 ? [AnyDifferentiable("cartContinue")] : []
        let continueShopping = products.count > 0 ? [AnyDifferentiable("continueShopping")] : []

        let target: [ArraySection<Sections, AnyDifferentiable>] = [
            ArraySection(model: .cartItems, elements: anyProducts),
            ArraySection(model: .cartContinue, elements: cartContinue),
            ArraySection(model: .continueShopping, elements: continueShopping),
        ]

        let stagedChangeSet = StagedChangeset(source: sections, target: target)
        for changeSet in stagedChangeSet {
            sections = changeSet.data
            collectionNode.performDiffBatchUpdates(changeSet: changeSet)
        }
    }

    @objc func didTapClose() {
        guard let nav = navigationController as? AVSNavigationController else { return }
        nav.dismissNavigation?()
    }

    func showEmptyScreen(_ show: Bool) {
        emptyNode.isHidden = !show
    }
}

extension CartViewController: ASCollectionDelegate, ASCollectionDataSource {

    public func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
        sections.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        sections[section].elements.count
    }

    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        guard let section = Sections(rawValue: indexPath.section) else { return { ASCellNode() } }

        switch section {
        case .cartItems:
            let cartProduct = products[indexPath.item].cartProduct
            return {
                let cell = CartProductNodes.CartProductCell(cartProduct: cartProduct)
                cell.delegate = self
                return cell
            }
        case .cartContinue:
            let title = cartManager.config.cartPrimaryLabel
            return {
                GenericNodes.PrimaryButtonCellNode(title: title)
            }
        case .continueShopping:
            let title = cartManager.config.cartSecondaryLabel
            return {
                .withNode(ASButtonNode.secondaryButton(title: title), margin: UIEdgeInsets(top: 0, left: 15, bottom: 15, right: 15))
            }
        }
    }

    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        guard let section = Sections(rawValue: indexPath.section) else { return }

        switch section {
        case .cartContinue:
            if let externalContinue = cartManager.config.cartExternalPrimaryAction {
                externalContinue()
            } else {
                let biometrics = Biometrics()
                if biometrics.shouldAuthenticate {
                    biometrics.authenticate().done {
                        self.openPaymentSelection()
                    }.cauterize()
                } else {
                    openPaymentSelection()
                }
            }
        case .cartItems:
            let cartProduct = products[indexPath.item].cartProduct
            if cartProduct.product.schedulable {
                let vc = ProductScheduleEditViewController(cartProduct: cartProduct)
                vc.didUpdateCartProduct = { [weak self] cartProduct in
                    guard let self = self else { return }
                    self.navigationController?.popToViewController(self, animated: true)
                    self.cartManager.updateCartProduct(cartProduct)
                }
                navigationController?.pushViewController(vc, animated: true)
            }
        case .continueShopping:
            if let externalSecondary = cartManager.config.cartExternalSecondaryAction {
                externalSecondary()
            } else {
                didTapClose()
            }
        }
    }

    func openPaymentSelection() {
        navigationController?.pushViewController(CartPaymentSelectionViewController(checkoutData: checkoutData), animated: true)
    }

    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        .fullWidth(collectionNode: collectionNode)
    }
}

extension CartViewController: CartProductCellProtocol {
    func selectedButton(action: CartProductNodes.ButtonType, cartProduct: CartProduct) {
        switch action {
        case .minus:
            cartManager.reduceQuantity(cartProduct)
        case .plus:
            cartManager.increaseQuantity(cartProduct)
        case .remove:
            cartManager.removeCartProduct(cartProduct)
        default:
            ()
        }
    }
}
