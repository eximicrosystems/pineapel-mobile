//
// Created by Andrei Stoicescu on 09/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import Stripe
import SVProgressHUD

class PaymentMethodManagerViewController: PaymentMethodListViewController {

    override init() {
        super.init()
        let biometrics = Biometrics()
        if biometrics.canUseBiometricsHardware {
            self.headerCell = PaymentsCellNodes.BiometricsCell(biometrics: biometrics)
        }
        
        
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func paymentMethodSelected(method: PaymentMethodSelection) {
        switch method {
        case .applePay:
            ()
        case .paymentMethod(let method):
            handleCardTapped(paymentMethod: method)
        }
    }

    func handleCardTapped(paymentMethod: STPPaymentMethod) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let deleteAction = UIAlertAction(title: L10n.General.delete, style: .destructive) { [unowned self] _ in
            self.confirmPaymentMethodDelete(paymentMethod: paymentMethod)
        }

        alertController.addAction(deleteAction)
        alertController.addAction(UIAlertAction(title: L10n.General.cancel, style: .cancel, handler: nil))

        if UIDevice.current.userInterfaceIdiom == .pad {
            alertController.popoverPresentationController?.sourceView = self.view
            alertController.popoverPresentationController?.sourceRect = self.view.bounds
            alertController.popoverPresentationController?.permittedArrowDirections = [.down, .up]
        }

        present(alertController, animated: true)
    }

    func confirmPaymentMethodDelete(paymentMethod: STPPaymentMethod) {
        let message = "Delete this payment method?"
        let deleteAlert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let stripeProvider = StripeProvider()

        deleteAlert.addAction(UIAlertAction(title: L10n.General.delete, style: .destructive, handler: { action in
            SVProgressHUD.show()
            stripeProvider.delete(paymentMethod: paymentMethod).done { [weak self] in
                self?.refreshItems()
            }.catch { error in
                Log.thisError(error)
                SVProgressHUD.showError(withStatus: error.localizedDescription)
            }
        }))

        deleteAlert.addAction(UIAlertAction(title: L10n.General.cancel, style: .cancel, handler: nil))

        present(deleteAlert, animated: true, completion: nil)
    }
}
