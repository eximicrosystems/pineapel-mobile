//
// Created by Andrei Stoicescu on 09/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import Stripe
import SVProgressHUD
import AsyncDisplayKit

class CartPaymentSelectionViewController: PaymentMethodListViewController {

    var checkoutData: CartCheckoutFlow.CheckoutData

    init(checkoutData: CartCheckoutFlow.CheckoutData) {
        self.checkoutData = checkoutData
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func paymentMethodSelected(method: PaymentMethodSelection) {
        checkoutData.paymentMethod = method
        navigationController?.pushViewController(CartCheckoutReviewViewController(checkoutData: checkoutData), animated: true)
    }
}
