//
// Created by Andrei Stoicescu on 17/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import PromiseKit
import DifferenceKit

struct PaymentSummary {
    enum Element {
        case line
        case lineLabel(label: String)
        case data(label: String, value: String?)
        case dataAttributed(label: NSAttributedString?, value: NSAttributedString?)
    }

    class ElementNode: ASDisplayNode {
        var labelNode = ASTextNode2()
        var valueNode = ASTextNode2()

        convenience init(label: String, value: String?) {

            self.init(label: label.attributed(.bodyHeavy, .primaryLabel),
                      value: value?.attributed(.bodyHeavy, .accentBackground))
        }

        init(label: NSAttributedString?, value: NSAttributedString?) {
            labelNode.attributedText = label
            valueNode.attributedText = value
            super.init()
            self.automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            [labelNode.filling(), valueNode].hStacked().spaced(40).justified(.spaceBetween).aligned(.start)
        }
    }

    class ElementLine: ASDisplayNode {
        var labelNode = ASTextNode2()

        init(label: String) {
            labelNode.attributedText = label.attributed(.note, .secondaryLabel)
            super.init()
            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            labelNode.wrapped()
        }
    }

    class Cell: ASCellNode {
        var elementNodes: [ASDisplayNode] = []

        convenience init(summary: OrderSummary) {
            self.init(elements: [
                .line,
                .data(label: "Subtotal", value: String(format: "$%.2f", summary.totalWithoutTax)),
                .data(label: "Tax", value: String(format: "$%.2f", summary.totalTax)),
                .line,
                .data(label: "Order Total", value: String(format: "$%.2f", summary.total))
            ])
        }

        init(elements: [Element]) {
            elementNodes = elements.map { el in
                switch el {
                case .line:
                    return ASDisplayNode.hLine(color: AVSColor.dividerSecondary.color)
                case .lineLabel(let label):
                    return ElementLine(label: label)
                case .data(let label, let value):
                    return ElementNode(label: label, value: value)
                case .dataAttributed(let labelAttributed, let valueAttributed):
                    return ElementNode(label: labelAttributed, value: valueAttributed)
                }
            }
            super.init()
            self.automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            let stackElements: [ASLayoutElement] = elementNodes.map { node in
                if let node = node as? ElementNode {
                    return node.margin(vertical: 5)
                }

                if let node = node as? ElementLine {
                    return node.margin(vertical: 5)
                }

                return node.margin(vertical: 10)
            }

            return ASStackLayoutSpec(direction: .vertical,
                                     spacing: 3,
                                     justifyContent: .start,
                                     alignItems: .stretch,
                                     children: stackElements).margin(20)
        }
    }

    struct OrderSummary: Hashable, Differentiable {
        let totalWithoutTax: Float
        let totalTax: Float
        let total: Float

        static func fromResponses(_ responses: [OrderResponseItem], divide: Bool = false) -> OrderSummary {
            let total = responses.reduce(0) { $0 + $1.totalPriceWithTax }
            let totalTax = responses.reduce(0) { $0 + $1.taxAmount }
            let totalWithoutTax = responses.reduce(0) { $0 + $1.totalPriceWithoutTax }
            return OrderSummary(totalWithoutTax: divide ? totalWithoutTax / 100 : totalWithoutTax,
                                totalTax: divide ? totalTax / 100 : totalTax,
                                total: divide ? total / 100 : total)
        }
    }

    static func calculateOrder(_ order: OrderRequestData) -> Promise<(OrderSummary, [OrderResponseItem])> {
        ResidentService().calculateOrder(params: order).map { responses in
            (OrderSummary.fromResponses(responses), responses)
        }
    }
}