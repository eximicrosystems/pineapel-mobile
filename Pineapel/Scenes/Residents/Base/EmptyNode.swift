//
// Created by Andrei Stoicescu on 02/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit

class EmptyNode: ASDisplayNode {
    struct EmptyNodeData {
        let title: String
        let subtitle: String
        let action: String
        let icon: UIImage
    }

    enum EmptyNodeType {
        case userNotification
        case reservation
        case chat
        case allowedGuests
        case request
        case amenityList
        case cart
        case productList
        case orderHistory
        case bellNotifications
        case leaseOffice
        case subscriptions
        case notes
        case conciergeAmenityReservations
        case generic
        case serviceProvider

        var data: EmptyNodeData {
            switch self {
            case .userNotification:
                return EmptyNodeData(title: L10n.Empty.UserNotifications.title,
                                     subtitle: L10n.Empty.UserNotifications.subtitle,
                                     action: L10n.Empty.UserNotifications.action,
                                     icon: Asset.Images.emptyFeedPlaceholder.image)
            case .reservation:
                return EmptyNodeData(title: L10n.Empty.Reservation.title,
                                     subtitle: L10n.Empty.Reservation.subtitle,
                                     action: L10n.Empty.Reservation.action,
                                     icon: Asset.Images.icReservationLarge.image)
            case .chat:
                return EmptyNodeData(title: L10n.Empty.Chat.title,
                                     subtitle: L10n.Empty.Chat.subtitle,
                                     action: L10n.Empty.Chat.action,
                                     icon: Asset.Images.icChatLarge.image)
            case .allowedGuests:
                return EmptyNodeData(title: L10n.Empty.AllowedGuests.title,
                                     subtitle: L10n.Empty.AllowedGuests.subtitle,
                                     action: L10n.Empty.AllowedGuests.action,
                                     icon: Asset.Images.icVisitorLarge.image)
            case .request:
                return EmptyNodeData(title: L10n.Empty.Request.title,
                                     subtitle: L10n.Empty.Request.subtitle,
                                     action: L10n.Empty.Request.action,
                                     icon: Asset.Images.icRequestLarge.image)
            case .amenityList:
                return EmptyNodeData(title: L10n.Empty.AmenityList.title,
                                     subtitle: L10n.Empty.AmenityList.subtitle,
                                     action: L10n.Empty.AmenityList.action,
                                     icon: Asset.Images.icAmenityEmptyLarge.image)
            case .cart:
                return EmptyNodeData(title: L10n.Empty.Cart.title,
                                     subtitle: L10n.Empty.Cart.subtitle,
                                     action: L10n.Empty.Cart.action,
                                     icon: Asset.Images.icGourmetCart.image)
            case .productList:
                return EmptyNodeData(title: L10n.Empty.ProductList.title,
                                     subtitle: L10n.Empty.ProductList.subtitle,
                                     action: L10n.Empty.ProductList.action,
                                     icon: Asset.Images.icGourmetCart.image)
            case .orderHistory:
                return EmptyNodeData(title: L10n.Empty.OrderList.title,
                                     subtitle: L10n.Empty.OrderList.subtitle,
                                     action: L10n.Empty.OrderList.action,
                                     icon: Asset.Images.icGourmetCart.image)
            case .bellNotifications:
                return EmptyNodeData(title: L10n.Empty.BellNotifications.title,
                                     subtitle: L10n.Empty.BellNotifications.subtitle,
                                     action: L10n.Empty.BellNotifications.action,
                                     icon: Asset.Images.emptyFeedPlaceholder.image)
            case .leaseOffice:
                return EmptyNodeData(title: L10n.Empty.LeaseOffice.title,
                                     subtitle: L10n.Empty.LeaseOffice.subtitle,
                                     action: L10n.Empty.LeaseOffice.action,
                                     icon: Asset.Images.emptyFeedPlaceholder.image)
            case .subscriptions:
                return EmptyNodeData(title: L10n.Empty.Subscriptions.title,
                                     subtitle: L10n.Empty.Subscriptions.subtitle,
                                     action: L10n.Empty.Subscriptions.action,
                                     icon: Asset.Images.emptyFeedPlaceholder.image)
            case .notes:
                return EmptyNodeData(title: L10n.Empty.Notes.title,
                                     subtitle: L10n.Empty.Notes.subtitle,
                                     action: L10n.Empty.Notes.action,
                                     icon: Asset.Images.emptyFeedPlaceholder.image)
            case .conciergeAmenityReservations:
                return EmptyNodeData(title: L10n.Empty.ConciergeAmenityReservations.title,
                                     subtitle: L10n.Empty.ConciergeAmenityReservations.subtitle,
                                     action: "",
                                     icon: Asset.Images.emptyFeedPlaceholder.image)
            case .generic:
                return EmptyNodeData(title: "Everything is quiet here.",
                                     subtitle: "No data available yet",
                                     action: "",
                                     icon: Asset.Images.emptyFeedPlaceholder.image)
            case .serviceProvider:
                return EmptyNodeData(title: "No Service Providers",
                                     subtitle: "There are no service providers available for this request",
                                     action: "",
                                     icon: Asset.Images.icRequestLarge.image)
            }
        }
    }

    var image: ASImageNode
    var title = ASTextNode2()
    var subtitle = ASTextNode2()
    var actionButton: ASButtonNode

    var showAction: Bool

    var actionButtonTapped: (() -> Void)?

    init(emptyNodeType: EmptyNodeType, showAction: Bool = true) {
        self.showAction = showAction
        let data = emptyNodeType.data

        title.attributedText = data.title.attributed(.H2, .primaryLabel, alignment: .center)
        subtitle.attributedText = data.subtitle.attributed(.body, .primaryLabel, alignment: .center)
        actionButton = ASButtonNode.primaryButton(title: data.action)
        image = ASImageNode(image: data.icon.withRenderingMode(.alwaysTemplate))

        super.init()

        actionButton.addTarget(self, action: #selector(tappedActionButton), forControlEvents: .touchUpInside)
        image.tintColor = AVSColor.primaryIcon.color
        automaticallyManagesSubnodes = true
    }

    @objc func tappedActionButton() {
        actionButtonTapped?()
    }

    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {

        subtitle.style.flexGrow = 1
        subtitle.style.flexShrink = 1

        let items = showAction ? [image, title, subtitle, actionButton.minWidth(.fraction(0.4))] : [image, title, subtitle]

        return ASStackLayoutSpec(direction: .vertical,
                                 spacing: 40,
                                 justifyContent: .center,
                                 alignItems: .center,
                                 children: items)
                .margin(horizontal: 40).margin(vertical: CGFloat.infinity)
    }
}