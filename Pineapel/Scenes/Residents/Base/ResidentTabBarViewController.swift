//
// Created by Andrei Stoicescu on 05/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import UIKit

class ResidentTabBarViewController: UITabBarController, PushNotificationHandler, PushNotificationActionable {

    var selectedPage: Int!
    
    enum ControllerType: Int, CaseIterable {
        case feed
        case chat
        case amenities
        case aLaCarte

        var icon: UIImage {
            switch self {
            case .feed:
                return Asset.Images.Concierge.icPineHome.image
            case .chat:
                return Asset.Images.Concierge.icPineHome.image
                //return Asset.Images.Concierge.icChatNav.image
            case .amenities:
                return Asset.Images.Concierge.icPineHome.image
                //return Asset.Images.Concierge.icCalendar.image
            case .aLaCarte:
                return Asset.Images.Concierge.icPineHome.image
                //return Asset.Images.Concierge.icAlacarte.image
//            case .more:
//                return Asset.Images.Concierge.icMore.image
            }
        }

        var title: String {
            switch self {
            case .feed:
                return L10n.UserNotificationsScreen.Navigation.tabBarTitle
            case .chat:
                return L10n.ChatScreen.Navigation.tabBarTitle
            case .amenities:
                return L10n.AmenityListScreen.Navigation.tabBarTitle
            case .aLaCarte:
                return "OFFERING"
//            case .more:
//                return L10n.MoreScreen.Navigation.tabBarTitle
            }
        }
    }
    
    init(page: Int?) {
        selectedPage = page
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var controllerTypes = [ControllerType]()

    func buildControllerType(_ controllerType: ControllerType) -> UIViewController {
        switch controllerType {
        case .feed:
            return createTabBarController(icon: controllerType.icon, controller: FeedNotificationsListViewController(), title: controllerType.title)
        case .chat:
            return createTabBarController(icon: controllerType.icon, controller: ConciergeChat.ResidentChatLoaderViewController(), title: controllerType.title)
        case .amenities:
            return createTabBarController(icon: controllerType.icon, controller: AmenityListViewController(requiresReservation: true), title: controllerType.title)
        case .aLaCarte:
            return createTabBarController(icon: controllerType.icon, controller: OfferingViewController(cartManager: .shared), title: controllerType.title)
//        case .more:
//            return createTabBarController(icon: controllerType.icon, controller: ResidentMoreViewController(), title: controllerType.title)
        }
    }

    func getAllowedControllerTypes() -> [ControllerType] {
        let features = AppState.features
        return ControllerType.allCases.filter { controllerType in
            switch controllerType {
            case .feed: //, .more:
                return true
            case .chat:
                return features.enabledFlags.contains(.chat)
            case .amenities:
                return features.enabledFlags.contains(.reservations)
            case .aLaCarte:
                return features.enabledFlags.contains(.aLaCarte)
            }
        }
    }

    func resetFeatureFlagsControllers() {
        guard let vcs = viewControllers, vcs.count > 0 else {
            viewControllers = controllerTypes.map { buildControllerType($0) }
            return
        }

        let allowedTypes = getAllowedControllerTypes()

        if allowedTypes == controllerTypes {
            return
        }

        let currentControllers = controllerTypes.enumerated().map { idx, el in
            (el, vcs[idx])
        }

        controllerTypes = allowedTypes

        viewControllers = allowedTypes.map { type -> UIViewController in
            if let existing = currentControllers.first(where: { $0.0 == type }){ //}, type != .more {
                return existing.1
            } else {
                return buildControllerType(type)
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        AppState.featureFlagsChanged = { [weak self] in
            DispatchQueue.main.async { [weak self] in
                self?.resetFeatureFlagsControllers()
            }
        }

        view.backgroundColor = AVSColor.colorMilitaryGreen.color
        
        updateAppVersion()

        controllerTypes = getAllowedControllerTypes()

        resetFeatureFlagsControllers()

        selectedViewController = viewControllers?.first
        if selectedPage != nil {
            selectedViewController = viewControllers?[selectedPage]
        }
        
        
        // Fetch user when launching the app initially. TODO: should be done somewhere else?
        ResidentService().getUser().cauterize()

        AVSPushNotifications.shared.registerForPush()

        handlePushMessage()
    }

    func updateAppVersion() {
        ResidentService().updateSettings(Settings(appVersion: AppState.appVersion)).cauterize()
    }

    func createTabBarController(icon: UIImage, controller: UIViewController, title: String) -> UINavigationController {
        let navController = AVSNavigationController(rootViewController: controller)
        navController.tabBarItem = UITabBarItem(title: title, image: icon, selectedImage: icon)
        return navController
    }
    
    static func buildNavControllerType(title: String, vc: UIViewController) -> UIViewController {
        return createNavBarController(icon: Asset.Images.Concierge.icPineHome.image, controller: vc, title: title)
    }
    
    static func createNavBarController(icon: UIImage, controller: UIViewController, title: String) -> UINavigationController {
        let navController = AVSNavigationController(rootViewController: controller)
        navController.tabBarItem = UITabBarItem(title: title, image: icon, selectedImage: icon)
        return navController
    }

    func switchTo(controllerType: ControllerType) {
        guard let typeIndex = controllerTypes.firstIndex(of: controllerType) else { return }
        selectedIndex = typeIndex
    }

    func controllerForType(_ controllerType: ControllerType) -> UIViewController? {
        guard let typeIndex = controllerTypes.firstIndex(of: controllerType) else { return nil }
        guard let nav = viewControllers?[safe: typeIndex] as? UINavigationController else { return nil }
        return nav.viewControllers.first
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        guard let items = tabBar.items else { return }

        // This is done here because we don't get the correct tab bar frame in viewDidLoad
        let tabBarButtonSize = CGSize(width: tabBar.frame.width / CGFloat(items.count), height: tabBar.frame.height)
        if let image = UIImage.createSelectionIndicator(color: AVSColor.colorMilitaryGreen.color, size: tabBarButtonSize, lineHeight: 2) {
            tabBar.selectionIndicatorImage = image
        }
    }

    func processNotification(_ message: PushMessage, completion: @escaping PushMessageCompletion) {
        guard let feedVC = controllerForType(.feed) as? FeedNotificationsListViewController else {
            completion(.noData)
            return
        }

        if let unread = message.unread {
            feedVC.updateBadgeCount(unread)
        }

        if message.clickAction?.type == .notification {
            feedVC.reloadNotifications(completion: completion)
        } else {
            completion(.noData)
        }
    }

    func processActionable(_ message: PushMessage) {
        guard let category = message.clickAction else { return }
        switch category.type {
        case .channel:
            switchTo(controllerType: .chat)
        case .request:
            ()
        case .notification:
            guard let feedVC = controllerForType(.feed) as? FeedNotificationsListViewController else { return }
            switchTo(controllerType: .feed)
            feedVC.processActionable(message)
        }
    }

    // this is for starting the app from force-quit state
    func handlePushMessage() {
        if let pushMessage = AppState.pushMessage {
            AppState.pushMessage = nil
            processActionable(pushMessage)
        }
    }

    #if DEBUG
    override func becomeFirstResponder() -> Bool {
        true
    }

    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            NetworkActivityLogger.shared.toggleLogging()
        }
    }
    #endif
}
