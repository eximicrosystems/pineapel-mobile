//
// Created by Andrei Stoicescu on 14/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit

struct GenericNodes {
    // using ASNetworkImageNode with imageModificationBlock sometimes fails on GIFs, so we wrap it in a node
    class AvatarNode: ASControlNode, ASNetworkImageNodeDelegate {

        struct NameFallback {
            let names: [String?]
            let backgroundColor: UIColor
            let font: UIFont
            let textColor: UIColor
            var image: UIImage? = nil

            var initials: String {
                names.compactMap { $0?.prefix(1).uppercased() }.prefix(2).joined()
            }

            static func initials(_ names: [String?]) -> NameFallback {
                NameFallback(names: names,
                             //backgroundColor: AVSColor.accentForeground.color,
                             backgroundColor: AVSColor.colorMilitaryGreen.color,
                             font: AVSFont.body.font,
                             textColor: AVSColor.colorMilitaryGreen.color)
            }

//            static func icon(_ icon: UIImage?, backgroundColor: UIColor = AVSColor.accentForeground.color) -> NameFallback {
            static func icon(_ icon: UIImage?, backgroundColor: UIColor = AVSColor.colorMilitaryGreen.color) -> NameFallback {
                NameFallback(names: [],
                             backgroundColor: backgroundColor,
                             font: AVSFont.body.font,
                             textColor: AVSColor.colorMilitaryGreen.color,
                             image: icon)
            }
        }

        var url: URL? {
            didSet {
                avatarImage.url = url
            }
        }

        private var avatarImage = ASNetworkImageNode()
        private var placeholderIcon: ASImageNode?
        private var initialsText: ASTextNode2?

        override func asyncTraitCollectionDidChange(withPreviousTraitCollection previousTraitCollection: ASPrimitiveTraitCollection) {
            super.asyncTraitCollectionDidChange(withPreviousTraitCollection: previousTraitCollection)
            if asyncTraitCollection().userInterfaceStyle != previousTraitCollection.userInterfaceStyle {
                avatarImage.borderColor = AVSColor.borderPrimary.color.cgColor
            }
        }

        init(size: CGFloat, shadow: Bool = true, border: Bool = true, nameFallback: NameFallback? = nil, tintColor: UIColor? = nil, rounded: Bool = true) {
            avatarImage.contentMode = .scaleAspectFill
            avatarImage.clipsToBounds = true

            if border {
                avatarImage.borderWidth = 2
                avatarImage.borderColor = AVSColor.borderPrimary.color.cgColor
            }

            if rounded {
                avatarImage.cornerRadius = size / 2
            }

            avatarImage.backgroundColor = nameFallback?.backgroundColor ?? AVSColor.borderPrimary.color
            super.init()

            avatarImage.delegate = self

            if shadow {
                shadowColor = UIColor.systemGray.cgColor
                shadowOpacity = 0.2
                shadowRadius = 1
                shadowOffset = CGSize(width: 0, height: 2)
            }

            if let tintColor = tintColor {
                avatarImage.tintColor = tintColor
            }

            if let fallback = nameFallback {
                if let image = fallback.image {
                    placeholderIcon = ASImageNode(image: image)
                    placeholderIcon?.tintColor = tintColor
                } else {
                    initialsText = ASTextNode2(with: NSAttributedString.string(text: fallback.initials, font: fallback.font, color: fallback.textColor))
                }
            }

            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            if let initials = initialsText {
                return initials.centered().overlay(avatarImage)
            } else if let icon = self.placeholderIcon {
                return icon.centered().overlay(avatarImage)
            }

            return avatarImage.wrapped()
        }

        func imageNode(_ imageNode: ASNetworkImageNode, didLoad image: UIImage) {
            initialsText?.isHidden = true
            placeholderIcon?.isHidden = true
        }

        func imageNode(_ imageNode: ASNetworkImageNode, didFailWithError error: Error) {
            initialsText?.isHidden = false
            placeholderIcon?.isHidden = false
            imageNode.url = nil
            imageNode.image = nil
        }
    }

    class LabelCellNode: ASCellNode {
        var label: String?
        var labelRight: String?
        var marginBottom: CGFloat = 5

        var backgroundNode = ASDisplayNode()
        var labelNode = ASTextNode2()
        var labelRightNode = ASTextNode2()

        init(text: String?, textRight: String? = nil) {
            label = text
            labelRight = textRight

            backgroundNode.backgroundColor = AVSColor.secondaryBackground.color

            labelNode.attributedText = text?.attributed(.bodyHeavy, .primaryLabel)
            labelRightNode.attributedText = textRight?.attributed(.body, .accentBackground)

            super.init()
            self.automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            let element: ASLayoutElement
            if labelRight != nil {
                element = ASStackLayoutSpec(direction: .horizontal, spacing: 10, justifyContent: .spaceBetween, alignItems: .center, children: [labelNode, labelRightNode])
            } else {
                element = labelNode
            }

            return element.margin(15).background(backgroundNode).margin(bottom: marginBottom)
        }
    }

    class PrimaryButtonCellNode: ASCellNode {
        var button: ASButtonNode
        var useInsets: Bool

        var tapAction: (() -> Void)? {
            didSet {
                button.isUserInteractionEnabled = tapAction != nil
            }
        }

        init(title: String, insets: Bool = true) {

            useInsets = insets

            button = .primaryButton(title: title)

            super.init()
            button.addTarget(self, action: #selector(tappedButton), forControlEvents: .touchUpInside)
            button.isUserInteractionEnabled = false
            automaticallyManagesSubnodes = true
        }

        @objc func tappedButton() {
            tapAction?()
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            button.style.height = ASDimensionMakeWithPoints(50)
            return button.margin(useInsets ? UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15) : .zero)
        }
    }

    class HeaderCell: ASCellNode {
        var titleLabel = ASTextNode2()

        init(title: String?) {
            super.init()
            titleLabel.attributedText = title?.attributed(.bodyTitleHeavy, .primaryLabel)
            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            titleLabel.margin(top: 15, left: 15, bottom: 5, right: 15)
        }
    }

    class TitleSubtitleCell: ASCellNode {
        var lineNode = ASDisplayNode()
        var titleLabel = ASTextNode2()
        var subtitleLabel = ASTextNode2()
        var rightLabelNode: ASTextNode2?

        init(title: String?, subtitle: String?, rightLabel: String? = nil) {
            lineNode.backgroundColor = AVSColor.divider.color

            titleLabel.attributedText = title?.attributed(.bodyHeavy, .primaryLabel)
            subtitleLabel.attributedText = subtitle?.attributed(.body, .secondaryLabel)

            if let label = rightLabel {
                rightLabelNode = .init(with: label.attributed(.body, .primaryLabel))
            }

            super.init()
            backgroundColor = AVSColor.secondaryBackground.color
            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            [
                [
                    [titleLabel, subtitleLabel].vStacked().aligned(.start).filling(),
                    rightLabelNode
                ].hStacked().justified(.spaceBetween).spaced(10)
                 .margin(15),
                lineNode.height(.points(1))
            ].vStacked()
        }
    }

    class UserBioCell: ASCellNode {
        static let avatarSize: CGFloat = 100

        var avatar: GenericNodes.AvatarNode
        var name = ASTextNode2()
        var title = ASTextNode2()
        var about = ASTextNode2()
        var backgroundNode = ASDisplayNode()

        init(_ bio: UserBioCellProtocol, body: NSAttributedString?) {
            avatar = GenericNodes.AvatarNode(size: UserBioCell.avatarSize)
            avatar.url = bio.userBioImageURL

            name = ASTextNode2(with: NSAttributedString.string(text: bio.userBioName, font: .NormalTextHead, color: .black))
            title = ASTextNode2(with: NSAttributedString.string(text: bio.userBioPosition, font: .Note))
            about = ASTextNode2(with: body)

            backgroundNode.backgroundColor = .white

            super.init()

            automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            about.style.alignSelf = .stretch
            about.style.spacingBefore = 20
            avatar.style.preferredSize = CGSize(width: UserBioCell.avatarSize, height: UserBioCell.avatarSize)
            return ASStackLayoutSpec(
                    direction: .vertical,
                    spacing: 15,
                    justifyContent: .start,
                    alignItems: .center,
                    children: [avatar, name, title, about]
            ).margin(20)
        }
    }

    class UserBioListCell: ASCellNode {
        static let avatarSize: CGFloat = 80

        var avatar: GenericNodes.AvatarNode
        var name = ASTextNode2()
        var title = ASTextNode2()
        var backgroundNode = ASDisplayNode()

        init(user: UserBioCellProtocol) {
            avatar = GenericNodes.AvatarNode(size: UserBioListCell.avatarSize)
            avatar.url = user.userBioImageURL

            backgroundNode.backgroundColor = .white

            name.attributedText = NSAttributedString.string(text: user.userBioName, font: .NormalTextHead, color: .black)
            title.attributedText = NSAttributedString.string(text: user.userBioPosition, font: .Note)

            super.init()

            self.automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            let labelsStack = ASStackLayoutSpec(
                    direction: .vertical,
                    spacing: 5,
                    justifyContent: .start,
                    alignItems: .start,
                    children: [name, title]
            )

            labelsStack.style.flexGrow = 1

            avatar.style.preferredSize = CGSize(width: UserBioListCell.avatarSize, height: UserBioListCell.avatarSize)

            let avatarLabelsStack = ASStackLayoutSpec(
                    direction: .horizontal,
                    spacing: 20,
                    justifyContent: .start,
                    alignItems: .center,
                    children: [avatar, labelsStack]
            )

            let contentMargin = UIEdgeInsets(top: 10, left: 15, bottom: 10, right: 15)
            let bottomMargin = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)

            return avatarLabelsStack
                    .margin(contentMargin)
                    .background(backgroundNode)
                    .margin(bottomMargin)
        }
    }

    class ArticleCell: ASCellNode {
        class Thumbnail {
            let url: URL?
            let ratio: CGFloat
            let insets: UIEdgeInsets

            init(url: URL?, ratio: CGFloat = 9 / 16, insets: UIEdgeInsets = .zero) {
                self.url = url
                self.ratio = ratio
                self.insets = insets
            }
        }

        static let margin: CGFloat = 20

        var thumbnailImage = ASNetworkImageNode()
        var title = ASTextNode2()
        var body = ASTextNode2()
        var backgroundNode = ASDisplayNode()
        var hasBottomMargin: Bool
        var hasTopMargin: Bool
        var thumbnail: Thumbnail
        var bottomNode: ASDisplayNode?

        init(thumbnail: Thumbnail,
             title: String?,
             body: NSAttributedString?,
             hasBottomMargin: Bool = false,
             hasTopMargin: Bool = false,
             bottomNode: ASDisplayNode? = nil) {

            self.bottomNode = bottomNode
            self.thumbnail = thumbnail

            self.thumbnailImage.url = thumbnail.url
            self.hasBottomMargin = hasBottomMargin
            self.hasTopMargin = hasTopMargin

            self.title.attributedText = NSAttributedString.string(text: title, font: Asset.Fonts.NormalTextHead.uiFont, color: .black)
            self.body.attributedText = body
            self.backgroundNode.backgroundColor = .white

            super.init()
            self.automaticallyManagesSubnodes = true
        }

        override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            let labelsStack = ASStackLayoutSpec(direction: .vertical,
                                                spacing: 20,
                                                justifyContent: .start,
                                                alignItems: .start,
                                                children: [title, body])

            return ASStackLayoutSpec(
                    direction: .vertical,
                    spacing: 0,
                    justifyContent: .start,
                    alignItems: .stretch,
                    children: [
                        thumbnailImage.ratio(thumbnail.ratio).margin(thumbnail.insets),
                        labelsStack.margin(15),
                        bottomNode?.margin(left: 15, bottom: 15, right: 15)
                    ].compactMap { $0 })
                    .background(backgroundNode)
                    .margin(top: hasTopMargin ? ArticleCell.margin : 0,
                                bottom: hasBottomMargin ? ArticleCell.margin : 0)
        }
    }
}
