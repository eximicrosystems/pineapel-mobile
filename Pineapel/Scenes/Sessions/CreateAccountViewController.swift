//
// Created by Andrei Stoicescu on 04/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import SwiftyUserDefaults
import SVProgressHUD
import Validator

class CreateAccountViewController: BaseSessionsViewController {

    lazy var submitButton = UIButton.primaryButton(title: L10n.SessionsScreen.Buttons.Submit.title)
    var hasTerms: Bool = true

    lazy var emailFieldContainer: AVSTextFieldContainer = {
        let emailFieldContainer = AVSTextFieldContainer(
                placeholder: L10n.SessionsScreen.Fields.EmailAddress.placeholder,
                noteText: L10n.SessionsScreen.Fields.EmailAddress.note,
                text: nil
        )

        emailFieldContainer.textField.keyboardType = .emailAddress

        #if DEBUG
        emailFieldContainer.text = "andrei4003@gmail.com"
        #endif

        return emailFieldContainer
    }()

    lazy var termsAndPrivacyLabel: UITextView = {
        SessionsHelpers.createTermsLabel()
    }()

    override func viewDidLoad() {
        titleLabelString = L10n.SessionsScreen.title
        subtitleLabelString = L10n.SessionsScreen.subtitle
        var buttons: [UIView] = [submitButton]
        if hasTerms {
            buttons.append(termsAndPrivacyLabel)
        }
        self.buttons = buttons
        fields = [emailFieldContainer]

        super.viewDidLoad()
        replaceSystemBackButton()

        // Setup Events
        submitButton.addTarget(self, action: #selector(createAccount), for: .touchUpInside)
    }

    func validate() -> Bool {
        guard let email = emailFieldContainer.text else {
            SVProgressHUD.showError(withStatus: L10n.Errors.cantBeBlank(L10n.SessionsScreen.Fields.EmailAddress.placeholder))
            return false
        }

        let emailFormatRule = ValidationRulePattern(pattern: EmailValidationPattern.standard, error: AVSValidationError(L10n.Errors.emailInvalid))

        let result = email.validate(rule: emailFormatRule)
        if !result.isValid {
            SVProgressHUD.showError(withStatus: emailFormatRule.error.message)
            return false
        }

        return true
    }

    @objc func createAccount(_ sender: UIButton) {
        if (validate()) {
            guard let email = emailFieldContainer.text else {
                return
            }

            SVProgressHUD.show()
            let authService = AuthService()

            authService.requestNewPassword(email: email).done { [weak self] in
                SVProgressHUD.dismiss()
                self?.navigationController?.pushViewController(CreateAccountVerificationViewController.init(email: email), animated: true)
            }.catch { error in
                SVProgressHUD.showError(withStatus: error.localizedDescription)
            }
        }
    }
}
