//
// Created by Andrei Stoicescu on 17.02.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//


import Foundation
import UIKit
import SnapKit

class UpdateAppViewController: UIViewController {
    class UpdateView: UIView {
        lazy var logoImageView: UIImageView = {
            let image = Asset.Images.appLogoSmall.image
            let imageView = UIImageView(image: image)
            imageView.contentMode = .scaleAspectFit
            imageView.translatesAutoresizingMaskIntoConstraints = false
            return imageView
        }()

        init() {
            super.init(frame: .zero)

            let infoLabel = UILabel()
            infoLabel.font = AVSFont.H4.font
            infoLabel.textColor = AVSColor.primaryLabel.color
            infoLabel.numberOfLines = 0
            infoLabel.textAlignment = .center
            infoLabel.text = L10n.ForceUpdate.title

            let updateButton = UIButton.primaryButton(title: L10n.ForceUpdate.subtitle)
            updateButton.addTarget(self, action: #selector(tappedUpdate), for: .touchUpInside)

            let stackView = UIStackView()
            stackView.translatesAutoresizingMaskIntoConstraints = false
            stackView.axis = .vertical
            stackView.alignment = .fill
            stackView.distribution = .fill
            stackView.spacing = 40

            addSubview(stackView)

            stackView.addArrangedSubview(logoImageView)
            stackView.addArrangedSubview(infoLabel)
            stackView.addArrangedSubview(updateButton)

            updateButton.snp.makeConstraints { maker in
                maker.height.equalTo(50)
            }

            logoImageView.snp.makeConstraints { make in
                make.height.equalTo(200)
            }

            stackView.snp.makeConstraints { make in
                make.edges.equalToSuperview()
            }
        }

        @objc func tappedUpdate() {
            if let url = URL(string: "https://apps.apple.com/in/app/id1518368257") {
                UIApplication.shared.open(url)
            }
        }

        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let updateView = UpdateView()

        view.addSubview(updateView)
        updateView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
        }
    }
}
