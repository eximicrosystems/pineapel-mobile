//
// Created by Andrei Stoicescu on 03/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import SwiftyUserDefaults
import SVProgressHUD
import Validator

class CreateAccountVerificationViewController: BaseSessionsViewController {

    var email: String

    init(email: String) {
        self.email = email
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    lazy var createAccountButton = UIButton.primaryButton(title: L10n.SessionsScreen.Buttons.CreateAccountSubmit.title)

    lazy var codeFieldContainer: AVSTextFieldContainer = {
        let container = AVSTextFieldContainer(
                placeholder: L10n.SessionsScreen.Fields.VerificationCode.placeholder
        )

        container.textField.keyboardType = .numberPad

        return container
    }()

    lazy var passwordFieldContainer: AVSTextFieldContainer = {
        let container = AVSTextFieldContainer(
                placeholder: L10n.SessionsScreen.Fields.Password.placeholder,
                noteText: L10n.SessionsScreen.Fields.Password.note
        )

        container.textField.isSecureTextEntry = true

        return container
    }()

    lazy var termsAndPrivacyLabel: UITextView = {
        SessionsHelpers.createTermsLabel()
    }()

    override func viewDidLoad() {
        replaceSystemBackButton()

        titleLabelString = L10n.SessionsScreen.title
        subtitleLabelString = "" //L10n.SessionsScreen.subtitle
        buttons = [createAccountButton, termsAndPrivacyLabel]
        fields = [codeFieldContainer, passwordFieldContainer]

        super.viewDidLoad()

        replaceSystemBackButton()

        // Setup Events
        createAccountButton.addTarget(self, action: #selector(createAccount), for: .touchUpInside)
    }

    func validate() -> Bool {
        guard !String.isBlank(codeFieldContainer.text) else {
            SVProgressHUD.showError(withStatus: L10n.Errors.cantBeBlank(L10n.SessionsScreen.Fields.VerificationCode.placeholder))
            return false
        }

        guard !String.isBlank(passwordFieldContainer.text) else {
            SVProgressHUD.showError(withStatus: L10n.Errors.cantBeBlank(L10n.SessionsScreen.Fields.Password.placeholder))
            return false
        }

        return true
    }

    @objc func createAccount(_ sender: UIButton) {
        if (validate()) {
            guard let code = codeFieldContainer.text else {
                return
            }

            guard let password = passwordFieldContainer.text else {
                return
            }

            let email = self.email
            let authService = AuthService()

            SVProgressHUD.show()
            authService.setNewPassword(email: email, code: code, password: password).then {
                authService.login(user: email, password: password)
            }.done {
                SVProgressHUD.dismiss()
                AppState.switchToMainViewController()
            }.catch { error in
                SVProgressHUD.showError(withStatus: error.localizedDescription)
            }
        }
    }
}
