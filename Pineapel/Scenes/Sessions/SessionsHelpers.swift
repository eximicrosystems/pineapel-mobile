//
// Created by Andrei Stoicescu on 25/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit

class SessionsHelpers {
    class UnselectableTextView: UITextView {
        override public var selectedTextRange: UITextRange? {
            get {
                nil
            }
            set { }
        }

        override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {

            if let tapGestureRecognizer = gestureRecognizer as? UITapGestureRecognizer,
               tapGestureRecognizer.numberOfTapsRequired == 1 {
                // required for compatibility with links
                return super.gestureRecognizerShouldBegin(gestureRecognizer)
            }

            return false
        }
    }

    static func createTermsLabel() -> UITextView {
        let label = UnselectableTextView()
        let linkAttributedString = NSLinkAttributedString(textAttributes: NSAttributedString.attributes(font: AVSFont.noteMedium.font,
                                                                                                        color: AVSColor.secondaryLabel.color,
                                                                                                        alignment: .center))

        linkAttributedString.append(text: L10n.Terms.byContinuing)
        linkAttributedString.append(text: L10n.Terms.terms, url: URL(string: "https://avotreservice.us/terms-of-use/"))
        linkAttributedString.append(text: L10n.Terms.confirmRead)
        linkAttributedString.append(text: L10n.Terms.privacyPolicy, url: URL(string: "https://avotreservice.us/our-privacy-policy/"))

        label.linkTextAttributes = NSAttributedString.attributes(font: AVSFont.noteMedium.font,
                                                                 color: AVSColor.secondaryLabel.color,
                                                                 alignment: .left)
        label.attributedText = linkAttributedString.attributedString
        label.backgroundColor = .clear
        label.isEditable = false
        label.isScrollEnabled = false
        return label
    }
}
