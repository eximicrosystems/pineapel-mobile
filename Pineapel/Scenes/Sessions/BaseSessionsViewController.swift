//
//  SessionsViewController.swift
//  A Votre Service
//
//  Created by Andrei Stoicescu on 02/06/2020.
//  Copyright © 2020 A Votre Service LLC. All rights reserved.
//

import UIKit
import SnapKit
import SwiftyUserDefaults
import SVProgressHUD
import IQKeyboardManagerSwift
import Validator

struct AVSValidationError: ValidationError {
    let message: String

    public init(_ message: String) {
        self.message = message
    }
}

class BaseSessionsViewController: UIViewController {

    var logoImage = UIImage(asset: Asset.Images.appLogo)
    var titleLabelString = ""
    var subtitleLabelString = ""
    var buttons: [UIView] = []
    var fields: [UIView] = []

    lazy var backgroundImageView: UIImageView = {
        let imageView = UIImageView(image: Asset.Images.bgSplash.image)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()

    lazy var logoImageView: UIImageView = {
        let image = logoImage
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = AVSColor.accentForeground.color
        label.font = AVSFont.H3.font
        label.text = titleLabelString
        label.textAlignment = NSTextAlignment.center
        return label
    }()

    lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = AVSColor.accentBackground.color
        label.font = AVSFont.H3.font
        label.text = subtitleLabelString
        return label
    }()

    lazy var iqView = IQPreviousNextView()

    override func viewDidLoad() {
        super.viewDidLoad()
        replaceSystemBackButton()

        view.addSubview(backgroundImageView)
        view.addSubview(iqView)

        let mainStackView = UIStackView()
        mainStackView.translatesAutoresizingMaskIntoConstraints = false
        mainStackView.axis = .vertical
        mainStackView.distribution = .equalSpacing
        mainStackView.alignment = .fill
        iqView.addSubview(mainStackView)

        mainStackView.addArrangedSubview(UIView())

        mainStackView.addArrangedSubview(logoImageView)

        logoImageView.snp.makeConstraints { make in
            make.height.equalTo(130)
        }

        let titlesStackView = UIStackView()
        titlesStackView.translatesAutoresizingMaskIntoConstraints = false
        titlesStackView.axis = .vertical
        titlesStackView.distribution = .fill
        titlesStackView.alignment = .fill
        titlesStackView.spacing = 0

        titlesStackView.addArrangedSubview(titleLabel)
        titlesStackView.addArrangedSubview(subtitleLabel)

        mainStackView.addArrangedSubview(titlesStackView)

        let fieldsStack = UIStackView()
        fieldsStack.translatesAutoresizingMaskIntoConstraints = false
        fieldsStack.axis = .vertical
        fieldsStack.distribution = .fill
        fieldsStack.alignment = .fill
        fieldsStack.spacing = 15

        fields.forEach {
            fieldsStack.addArrangedSubview($0)
        }

        mainStackView.addArrangedSubview(fieldsStack)

        let buttonsStack = UIStackView()
        buttonsStack.translatesAutoresizingMaskIntoConstraints = false
        buttonsStack.axis = .vertical
        buttonsStack.distribution = .fill
        buttonsStack.alignment = .fill
        buttonsStack.spacing = 10

        buttons.forEach {
            buttonsStack.addArrangedSubview($0)
        }

        mainStackView.addArrangedSubview(buttonsStack)
        mainStackView.addArrangedSubview(UIView())

        // Setup constraints
        buttons.filter { view in
            if let _ = view as? UITextView {
                return false
            }
            return true
        }.forEach {
            $0.snp.makeConstraints { make in
                make.height.equalTo(50)
            }
        }

        backgroundImageView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        iqView.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.topMargin)
            make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottomMargin)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
        }

        mainStackView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
