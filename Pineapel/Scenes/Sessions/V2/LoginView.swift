// //
// //  LoginView.swift
// //  A Votre Service
// //
// //  Created by Andrei Stoicescu on 14.12.2020.
// //  Copyright © 2020 A Votre Service LLC. All rights reserved.
// //
//
// import SwiftUI
// import Combine
// import SVProgressHUD
//
//  struct KeyboardIgnoreSafeArea: ViewModifier {
//      func body(content: Content) -> some View {
//          Group {
//              if #available(iOS 14.0, *) {
//                  content.ignoresSafeArea(.keyboard, edges: .bottom)
//              } else {
//                  content
//              }
//          }
//      }
//  }
//
//  extension View {
//      func compatibleIgnoreKeyboardSafeArea() -> some View {
//          self.modifier(KeyboardIgnoreSafeArea())
//      }
//  }
//
// struct LoginView: View {
//     @State var email: String = ""
//     @State var password: String = ""
//     @State var biometricsOn: Bool = false
//     @State var loginTitle = ""
//
//     private var biometrics = Biometrics()
//
//     var content: some View {
//         VStack {
//             Image(uiImage: Asset.Images.appLogoSmall.image)
//                 .resizable()
//                 .aspectRatio(contentMode: .fit)
//                 .frame(maxHeight: 130)
//
//             Spacer()
//
//             VStack {
//                 HStack {
//                     Text(L10n.SessionsScreen.title)
//                         .font(Asset.Fonts.H1.font)
//                         .foregroundColor(Asset.Colors.secondary.sColor)
//                     Spacer()
//                 }
//
//                 HStack {
//                     Text(L10n.SessionsScreen.subtitle)
//                         .font(Asset.Fonts.H2Normal.font)
//                         .foregroundColor(Asset.Colors.primary.sColor)
//                     Spacer()
//                 }
//             }
//
//             Spacer()
//
//             VStack(spacing: 10) {
//                 VStack(spacing: 20) {
//                     ConciergeForm.TextInput(label: L10n.SessionsScreen.Fields.EmailAddress.placeholder,
//                                             value: $email)
//
//                     ConciergeForm.SecureTextInput(label: L10n.SessionsScreen.Fields.Password.placeholder,
//                                                   value: $password)
//                 }
//
//                 HStack {
//                     Button(action: {
//
//                     }, label: {
//                         Text(L10n.SessionsScreen.Buttons.ForgotPassword.title)
//                             .foregroundColor(Asset.Colors.primary.sColor)
//                             .font(Asset.Fonts.Body.font)
//                     })
//
//                     Spacer()
//
//                     if let biometryTypeString = biometrics.biometryTypeString {
//                         Text("Enable \(biometryTypeString)")
//                             .foregroundColor(Asset.Colors.secondary.sColor)
//                             .font(Asset.Fonts.Body.font)
//                         Toggle(isOn: $biometricsOn) {
//                             EmptyView()
//                         }
//                         .labelsHidden()
//                         .onReceive(Just(biometricsOn)) { value in
//                             if value {
//                                 biometrics.authenticate(saveResult: true).catch { error in
//                                     biometricsOn = false
//                                     Log.thisError(error)
//                                 }.finally {
//                                     loginTitle = loginButtonTitle()
//                                 }
//                             } else {
//                                 Biometrics().isEnabled = false
//                                 loginTitle = loginButtonTitle()
//                             }
//                         }
//                     }
//                 }
//             }
//
//             Spacer()
//
//             Button(action: {
//                 handleLogin()
//             }, label: {
//                 Text(loginTitle)
//                     .foregroundColor(.white)
//                     .padding()
//                     .frame(maxWidth: .infinity)
//                     .background(Asset.Colors.primary.sColor)
//                     .clipShape(Capsule())
//                     .padding(.vertical)
//             })
//
//             HStack {
//                 Text("Don't have an account?")
//                     .foregroundColor(Asset.Colors.secondary.sColor)
//
//                 Button(action: {
//
//                 }, label: {
//                     Text("Create one")
//                         .foregroundColor(Asset.Colors.primary.sColor)
//
//                 })
//             }.font(Asset.Fonts.Body.font)
//
//         }
//         .padding(.horizontal, 30)
//         .padding(.vertical)
//         .compatibleIgnoreKeyboardSafeArea()
//     }
//
//     var body: some View {
//         content
//             .background(Image(uiImage: Asset.Images.bgSplash.image))
//             .onAppear {
//                 biometricsOn = biometrics.isEnabled
//                 loginTitle = loginButtonTitle()
//
//                 if biometrics.shouldAuthenticate, let user = KeychainHelper().user {
//                     biometrics.authenticate().done {
//                         login(email: user.email, password: user.password)
//                     }.cauterize()
//                 }
//             }
//     }
//
//     func handleLogin() {
//         if biometrics.shouldAuthenticate, let user = KeychainHelper().user {
//             biometrics.authenticate().done {
//                 login(email: user.email, password: user.password)
//             }.cauterize()
//         } else {
//
//             guard email.count > 0, password.count > 0 else {
//                 SVProgressHUD.showError(withStatus: L10n.Errors.cantBeBlank("email and password"))
//                 return
//             }
//
//             login(email: email, password: password)
//         }
//     }
//
//     func login(email: String, password: String) {
//            SVProgressHUD.show()
//            AuthService().login(user: email, password: password).done {
//                SVProgressHUD.dismiss()
//                AppState.switchToMainViewController()
//            }.catch { error in
//                SVProgressHUD.showError(withStatus: error.localizedDescription)
//            }
//        }
//
//     func loginButtonTitle() -> String {
//         if biometrics.shouldAuthenticate, KeychainHelper().user != nil, let biometryType = biometrics.biometryTypeString {
//             return L10n.SessionsScreen.Buttons.loginWith(biometryType)
//         } else {
//             return L10n.SessionsScreen.Buttons.Login.title
//         }
//     }
// }
//
// struct LoginView_Previews: PreviewProvider {
//     static var previews: some View {
//         LoginView()
//     }
// }
