//
//  PopTableView.swift
//  A Votre Service
//
//  Created by Oscar Sevilla on 17/08/21.
//  Copyright © 2021 A Votre Service LLC. All rights reserved.
//

import UIKit

class PopTableView: UIView, UITableViewDataSource {
    
    @IBOutlet var table: UITableView!
    
    private var botones: [String]!
    
    init?(tBotones: [String]) {
        botones = tBotones
        super.init(coder: NSCoder())
        commonInit()
        table.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        table.dataSource = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func commonInit() {
        let viewFromXib = Bundle.main.loadNibNamed("PopTableView", owner: self, options: nil)![0] as! UIView
        viewFromXib.frame = self.bounds
        addSubview(viewFromXib)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return botones.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = botones[indexPath.row]
        return cell
    }
}
