//
//  HeaderTabBarView.swift
//  A Votre Service
//
//  Created by Oscar Sevilla Garduño on 04/08/21.
//  Copyright © 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit

class HeaderTabBarView: UIView {
    
    @IBOutlet var buttonMenu: UIButton!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var chatButton: UIButton!
    
    var lateralMenu: LateralMenuView!
    var tabBar: UIViewController!
    
    init(frame: CGRect, tabBarVC: UIViewController, lm: LateralMenuView) {
        lateralMenu = lm
        tabBar = tabBarVC
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func commonInit() {
        let viewFromXib = Bundle.main.loadNibNamed("HeaderTabBar", owner: self, options: nil)![0] as! UIView
        viewFromXib.frame = self.bounds
        addSubview(viewFromXib)
    }
    
    @IBAction func buttonMenuAction(_ sender: Any) {
        if lateralMenu.isHidden {
            lateralMenu.isHidden = false
            tabBar.tabBarController?.tabBar.isHidden = true
        }
    }
    @IBAction func backButtonAction(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController(page: nil))
    }
    
    @IBAction func chatButtonAction(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController(page: 1))
    }
    
}
