//
//  OfferingTableView.swift
//  A Votre Service
//
//  Created by Oscar Sevilla on 18/08/21.
//  Copyright © 2021 A Votre Service LLC. All rights reserved.
//

import UIKit
import Popover

class OfferingTableView: UITableView, UITableViewDataSource {
    
    @IBOutlet var table: UITableView!
    
    var delegateVC: OfferingServiceViewController!
    var pop: Popover!
    
    private var data : [String]?
    
    init(frame: CGRect, style: UITableView.Style, data: [String]) {
        self.data = data
        super.init(frame: frame,style: style)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func commonInit() {
        let viewFromXib = Bundle.main.loadNibNamed("OfferingTableView", owner: self, options: nil)![0] as! UIView
        viewFromXib.frame = self.bounds
        addSubview(viewFromXib)
        
        table.register(OfferingTableViewCell.nib(),
                       forCellReuseIdentifier: OfferingTableViewCell.identifier)
        table.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: OfferingTableViewCell.identifier,
                                                 for: indexPath) as! OfferingTableViewCell
        cell.configure(title: (data?[indexPath.row])!, index: indexPath.row)
        cell.delegate = self
        return cell
    }
}


extension OfferingTableView: MyTableViewCellDelegate{
    func didTabButton(title: String, index: Int) {
        print("Se pulsó \(title) y la opción es \(index)")
        delegateVC.dataSelector(option: index)
        pop.dismiss()
    }
}
