//
//  EXIFile.swift
//  A Votre Service
//
//  Created by Oscar Sevilla on 12/08/21.
//  Copyright © 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit

class EXIStaticComponents: NSObject {
    
    static func addHeaderLateralMenu(vc: UIViewController){
//        Se crea el menú lateral
        let lateralMenu = LateralMenuView(frame: CGRect(x: 80, y: 0, width: 347, height: 844))
//        Se crea el HeaderTabBar
        let myView = HeaderTabBarView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 190), tabBarVC: vc, lm: lateralMenu)
//        el menú lateral se oculta
        lateralMenu.isHidden = true
//        le pasamos el header a la barra lateral para uso de acciones
        lateralMenu.header = myView
//        se agregan el header y el menú lateral
        vc.view.addSubview(myView)
        vc.view.addSubview(lateralMenu)
    }
    
    static func addCustomTabBar(vc: UIViewController){
//        Se crea y se agrega la TabBar
        let tabBar = BottonTabBarView(frame: CGRect(x: 0, y: UIScreen.main.bounds.height-163, width: UIScreen.main.bounds.width, height: 83))
        vc.view.addSubview(tabBar)
    }
    
    static func addCustomNavBar(vc: UIViewController, title: String, colorButton: UIColor, navColor: UIColor){
        let navigationBar = UINavigationBar(frame: CGRect(origin: CGPoint(x: 0, y: -60), size: CGSize(width: UIScreen.main.bounds.width, height: 64)))
        navigationBar.backgroundColor = navColor
        navigationBar.delegate = vc as? UINavigationBarDelegate;
        
        let navigationItem = UINavigationItem()
        navigationItem.title = title
        let backButton = UIBarButtonItem(image: UIImage(systemName: "chevron.left"), style: .plain, target: self, action: nil)
        backButton.tintColor = colorButton
        navigationItem.leftBarButtonItem = backButton
        navigationBar.items = [navigationItem]

        // Make the navigation bar a subview of the current view controller
        vc.view.addSubview(navigationBar)
    }
}
