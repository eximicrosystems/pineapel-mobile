//
// Created by Andrei Stoicescu on 10/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyUserDefaults
import PromiseKit
import Fakery

class ApiMockService {
    var faker = Faker()

    lazy var subscriptions: [Subscription] = {
        [10, 20, 30].map {
            Subscription(subscriptionID: UUID().uuidString,
                         name: faker.commerce.productName(),
                         description: faker.lorem.paragraph(sentencesAmount: 3),
                         price: $0,
                         taxPercent: 5,
                         image: nil,
                         recommended: false)
        }
    }()

    lazy var userSubscription: UserSubscription = {
        let sub = subscriptions.randomElement()!
        return UserSubscription(subscriptionID: sub.subscriptionID,
                                name: sub.name,
                                description: sub.description,
                                priceWithTax: sub.price * sub.taxPercent,
                                startTime: 1.weeks.earlier,
                                endTime: 3.weeks.later,
                                status: .active,
                                canceled: false)
    }()

    func getSubscriptionList() -> Promise<[Subscription]> {
        .value(subscriptions)
    }

    func getUserSubscription() -> Promise<UserSubscription> {
        .value(userSubscription)
    }
}
