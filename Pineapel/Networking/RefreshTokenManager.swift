//
// Created by Andrei Stoicescu on 24/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import PromiseKit
import SwiftyUserDefaults

class RefreshTokenManager: ObservableObject {
    enum TokenStatus {
        case shouldRefresh
        case valid(secondsLeft: Int)
    }

    typealias RefreshTokenResult = Swift.Result<RefreshTokenResponse, APIError>
    typealias RefreshTokenResultHandler = (RefreshTokenResult) -> Void

    static var shared = RefreshTokenManager()

    private var isRefreshing: Bool = false

    private var refreshTokenHandlers: [RefreshTokenResultHandler] = []

    @Published var refreshTokenResult: RefreshTokenResult?

    func refresh(_ completionHandler: RefreshTokenResultHandler? = nil) {
        if let handler = completionHandler {
            refreshTokenHandlers.append(handler)
        }

        if isRefreshing { return }
        isRefreshing = true

        Log.this("REFRESHING TOKEN")

        AuthService().refreshToken().done { [weak self] response in
            guard let self = self else { return }
            self.refreshTokenResult = .success(response)
            self.performRefreshTokenHandlerQueue(result: .success(response))
        }.catch { [weak self] error in
            Log.thisError(error)
            guard let self = self else { return }
            let apiError = APIError.from(error)
            self.refreshTokenResult = .failure(apiError)
            self.performRefreshTokenHandlerQueue(result: .failure(apiError))

            if let responseCode = error.asAPIError?.error?.asAFError?.responseCode {
                if responseCode == 401 {
                    Log.this("GOT 401 on REFRESH TOKEN - LOGGING OUT USER")
                    AppState.logout()
                }
                Log.this("RESPONSE CODE: \(responseCode)")
            }
        }
    }

    private func performRefreshTokenHandlerQueue(result: RefreshTokenResult) {
        for handler in refreshTokenHandlers {
            handler(result)
        }
        refreshTokenHandlers.removeAll()
        isRefreshing = false
    }

    var tokenStatus: TokenStatus {
        if isRefreshing { return .shouldRefresh }
        guard let date = AppState.refreshTokenCredentials?.expiresAt else { return .shouldRefresh }
        if 30.seconds.later.isEarlier(than: date) {
            // There's more than 30 seconds until the access token expires
            return .valid(secondsLeft: date.seconds(from: Date()))
        } else {
            return .shouldRefresh
        }
    }

    var shouldRefresh: Bool {
        switch tokenStatus {
        case .shouldRefresh:
            return true
        default:
            return false
        }
    }
}