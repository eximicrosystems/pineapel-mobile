//
// Created by Andrei Stoicescu on 25/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import Alamofire

enum ConciergeRouter: URLRequestConvertible {

    static let baseURL = Configuration.default.aVotreService.apis.main.baseURL

    case GetReservationsCount
    case GetRequestsCount
    case GetPendingRequestsCount
    case GetRunOfShow
    case GetUsers(Page)
    case GetUser(String)
    case GetConcierges(Page)
    case GetConciergeStats
    case UpdateRunOfShow(RunOfShowRequest)
    case GetNotes(Page, id: String? = nil)
    case CreateNote(ConciergeNoteRequest)
    case UpdateNote(ConciergeNoteRequest, noteID: String)
    case DeleteNote(noteID: String)
    case GetAmenityReservations(Page, startDate: Date? = nil, endDate: Date? = nil, amenityID: String? = nil)
    case UpdateAmenityReservationStatus(AmenityReservation.Status, reservationID: String)
    case UploadImage
    case UploadVideo
    case UpdateMedia(ConciergeMediaRequest)
    case GetTags
    case CreateNotification(ConciergeNotificationRequest)
    case GetUserRequests(Page, status: UserRequest.Status)
    case UpdateUserRequest(requestID: String, status: UserRequest.Status)
    case GetOrderList(Page, orderType: OrderData.OrderType?)
    case CompleteOrder(orderID: String)
    case GetBuildingList(Page)
    case GetResidentList(userID: String, Page)
    case LogResident(guestID: String, ResidentGuest)
    case GetGlobalResidentList(Page)
    case LogVisit(VisitorRequest)
    case UploadPackageImage
    case CreatePackageRequest(PackageRequest)
    case NotifyPackage(packageID: String)
    case GetPackageList(Page, status: Package.Status)
    case DeliverPackage(packageID: String, PackageDeliveryRequest)
    case GetAmenityList(Page)
    case CreateAmenityReservation(ConciergeAmenityReservation)
    case GetCurrentShift
    case GetIncidentReports(Page)
    case GetActivityLogs(Page)
    case CreateIncidentReport(IncidentReportRequest)
    case CreateActivityLog(ActivityLogRequest)
    case UpdateIncidentReport(IncidentReportRequest, incidentReportID: String)
    case UpdateActivityLog(ActivityLogRequest, activityLogID: String)
    case DeleteActivityLog(_ activityLogID: String)
    case GetNotifications(Page, buildingID: String)
    case GetSubscriptionList(Page)
    case GetFeedback(Page)
    case GetALaCarteItems(categoryID: String, buildingID: String, page: Page)
    case GetALaCarteCategories(buildingID: String?, conciergeCanEdit: Bool = true)
    case UploadALaCarteImage
    case CreateALaCarteProduct(ConciergeProductRequest)
    case UpdateALaCarteProduct(ConciergeProductRequest, productId: String)
    case GetReports(Page?, reportType: ConciergeReport.ReportType, params: ReportFilterParams)
    case GetContacts(Page)
    case GetServices(userId: String)
    case GetServiceProviders(Page, buildingId: String, serviceId: String)
    case AssignServiceProvider(RequestProviderAssignParams)
    case SearchActivityLogs(Page, term: String)
    case SearchNotes(Page, term: String)
    case ConciergeFAQ(Page, term: String? = nil)
    case CreateFAQ(ConciergeFAQRequest)
    case UpdateFAQ(ConciergeFAQRequest, id: String)
    case DeleteFAQ(_ id: String)

    var method: Alamofire.HTTPMethod {
        switch self {
        case .GetReservationsCount,
             .GetRunOfShow,
             .GetRequestsCount,
             .GetPendingRequestsCount,
             .GetUsers,
             .GetUser,
             .GetConcierges,
             .GetConciergeStats,
             .GetNotes,
             .GetAmenityReservations,
             .GetTags,
             .GetUserRequests,
             .GetOrderList,
             .GetBuildingList,
             .GetResidentList,
             .GetGlobalResidentList,
             .GetPackageList,
             .GetAmenityList,
             .GetCurrentShift,
             .GetIncidentReports,
             .GetActivityLogs,
             .GetSubscriptionList,
             .GetNotifications,
             .GetFeedback,
             .GetALaCarteItems,
             .GetALaCarteCategories,
             .GetReports,
             .GetContacts,
             .GetServices,
             .GetServiceProviders,
             .SearchActivityLogs,
             .SearchNotes,
             .ConciergeFAQ:
            return .get
        case .UpdateRunOfShow,
             .CreateNote,
             .UpdateAmenityReservationStatus,
             .UploadImage,
             .UploadVideo,
             .CreateNotification,
             .UpdateMedia,
             .UpdateUserRequest,
             .LogResident,
             .LogVisit,
             .UploadPackageImage,
             .UploadALaCarteImage,
             .CreatePackageRequest,
             .NotifyPackage,
             .CreateAmenityReservation,
             .CompleteOrder,
             .CreateActivityLog,
             .CreateIncidentReport,
             .CreateALaCarteProduct,
             .AssignServiceProvider,
             .CreateFAQ:
            return .post
        case .UpdateNote,
             .DeliverPackage,
             .UpdateActivityLog,
             .UpdateIncidentReport,
             .UpdateALaCarteProduct,
             .UpdateFAQ:
            return .patch
        case .DeleteNote,
             .DeleteActivityLog,
             .DeleteFAQ:
            return .delete
        }
    }

    var path: String {
        switch self {
        case .GetReservationsCount:
            return "/api/stats/reservations"
        case .GetRequestsCount:
            return "/api/stats/requests"
        case .GetPendingRequestsCount:
            return "/api/request/new"
        case .GetRunOfShow:
            return "/api/ros"
        case .GetUsers:
            return "/api/user/list"
        case .GetUser(let userID):
            return "/api/user/\(userID)"
        case .GetConcierges:
            return "/mobile/concierges/list"
        case .GetConciergeStats:
            return "/api/building/concierge-stats"
        case .UpdateRunOfShow:
            return "/api/ros/update-status"
        case .GetNotes(_, let id):
            if let id = id {
                return "/api/notes/\(id)"
            } else {
                return "/api/notes"
            }
        case .CreateNote:
            return "/entity/avs_note"
        case .UpdateNote(_, let noteID):
            return "/admin/structure/avs/avs_note/\(noteID)"
        case .DeleteNote(let noteID):
            return "/admin/structure/avs/avs_note/\(noteID)"
        case .GetAmenityReservations:
            return "/api/reservations"
        case .UpdateAmenityReservationStatus(_, let reservationID):
            return "/api/amenity-reservation/\(reservationID)/update"
        case .UploadImage:
            return "/file/upload/media/image/field_media_image"
        case .UploadVideo:
            return "/file/upload/media/video/field_media_video_file"
        case .UpdateMedia:
            return "/entity/media"
        case .GetTags:
            return "/mobile/tags"
        case .CreateNotification:
            return "/api/notifications/send"
        case .GetUserRequests:
            return "/api/requests"
        case .UpdateUserRequest(let requestID, _):
            return "/api/request/\(requestID)/update"
        case .GetOrderList:
            return "/api/commerce/orders"
        case .GetBuildingList:
            return "/mobile/buildings"
        case .GetResidentList(let userID, _):
            return "/api/user/\(userID)/guests"
        case .LogResident(let guestID, _):
            return "/api/guests/\(guestID)/log-access"
        case .GetGlobalResidentList:
            return "/api/visitors"
        case .LogVisit:
            return "/api/guests/log-access"
        case .UploadPackageImage:
            return "/file/upload/avs_package/avs_package/field_image"
        case .UploadALaCarteImage:
            return "/file/upload/avs_alacarte/avs_alacarte/field_image"
        case .CreatePackageRequest:
            return "/entity/avs_package"
        case .NotifyPackage(let packageID):
            return "/api/package/\(packageID)/notify"
        case .GetPackageList:
            return "/api/packages"
        case .DeliverPackage(let packageID, _):
            return "/admin/structure/avs/avs_package/\(packageID)"
        case .GetAmenityList:
            return "/mobile/amenities/list"
        case .CreateAmenityReservation:
            return "/entity/avs_amenity_reservation"
        case .GetCurrentShift:
            return "/api/ros/shift"
        case .CompleteOrder(let orderID):
            return "/api/commerce/order/\(orderID)/update"
        case .GetIncidentReports:
            return "/api/incident-reports"
        case .CreateIncidentReport:
            return "/entity/incident_report"
        case .CreateActivityLog:
            return "/entity/activity_log"
        case .UpdateIncidentReport(_, let id):
            return "/admin/structure/avs/incident_report/\(id)"
        case .UpdateActivityLog(_, let id):
            return "/admin/structure/avs/activity_log/\(id)"
        case .GetNotifications(_, let buildingID):
            return "/api/notifications/building/\(buildingID)"
        case .GetActivityLogs:
            return "/api/activity-logs"
        case .DeleteActivityLog(let activityLogID):
            return "/admin/structure/avs/activity_log/\(activityLogID)"
        case .GetSubscriptionList:
            return "/api/commerce/subscriptions"
        case .GetFeedback:
            return "/api/concierge/feedback"
        case .GetALaCarteItems:
            return "/api/alacarte/items"
        case .GetALaCarteCategories:
            return "/mobile/tags"
        case .CreateALaCarteProduct:
            return "/entity/avs_alacarte"
        case .UpdateALaCarteProduct(_, let productId):
            return "/admin/structure/avs/avs_alacarte/\(productId)"
        case .GetReports:
            return "/api/reports/preview"
        case .GetContacts:
            return "/api/users"
        case .GetServices(let userId):
            return "/api/alacarte/user/\(userId)/services"
        case .GetServiceProviders(_, let buildingId, _):
            return "/api/building/\(buildingId)/service-providers"
        case .AssignServiceProvider(let params):
            return "/api/request/\(params.requestId)/add-provider"
        case .SearchActivityLogs:
            return "/api/activity-logs"
        case .SearchNotes:
            return "/api/notes"
        case .ConciergeFAQ:
            return "/api/concierge-faq"
        case .UpdateFAQ(_, let id):
            return "/admin/structure/avs/avs_concierge_faq/\(id)"
        case .CreateFAQ:
            return "/entity/avs_concierge_faq"
        case .DeleteFAQ(let id):
            return "/admin/structure/avs/avs_concierge_faq/\(id)"
        }
    }

    func asURLRequest() throws -> URLRequest {
        var urlRequest = URLRequest(url: type(of: self).baseURL.appendingPathComponent(path))
        urlRequest.method = method

        urlRequest = try URLEncoding.init(destination: .queryString).encode(urlRequest, with: ["_format": "json"])
        urlRequest = try URLEncoding.init(destination: .queryString).encode(urlRequest, with: ["ts": Date().timeIntervalSince1970])

        switch self {
        case .GetReservationsCount,
             .GetRequestsCount,
             .GetPendingRequestsCount,
             .GetRunOfShow,
             .GetConciergeStats,
             .DeleteNote,
             .DeleteActivityLog,
             .DeleteFAQ,
             .GetCurrentShift,
             .GetUser,
             .GetServices:
            ()
        case .GetUsers(let page),
             .GetConcierges(let page),
             .GetBuildingList(let page),
             .GetAmenityList(let page),
             .GetGlobalResidentList(let page),
             .GetActivityLogs(let page),
             .GetSubscriptionList(let page),
             .GetIncidentReports(let page):
            urlRequest = try urlRequest.addQueryParams(page.params)
        case .GetNotes(let page, _):
            urlRequest = try urlRequest.addQueryParams(page.params)
        case .GetResidentList(_, let page):
            urlRequest = try urlRequest.addQueryParams(page.params)
        case .GetAmenityReservations(let page, let startDate, let endDate, let amenityID):
            urlRequest = try urlRequest.addQueryParams(page.params)
            if let startDate = startDate { urlRequest = try urlRequest.addQueryParams(["date": ["min": AVSISO8601DateFormatter.formattedString(date: startDate, timeZone: AppState.buildingTimeZone)]]) }
            if let endDate = endDate { urlRequest = try urlRequest.addQueryParams(["date": ["max": AVSISO8601DateFormatter.formattedString(date: endDate, timeZone: AppState.buildingTimeZone)]]) }
            if let amenityID = amenityID { urlRequest = try urlRequest.addQueryParams(["amenity": amenityID]) }
        case .UpdateRunOfShow(let params):
            urlRequest = try JSONParameterEncoder.default.encode(params, into: urlRequest)
        case .CreateNote(let params):
            urlRequest = try JSONParameterEncoder.default.encode(params, into: urlRequest)
        case .CreateNotification(let params):
            urlRequest = try JSONParameterEncoder.default.encode(params, into: urlRequest)
        case .UpdateNote(let params, _):
            urlRequest = try JSONParameterEncoder.default.encode(params, into: urlRequest)
        case .UpdateAmenityReservationStatus(let status, _):
            urlRequest = try JSONParameterEncoder.default.encode(["field_status": status.rawValue], into: urlRequest)
        case .UploadImage:
            urlRequest.headers.add(.contentType(ContentType.octetStreamFormData))
            urlRequest.headers.add(.contentDisposition("file; filename=\"notification_image.jpg\""))
        case .UploadVideo:
            urlRequest.headers.add(.contentType(ContentType.octetStreamFormData))
            urlRequest.headers.add(.contentDisposition("file; filename=\"notification_image.mp4\""))
        case .UpdateMedia(let params):
            urlRequest = try JSONParameterEncoder.default.encode(params, into: urlRequest)
        case .GetTags:
            urlRequest = try urlRequest.addQueryParams(["vid": ["user_tags"]])
        case .GetUserRequests(let page, let status):
            urlRequest = try urlRequest.addQueryParams(page.params)
            urlRequest = try urlRequest.addQueryParams(["field_status": status.rawValue])
        case .UpdateUserRequest(_, let status):
            urlRequest = try JSONParameterEncoder.default.encode(["field_status": status.rawValue], into: urlRequest)
        case .LogResident(_, let params):
            urlRequest = try JSONParameterEncoder.default.encode(params, into: urlRequest)
        case .LogVisit(let params):
            urlRequest = try JSONParameterEncoder.default.encode(params, into: urlRequest)
        case .UploadPackageImage:
            urlRequest.headers.add(.contentType(ContentType.octetStreamFormData))
            urlRequest.headers.add(.contentDisposition("file; filename=\"package_image.jpg\""))
        case .UploadALaCarteImage:
            urlRequest.headers.add(.contentType(ContentType.octetStreamFormData))
            urlRequest.headers.add(.contentDisposition("file; filename=\"alacarte_image.jpg\""))
        case .CreatePackageRequest(let params):
            urlRequest = try JSONParameterEncoder.default.encode(params, into: urlRequest)
        case .NotifyPackage:
            ()
        case .GetPackageList(let page, let status):
            urlRequest = try urlRequest.addQueryParams(page.params)
            urlRequest = try urlRequest.addQueryParams(["status": status.rawValue.lowercased()])
        case .DeliverPackage(_, let params):
            urlRequest = try JSONParameterEncoder.default.encode(params, into: urlRequest)
        case .CreateAmenityReservation(let params):
            urlRequest = try JSONParameterEncoder.default.encode(params, into: urlRequest)
        case .CreateIncidentReport(let params):
            urlRequest = try JSONParameterEncoder.default.encode(params, into: urlRequest)
        case .CreateActivityLog(let params):
            urlRequest = try JSONParameterEncoder.default.encode(params, into: urlRequest)
        case .GetOrderList(let page, let orderType):
            urlRequest = try urlRequest.addQueryParams(page.params)
            if let orderType = orderType {
                urlRequest = try urlRequest.addQueryParams(["completion_status": orderType.rawValue])
            }
        case .CompleteOrder(_):
            urlRequest = try JSONParameterEncoder.default.encode(["completion_status": OrderData.OrderType.done.rawValue], into: urlRequest)
        case .UpdateIncidentReport(let params, _):
            urlRequest = try JSONParameterEncoder.default.encode(params, into: urlRequest)
        case .UpdateActivityLog(let params, _):
            urlRequest = try JSONParameterEncoder.default.encode(params, into: urlRequest)
        case .GetNotifications(let page, _):
            urlRequest = try urlRequest.addQueryParams(page.params)
        case .GetFeedback(let page):
            urlRequest = try urlRequest.addQueryParams(page.params)
        case .GetALaCarteItems(let categoryId, let buildingId, let page):
            urlRequest = try urlRequest.addQueryParams(page.params)
            urlRequest = try urlRequest.addQueryParams(["category": categoryId])
            urlRequest = try urlRequest.addQueryParams(["building": buildingId])
        case .GetALaCarteCategories(let buildingId, let conciergesCanEdit):
            urlRequest = try urlRequest.addQueryParams(["vid": ["a_la_carte_categories"]])
            if let buildingId = buildingId {
                urlRequest = try urlRequest.addQueryParams(["building": buildingId])
            }
            if conciergesCanEdit {
                urlRequest = try urlRequest.addQueryParams(["cce": "1"])
            }
        case .CreateALaCarteProduct(let params):
            urlRequest = try JSONParameterEncoder.default.encode(params, into: urlRequest)
        case .UpdateALaCarteProduct(let params, _):
            urlRequest = try JSONParameterEncoder.default.encode(params, into: urlRequest)
        case .GetReports(let page, let reportType, let params):
            if let page = page {
                urlRequest = try urlRequest.addQueryParams(page.params)
            }
            urlRequest = try urlRequest.addQueryParams(["type": reportType.rawValue])
            urlRequest = try urlRequest.addQueryParams(params.toParams(reportType))
        case .GetContacts(let page):
            urlRequest = try urlRequest.addQueryParams(["role": "service_provider"])
            urlRequest = try urlRequest.addQueryParams(page.params)
        case .GetServiceProviders(let page, _, let serviceId):
            urlRequest = try urlRequest.addQueryParams(page.params)
            urlRequest = try urlRequest.addQueryParams(["service_id": serviceId])
        case .AssignServiceProvider(let params):
            urlRequest = try JSONParameterEncoder.default.encode(params, into: urlRequest)
        case .SearchActivityLogs(let page, let term),
             .SearchNotes(let page, let term):
            urlRequest = try urlRequest.addQueryParams(page.params)
            urlRequest = try urlRequest.addQueryParams(["search": term])
        case .ConciergeFAQ(let page, let term):
            urlRequest = try urlRequest.addQueryParams(page.params)
            if let term = term {
                urlRequest = try urlRequest.addQueryParams(["search": term])
            }
        case .UpdateFAQ(let params, _):
            urlRequest = try JSONParameterEncoder.default.encode(params, into: urlRequest)
        case .CreateFAQ(let params):
            urlRequest = try JSONParameterEncoder.default.encode(params, into: urlRequest)
        }

        return urlRequest
    }

}
