//
// Created by Andrei Stoicescu on 03/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import Alamofire

extension URLRequest {
    func addQueryParams(_ params: [String: Any]) throws -> URLRequest {
        try URLEncoding.init(destination: .queryString).encode(self, with: params)
    }
}

enum ResidentRouter: URLRequestConvertible {

    static let baseURL = Configuration.default.aVotreService.apis.main.baseURL

    case GetUser
    case UploadUserImage
    case UpdateUserProfile(userID: String, params: UserProfile)
    case GetFeedNotificationList(Page)
    case GetFeedNotification(notificationID: String)
    case GetBuildingDetails
    case GetBuildingInfoArticles(buildingID: String)
    case GetBuildingFAQ(buildingID: String)
    case GetBuildingThingsToKnow(buildingID: String)
    case GetConciergeList(Page)
    case GetAmenityList(requiresReservation: Bool, page: Page)
    case GetAmenity(amenityID: String)
    case GetResidentGuests(Page)
    case CreateResidentGuest(params: ResidentGuestRequest)
    case DeleteResidentGuest(guestID: String)
    case UpdateResidentGuest(guestID: String, params: ResidentGuestRequest)
    case GetFeedbackList
    case CreateFeedback(params: Feedback)
    case DeleteFeedback(feedbackID: String)
    case GetTerms
    case GetPrivacy
    case GetSettings
    case UpdateSettings(params: Settings)
    case GetAmenityAvailability(amenityID: String, date: Date)
    case GetAmenityDaysAvailability(amenityID: String, startDate: Date, endDate: Date)
    case CreateAmenityReservation(params: AmenityReservationRequest)
    case DeleteAmenityReservation(reservationID: String)
    case GetAmenityReservationList(Page, startDate: Date? = nil, endDate: Date? = nil)
    case GetAmenityReservation(reservationID: String)
    case UpdateAmenityReservation(reservationID: String, params: AmenityReservationRequest)
    case GetUserRequestList(statuses: [UserRequest.Status]? = nil, page: Page)
    case GetUserRequest(requestID: String)
    case CreateUserRequest(params: UserRequestParams)
    case DeleteUserRequest(requestID: String)
    case DeleteNotification(notificationID: String)
    case GetConciergeOnDutyList
    case GetTwilioAccessToken
    case CreateTwilioChannel(userID: String? = nil)
    case JoinTwilioChannel
    case DeleteTwilioChannel

    case GetStripeEphemeralKey(stripeApiVersion: String = Configuration.default.aVotreService.stripe.apiVersion)
    case GetStripeCustomerDetails
    case CreateOrder(params: OrderRequestData)
    case CalculateOrder(params: OrderRequestData)
    case CreateSetupIntent
    case GetUserOrders(Page)
    case GetUserOrder(orderID: String)

    case GetALaCarteItems(categoryID: String, page: Page)
    case GetALaCarteCategories

    case GetBellNotifications(Page)
    case ReadBellNotification(notificationID: String)
    case ReadAllNotifications
    case ReadBellNotificationObject(BellNotificationReadRequest)
    case GetUnreadBellNotificationCount

    case GetLeasingOffice
    case GetLeasingOfficeTeam(officeID: String)

    case GetSubscriptionList
    case GetSubscription
    case UpdateSubscription(SubscriptionManageRequest)
    case NextInvoice(subscriptionID: String)

    case GetLibraryItemList(Page, type: LibraryItem.ItemType, categoryID: String? = nil)
    case GetLibraryCategories
    case GetRefundInfo(reservationID: String)

    var method: Alamofire.HTTPMethod {
        switch self {
        case .GetUser,
             .GetFeedNotificationList,
             .GetFeedNotification,
             .GetBuildingDetails,
             .GetConciergeList,
             .GetAmenityList,
             .GetAmenity,
             .GetBuildingInfoArticles,
             .GetBuildingFAQ,
             .GetBuildingThingsToKnow,
             .GetResidentGuests,
             .GetFeedbackList,
             .GetTerms,
             .GetPrivacy,
             .GetSettings,
             .GetConciergeOnDutyList,
             .GetUserRequestList,
             .GetUserRequest,
             .GetAmenityReservationList,
             .GetAmenityReservation,
             .GetALaCarteItems,
             .GetALaCarteCategories,
             .GetUserOrders,
             .GetUserOrder,
             .GetBellNotifications,
             .GetUnreadBellNotificationCount,
             .GetLeasingOffice,
             .GetLeasingOfficeTeam,
             .GetSubscriptionList,
             .GetLibraryItemList,
             .GetLibraryCategories,
             .GetRefundInfo:
            return .get
        case .UploadUserImage,
             .CreateResidentGuest,
             .CreateFeedback,
             .UpdateSettings,
             .GetAmenityAvailability,
             .GetAmenityDaysAvailability,
             .CreateAmenityReservation,
             .CreateUserRequest,
             .GetTwilioAccessToken,
             .CreateTwilioChannel,
             .JoinTwilioChannel,
             .DeleteTwilioChannel,
             .GetStripeEphemeralKey,
             .GetStripeCustomerDetails,
             .CreateOrder,
             .CalculateOrder,
             .CreateSetupIntent,
             .ReadBellNotification,
             .ReadBellNotificationObject,
             .ReadAllNotifications,
             .GetSubscription,
             .UpdateSubscription,
             .NextInvoice:
            return .post
        case .UpdateUserProfile,
             .UpdateAmenityReservation,
             .UpdateResidentGuest:
            return .patch
        case .DeleteResidentGuest,
             .DeleteFeedback,
             .DeleteAmenityReservation,
             .DeleteUserRequest,
             .DeleteNotification:
            return .delete
        }
    }

    var path: String {
        switch self {
        case .GetUser:
            return "/mobile/user"
        case .UploadUserImage:
            return "/file/upload/user/user/user_picture/custom"
        case .UpdateUserProfile(let userID, _):
            return "/user/\(userID)"
        case .GetFeedNotificationList:
            return "/mobile/notifications/new"
        case .GetFeedNotification(let notificationID):
            return "/mobile/notification/\(notificationID)/new"
        case .GetBuildingDetails:
            return "/mobile/building"
        case .GetConciergeList:
            return "/mobile/concierges/list"
        case .GetAmenityList:
            return "/mobile/amenities/list"
        case .GetAmenity(let amenityID):
            return "/mobile/amenities/list/\(amenityID)"
        case .GetBuildingInfoArticles(let buildingID):
            return "/mobile/building/\(buildingID)/info"
        case .GetBuildingFAQ(let buildingID):
            return "/mobile/building/\(buildingID)/faq"
        case .GetBuildingThingsToKnow(let buildingID):
            return "/mobile/building/\(buildingID)/things-to-know"
        case .GetResidentGuests:
            return "/mobile/user/guests"
        case .CreateResidentGuest:
            return "/entity/avs_guest"
        case .DeleteResidentGuest(let guestID):
            return "/admin/structure/avs/avs_guest/\(guestID)"
        case .UpdateResidentGuest(let guestID, _):
            return "/admin/structure/avs/avs_guest/\(guestID)"
        case .GetFeedbackList:
            return "/mobile/user/feedback"
        case .CreateFeedback:
            return "/entity/avs_feedback"
        case .DeleteFeedback(let feedbackID):
            return "/admin/structure/avs/avs_feedback/\(feedbackID)"
        case .GetTerms:
            return "/mobile/pages/terms"
        case .GetPrivacy:
            return "/mobile/pages/privacy"
        case .GetSettings:
            return "/mobile/settings/get"
        case .UpdateSettings:
            return "/mobile/settings/set"
        case .GetAmenityAvailability(let amenityID, _):
            return "/mobile/amenity-reservation/\(amenityID)/get-availability"
        case .GetAmenityDaysAvailability(let amenityID, _, _):
            return "/mobile/amenity-reservation/\(amenityID)/get-availability"
        case .CreateAmenityReservation:
            return "/entity/avs_amenity_reservation"
        case .GetAmenityReservationList:
            return "/mobile/reservations"
        case .GetAmenityReservation(let reservationID):
            return "mobile/reservation/\(reservationID)"
        case .UpdateAmenityReservation(let reservationID, _):
            return "/admin/structure/avs/avs_amenity_reservation/\(reservationID)"
        case .DeleteAmenityReservation(let reservationID):
            return "/admin/structure/avs/avs_amenity_reservation/\(reservationID)"
        case .CreateUserRequest:
            return "/entity/avs_request"
        case .DeleteUserRequest(let requestID):
            return "/admin/structure/avs/avs_request/\(requestID)"
        case .DeleteNotification(let notificationID):
            return "/admin/structure/avs/avs_notification/\(notificationID)"
        case .GetConciergeOnDutyList:
            return "/mobile/concierges/onduty"
        case .GetUserRequestList:
            return "/mobile/user/requests"
        case .GetUserRequest(let requestID):
            return "/mobile/request/\(requestID)"
        case .GetTwilioAccessToken:
            return "/mobile/twilio/access-token"
        case .CreateTwilioChannel:
            return "/mobile/twilio/create-channel"
        case .JoinTwilioChannel:
            return "/mobile/twilio/join-channel"
        case .DeleteTwilioChannel:
            return "/mobile/twilio/delete-channel"
        case .GetStripeEphemeralKey:
            return "/mobile/commerce/key"
        case .GetStripeCustomerDetails:
            return "/mobile/commerce/customer"
        case .GetALaCarteItems:
            return "/mobile/alacarte/items"
        case .GetALaCarteCategories:
            return "/mobile/alacarte/categories"
        case .CreateOrder:
            return "/mobile/commerce/order"
        case .CalculateOrder:
            return "/mobile/commerce/order/subtotal"
        case .CreateSetupIntent:
            return "/mobile/commerce/setupintent"
        case .GetUserOrders:
            return "/mobile/commerce/orders"
        case .GetUserOrder(let orderID):
            return "/mobile/commerce/order/\(orderID)"
        case .GetBellNotifications:
            return "/mobile/notifications/list"
        case .ReadBellNotification, .ReadBellNotificationObject, .ReadAllNotifications:
            return "/mobile/notifications/mark-read"
        case .GetUnreadBellNotificationCount:
            return "/mobile/notifications/unread-count"
        case .GetLeasingOffice:
            return "/mobile/leasing-office"
        case .GetLeasingOfficeTeam(let officeID):
            return "/mobile/leasing-office/\(officeID)/management"
        case .GetSubscriptionList:
            return "/mobile/commerce/subscription/packages"
        case .GetSubscription:
            return "/mobile/commerce/subscription"
        case .UpdateSubscription:
            return "/mobile/commerce/subscription/manage"
        case .NextInvoice:
            return "/mobile/commerce/next-invoice"
        case .GetLibraryItemList:
            return "/mobile/library-items"
        case .GetLibraryCategories:
            return "/mobile/tags"
        case .GetRefundInfo(let reservationID):
            return "/mobile/commerce/calculate-refund/\(reservationID)"
        }
    }

    func asURLRequest() throws -> URLRequest {
        var urlRequest = URLRequest(url: type(of: self).baseURL.appendingPathComponent(path))
        urlRequest.method = method

        urlRequest = try URLEncoding.init(destination: .queryString).encode(urlRequest, with: ["_format": "json"])

        switch self {
        case .GetAmenityAvailability(_, let date):
            urlRequest = try JSONParameterEncoder.default.encode(["date": date.avsDateFormatted], into: urlRequest)
        case .GetAmenityDaysAvailability(_, let startDate, let endDate):
            urlRequest = try JSONParameterEncoder.default.encode(["start_date": startDate.avsDateFormatted, "end_date": endDate.avsDateFormatted], into: urlRequest)
        case .CreateAmenityReservation(let parameters):
            urlRequest = try JSONParameterEncoder.default.encode(parameters, into: urlRequest)
        case .CreateUserRequest(let parameters):
            urlRequest = try JSONParameterEncoder.default.encode(parameters, into: urlRequest)
        case .UpdateSettings(let parameters):
            urlRequest = try JSONParameterEncoder.default.encode(parameters, into: urlRequest)
        case .UpdateAmenityReservation(_, let parameters):
            urlRequest = try JSONParameterEncoder.default.encode(parameters, into: urlRequest)
        case .UpdateUserProfile(_, let parameters):
            urlRequest = try JSONParameterEncoder.default.encode(parameters, into: urlRequest)
        case .UpdateResidentGuest(_, let parameters):
            urlRequest = try JSONParameterEncoder.default.encode(parameters, into: urlRequest)
        case .UploadUserImage:
            urlRequest.headers.add(.contentType(ContentType.octetStreamFormData))
            urlRequest.headers.add(.contentDisposition("file; filename=\"user_avatar.jpg\""))
        case .GetUser,
             .GetBuildingDetails,
             .GetBuildingInfoArticles,
             .GetBuildingFAQ,
             .GetBuildingThingsToKnow,
             .DeleteResidentGuest,
             .GetFeedbackList,
             .DeleteFeedback,
             .GetTerms,
             .GetPrivacy,
             .GetSettings,
             .DeleteUserRequest,
             .DeleteNotification,
             .GetConciergeOnDutyList,
             .GetTwilioAccessToken,
             .JoinTwilioChannel,
             .DeleteAmenityReservation,
             .DeleteTwilioChannel,
             .GetAmenity,
             .GetAmenityReservation,
             .GetStripeCustomerDetails,
             .GetALaCarteCategories,
             .CreateSetupIntent,
             .GetUserOrder,
             .GetUnreadBellNotificationCount,
             .GetLeasingOffice,
             .GetLeasingOfficeTeam,
             .GetUserRequest,
             .GetFeedNotification,
             .GetSubscriptionList,
             .GetRefundInfo,
             .GetSubscription:
            ()
        case .GetUserRequestList(let statuses, let page):
            if let statuses = statuses { urlRequest = try urlRequest.addQueryParams(["status": statuses.map(\.rawValue)]) }
            urlRequest = try urlRequest.addQueryParams(page.params)
        case .GetConciergeList(let page):
            urlRequest = try urlRequest.addQueryParams(page.params)
        case .GetResidentGuests(let page):
            urlRequest = try urlRequest.addQueryParams(page.params)
        case .CreateResidentGuest(let params):
            urlRequest = try JSONParameterEncoder.default.encode(params, into: urlRequest)
        case .CreateFeedback(let params):
            urlRequest = try JSONParameterEncoder.default.encode(params, into: urlRequest)
        case .GetBellNotifications(let page):
            urlRequest = try urlRequest.addQueryParams(page.params)
        case .ReadBellNotification(let notificationID):
            urlRequest = try JSONParameterEncoder.default.encode(["id": notificationID], into: urlRequest)
        case .ReadBellNotificationObject(let params):
            urlRequest = try JSONParameterEncoder.default.encode(params, into: urlRequest)
        case .ReadAllNotifications:
            urlRequest = try JSONParameterEncoder.default.encode([String: String](), into: urlRequest)
        case .GetAmenityList(let requiresReservation, let page):
            var params = page.params
            if requiresReservation { params["requires_reservation"] = "1" }
            urlRequest = try urlRequest.addQueryParams(params)
        case .GetFeedNotificationList(let page):
            urlRequest = try urlRequest.addQueryParams(page.params)
        case .GetAmenityReservationList(let page, let startDate, let endDate):
            urlRequest = try urlRequest.addQueryParams(page.params)
            if let startDate = startDate { urlRequest = try urlRequest.addQueryParams(["date": ["min": AVSISO8601DateFormatter.formattedString(date: startDate, timeZone: AppState.buildingTimeZone)]]) }
            if let endDate = endDate { urlRequest = try urlRequest.addQueryParams(["date": ["max": AVSISO8601DateFormatter.formattedString(date: endDate, timeZone: AppState.buildingTimeZone)]]) }
        case .GetALaCarteItems(let categoryID, let page):
            var params = page.params
            params["category"] = categoryID
            urlRequest = try urlRequest.addQueryParams(params)
        case .GetUserOrders(let page):
            urlRequest = try urlRequest.addQueryParams(page.params)
        case .GetStripeEphemeralKey(let apiVersion):
            urlRequest = try JSONParameterEncoder.default.encode(["api_version": apiVersion], into: urlRequest)
        case .CreateOrder(let parameters), .CalculateOrder(let parameters):
            urlRequest = try JSONParameterEncoder.default.encode(parameters, into: urlRequest)
        case .UpdateSubscription(let parameters):
            urlRequest = try JSONParameterEncoder.default.encode(parameters, into: urlRequest)
        case .NextInvoice(let subscriptionID):
            urlRequest = try JSONParameterEncoder.default.encode(["product": subscriptionID], into: urlRequest)
        case .GetLibraryItemList(let page, let type, let categoryID):
            urlRequest = try urlRequest.addQueryParams(page.params)
            urlRequest = try urlRequest.addQueryParams(["type": type.rawValue])
            if let category = categoryID {
                urlRequest = try urlRequest.addQueryParams(["category": category])
            }
        case .GetLibraryCategories:
            urlRequest = try urlRequest.addQueryParams(["vid": ["library_tags"]])
        case .CreateTwilioChannel(let userID):
            if let userID = userID {
                urlRequest = try JSONParameterEncoder.default.encode(["channel": userID], into: urlRequest)
            }
        }

        return urlRequest
    }

}
