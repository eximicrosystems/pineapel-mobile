//
// Created by Andrei Stoicescu on 09/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import DifferenceKit

struct FeedNotificationMediaFile: Hashable {
    enum MediaType: String, Codable, Hashable {
        case image
        case video
    }

    let mediaURL: URL
    let type: MediaType
    let ratio: Float?
    let targetID: String

    static func generateMediaWithURL(_ url: URL, targetID: String) -> FeedNotificationMediaFile {
        FeedNotificationMediaFile(mediaURL: url, type: .image, ratio: nil, targetID: targetID)
    }
}

extension FeedNotificationMediaFile: Codable {
    enum CodingKeys: String, CodingKey {
        case mediaURL = "media_url"
        case type
        case ratio
        case targetID = "target_id"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        mediaURL = try map.decode(.mediaURL)
        type = try map.decode(.type)
        ratio = try? map.decodeLosslessIfPresent(.ratio)
        targetID = try map.decode(.targetID)
    }
}

struct FeedNotificationLink: Codable, Hashable {
    enum LinkType: String, Codable, Hashable {
        case article = "article"
        case url = "url"
    }

    let type: LinkType
    let url: URL?
    let articleID: String?
    let articleTitle: String?

    enum CodingKeys: String, CodingKey {
        case type
        case url
        case articleID = "id"
        case articleTitle = "title"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        url = try? map.decodeIfPresent(.url)
        type = try map.decode(.type)
        articleID = try? map.decodeIfPresent(.articleID)
        articleTitle = try? map.decodeIfPresent(.articleTitle)
    }
}

extension FeedNotificationItem: Hashable, Differentiable {
    // static func ==(lhs: FeedNotificationItem, rhs: FeedNotificationItem) -> Bool {
    //     lhs.notificationID == rhs.notificationID && lhs.read == rhs.read
    // }
    //
    // func hash(into hasher: inout Hasher) { hasher.combine(notificationID) }
    var differenceIdentifier: Int { hashValue }
}

struct FeedNotificationItem: Codable, Identifiable {
    enum NotificationType: String, Codable, CaseIterable, Hashable {
        case info
        case packages
        case mailDelivery = "mail_delivery"
        case fitness
        case delivery
        case event

        var string: String {
            switch self {
            case .info:
                return "Information"
            case .packages:
                return "Packages"
            case .mailDelivery:
                return "Mail Delivery"
            case .fitness:
                return "Fitness"
            case .delivery:
                return "Delivery"
            case .event:
                return "Event"
            }
        }
    }

    enum LinkTypeResult {
        case url(URL)
        case article(id: String, title: String)
    }

    var id: String { notificationID }

    let notificationID: String
    let notificationType: NotificationType
    let createdAt: Date?
    let author: User?
    let name: String?
    let bodyText: String?
    let mediaFile: [FeedNotificationMediaFile]

    let deliveryTime: Date?
    let itemForDelivery: String?
    let duration: String?
    let instructor: String?
    let startDate: Date?
    let type: String? // Fitness type :/
    let number: String?
    let signedBy: User?

    var requestID: String?
    var read: Bool

    let externalLink: FeedNotificationLink?
    let internalLink: FeedNotificationLink?

    let eventName: String?
    let eventHost: String?
    let sent: Bool

    func linkType() -> LinkTypeResult? {
        func result(link: FeedNotificationLink?) -> LinkTypeResult? {
            guard let link = link else { return nil }
            if let id = link.articleID, let title = link.articleTitle { return .article(id: id, title: title) }
            if let url = link.url { return .url(url) }
            return nil
        }

        return [internalLink, externalLink].compactMap { result(link: $0) }.first
    }

    enum CodingKeys: String, CodingKey {
        case notificationID = "id"
        case notificationType = "type"
        case createdAt = "created"
        case author
        case name
        case bodyText = "field_body"
        case mediaFile = "field_media_file"

        case deliveryTime = "field_delivery_time"
        case itemForDelivery = "field_item_for_delivery"
        case duration = "field_duration"
        case instructor = "field_instructor"
        case startDate = "field_start_date"
        case type = "field_type"
        case number = "field_number"
        case signedBy = "field_signed_by"

        case requestID = "request"
        case read = "field_read"

        case externalLink = "field_external_link"
        case internalLink = "field_internal_link"

        case eventName = "field_event_name"
        case eventHost = "field_host"
        case sent = "status"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        notificationID = try map.decode(.notificationID)
        notificationType = try map.decode(.notificationType)
        createdAt = map.decodeDateIfPresent(.createdAt)
        author = try? map.decodeIfPresent(.author)
        name = try? map.decodeIfPresent(.name)
        bodyText = try? map.decodeIfPresent(.bodyText)
        mediaFile = (try? map.decodeIfPresent([Safe<FeedNotificationMediaFile>].self, forKey: .mediaFile)?.compactMap { $0.value }) ?? []

        deliveryTime = try? map.decodeIfPresent(.deliveryTime)
        itemForDelivery = try? map.decodeIfPresent(.itemForDelivery)
        duration = try? map.decodeIfPresent(.duration)
        instructor = try? map.decodeIfPresent(.instructor)
        startDate = map.decodeDateIfPresent(.startDate)
        type = try? map.decodeIfPresent(.type)
        number = try? map.decodeIfPresent(.number)
        signedBy = try? map.decodeIfPresent(.signedBy)

        requestID = try? map.decodeIfPresent(.requestID)
        read = (try? map.decodeLosslessIfPresent(.read)) ?? true

        externalLink = try? map.decodeIfPresent(.externalLink)
        internalLink = try? map.decodeIfPresent(.internalLink)

        eventName = try? map.decodeIfPresent(.eventName)
        eventHost = try? map.decodeIfPresent(.eventHost)

        sent = (try? map.decodeLosslessIfPresent(.sent)) ?? false
    }
}