//
// Created by Andrei Stoicescu on 18.11.2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation

struct ConciergeSubscription: Identifiable {
    struct Package: Codable, Hashable {
        let id: String
        let name: String
    }

    var id: String { subscriptionID }
    let subscriptionID: String
    let userID: String
    let package: Package
    let amount: String
    let status: String
    let periodStart: Date?
    let periodEnd: Date?
    let cancel: Bool
}

extension ConciergeSubscription: Codable, Hashable {
    enum CodingKeys: String, CodingKey {
        case subscriptionID = "subscription_id"
        case userID = "uid"
        case package = "product"
        case amount
        case status
        case periodStart = "current_period_start"
        case periodEnd = "current_period_end"
        case cancel = "cancel_period_end"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        subscriptionID = try map.decode(.subscriptionID)
        userID = try map.decode(.userID)
        package = try map.decode(.package)
        amount = try map.decode(.amount)
        status = try map.decode(.status)
        periodStart = map.decodeDateIfPresent(.periodStart)
        periodEnd = map.decodeDateIfPresent(.periodEnd)
        cancel = try map.decodeLossless(.cancel)
    }
}