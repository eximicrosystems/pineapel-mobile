//
// Created by Andrei Stoicescu on 26/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation

struct CountResponse: Codable {
    let count: Int

    static var zero: CountResponse {
        CountResponse(count: 0)
    }
}

extension CountResponse {
    enum CodingKeys: String, CodingKey {
        case count
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        count = (try? map.decodeLosslessIfPresent(.count)) ?? 0
    }
}