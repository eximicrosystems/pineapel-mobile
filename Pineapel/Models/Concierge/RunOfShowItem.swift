//
// Created by Andrei Stoicescu on 27/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation

struct RunOfShowItem {
    let id: String
    let locked: Bool
    let name: String
    var status: Bool
}

extension RunOfShowItem: Codable {
    enum CodingKeys: String, CodingKey {
        case id
        case locked
        case name
        case status
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        id = try map.decode(.id)
        locked = try map.decodeLossless(.locked)
        name = try map.decode(.name)
        status = try map.decodeLossless(.status)
    }
}

struct RunOfShowRequest: Encodable {
    let id: String
    let status: Bool

    enum CodingKeys: String, CodingKey {
        case id
        case status
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(status ? "1" : "0", forKey: .status)
        try container.encode(id, forKey: .id)
    }
}

struct RunOfShow {
    var items: [RunOfShowItem]

    var percent: Float {
        guard items.count > 0 else { return 0 }
        return Float(completedItems.count) / Float(items.count)
    }

    var completedItems: [RunOfShowItem] {
        items.filter { $0.status }
    }
}