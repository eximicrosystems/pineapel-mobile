//
// Created by Andrei Stoicescu on 02/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation

struct ConciergeTag: Decodable {
    let name: String
    let tagID: String

    enum CodingKeys: String, CodingKey {
        case name
        case tagID = "tid"
    }
}