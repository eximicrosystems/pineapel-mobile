//
// Created by Andrei Stoicescu on 17/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit

struct PackageRequest: Encodable {
    let buildingID: String
    let from: String
    let image: String
    let receivedBy: String
    let recipient: String
    let signedBy: String
    let name: String

    enum CodingKeys: String, CodingKey {
        case buildingID = "field_building"
        case from = "field_from"
        case image = "field_image"
        case receivedBy = "field_received_by"
        case recipient = "field_recipient"
        case signedBy = "field_signed_by"
        case name = "name"
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeSingleItemArray(self.buildingID, paramName: "target_id", forKey: .buildingID)
        try container.encodeSingleItemArray(self.from, paramName: "target_id", forKey: .from)
        try container.encodeSingleItemArray(self.image, paramName: "target_id", forKey: .image)
        try container.encodeSingleItemArray(self.receivedBy, paramName: "target_id", forKey: .receivedBy)
        try container.encodeSingleItemArray(self.recipient, paramName: "target_id", forKey: .recipient)
        try container.encodeSingleItemArray(self.signedBy, paramName: "target_id", forKey: .signedBy)
        try container.encodeValueCodableItemArray(self.name, forKey: .name)
    }
}

struct Package: Codable, Identifiable {
    enum Status: String, Codable {
        // On the Package model these come capitalized. in the query string they must be sent lowercased
        case pending = "Pending"
        case delivered = "Delivered"

        var displayString: String {
            switch self {
            case .pending:
                return "New"
            case .delivered:
                return "Past"
            }
        }

        var detailsDisplayString: String {
            switch self {
            case .pending:
                return "Pending"
            case .delivered:
                return "Delivered"
            }
        }

        var color: UIColor {
            switch self {
            case .pending:
                return AVSColor.accentBackground.color
            case .delivered:
                return AVSColor.colorGreen.color
            }
        }

        var nextStatus: Status? {
            if self == .pending {
                return .delivered
            }
            return nil
        }
    }

    let id: String
    let createdAt: Date?
    let apartment: String
    let buildingID: String
    let firstName: String
    let lastName: String
    let from: String
    let image: URL?
    let receivedByID: String
    let recipientID: String
    let signedByID: String
    var status: Status
    let name: String
    let receivedByFirstName: String
    let receivedByLastName: String

    enum CodingKeys: String, CodingKey {
        case id
        case createdAt = "created"
        case apartment = "field_apartment_nr"
        case buildingID = "field_building"
        case firstName = "field_first_name"
        case lastName = "field_last_name"
        case from = "field_from"
        case image = "field_image"
        case receivedByID = "field_received_by"
        case recipientID = "field_recipient"
        case signedByID = "field_signed_by"
        case status = "field_status"
        case name
        case receivedByFirstName = "received_by_first_name"
        case receivedByLastName = "received_by_last_name"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        id = try map.decode(.id)
        createdAt = map.decodeDateIfPresent(.createdAt)
        apartment = try map.decode(.apartment)
        buildingID = try map.decode(.buildingID)
        firstName = try map.decode(.firstName)
        lastName = try map.decode(.lastName)
        from = try map.decode(.from)
        image = try? map.decode(.image)
        receivedByID = try map.decode(.receivedByID)
        recipientID = try map.decode(.recipientID)
        signedByID = try map.decode(.signedByID)
        status = try map.decode(.status)
        name = try map.decode(.name)
        receivedByFirstName = try map.decode(.receivedByFirstName)
        receivedByLastName = try map.decode(.receivedByLastName)
    }

    var fullName: String {
        [firstName, lastName].joined(separator: " ")
    }
}

struct PackageCreateResponse: Decodable {
    let id: String

    enum CodingKeys: String, CodingKey {
        case id = "id"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        let valueCodable: ValueCodable<Int> = try map.decodeSingleItem(.id)
        self.id = String(valueCodable.value)
    }
}

struct PackageDeliveryRequest: Encodable {
    var buildingID: String
    var status: Package.Status
    var id: String

    enum CodingKeys: String, CodingKey {
        case buildingID = "field_building"
        case status = "field_status"
        case id = "id"
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeSingleItemArray(self.buildingID, paramName: "target_id", forKey: .buildingID)
        try container.encodeValueCodableItemArray(self.status.rawValue.lowercased(), forKey: .status)
        try container.encodeValueCodableItemArray(self.id, forKey: .id)
    }
}