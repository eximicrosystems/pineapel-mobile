//
// Created by Andrei Stoicescu on 01.03.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import DifferenceKit


struct ConciergeALaCarteProduct: Hashable, Identifiable, Differentiable {
    var id: String { productID }
    var fileId: String?
    var name: String?
    var description: String?
    var productImageURL: URL?
    var price: String?
    var productID: String
    var hasInventory: Bool
    var inventoryCount: String?
    var schedulable: Bool
    var taxPercent: String?
    var weight: String?
    var published: Bool

    var entityType: OrderRequestItem.ItemType

    func hash(into hasher: inout Hasher) { hasher.combine(productID) }

    var differenceIdentifier: Int { hashValue }
}

extension ConciergeALaCarteProduct: Codable {
    enum CodingKeys: String, CodingKey {
        case fileId = "fid"
        case name = "name"
        case productID = "id"
        case price = "field_price"
        case description = "field_description"
        case productImageURL = "field_image"
        case entityType = "entity_type"
        case schedulable = "field_scheduled_fulfilment"
        case taxPercent = "field_tax_percent"
        case inventoryCount = "field_inventory_count"
        case weight = "field_weight"
        case published = "status"
        case hasInventory = "field_has_inventory"
    }

    public init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        fileId = try? map.decodeIfPresent(.fileId)
        name = try? map.decodeIfPresent(.name)
        productID = try map.decode(.productID)

        schedulable = (try? map.decodeLosslessIfPresent(.schedulable)) ?? false
        taxPercent = try? map.decodeIfPresent(.taxPercent)
        price = try? map.decodeIfPresent(.price)

        description = try? map.decodeIfPresent(.description)
        productImageURL = try? map.decodeIfPresent(.productImageURL)
        entityType = try map.decode(.entityType)
        inventoryCount = try? map.decodeIfPresent(.inventoryCount)
        weight = try? map.decodeIfPresent(.weight)
        published = (try? map.decodeLosslessIfPresent(.published)) ?? false
        hasInventory = (try? map.decodeLosslessIfPresent(.hasInventory)) ?? false
    }
}


struct ConciergeALaCarteCategory: Hashable, Identifiable, Differentiable {
    var id: String { categoryId }

    var categoryId: String
    var depth: Int
    var buildingId: String
    var icon: ALaCarteCategory.IconTag?
    var imageUrl: URL?
    var scheduled: Bool
    var name: String
    var parentId: String

    func hash(into hasher: inout Hasher) { hasher.combine(categoryId) }

    var differenceIdentifier: Int { hashValue }
}

extension ConciergeALaCarteCategory: Codable {
    enum CodingKeys: String, CodingKey {
        case categoryId = "tid"
        case depth
        case buildingId = "field_building"
        case icon = "field_icon_tag"
        case imageUrl = "field_image"
        case scheduled = "field_scheduled_fulfilment"
        case name
        case parentId = "parent"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        categoryId = try map.decode(.categoryId)
        depth = (try? map.decodeIfPresent(.depth)) ?? 0
        buildingId = try map.decode(.buildingId)
        icon = try? map.decodeIfPresent(.icon)
        imageUrl = try? map.decodeIfPresent(.imageUrl)
        scheduled = try map.decodeLossless(.scheduled)
        name = try map.decode(.name)
        parentId = try map.decode(.parentId)
    }
}

struct ConciergeProductRequest: Encodable {
    let categoryId: String
    let description: String
    let hasInventory: Bool
    let imageId: String
    let inventoryCount: String
    let price: String
    let taxPercent: String
    let weight: String
    let id: String?
    let name: String
    let published: Bool

    enum CodingKeys: String, CodingKey {
        case categoryId = "field_category"
        case description = "field_description"
        case hasInventory = "field_has_inventory"
        case imageId = "field_image"
        case inventoryCount = "field_inventory_count"
        case price = "field_price"
        case taxPercent = "field_tax_percent"
        case weight = "field_weight"
        case id = "id"
        case name = "name"
        case published = "status"
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeSingleItemArray(categoryId, paramName: "target_id", forKey: .categoryId)
        try container.encodeItemArray(["value": description, "format": "basic_html"], forKey: .description)
        try container.encodeValueCodableItemArray(hasInventory ? "1" : "0", forKey: .hasInventory)
        try container.encodeSingleItemArray(imageId, paramName: "target_id", forKey: .imageId)
        try container.encodeValueCodableItemArray(inventoryCount, forKey: .inventoryCount)
        try container.encodeValueCodableItemArray(price, forKey: .price)
        try container.encodeValueCodableItemArray(taxPercent, forKey: .taxPercent)
        try container.encodeValueCodableItemArray(weight, forKey: .weight)
        try container.encodeValueCodableItemArray(name, forKey: .name)
        try container.encodeValueCodableItemArrayIfPresent(id, forKey: .id)
    }
}