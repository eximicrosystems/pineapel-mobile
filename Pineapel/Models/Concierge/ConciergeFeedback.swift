//
// Created by Andrei Stoicescu on 24.02.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import DifferenceKit

struct ConciergeFeedback: Codable, Identifiable, Hashable, Differentiable {
    let buildingId: String
    let buildingName: String
    let comment: String
    let commentTeaser: String
    let conciergeFirstName: String
    let conciergeLastName: String
    let created: String
    let apartment: String
    let conciergeId: String
    let firstName: String
    let lastName: String
    let rating: String
    let id: String
    let type: String
    let typeId: Feedback.FeedbackType
    let userId: String
    let userPicture: URL?

    enum CodingKeys: String, CodingKey {
        case buildingId = "building"
        case buildingName = "building_name"
        case comment
        case commentTeaser = "comment_teaser"
        case conciergeFirstName = "concierge_first_name"
        case conciergeLastName = "concierge_last_name"
        case created
        case apartment = "field_apartment_nr"
        case conciergeId = "field_concierge"
        case firstName = "field_first_name"
        case lastName = "field_last_name"
        case rating = "field_rating"
        case id
        case type
        case typeId = "type_id"
        case userId = "user_id"
        case userPicture = "user_picture"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        buildingId = try map.decode(.buildingId)
        buildingName = try map.decode(.buildingName)
        comment = try map.decode(.comment)
        commentTeaser = try map.decode(.commentTeaser)
        conciergeFirstName = try map.decode(.conciergeFirstName)
        conciergeLastName = try map.decode(.conciergeLastName)
        created = try map.decode(.created)
        apartment = try map.decode(.apartment)
        conciergeId = try map.decode(.conciergeId)
        firstName = try map.decode(.firstName)
        lastName = try map.decode(.lastName)
        rating = try map.decode(.rating)
        id = try map.decode(.id)
        type = try map.decode(.type)
        typeId = try map.decode(.typeId)
        userId = try map.decode(.userId)
        userPicture = try? map.decodeIfPresent(.userPicture)
    }

    var differenceIdentifier: Int { hashValue }

    var fullName: String {
        [firstName, lastName].filter { !String.isBlank($0) }.joined(separator: " ")
    }

    var conciergeFullName: String {
        [conciergeFirstName, conciergeLastName].filter { !String.isBlank($0) }.joined(separator: " ")
    }

    var ratingNumber: Int? {
        Int(rating.split(separator: "/").first ?? "0")
    }

    var displayRating: String {
        if let number = ratingNumber, number <= 5, number >= 0 {
            return (1...5).map { i -> String in
                if number >= i {
                    return "⭑"
                } else {
                    return "⭒"
                }
            }.joined(separator: "")
        }

        return rating
    }

    var displayAbout: String {
        switch typeId {
        case .building:
            return "\(type) about \(buildingName)"
        case .concierge:
            return "\(type) about \(conciergeFullName)"
        case .other:
            return type
        }
    }
    var displayApartment: String? {
        String.isBlank(apartment) ? nil : "apt #\(apartment)"
    }
}