//
// Created by Andrei Stoicescu on 02/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation

struct ConciergeNotificationRequest: Encodable {
    typealias NotificationType = FeedNotificationItem.NotificationType

    // Existing Identifier
    let id: String?

    // Common
    let notificationType: NotificationType
    let name: String
    let buildingID: String
    let recipientIDs: [String]
    let mediaFileID: [String]

    // information, fitness
    let body: String?

    // information
    let articleID: String?
    let externalLink: String?

    // delivery, mail_delivery, packages
    let deliveryTime: Date?

    // delivery
    let deliveryItem: String?

    // fitness
    let startDate: Date? // also in events
    let fitnessType: String?
    let fitnessInstructor: String?
    let fitnessDuration: String?

    // packages
    let packagesNumber: String?
    let packagesSignedByConciergeID: String?

    // events
    let eventName: String?
    let eventHost: String?

    enum CodingKeys: String, CodingKey, Identifiable {
        var id: String { rawValue }

        case id = "id"
        case notificationType = "type"
        case name = "name"
        case buildingID = "building"
        case recipientIDs = "field_recipient"
        case mediaFileID = "field_media_file"
        case body = "field_body"
        case articleID = "field_internal_link"
        case externalLink = "field_external_link"
        case deliveryTime = "field_delivery_time"
        case deliveryItem = "field_item_for_delivery"
        case startDate = "field_start_date"
        case fitnessType = "field_type"
        case fitnessInstructor = "field_instructor"
        case fitnessDuration = "field_duration"
        case packagesNumber = "field_number"
        case packagesSignedByConciergeID = "field_signed_by"
        case eventName = "field_event_name"
        case eventHost = "field_host"
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encodeIfPresent(id, forKey: .id)

        try container.encode(notificationType, forKey: .notificationType)
        try container.encode(name, forKey: .name)
        try container.encode(buildingID, forKey: .buildingID)

        if recipientIDs.count == 1,
           let first = recipientIDs.first {
            try container.encode(first, forKey: .recipientIDs)
        } else {
            try container.encode(recipientIDs, forKey: .recipientIDs)
        }

        try? container.encodeIfPresent(mediaFileID, forKey: .mediaFileID)
        try? container.encodeIfPresent(body, forKey: .body)
        try? container.encodeIfPresent(articleID, forKey: .articleID)
        try? container.encodeIfPresent(externalLink, forKey: .externalLink)
        try? container.encodeIfPresent(deliveryTime?.timeIntervalString, forKey: .deliveryTime)
        try? container.encodeIfPresent(deliveryItem, forKey: .deliveryItem)
        try? container.encodeIfPresent(startDate?.timeIntervalString, forKey: .startDate)
        try? container.encodeIfPresent(fitnessType, forKey: .fitnessType)
        try? container.encodeIfPresent(fitnessInstructor, forKey: .fitnessInstructor)
        try? container.encodeIfPresent(fitnessDuration, forKey: .fitnessDuration)
        try? container.encodeIfPresent(packagesNumber, forKey: .packagesNumber)
        try? container.encodeIfPresent(packagesSignedByConciergeID, forKey: .packagesSignedByConciergeID)
        try? container.encodeIfPresent(eventName, forKey: .eventName)
        try? container.encodeIfPresent(eventHost, forKey: .eventHost)
    }

    static func codingKeys(notificationType: NotificationType) -> [CodingKeys] {
        var keys: [CodingKeys] = [.notificationType, .name, .buildingID, .recipientIDs, .mediaFileID]

        switch notificationType {
        case .info:
            keys += [.body, .articleID, .externalLink]
        case .packages:
            keys += [.deliveryTime, .packagesNumber, .packagesSignedByConciergeID]
        case .mailDelivery:
            keys += [.deliveryTime]
        case .fitness:
            keys += [.startDate, .fitnessType, .fitnessInstructor, .fitnessDuration]
        case .delivery:
            keys += [.deliveryTime, .deliveryItem]
        case .event:
            keys += [.body, .startDate, .eventName, .eventHost]
        }

        return keys
    }
}