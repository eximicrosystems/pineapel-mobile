//
// Created by Andrei Stoicescu on 16.11.2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import DifferenceKit

struct ActivityLogRequest: Encodable {
    let id: String?
    let body: String
    let subject: String
    let buildingID: String

    enum CodingKeys: String, CodingKey {
        case id
        case body = "field_body"
        case subject = "name"
        case buildingID = "field_building"
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encodeValueCodableItemArrayIfPresent(id, forKey: .id)
        try? container.encodeItemArray(["value": body, "format": "basic_html"], forKey: .body)
        try? container.encodeValueCodableItemArrayIfPresent(subject, forKey: .subject)
        try? container.encodeSingleItemArrayIfPresent(buildingID, paramName: "target_id", forKey: .buildingID)
    }
}

struct ActivityLog: Identifiable, Hashable {
    enum ActivityLogType: String, Codable {
        case automatic = "Automatic"
        case manual = "Manual"
    }

    let id: String
    let shift: String?
    let createdAt: Date?
    let subject: String?
    let body: String?
    let buildingID: String?
    let type: ActivityLogType

    var isEditable: Bool {
        type == .manual
    }
}

extension ActivityLog: Differentiable {
    func hash(into hasher: inout Hasher) { hasher.combine(id) }
    var differenceIdentifier: Int { hashValue }
}

extension ActivityLog: Codable {
    enum CodingKeys: String, CodingKey {
        case id
        case shift = "shift_name"
        case createdAt = "created"
        case subject = "name"
        case body = "field_body"
        case buildingID = "field_building"
        case type = "field_type"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        id = try map.decode(.id)
        shift = try? map.decodeIfPresent(.shift)
        createdAt = map.decodeDateIfPresent(.createdAt)
        subject = try? map.decodeIfPresent(.subject)
        body = try? map.decodeIfPresent(.body)
        buildingID = try? map.decodeIfPresent(.buildingID)
        type = (try? map.decodeIfPresent(.type)) ?? .automatic
    }
}