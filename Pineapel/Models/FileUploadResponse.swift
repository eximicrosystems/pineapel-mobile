//
// Created by Andrei Stoicescu on 06/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation

//struct AVSValueTransformer<T: Decodable>: DecodableTransformer {
//    typealias Value = T?
//
//    func value(from decoder: Decoder) throws -> Value {
//        let container = try? decoder.singleValueContainer()
//        let valueContainer = try container?.decode([[String: T]].self)
//        return valueContainer?.first?["value"]
//    }
//}

struct FileUploadResponse: Decodable {
    var fileID: Int
    var url: URL?
    var fileName: String?
    var fileMime: String?

    enum CodingKeys: String, CodingKey {
        case fileID = "fid"
        case url = "uri"
        case fileName = "filename"
        case fileMime = "filemime"
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        let fileIDContainer = try container.decodeIfPresent([ValueCodable<Int>].self, forKey: .fileID)
        guard let fileID = fileIDContainer?.first?.value else {
            throw APIError(message: "File ID missing")
        }
        self.fileID = fileID

        let urlContainer = try container.decodeIfPresent([[String: String]].self, forKey: .url)
        self.url = URL(string: urlContainer?.first?["url"] ?? "")

        let fileNameContainer = try container.decodeIfPresent([ValueCodable<String>].self, forKey: .fileName)
        self.fileName = fileNameContainer?.first?.value

        let fileMimeContainer = try container.decodeIfPresent([ValueCodable<String>].self, forKey: .fileMime)
        self.fileMime = fileMimeContainer?.first?.value
    }
}
