//
// Created by Andrei Stoicescu on 20/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit
import DateToolsSwift
import PromiseKit
import DifferenceKit

struct AmenityReservationResponse: Decodable {
    let reservationID: String

    enum CodingKeys: String, CodingKey {
        case reservationID = "id"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)

        let reservationIDValue = try map.decode([ValueCodable<Int>].self, forKey: .reservationID)

        guard let reservationID = reservationIDValue.first?.value else {
            throw AmenityReservationError.idDecodingError
        }

        self.reservationID = String(reservationID)
    }
}

struct AmenityReservation: Identifiable {
    enum Status: String, Codable {
        case pending = "Pending"
        case accepted = "Accepted"
        case rejected = "Rejected"

        var string: String {
            switch self {
            case .pending:
                return "Pending"
            case .accepted:
                return "Accepted"
            case .rejected:
                return "Declined"
            }
        }

        var conciergeStatusColor: UIColor {
            switch self {
            case .rejected:
                return AVSColor.accentBackground.color
            case .accepted:
                return AVSColor.colorGreen.color
            case .pending:
                return AVSColor.colorBlue.color
            }
        }
    }

    var id: String { amenityID }

    let amenityID: String
    let amenityName: String?
    let price: Float?
    let note: String?

    // for amenity reservations with daily reservations. Decoded in current timezone
    let startDate: Date?
    let endDate: Date?

    // for amenity reservation with hourly reservations. Decoded in current timezone
    let startTime: Date?
    let endTime: Date?

    // date is used for the DATE part of startTime and endTime. Decoded in current timezone
    let date: Date?

    var reservationID: String?
    let firstName: String?
    let lastName: String?
    let userID: String?
    var status: Status?
    let userPictureURL: URL?
    let guestName: String?
    let guestPhone: String?

    enum CodingKeys: String, CodingKey {
        case amenityID = "field_amenity"
        case note = "field_note"
        case startDate = "field_start_date"
        case endDate = "field_end_date"
        case startTime = "field_start_time"
        case endTime = "field_end_time"
        case date = "field_date"
        case amenityName = "field_amenity_name"
        case reservationID = "id"
        case firstName = "field_first_name"
        case lastName = "field_last_name"
        case status = "field_status"
        case price = "total_price_with_tax"
        case userID = "uid"
        case userPictureURL = "user_picture"
        case guestName = "field_guest_name"
        case guestPhone = "field_guest_phone_number"
    }

    var fullName: String {
        [firstName, lastName].compactMap { $0 }.joined(separator: " ")
    }

    func dateString(amenity: Amenity) -> String? {
        if amenity.priceType == .day {
            return Self.dateString(amenity: amenity, startDate: startDate, endDate: endDate)
        } else {
            return Self.dateString(amenity: amenity, startDate: startTime, endDate: endTime)
        }
    }

    func timeString(amenity: Amenity) -> String? {
        if amenity.priceType == .day {
            return Self.timeString(amenity: amenity, startDate: startDate, endDate: endDate)
        } else {
            return Self.timeString(amenity: amenity, startDate: startTime, endDate: endTime)
        }
    }

    static func dateString(amenity: Amenity, startDate: Date?, endDate: Date?) -> String? {
        if amenity.priceType == .day {
            guard let startDate = startDate, let endDate = endDate else { return nil }
            return "\(startDate.avsDateShortMonthString()) - \(endDate.avsDateShortMonthString()), \(startDate.year)"
        } else {
            guard let startDate = startDate else { return nil }
            return "\(startDate.avsDateShortMonthString()), \(startDate.year)"
        }
    }

    static func timeString(amenity: Amenity, startDate: Date?, endDate: Date?) -> String? {
        if amenity.priceType == .day {
            return nil
        } else {
            guard let startDate = startDate, let endDate = endDate else { return nil }
            return "\(startDate.avsTimeString()) to \(endDate.avsTimeString())"
        }
    }

    // Factory for creating daily reservation params
    static func buildDailyReservation(amenityID: String, note: String?, startDate: Date?, endDate: Date?, guestName: String?, guestPhone: String?) -> AmenityReservation {
        AmenityReservation(amenityID: amenityID,
                           amenityName: nil,
                           price: nil,
                           note: note,
                           startDate: startDate,
                           endDate: endDate,
                           startTime: nil,
                           endTime: nil,
                           date: nil,
                           reservationID: nil,
                           firstName: nil, lastName: nil, userID: nil, status: nil, userPictureURL: nil,
                           guestName: guestName,
                           guestPhone: guestPhone)
    }

    // Factory for creating slot/flat fee reservation params
    static func buildSlotReservation(amenityID: String, note: String?, startTime: Date?, endTime: Date?) -> AmenityReservation {
        AmenityReservation(amenityID: amenityID,
                           amenityName: nil,
                           price: nil,
                           note: note,
                           startDate: nil,
                           endDate: nil,
                           startTime: startTime,
                           endTime: endTime,
                           date: startTime,
                           reservationID: nil,
                           firstName: nil, lastName: nil, userID: nil, status: nil, userPictureURL: nil,
                           guestName: nil,
                           guestPhone: nil)
    }

}

extension AmenityReservation: Hashable, Differentiable {
    var differenceIdentifier: Int { hashValue }
}

enum AmenityReservationError: Error {
    case dateDecodingError
    case timeDecodingError
    case idDecodingError
}

// Encodable for cache. Use AmenityReservationRequest for amenity reservation encoded request params
extension AmenityReservation: Codable {
    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        amenityID = try map.decode(.amenityID)
        amenityName = try? map.decodeIfPresent(.amenityName)
        note = try? map.decodeIfPresent(.note)
        reservationID = try? map.decodeIfPresent(.reservationID)
        firstName = try? map.decodeIfPresent(.firstName)
        lastName = try? map.decodeIfPresent(.lastName)
        status = try? map.decodeIfPresent(.status)
        userID = try? map.decodeIfPresent(.userID)

        let price = try? map.decodeLosslessIfPresent(.price, as: Float.self)
        if let price = price {
            self.price = price / 100
        } else {
            self.price = nil
        }

        let calendar = Calendar.current

        date = map.decodeDateIfPresent(.date)

        // Server has minutes, decoder uses both minutes (from server results) and iso8601 for local cache
        // Since we don't know what case is here, when decoding the startTime first as a date,
        //    ignore any numeric data present, but allow the dates to be parsed if they're iso8601
        // Otherwise try the minute-conversion
        if let _ = date,
           let startTimeDate = map.decodeDateIfPresent(.startTime, skipUnix: true),
           let endTimeDate = map.decodeDateIfPresent(.endTime, skipUnix: true) {
            startTime = startTimeDate
            endTime = endTimeDate
        } else if let date = date,
                  let startTimeInt = try? map.decodeLosslessIfPresent(.startTime, as: Int.self),
                  let endTimeInt = try? map.decodeLosslessIfPresent(.endTime, as: Int.self) {

            let midnight = calendar.startOfDay(for: date)
            startTime = midnight.add(startTimeInt.minutes)
            endTime = midnight.add(endTimeInt.minutes)
        } else {
            startTime = nil
            endTime = nil
        }

        startDate = map.decodeDateIfPresent(.startDate)
        endDate = map.decodeDateIfPresent(.endDate)

        userPictureURL = try? map.decodeIfPresent(.userPictureURL)
        guestName = try? map.decodeIfPresent(.guestName)
        guestPhone = try? map.decodeIfPresent(.guestPhone)
    }
}

extension AmenityReservation {
    static var availabilityCheckEndDate: Date {
        2.months.later.end(of: .month)
    }

    static var dailyReservationAvailabilityCheckEndDate: Date {
        5.months.later.end(of: .month)
    }

    var durationString: String? {
        guard let reservationStartTime = startTime,
              let reservationEndTime = endTime else {
            return nil
        }

        let chunk = reservationStartTime.chunkBetween(date: reservationEndTime)

        var chunkString = ""

        let g = L10n.General.self

        if chunk.hours > 0 {
            chunkString = "\(chunk.hours) \(chunk.hours == 1 ? g.hour : g.hours)"
            if chunk.minutes > 0 {
                chunkString += " \(g.and) \(chunk.minutes) \(g.minutes)"
            }
        } else {
            if chunk.minutes > 0 {
                chunkString += "\(chunk.minutes) \(g.minutes)"
            }
        }

        return chunkString
    }

    func cancellable(buildingTimeZone: TimeZone) -> Bool {
        // start date is in current TZ
        // ex: 8:00 GMT+2
        // we need to convert it to the building's timezone keeping the same hour
        // ex: 8:00 GMT+2 -> 8:00 America/NY
        guard let startTime = startTime ?? startDate else { return false }
        return startTime.convertedToSameTimeZone(buildingTimeZone).isLater(than: Date())
    }

    func editable(buildingTimeZone: TimeZone, amenity: Amenity) -> Bool {
        cancellable(buildingTimeZone: buildingTimeZone)
        &&
        (
                amenity.isFree
                ||
                (amenity.priceType == .flat && AppState.firebaseFlags.editableFlatReservation)
        )
    }
}

struct AmenityReservationRequest: Encodable {
    let amenityID: String
    let note: String?

    // for amenity reservations with daily reservations. Decoded in current timezone
    let startDate: Date?
    let endDate: Date?

    // for amenity reservation with hourly reservations. Decoded in current timezone
    let startTime: Date?
    let endTime: Date?

    // date is used for the DATE part of startTime and endTime. Decoded in current timezone
    let date: Date?

    var reservationID: String?

    var guestName: String?
    var guestPhone: String?

    enum CodingKeys: String, CodingKey {
        case amenityID = "field_amenity"
        case note = "field_note"
        case startDate = "field_start_date"
        case endDate = "field_end_date"
        case startTime = "field_start_time"
        case endTime = "field_end_time"
        case date = "field_date"
        case reservationID = "id"
        case guestName = "field_guest_name"
        case guestPhone = "field_guest_phone_number"
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encodeValueCodableItemArrayIfPresent(reservationID, forKey: .reservationID)
        try container.encodeSingleItemArray(amenityID, paramName: "target_id", forKey: .amenityID)
        try? container.encodeValueCodableItemArrayIfPresent(reservationHourKey(date: startTime), forKey: .startTime)
        try? container.encodeValueCodableItemArrayIfPresent(reservationHourKey(date: endTime), forKey: .endTime)
        try? container.encodeValueCodableItemArrayIfPresent(reservationDateFormatted(date: date), forKey: .date)
        try? container.encodeValueCodableItemArrayIfPresent(reservationDateFormatted(date: startDate), forKey: .startDate)
        try? container.encodeValueCodableItemArrayIfPresent(reservationDateFormatted(date: endDate), forKey: .endDate)
        try? container.encodeValueCodableItemArrayIfPresent(guestName, forKey: .guestName)
        try? container.encodeValueCodableItemArrayIfPresent(guestPhone, forKey: .guestPhone)
        try? container.encodeValueCodableItemArrayIfPresent(note, forKey: .note)
    }

    static func withReservation(_ reservation: AmenityReservation) -> AmenityReservationRequest {
        AmenityReservationRequest(amenityID: reservation.amenityID,
                                  note: reservation.note,
                                  startDate: reservation.startDate,
                                  endDate: reservation.endDate,
                                  startTime: reservation.startTime,
                                  endTime: reservation.endTime,
                                  date: reservation.date,
                                  reservationID: reservation.reservationID,
                                  guestName: reservation.guestName,
                                  guestPhone: reservation.guestPhone)
    }
}

extension AmenityReservationRequest {
    func reservationHourKey(date: Date?) -> String? {
        guard let date = date else { return nil }
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minute = calendar.component(.minute, from: date)
        return String(hour * 60 + minute)
    }

    func reservationDateFormatted(date: Date?) -> String? {
        guard let date = date else { return nil }
        return date.format(with: "yyyy-MM-dd")
    }
}

class AmenityPriceFetcher {
    struct PriceQuery: Equatable {
        let startTime: Date
        let endTime: Date
        let discountCodes: [String]?
        let products: [CartProduct]
    }

    struct Price {
        let query: PriceQuery
        let price: Float
        let items: [OrderResponseItem]
        var priceString: String {
            String(format: "$%.2f", price)
        }
    }

    var cachedData = [Price]()
    var currentQuery: PriceQuery?
    let apiService = ResidentService()
    var amenity: Amenity

    func getPrice(query: PriceQuery) -> Promise<Price> {
        currentQuery = query

        let cached = cachedData.first {
            $0.query == query
        }

        if let cached = cached {
            return .value(cached)
        } else {
            var items: [OrderRequestItem]
            if amenity.priceType == .day {
                items = [OrderRequestItem(type: .amenity,
                                          id: amenity.amenityID,
                                          quantity: nil,
                                          dueTimestamp: nil,
                                          reservationID: nil,
                                          startTime: nil,
                                          endTime: nil,
                                          startDate: query.startTime,
                                          endDate: query.endTime)]
            } else {
                items = [OrderRequestItem(type: .amenity,
                                          id: amenity.amenityID,
                                          quantity: nil,
                                          dueTimestamp: nil,
                                          reservationID: nil,
                                          startTime: query.startTime,
                                          endTime: query.endTime,
                                          startDate: nil,
                                          endDate: nil)]
            }

            if query.products.count > 0 {
                items.append(contentsOf: query.products.map(\.orderRequestItem))
            }

            return apiService.calculateOrder(params: OrderRequestData(
                    paymentMethodID: nil,
                    items: items,
                    discountCodes: query.discountCodes)).then { responses in
                Promise<Price> { seal in
                    let total = PaymentSummary.OrderSummary.fromResponses(responses).totalWithoutTax
                    let price = Price(query: query, price: total, items: responses)
                    self.cachedData.append(price)
                    seal.fulfill(price)
                }
            }
        }
    }

    init(amenity: Amenity) {
        self.amenity = amenity
    }
}