//
// Created by Andrei Stoicescu on 14/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import DifferenceKit

struct LibraryCategory: Codable, Identifiable, Hashable, Differentiable {
    let id: String
    let name: String

    enum CodingKeys: String, CodingKey {
        case id = "tid"
        case name
    }

    func hash(into hasher: inout Hasher) { hasher.combine(id) }
    var differenceIdentifier: Int { hashValue }
}

struct LibraryContent: Codable, Hashable, Identifiable {
    let id: String
    let url: URL
    let name: String

    enum CodingKeys: String, CodingKey {
        case id = "target_id"
        case url
        case name
    }
}

struct LibraryItem: Codable, Hashable, Identifiable, Differentiable {
    enum ItemType: String, Codable {
        case document
        case video
    }

    let id: String
    let name: String
    let type: ItemType
    let body: String?
    let buildingID: String
    let teaser: String?
    let documents: [LibraryContent]
    let video: LibraryContent?
    let image: LibraryContent?

    enum CodingKeys: String, CodingKey {
        case id
        case name
        case type
        case body = "field_body"
        case buildingID = "field_building"
        case teaser
        case documents = "field_document"
        case video = "field_video"
        case image = "field_image"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        id = try map.decode(.id)
        name = try map.decode(.name)
        type = try map.decode(.type)
        body = try map.decode(.body)
        buildingID = try map.decode(.buildingID)
        teaser = try? map.decodeIfPresent(.teaser)
        documents = (try? map.decode([Safe].self, forKey: .documents).compactMap(\.value)) ?? []
        video = try? map.decodeSingleItem(.video)
        image = try? map.decodeSingleItem(.image)
    }

    func hash(into hasher: inout Hasher) { hasher.combine(id) }
    var differenceIdentifier: Int { hashValue }
}