//
// Created by Andrei Stoicescu on 13/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import DifferenceKit

struct ResidentGuest: Identifiable, Hashable, Differentiable {
    var id: String { guestID }

    enum Frequency: String, Codable, CaseIterable {
        // These are the actual values to send to the server
        case once = "Once"
        case always = "Always"
        case month = "Month"
        case week = "Week"
    }

    let guestID: String
    let frequency: Frequency?
    let phoneNumber: String?
    let startDate: Date?
    let endDate: Date?
    let name: String?
    let apartment: String?

    func hash(into hasher: inout Hasher) { hasher.combine(guestID) }
    var differenceIdentifier: Int { hashValue }
}

struct ResidentGuestRequest: Encodable {
    var guest: ResidentGuest

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: ResidentGuest.CodingKeys.self)

        try? container.encodeValueCodableItemArrayIfPresent(guest.frequency, forKey: .frequency)
        try? container.encodeValueCodableItemArrayIfPresent(guest.phoneNumber, forKey: .phoneNumber)
        try? container.encodeValueCodableItemArrayIfPresent(guest.name, forKey: .name)

        if let startDate = guest.startDate {
            try container.encodeValueCodableItemArray(Int(startDate.timeIntervalSince1970), forKey: .startDate)
        }

        if let endDate = guest.endDate {
            try container.encodeValueCodableItemArray(Int(endDate.timeIntervalSince1970), forKey: .endDate)
        }
    }
}

extension ResidentGuest: Codable {
    enum CodingKeys: String, CodingKey {
        case frequency = "field_frequency"
        case phoneNumber = "field_phone_number"
        case startDate = "field_start_date"
        case endDate = "field_end_date"
        case name = "name"
        case guestID = "id"
        case apartment = "field_apartment_nr"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        guestID = try map.decode(.guestID)
        frequency = try? map.decodeIfPresent(.frequency)
        phoneNumber = try? map.decodeIfPresent(.phoneNumber)
        startDate = map.decodeDateIfPresent(.startDate)
        endDate = map.decodeDateIfPresent(.endDate)
        name = try? map.decodeIfPresent(.name)
        apartment = try? map.decodeIfPresent(.apartment)
    }
}

struct Visitor: Codable, Identifiable {
    let id: String
    let created: Date?
    let apartment: String?
    let name: String?
    let buildingName: String?
    let reason: String?

    enum CodingKeys: String, CodingKey {
        case id
        case created
        case apartment = "apt"
        case name
        case buildingName = "name_1"
        case reason
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        id = try map.decode(.id)
        created = map.decodeDateIfPresent(.created)
        apartment = try? map.decodeIfPresent(.apartment)
        name = try? map.decodeIfPresent(.name)
        buildingName = try? map.decodeIfPresent(.buildingName)
        reason = try? map.decodeIfPresent(.reason)
    }
}

struct VisitorRequest: Encodable {
    let name: String
    let apartment: String
    let reason: String
    let resident = "0"

    enum CodingKeys: String, CodingKey {
        case name
        case apartment = "apt"
        case reason
        case resident
    }
}