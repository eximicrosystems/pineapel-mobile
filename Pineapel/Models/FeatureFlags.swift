//
// Created by Andrei Stoicescu on 14/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import Disk

extension CodableCacheKey {
    static var featureFlags = CodableCacheKey(key: "feature_flags")
}

struct FeatureFlags: Codable {
    enum Flag: String, CaseIterable, Codable {
        case chat = "chat"
        case profile = "profile"
        case buildingInfo = "building_info"
        case leasingOffice = "leasing_office"
        case reservations = "reservations"
        case requests = "requests"
        case thingsToKnow = "things_to_know"
        case concierges = "concierges"
        case allowedGuestList = "allowed_guest_list"
        case payment = "payment"
        case aLaCarte = "a_la_carte"
        case orderHistory = "order_history"
        case feedback = "feedback"
        case notifications = "notifications"
        case help = "help"
        case terms = "terms"
        case privacy = "privacy"
        case maintenanceRequest = "maintenance_request"
        case subscriptions = "subscriptions"
        case documents = "documents"
        case videoLibrary = "video_library"
    }

    let enabledFlags: [Flag]

    func isEnabled(_ flag: Flag) -> Bool {
        enabledFlags.contains(flag)
    }

    static var all: FeatureFlags = {
        FeatureFlags(enabledFlags: Flag.allCases)
    }()

    static var `none`: FeatureFlags = {
        FeatureFlags(enabledFlags: [])
    }()

    static func allExcept(_ flags: [Flag]) -> FeatureFlags {
        FeatureFlags(enabledFlags: Flag.allCases.filter { !flags.contains($0) })
    }

    static func cached() -> FeatureFlags? {
        FeatureFlags.readCached(.featureFlags)
    }

    func save() {
        cache(.featureFlags)
    }
}