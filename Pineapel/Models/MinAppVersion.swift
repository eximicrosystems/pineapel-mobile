//
// Created by Andrei Stoicescu on 23/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation

struct MinAppVersion: Decodable {
    let version: String?

    enum CodingKeys: String, CodingKey {
        case version = "minimum_ios_version"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        self.version = try? map.decodeIfPresent(.version)
    }
}