//
// Created by Andrei Stoicescu on 10/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import PromiseKit
import SwiftyUserDefaults
import DifferenceKit

struct BuildingDetails: Codable, ArticleProtocol, Identifiable {
    var id: String { buildingID }
    let name: String?
    let buildingID: String
    let bodyText: String?
    let teaser: String?
    let images: [URL]?
    let apartmentAmenities: String?
    let communityAmenities: String?
    let address: String?
    let rooms: String?
    let timezone: String?

    enum CodingKeys: String, CodingKey {
        case name
        case buildingID = "id"
        case bodyText = "field_body"
        case teaser = "field_teaser"
        case images = "field_images"
        case apartmentAmenities = "field_apartment_amenities"
        case communityAmenities = "field_community_amenities"
        case address = "field_address"
        case rooms = "field_rooms"
        case timezone = "field_timezone"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        name = try? map.decode(.name)
        buildingID = try map.decode(.buildingID)
        bodyText = try? map.decode(.bodyText)
        teaser = try? map.decode(.teaser)
        images = try? map.decodeIfPresent([Safe<URL>].self, forKey: .images)?.compactMap { $0.value }
        apartmentAmenities = try? map.decode(.apartmentAmenities)
        communityAmenities = try? map.decode(.communityAmenities)
        address = try? map.decode(.address)
        rooms = try? map.decode(.rooms)
        timezone = try? map.decodeIfPresent(.timezone)
    }

    func getBuildingInfoImage() -> URL? {
        guard let images = images else {
            return nil
        }
        if images.count > 0 {
            return images[0]
        }

        return nil
    }

    func getAmenityInfoImage() -> URL? {
        guard let images = images else {
            return nil
        }

        // Preferably return the second image if exists
        if images.count > 1 {
            return images[1]
        } else if images.count > 0 {
            return images[0]
        }

        return nil
    }

    var buildingTimezone: TimeZone? {
        guard let tz = timezone else { return nil }
        return TimeZone(abbreviation: tz)
    }

    // MARK: - ArticleProtocol
    var articleThumbnailURL: URL? {
        images?.first
    }

    var articleBody: String? {
        bodyText
    }

    var articleTitle: String? {
        name
    }
}

extension BuildingDetails {
    static func getBuildingName(force: Bool = false) -> Promise<String> {
        if !force, let buildingName = Defaults.buildingName {
            return .value(buildingName)
        } else {
            return ResidentService().getBuildingDetails().map {
                $0.name ?? ""
            }
        }
    }

    static func getBuildingID() -> Promise<String> {
        if let buildingID = Defaults.buildingID {
            return .value(buildingID)
        } else {
            return ResidentService().getBuildingDetails().map {
                $0.buildingID
            }
        }
    }

    static func getBuildingTimeZone() -> Promise<TimeZone> {
        let defaultTZ = TimeZone(abbreviation: "UTC")!
        if let buildingTimeZone = Defaults.buildingTimeZone {
            return .value(TimeZone(identifier: buildingTimeZone) ?? defaultTZ)
        } else {
            return ResidentService().getBuildingDetails().map {
                if let timezone = $0.timezone {
                    return TimeZone(identifier: timezone) ?? defaultTZ
                }

                return defaultTZ
            }
        }
    }
}

extension BuildingDetails: Hashable, Differentiable {
    var differenceIdentifier: Int { hashValue }
}