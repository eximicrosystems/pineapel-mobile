//
// Created by Andrei Stoicescu on 18/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import DateToolsSwift

struct AvailableDays: Decodable {
    let availableDays: [String: Int]

    enum CodingKeys: String, CodingKey {
        case availableDays = "available_days"
    }

    var notAvailableDates: [Date] {
        let calendar = Calendar.current
        return availableDays.compactMap { stringDate, available in
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = calendar.timeZone
            dateFormatter.dateFormat = "yyyy-MM-dd"
            if available == 0 {
                return dateFormatter.date(from: stringDate)
            }
            return nil
        }
    }
}

struct AvailableHours: Decodable {

    class AvailableInterval {
        var startDate: Date? = nil
        var endDate: Date? = nil
    }

    struct TimeSlot {
        let startDate: Date
        let endDate: Date
        let maxSeats: Int
    }

    struct Interval {
        let minute: Int
        let available: Bool
        let maxSeats: Int
    }

    let availableHours: [String: Int]
    let slotDuration: Int
    let availableSlots: Int

    enum CodingKeys: String, CodingKey {
        case availableHours = "available_hours"
        case slotDuration = "slot_duration"
        case availableSlots = "available_slots"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        availableHours = try map.decode(.availableHours)
        slotDuration = try map.decode(.slotDuration)
        let slotsFloat: Float = try map.decodeLossless(.availableSlots)
        availableSlots = Int(slotsFloat)
    }

    func availableTimeSlots(date: Date) -> [TimeSlot] {
        let midnight = Calendar.current.startOfDay(for: date)

        return availableHours.compactMap { key, value -> Interval? in
            if let intKey = Int(key) {
                return Interval(minute: intKey, available: value > 0, maxSeats: value)
            }
            return nil
        }.sorted { interval1, interval2 in
            interval1.minute < interval2.minute
        }.map { interval in
            let startDate = midnight.add(interval.minute.minutes)
            Log.this(startDate)
            Log.this(startDate.add(slotDuration.minutes))
            return TimeSlot(startDate: startDate, endDate: startDate.add(slotDuration.minutes), maxSeats: interval.maxSeats)
        }
    }

}