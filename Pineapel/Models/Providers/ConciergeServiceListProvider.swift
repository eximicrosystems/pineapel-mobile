//
// Created by Andrei Stoicescu on 05.03.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import Combine
import PromiseKit

struct CategoryService: Codable {
    var service: ConciergeServiceModel
    var category: ALaCarteCategory?
}

class ConciergeServiceListProvider {

    enum ListType {
        case user(userId: String)
        case serviceList(buildingId: String, serviceId: String)
    }

    private var services = [ConciergeServiceModel]()
    private var categories = [ALaCarteCategory]()
    var categoryServices = [CategoryService]()

    var listType: ListType

    init(listType: ListType) {
        self.listType = listType
    }

    func loadCategories() -> Promise<Void> {
        ResidentService().getALaCarteCategories().map { [weak self] categories in
            self?.categories = categories
        }
    }

    func loadServices() -> Promise<Void> {
        switch listType {
        case .user(let userId):
            return ConciergeService().getServices(userId: userId).map { [weak self] services in
                self?.services = services
            }
        case .serviceList(let buildingId, let serviceId):
            return ConciergeService().getServiceProviders(per: 50, page: 0, buildingId: buildingId, serviceId: serviceId).map { [weak self] services in
                self?.services = services
            }
        }
    }

    func reload() -> Promise<Void> {
        when(fulfilled: [
            loadCategories(),
            loadServices()
        ]).map {
            self.remapCategories()
        }
    }

    private func remapCategories(cache: Bool = false) {
        categoryServices = services.map { service in
            CategoryService(service: service, category: findCategory(service.categoryId))
        }
    }

    private func findCategory(_ categoryID: String?) -> ALaCarteCategory? {
        categories.first { category in
            category.categoryID == categoryID
        }
    }
}