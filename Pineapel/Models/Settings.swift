//
// Created by Andrei Stoicescu on 18/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import DifferenceKit

struct Settings: Hashable, Differentiable {
    var pushNotificationsToken: String? = nil
    var conciergeChatMessages: Bool? = nil
    var deliveries: Bool? = nil
    var fitness: Bool? = nil
    var information: Bool? = nil
    var mailDelivery: Bool? = nil
    var packages: Bool? = nil
    var event: Bool? = nil
    var appVersion: String? = nil

    var differenceIdentifier: Int { hashValue }
}

extension Settings: Codable {
    enum CodingKeys: String, CodingKey {
        case pushNotificationsToken = "field_push_notifications_token"
        case conciergeChatMessages = "field_concierge_chat_messages"
        case deliveries = "field_deliveries"
        case fitness = "field_fitness"
        case information = "field_information"
        case mailDelivery = "field_mail_delivery"
        case packages = "field_packages"
        case event = "field_events"
        case appVersion = "field_app_version"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)

        pushNotificationsToken = try? map.decodeIfPresent(.pushNotificationsToken)
        conciergeChatMessages = try? map.decodeLosslessIfPresent(.conciergeChatMessages)
        deliveries = try? map.decodeLosslessIfPresent(.deliveries)
        fitness = try? map.decodeLosslessIfPresent(.fitness)
        information = try? map.decodeLosslessIfPresent(.information)
        mailDelivery = try? map.decodeLosslessIfPresent(.mailDelivery)
        packages = try? map.decodeLosslessIfPresent(.packages)
        event = try? map.decodeLosslessIfPresent(.event)
        appVersion = try? map.decodeIfPresent(.appVersion)
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(pushNotificationsToken, forKey: .pushNotificationsToken)
        try container.encodeNumericBoolIfPresent(conciergeChatMessages, forKey: .conciergeChatMessages)
        try container.encodeNumericBoolIfPresent(deliveries, forKey: .deliveries)
        try container.encodeNumericBoolIfPresent(fitness, forKey: .fitness)
        try container.encodeNumericBoolIfPresent(information, forKey: .information)
        try container.encodeNumericBoolIfPresent(mailDelivery, forKey: .mailDelivery)
        try container.encodeNumericBoolIfPresent(event, forKey: .event)
        try container.encodeNumericBoolIfPresent(packages, forKey: .packages)
        try container.encodeIfPresent(appVersion, forKey: .appVersion)
    }
}