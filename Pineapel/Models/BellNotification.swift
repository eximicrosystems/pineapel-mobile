//
// Created by Andrei Stoicescu on 21/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation

struct UnreadBellNotifications: Decodable {
    let count: Int

    enum CodingKeys: String, CodingKey {
        case count = "unread_notifications"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        count = (try? map.decodeLossless(.count)) ?? 0
    }
}

struct BellNotification: Codable, Identifiable, Hashable {
    enum NotificationType: String, Codable, Hashable {
        case notification = "avs_notification"
        case request = "avs_request"
        case order = "commerce_order"
    }

    var id: String { notificationID }

    let notificationID: String
    let itemType: NotificationType?
    let itemID: String
    var read: Bool
    let data: String?
    let createdAt: Date?

    enum CodingKeys: String, CodingKey {
        case notificationID = "id"
        case itemType = "item_type"
        case itemID = "item_id"
        case read
        case data
        case createdAt = "created"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)

        notificationID = try map.decode(.notificationID)
        itemType = try? map.decodeIfPresent(.itemType)
        itemID = try map.decode(.itemID)
        read = try map.decodeLossless(.read)
        data = try? map.decodeIfPresent(.data)
        createdAt = map.decodeDateIfPresent(.createdAt)
    }

    mutating func readNotification() {
        read = true
    }
}

struct BellNotificationReadRequest: Encodable {
    let type: BellNotification.NotificationType
    let itemID: String

    enum CodingKeys: String, CodingKey {
        case type = "item_type"
        case itemID = "item_id"
    }
}