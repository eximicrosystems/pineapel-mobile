//
// Created by Andrei Stoicescu on 04/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import Alamofire
import TwilioChatClient

struct APIError: Error {
    let message: String?
    var error: Error? = nil

    static func from<T>(_ response: AFDataResponse<T>) -> APIError {
        if let data = response.data {
            if var error = try? JSONDecoder().decode(APIError.self, from: data) {
                error.error = response.error
                return error
            } else if let error = try? JSONDecoder().decode(String.self, from: data) {
                return APIError(message: error, error: nil)
            }
        }
        return APIError(message: nil, error: response.error)
    }

    static func from(_ error: Error?) -> APIError {
        if let error = error as? APIError {
            return error
        }
        return APIError(message: nil, error: error)
    }

    static func from(_ error: TCHError?) -> APIError {
        APIError(message: nil, error: error)
    }
}

extension APIError: Decodable {

    enum CodingKeys: String, CodingKey {
        case message = "message"
        case error // this does not refer to the error property in APIError, but an "error" key that comes up from time to time from the API as a string instead of "message"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        message = [
            try? map.decodeIfPresent(String.self, forKey: .error),
            try? map.decodeIfPresent(String.self, forKey: .message)
        ].compactMap { $0 }.first
    }
}

extension APIError: LocalizedError {
    public var errorDescription: String? {
        if let message = message {
            return message
        }

        if let error = error as? AFError {
            return error.localizedDescription
        }

        return L10n.Errors.unknown
    }
}

extension Error {
    var asAPIError: APIError? {
        self as? APIError
    }
}