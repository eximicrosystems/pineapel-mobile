//
// Created by Andrei Stoicescu on 08/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation

struct UserPassword: Codable {
    let password: String?
    let newPassword: String?
    let confirmNewPassword: String?

    enum CodingKeys: String, CodingKey {
        case password = "existing"
        case newPassword = "value"
        case confirmNewPassword = "confirm"
    }
}