//
// Created by Andrei Stoicescu on 06/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation

struct ValueCodable<T: Codable>: Codable {
    let value: T
}