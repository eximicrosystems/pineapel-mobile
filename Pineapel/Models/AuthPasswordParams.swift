//
// Created by Andrei Stoicescu on 03/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation

struct AuthPasswordParams: Encodable {
    let mail: String?
    let code: String?
    let password: String?
}