//
//  AuthRequest.swift
//  A Votre Service
//
//  Created by Andrei Stoicescu on 02/06/2020.
//  Copyright © 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import SwiftyUserDefaults

struct AuthRequestParams: Encodable {
    let username: String?
    let password: String?
    let clientId: String?
    let clientSecret: String?
    let scope: String?
    let grantType: String?
    let refreshToken: String?

    enum CodingKeys: String, CodingKey {
        case username
        case clientId = "client_id"
        case clientSecret = "client_secret"
        case password
        case scope
        case grantType = "grant_type"
        case refreshToken = "refresh_token"
    }

    static func `default`(user: String, password: String) -> AuthRequestParams {
        let scope: String
        switch Configuration.default.aVotreService.app.environment {
        case .development:
            scope = "resident concierge"
        case .production:
            scope = "resident concierge"
        }

        return AuthRequestParams(
                username: user,
                password: password,
                clientId: Configuration.default.aVotreService.apis.main.clientId,
                clientSecret: Configuration.default.aVotreService.apis.main.clientSecret,
                scope: scope,
                grantType: "password",
                refreshToken: nil
        )
    }

    static func refresh(token: String?) -> AuthRequestParams {
        AuthRequestParams(
                username: nil,
                password: nil,
                clientId: Configuration.default.aVotreService.apis.main.clientId,
                clientSecret: Configuration.default.aVotreService.apis.main.clientSecret,
                scope: nil,
                grantType: "refresh_token",
                refreshToken: token
        )
    }
}
