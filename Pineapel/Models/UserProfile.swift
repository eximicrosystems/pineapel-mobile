//
// Created by Andrei Stoicescu on 06/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation

struct UserProfile {
    var userID: String? = nil
    var userPicture: UserPicture? = nil
    var email: String? = nil
    var password: UserPassword? = nil
    var firstName: String? = nil
    var lastName: String? = nil
    var phoneNumber: String? = nil
    var gender: User.Gender? = nil
    var about: String? = nil
}

extension UserProfile: Codable {
    enum CodingKeys: String, CodingKey {
        case userID = "uid"
        case userPicture = "user_picture"
        case email = "mail"
        case password = "pass"
        case firstName = "field_first_name"
        case lastName = "field_last_name"
        case phoneNumber = "field_phone_number"
        case gender = "field_gender"
        case about = "field_about"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        self.userID = try? map.decodeIfPresent([ValueCodable<String>].self, forKey: .userID)?.first?.value
        self.userPicture = try? map.decodeIfPresent([UserPicture].self, forKey: .userPicture)?.first
        self.email = try? map.decodeIfPresent([ValueCodable<String>].self, forKey: .email)?.first?.value
        self.password = nil
        self.firstName = try? map.decodeIfPresent([ValueCodable<String>].self, forKey: .firstName)?.first?.value
        self.lastName = try? map.decodeIfPresent([ValueCodable<String>].self, forKey: .lastName)?.first?.value
        self.phoneNumber = try? map.decodeIfPresent([ValueCodable<String>].self, forKey: .phoneNumber)?.first?.value
        self.gender = try? map.decodeIfPresent(.gender)
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        if self.userPicture != nil {
            var arrayContainer = container.nestedUnkeyedContainer(forKey: .userPicture)
            try arrayContainer.encode(userPicture)
        }

        try container.encodeValueCodableItemArrayIfPresent(userID, forKey: .userID)
        try container.encodeValueCodableItemArrayIfPresent(email, forKey: .email)
        try container.encodeSingleItemArrayIfPresent(password, forKey: .password)
        try container.encodeValueCodableItemArrayIfPresent(firstName, forKey: .firstName)
        try container.encodeValueCodableItemArrayIfPresent(lastName, forKey: .lastName)
        try container.encodeValueCodableItemArrayIfPresent(phoneNumber, forKey: .phoneNumber)
        try container.encodeValueCodableItemArrayIfPresent(gender, forKey: .gender)
        try container.encodeValueCodableItemArrayIfPresent(about, forKey: .about)
    }
}
