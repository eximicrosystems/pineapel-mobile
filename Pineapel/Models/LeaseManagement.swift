//
// Created by Andrei Stoicescu on 23/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation

struct LeaseOfficeData: Codable, Hashable {
    let buildingID: String
    let title: String?
    let body: String?
    let imageURL: URL?
    let position: String?

    enum CodingKeys: String, CodingKey {
        case buildingID = "nid"
        case title
        case body
        case imageURL = "field_image"
        case position = "field_position"
    }

    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: CodingKeys.self)
        buildingID = try map.decode(.buildingID)
        title = try? map.decodeIfPresent(.title)
        body = try? map.decodeIfPresent(.body)
        imageURL = try? map.decodeIfPresent(.imageURL)
        position = try? map.decodeIfPresent(.position)
    }
}
