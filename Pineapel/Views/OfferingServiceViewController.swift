//
//  OfferingServiceViewController.swift
//  A Votre Service
//
//  Created by Oscar Sevilla on 06/08/21.
//  Copyright © 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit
import Popover
import SVProgressHUD
import Combine
import DifferenceKit

protocol OfferingServiceDelegate: AnyObject {
    func dataSelector(option: Int)
}

class OfferingServiceViewController: UIViewController {
    
    struct ProductModel: Hashable, Differentiable {
        static func ==(lhs: ProductModel, rhs: ProductModel) -> Bool {
            lhs.product.productID == rhs.product.productID && lhs.currentQuantity == rhs.currentQuantity
        }

        let product: ProductProtocol
        let cartProduct: CartProduct?
        let currentQuantity: Int

        func hash(into hasher: inout Hasher) { hasher.combine(product.productID) }

        var differenceIdentifier: Int { hashValue }
    }
    
    @IBOutlet var logo: UIImageView!
    @IBOutlet var type: UILabel!
    @IBOutlet var producto: UILabel!
    @IBOutlet var costo: UILabel!
    @IBOutlet var descripcion: UILabel!
    @IBOutlet var popoverButton: UIButton!
    @IBOutlet var tabbarView: UIView!
    
    weak var delegate: OfferingServiceDelegate?
    
    private var popoverView: Popover!
    private var heightView = 0
    private var selected = 0
    private var intentos = 0
    
    var botones = [String]()
    
    var cartManager: CartProductManager
    var category: ALaCarteCategory
    
    var dataSource: ObservablePaginatedDataSource<CartProductManager.Product>
    var products = [ProductModel]()
    private var subscriptions = Set<AnyCancellable>()
    
    init(cartManager: CartProductManager, category: ALaCarteCategory) {
        self.category = category
        self.cartManager = cartManager
        
        dataSource = .init { per, page in
            ResidentService().getALaCarteItems(categoryID: category.categoryID, page: page, per: per)
        }
        
        dataSource.cacheable = true
        dataSource.cacheKey = "products_category_\(category.categoryID)"
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addComponents()
        setupBindings()
        refreshItems()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func addComponents(){
        let lateralMenu = LateralMenuView(frame: CGRect(x: 80, y: 0, width: 347, height: 844))
        let myView = HeaderTabBarView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 190), tabBarVC: self, lm: lateralMenu)
        lateralMenu.isHidden = true
        lateralMenu.header = myView
        self.view.addSubview(myView)
        self.view.addSubview(lateralMenu)
    }
    
    func setupBindings() {
        cartManager.$products.receive(on: DispatchQueue.main).sink { [weak self] products in
            self?.reloadData()
        }.store(in: &subscriptions)

        cartManager.$totalProductsCount.receive(on: DispatchQueue.main)
                                       .sink { [weak self] count in
                                           self?.updateBadgeLabel()
                                       }.store(in: &subscriptions)

        dataSource.$items
                .receive(on: DispatchQueue.main)
                .sink { [weak self] items in
                    self?.reloadData()
                }.store(in: &subscriptions)

        dataSource.$showLoadingIndicator
                .receive(on: DispatchQueue.main)
                .sink { [weak self] loading in
                    guard self != nil else { return }
                    if loading {
                        SVProgressHUD.show()
                    } else {
                        SVProgressHUD.dismiss()
                    }
                }.store(in: &subscriptions)

        dataSource.$hasNoItemsAfterLoading.receive(on: DispatchQueue.main)
                                          .sink { [weak self] noItems in
                                            guard self != nil else { return }
                                          }.store(in: &subscriptions)
    }
    
    @objc func refreshItems() {
        dataSource.loadMoreContentIfNeeded()
        SVProgressHUD.show()
        while products.count == 0 {
            reloadData()
            intentos+=1
            if intentos > 1000 {
                break
            }
        }
        SVProgressHUD.dismiss()
        if products.count == 0 {
            let alert = UIAlertController(title: "Product not found", message: "We currently don't have this product", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            AppState.switchTo(viewController: ResidentTabBarViewController(page: 3))
        }else{
            showProducts()
        }
    }
    
    func reloadData() {
        let newProducts = dataSource.items.map { product -> ProductModel in
            let cartProduct = cartManager.findCartProduct(product: product)
            return ProductModel(product: product, cartProduct: cartProduct, currentQuantity: cartProduct?.quantity ?? 0)
        }

        let stagedChangeSet = StagedChangeset(source: products, target: newProducts)
        for changeSet in stagedChangeSet {
            products = changeSet.data
        }
    }
    
    func updateBadgeLabel() {
        print(cartManager.totalProductsCountString)
    }
    
    func showProducts(){
        switch category.categoryID {
        case "36":
            logo.image = UIImage(named: "Car Wash")
            type.text = "CAR WASH"
            heightView = 280
        case "41":
            logo.image = UIImage(named: "Dog Walking")
            type.text = "DOG WALKING & CARE"
            heightView = 172
        case "46":
            logo.image = UIImage(named: "Gourmet Cart")
            type.text = "PINEAPEL MARKET"
            producto.text = ""
            costo.text = ""
            descripcion.text = ""
        case "56":
            logo.image = UIImage(named: "Helping Hand")
            type.text = "HELPING HAND"
            heightView = 244
        case "51":
            logo.image = UIImage(named: "Maid Service")
            type.text = "MAID SERVICE"
            heightView = 172
        case "76":
            logo.image = UIImage(named: "Private Chef")
            type.text = "PRIVATE CHEF"
            producto.text = "Private Chef"
        default:
            logo.image = UIImage(named: "Gourmet Cart")
            type.text = "PINEAPEL MARKET"
            print("Hay \(products.count) productos")
            heightView = 35 * products.count
        }
        
        producto.text = products[0].product.name
        costo.text = products[0].product.displayPrice
        descripcion.text = products[0].product.description?.htmlToString
        
        for i in 0...products.count-1 {
            botones.insert(products[safe: i]?.product.name ?? "", at: i)
        }
    }
    
    func dataSelector(option: Int) {
        self.selected = option
        producto.text = products[option].product.name
        costo.text = products[option].product.displayPrice
        descripcion.text = products[option].product.description?.htmlToString
    }
    
    @IBAction func feedButton(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController(page: nil))
    }
    
    @IBAction func chatButton2(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController(page: 1))
    }
    
    @IBAction func reserveButton(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController(page: 2))
    }
    
    @IBAction func offerngButton(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController(page: 3))
    }
    
    @IBAction func optionButton(_ sender: Any){
        print(botones)
        let aView = UIView(frame: CGRect(x: 0, y: 0, width: Int(UIScreen.main.bounds.width)-90, height:280))
        let type = OfferingTableView(frame: CGRect(x: 0, y: 20, width: aView.frame.width, height:aView.frame.height), style: .plain, data: botones)
        type.delegateVC = self
        aView.addSubview(type)
        let popoverView = Popover(options: nil, showHandler: nil, dismissHandler: nil)
        type.pop = popoverView
        popoverView.show(aView, fromView: self.popoverButton)
    }
    
    @IBAction func purchaseButton(_ sender: Any) {
        
        self.cartManager.add(product: products[selected].product)
        
        if cartManager.config.openCartModal {
            let nav = AVSNavigationController(rootViewController: CartViewController(cartManager: cartManager))
            nav.dismissNavigation = {
                self.navigationController?.dismiss(animated: true)
            }
            navigationController?.present(nav, animated: true)
        } else {
            navigationController?.pushViewController(CartViewController(cartManager: cartManager), animated: true)
        }
    }
}
