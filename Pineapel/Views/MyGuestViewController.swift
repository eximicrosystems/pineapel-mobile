//
//  MyGuestViewController.swift
//  A Votre Service
//
//  Created by Oscar Sevilla on 05/08/21.
//  Copyright © 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit

class MyGuestViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func chatButton(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController(page: 1))
    }
    
    @IBAction func backButton(_ sender: Any){
        AppState.switchTo(viewController: ResidentTabBarViewController(page: nil))
    }
    
    @IBAction func feedButton(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController(page: nil))
    }
    
    @IBAction func chatButton2(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController(page: 1))
    }
    
    @IBAction func reserveButton(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController(page: 2))
    }
    
    @IBAction func offerngButton(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController(page: 3))
    }
}
