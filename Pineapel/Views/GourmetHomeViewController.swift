//
//  GourmetHomeViewController.swift
//  A Votre Service
//
//  Created by Oscar Sevilla on 09/08/21.
//  Copyright © 2021 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD
import Combine
import DifferenceKit

class GourmetHomeViewController: UIViewController {
    
    @IBOutlet var drinksButton: UIButton!
    @IBOutlet var sweetButton: UIButton!
    @IBOutlet var savoryButton: UIButton!
    @IBOutlet var jellyButton: UIButton!
    @IBOutlet var alcoholicButton: UIButton!
    
    var items = [ALaCarteCategory]()
    var cartManager: CartProductManager
    private var isMainCategory = true
    private var subscriptions = Set<AnyCancellable>()
    
    init(cartManager: CartProductManager, items: [ALaCarteCategory]? = nil) {
        self.cartManager = cartManager
        self.items = items ?? []
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addComponents()
        getItems()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func addComponents(){
        let lateralMenu = LateralMenuView(frame: CGRect(x: 80, y: 0, width: 347, height: 844))
        let myView = HeaderTabBarView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 190), tabBarVC: self, lm: lateralMenu)
        lateralMenu.isHidden = true
        lateralMenu.header = myView
        self.view.addSubview(myView)
        self.view.addSubview(lateralMenu)
        
        drinksButton.layer.borderColor = #colorLiteral(red: 0.150377363, green: 0.2019616961, blue: 0.1659674644, alpha: 1)
        sweetButton.layer.borderColor = #colorLiteral(red: 0.150377363, green: 0.2019616961, blue: 0.1659674644, alpha: 1)
        savoryButton.layer.borderColor = #colorLiteral(red: 0.150377363, green: 0.2019616961, blue: 0.1659674644, alpha: 1)
        jellyButton.layer.borderColor = #colorLiteral(red: 0.150377363, green: 0.2019616961, blue: 0.1659674644, alpha: 1)
    }
    
    func getItems(){
        cartManager.$totalProductsCount.receive(on: DispatchQueue.main)
                                       .sink { [weak self] count in
                                           self?.updateBadgeLabel()
                                       }.store(in: &subscriptions)
        updateBadgeLabel()
    }
    
    func updateBadgeLabel() {
        print(cartManager.totalProductsCountString)
    }
    
    @IBAction func drinksButtonAction(_ sender: Any) {
        print("drinks button")
        AppState.switchTo(viewController: ResidentTabBarViewController.buildNavControllerType(title: "Services", vc: OfferingServiceViewController(cartManager: cartManager, category: items[0])))
    }
    @IBAction func sweetButtonAction(_ sender: Any) {
        print("Sweet button")
        AppState.switchTo(viewController: ResidentTabBarViewController.buildNavControllerType(title: "Services", vc: OfferingServiceViewController(cartManager: cartManager, category: items[1])))
    }
    @IBAction func savoryButtonAction(_ sender: Any) {
        print("Savory button")
        AppState.switchTo(viewController: ResidentTabBarViewController.buildNavControllerType(title: "Services", vc: OfferingServiceViewController(cartManager: cartManager, category: items[2])))
    }
    @IBAction func jellyButtonAction(_ sender: Any) {
        print("Jelly button")
        AppState.switchTo(viewController: ResidentTabBarViewController.buildNavControllerType(title: "Services", vc: OfferingServiceViewController(cartManager: cartManager, category: items[3])))
    }
    @IBAction func alcoholicButtonAction(_ sender: Any) {
        print("Alcoholic button")
        AppState.switchTo(viewController: ResidentTabBarViewController.buildNavControllerType(title: "Services", vc: OfferingServiceViewController(cartManager: cartManager, category: items[4])))
    }
    
    @IBAction func feedButton(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController(page: nil))
    }
    @IBAction func chatButton(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController(page: 1))
    }
    @IBAction func reserveButton(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController(page: 2))
    }
    @IBAction func offerngButton(_ sender: Any) {
        AppState.switchTo(viewController: ResidentTabBarViewController(page: 3))
    }
}
