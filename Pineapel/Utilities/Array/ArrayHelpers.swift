//
// Created by Andrei Stoicescu on 11/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation

extension Collection {
    /// Returns the element at the specified index if it is within bounds, otherwise nil.
    subscript(safe index: Index) -> Element? {
        indices.contains(index) ? self[index] : nil
    }
}

extension Array {
    func chunked(into size: Int) -> [[Element]] {
        stride(from: 0, to: count, by: size).map {
            Array(self[$0 ..< Swift.min($0 + size, count)])
        }
    }

    func propertySorted<T: Comparable>(_ property: (Element) -> T?, descending: Bool = false) -> [Element] {
        sorted(by: {
            switch (property($0), property($1)) {
            case (.some, .some):
                return descending ? property($0)! > property($1)! : property($0)! < property($1)!

            case (.some, .none):
                return true

            case (.none, _):
                return false
            }
        })
    }

    mutating func propertySort<T: Comparable>(_ property: (Element) -> T?, descending: Bool = false) {
        sort(by: {
            switch (property($0), property($1)) {
            case (.some, .some):
                return descending ? property($0)! > property($1)! : property($0)! < property($1)!

            case (.some, .none):
                return true

            case (.none, _):
                return false
            }
        })
    }

    func split(condition: (Element, Element) -> Bool) -> [[Element]] {
        var returnArray = [[Element]]()

        var currentSubArray = [Element]()

        for (index, element) in enumerated() {
            currentSubArray.append(element)

            if index == count - 1 || condition(element, self[index + 1]) {
                returnArray.append(currentSubArray)
                currentSubArray = []
            }
        }

        return returnArray
    }
}
