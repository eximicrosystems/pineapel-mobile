//
// Created by Andrei Stoicescu on 05/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import UIKit
import SVProgressHUD

struct Appearance {

    static func applyForSessions() {
        let appearance = UINavigationBarAppearance()
        appearance.configureWithTransparentBackground()
        appearance.backgroundColor = .clear
        appearance.shadowImage = UIImage()
        appearance.shadowColor = .clear
        appearance.backgroundImage = UIImage()

        let navigationBarAppearance = UINavigationBar.appearance()
        navigationBarAppearance.standardAppearance = appearance
        navigationBarAppearance.tintColor = AVSColor.tertiaryBackground.color
    }

    static var navigationBarTitleTextAttributes: [NSAttributedString.Key: Any] = {
        [.foregroundColor: AVSColor.accentBackground.color, .font: AVSFont.H4.font]
    }()

    static func apply() {
        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = AVSColor.tertiaryBackground.color
        appearance.titleTextAttributes = navigationBarTitleTextAttributes

        let navigationBarAppearance = UINavigationBar.appearance()
        navigationBarAppearance.standardAppearance = appearance
        navigationBarAppearance.tintColor = AVSColor.accentBackground.color
        navigationBarAppearance.backIndicatorImage = Asset.Images.icBackArrow.image
        navigationBarAppearance.backIndicatorTransitionMaskImage = Asset.Images.icBackArrow.image

        UITabBarItem.appearance().badgeColor = AVSColor.accentBackground.color

        let tabBarAppearance = UITabBar.appearance()
        tabBarAppearance.unselectedItemTintColor = AVSColor.accentForeground.color
        tabBarAppearance.barTintColor = AVSColor.tertiaryBackground.color
        tabBarAppearance.tintColor = AVSColor.accentBackground.color
        tabBarAppearance.isTranslucent = false
    }
}

extension UIEdgeInsets {
    static var collectionDefaultInset: UIEdgeInsets {
        UIEdgeInsets(top: 30, left: 0, bottom: 30, right: 0)
    }

    static var collectionBottomInset: UIEdgeInsets {
        UIEdgeInsets(top: 0, left: 0, bottom: 30, right: 0)
    }

    static var collectionConciergeInset: UIEdgeInsets {
        UIEdgeInsets(top: 10, left: 0, bottom: 30, right: 0)
    }
}
