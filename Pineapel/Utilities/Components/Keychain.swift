//
// Created by Andrei Stoicescu on 03/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import KeychainSwift

fileprivate extension KeychainSwift {
    func set(_ value: String, forKey key: KeychainHelper.Keys, withAccess access: KeychainSwiftAccessOptions? = nil) -> Bool {
        set(value, forKey: key.rawValue)
    }

    func get(_ key: KeychainHelper.Keys) -> String? {
        get(key.rawValue)
    }

    func delete(_ key: KeychainHelper.Keys) -> Bool {
        delete(key.rawValue)
    }
}

class KeychainHelper {

    enum Keys: String {
        case email
        case password
    }

    struct User {
        let email: String
        let password: String

        static func from(email: String?, password: String?) -> User? {
            guard let email = email, let password = password else { return nil }
            return User(email: email, password: password)
        }
    }

    private var keychain = KeychainSwift()

    var user: User? {
        didSet {
            if let user = user {
                _ = keychain.set(user.email, forKey: .email)
                _ = keychain.set(user.password, forKey: .password)
            } else {
                _ = keychain.delete(.email)
                _ = keychain.delete(.password)
            }
        }
    }

    init() {
        self.user = User.from(email: keychain.get(.email), password: keychain.get(.password))
    }

    func clearAll() {
        self.keychain.clear()
    }
}

