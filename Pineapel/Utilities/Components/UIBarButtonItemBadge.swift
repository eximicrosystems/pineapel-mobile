//
// Created by Andrei Stoicescu on 13/07/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class PaddingLabel: UILabel {

    @IBInspectable var topInset: CGFloat = 5.0
    @IBInspectable var bottomInset: CGFloat = 5.0
    @IBInspectable var leftInset: CGFloat = 7.0
    @IBInspectable var rightInset: CGFloat = 7.0

    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
    }

    override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(width: size.width + leftInset + rightInset,
                      height: size.height + topInset + bottomInset)
    }

    override var bounds: CGRect {
        didSet {
            // ensures this works within stack views if multi-line
            preferredMaxLayoutWidth = bounds.width - (leftInset + rightInset)
        }
    }
}

class UIBadgeLabel: UIView {
    var label = PaddingLabel()
    var tappedButton: (() -> Void)?

    init(image: UIImage) {
        super.init(frame: .zero)

        label.textAlignment = .center
        label.layer.masksToBounds = true
        label.textColor = .white
        label.font = AVSFont.note.font
        label.backgroundColor = AVSColor.accentBackground.color
        label.leftInset = 5
        label.rightInset = 4
        label.topInset = 1
        label.bottomInset = 1

        let button = UIButton(type: .custom)
        button.setBackgroundImage(image.withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = AVSColor.accentForeground.color
        button.addTarget(self, action: #selector(tapButton), for: .touchUpInside)

        addSubview(button)
        addSubview(label)

        button.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        label.snp.makeConstraints { make in
            make.right.equalToSuperview().offset(10)
            make.top.equalToSuperview().offset(-10)
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        label.layer.cornerRadius = 8
    }

    @objc func tapButton() {
        tappedButton?()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
