//
// Created by Andrei Stoicescu on 24/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UserNotifications
import FirebaseCore
//import FirebaseInstanceID
import FirebaseMessaging

class AVSPushNotifications: NSObject {
    static var shared = AVSPushNotifications()
    var didSendPushToken: Bool = false

    func didRegisterWithDeviceToken(deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken

        Messaging.messaging().token { token, error in
            if let error = error {
                Log.thisError(error)
            } else if let token = token {
                self.sendPushToken(token)
            }
        }
    }

    func sendPushToken(_ token: String) {
        guard AppState.pushToken != token, !didSendPushToken, AppState.isLoggedIn else { return }
        let apiService = ResidentService()
        AppState.pushToken = token

        let settings = Settings(pushNotificationsToken: token)
        Log.this("SENDING PUSH TOKEN: \(token)")
        apiService.updateSettings(settings).done {
            self.didSendPushToken = true
        }.catch { error in
            Log.thisError(error)
        }
    }

    func registerForPush() {
        Log.this("REGISTERING FOR PUSH NOTIFICATIONS")
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: { _, _ in })
        UIApplication.shared.registerForRemoteNotifications()
        registerCategories()
    }

    func registerCategories() {
        guard AppState.userType == .resident else {
            Log.this("Current user is not a resident. Skipping notification categories")
            return
        }

        UNUserNotificationCenter.current().setNotificationCategories(PushCategoryIdentifier.categories)
    }

    func logout() {
        Messaging.messaging().apnsToken = nil
        didSendPushToken = false
        AppState.pushToken = ""
    }
}

extension AVSPushNotifications: UNUserNotificationCenterDelegate {
    // this is for showing the notification while the app is running
    //    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
    //        Log.this("will present \(notification)")
    //        completionHandler([.alert, .sound, .badge])
    //    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        guard let jsonData = try? JSONSerialization.data(withJSONObject: userInfo, options: []),
              var message = try? JSONDecoder().decode(PushMessage.self, from: jsonData) else {
            Log.this("cannot decode user notification \(userInfo)")
            completionHandler()
            return
        }

        Log.this(userInfo)

        if let actionIdentifier = PushActionIdentifier(rawValue: response.actionIdentifier) {
            message.actionIdentifier = actionIdentifier
        }

        if let window = UIApplication.shared.windows.first(where: { $0.isKeyWindow }),
           let vc = window.rootViewController as? PushNotificationActionable {
            vc.processActionable(message)
        } else {
            AppState.pushMessage = message
        }

        completionHandler()
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, openSettingsFor notification: UNNotification?) {
        Log.this("open settings for \(notification)")
    }
}

extension AVSPushNotifications: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        if let token = fcmToken {
            Log.this("Firebase registration token: \(token)")
            sendPushToken(token)
        }
    }
}
