//
// Created by Andrei Stoicescu on 09/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit

struct HTMLTemplateAttributedString {
    static func from(string: String?) -> NSAttributedString? {
        guard let string = string else {
            return NSAttributedString.string(text: "")
        }

        let font = AVSFont.body.font
        let color = AVSColor.primaryLabel.color
        let linkColor = AVSColor.secondaryLabel.color

        let template = """
                       <html>
                         <head>
                           <style type="text/css">
                             body {
                               font-size: \(font.pointSize);
                               font-family: '-apple-system';
                               color: \(color.asHexString);
                             }
                             a {
                               color: \(linkColor.asHexString);
                             }
                           </style>
                         </head>
                         <body>
                           \(string)
                         </body>
                       </html> 
                       """
        return NSAttributedString(html: template)
    }
}

extension NSAttributedString {

    convenience init?(string: String?, attributes: [Key: Any]?) {
        guard let string = string else { return nil }
        self.init(string: string, attributes: attributes)
    }

    static func string(text: String?, font: Asset.Fonts, color: UIColor = .darkGray, alignment: NSTextAlignment = .left, otherAttributes: [Key: Any]? = nil) -> NSAttributedString? {
        string(text: text, font: font.uiFont, color: color, alignment: alignment, otherAttributes: otherAttributes)
    }

    static func string(text: String?, conciergeFont: Asset.ConciergeFonts, color: UIColor = .darkGray, alignment: NSTextAlignment = .left, otherAttributes: [Key: Any]? = nil) -> NSAttributedString? {
        string(text: text, font: conciergeFont.font, color: color, alignment: alignment, otherAttributes: otherAttributes)
    }

    static func string(text: String?, font: UIFont = Asset.Fonts.NormalText.uiFont, color: UIColor = .darkGray, alignment: NSTextAlignment = .left, otherAttributes: [Key: Any]? = nil) -> NSAttributedString? {
        guard let text = text else { return nil }
        return NSAttributedString(string: text, attributes: attributes(font: font, color: color, alignment: alignment, otherAttributes: otherAttributes))
    }

    static func attributes(font: Asset.Fonts, color: UIColor, alignment: NSTextAlignment, otherAttributes: [Key: Any]? = nil) -> [Key: Any] {
        attributes(font: font.uiFont, color: color, alignment: alignment, otherAttributes: otherAttributes)
    }

    static func attributes(font: UIFont, color: UIColor, alignment: NSTextAlignment, otherAttributes: [Key: Any]? = nil) -> [Key: Any] {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = alignment

        var attrs: [Key: Any] = [
            .font: font,
            .foregroundColor: color,
            .backgroundColor: UIColor.clear,
            .paragraphStyle: paragraphStyle
        ]

        if let otherAttributes = otherAttributes {
            attrs.merge(otherAttributes) { a, b in a }
        }

        return attrs
    }

    static func avsAttributes(font: AVSFont, color: AVSColor, alignment: NSTextAlignment = .left, otherAttributes: [Key: Any]? = nil) -> [Key: Any] {
        attributes(font: font.font, color: color.color, alignment: alignment, otherAttributes: otherAttributes)
    }

    func appendingString(_ string: String?, attributes: [Key: Any]) -> NSAttributedString {
        guard let string = string else { return self }
        return appendingAttributedString(NSAttributedString(string: string, attributes: attributes))
    }

    func appendingAttributedString(_ attributedString: NSAttributedString?) -> NSAttributedString {
        guard let attributedString = attributedString else { return self }
        let str = NSMutableAttributedString(attributedString: self)
        str.append(attributedString)
        return str
    }

    internal convenience init?(html: String) {
        guard let data = html.data(using: String.Encoding.utf16, allowLossyConversion: false) else {
            return nil
        }

        guard let attributedString = try? NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil) else {
            return nil
        }

        self.init(attributedString: attributedString)
    }
}

extension Sequence where Iterator.Element == NSAttributedString {
    func joined(with separator: NSAttributedString) -> NSAttributedString {
        self.reduce(NSMutableAttributedString()) {
            (r, e) in
            if r.length > 0 {
                r.append(separator)
            }
            r.append(e)
            return r
        }
    }

    func joined(with separator: String = "") -> NSAttributedString {
        self.joined(with: NSAttributedString(string: separator))
    }
}
