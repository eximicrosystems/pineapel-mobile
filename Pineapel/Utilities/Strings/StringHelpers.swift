//
// Created by Andrei Stoicescu on 24/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
extension String {
    static func isBlank(_ string: String?) -> Bool {
        if let string = string {
            return string.isEmpty
        }
        return true
    }
}