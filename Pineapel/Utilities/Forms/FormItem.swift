//
// Created by Andrei Stoicescu on 08/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import Validator

extension AVSForm {
    class FormItem: AVSFormValidatable, Equatable {
        static func ==(lhs: AVSForm.FormItem, rhs: AVSForm.FormItem) -> Bool {
            lhs.formItemID == rhs.formItemID
        }

        struct SelectOption: Equatable {
            struct SelectOptionMetadata {
                var url: URL?
            }

            static func ==(lhs: AVSForm.FormItem.SelectOption, rhs: AVSForm.FormItem.SelectOption) -> Bool {
                lhs.value == rhs.value
            }

            var value: String
            var label: String
            var metadata: SelectOptionMetadata? = nil
        }

        let formItemID = UUID().uuidString

        // TODO make value reactive
        var value: String? {
            didSet {
                self.prevValue = oldValue
            }
        }
        var prevValue: String?
        var note: String?
        var title = ""
        var placeholder = ""
        var indexPath: IndexPath?
        var valueCompletion: ((String?) -> Void)?
        var selectOptions: [SelectOption] = []

        var isMandatory = true
        var validation: ((String?) -> ValidationResult?)?
        var isValid = true {
            didSet {
                if isValid {
                    self.errors = nil
                }
            }
        }

        var uiProperties = FormItemUIProperties()
        var errors: [ValidationError]?

        // MARK: Init
        init(placeholder: String, value: String? = nil, note: String? = nil) {
            self.placeholder = placeholder
            self.value = value
            self.note = note
        }

        // MARK: FormValidable
        func checkValidity() {

            if self.isMandatory {
                self.isValid = self.value != nil && self.value?.isEmpty == false
                if !self.isValid {
                    self.errors = [AVSFormSingleValidationError(message: L10n.Errors.mustBePresent(self.placeholder))]
                    return
                }
            }

            if let result = self.validation?(self.value) {
                switch result {
                case .valid:
                    self.isValid = true
                case .invalid(let errors):
                    self.isValid = false
                    self.errors = errors
                }
            } else {
                self.isValid = true
            }
        }
    }
}