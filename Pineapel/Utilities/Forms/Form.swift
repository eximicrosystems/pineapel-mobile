//
// Created by Andrei Stoicescu on 08/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import Validator

struct AVSFormValidationError: Error {
    var errors: [ValidationError]
}

struct AVSFormSingleValidationError: ValidationError {
    var message: String
}

class AVSForm {
    var formItems = [FormItem]()

    var errors: [ValidationError] {
        get {
            formItems.compactMap {
                $0.errors
            }.flatMap {
                $0
            }
        }
    }

    // MARK: Form Validation
    @discardableResult
    func isValid() -> Result<Bool, AVSFormValidationError> {
        for item in self.formItems {
            item.checkValidity()
        }

        let errors = self.errors
        if errors.count == 0 {
            return .success(true)
        } else {
            return .failure(AVSFormValidationError(errors: errors))
        }
    }
}