//
// Created by Andrei Stoicescu on 08/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import UIKit
import UITextView_Placeholder

extension AVSForm {
    struct FormCells {
        class InfoLabelCell: UITableViewCell {
            lazy var label = UILabel()

            override init(style: CellStyle, reuseIdentifier: String?) {
                super.init(style: style, reuseIdentifier: reuseIdentifier)

                label.font = AVSFont.bodyTitleHeavy.font
                label.textColor = AVSColor.primaryLabel.color

                addSubview(label)

                label.snp.makeConstraints { make in
                    make.top.equalToSuperview().offset(0)
                    make.bottom.equalToSuperview().offset(-15)
                    make.left.equalToSuperview().offset(20)
                    make.right.equalToSuperview().offset(-20)
                }

                backgroundColor = .clear
                selectionStyle = .none
            }

            var title: String = "" {
                didSet {
                    label.text = title
                }
            }

            required init?(coder: NSCoder) {
                fatalError("init(coder:) has not been implemented")
            }
        }

        class SaveCell: UITableViewCell {

            lazy var saveButton = UIButton.secondaryButton(title: title)

            var title: String = "" {
                didSet {
                    saveButton.setTitle(title, for: .normal)
                }
            }

            override init(style: CellStyle, reuseIdentifier: String?) {
                super.init(style: style, reuseIdentifier: reuseIdentifier)
                contentView.addSubview(saveButton)
                backgroundColor = .clear
                selectionStyle = .none
                saveButton.isUserInteractionEnabled = false
                saveButton.snp.makeConstraints { make in
                    make.top.equalToSuperview().offset(40)
                    make.bottom.equalToSuperview().offset(-20)
                    make.left.equalToSuperview().offset(20)
                    make.right.equalToSuperview().offset(-20)
                    make.height.equalTo(50)
                }
            }

            required init?(coder: NSCoder) {
                fatalError("init(coder:) has not been implemented")
            }
        }

        class TextFieldCell: UITableViewCell, AVSFormConformity, AVSFormUpdatable, UIPickerViewDelegate, UIPickerViewDataSource {

            lazy var pickerButtonActivator: UIButton = {
                let button = UIButton()
                button.addTarget(self, action: #selector(tappedButtonActivator), for: .touchUpInside)
                return button
            }()

            var formItemValueUpdate: ((FormItem) -> Void)?

            var formItem: FormItem?
            var pickerOptions: [String] = []
            var pickerView: UIPickerView?

            lazy var fieldView = AVSTextFieldContainer(placeholder: "")

            override init(style: CellStyle, reuseIdentifier: String?) {
                super.init(style: style, reuseIdentifier: reuseIdentifier)
                contentView.addSubview(fieldView)
                contentView.addSubview(pickerButtonActivator)
                backgroundColor = .clear
                selectionStyle = .none
                fieldView.snp.makeConstraints { make in
                    make.top.equalToSuperview().offset(10)
                    make.bottom.equalToSuperview().offset(-10)
                    make.left.equalToSuperview().offset(20)
                    make.right.equalToSuperview().offset(-20)
                }
                pickerButtonActivator.snp.makeConstraints { make in
                    make.edges.equalToSuperview()
                }

                fieldView.textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            }

            @objc func textFieldDidChange(_ textField: UITextField) {
                formItem?.valueCompletion?(textField.text)
                if let item = formItem {
                    formItemValueUpdate?(item)
                }
            }

            func update(with formItem: AVSForm.FormItem) {
                self.formItem = formItem

                // TODO Add validation UI rules here using self.formItem?.isValid

                fieldView.text = self.formItem?.value
                fieldView.placeholder = self.formItem?.placeholder
                fieldView.noteText = self.formItem?.note
                fieldView.textField.keyboardType = self.formItem?.uiProperties.keyboardType ?? .default
                fieldView.textField.isSecureTextEntry = self.formItem?.uiProperties.secureText ?? false

                if let readonly = self.formItem?.uiProperties.readonly {
                    fieldView.textField.isUserInteractionEnabled = !readonly
                } else {
                    fieldView.textField.isUserInteractionEnabled = true
                }

                if formItem.uiProperties.cellType == .picker {
                    pickerButtonActivator.isUserInteractionEnabled = true
                    let pickerView = UIPickerView()
                    fieldView.textField.tintColor = .clear
                    fieldView.textField.inputView = pickerView
                    pickerOptions = formItem.uiProperties.picker.options

                    pickerView.delegate = self
                    pickerView.dataSource = self
                    self.pickerView = pickerView
                } else {
                    self.pickerButtonActivator.isUserInteractionEnabled = false
                }
            }

            @objc func tappedButtonActivator() {
                self.fieldView.textField.becomeFirstResponder()
                // Opened Picker

                if String.isBlank(self.formItem?.value) {
                    // If there's no preselected value, pick the first item if available
                    if let option = self.pickerOptions.first {
                        self.selectPickerOption(option: option)
                    }
                } else {
                    // There is a preselected value in the form input. Make sure it's selected in the picker too
                    let pickerOptionIndex = self.pickerOptions.firstIndex { $0 == self.formItem?.value }
                    if let pickerOptionIndex = pickerOptionIndex {
                        self.pickerView?.selectRow(pickerOptionIndex, inComponent: 0, animated: false)
                    }
                }
            }

            required init?(coder: NSCoder) {
                fatalError("init(coder:) has not been implemented")
            }

            func numberOfComponents(in pickerView: UIPickerView) -> Int {
                1
            }

            func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
                pickerOptions.count
            }

            func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
                pickerOptions[row]
            }

            func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
                self.selectPickerOption(option: pickerOptions[row])
            }

            func selectPickerOption(option: String) {
                guard let formItem = self.formItem else {
                    return
                }

                self.fieldView.text = option
                formItem.valueCompletion?(option)
                self.formItemValueUpdate?(formItem)
            }
        }

        class SingleSelectCell<T: SingleSelectViewProtocol>: UITableViewCell, AVSFormConformity, AVSFormUpdatable {
            var formItem: FormItem?

            var formItemValueUpdate: ((FormItem) -> Void)?

            var options = [FormItem.SelectOption]()
            lazy var fieldView = T()

            lazy var placeholderLabel: UILabel = {
                let label = UILabel()
                label.font = AVSFont.bodyTitleHeavy.font
                label.textColor = AVSColor.primaryLabel.color
                label.translatesAutoresizingMaskIntoConstraints = true
                return label
            }()

            override init(style: CellStyle, reuseIdentifier: String?) {
                super.init(style: style, reuseIdentifier: reuseIdentifier)
                backgroundColor = .clear
                selectionStyle = .none
                contentView.addSubview(fieldView)
                contentView.addSubview(placeholderLabel)

                placeholderLabel.snp.makeConstraints { make in
                    make.top.equalToSuperview().offset(10)
                    make.left.equalToSuperview().offset(20)
                    make.right.equalToSuperview().offset(-20)
                }

                fieldView.snp.makeConstraints { make in
                    make.top.equalTo(placeholderLabel.snp.bottom).offset(20)
                    make.bottom.equalToSuperview().offset(-10)
                    make.left.equalToSuperview().offset(20)
                    make.right.equalToSuperview().offset(-20)
                }
            }

            func update(with formItem: AVSForm.FormItem) {
                self.formItem = formItem

                // TODO Add validation UI rules here using self.formItem?.isValid

                let option = formItem.selectOptions.first {
                    $0.value == self.formItem?.value
                }

                fieldView.configure(with: formItem.selectOptions, selectedOption: option)
                fieldView.onSelectedOption = { [weak self] option in
                    guard let self = self else { return }
                    self.formItem?.valueCompletion?(option.value)
                    if let item = self.formItem {
                        self.formItemValueUpdate?(item)
                    }
                }
                placeholderLabel.text = formItem.placeholder

                if let readonly = self.formItem?.uiProperties.readonly {
                    fieldView.isUserInteractionEnabled = !readonly
                } else {
                    fieldView.isUserInteractionEnabled = true
                }
            }

            required init?(coder: NSCoder) {
                fatalError("init(coder:) has not been implemented")
            }
        }

        class TextAreaCell: UITableViewCell, AVSFormConformity, AVSFormUpdatable, UITextViewDelegate {
            var formItemValueUpdate: ((FormItem) -> Void)?

            lazy var titleLabel: UILabel = {
                let label = UILabel()
                label.font = AVSFont.bodyTitleHeavy.font
                label.textColor = AVSColor.primaryLabel.color
                label.translatesAutoresizingMaskIntoConstraints = true
                return label
            }()

            var formItem: FormItem?

            lazy var fieldView = UITextView()

            override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
                super.traitCollectionDidChange(previousTraitCollection)
                if traitCollection.userInterfaceStyle != previousTraitCollection?.userInterfaceStyle {
                    fieldView.layer.borderColor = AVSColor.inputBorder.color.cgColor
                }
            }

            override init(style: CellStyle, reuseIdentifier: String?) {
                super.init(style: style, reuseIdentifier: reuseIdentifier)
                contentView.addSubview(fieldView)
                contentView.addSubview(titleLabel)
                backgroundColor = .clear
                selectionStyle = .none
                fieldView.textContainerInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
                fieldView.backgroundColor = AVSColor.inputFill.color
                fieldView.font = AVSFont.bodyRoman.font
                fieldView.textColor = AVSColor.primaryLabel.color
                fieldView.layer.borderWidth = 1
                fieldView.layer.borderColor = AVSColor.inputBorder.color.cgColor
                fieldView.delegate = self

                titleLabel.snp.makeConstraints { make in
                    make.top.equalToSuperview().offset(10)
                    make.left.equalToSuperview().offset(20)
                    make.right.equalToSuperview().offset(-20)
                }

                fieldView.snp.makeConstraints { make in
                    make.top.equalTo(titleLabel.snp.bottom).offset(20)
                    make.bottom.equalToSuperview().offset(-10)
                    make.left.equalToSuperview().offset(20)
                    make.right.equalToSuperview().offset(-20)
                    make.height.greaterThanOrEqualTo(150)
                }
            }

            func textViewDidChange(_ textView: UITextView) {
                formItem?.valueCompletion?(textView.text)
                if let item = formItem {
                    formItemValueUpdate?(item)
                }
            }

            func update(with formItem: AVSForm.FormItem) {
                self.formItem = formItem

                // TODO Add validation UI rules here using self.formItem?.isValid

                fieldView.text = self.formItem?.value
                titleLabel.text = self.formItem?.title
                fieldView.placeholder = self.formItem?.placeholder ?? ""
                fieldView.placeholderColor = AVSColor.secondaryLabel.color

                fieldView.keyboardType = self.formItem?.uiProperties.keyboardType ?? .default

                if let readonly = self.formItem?.uiProperties.readonly {
                    fieldView.isUserInteractionEnabled = !readonly
                } else {
                    fieldView.isUserInteractionEnabled = true
                }

            }

            required init?(coder: NSCoder) {
                fatalError("init(coder:) has not been implemented")
            }
        }

        class RatingCell: UITableViewCell, AVSFormConformity, AVSFormUpdatable {
            var formItemValueUpdate: ((FormItem) -> Void)?

            var starButtons = [UIButton]()
            var maxRating = 5
            private var _rating: Int = 0
            var rating: Int {
                set(newRating) {
                    _rating = min(newRating, maxRating)

                    starButtons.forEach { button in
                        button.setImage(Asset.Images.icStarUnselected.image.withRenderingMode(.alwaysTemplate), for: .normal)
                        button.tintColor = AVSColor.primaryIcon.color
                    }

                    if _rating > 0 {
                        (0 ... _rating - 1).forEach { i in
                            self.starButtons[i].setImage(Asset.Images.icStarSelected.image.withRenderingMode(.alwaysTemplate), for: .normal)
                            self.starButtons[i].tintColor = AVSColor.colorYellowRating.color
                        }

                        formItem?.valueCompletion?(String(_rating))
                        if let item = formItem {
                            formItemValueUpdate?(item)
                        }
                    }
                }
                get {
                    _rating
                }
            }

            lazy var titleLabel: UILabel = {
                let label = UILabel()
                label.font = AVSFont.bodyTitleHeavy.font
                label.textColor = AVSColor.primaryLabel.color
                label.translatesAutoresizingMaskIntoConstraints = true
                return label
            }()

            var formItem: FormItem?

            override init(style: CellStyle, reuseIdentifier: String?) {
                super.init(style: style, reuseIdentifier: reuseIdentifier)

                starButtons = (1 ... 5).map { i -> UIButton in
                    let button = UIButton(type: .custom)
                    button.setImage(Asset.Images.icStarUnselected.image, for: .normal)
                    return button
                }

                let stackView = UIStackView()
                stackView.translatesAutoresizingMaskIntoConstraints = false
                stackView.axis = .horizontal
                stackView.alignment = .fill
                stackView.distribution = .fillProportionally
                stackView.spacing = 10

                stackView.addArrangedSubviews(subviews: starButtons)

                for button in starButtons {
                    button.snp.makeConstraints { make in
                        make.width.height.equalTo(25)
                    }
                    button.addTarget(self, action: #selector(tapStarButton), for: .touchUpInside)
                }

                contentView.addSubview(stackView)
                contentView.addSubview(titleLabel)

                backgroundColor = .clear
                selectionStyle = .none

                titleLabel.snp.makeConstraints { make in
                    make.top.equalToSuperview().offset(10)
                    make.left.equalToSuperview().offset(20)
                    make.right.equalToSuperview().offset(-20)
                }

                stackView.snp.makeConstraints { make in
                    make.top.equalTo(titleLabel.snp.bottom).offset(20)
                    make.bottom.equalToSuperview().offset(-10)
                    make.left.equalToSuperview().offset(20)
                }
            }

            @objc func tapStarButton(sender: UIButton) {
                if let idx = starButtons.firstIndex(of: sender) {
                    rating = idx + 1
                }
            }

            func update(with formItem: AVSForm.FormItem) {
                self.formItem = formItem
                titleLabel.text = self.formItem?.placeholder

                if let value = formItem.value {
                    if let intValue = Int(value) {
                        if intValue <= 5 {
                            rating = intValue
                        }
                    }
                }

                let readonly = self.formItem?.uiProperties.readonly ?? false
                starButtons.forEach { button in
                    button.isUserInteractionEnabled = !readonly
                }
            }

            required init?(coder: NSCoder) {
                fatalError("init(coder:) has not been implemented")
            }
        }
    }
}
