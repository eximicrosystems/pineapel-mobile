//
// Created by Andrei Stoicescu on 17/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit

class SingleSelectAvatarViewItem: UIControl {
    lazy var selectOptionView: UIImageView = {
        let image = UIImage(asset: Asset.Images.icUnselected)
        let imageView = UIImageView(image: image)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    lazy var selectLabel: UILabel = {
        let label = UILabel()
        label.font = AVSFont.body.font
        label.textColor = AVSColor.primaryLabel.color
        label.translatesAutoresizingMaskIntoConstraints = true
        return label
    }()

    lazy var avatarView = AvatarShadowView()

    var option: AVSForm.FormItem.SelectOption
    var optionSelected: Bool {
        didSet {
            let asset = optionSelected ? Asset.Images.icSelected : Asset.Images.icUnselected
            self.selectOptionView.image = asset.image
        }
    }

    init(option: AVSForm.FormItem.SelectOption, selected: Bool = false) {
        self.option = option
        self.optionSelected = selected
        super.init(frame: .zero)

        selectLabel.isUserInteractionEnabled = false
        avatarView.isUserInteractionEnabled = false
        selectOptionView.isUserInteractionEnabled = false

        addSubview(selectOptionView)
        addSubview(selectLabel)
        addSubview(avatarView)

        avatarView.imageView.pin_setImage(from: option.metadata?.url)

        selectLabel.text = option.label

        selectOptionView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview()
            make.width.height.equalTo(15)
        }

        avatarView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
            make.left.equalTo(selectOptionView.snp.right).offset(15)
            make.width.height.equalTo(50)
        }

        selectLabel.snp.makeConstraints { make in
            make.left.equalTo(avatarView.snp.right).offset(15)
            make.centerY.equalToSuperview()
            make.right.equalToSuperview()
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class SingleSelectAvatarView: UIView, SingleSelectViewProtocol {
    var onSelectedOption: ((AVSForm.FormItem.SelectOption) -> Void)?

    var readonly: Bool = false {
        didSet {
            self.isUserInteractionEnabled = !readonly
        }
    }

    private var options: [AVSForm.FormItem.SelectOption] = [] {
        didSet {
            self.recreateOptionViews()
        }
    }

    private var selectedOption: AVSForm.FormItem.SelectOption? {
        didSet {
            self.optionViews.forEach { viewArray in
                viewArray.forEach { view in
                    view.optionSelected = view.option == selectedOption
                }
            }
        }
    }

    private var optionViews: [[SingleSelectAvatarViewItem]] = []

    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.alignment = .fill
        stackView.spacing = 20
        return stackView
    }()

    private func recreateOptionViews() {
        self.optionViews.forEach { viewArray in
            viewArray.forEach { view in
                view.removeFromSuperview()
            }
        }

        stackView.removeArrangedSubviews()

        self.optionViews = self.options.map { option in
            SingleSelectAvatarViewItem(option: option)
        }.chunked(into: 2)

        self.optionViews.forEach { viewArray in
            viewArray.forEach { view in
                view.addTarget(self, action: #selector(didSelectOption), for: .touchUpInside)
            }

            let stackView = UIStackView()
            stackView.translatesAutoresizingMaskIntoConstraints = false
            stackView.axis = .horizontal
            stackView.distribution = .fillEqually
            stackView.alignment = .fill
            stackView.spacing = 10

            stackView.addArrangedSubviews(subviews: viewArray)

            self.stackView.addArrangedSubview(stackView)
        }

    }

    @objc func didSelectOption(_ sender: SingleSelectAvatarViewItem) {
        self.selectedOption = sender.option
        self.onSelectedOption?(sender.option)
    }

    func configure(with options: [AVSForm.FormItem.SelectOption], selectedOption: AVSForm.FormItem.SelectOption? = nil) {
        self.options = options
        self.selectedOption = selectedOption
    }

    init() {
        super.init(frame: .zero)
        addSubview(stackView)
        stackView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}