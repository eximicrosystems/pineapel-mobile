//
// Created by Andrei Stoicescu on 08/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import UIKit

extension AVSForm {
    enum FormItemCellType {
        case textField
        case saveButton
        case singleSelect
        case infoLabel
        case textArea
        case rating
        case singleAvatarSelect
        case picker

        static func registerCells(for tableView: UITableView) {
            tableView.register(cellClass: FormCells.TextFieldCell.self)
            tableView.register(cellClass: FormCells.SaveCell.self)
            tableView.register(cellClass: FormCells.SingleSelectCell<SingleSelectView>.self)
            tableView.register(cellClass: FormCells.InfoLabelCell.self)
            tableView.register(cellClass: FormCells.TextAreaCell.self)
            tableView.register(cellClass: FormCells.RatingCell.self)
            tableView.register(cellClass: FormCells.SingleSelectCell<SingleSelectAvatarView>.self)
        }

        func dequeueCell(for tableView: UITableView, at indexPath: IndexPath) -> UITableViewCell {
            let cell: UITableViewCell

            switch self {
            case .textField, .picker:
                cell = tableView.dequeueReusableCell(withClass: FormCells.TextFieldCell.self, forIndexPath: indexPath)
            case .saveButton:
                cell = tableView.dequeueReusableCell(withClass: FormCells.SaveCell.self, forIndexPath: indexPath)
            case .singleSelect:
                cell = tableView.dequeueReusableCell(withClass: FormCells.SingleSelectCell<SingleSelectView>.self, forIndexPath: indexPath)
            case .infoLabel:
                cell = tableView.dequeueReusableCell(withClass: FormCells.InfoLabelCell.self, forIndexPath: indexPath)
            case .textArea:
                cell = tableView.dequeueReusableCell(withClass: FormCells.TextAreaCell.self, forIndexPath: indexPath)
            case .rating:
                cell = tableView.dequeueReusableCell(withClass: FormCells.RatingCell.self, forIndexPath: indexPath)
            case .singleAvatarSelect:
                cell = tableView.dequeueReusableCell(withClass: FormCells.SingleSelectCell<SingleSelectAvatarView>.self, forIndexPath: indexPath)
            }

            return cell
        }
    }
}