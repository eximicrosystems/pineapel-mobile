//
// Created by Andrei Stoicescu on 15/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit

protocol SingleSelectViewProtocol: UIView {
    var onSelectedOption: ((AVSForm.FormItem.SelectOption) -> Void)? { get set }
    func configure(with options: [AVSForm.FormItem.SelectOption], selectedOption: AVSForm.FormItem.SelectOption?)
    var readonly: Bool { get set }
}

class SingleSelectViewItem: UIControl {
    lazy var selectOptionView: UIImageView = {
        let image = UIImage(asset: Asset.Images.icUnselected)
        let imageView = UIImageView(image: image)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    lazy var selectLabel: UILabel = {
        let label = UILabel()
        label.font = AVSFont.body.font
        label.textColor = AVSColor.primaryLabel.color
        label.translatesAutoresizingMaskIntoConstraints = true
        return label
    }()

    var option: AVSForm.FormItem.SelectOption
    var optionSelected: Bool {
        didSet {
            let asset = optionSelected ? Asset.Images.icSelected : Asset.Images.icUnselected
            selectOptionView.image = asset.image
        }
    }

    init(option: AVSForm.FormItem.SelectOption, selected: Bool = false) {
        self.option = option
        optionSelected = selected
        super.init(frame: .zero)
        addSubview(selectOptionView)
        addSubview(selectLabel)

        selectLabel.text = option.label

        selectOptionView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.left.equalToSuperview()
            make.bottom.equalToSuperview()
            make.width.height.equalTo(15)
        }

        selectLabel.snp.makeConstraints { make in
            make.left.equalTo(selectOptionView.snp.right).offset(15)
            make.centerY.equalToSuperview()
            make.right.equalToSuperview()
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class SingleSelectView: UIView, SingleSelectViewProtocol {

    var onSelectedOption: ((AVSForm.FormItem.SelectOption) -> Void)?

    var readonly: Bool = false {
        didSet {
            isUserInteractionEnabled = !readonly
        }
    }

    private var options: [AVSForm.FormItem.SelectOption] = [] {
        didSet {
            recreateOptionViews()
        }
    }

    private var selectedOption: AVSForm.FormItem.SelectOption? {
        didSet {
            optionViews.forEach { view in
                view.optionSelected = view.option == selectedOption
            }
        }
    }
    private var optionViews: [SingleSelectViewItem] = []

    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.alignment = .fill
        stackView.spacing = 20
        return stackView
    }()

    private func recreateOptionViews() {
        stackView.removeArrangedSubviews()

        optionViews = options.map { .init(option: $0) }

        optionViews.forEach { view in
            view.addTarget(self, action: #selector(didSelectOption), for: .touchUpInside)
        }

        stackView.addArrangedSubviews(subviews: optionViews)
    }

    @objc func didSelectOption(_ sender: SingleSelectViewItem) {
        selectedOption = sender.option
        onSelectedOption?(sender.option)
    }

    func configure(with options: [AVSForm.FormItem.SelectOption], selectedOption: AVSForm.FormItem.SelectOption? = nil) {
        self.options = options
        self.selectedOption = selectedOption
    }

    init() {
        super.init(frame: .zero)
        addSubview(stackView)
        stackView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}