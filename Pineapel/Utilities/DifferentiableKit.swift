//
// Created by Andrei Stoicescu on 16/10/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import DifferenceKit

extension String: Differentiable {}
extension Int: Differentiable {}
