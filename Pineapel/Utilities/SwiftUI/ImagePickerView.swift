//
//  ImagePicker.swift
//  A Votre Service
//
//  Created by Andrei Stoicescu on 02/09/2020.
//  Copyright © 2020 A Votre Service LLC. All rights reserved.
//

import SwiftUI
import OpalImagePicker
import Photos

struct ImagePickerView: UIViewControllerRepresentable {

    typealias ImageSelected = (UIImage?) -> Void

    @Binding var image: UIImage?
    @Binding var isShown: Bool
    var sourceType: UIImagePickerController.SourceType = .photoLibrary
    var imageSelected: ImageSelected

    func makeCoordinator() -> Coordinator {
        Coordinator(isShown: $isShown, image: $image, imageSelected: imageSelected)
    }

    func makeUIViewController(context: UIViewControllerRepresentableContext<ImagePickerView>) -> UIImagePickerController {
        let picker = UIImagePickerController()
        picker.delegate = context.coordinator
        picker.mediaTypes = ["public.image"]
        picker.sourceType = sourceType
        return picker
    }

    func updateUIViewController(_ uiViewController: UIImagePickerController,
                                context: UIViewControllerRepresentableContext<ImagePickerView>) {

    }

    class Coordinator: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

        @Binding var image: UIImage?
        @Binding var isShown: Bool
        var imageSelected: (UIImage?) -> Void

        init(isShown: Binding<Bool>, image: Binding<UIImage?>, imageSelected: @escaping ImageSelected) {
            _isShown = isShown
            _image = image
            self.imageSelected = imageSelected
        }

        func imagePickerController(_ picker: UIImagePickerController,
                                   didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            let img = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            image = img
            imageSelected(img)
            isShown = false
        }

        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            isShown = false
        }
    }
}

struct MultiImagePickerView: UIViewControllerRepresentable {

    typealias AssetsSelected = ([PHAsset]) -> Void

    @Binding var isShown: Bool
    var maximumSelectionsAllowed: Int = -1
    var allowedMediaTypes: Set<PHAssetMediaType> = [.image, .video]
    var assetsSelected: AssetsSelected

    func makeCoordinator() -> Coordinator {
        Coordinator(isShown: $isShown, assetsSelected: assetsSelected)
    }

    func makeUIViewController(context: UIViewControllerRepresentableContext<MultiImagePickerView>) -> OpalImagePickerController {
        let picker = OpalImagePickerController()
        picker.imagePickerDelegate = context.coordinator
        picker.allowedMediaTypes = allowedMediaTypes
        picker.maximumSelectionsAllowed = maximumSelectionsAllowed
        return picker
    }

    func updateUIViewController(_ uiViewController: OpalImagePickerController,
                                context: UIViewControllerRepresentableContext<MultiImagePickerView>) {

    }

    class Coordinator: NSObject, UINavigationControllerDelegate, OpalImagePickerControllerDelegate {

        @Binding var isShown: Bool
        var assetsSelected: AssetsSelected

        init(isShown: Binding<Bool>, assetsSelected: @escaping AssetsSelected) {
            _isShown = isShown
            self.assetsSelected = assetsSelected
        }

        func imagePicker(_ picker: OpalImagePickerController, didFinishPickingAssets assets: [Photos.PHAsset]) {
            self.assetsSelected(assets)
            isShown = false
        }

        func imagePickerDidCancel(_ picker: OpalImagePickerController) {
            isShown = false
        }
    }
}
