//
// Created by Andrei Stoicescu on 30/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import SwiftUI

struct TextEditView: UIViewRepresentable {
    @Binding var text: String
    var placeholder: NSAttributedString? = nil

    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }

    func makeUIView(context: Context) -> UITextView {

        let textView = UITextView()
        textView.delegate = context.coordinator

        context.coordinator.textView = textView

        if let placeholder = placeholder {
            textView.attributedPlaceholder = placeholder
        }
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.font = AVSFont.bodyRoman.font
        textView.isScrollEnabled = false
        textView.isEditable = true
        textView.isUserInteractionEnabled = true
        textView.backgroundColor = AVSColor.inputFill.color
        textView.textColor = ConciergeForm.textColor.color
        textView.textContainerInset = .zero
        textView.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)

        return textView
    }

    func updateUIView(_ uiView: UITextView, context: Context) {
        uiView.text = text
    }

    class Coordinator: NSObject, UITextViewDelegate {

        var textViewWrapper: TextEditView
        weak var textView: UITextView?

        init(_ uiTextView: TextEditView) {
            textViewWrapper = uiTextView
        }

        func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
            true
        }

        func textViewDidChange(_ textView: UITextView) {
            textViewWrapper.text = textView.text
        }
    }
}
