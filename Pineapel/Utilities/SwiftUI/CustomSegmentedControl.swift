//
// Created by Andrei Stoicescu on 07/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import SwiftUI

extension View {
    func eraseToAnyView() -> AnyView {
        AnyView(self)
    }
}

struct SizePreferenceKey: PreferenceKey {
    typealias Value = CGSize
    static var defaultValue: CGSize = .zero

    static func reduce(value: inout CGSize, nextValue: () -> CGSize) {
        value = nextValue()
    }
}

struct BackgroundGeometryReader: View {
    var body: some View {
        GeometryReader { geometry in
            Color.clear.preference(key: SizePreferenceKey.self, value: geometry.size)
        }
    }
}

struct SizeAwareViewModifier: ViewModifier {

    @Binding private var viewSize: CGSize

    init(viewSize: Binding<CGSize>) {
        self._viewSize = viewSize
    }

    func body(content: Content) -> some View {
        content
                .background(BackgroundGeometryReader())
                .onPreferenceChange(SizePreferenceKey.self, perform: { if self.viewSize != $0 { self.viewSize = $0 } })
    }
}

// struct SegmentedPicker: View {
//     private static let ActiveSegmentColor: Color = .avsColor(.accentBackground)
//     private static let BackgroundColor: Color = .avsColor(.primaryBackground)
//     private static let TextColor: Color = .avsColor(.primaryLabel)
//     private static let SelectedTextColor: Color = .avsColor(.accentBackground)
//
//     private static let SegmentCornerRadius: CGFloat = 12
//     private static let ShadowRadius: CGFloat = 4
//     private static let SegmentXPadding: CGFloat = 16
//     private static let SegmentYPadding: CGFloat = 10
//     private static let PickerPadding: CGFloat = 4
//
//     private static let AnimationDuration: Double = 0.2
//
//     // Stores the size of a segment, used to create the active segment rect
//     @State private var segmentSize: CGSize = .zero
//     // Rounded rectangle to denote active segment
//     private var activeSegmentView: some View {
//         VStack {
//             if self.segmentSize == .zero {
//                 EmptyView()
//             } else {
//                 Spacer()
//                 Rectangle()
//                     .fill(Self.BackgroundColor)
//                     .foregroundColor(Self.SelectedTextColor)
//                     .frame(width: self.segmentSize.width - 40, height: 2)
//                     .offset(x: self.computeActiveSegmentHorizontalOffset() + 20, y: 0)
//                     .animation(segmentSize != .zero ? .easeInOut(duration: SegmentedPicker.AnimationDuration) : nil)
//             }
//
//         }.frame(height: self.segmentSize.height)
//
//     }
//
//     @Binding private var selection: Int
//     private let items: [String]
//
//     init(items: [String], selection: Binding<Int>) {
//         self._selection = selection
//         self.items = items
//     }
//
//     var body: some View {
//         // Align the ZStack to the leading edge to make calculating offset on activeSegmentView easier
//         ZStack(alignment: .leading) {
//             // activeSegmentView indicates the current selection
//             self.activeSegmentView
//             HStack {
//                 ForEach(0 ..< self.items.count, id: \.self) { index in
//                     self.getSegmentView(for: index)
//                 }
//             }
//         }
//     }
//
//     // Helper method to compute the offset based on the selected index
//     private func computeActiveSegmentHorizontalOffset() -> CGFloat {
//         CGFloat(self.selection) * (self.segmentSize.width + SegmentedPicker.SegmentXPadding / 2)
//     }
//
//     // Gets text view for the segment
//     private func getSegmentView(for index: Int) -> some View {
//         guard index < self.items.count else {
//             return EmptyView().eraseToAnyView()
//         }
//         let isSelected = self.selection == index
//         return
//                 Text(self.items[index])
//                         .foregroundColor(isSelected ? SegmentedPicker.SelectedTextColor : SegmentedPicker.TextColor)
//                         .lineLimit(1)
//                         .conciergeFont(.BodyHead)
//                         .padding(.vertical, SegmentedPicker.SegmentYPadding)
//                         .padding(.horizontal, SegmentedPicker.SegmentXPadding)
//                         .frame(minWidth: 0, maxWidth: .infinity)
//                         // Watch for the size of the
//                         .modifier(SizeAwareViewModifier(viewSize: self.$segmentSize))
//                         .onTapGesture { self.onItemTap(index: index) }
//                         .eraseToAnyView()
//     }
//
//     // On tap to change the selection
//     private func onItemTap(index: Int) {
//         guard index < self.items.count else {
//             return
//         }
//         self.selection = index
//     }
//
// }
//
// private struct SegmentedPickerPreviewView: View {
//     @State var selection = 1
//     var body: some View {
//         SegmentedPicker(items: ["Pending", "Accepted", "Completed"], selection: $selection)
//     }
// }
//
// struct SegmentedPickerPreview_Previews: PreviewProvider {
//     static var previews: some View {
//       VStack(spacing: 0) {
//         Rectangle().fill(Color.gray)
//         SegmentedPickerPreviewView()
//         Rectangle().fill(Color.gray)
//       }
//     }
// }
