//
// Created by Andrei Stoicescu on 31/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import SwiftUI
// import FetchImage

struct Unwrap<Value, Content: View>: View {
    private let value: Value?
    private let contentProvider: (Value) -> Content

    init(_ value: Value?, @ViewBuilder content: @escaping (Value) -> Content) {
        self.value = value
        self.contentProvider = content
    }

    var body: some View {
        value.map(contentProvider)
    }
}

// struct ImageView: View {
//     @ObservedObject var image: FetchImage
//
//     public var body: some View {
//         ZStack {
//             image.view?
//                  .resizable()
//                  .aspectRatio(contentMode: .fill)
//         }.animation(.default)
//          .onAppear(perform: image.fetch)
//          .onDisappear(perform: image.cancel)
//     }
// }

struct ProgressBar: View {
    @Binding var value: Double
    var color: UIColor = AVSColor.colorGreen.color

    var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .leading) {
                Rectangle().frame(width: geometry.size.width, height: geometry.size.height)
                           .opacity(0.3)
                           .foregroundColor(.inputBorder)

                Rectangle().frame(width: min(CGFloat(value) * geometry.size.width, geometry.size.width), height: geometry.size.height)
                           .foregroundColor(Color(color))
                           .animation(.linear)
            }.clipShape(RoundedRectangle(cornerRadius: 4))
        }
    }
}

struct DummyView: View {
    var body: some View {
        Text("empty")
    }
}

extension View {
    func hideKeyboard() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}

typealias SwiftUIColor = Color

extension ColorAsset {
    var sColor: SwiftUIColor {
        SwiftUIColor.asset(self)
    }
}

// TODO: remove once we don't support ios 13
public struct CompatibleLazyVStack<Content>: View where Content : View {

    let content: () -> Content
    let alignment: HorizontalAlignment
    let spacing: CGFloat?

    public init(alignment: HorizontalAlignment = .center, spacing: CGFloat? = nil, @ViewBuilder content: @escaping () -> Content) {
        self.content = content
        self.alignment = alignment
        self.spacing = spacing
    }

    @ViewBuilder public var body: some View {
        if #available(iOS 14.0, *) {
            LazyVStack(alignment: alignment, spacing: spacing, content: self.content)
        } else {
            VStack(alignment: alignment, spacing: spacing, content: self.content)
        }
    }
}