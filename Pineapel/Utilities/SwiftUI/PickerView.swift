//
// Created by Andrei Stoicescu on 04/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI

struct PickerOption: Identifiable, Equatable {
    let id: String
    let label: String
}

class PickerTextField: UITextField, UIPickerViewDelegate, UIPickerViewDataSource {
    // MARK: - Public properties
    @Binding var option: PickerOption?
    var options: [PickerOption]

    // MARK: - Initializers
    init(option: Binding<PickerOption?>, options: [PickerOption]) {
        self._option = option
        self.options = options
        super.init(frame: .zero)

        self.pickerView.delegate = self
        self.pickerView.dataSource = self

        if let option = option.wrappedValue,
           let idx = self.options.firstIndex(of: option) {
            self.pickerView.selectRow(idx, inComponent: 0, animated: false)
        }

        // TODO provide this in a different configuration
        font = AVSFont.body.font
        textColor = AVSColor.primaryLabel.color
        inputView = pickerView
        tintColor = .clear
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private lazy var pickerView = UIPickerView()

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        options.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        options[row].label
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.option = options[row]
    }
}

struct PickerView: UIViewRepresentable {
    // MARK: - Public properties
    @Binding var option: PickerOption?
    var options: [PickerOption]

    // MARK: - Initializers
    init<S>(_ title: S, option: Binding<PickerOption?>, options: [PickerOption]) where S: StringProtocol {
        self.placeholder = String(title)
        self._option = option
        self.options = options

        self.textField = PickerTextField(option: option, options: options)
    }

    // MARK: - Public methods
    func makeUIView(context: UIViewRepresentableContext<PickerView>) -> PickerTextField {
        textField.placeholder = placeholder
        return textField
    }

    func updateUIView(_ uiView: PickerTextField, context: UIViewRepresentableContext<PickerView>) {
        uiView.text = option?.label
    }

    // MARK: - Private properties
    private var placeholder: String
    private let textField: PickerTextField
}