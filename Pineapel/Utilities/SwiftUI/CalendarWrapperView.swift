//
// Created by Andrei Stoicescu on 22/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import SwiftUI
import UIKit

struct CalendarWrapperView: UIViewControllerRepresentable {

    @Binding var isShown: Bool
    @Binding var startDate: Date?
    @Binding var endDate: Date?
    var selectedStartDate: Date?
    var maximumDate: Date?
    var minimumDate: Date?
    var title: String
    var selectionMode: CalendarDateRangePickerViewController.SelectionMode = .single
    var disabledDates = [Date]()

    func makeCoordinator() -> Coordinator {
        Coordinator(self, isShown: $isShown, startDate: $startDate, endDate: $endDate)
    }

    func makeUIViewController(context: Context) -> CalendarDateRangePickerViewController {
        let dateRangePicker = CalendarDateRangePickerViewController(collectionViewLayout: UICollectionViewFlowLayout())
        dateRangePicker.delegate = context.coordinator
        dateRangePicker.selectionMode = selectionMode
        dateRangePicker.minimumDate = minimumDate
        dateRangePicker.selectedStartDate = startDate
        dateRangePicker.selectedEndDate = endDate
        dateRangePicker.maximumDate = maximumDate
        dateRangePicker.firstDayOfWeek = .monday
        dateRangePicker.titleText = title
        dateRangePicker.disabledDates = disabledDates
        return dateRangePicker
    }

    func updateUIViewController(_ calendarViewController: CalendarDateRangePickerViewController, context: Context) {
        calendarViewController.disabledDates = self.disabledDates
        calendarViewController.collectionView.reloadData()
    }

    class Coordinator: NSObject, CalendarDateRangePickerViewControllerDelegate {
        let parent: CalendarWrapperView
        @Binding var isShown: Bool
        @Binding var startDate: Date?
        @Binding var endDate: Date?

        init(_ parent: CalendarWrapperView, isShown: Binding<Bool>, startDate: Binding<Date?>, endDate: Binding<Date?>) {
            self.parent = parent
            _isShown = isShown
            _startDate = startDate
            _endDate = endDate
        }

        func didCancelPickingDateRange() {
            isShown = false
        }

        func didPickDateRange(startDate: Date!, endDate: Date!) {}

        func didPickSingleDate(date: Date) {}

        func didSelectStartDate(startDate: Date!) {
            self.startDate = startDate
            if self.parent.selectionMode == .single {
                isShown = false
            }

            if self.parent.selectionMode == .range {
                self.endDate = nil
            }
        }

        func didSelectEndDate(endDate: Date!) {
            self.endDate = endDate
        }
    }
}