//
// Created by Andrei Stoicescu on 07/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import SwiftUI
import UIKit
struct PageNumber: ExpressibleByIntegerLiteral, Comparable {
    static func < (lhs: PageNumber, rhs: PageNumber) -> Bool {
        lhs.value < rhs.value
    }

    init(integerLiteral value: Int) {
        self.value = value
        self.previousValue = value
    }

    var value: Int {
        willSet {
            previousValue = value
        }
    }

    var previousValue: Int
}

struct PageViewController: UIViewControllerRepresentable {
    var controllers: [UIViewController]
    @Binding var currentPage: PageNumber

    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }

    func makeUIViewController(context: Context) -> UIPageViewController {
        let pageVC = UIPageViewController(transitionStyle: .scroll,
                                          navigationOrientation: .horizontal)
        pageVC.dataSource = context.coordinator
        pageVC.delegate = context.coordinator

        return pageVC
    }

    func updateUIViewController(_ pageViewController: UIPageViewController, context: Context) {
        let direction: UIPageViewController.NavigationDirection = currentPage.value >= currentPage.previousValue ? .forward : .reverse
        pageViewController.setViewControllers([controllers[currentPage.value]],
                                              direction: direction,
                                              animated: true,
                                              completion: nil)
    }

    class Coordinator: NSObject, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
        let parent: PageViewController

        init(_ parent: PageViewController) {
            self.parent = parent
        }

        func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
            guard let index = parent.controllers.firstIndex(of: viewController) else { return nil }

            if index == 0 {
                return nil // or parent.controllers.last
            }

            return parent.controllers[index - 1]
        }

        func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
            guard let index = parent.controllers.firstIndex(of: viewController) else { return nil }

            if index == parent.controllers.endIndex - 1 {
                return nil // or parent.controllers.first
            }

            return parent.controllers[index + 1]
        }

        func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
            if completed,
               let visibleViewController = pageViewController.viewControllers?.first,
               let index = parent.controllers.firstIndex(of: visibleViewController) {

                parent.currentPage.value = index
            }

        }
    }
}

struct PageView<Page: View>: View {

    let controllers: [UIHostingController<Page>]
    @Binding var currentPage: PageNumber

    init(_ pages: [Page], currentPage: Binding<PageNumber>) {
        controllers = pages.map { UIHostingController(rootView: $0) }
        self._currentPage = currentPage
    }

    var body: some View {
        VStack {
            PageViewController(controllers: controllers, currentPage: $currentPage)
                    .id(UUID())
        }
    }
}
