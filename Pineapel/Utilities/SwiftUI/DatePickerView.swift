//
// Created by Andrei Stoicescu on 02/09/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI

class DateTextField: UITextField {

    struct DatePickerConfig {
        var datePickerMode: UIDatePicker.Mode = .dateAndTime
        var locale: Locale = .current
        var calendar: Calendar = .current
        var timeZone: TimeZone? = nil
        var minimumDate: Date? = nil
        var maximumDate: Date? = nil
        var countDownDuration: TimeInterval = 0
        var minuteInterval: Int = 1

        static var `default`: DatePickerConfig {
            DatePickerConfig()
        }

        static var dateOnly: DatePickerConfig {
            DatePickerConfig(datePickerMode: .date)
        }
    }

    // MARK: - Public properties
    @Binding var date: Date?

    // MARK: - Initializers
    init(date: Binding<Date?>, config: DatePickerConfig) {
        self._date = date
        super.init(frame: .zero)

        if let date = date.wrappedValue {
            self.datePickerView.date = date
        }

        // TODO provide this in a different configuration
        self.font = AVSFont.body.font
        self.textColor = AVSColor.primaryLabel.color

        self.datePickerView.datePickerMode = config.datePickerMode
        self.datePickerView.locale = config.locale
        self.datePickerView.calendar = config.calendar
        self.datePickerView.timeZone = config.timeZone
        self.datePickerView.minimumDate = config.minimumDate
        self.datePickerView.maximumDate = config.maximumDate
        self.datePickerView.countDownDuration = config.countDownDuration
        self.datePickerView.minuteInterval = config.minuteInterval
        if #available(iOS 13.4, *) {
            self.datePickerView.preferredDatePickerStyle = .wheels
        }

        self.datePickerView.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
        self.inputView = datePickerView
        self.tintColor = .clear
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private properties
    private lazy var datePickerView = UIDatePicker()

    // MARK: - Private methods
    @objc func dateChanged(_ sender: UIDatePicker) {
        self.date = sender.date
    }
}

struct DatePickerField: UIViewRepresentable {
    // MARK: - Public properties
    @Binding var date: Date?

    // MARK: - Initializers
    init<S>(_ title: S, date: Binding<Date?>, formatter: DateFormatter = .yearMonthDay, config: DateTextField.DatePickerConfig = .default) where S: StringProtocol {
        self.placeholder = String(title)
        self._date = date

        self.textField = DateTextField(date: date, config: config)
        self.formatter = formatter
    }

    // MARK: - Public methods
    func makeUIView(context: UIViewRepresentableContext<DatePickerField>) -> UITextField {
        textField.placeholder = placeholder
        return textField
    }

    func updateUIView(_ uiView: UITextField, context: UIViewRepresentableContext<DatePickerField>) {
        if let date = date {
            uiView.text = formatter.string(from: date)
        } else {
            uiView.text = nil
        }
    }

    // MARK: - Private properties
    private var placeholder: String
    private let formatter: DateFormatter
    private let textField: DateTextField
}

extension DateFormatter {
    static var yearMonthDay: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }
}