//
//  DynamicTextView.swift
//  A Votre Service
//
//  Created by Andrei Stoicescu on 30/09/2020.
//  Copyright © 2020 A Votre Service LLC. All rights reserved.
//

import SwiftUI

struct DynamicHeightTextField: UIViewRepresentable {
    @Binding var text: String
    @Binding var height: CGFloat
    var placeholder: NSAttributedString? = nil

    func makeUIView(context: Context) -> UITextView {
        let textView = UITextView()

        textView.isScrollEnabled = true
        textView.alwaysBounceVertical = false
        textView.isEditable = true
        textView.isUserInteractionEnabled = true

        textView.text = text
        textView.backgroundColor = UIColor.clear

        if let placeholder = placeholder {
            textView.attributedPlaceholder = placeholder
        }

        textView.font = AVSFont.body.font
        textView.textColor = AVSColor.primaryLabel.color
        textView.textContainerInset = .zero

        context.coordinator.textView = textView
        textView.delegate = context.coordinator
        textView.layoutManager.delegate = context.coordinator

        return textView
    }

    func updateUIView(_ uiView: UITextView, context: Context) {
        uiView.text = text
    }

    func makeCoordinator() -> Coordinator {
        Coordinator(dynamicSizeTextField: self)
    }

    class Coordinator: NSObject, UITextViewDelegate, NSLayoutManagerDelegate {

        var dynamicHeightTextField: DynamicHeightTextField

        weak var textView: UITextView?

        init(dynamicSizeTextField: DynamicHeightTextField) {
            dynamicHeightTextField = dynamicSizeTextField
        }

        func textViewDidChange(_ textView: UITextView) {
            dynamicHeightTextField.text = textView.text
        }

        func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
            true
        }

        func textViewDidBeginEditing(_ textView: UITextView) {

        }

        func layoutManager(_ layoutManager: NSLayoutManager, didCompleteLayoutFor textContainer: NSTextContainer?, atEnd layoutFinishedFlag: Bool) {
            DispatchQueue.main.async { [weak self] in
                guard let textView = self?.textView else {
                    return
                }
                let size = textView.sizeThatFits(textView.bounds.size)
                if self?.dynamicHeightTextField.height != size.height {
                    self?.dynamicHeightTextField.height = size.height
                }
            }

        }
    }
}

struct DynamicHeightTextView: View {
    @Binding var text: String
    var minHeight: CGFloat
    var maxHeight: CGFloat
    var placeholder: NSAttributedString?

    @State private var textHeight: CGFloat = 0

    private var textFieldHeight: CGFloat {
        if textHeight < minHeight { return minHeight }
        if textHeight > maxHeight { return maxHeight }
        return textHeight
    }

    var body: some View {
        DynamicHeightTextField(text: $text, height: $textHeight, placeholder: placeholder)
                .frame(height: textFieldHeight)
    }
}

private struct DynamicHeightTextViewWrapper: View {
    @State private var text: String = ""
    var body: some View {
        DynamicHeightTextView(text: $text, minHeight: 30, maxHeight: 200, placeholder: "Type your message here".attributed(.body, .secondaryLabel))
                .padding(10)
                .background(.inputFill)
                .border(.inputBorder)
                .padding(10)
    }
}

struct DynamicTextView_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            Spacer()
            DynamicHeightTextViewWrapper()
        }
    }
}
