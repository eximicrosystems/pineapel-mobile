//
// Created by Andrei Stoicescu on 04/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation

public typealias LosslessStringCodable = LosslessStringConvertible & Codable

enum CodableError: Error {
    case singleItemEmpty
    case dateDecoding
}

public struct Safe<Base: Decodable>: Decodable {
    public let value: Base?

    public init(from decoder: Decoder) throws {
        do {
            let container = try decoder.singleValueContainer()
            self.value = try container.decode(Base.self)
        } catch {
            assertionFailure("ERROR: \(error)")
            // TODO: automatically send a report about a corrupted data
            self.value = nil
        }
    }
}

struct SafeArrayDecodable<T: Decodable>: Decodable {
    let array: [T]

    init(from decoder: Decoder) throws {
        var map = try decoder.unkeyedContainer()
        var elements = [T]()
        while !map.isAtEnd {
            if let element = try map.decode(Safe<T>.self).value {
                elements.append(element)
            }
        }

        self.array = elements
    }
}

extension KeyedDecodingContainer {
    public func decode<T: Decodable>(_ key: Key, as type: T.Type = T.self) throws -> T {
        try self.decode(type, forKey: key)
    }

    public func decodeIfPresent<T: Decodable>(_ key: Key, as type: T.Type = T.self) throws -> T? {
        let value = try decodeIfPresent(type, forKey: key)
        if type == String.self,
           let stringValue = value as? String,
           stringValue.isEmpty {
            return nil
        }
        return value
    }

    public func decodeSingleItem<T: Decodable>(_ key: Key) throws -> T {
        let decoded = try self.decode([T].self, forKey: key)
        guard let item = decoded.first else { throw CodableError.singleItemEmpty }
        return item
    }
}

extension KeyedDecodingContainer {
    public func decodeSafely<T: Decodable>(_ key: KeyedDecodingContainer.Key) -> T? {
        return self.decodeSafely(T.self, forKey: key)
    }

    public func decodeSafely<T: Decodable>(_ type: T.Type, forKey key: KeyedDecodingContainer.Key) -> T? {
        let decoded = try? decode(Safe<T>.self, forKey: key)
        return decoded?.value
    }

    public func decodeSafelyIfPresent<T: Decodable>(_ key: KeyedDecodingContainer.Key) -> T? {
        return self.decodeSafelyIfPresent(T.self, forKey: key)
    }

    public func decodeSafelyIfPresent<T: Decodable>(_ type: T.Type, forKey key: KeyedDecodingContainer.Key) -> T? {
        let decoded = try? decodeIfPresent(Safe<T>.self, forKey: key)
        return decoded?.value
    }

    public func decodeDateIfPresent(_ key: Key, timeZone: TimeZone? = nil, skipUnix: Bool = false) -> Date? {
        var date: Date? = nil
        if !skipUnix, let dateDouble = try? decodeLosslessIfPresent(key, as: Double.self) {
            date = Date(timeIntervalSince1970: dateDouble)
        } else if let dateString = try? decodeIfPresent(key, as: String.self) {
            // https://nsdateformatter.com/
            date = [
                "yyyy-MM-dd'T'HH:mm:ssZZZZZ",
                "yyyy-MM-dd'T'hh:mm:ssZZZZZ",
                "yyyy-MM-dd'T'hh:mm:ss",
                "MM/dd/yyyy 'at' h:mm a",
                "MMM dd'st,' yyyy hh:mma",
                "MMM dd'th,' yyyy hh:mma",
                "yyyy-MM-dd"
            ].map { DateFormatter(format: $0, timeZone: timeZone) }.first(where: { $0.date(from: dateString) != nil })?.date(from: dateString)
        }
        return date
    }

    public func decodeDate(_ key: Key) throws -> Date {
        guard let date = decodeDateIfPresent(key) else { throw CodableError.dateDecoding }
        return date
    }

    public func decodeLossless<T: LosslessStringCodable>(_ key: KeyedDecodingContainer.Key, as type: T.Type = T.self) throws -> T {
        do {
            return try decode(type, forKey: key)
        } catch let error {

            func decodeType<T: LosslessStringCodable>(_ decodeAs: T.Type) -> () -> LosslessStringCodable? {
                {
                    if type == Bool.self && decodeAs == String.self {
                        guard let str = try? self.decode(key, as: String.self) else { return nil }

                        switch str.lowercased() {
                        case "true", "t", "yes", "y", "1":
                            return true
                        case "false", "f", "no", "n", "0", "":
                            return false
                        default:
                            return nil
                        }
                    } else if type == Float.self && decodeAs == String.self {
                        let numberFormatter = NumberFormatter()
                        numberFormatter.numberStyle = .decimal
                        numberFormatter.locale = Locale(identifier: "en_US")
                        guard let str = try? self.decode(key, as: String.self) else { return nil }
                        return numberFormatter.number(from: str)?.floatValue
                    } else {
                        return try? self.decode(key, as: decodeAs)
                    }
                }
            }

            let types: [() -> LosslessStringCodable?] = [
                decodeType(String.self),
                decodeType(Bool.self),
                decodeType(Int.self),
                decodeType(Double.self),
                decodeType(Float.self),
            ]

            guard let rawValue = types.lazy.compactMap({ $0() }).first,
                  let value = T.init("\(rawValue)") else {
                throw error
            }

            return value
        }
    }

    public func decodeLosslessIfPresent<T: LosslessStringCodable>(_ key: KeyedDecodingContainer.Key, as type: T.Type = T.self) throws -> T? {
        try? decodeLossless(key, as: type)
    }
}

extension KeyedEncodingContainer {
    public mutating func encodeSingleItemArray<T>(_ value: T?, paramName: String, forKey key: KeyedEncodingContainer.Key) throws where T: Encodable {
        var arrayContainer = nestedUnkeyedContainer(forKey: key)
        try arrayContainer.encode([paramName: value])
    }

    public mutating func encodeSingleItemArray<T>(_ value: T?, forKey key: KeyedEncodingContainer.Key) throws where T: Encodable {
        var arrayContainer = nestedUnkeyedContainer(forKey: key)
        try arrayContainer.encode(value)
    }

    public mutating func encodeSingleItemArrayIfPresent<T>(_ value: T?, paramName: String, forKey key: KeyedEncodingContainer.Key) throws where T: Encodable {
        if let value = value {
            try self.encodeSingleItemArray(value, paramName: paramName, forKey: key)
        }
    }

    public mutating func encodeSingleItemArrayIfPresent<T>(_ value: T?, forKey key: KeyedEncodingContainer.Key) throws where T: Encodable {
        if let value = value {
            try self.encodeSingleItemArray(value, forKey: key)
        }
    }

    public mutating func encodeValueCodableItemArray<T>(_ value: T, forKey key: KeyedEncodingContainer.Key) throws where T: Encodable {
        var arrayContainer = nestedUnkeyedContainer(forKey: key)
        try arrayContainer.encode(["value": value])
    }

    public mutating func encodeItemArray<T>(_ value: T, forKey key: KeyedEncodingContainer.Key) throws where T: Encodable {
        var arrayContainer = nestedUnkeyedContainer(forKey: key)
        try arrayContainer.encode(value)
    }

    public mutating func encodeNumericBoolIfPresent(_ value: Bool?, forKey key: KeyedEncodingContainer.Key) throws {
        if let value = value {
            try self.encodeIfPresent(value ? "1" : "0", forKey: key)
        }
    }

    public mutating func encodeValueCodableItemArrayIfPresent<T>(_ value: T?, forKey key: KeyedEncodingContainer.Key) throws where T: Encodable {
        if let value = value {
            try self.encodeValueCodableItemArray(value, forKey: key)
        }
    }
}