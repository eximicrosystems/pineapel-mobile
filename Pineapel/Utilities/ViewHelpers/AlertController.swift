//
// Created by Andrei Stoicescu on 20/08/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import UIKit

extension UIAlertController {
    func fixIpadIfNeeded(view: UIView) {
        if UIDevice.current.userInterfaceIdiom == .pad {
            self.popoverPresentationController?.sourceView = view
            self.popoverPresentationController?.sourceRect = view.bounds
            self.popoverPresentationController?.permittedArrowDirections = [.down, .up]
        }
    }
}