//
// Created by Andrei Stoicescu on 23/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import ImageViewer
import UIKit

struct GalleryHelpers {
    static var defaultConfiguration: GalleryConfiguration {
        [
            .seeAllCloseButtonMode(.none),
            .thumbnailsButtonMode(.none),
            .deleteButtonMode(.none),
            .videoAutoPlay(true)
        ]
    }
}

class GalleryCounterView: UIView {

    var count: Int
    let countPager = UIPageControl()

    var currentIndex: Int {
        didSet {
            updatePager()
        }
    }

    init(frame: CGRect, currentIndex: Int, count: Int) {
        self.currentIndex = currentIndex
        self.count = count

        super.init(frame: frame)

        configurePager()
        updatePager()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func configurePager() {
        countPager.pageIndicatorTintColor = AVSColor.pageIndicatorTintColor.color
        countPager.currentPageIndicatorTintColor = AVSColor.currentPageIndicatorTintColor.color
        countPager.numberOfPages = count
        countPager.isUserInteractionEnabled = false
        addSubview(countPager)
    }

    func updatePager() {
        countPager.numberOfPages = count
        countPager.currentPage = currentIndex
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        countPager.frame = bounds
    }
}
