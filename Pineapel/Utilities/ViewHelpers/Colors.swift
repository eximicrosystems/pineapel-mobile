//
// Created by Andrei Stoicescu on 04.01.2021.
// Copyright (c) 2021 A Votre Service LLC. All rights reserved.
//

import UIKit
import SwiftUI

enum AVSColor {
    case clear
    case accentBackground
    case accentForeground
    case borderPrimary
    case calendarSelectionFill
    case chatInputFill
    case chatMessageFill
    case colorAccentLight
    case colorGreen
    case colorMilitaryGreen
    case colorOrange
    case colorTeal
    case colorYellow
    case colorPurple
    case colorBlue
    case colorYellowRating
    case divider
    case dividerSecondary
    case inputBorder
    case inputFill
    case primaryBackground
    case primaryIcon
    case primaryLabel
    case secondaryBackground
    case secondaryLabel
    case selectSheetFill
    case tertiaryBackground
    case tertiaryLabel

    case pageIndicatorTintColor
    case currentPageIndicatorTintColor

    var color: UIColor {
        switch self {
        case .clear:
            return .clear
        case .accentBackground:
//            return Asset.Colors.Semantic.accentFill.color
            return Asset.Colors.Semantic.accentLabel.color
//            return Asset.Colors.Semantic.tertiaryLabel.color
        
        case .accentForeground:
            return Asset.Colors.Semantic.accentLabel.color
//            return Asset.Colors.Semantic.colorMilitary.color
        case .borderPrimary:
            return Asset.Colors.Semantic.borderPrimary.color
        case .calendarSelectionFill:
            return Asset.Colors.Semantic.calendarSelectionFill.color
        case .chatInputFill:
            return Asset.Colors.Semantic.accentLabel.color
//            return Asset.Colors.Semantic.chatInputFill.color
        case .chatMessageFill:
            return Asset.Colors.Semantic.chatMessageFill.color
        case .colorAccentLight:
            return Asset.Colors.Semantic.colorAccentLight.color
        case .colorGreen:
            return Asset.Colors.Semantic.colorGreen.color
        case .colorMilitaryGreen:
            return Asset.Colors.Semantic.colorMilitary.color
        case .colorOrange:
            return Asset.Colors.Semantic.colorOrange.color
        case .colorTeal:
            return Asset.Colors.Semantic.colorTeal.color
        case .colorYellow:
            return Asset.Colors.Semantic.colorYellow.color
        case .colorYellowRating:
            return Asset.Colors.Semantic.colorYellowRating.color
        case .colorBlue:
            return Asset.Colors.Semantic.colorBlue.color
        case .colorPurple:
            return Asset.Colors.Semantic.colorPurple.color
        case .divider:
            return Asset.Colors.Semantic.divider.color
        case .dividerSecondary:
            return Asset.Colors.Semantic.dividerSecondary.color
        case .inputBorder:
            return Asset.Colors.Semantic.inputBorder.color
        case .inputFill:
            return Asset.Colors.Semantic.inputFill.color
        case .primaryBackground:
            return Asset.Colors.Semantic.primaryBackground.color
//            return Asset.Colors.Semantic.colorMilitary.color
        case .primaryIcon:
//            return Asset.Colors.Semantic.primaryIcon.color
            return Asset.Colors.Semantic.colorMilitary.color
        case .primaryLabel:
//            return Asset.Colors.Semantic.primaryLabel.color
            return Asset.Colors.Semantic.colorMilitary.color
        case .secondaryBackground:
            return Asset.Colors.Semantic.secondaryBackground.color
        case .secondaryLabel:
            return Asset.Colors.Semantic.secondaryLabel.color
        case .selectSheetFill:
            return Asset.Colors.Semantic.selectSheetFill.color
        case .tertiaryBackground:
            return Asset.Colors.Semantic.colorMilitary.color
        case .tertiaryLabel:
//            return Asset.Colors.Semantic.tertiaryLabel.color
//            return Asset.Colors.Semantic.accentLabel.color
            return Asset.Colors.Semantic.colorMilitary.color
        case .pageIndicatorTintColor:
//            return Asset.Colors.Semantic.tertiaryLabel.color
            return Asset.Colors.Semantic.accentLabel.color
        case .currentPageIndicatorTintColor:
            return Asset.Colors.Semantic.primaryLabel.color
        }
    }
}

enum AVSFont {
    case body
    case bodyRoman
    case bodyHeavy
    case bodyTitleHeavy
    case bodyTitleMedium
    case button
    case note
    case noteMedium
    case H1
    case H1Large
    case H2
    case H2Black
    case H3
    case H4
    case H5

    var font: UIFont {
        switch self {
        case .body:
            return UIFont(name: "Avenir-Roman", size: 14)!
        case .bodyHeavy:
            return UIFont(name: "Avenir-Heavy", size: 14)!
        case .bodyTitleHeavy:
            return UIFont(name: "Avenir-Heavy", size: 16)!
        case .bodyTitleMedium:
            return UIFont(name: "Avenir-Medium", size: 16)!
        case .bodyRoman:
            return UIFont(name: "Avenir-Book", size: 14)! // Input text
        case .note:
            return UIFont(name: "Avenir-Roman", size: 12)!
        case .noteMedium:
            return UIFont(name: "Avenir-Medium", size: 12)!
        case .H1:
            return UIFont(name: "Avenir-Black", size: 38)! // login title
        case .H1Large:
            return UIFont(name: "Avenir-Medium", size: 60)! // dashboard items
        case .H2:
            return UIFont(name: "Avenir-Heavy", size: 28)!
        case .H2Black:
            return UIFont(name: "Avenir-Black", size: 28)!
        case .H3:
            return UIFont(name: "Avenir-Medium", size: 22)! // login subtitle
        case .H4:
            return UIFont(name: "Avenir-Heavy", size: 20)!
        case .H5:
            return UIFont(name: "Avenir-Heavy", size: 12)!
        case .button:
            return UIFont(name: "Avenir-Medium", size: 19)!
        }
    }
}

extension String {
    func attributed(_ font: AVSFont, _ color: AVSColor, alignment: NSTextAlignment = .left, otherAttributes: [NSAttributedString.Key: Any]? = nil) -> NSAttributedString? {
        attributed(font.font, color.color, alignment: alignment, otherAttributes: otherAttributes)
    }

    func attributed(_ font: UIFont, _ color: UIColor, alignment: NSTextAlignment = .left, otherAttributes: [NSAttributedString.Key: Any]? = nil) -> NSAttributedString? {
        NSAttributedString(string: self, attributes: NSAttributedString.attributes(font: font,
                                                                                   color: color,
                                                                                   alignment: alignment,
                                                                                   otherAttributes: otherAttributes))
    }

    var html: NSAttributedString? {
        HTMLTemplateAttributedString.from(string: self)
    }
}

extension Color {
    static func avsColor(_ color: AVSColor, alpha: CGFloat = 1) -> Color {
        .init(color.color.withAlphaComponent(alpha))
    }
}
