//
// Created by Andrei Stoicescu on 09/06/2020.
// Copyright (c) 2020 A Votre Service LLC. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SwiftyUserDefaults
import DifferenceKit

extension ASDisplayNode {
    enum AVSDimension {
        case points(CGFloat)
        case fraction(CGFloat)
    }

    static func spacer(color: UIColor = .clear) -> ASDisplayNode {
        let node = ASDisplayNode()
        node.style.flexGrow = 1
        node.style.preferredSize = .zero
        node.backgroundColor = color
        return node
    }

    static func hLine(color: UIColor) -> ASDisplayNode {
        let node = ASDisplayNode()
        node.style.height = ASDimensionMakeWithPoints(1)
        node.backgroundColor = color
        return node
    }

    static func divider() -> ASDisplayNode {
        hLine(color: AVSColor.divider.color)
    }

    static func vLine(color: UIColor) -> ASDisplayNode {
        let node = ASDisplayNode()
        node.style.width = ASDimensionMakeWithPoints(1)
        node.backgroundColor = color
        return node
    }
}

enum ASRelativePosition {
    case topLeft
    case topMiddle
    case topRight
    case centerLeft
    case center
    case centerRight
    case bottomLeft
    case bottomCenter
    case bottomRight
}

extension ASLayoutElement {


    func el() -> ASLayoutElement {
        self
    }

    func alignSelf(_ alignment: ASStackLayoutAlignSelf) -> ASLayoutElement {
        style.alignSelf = alignment
        return self
    }

    func wrapped() -> ASLayoutSpec {
        ASWrapperLayoutSpec.wrapper(with: self)
    }

    func margin(_ insets: UIEdgeInsets) -> ASLayoutSpec {
        ASInsetLayoutSpec(insets: insets, child: self)
    }

    func margin(top: CGFloat = 0, left: CGFloat = 0, bottom: CGFloat = 0, right: CGFloat = 0) -> ASLayoutSpec {
        margin(UIEdgeInsets(top: top, left: left, bottom: bottom, right: right))
    }

    func margin(horizontal: CGFloat = 0, vertical: CGFloat = 0) -> ASLayoutSpec {
        margin(top: vertical, left: horizontal, bottom: vertical, right: horizontal)
    }

    func margin(_ m: CGFloat) -> ASLayoutSpec {
        margin(top: m, left: m, bottom: m, right: m)
    }

    func ratio(_ ratio: CGFloat) -> ASLayoutSpec {
        ASRatioLayoutSpec(ratio: ratio, child: self)
    }

    func background(_ background: ASLayoutElement) -> ASLayoutSpec {
        ASBackgroundLayoutSpec(child: self, background: background)
    }

    func overlay(_ element: ASLayoutElement) -> ASLayoutSpec {
        ASOverlayLayoutSpec(child: element, overlay: self)
    }

    func centered() -> ASLayoutSpec {
        ASCenterLayoutSpec(centeringOptions: .XY, sizingOptions: .minimumXY, child: self)
    }

    func relative(_ position: ASRelativePosition) -> ASLayoutSpec {
        switch position {
        case .topLeft:
            return ASRelativeLayoutSpec(horizontalPosition: .start, verticalPosition: .start, sizingOption: .minimumSize, child: self)
        case .topMiddle:
            return ASRelativeLayoutSpec(horizontalPosition: .center, verticalPosition: .start, sizingOption: .minimumSize, child: self)
        case .topRight:
            return ASRelativeLayoutSpec(horizontalPosition: .end, verticalPosition: .start, sizingOption: .minimumSize, child: self)
        case .centerLeft:
            return ASRelativeLayoutSpec(horizontalPosition: .start, verticalPosition: .center, sizingOption: .minimumSize, child: self)
        case .center:
            return ASRelativeLayoutSpec(horizontalPosition: .center, verticalPosition: .center, sizingOption: .minimumSize, child: self)
        case .centerRight:
            return ASRelativeLayoutSpec(horizontalPosition: .end, verticalPosition: .center, sizingOption: .minimumSize, child: self)
        case .bottomLeft:
            return ASRelativeLayoutSpec(horizontalPosition: .start, verticalPosition: .end, sizingOption: .minimumSize, child: self)
        case .bottomCenter:
            return ASRelativeLayoutSpec(horizontalPosition: .center, verticalPosition: .end, sizingOption: .minimumSize, child: self)
        case .bottomRight:
            return ASRelativeLayoutSpec(horizontalPosition: .end, verticalPosition: .end, sizingOption: .minimumSize, child: self)
        }
    }

    func growing(_ flexGrow: CGFloat = 1) -> ASLayoutElement {
        style.flexGrow = flexGrow
        return self
    }

    func shrinking() -> ASLayoutElement {
        style.flexShrink = 1
        return self
    }

    private func _dimension(_ dimension: ASDisplayNode.AVSDimension) -> ASDimension {
        switch dimension {
        case .points(let dim):
            return ASDimensionMakeWithPoints(dim)
        case .fraction(let dim):
            return ASDimensionMakeWithFraction(dim)
        }
    }

    func maxWidth(_ dimension: ASDisplayNode.AVSDimension) -> ASLayoutElement {
        style.maxWidth = _dimension(dimension)
        return self
    }

    func maxHeight(_ dimension: ASDisplayNode.AVSDimension) -> ASLayoutElement {
        style.maxHeight = _dimension(dimension)
        return self
    }

    func minHeight(_ dimension: ASDisplayNode.AVSDimension) -> ASLayoutElement {
        style.minHeight = _dimension(dimension)
        return self
    }

    func minWidth(_ dimension: ASDisplayNode.AVSDimension) -> ASLayoutElement {
        style.minWidth = _dimension(dimension)
        return self
    }

    func width(_ dimension: ASDisplayNode.AVSDimension) -> ASLayoutElement {
        style.width = _dimension(dimension)
        return self
    }

    func height(_ dimension: ASDisplayNode.AVSDimension) -> ASLayoutElement {
        style.height = _dimension(dimension)
        return self
    }

    func filling() -> ASLayoutElement {
        growing().shrinking()
    }

    func size(_ width: CGFloat, _ height: CGFloat) -> ASLayoutElement {
        size(CGSize(width: width, height: height))
    }

    func size(_ size: CGSize) -> ASLayoutElement {
        style.preferredSize = size
        return self
    }

    func square(_ dimension: CGFloat) -> ASLayoutElement {
        size(CGSize(width: dimension, height: dimension))
    }

    func spacingBefore(_ spacing: CGFloat) -> ASLayoutElement {
        style.spacingBefore = spacing
        return self
    }

    func spacingAfter(_ spacing: CGFloat) -> ASLayoutElement {
        style.spacingAfter = spacing
        return self
    }
}

extension Array where Element == ASLayoutElement {
    func vStacked(spacing: CGFloat = 0) -> ASStackLayoutSpec {
        ASStackLayoutSpec(direction: .vertical, spacing: spacing, justifyContent: .start, alignItems: .stretch, children: self)
    }

    func hStacked(spacing: CGFloat = 0) -> ASStackLayoutSpec {
        ASStackLayoutSpec(direction: .horizontal, spacing: spacing, justifyContent: .start, alignItems: .center, children: self)
    }

    func stackEqualSizes() -> [Element] {
        enumerated().forEach { index, item in
            self[index].style.flexGrow = 1
            self[index].style.flexShrink = 1
            self[index].style.flexBasis = ASDimensionMakeWithPoints(10)
        }
        return self
    }

    func wrapped() -> ASWrapperLayoutSpec {
        ASWrapperLayoutSpec(layoutElements: self)
    }
}

extension Array where Element == ASLayoutElement? {
    func vStacked(spacing: CGFloat = 0) -> ASStackLayoutSpec {
        ASStackLayoutSpec(direction: .vertical, spacing: spacing, justifyContent: .start, alignItems: .stretch, children: self.compactMap { $0 })
    }

    func hStacked(spacing: CGFloat = 0) -> ASStackLayoutSpec {
        ASStackLayoutSpec(direction: .horizontal, spacing: spacing, justifyContent: .start, alignItems: .center, children: self.compactMap { $0 })
    }

    func stackEqualSizes() -> [Element] {
        enumerated().forEach { index, item in
            self[index]?.style.flexGrow = 1
            self[index]?.style.flexShrink = 1
            self[index]?.style.flexBasis = ASDimensionMakeWithPoints(10)
        }
        return self
    }

    func wrapped() -> ASWrapperLayoutSpec {
        ASWrapperLayoutSpec(layoutElements: self.compactMap { $0 })
    }
}

extension ASStackLayoutSpec {
    func spaced(_ spacing: CGFloat) -> ASStackLayoutSpec {
        self.spacing = spacing
        return self
    }

    func justified(_ justify: ASStackLayoutJustifyContent) -> ASStackLayoutSpec {
        justifyContent = justify
        return self
    }

    func aligned(_ alignment: ASStackLayoutAlignItems) -> ASStackLayoutSpec {
        alignItems = alignment
        return self
    }
}

extension ASTextNode2 {
    convenience init(with attributedText: NSAttributedString?) {
        self.init()
        self.attributedText = attributedText
    }

    var isEmpty: Bool {
        guard let string = attributedText?.string else { return true }
        return string.count == 0
    }
}

extension ASImageNode {
    convenience init(image: UIImage?, tintColor: UIColor? = nil) {
        self.init()
        self.image = image
        if let tintColor = tintColor {
            self.image = image?.withRenderingMode(.alwaysTemplate)
            self.tintColor = tintColor
        }
    }
}

extension ASSizeRange {
    static func fullWidth(collectionNode: ASCollectionNode) -> ASSizeRange {
        ASSizeRangeMake(
                CGSize(width: collectionNode.bounds.size.width, height: 0),
                CGSize(width: collectionNode.bounds.size.width, height: CGFloat.greatestFiniteMagnitude)
        )
    }

    static func halfWidth(collectionNode: ASCollectionNode, right: Bool = false) -> ASSizeRange {
        let width = collectionNode.bounds.size.width

        let leftWidth = Int(floor(width / 2))
        let rightWidth = Int(width) - leftWidth

        let finalWidth = CGFloat(right ? rightWidth : leftWidth)

        return ASSizeRangeMake(
                CGSize(width: finalWidth, height: 44),
                CGSize(width: finalWidth, height: CGFloat.greatestFiniteMagnitude)
        )
    }
}

extension ASCollectionNode {
    static func avsCollection(flowLayout: UICollectionViewFlowLayout = .vertical) -> ASCollectionNode {
        let collectionNode = ASCollectionNode(collectionViewLayout: flowLayout)

        collectionNode.contentInset = .collectionConciergeInset
        collectionNode.backgroundColor = AVSColor.primaryBackground.color

        collectionNode.alwaysBounceVertical = true
        return collectionNode
    }

    func performDiffBatchUpdates<C>(changeSet: Changeset<C>, animated: Bool) {
        self.performBatch(animated: animated) {
            if !changeSet.sectionDeleted.isEmpty {
                self.deleteSections(IndexSet(changeSet.sectionDeleted))
            }

            if !changeSet.sectionInserted.isEmpty {
                self.insertSections(IndexSet(changeSet.sectionInserted))
            }

            if !changeSet.sectionUpdated.isEmpty {
                self.reloadSections(IndexSet(changeSet.sectionUpdated))
            }

            for (source, target) in changeSet.sectionMoved {
                self.moveSection(source, toSection: target)
            }

            if !changeSet.elementDeleted.isEmpty {
                self.deleteItems(at: changeSet.elementDeleted.map { IndexPath(item: $0.element, section: $0.section) })
            }

            if !changeSet.elementInserted.isEmpty {
                self.insertItems(at: changeSet.elementInserted.map { IndexPath(item: $0.element, section: $0.section) })
            }

            if !changeSet.elementUpdated.isEmpty {
                self.reloadItems(at: changeSet.elementUpdated.map { IndexPath(item: $0.element, section: $0.section) })
            }

            for (source, target) in changeSet.elementMoved {
                self.moveItem(at: IndexPath(item: source.element, section: source.section), to: IndexPath(item: target.element, section: target.section))
            }
        }
    }

    func performDiffBatchUpdates<C>(changeSet: Changeset<C>) {
        performDiffBatchUpdates(changeSet: changeSet, animated: true)
    }
}

extension ASCellNode {
    static func withNode(_ customNode: ASDisplayNode, margin: UIEdgeInsets = .zero) -> ASCellNode {
        let cell = ASCellNode()
        cell.automaticallyManagesSubnodes = true
        cell.layoutSpecBlock = { node, range in
            customNode.margin(margin)
        }
        return cell
    }
}
